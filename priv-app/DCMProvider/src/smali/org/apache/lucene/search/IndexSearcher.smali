.class public Lorg/apache/lucene/search/IndexSearcher;
.super Ljava/lang/Object;
.source "IndexSearcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;,
        Lorg/apache/lucene/search/IndexSearcher$LeafSlice;,
        Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;,
        Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final defaultSimilarity:Lorg/apache/lucene/search/similarities/Similarity;


# instance fields
.field private final executor:Ljava/util/concurrent/ExecutorService;

.field protected final leafContexts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field protected final leafSlices:[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

.field final reader:Lorg/apache/lucene/index/IndexReader;

.field protected final readerContext:Lorg/apache/lucene/index/IndexReaderContext;

.field private similarity:Lorg/apache/lucene/search/similarities/Similarity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/IndexSearcher;->$assertionsDisabled:Z

    .line 92
    new-instance v0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;

    invoke-direct {v0}, Lorg/apache/lucene/search/similarities/DefaultSimilarity;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/IndexSearcher;->defaultSimilarity:Lorg/apache/lucene/search/similarities/Similarity;

    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;Ljava/util/concurrent/ExecutorService;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "executor"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 125
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReaderContext;Ljava/util/concurrent/ExecutorService;)V

    .line 126
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReaderContext;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/IndexReaderContext;

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReaderContext;Ljava/util/concurrent/ExecutorService;)V

    .line 162
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReaderContext;Ljava/util/concurrent/ExecutorService;)V
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/IndexReaderContext;
    .param p2, "executor"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    sget-object v0, Lorg/apache/lucene/search/IndexSearcher;->defaultSimilarity:Lorg/apache/lucene/search/similarities/Similarity;

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 145
    sget-boolean v0, Lorg/apache/lucene/search/IndexSearcher;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lorg/apache/lucene/index/IndexReaderContext;->isTopLevel:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IndexSearcher\'s ReaderContext must be topLevel for reader"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 146
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 147
    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    .line 148
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher;->readerContext:Lorg/apache/lucene/index/IndexReaderContext;

    .line 149
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    .line 150
    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->leafSlices:[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    .line 151
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->slices(Ljava/util/List;)[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDefaultSimilarity()Lorg/apache/lucene/search/similarities/Similarity;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lorg/apache/lucene/search/IndexSearcher;->defaultSimilarity:Lorg/apache/lucene/search/similarities/Similarity;

    return-object v0
.end method


# virtual methods
.method public collectionStatistics(Ljava/lang/String;)Lorg/apache/lucene/search/CollectionStatistics;
    .locals 12
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 933
    sget-boolean v0, Lorg/apache/lucene/search/IndexSearcher;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 935
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-static {v0, p1}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v11

    .line 936
    .local v11, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v11, :cond_1

    .line 937
    const/4 v10, 0x0

    .line 938
    .local v10, "docCount":I
    const-wide/16 v6, 0x0

    .line 939
    .local v6, "sumTotalTermFreq":J
    const-wide/16 v8, 0x0

    .line 945
    .local v8, "sumDocFreq":J
    :goto_0
    new-instance v0, Lorg/apache/lucene/search/CollectionStatistics;

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    int-to-long v2, v1

    int-to-long v4, v10

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lorg/apache/lucene/search/CollectionStatistics;-><init>(Ljava/lang/String;JJJJ)V

    return-object v0

    .line 941
    .end local v6    # "sumTotalTermFreq":J
    .end local v8    # "sumDocFreq":J
    .end local v10    # "docCount":I
    :cond_1
    invoke-virtual {v11}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v10

    .line 942
    .restart local v10    # "docCount":I
    invoke-virtual {v11}, Lorg/apache/lucene/index/Terms;->getSumTotalTermFreq()J

    move-result-wide v6

    .line 943
    .restart local v6    # "sumTotalTermFreq":J
    invoke-virtual {v11}, Lorg/apache/lucene/index/Terms;->getSumDocFreq()J

    move-result-wide v8

    .restart local v8    # "sumDocFreq":J
    goto :goto_0
.end method

.method public createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;
    .locals 4
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 674
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;

    move-result-object p1

    .line 675
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v2

    .line 676
    .local v2, "weight":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v1

    .line 677
    .local v1, "v":F
    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/similarities/Similarity;->queryNorm(F)F

    move-result v0

    .line 678
    .local v0, "norm":F
    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 679
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 681
    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v0, v3}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 682
    return-object v2
.end method

.method public doc(I)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public doc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/document/Document;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    .local p2, "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public doc(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 1
    .param p1, "docID"    # I
    .param p2, "fieldVisitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 197
    return-void
.end method

.method public final document(ILjava/util/Set;)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/document/Document;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 212
    .local p2, "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->doc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public explain(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/Explanation;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 643
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/search/IndexSearcher;->explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method protected explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 659
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    invoke-static {p2, v3}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(ILjava/util/List;)I

    move-result v2

    .line 660
    .local v2, "n":I
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 661
    .local v0, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    iget v3, v0, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    sub-int v1, p2, v3

    .line 663
    .local v1, "deBasedDoc":I
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v3

    return-object v3
.end method

.method public getIndexReader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    return-object v0
.end method

.method public getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->readerContext:Lorg/apache/lucene/index/IndexReaderContext;

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "original"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 626
    move-object v0, p1

    .line 627
    .local v0, "query":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .local v1, "rewrittenQuery":Lorg/apache/lucene/search/Query;
    :goto_0
    if-ne v1, v0, :cond_0

    .line 631
    return-object v0

    .line 629
    :cond_0
    move-object v0, v1

    .line 628
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    goto :goto_0
.end method

.method protected search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 3
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p4, "nDocs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;",
            "Lorg/apache/lucene/search/Weight;",
            "Lorg/apache/lucene/search/ScoreDoc;",
            "I)",
            "Lorg/apache/lucene/search/TopDocs;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 476
    .local p1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    .line 477
    .local v1, "limit":I
    if-nez v1, :cond_0

    .line 478
    const/4 v1, 0x1

    .line 480
    :cond_0
    invoke-static {p4, v1}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 481
    invoke-virtual {p2}, Lorg/apache/lucene/search/Weight;->scoresDocsOutOfOrder()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-static {p4, p3, v2}, Lorg/apache/lucene/search/TopScoreDocCollector;->create(ILorg/apache/lucene/search/ScoreDoc;Z)Lorg/apache/lucene/search/TopScoreDocCollector;

    move-result-object v0

    .line 482
    .local v0, "collector":Lorg/apache/lucene/search/TopScoreDocCollector;
    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Collector;)V

    .line 483
    invoke-virtual {v0}, Lorg/apache/lucene/search/TopScoreDocCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v2

    return-object v2

    .line 481
    .end local v0    # "collector":Lorg/apache/lucene/search/TopScoreDocCollector;
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public search(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method protected search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 18
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p3, "nDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    if-nez v4, :cond_0

    .line 438
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v4, v1, v2, v3}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v4

    .line 462
    :goto_0
    return-object v4

    .line 440
    :cond_0
    new-instance v11, Lorg/apache/lucene/search/HitQueue;

    const/4 v4, 0x0

    move/from16 v0, p3

    invoke-direct {v11, v0, v4}, Lorg/apache/lucene/search/HitQueue;-><init>(IZ)V

    .line 441
    .local v11, "hq":Lorg/apache/lucene/search/HitQueue;
    new-instance v5, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v5}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 442
    .local v5, "lock":Ljava/util/concurrent/locks/Lock;
    new-instance v14, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v14, v4}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 444
    .local v14, "runner":Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper<Lorg/apache/lucene/search/TopDocs;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/IndexSearcher;->leafSlices:[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    array-length v4, v4

    if-lt v12, v4, :cond_2

    .line 449
    const/16 v17, 0x0

    .line 450
    .local v17, "totalHits":I
    const/high16 v13, -0x800000    # Float.NEGATIVE_INFINITY

    .line 451
    .local v13, "maxScore":F
    invoke-virtual {v14}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 458
    invoke-virtual {v11}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v4

    new-array v15, v4, [Lorg/apache/lucene/search/ScoreDoc;

    .line 459
    .local v15, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual {v11}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v4

    add-int/lit8 v12, v4, -0x1

    :goto_3
    if-gez v12, :cond_4

    .line 462
    new-instance v4, Lorg/apache/lucene/search/TopDocs;

    move/from16 v0, v17

    invoke-direct {v4, v0, v15, v13}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    goto :goto_0

    .line 446
    .end local v13    # "maxScore":F
    .end local v15    # "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v17    # "totalHits":I
    :cond_2
    new-instance v4, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/search/IndexSearcher;->leafSlices:[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    aget-object v7, v6, v12

    move-object/from16 v6, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v10, p3

    invoke-direct/range {v4 .. v11}, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableNoSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/IndexSearcher$LeafSlice;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/HitQueue;)V

    .line 445
    invoke-virtual {v14, v4}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 444
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 451
    .restart local v13    # "maxScore":F
    .restart local v17    # "totalHits":I
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/search/TopDocs;

    .line 452
    .local v16, "topDocs":Lorg/apache/lucene/search/TopDocs;
    move-object/from16 v0, v16

    iget v6, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    if-eqz v6, :cond_1

    .line 453
    move-object/from16 v0, v16

    iget v6, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    add-int v17, v17, v6

    .line 454
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/TopDocs;->getMaxScore()F

    move-result v6

    invoke-static {v13, v6}, Ljava/lang/Math;->max(FF)F

    move-result v13

    goto :goto_2

    .line 460
    .end local v16    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    .restart local v15    # "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    :cond_4
    invoke-virtual {v11}, Lorg/apache/lucene/search/HitQueue;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v4, v15, v12

    .line 459
    add-int/lit8 v12, v12, -0x1

    goto :goto_3
.end method

.method protected search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 9
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "after"    # Lorg/apache/lucene/search/FieldDoc;
    .param p4, "nDocs"    # I
    .param p5, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p6, "fillFields"    # Z
    .param p7, "doDocScores"    # Z
    .param p8, "doMaxScore"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;",
            "Lorg/apache/lucene/search/Weight;",
            "Lorg/apache/lucene/search/FieldDoc;",
            "I",
            "Lorg/apache/lucene/search/Sort;",
            "ZZZ)",
            "Lorg/apache/lucene/search/TopFieldDocs;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 563
    .local p1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v8

    .line 564
    .local v8, "limit":I
    if-nez v8, :cond_0

    .line 565
    const/4 v8, 0x1

    .line 567
    :cond_0
    invoke-static {p4, v8}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 571
    invoke-virtual {p2}, Lorg/apache/lucene/search/Weight;->scoresDocsOutOfOrder()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    :goto_0
    move-object v0, p5

    move v1, p4

    move-object v2, p3

    move v3, p6

    move/from16 v4, p7

    move/from16 v5, p8

    .line 569
    invoke-static/range {v0 .. v6}, Lorg/apache/lucene/search/TopFieldCollector;->create(Lorg/apache/lucene/search/Sort;ILorg/apache/lucene/search/FieldDoc;ZZZZ)Lorg/apache/lucene/search/TopFieldCollector;

    move-result-object v7

    .line 572
    .local v7, "collector":Lorg/apache/lucene/search/TopFieldCollector;
    invoke-virtual {p0, p1, p2, v7}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Collector;)V

    .line 573
    invoke-virtual {v7}, Lorg/apache/lucene/search/TopFieldCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/TopFieldDocs;

    return-object v0

    .line 571
    .end local v7    # "collector":Lorg/apache/lucene/search/TopFieldCollector;
    :cond_1
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public search(Lorg/apache/lucene/search/Query;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "n"    # I
    .param p3, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 378
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;ILorg/apache/lucene/search/Sort;ZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 326
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    move-object v0, p0

    move v2, p3

    move-object v3, p4

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;ILorg/apache/lucene/search/Sort;ZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;ZZ)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 6
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p5, "doDocScores"    # Z
    .param p6, "doMaxScore"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    move-object v0, p0

    move v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;ILorg/apache/lucene/search/Sort;ZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method protected search(Lorg/apache/lucene/search/Weight;ILorg/apache/lucene/search/Sort;ZZ)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 8
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "nDocs"    # I
    .param p3, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p4, "doDocScores"    # Z
    .param p5, "doMaxScore"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 501
    const/4 v2, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method protected search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 18
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "after"    # Lorg/apache/lucene/search/FieldDoc;
    .param p3, "nDocs"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p5, "fillFields"    # Z
    .param p6, "doDocScores"    # Z
    .param p7, "doMaxScore"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 520
    if-nez p4, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v3, "Sort must not be null"

    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 522
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_1

    .line 524
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-virtual/range {v1 .. v9}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v1

    .line 550
    :goto_0
    return-object v1

    .line 531
    :cond_1
    const/4 v7, 0x0

    move-object/from16 v1, p4

    move/from16 v2, p3

    move-object/from16 v3, p2

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    .line 526
    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/search/TopFieldCollector;->create(Lorg/apache/lucene/search/Sort;ILorg/apache/lucene/search/FieldDoc;ZZZZ)Lorg/apache/lucene/search/TopFieldCollector;

    move-result-object v8

    .line 533
    .local v8, "topCollector":Lorg/apache/lucene/search/TopFieldCollector;
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 534
    .local v2, "lock":Ljava/util/concurrent/locks/Lock;
    new-instance v14, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v14, v1}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 535
    .local v14, "runner":Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper<Lorg/apache/lucene/search/TopFieldDocs;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/IndexSearcher;->leafSlices:[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    array-length v1, v1

    if-lt v12, v1, :cond_3

    .line 539
    const/16 v17, 0x0

    .line 540
    .local v17, "totalHits":I
    const/high16 v13, -0x800000    # Float.NEGATIVE_INFINITY

    .line 541
    .local v13, "maxScore":F
    invoke-virtual {v14}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 548
    invoke-virtual {v8}, Lorg/apache/lucene/search/TopFieldCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/search/TopFieldDocs;

    .line 550
    .local v15, "topDocs":Lorg/apache/lucene/search/TopFieldDocs;
    new-instance v1, Lorg/apache/lucene/search/TopFieldDocs;

    iget-object v3, v15, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    iget-object v4, v15, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-virtual {v15}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v5

    move/from16 v0, v17

    invoke-direct {v1, v0, v3, v4, v5}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    goto :goto_0

    .line 537
    .end local v13    # "maxScore":F
    .end local v15    # "topDocs":Lorg/apache/lucene/search/TopFieldDocs;
    .end local v17    # "totalHits":I
    :cond_3
    new-instance v1, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->leafSlices:[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    aget-object v4, v3, v12

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v9, p4

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v1 .. v11}, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/IndexSearcher$LeafSlice;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/TopFieldCollector;Lorg/apache/lucene/search/Sort;ZZ)V

    .line 536
    invoke-virtual {v14, v1}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 535
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 541
    .restart local v13    # "maxScore":F
    .restart local v17    # "totalHits":I
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/search/TopFieldDocs;

    .line 542
    .local v16, "topFieldDocs":Lorg/apache/lucene/search/TopFieldDocs;
    move-object/from16 v0, v16

    iget v3, v0, Lorg/apache/lucene/search/TopFieldDocs;->totalHits:I

    if-eqz v3, :cond_2

    .line 543
    move-object/from16 v0, v16

    iget v3, v0, Lorg/apache/lucene/search/TopFieldDocs;->totalHits:I

    add-int v17, v17, v3

    .line 544
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v3

    invoke-static {v13, v3}, Ljava/lang/Math;->max(FF)F

    move-result v13

    goto :goto_2
.end method

.method protected search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Collector;)V
    .locals 7
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;",
            "Lorg/apache/lucene/search/Weight;",
            "Lorg/apache/lucene/search/Collector;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    const/4 v4, 0x1

    .line 601
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 619
    return-void

    .line 601
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 603
    .local v0, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    :try_start_0
    invoke-virtual {p3, v0}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    :try_end_0
    .catch Lorg/apache/lucene/search/CollectionTerminatedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 609
    invoke-virtual {p3}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    invoke-virtual {p2, v0, v3, v4, v6}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    .line 610
    .local v2, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v2, :cond_0

    .line 612
    :try_start_1
    invoke-virtual {v2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V
    :try_end_1
    .catch Lorg/apache/lucene/search/CollectionTerminatedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 613
    :catch_0
    move-exception v3

    goto :goto_0

    .line 604
    .end local v2    # "scorer":Lorg/apache/lucene/search/Scorer;
    :catch_1
    move-exception v1

    .line 607
    .local v1, "e":Lorg/apache/lucene/search/CollectionTerminatedException;
    goto :goto_0

    .end local v1    # "e":Lorg/apache/lucene/search/CollectionTerminatedException;
    :cond_2
    move v3, v4

    .line 609
    goto :goto_1
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Collector;)V

    .line 310
    return-void
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->leafContexts:Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Collector;)V

    .line 298
    return-void
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;
    .locals 8
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 393
    if-eqz p1, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/search/FieldDoc;

    if-nez v0, :cond_0

    .line 396
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "after must be a FieldDoc; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398
    :cond_0
    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/search/FieldDoc;

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p3

    move-object v4, p4

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p4, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p4}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;
    .locals 8
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p4, "n"    # I
    .param p5, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 360
    if-eqz p1, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/search/FieldDoc;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "after must be a FieldDoc; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_0
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/search/FieldDoc;

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p4

    move-object v4, p5

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;ZZ)Lorg/apache/lucene/search/TopDocs;
    .locals 8
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p4, "n"    # I
    .param p5, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p6, "doDocScores"    # Z
    .param p7, "doMaxScore"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 419
    if-eqz p1, :cond_0

    instance-of v0, p1, Lorg/apache/lucene/search/FieldDoc;

    if-nez v0, :cond_0

    .line 422
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "after must be a FieldDoc; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :cond_0
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/search/FieldDoc;

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p4

    move-object v4, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public setSimilarity(Lorg/apache/lucene/search/similarities/Similarity;)V
    .locals 0
    .param p1, "similarity"    # Lorg/apache/lucene/search/similarities/Similarity;

    .prologue
    .line 219
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 220
    return-void
.end method

.method protected slices(Ljava/util/List;)[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;)[",
            "Lorg/apache/lucene/search/IndexSearcher$LeafSlice;"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    .line 171
    .local v1, "slices":[Lorg/apache/lucene/search/IndexSearcher$LeafSlice;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 174
    return-object v1

    .line 172
    :cond_0
    new-instance v3, Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    const/4 v2, 0x1

    new-array v4, v2, [Lorg/apache/lucene/index/AtomicReaderContext;

    const/4 v5, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReaderContext;

    aput-object v2, v4, v5

    invoke-direct {v3, v4}, Lorg/apache/lucene/search/IndexSearcher$LeafSlice;-><init>([Lorg/apache/lucene/index/AtomicReaderContext;)V

    aput-object v3, v1, v0

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public termStatistics(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)Lorg/apache/lucene/search/TermStatistics;
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "context"    # Lorg/apache/lucene/index/TermContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 918
    new-instance v0, Lorg/apache/lucene/search/TermStatistics;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p2}, Lorg/apache/lucene/index/TermContext;->totalTermFreq()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermStatistics;-><init>(Lorg/apache/lucene/util/BytesRef;JJ)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 907
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IndexSearcher("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; executor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected wrapFilter(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 228
    if-nez p2, :cond_0

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object p1

    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/FilteredQuery;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/search/FilteredQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)V

    move-object p1, v0

    goto :goto_0
.end method
