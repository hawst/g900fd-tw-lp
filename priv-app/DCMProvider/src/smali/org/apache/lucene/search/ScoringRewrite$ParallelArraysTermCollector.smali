.class final Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;
.super Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
.source "ScoringRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ScoringRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ParallelArraysTermCollector"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

.field private boostAtt:Lorg/apache/lucene/search/BoostAttribute;

.field final terms:Lorg/apache/lucene/util/BytesRefHash;

.field termsEnum:Lorg/apache/lucene/index/TermsEnum;

.field final synthetic this$0:Lorg/apache/lucene/search/ScoringRewrite;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    const-class v0, Lorg/apache/lucene/search/ScoringRewrite;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/ScoringRewrite;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 125
    iput-object p1, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->this$0:Lorg/apache/lucene/search/ScoringRewrite;

    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;-><init>()V

    .line 126
    new-instance v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    invoke-direct {v0, v3}, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    .line 127
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash;

    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool;

    new-instance v2, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;

    invoke-direct {v2}, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;-><init>()V

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    iput-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->terms:Lorg/apache/lucene/util/BytesRefHash;

    return-void
.end method


# virtual methods
.method public collect(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 12
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->terms:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;)I

    move-result v10

    .line 141
    .local v10, "e":I
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v1

    .line 142
    .local v1, "state":Lorg/apache/lucene/index/TermState;
    sget-boolean v0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 143
    :cond_0
    if-gez v10, :cond_1

    .line 145
    neg-int v0, v10

    add-int/lit8 v11, v0, -0x1

    .line 146
    .local v11, "pos":I
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    iget-object v0, v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    aget-object v0, v0, v11

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v2, v2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    iget-object v3, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    .line 147
    sget-boolean v0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    iget-object v0, v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;->boost:[F

    aget v0, v0, v11

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/search/BoostAttribute;->getBoost()F

    move-result v2

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "boost should be equal in all segment TermsEnums"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 150
    .end local v11    # "pos":I
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    iget-object v0, v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;->boost:[F

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/search/BoostAttribute;->getBoost()F

    move-result v2

    aput v2, v0, v10

    .line 151
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->array:Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;

    iget-object v0, v0, Lorg/apache/lucene/search/ScoringRewrite$TermFreqBoostByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    new-instance v3, Lorg/apache/lucene/index/TermContext;

    iget-object v4, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v6, v2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v7

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v8

    move-object v5, v1

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/TermState;IIJ)V

    aput-object v3, v0, v10

    .line 152
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->this$0:Lorg/apache/lucene/search/ScoringRewrite;

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->terms:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/ScoringRewrite;->checkMaxClauseCount(I)V

    .line 154
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public setNextEnum(Lorg/apache/lucene/index/TermsEnum;)V
    .locals 2
    .param p1, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 134
    iput-object p1, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 135
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    const-class v1, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BoostAttribute;

    iput-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$ParallelArraysTermCollector;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    .line 136
    return-void
.end method
