.class public Lorg/apache/lucene/search/WildcardQuery;
.super Lorg/apache/lucene/search/AutomatonQuery;
.source "WildcardQuery.java"


# static fields
.field public static final WILDCARD_CHAR:C = '?'

.field public static final WILDCARD_ESCAPE:C = '\\'

.field public static final WILDCARD_STRING:C = '*'


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 57
    invoke-static {p1}, Lorg/apache/lucene/search/WildcardQuery;->toAutomaton(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/AutomatonQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/automaton/Automaton;)V

    .line 58
    return-void
.end method

.method public static toAutomaton(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 8
    .param p0, "wildcardquery"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v0, "automata":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/Automaton;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    .line 70
    .local v5, "wildcardText":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v2, v6, :cond_0

    .line 94
    invoke-static {v0}, Lorg/apache/lucene/util/automaton/BasicOperations;->concatenate(Ljava/util/List;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    return-object v6

    .line 71
    :cond_0
    invoke-virtual {v5, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    .line 72
    .local v1, "c":I
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    .line 73
    .local v3, "length":I
    sparse-switch v1, :sswitch_data_0

    .line 89
    :cond_1
    invoke-static {v1}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeChar(I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :goto_1
    add-int/2addr v2, v3

    goto :goto_0

    .line 75
    :sswitch_0
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeAnyString()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 78
    :sswitch_1
    invoke-static {}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeAnyChar()Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 82
    :sswitch_2
    add-int v6, v2, v3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 83
    add-int v6, v2, v3

    invoke-virtual {v5, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 84
    .local v4, "nextChar":I
    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 85
    invoke-static {v4}, Lorg/apache/lucene/util/automaton/BasicAutomata;->makeChar(I)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 73
    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_0
        0x3f -> :sswitch_1
        0x5c -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/search/WildcardQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lorg/apache/lucene/search/WildcardQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {p0}, Lorg/apache/lucene/search/WildcardQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
