.class public abstract Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;
.super Lorg/apache/lucene/search/similarities/Similarity;
.source "PerFieldSimilarityWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity;-><init>()V

    return-void
.end method


# virtual methods
.method public final computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 45
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;->get(Ljava/lang/String;)Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/similarities/Similarity;->computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final varargs computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .locals 2
    .param p1, "queryBoost"    # F
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;

    invoke-direct {v0}, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;-><init>()V

    .line 51
    .local v0, "weight":Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;
    invoke-virtual {p2}, Lorg/apache/lucene/search/CollectionStatistics;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;->get(Ljava/lang/String;)Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegate:Lorg/apache/lucene/search/similarities/Similarity;

    .line 52
    iget-object v1, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegate:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v1, p1, p2, p3}, Lorg/apache/lucene/search/similarities/Similarity;->computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegateWeight:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 53
    return-object v0
.end method

.method public final exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .locals 3
    .param p1, "weight"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;

    .line 59
    .local v0, "perFieldWeight":Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;
    iget-object v1, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegate:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v2, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegateWeight:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/search/similarities/Similarity;->exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    move-result-object v1

    return-object v1
.end method

.method public abstract get(Ljava/lang/String;)Lorg/apache/lucene/search/similarities/Similarity;
.end method

.method public final sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .locals 3
    .param p1, "weight"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;

    .line 65
    .local v0, "perFieldWeight":Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;
    iget-object v1, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegate:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v2, v0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegateWeight:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v1

    return-object v1
.end method
