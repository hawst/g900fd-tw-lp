.class public Lorg/apache/lucene/search/SortField;
.super Ljava/lang/Object;
.source "SortField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/SortField$Type;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$search$SortField$Type:[I

.field static final synthetic $assertionsDisabled:Z

.field public static final FIELD_DOC:Lorg/apache/lucene/search/SortField;

.field public static final FIELD_SCORE:Lorg/apache/lucene/search/SortField;


# instance fields
.field private bytesComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

.field private field:Ljava/lang/String;

.field public missingValue:Ljava/lang/Object;

.field private parser:Lorg/apache/lucene/search/FieldCache$Parser;

.field reverse:Z

.field private type:Lorg/apache/lucene/search/SortField$Type;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$search$SortField$Type()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/search/SortField;->$SWITCH_TABLE$org$apache$lucene$search$SortField$Type:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/search/SortField$Type;->values()[Lorg/apache/lucene/search/SortField$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->BYTE:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->BYTES:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->CUSTOM:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->DOC:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->DOUBLE:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->FLOAT:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->LONG:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->REWRITEABLE:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->SCORE:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->SHORT:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->STRING:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->STRING_VAL:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lorg/apache/lucene/search/SortField;->$SWITCH_TABLE$org$apache$lucene$search$SortField$Type:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    const-class v0, Lorg/apache/lucene/search/SortField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/SortField;->$assertionsDisabled:Z

    .line 101
    new-instance v0, Lorg/apache/lucene/search/SortField;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->SCORE:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    sput-object v0, Lorg/apache/lucene/search/SortField;->FIELD_SCORE:Lorg/apache/lucene/search/SortField;

    .line 104
    new-instance v0, Lorg/apache/lucene/search/SortField;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->DOC:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    sput-object v0, Lorg/apache/lucene/search/SortField;->FIELD_DOC:Lorg/apache/lucene/search/SortField;

    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Z)V

    .line 151
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Z)V
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "reverse"    # Z

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 349
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    .line 165
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$IntParser;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 175
    :goto_0
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 176
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    .line 177
    return-void

    .line 166
    :cond_0
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$FloatParser;

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->FLOAT:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    goto :goto_0

    .line 167
    :cond_1
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$ShortParser;

    if-eqz v0, :cond_2

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->SHORT:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    goto :goto_0

    .line 168
    :cond_2
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$ByteParser;

    if-eqz v0, :cond_3

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->BYTE:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    goto :goto_0

    .line 169
    :cond_3
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$LongParser;

    if-eqz v0, :cond_4

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->LONG:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    goto :goto_0

    .line 170
    :cond_4
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$DoubleParser;

    if-eqz v0, :cond_5

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->DOUBLE:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    goto :goto_0

    .line 172
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Parser instance does not subclass existing numeric parser from FieldCache (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldComparatorSource;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "comparator"    # Lorg/apache/lucene/search/FieldComparatorSource;

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 349
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    .line 192
    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->CUSTOM:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 193
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    .line 194
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldComparatorSource;Z)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "comparator"    # Lorg/apache/lucene/search/FieldComparatorSource;
    .param p3, "reverse"    # Z

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 349
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    .line 202
    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->CUSTOM:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 203
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 204
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    .line 205
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # Lorg/apache/lucene/search/SortField$Type;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 349
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    .line 124
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;Z)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # Lorg/apache/lucene/search/SortField$Type;
    .param p3, "reverse"    # Z

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 349
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    .line 135
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 136
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 137
    return-void
.end method

.method private initFieldType(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # Lorg/apache/lucene/search/SortField$Type;

    .prologue
    .line 210
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    .line 211
    if-nez p1, :cond_0

    .line 212
    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->SCORE:Lorg/apache/lucene/search/SortField$Type;

    if-eq p2, v0, :cond_1

    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->DOC:Lorg/apache/lucene/search/SortField$Type;

    if-eq p2, v0, :cond_1

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "field can only be null when type is SCORE or DOC"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    .line 218
    :cond_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    if-ne p0, p1, :cond_1

    .line 328
    :cond_0
    :goto_0
    return v1

    .line 326
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/SortField;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 327
    check-cast v0, Lorg/apache/lucene/search/SortField;

    .line 329
    .local v0, "other":Lorg/apache/lucene/search/SortField;
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-static {v3, v4}, Lorg/apache/lucene/util/StringHelper;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 330
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    if-ne v3, v4, :cond_3

    .line 331
    iget-boolean v3, v0, Lorg/apache/lucene/search/SortField;->reverse:Z

    iget-boolean v4, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    if-ne v3, v4, :cond_3

    .line 332
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-nez v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 328
    goto :goto_0

    .line 332
    :cond_4
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getBytesComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public getComparator(II)Lorg/apache/lucene/search/FieldComparator;
    .locals 4
    .param p1, "numHits"    # I
    .param p2, "sortPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    invoke-static {}, Lorg/apache/lucene/search/SortField;->$SWITCH_TABLE$org$apache$lucene$search$SortField$Type()[I

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 412
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal sort type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :pswitch_1
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;-><init>(I)V

    .line 406
    :goto_0
    return-object v0

    .line 378
    :pswitch_2
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$DocComparator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/FieldComparator$DocComparator;-><init>(I)V

    goto :goto_0

    .line 381
    :pswitch_3
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$IntComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$IntComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Integer;)V

    move-object v0, v1

    goto :goto_0

    .line 384
    :pswitch_4
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$FloatComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$FloatComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Float;)V

    move-object v0, v1

    goto :goto_0

    .line 387
    :pswitch_5
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$LongComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$LongComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Long;)V

    move-object v0, v1

    goto :goto_0

    .line 390
    :pswitch_6
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;)V

    move-object v0, v1

    goto :goto_0

    .line 393
    :pswitch_7
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$ByteComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$ByteComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;)V

    move-object v0, v1

    goto :goto_0

    .line 396
    :pswitch_8
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$ShortComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Short;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$ShortComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Short;)V

    move-object v0, v1

    goto :goto_0

    .line 399
    :pswitch_9
    sget-boolean v0, Lorg/apache/lucene/search/SortField;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 400
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-boolean v2, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    invoke-virtual {v0, v1, p1, p2, v2}, Lorg/apache/lucene/search/FieldComparatorSource;->newComparator(Ljava/lang/String;IIZ)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    goto :goto_0

    .line 403
    :pswitch_a
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/FieldComparator$TermOrdValComparator;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 406
    :pswitch_b
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$TermValComparator;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/FieldComparator$TermValComparator;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 409
    :pswitch_c
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SortField needs to be rewritten through Sort.rewrite(..) and SortField.rewrite(..)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_a
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_b
        :pswitch_0
        :pswitch_c
    .end packed-switch
.end method

.method public getComparatorSource()Lorg/apache/lucene/search/FieldComparatorSource;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getParser()Lorg/apache/lucene/search/FieldCache$Parser;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    return-object v0
.end method

.method public getReverse()Z
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    return v0
.end method

.method public getType()Lorg/apache/lucene/search/SortField$Type;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 343
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v1}, Lorg/apache/lucene/search/SortField$Type;->hashCode()I

    move-result v1

    const v2, 0x346565dd

    iget-boolean v3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    xor-int/2addr v1, v2

    const v2, -0x50a66745

    xor-int v0, v1, v2

    .line 344
    .local v0, "hash":I
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0xa97a23

    xor-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 345
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    :cond_1
    return v0
.end method

.method public rewrite(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/SortField;
    .locals 0
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    return-object p0
.end method

.method public setBytesComparator(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 352
    .local p1, "b":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    iput-object p1, p0, Lorg/apache/lucene/search/SortField;->bytesComparator:Ljava/util/Comparator;

    .line 353
    return-void
.end method

.method public setMissingValue(Ljava/lang/Object;)Lorg/apache/lucene/search/SortField;
    .locals 2
    .param p1, "missingValue"    # Ljava/lang/Object;

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->BYTE:Lorg/apache/lucene/search/SortField$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->SHORT:Lorg/apache/lucene/search/SortField$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->FLOAT:Lorg/apache/lucene/search/SortField$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->LONG:Lorg/apache/lucene/search/SortField$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->DOUBLE:Lorg/apache/lucene/search/SortField$Type;

    if-eq v0, v1, :cond_0

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing value only works for numeric types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 184
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-static {}, Lorg/apache/lucene/search/SortField;->$SWITCH_TABLE$org$apache$lucene$search$SortField$Type()[I

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->type:Lorg/apache/lucene/search/SortField$Type;

    invoke-virtual {v2}, Lorg/apache/lucene/search/SortField$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 310
    :pswitch_0
    const-string v1, "<???: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :goto_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 262
    :pswitch_1
    const-string v1, "<score>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 266
    :pswitch_2
    const-string v1, "<doc>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 270
    :pswitch_3
    const-string v1, "<string: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 274
    :pswitch_4
    const-string v1, "<string_val: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 278
    :pswitch_5
    const-string v1, "<byte: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 282
    :pswitch_6
    const-string v1, "<short: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 286
    :pswitch_7
    const-string v1, "<int: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 290
    :pswitch_8
    const-string v1, "<long: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 294
    :pswitch_9
    const-string v1, "<float: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 298
    :pswitch_a
    const-string v1, "<double: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 302
    :pswitch_b
    const-string v1, "<custom:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 306
    :pswitch_c
    const-string v1, "<rewriteable: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 260
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_6
        :pswitch_b
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_c
    .end packed-switch
.end method
