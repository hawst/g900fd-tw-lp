.class Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;
.super Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
.source "BM25Similarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/BM25Similarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BM25Stats"
.end annotation


# instance fields
.field private final avgdl:F

.field private final cache:[F

.field private final field:Ljava/lang/String;

.field private final idf:Lorg/apache/lucene/search/Explanation;

.field private final queryBoost:F

.field private topLevelBoost:F

.field private weight:F


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/Explanation;FF[F)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "idf"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "queryBoost"    # F
    .param p4, "avgdl"    # F
    .param p5, "cache"    # [F

    .prologue
    .line 335
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;-><init>()V

    .line 336
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->field:Ljava/lang/String;

    .line 337
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->idf:Lorg/apache/lucene/search/Explanation;

    .line 338
    iput p3, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->queryBoost:F

    .line 339
    iput p4, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->avgdl:F

    .line 340
    iput-object p5, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->cache:[F

    .line 341
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->weight:F

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)[F
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->cache:[F

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->field:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->queryBoost:F

    return v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->topLevelBoost:F

    return v0
.end method

.method static synthetic access$5(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)Lorg/apache/lucene/search/Explanation;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->idf:Lorg/apache/lucene/search/Explanation;

    return-object v0
.end method

.method static synthetic access$6(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->avgdl:F

    return v0
.end method


# virtual methods
.method public getValueForNormalization()F
    .locals 3

    .prologue
    .line 346
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->idf:Lorg/apache/lucene/search/Explanation;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->queryBoost:F

    mul-float v0, v1, v2

    .line 347
    .local v0, "queryWeight":F
    mul-float v1, v0, v0

    return v1
.end method

.method public normalize(FF)V
    .locals 2
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 353
    iput p2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->topLevelBoost:F

    .line 354
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->idf:Lorg/apache/lucene/search/Explanation;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->queryBoost:F

    mul-float/2addr v0, v1

    mul-float/2addr v0, p2

    iput v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->weight:F

    .line 355
    return-void
.end method
