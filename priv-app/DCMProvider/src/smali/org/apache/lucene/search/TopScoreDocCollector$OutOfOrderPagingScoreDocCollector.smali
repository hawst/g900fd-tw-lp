.class Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;
.super Lorg/apache/lucene/search/TopScoreDocCollector;
.source "TopScoreDocCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopScoreDocCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OutOfOrderPagingScoreDocCollector"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final after:Lorg/apache/lucene/search/ScoreDoc;

.field private afterDoc:I

.field private collectedHits:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    const-class v0, Lorg/apache/lucene/search/TopScoreDocCollector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/search/ScoreDoc;I)V
    .locals 1
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "numHits"    # I

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lorg/apache/lucene/search/TopScoreDocCollector;-><init>(ILorg/apache/lucene/search/TopScoreDocCollector;)V

    .line 175
    iput-object p1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    .line 176
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;-><init>(Lorg/apache/lucene/search/ScoreDoc;I)V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 2
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 183
    .local v0, "score":F
    sget-boolean v1, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 185
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->totalHits:I

    .line 186
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->afterDoc:I

    if-gt p1, v1, :cond_2

    .line 203
    :cond_1
    :goto_0
    return-void

    .line 190
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 194
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->docBase:I

    add-int/2addr p1, v1

    .line 195
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    if-gt p1, v1, :cond_1

    .line 199
    :cond_3
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->collectedHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->collectedHits:I

    .line 200
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iput p1, v1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 201
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iput v0, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 202
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    iput-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    goto :goto_0
.end method

.method protected newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 4
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "start"    # I

    .prologue
    .line 223
    if-nez p1, :cond_0

    new-instance v0, Lorg/apache/lucene/search/TopDocs;

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->totalHits:I

    const/4 v2, 0x0

    new-array v2, v2, [Lorg/apache/lucene/search/ScoreDoc;

    const/high16 v3, 0x7fc00000    # NaNf

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/TopDocs;

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->totalHits:I

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;)V

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;

    .prologue
    .line 212
    invoke-super {p0, p1}, Lorg/apache/lucene/search/TopScoreDocCollector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 213
    iget-object v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v0, v0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->docBase:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->afterDoc:I

    .line 214
    return-void
.end method

.method protected topDocsSize()I
    .locals 2

    .prologue
    .line 218
    iget v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->collectedHits:I

    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->collectedHits:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderPagingScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v0

    goto :goto_0
.end method
