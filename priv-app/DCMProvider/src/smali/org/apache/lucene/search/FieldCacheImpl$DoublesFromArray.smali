.class Lorg/apache/lucene/search/FieldCacheImpl$DoublesFromArray;
.super Lorg/apache/lucene/search/FieldCache$Doubles;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DoublesFromArray"
.end annotation


# instance fields
.field private final values:[D


# direct methods
.method public constructor <init>([D)V
    .locals 0
    .param p1, "values"    # [D

    .prologue
    .line 994
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Doubles;-><init>()V

    .line 995
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$DoublesFromArray;->values:[D

    .line 996
    return-void
.end method


# virtual methods
.method public get(I)D
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 1000
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$DoublesFromArray;->values:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method
