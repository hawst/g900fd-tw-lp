.class public Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;
.super Lorg/apache/lucene/search/similarities/LMSimilarity;
.source "LMDirichletSimilarity.java"


# instance fields
.field private final mu:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    const/high16 v0, 0x44fa0000    # 2000.0f

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;-><init>(F)V

    .line 62
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0
    .param p1, "mu"    # F

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/LMSimilarity;-><init>()V

    .line 51
    iput p1, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    .line 52
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;)V
    .locals 1
    .param p1, "collectionModel"    # Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;

    .prologue
    .line 56
    const/high16 v0, 0x44fa0000    # 2000.0f

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;-><init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;F)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;F)V
    .locals 0
    .param p1, "collectionModel"    # Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;
    .param p2, "mu"    # F

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/similarities/LMSimilarity;-><init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;)V

    .line 46
    iput p2, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    .line 47
    return-void
.end method


# virtual methods
.method protected explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V
    .locals 5
    .param p1, "expl"    # Lorg/apache/lucene/search/Explanation;
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "doc"    # I
    .param p4, "freq"    # F
    .param p5, "docLen"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 75
    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v1

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_0

    .line 76
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v2

    const-string v3, "boost"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 79
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    iget v2, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    const-string v3, "mu"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 80
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 82
    .local v0, "weightExpl":Lorg/apache/lucene/search/Explanation;
    iget v2, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    move-object v1, p2

    check-cast v1, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;->getCollectionProbability()F

    move-result v1

    mul-float/2addr v1, v2

    div-float v1, p4, v1

    add-float/2addr v1, v4

    float-to-double v2, v1

    .line 81
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 83
    const-string v1, "term weight"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 85
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    .line 86
    iget v2, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    iget v3, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    add-float/2addr v3, p5

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v2, v2

    const-string v3, "document norm"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 85
    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 87
    invoke-super/range {p0 .. p5}, Lorg/apache/lucene/search/similarities/LMSimilarity;->explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V

    .line 88
    return-void
.end method

.method public getMu()F
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 97
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v1, "Dirichlet(%f)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->getMu()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 8
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "freq"    # F
    .param p3, "docLen"    # F

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 67
    iget v4, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    check-cast p1, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;

    .end local p1    # "stats":Lorg/apache/lucene/search/similarities/BasicStats;
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;->getCollectionProbability()F

    move-result v5

    mul-float/2addr v4, v5

    div-float v4, p2, v4

    add-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    .line 68
    iget v3, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    iget v6, p0, Lorg/apache/lucene/search/similarities/LMDirichletSimilarity;->mu:F

    add-float/2addr v6, p3

    div-float/2addr v3, v6

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    add-double/2addr v4, v6

    double-to-float v3, v4

    .line 66
    mul-float v0, v2, v3

    .line 69
    .local v0, "score":F
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    .end local v0    # "score":F
    :goto_0
    return v0

    .restart local v0    # "score":F
    :cond_0
    move v0, v1

    goto :goto_0
.end method
