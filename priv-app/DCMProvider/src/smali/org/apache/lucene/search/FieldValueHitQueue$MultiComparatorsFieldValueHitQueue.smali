.class final Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;
.super Lorg/apache/lucene/search/FieldValueHitQueue;
.source "FieldValueHitQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldValueHitQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MultiComparatorsFieldValueHitQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
        ">",
        "Lorg/apache/lucene/search/FieldValueHitQueue",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const-class v0, Lorg/apache/lucene/search/FieldValueHitQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/search/SortField;I)V
    .locals 5
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>.MultiComparatorsFieldValueHitQueue<TT;>;"
    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, v3}, Lorg/apache/lucene/search/FieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;ILorg/apache/lucene/search/FieldValueHitQueue;)V

    .line 105
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v2, v3

    .line 106
    .local v2, "numComparators":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 112
    return-void

    .line 107
    :cond_0
    aget-object v0, p1, v1

    .line 109
    .local v0, "field":Lorg/apache/lucene/search/SortField;
    iget-object v4, p0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->reverseMul:[I

    iget-boolean v3, v0, Lorg/apache/lucene/search/SortField;->reverse:Z

    if-eqz v3, :cond_1

    const/4 v3, -0x1

    :goto_1
    aput v3, v4, v1

    .line 110
    invoke-virtual {v0, p2, v1}, Lorg/apache/lucene/search/SortField;->getComparator(II)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->setComparator(ILorg/apache/lucene/search/FieldComparator;)V

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected lessThan(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Z
    .locals 9

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>.MultiComparatorsFieldValueHitQueue<TT;>;"
    .local p1, "hitA":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;, "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;"
    .local p2, "hitB":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;, "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 117
    sget-boolean v5, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-ne p1, p2, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 118
    :cond_0
    sget-boolean v5, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget v5, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    iget v6, p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    if-ne v5, v6, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 120
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v2, v5

    .line 121
    .local v2, "numComparators":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_3

    .line 130
    iget v5, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    iget v6, p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v5, v6, :cond_5

    :cond_2
    :goto_1
    return v3

    .line 122
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->reverseMul:[I

    aget v5, v5, v1

    iget-object v6, p0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v6, v6, v1

    iget v7, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    iget v8, p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/search/FieldComparator;->compare(II)I

    move-result v6

    mul-int v0, v5, v6

    .line 123
    .local v0, "c":I
    if-eqz v0, :cond_4

    .line 125
    if-gtz v0, :cond_2

    move v3, v4

    goto :goto_1

    .line 121
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "c":I
    :cond_5
    move v3, v4

    .line 130
    goto :goto_1
.end method
