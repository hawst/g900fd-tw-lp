.class public Lorg/apache/lucene/search/SearcherFactory;
.super Ljava/lang/Object;
.source "SearcherFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public newSearcher(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lorg/apache/lucene/search/IndexSearcher;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-object v0
.end method
