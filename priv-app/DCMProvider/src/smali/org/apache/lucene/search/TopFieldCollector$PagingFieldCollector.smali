.class final Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;
.super Lorg/apache/lucene/search/TopFieldCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PagingFieldCollector"
.end annotation


# instance fields
.field final after:Lorg/apache/lucene/search/FieldDoc;

.field afterDoc:I

.field collectedHits:I

.field final comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field final queue:Lorg/apache/lucene/search/FieldValueHitQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;"
        }
    .end annotation
.end field

.field final reverseMul:[I

.field scorer:Lorg/apache/lucene/search/Scorer;

.field final trackDocScores:Z

.field final trackMaxScore:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;Lorg/apache/lucene/search/FieldDoc;IZZZ)V
    .locals 1
    .param p2, "after"    # Lorg/apache/lucene/search/FieldDoc;
    .param p3, "numHits"    # I
    .param p4, "fillFields"    # Z
    .param p5, "trackDocScores"    # Z
    .param p6, "trackMaxScore"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;",
            "Lorg/apache/lucene/search/FieldDoc;",
            "IZZZ)V"
        }
    .end annotation

    .prologue
    .line 862
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, p4, v0}, Lorg/apache/lucene/search/TopFieldCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;IZLorg/apache/lucene/search/TopFieldCollector;)V

    .line 863
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    .line 864
    iput-boolean p5, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackDocScores:Z

    .line 865
    iput-boolean p6, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackMaxScore:Z

    .line 866
    iput-object p2, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->after:Lorg/apache/lucene/search/FieldDoc;

    .line 867
    invoke-virtual {p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getComparators()[Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 868
    invoke-virtual {p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getReverseMul()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->reverseMul:[I

    .line 871
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->maxScore:F

    .line 872
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 990
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 11
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 883
    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->totalHits:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->totalHits:I

    .line 889
    const/4 v5, 0x1

    .line 890
    .local v5, "sameValues":Z
    const/4 v3, 0x0

    .local v3, "compIDX":I
    :goto_0
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v8, v8

    if-lt v3, v8, :cond_1

    .line 907
    :goto_1
    if-eqz v5, :cond_3

    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->afterDoc:I

    if-gt p1, v8, :cond_3

    .line 978
    :cond_0
    :goto_2
    return-void

    .line 891
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v2, v8, v3

    .line 893
    .local v2, "comp":Lorg/apache/lucene/search/FieldComparator;
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->reverseMul:[I

    aget v8, v8, v3

    iget-object v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->after:Lorg/apache/lucene/search/FieldDoc;

    iget-object v9, v9, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v9, v9, v3

    invoke-virtual {v2, p1, v9}, Lorg/apache/lucene/search/FieldComparator;->compareDocToValue(ILjava/lang/Object;)I

    move-result v9

    mul-int v1, v8, v9

    .line 894
    .local v1, "cmp":I
    if-ltz v1, :cond_0

    .line 898
    if-lez v1, :cond_2

    .line 900
    const/4 v5, 0x0

    .line 902
    goto :goto_1

    .line 890
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 913
    .end local v1    # "cmp":I
    .end local v2    # "comp":Lorg/apache/lucene/search/FieldComparator;
    :cond_3
    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->collectedHits:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->collectedHits:I

    .line 915
    const/high16 v6, 0x7fc00000    # NaNf

    .line 916
    .local v6, "score":F
    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackMaxScore:Z

    if-eqz v8, :cond_4

    .line 917
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v6

    .line 918
    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->maxScore:F

    cmpl-float v8, v6, v8

    if-lez v8, :cond_4

    .line 919
    iput v6, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->maxScore:F

    .line 923
    :cond_4
    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->queueFull:Z

    if-eqz v8, :cond_a

    .line 925
    const/4 v4, 0x0

    .line 926
    .local v4, "i":I
    :goto_3
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->reverseMul:[I

    aget v8, v8, v4

    iget-object v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v9, v9, v4

    invoke-virtual {v9, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v9

    mul-int v0, v8, v9

    .line 927
    .local v0, "c":I
    if-ltz v0, :cond_0

    .line 930
    if-lez v0, :cond_7

    .line 944
    :cond_5
    const/4 v4, 0x0

    :goto_4
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v8, v8

    if-lt v4, v8, :cond_9

    .line 949
    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackDocScores:Z

    if-eqz v8, :cond_6

    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackMaxScore:Z

    if-nez v8, :cond_6

    .line 950
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v6

    .line 952
    :cond_6
    invoke-virtual {p0, p1, v6}, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->updateBottom(IF)V

    .line 954
    const/4 v4, 0x0

    :goto_5
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v8, v8

    if-ge v4, v8, :cond_0

    .line 955
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v8, v8, v4

    iget-object v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v9, v9, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 954
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 933
    :cond_7
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    if-ne v4, v8, :cond_8

    .line 935
    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->docBase:I

    add-int/2addr v8, p1

    iget-object v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v9, v9, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v8, v9, :cond_5

    goto/16 :goto_2

    .line 925
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 945
    :cond_9
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v8, v8, v4

    iget-object v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v9, v9, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v8, v9, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 944
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 959
    .end local v0    # "c":I
    .end local v4    # "i":I
    :cond_a
    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->collectedHits:I

    add-int/lit8 v7, v8, -0x1

    .line 962
    .local v7, "slot":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_6
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v8, v8

    if-lt v4, v8, :cond_c

    .line 967
    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackDocScores:Z

    if-eqz v8, :cond_b

    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->trackMaxScore:Z

    if-nez v8, :cond_b

    .line 968
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v6

    .line 970
    :cond_b
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    new-instance v9, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v10, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->docBase:I

    add-int/2addr v10, p1

    invoke-direct {v9, v7, v10, v6}, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;-><init>(IIF)V

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 971
    iget v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->collectedHits:I

    iget v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->numHits:I

    if-ne v8, v9, :cond_d

    const/4 v8, 0x1

    :goto_7
    iput-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->queueFull:Z

    .line 972
    iget-boolean v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->queueFull:Z

    if-eqz v8, :cond_0

    .line 973
    const/4 v4, 0x0

    :goto_8
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v8, v8

    if-ge v4, v8, :cond_0

    .line 974
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v8, v8, v4

    iget-object v9, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v9, v9, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 973
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 963
    :cond_c
    iget-object v8, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v8, v8, v4

    invoke-virtual {v8, v7, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 962
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 971
    :cond_d
    const/4 v8, 0x0

    goto :goto_7
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 995
    iget v1, p1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    iput v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->docBase:I

    .line 996
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->after:Lorg/apache/lucene/search/FieldDoc;

    iget v1, v1, Lorg/apache/lucene/search/FieldDoc;->doc:I

    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->docBase:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->afterDoc:I

    .line 997
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 1000
    return-void

    .line 998
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/search/FieldValueHitQueue;->setComparator(ILorg/apache/lucene/search/FieldComparator;)V

    .line 997
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 982
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 983
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 986
    return-void

    .line 984
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldComparator;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 983
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method updateBottom(IF)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 875
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 876
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput p2, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    .line 877
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$PagingFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 878
    return-void
.end method
