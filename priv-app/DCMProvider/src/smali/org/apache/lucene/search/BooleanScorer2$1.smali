.class Lorg/apache/lucene/search/BooleanScorer2$1;
.super Lorg/apache/lucene/search/MinShouldMatchSumScorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/search/Weight;
    .param p4, "$anonymous2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    .local p3, "$anonymous1":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    .line 161
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V

    return-void
.end method


# virtual methods
.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    invoke-static {v0}, Lorg/apache/lucene/search/BooleanScorer2;->access$2(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    move-result-object v0

    iget v1, v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    iget v2, p0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->nrMatchers:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 165
    invoke-super {p0}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;->score()F

    move-result v0

    return v0
.end method
