.class Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
.super Ljava/lang/Object;
.source "PhraseQuery.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/PhraseQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PostingsAndFreq"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;",
        ">;"
    }
.end annotation


# instance fields
.field final docFreq:I

.field final nTerms:I

.field final position:I

.field final postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field final terms:[Lorg/apache/lucene/index/Term;


# direct methods
.method public varargs constructor <init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;II[Lorg/apache/lucene/index/Term;)V
    .locals 4
    .param p1, "postings"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p2, "docFreq"    # I
    .param p3, "position"    # I
    .param p4, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v2, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 145
    iput p2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    .line 146
    iput p3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    .line 147
    if-nez p4, :cond_0

    move v1, v2

    :goto_0
    iput v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    .line 148
    iget v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-lez v1, :cond_2

    .line 149
    array-length v1, p4

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 150
    iput-object p4, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    .line 160
    :goto_1
    return-void

    .line 147
    :cond_0
    array-length v1, p4

    goto :goto_0

    .line 152
    :cond_1
    array-length v1, p4

    new-array v0, v1, [Lorg/apache/lucene/index/Term;

    .line 153
    .local v0, "terms2":[Lorg/apache/lucene/index/Term;
    array-length v1, p4

    invoke-static {p4, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 155
    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    goto :goto_1

    .line 158
    .end local v0    # "terms2":[Lorg/apache/lucene/index/Term;
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->compareTo(Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;)I
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .prologue
    const/4 v2, 0x0

    .line 164
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    iget v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    if-eq v3, v4, :cond_1

    .line 165
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    iget v3, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    sub-int v1, v2, v3

    .line 180
    :cond_0
    :goto_0
    return v1

    .line 167
    :cond_1
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    iget v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    if-eq v3, v4, :cond_2

    .line 168
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    iget v3, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 170
    :cond_2
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    iget v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-eq v3, v4, :cond_3

    .line 171
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    iget v3, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 173
    :cond_3
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-nez v3, :cond_4

    move v1, v2

    .line 174
    goto :goto_0

    .line 176
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    array-length v3, v3

    if-lt v0, v3, :cond_5

    move v1, v2

    .line 180
    goto :goto_0

    .line 177
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    aget-object v3, v3, v0

    iget-object v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v1

    .line 178
    .local v1, "res":I
    if-nez v1, :cond_0

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    if-ne p0, p1, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v1

    .line 198
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 199
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 200
    check-cast v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .line 201
    .local v0, "other":Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    iget v4, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 202
    :cond_4
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    iget v4, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 203
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_6

    iget-object v3, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 204
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    iget-object v2, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 185
    const/16 v1, 0x1f

    .line 186
    .local v1, "prime":I
    const/4 v2, 0x1

    .line 187
    .local v2, "result":I
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    add-int/lit8 v2, v3, 0x1f

    .line 188
    mul-int/lit8 v3, v2, 0x1f

    iget v4, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    add-int v2, v3, v4

    .line 189
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-lt v0, v3, :cond_0

    .line 192
    return v2

    .line 190
    :cond_0
    mul-int/lit8 v3, v2, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v4

    add-int v2, v3, v4

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
