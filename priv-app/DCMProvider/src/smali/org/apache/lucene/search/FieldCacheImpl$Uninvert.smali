.class abstract Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
.super Ljava/lang/Object;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Uninvert"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public docsWithField:Lorg/apache/lucene/util/Bits;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285
    const-class v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;)V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public uninvert(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    .line 291
    .local v3, "maxDoc":I
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 292
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v5, :cond_3

    .line 293
    if-eqz p3, :cond_1

    .line 294
    invoke-virtual {v5}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v6

    .line 295
    .local v6, "termsDocCount":I
    sget-boolean v8, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    if-le v6, v3, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 296
    :cond_0
    if-ne v6, v3, :cond_1

    .line 298
    new-instance v8, Lorg/apache/lucene/util/Bits$MatchAllBits;

    invoke-direct {v8, v3}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    iput-object v8, p0, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->docsWithField:Lorg/apache/lucene/util/Bits;

    .line 299
    const/4 p3, 0x0

    .line 303
    .end local v6    # "termsDocCount":I
    :cond_1
    invoke-virtual {p0, v5}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v7

    .line 305
    .local v7, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v1, 0x0

    .line 306
    .local v1, "docs":Lorg/apache/lucene/index/DocsEnum;
    const/4 v2, 0x0

    .line 308
    .local v2, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_2
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    .line 309
    .local v4, "term":Lorg/apache/lucene/util/BytesRef;
    if-nez v4, :cond_4

    .line 330
    .end local v1    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v2    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .end local v4    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v7    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_3
    return-void

    .line 312
    .restart local v1    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v2    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "term":Lorg/apache/lucene/util/BytesRef;
    .restart local v7    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_4
    invoke-virtual {p0, v4}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->visitTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 313
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v1, v9}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v1

    .line 315
    :cond_5
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .line 316
    .local v0, "docID":I
    const v8, 0x7fffffff

    if-eq v0, v8, :cond_2

    .line 319
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->visitDoc(I)V

    .line 320
    if-eqz p3, :cond_5

    .line 321
    if-nez v2, :cond_6

    .line 323
    new-instance v2, Lorg/apache/lucene/util/FixedBitSet;

    .end local v2    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    invoke-direct {v2, v3}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .restart local v2    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    iput-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;->docsWithField:Lorg/apache/lucene/util/Bits;

    .line 325
    :cond_6
    invoke-virtual {v2, v0}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0
.end method

.method protected abstract visitDoc(I)V
.end method

.method protected abstract visitTerm(Lorg/apache/lucene/util/BytesRef;)V
.end method
