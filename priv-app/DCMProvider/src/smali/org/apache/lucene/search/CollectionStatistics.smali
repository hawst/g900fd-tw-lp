.class public Lorg/apache/lucene/search/CollectionStatistics;
.super Ljava/lang/Object;
.source "CollectionStatistics.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final docCount:J

.field private final field:Ljava/lang/String;

.field private final maxDoc:J

.field private final sumDocFreq:J

.field private final sumTotalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/search/CollectionStatistics;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/CollectionStatistics;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;JJJJ)V
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "maxDoc"    # J
    .param p4, "docCount"    # J
    .param p6, "sumTotalTermFreq"    # J
    .param p8, "sumDocFreq"    # J

    .prologue
    const-wide/16 v2, -0x1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-boolean v0, Lorg/apache/lucene/search/CollectionStatistics;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/search/CollectionStatistics;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    cmp-long v0, p4, v2

    if-ltz v0, :cond_1

    cmp-long v0, p4, p2

    if-lez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/search/CollectionStatistics;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    cmp-long v0, p8, v2

    if-eqz v0, :cond_3

    cmp-long v0, p8, p4

    if-gez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_3
    sget-boolean v0, Lorg/apache/lucene/search/CollectionStatistics;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    cmp-long v0, p6, v2

    if-eqz v0, :cond_4

    cmp-long v0, p6, p8

    if-gez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_4
    iput-object p1, p0, Lorg/apache/lucene/search/CollectionStatistics;->field:Ljava/lang/String;

    .line 40
    iput-wide p2, p0, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc:J

    .line 41
    iput-wide p4, p0, Lorg/apache/lucene/search/CollectionStatistics;->docCount:J

    .line 42
    iput-wide p6, p0, Lorg/apache/lucene/search/CollectionStatistics;->sumTotalTermFreq:J

    .line 43
    iput-wide p8, p0, Lorg/apache/lucene/search/CollectionStatistics;->sumDocFreq:J

    .line 44
    return-void
.end method


# virtual methods
.method public final docCount()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lorg/apache/lucene/search/CollectionStatistics;->docCount:J

    return-wide v0
.end method

.method public final field()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/search/CollectionStatistics;->field:Ljava/lang/String;

    return-object v0
.end method

.method public final maxDoc()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc:J

    return-wide v0
.end method

.method public final sumDocFreq()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lorg/apache/lucene/search/CollectionStatistics;->sumDocFreq:J

    return-wide v0
.end method

.method public final sumTotalTermFreq()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/apache/lucene/search/CollectionStatistics;->sumTotalTermFreq:J

    return-wide v0
.end method
