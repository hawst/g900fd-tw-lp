.class Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;
.super Lorg/apache/lucene/search/Weight;
.source "PhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/PhraseQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhraseWeight"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field private transient states:[Lorg/apache/lucene/index/TermContext;

.field private final stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

.field final synthetic this$0:Lorg/apache/lucene/search/PhraseQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208
    const-class v0, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/PhraseQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 7
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iput-object p1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    .line 213
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 215
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 216
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    .line 217
    .local v0, "context":Lorg/apache/lucene/index/IndexReaderContext;
    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lorg/apache/lucene/index/TermContext;

    iput-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->states:[Lorg/apache/lucene/index/TermContext;

    .line 218
    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v3, v4, [Lorg/apache/lucene/search/TermStatistics;

    .line 219
    .local v3, "termStats":[Lorg/apache/lucene/search/TermStatistics;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 224
    iget-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {p1}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v5

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/search/PhraseQuery;->access$1(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lorg/apache/lucene/search/IndexSearcher;->collectionStatistics(Ljava/lang/String;)Lorg/apache/lucene/search/CollectionStatistics;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v3}, Lorg/apache/lucene/search/similarities/Similarity;->computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 225
    return-void

    .line 220
    :cond_0
    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/Term;

    .line 221
    .local v2, "term":Lorg/apache/lucene/index/Term;
    iget-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->states:[Lorg/apache/lucene/index/TermContext;

    const/4 v5, 0x1

    invoke-static {v0, v2, v5}, Lorg/apache/lucene/index/TermContext;->build(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/index/TermContext;

    move-result-object v5

    aput-object v5, v4, v1

    .line 222
    iget-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->states:[Lorg/apache/lucene/index/TermContext;

    aget-object v4, v4, v1

    invoke-virtual {p2, v2, v4}, Lorg/apache/lucene/search/IndexSearcher;->termStatistics(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)Lorg/apache/lucene/search/TermStatistics;

    move-result-object v4

    aput-object v4, v3, v1

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private termNotInReader(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/Term;)Z
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 10
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 304
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    invoke-virtual {p0, p1, v9, v8, v6}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 305
    .local v5, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v5, :cond_1

    .line 306
    invoke-virtual {v5, p2}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v2

    .line 307
    .local v2, "newDoc":I
    if-ne v2, p2, :cond_1

    .line 308
    iget-object v6, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v6}, Lorg/apache/lucene/search/PhraseQuery;->access$3(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v6

    int-to-float v1, v6

    .line 309
    .end local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    .local v1, "freq":F
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v7, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v6, v7, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v0

    .line 310
    .local v0, "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 311
    .local v3, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "weight("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], result of:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 312
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "phraseFreq="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v1, v7}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, p2, v6}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    .line 313
    .local v4, "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 314
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 315
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 320
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v1    # "freq":F
    .end local v2    # "newDoc":I
    .end local v3    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v4    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :goto_1
    return-object v3

    .line 308
    .restart local v2    # "newDoc":I
    .restart local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    :cond_0
    check-cast v5, Lorg/apache/lucene/search/SloppyPhraseScorer;

    .end local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;->sloppyFreq()F

    move-result v1

    goto :goto_0

    .line 320
    .end local v2    # "newDoc":I
    .restart local v5    # "scorer":Lorg/apache/lucene/search/Scorer;
    :cond_1
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v6, 0x0

    const-string v7, "no matching term"

    invoke-direct {v3, v8, v6, v7}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->getValueForNormalization()F

    move-result v0

    return v0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->normalize(FF)V

    .line 241
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 16
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    sget-boolean v11, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->$assertionsDisabled:Z

    if-nez v11, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_0

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 247
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    .line 248
    .local v6, "reader":Lorg/apache/lucene/index/AtomicReader;
    move-object/from16 v3, p4

    .line 249
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v5, v11, [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .line 251
    .local v5, "postingsFreqs":[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$1(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 252
    .local v1, "fieldTerms":Lorg/apache/lucene/index/Terms;
    if-nez v1, :cond_2

    .line 253
    const/4 v7, 0x0

    .line 292
    :cond_1
    :goto_0
    return-object v7

    .line 257
    :cond_2
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v10

    .line 259
    .local v10, "te":Lorg/apache/lucene/index/TermsEnum;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v2, v11, :cond_4

    .line 280
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$3(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v11

    if-nez v11, :cond_3

    .line 281
    invoke-static {v5}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;)V

    .line 284
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$3(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v11

    if-nez v11, :cond_9

    .line 285
    new-instance v7, Lorg/apache/lucene/search/ExactPhraseScorer;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-object/from16 v0, p1

    invoke-virtual {v11, v12, v0}, Lorg/apache/lucene/search/similarities/Similarity;->exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v5, v11}, Lorg/apache/lucene/search/ExactPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V

    .line 286
    .local v7, "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    iget-boolean v11, v7, Lorg/apache/lucene/search/ExactPhraseScorer;->noDocs:Z

    if-eqz v11, :cond_1

    .line 287
    const/4 v7, 0x0

    goto :goto_0

    .line 260
    .end local v7    # "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$0(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/Term;

    .line 261
    .local v9, "t":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->states:[Lorg/apache/lucene/index/TermContext;

    aget-object v11, v11, v2

    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v11, v12}, Lorg/apache/lucene/index/TermContext;->get(I)Lorg/apache/lucene/index/TermState;

    move-result-object v8

    .line 262
    .local v8, "state":Lorg/apache/lucene/index/TermState;
    if-nez v8, :cond_6

    .line 263
    sget-boolean v11, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->$assertionsDisabled:Z

    if-nez v11, :cond_5

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v9}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->termNotInReader(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/Term;)Z

    move-result v11

    if-nez v11, :cond_5

    new-instance v11, Ljava/lang/AssertionError;

    const-string v12, "no termstate found but term exists in reader"

    invoke-direct {v11, v12}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v11

    .line 264
    :cond_5
    const/4 v7, 0x0

    goto :goto_0

    .line 266
    :cond_6
    invoke-virtual {v9}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v11

    invoke-virtual {v10, v11, v8}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 267
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v4

    .line 271
    .local v4, "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    if-nez v4, :cond_8

    .line 272
    sget-boolean v11, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->$assertionsDisabled:Z

    if-nez v11, :cond_7

    invoke-virtual {v9}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v11

    if-nez v11, :cond_7

    new-instance v11, Ljava/lang/AssertionError;

    const-string v12, "termstate found but no term exists in reader"

    invoke-direct {v11, v12}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v11

    .line 274
    :cond_7
    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "field \""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\" was indexed without position data; cannot run PhraseQuery (term="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v9}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 276
    :cond_8
    new-instance v12, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    invoke-virtual {v10}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$2(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    const/4 v14, 0x1

    new-array v14, v14, [Lorg/apache/lucene/index/Term;

    const/4 v15, 0x0

    aput-object v9, v14, v15

    invoke-direct {v12, v4, v13, v11, v14}, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;-><init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;II[Lorg/apache/lucene/index/Term;)V

    aput-object v12, v5, v2

    .line 259
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 293
    .end local v4    # "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v8    # "state":Lorg/apache/lucene/index/TermState;
    .end local v9    # "t":Lorg/apache/lucene/index/Term;
    :cond_9
    new-instance v7, Lorg/apache/lucene/search/SloppyPhraseScorer;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v11}, Lorg/apache/lucene/search/PhraseQuery;->access$3(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v5, v11, v12}, Lorg/apache/lucene/search/SloppyPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;ILorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "weight("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
