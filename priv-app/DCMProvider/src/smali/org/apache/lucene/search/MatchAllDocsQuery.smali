.class public Lorg/apache/lucene/search/MatchAllDocsQuery;
.super Lorg/apache/lucene/search/Query;
.source "MatchAllDocsQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;,
        Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 138
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;-><init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 155
    instance-of v2, p1, Lorg/apache/lucene/search/MatchAllDocsQuery;

    if-nez v2, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 157
    check-cast v0, Lorg/apache/lucene/search/MatchAllDocsQuery;

    .line 158
    .local v0, "other":Lorg/apache/lucene/search/MatchAllDocsQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const v1, 0x1aa71190

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v1, "*:*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
