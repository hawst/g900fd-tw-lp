.class Lorg/apache/lucene/search/ConjunctionScorer$1;
.super Ljava/lang/Object;
.source "ConjunctionScorer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/ConjunctionScorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/ConjunctionScorer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/ConjunctionScorer$1;->this$0:Lorg/apache/lucene/search/ConjunctionScorer;

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    check-cast p2, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/ConjunctionScorer$1;->compare(Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;)I
    .locals 4
    .param p1, "o1"    # Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;
    .param p2, "o2"    # Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;

    .prologue
    .line 50
    iget-wide v0, p1, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->cost:J

    iget-wide v2, p2, Lorg/apache/lucene/search/ConjunctionScorer$DocsAndFreqs;->cost:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method
