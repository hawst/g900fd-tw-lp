.class public Lorg/apache/lucene/search/similarities/IBSimilarity;
.super Lorg/apache/lucene/search/similarities/SimilarityBase;
.source "IBSimilarity.java"


# instance fields
.field protected final distribution:Lorg/apache/lucene/search/similarities/Distribution;

.field protected final lambda:Lorg/apache/lucene/search/similarities/Lambda;

.field protected final normalization:Lorg/apache/lucene/search/similarities/Normalization;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/similarities/Distribution;Lorg/apache/lucene/search/similarities/Lambda;Lorg/apache/lucene/search/similarities/Normalization;)V
    .locals 0
    .param p1, "distribution"    # Lorg/apache/lucene/search/similarities/Distribution;
    .param p2, "lambda"    # Lorg/apache/lucene/search/similarities/Lambda;
    .param p3, "normalization"    # Lorg/apache/lucene/search/similarities/Normalization;

    .prologue
    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/SimilarityBase;-><init>()V

    .line 92
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->distribution:Lorg/apache/lucene/search/similarities/Distribution;

    .line 93
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->lambda:Lorg/apache/lucene/search/similarities/Lambda;

    .line 94
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    .line 95
    return-void
.end method


# virtual methods
.method protected explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V
    .locals 5
    .param p1, "expl"    # Lorg/apache/lucene/search/Explanation;
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "doc"    # I
    .param p4, "freq"    # F
    .param p5, "docLen"    # F

    .prologue
    .line 109
    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 110
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v3

    const-string v4, "boost"

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 112
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    invoke-virtual {v2, p2, p4, p5}, Lorg/apache/lucene/search/similarities/Normalization;->explain(Lorg/apache/lucene/search/similarities/BasicStats;FF)Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 113
    .local v1, "normExpl":Lorg/apache/lucene/search/Explanation;
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->lambda:Lorg/apache/lucene/search/similarities/Lambda;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/similarities/Lambda;->explain(Lorg/apache/lucene/search/similarities/BasicStats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 114
    .local v0, "lambdaExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 115
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->distribution:Lorg/apache/lucene/search/similarities/Distribution;

    .line 117
    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    .line 116
    invoke-virtual {v2, p2, v3, v4}, Lorg/apache/lucene/search/similarities/Distribution;->explain(Lorg/apache/lucene/search/similarities/BasicStats;FF)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 118
    return-void
.end method

.method public getDistribution()Lorg/apache/lucene/search/similarities/Distribution;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->distribution:Lorg/apache/lucene/search/similarities/Distribution;

    return-object v0
.end method

.method public getLambda()Lorg/apache/lucene/search/similarities/Lambda;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->lambda:Lorg/apache/lucene/search/similarities/Lambda;

    return-object v0
.end method

.method public getNormalization()Lorg/apache/lucene/search/similarities/Normalization;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    return-object v0
.end method

.method protected score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "freq"    # F
    .param p3, "docLen"    # F

    .prologue
    .line 99
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v0

    .line 100
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->distribution:Lorg/apache/lucene/search/similarities/Distribution;

    .line 102
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    invoke-virtual {v2, p1, p2, p3}, Lorg/apache/lucene/search/similarities/Normalization;->tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v2

    .line 103
    iget-object v3, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->lambda:Lorg/apache/lucene/search/similarities/Lambda;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/similarities/Lambda;->lambda(Lorg/apache/lucene/search/similarities/BasicStats;)F

    move-result v3

    .line 100
    invoke-virtual {v1, p1, v2, v3}, Lorg/apache/lucene/search/similarities/Distribution;->score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v1

    .line 99
    mul-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IB "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->distribution:Lorg/apache/lucene/search/similarities/Distribution;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/Distribution;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->lambda:Lorg/apache/lucene/search/similarities/Lambda;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/Lambda;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/IBSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/Normalization;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
