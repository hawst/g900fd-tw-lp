.class Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;
.super Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
.source "PerFieldSimilarityWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PerFieldSimWeight"
.end annotation


# instance fields
.field delegate:Lorg/apache/lucene/search/similarities/Similarity;

.field delegateWeight:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;-><init>()V

    return-void
.end method


# virtual methods
.method public getValueForNormalization()F
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegateWeight:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->getValueForNormalization()F

    move-result v0

    return v0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper$PerFieldSimWeight;->delegateWeight:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->normalize(FF)V

    .line 85
    return-void
.end method
