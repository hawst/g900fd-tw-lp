.class Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;
.super Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheImpl$LongCache;->createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentValue:J

.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheImpl$LongCache;

.field private final synthetic val$parser:Lorg/apache/lucene/search/FieldCache$LongParser;

.field private final synthetic val$reader:Lorg/apache/lucene/index/AtomicReader;

.field private final synthetic val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

.field private values:[J


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl$LongCache;Lorg/apache/lucene/search/FieldCache$LongParser;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->this$1:Lorg/apache/lucene/search/FieldCacheImpl$LongCache;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$reader:Lorg/apache/lucene/index/AtomicReader;

    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    .line 918
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;)V

    return-void
.end method


# virtual methods
.method protected termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 942
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$LongParser;->termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public visitDoc(I)V
    .locals 4
    .param p1, "docID"    # I

    .prologue
    .line 937
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->values:[J

    iget-wide v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->currentValue:J

    aput-wide v2, v0, p1

    .line 938
    return-void
.end method

.method public visitTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 924
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$LongParser;->parseLong(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->currentValue:J

    .line 925
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->values:[J

    if-nez v0, :cond_0

    .line 930
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$reader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->values:[J

    .line 931
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache$1;->values:[J

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;->set(Ljava/lang/Object;)V

    .line 933
    :cond_0
    return-void
.end method
