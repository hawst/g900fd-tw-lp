.class public final Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "FuzzyTermsEnum.java"

# interfaces
.implements Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FuzzyTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LevenshteinAutomataAttributeImpl"
.end annotation


# instance fields
.field private final automata:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/automaton/CompiledAutomaton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 412
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 413
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    .line 412
    return-void
.end method


# virtual methods
.method public automata()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/automaton/CompiledAutomaton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 417
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 423
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 442
    check-cast p1, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;

    .end local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    invoke-interface {p1}, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttribute;->automata()Ljava/util/List;

    move-result-object v0

    .line 443
    .local v0, "targetAutomata":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/automaton/CompiledAutomaton;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 444
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 445
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 432
    if-ne p0, p1, :cond_0

    .line 433
    const/4 v0, 0x1

    .line 436
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 434
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;

    if-nez v0, :cond_1

    .line 435
    const/4 v0, 0x0

    goto :goto_0

    .line 436
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    check-cast p1, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermsEnum$LevenshteinAutomataAttributeImpl;->automata:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method
