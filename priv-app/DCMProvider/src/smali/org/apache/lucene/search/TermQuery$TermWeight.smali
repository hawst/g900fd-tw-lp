.class final Lorg/apache/lucene/search/TermQuery$TermWeight;
.super Lorg/apache/lucene/search/Weight;
.source "TermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "TermWeight"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field private final stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

.field private final termStates:Lorg/apache/lucene/index/TermContext;

.field final synthetic this$0:Lorg/apache/lucene/search/TermQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/TermQuery;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/index/TermContext;)V
    .locals 6
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "termStates"    # Lorg/apache/lucene/index/TermContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iput-object p1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 52
    sget-boolean v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "TermContext must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 53
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->termStates:Lorg/apache/lucene/index/TermContext;

    .line 54
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 56
    invoke-virtual {p1}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v1

    .line 57
    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {p1}, Lorg/apache/lucene/search/TermQuery;->access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/lucene/search/IndexSearcher;->collectionStatistics(Ljava/lang/String;)Lorg/apache/lucene/search/CollectionStatistics;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lorg/apache/lucene/search/TermStatistics;

    const/4 v4, 0x0

    .line 58
    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {p1}, Lorg/apache/lucene/search/TermQuery;->access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v5

    invoke-virtual {p2, v5, p3}, Lorg/apache/lucene/search/IndexSearcher;->termStatistics(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)Lorg/apache/lucene/search/TermStatistics;

    move-result-object v5

    aput-object v5, v3, v4

    .line 55
    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/search/similarities/Similarity;->computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 59
    return-void
.end method

.method private getTermsEnum(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/index/TermsEnum;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->termStates:Lorg/apache/lucene/index/TermContext;

    iget v3, p1, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/TermContext;->get(I)Lorg/apache/lucene/index/TermState;

    move-result-object v0

    .line 96
    .local v0, "state":Lorg/apache/lucene/index/TermState;
    if-nez v0, :cond_0

    .line 97
    sget-boolean v2, Lorg/apache/lucene/search/TermQuery$TermWeight;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v3}, Lorg/apache/lucene/search/TermQuery;->access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/search/TermQuery$TermWeight;->termNotInReader(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/Term;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "no termstate found but term exists in reader term="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v4}, Lorg/apache/lucene/search/TermQuery;->access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 101
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v3}, Lorg/apache/lucene/search/TermQuery;->access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 102
    .local v1, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v2}, Lorg/apache/lucene/search/TermQuery;->access$0(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 103
    .end local v1    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_1
    return-object v1
.end method

.method private termNotInReader(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/Term;)Z
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 10
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 114
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    invoke-virtual {p0, p1, v9, v8, v6}, Lorg/apache/lucene/search/TermQuery$TermWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 115
    .local v5, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v5, :cond_0

    .line 116
    invoke-virtual {v5, p2}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v2

    .line 117
    .local v2, "newDoc":I
    if-ne v2, p2, :cond_0

    .line 118
    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v6

    int-to-float v1, v6

    .line 119
    .local v1, "freq":F
    iget-object v6, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v7, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v6, v7, p1}, Lorg/apache/lucene/search/similarities/Similarity;->exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    move-result-object v0

    .line 120
    .local v0, "docScorer":Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 121
    .local v3, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "weight("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery$TermWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], result of:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 122
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "termFreq="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v1, v7}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, p2, v6}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    .line 123
    .local v4, "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 124
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 125
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 129
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .end local v1    # "freq":F
    .end local v2    # "newDoc":I
    .end local v3    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v4    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v6, 0x0

    const-string v7, "no matching term"

    invoke-direct {v3, v8, v6, v7}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    goto :goto_0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->getValueForNormalization()F

    move-result v0

    return v0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->normalize(FF)V

    .line 75
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 80
    sget-boolean v3, Lorg/apache/lucene/search/TermQuery$TermWeight;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->termStates:Lorg/apache/lucene/index/TermContext;

    iget-object v3, v3, Lorg/apache/lucene/index/TermContext;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    invoke-static {p1}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v4

    if-eq v3, v4, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The top-reader used to create Weight ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->termStates:Lorg/apache/lucene/index/TermContext;

    iget-object v4, v4, Lorg/apache/lucene/index/TermContext;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") is not the same as the current reader\'s top-reader ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 81
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TermQuery$TermWeight;->getTermsEnum(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 82
    .local v1, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    if-nez v1, :cond_1

    .line 87
    :goto_0
    return-object v2

    .line 85
    :cond_1
    invoke-virtual {v1, p4, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    .line 86
    .local v0, "docs":Lorg/apache/lucene/index/DocsEnum;
    sget-boolean v2, Lorg/apache/lucene/search/TermQuery$TermWeight;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 87
    :cond_2
    new-instance v2, Lorg/apache/lucene/search/TermScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v4, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v3, v4, p1}, Lorg/apache/lucene/search/similarities/Similarity;->exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, Lorg/apache/lucene/search/TermScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "weight("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
