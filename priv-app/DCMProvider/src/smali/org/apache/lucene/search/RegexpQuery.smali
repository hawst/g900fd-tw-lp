.class public Lorg/apache/lucene/search/RegexpQuery;
.super Lorg/apache/lucene/search/AutomatonQuery;
.source "RegexpQuery.java"


# static fields
.field private static defaultProvider:Lorg/apache/lucene/util/automaton/AutomatonProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lorg/apache/lucene/search/RegexpQuery$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/RegexpQuery$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/RegexpQuery;->defaultProvider:Lorg/apache/lucene/util/automaton/AutomatonProvider;

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 69
    const v0, 0xffff

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/RegexpQuery;-><init>(Lorg/apache/lucene/index/Term;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;I)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "flags"    # I

    .prologue
    .line 79
    sget-object v0, Lorg/apache/lucene/search/RegexpQuery;->defaultProvider:Lorg/apache/lucene/util/automaton/AutomatonProvider;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/RegexpQuery;-><init>(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/automaton/AutomatonProvider;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/automaton/AutomatonProvider;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "flags"    # I
    .param p3, "provider"    # Lorg/apache/lucene/util/automaton/AutomatonProvider;

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/util/automaton/RegExp;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lorg/apache/lucene/util/automaton/RegExp;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, p3}, Lorg/apache/lucene/util/automaton/RegExp;->toAutomaton(Lorg/apache/lucene/util/automaton/AutomatonProvider;)Lorg/apache/lucene/util/automaton/Automaton;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/AutomatonQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/automaton/Automaton;)V

    .line 91
    return-void
.end method


# virtual methods
.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x2f

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/RegexpQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/search/RegexpQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    iget-object v1, p0, Lorg/apache/lucene/search/RegexpQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    invoke-virtual {p0}, Lorg/apache/lucene/search/RegexpQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
