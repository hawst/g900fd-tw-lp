.class final Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;
.super Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
.source "ConstantScoreAutoRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CutOffTermCollector"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final array:Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

.field final docCountCutoff:I

.field docVisitCount:I

.field hasCutOff:Z

.field final pendingTerms:Lorg/apache/lucene/util/BytesRefHash;

.field final termCountLimit:I

.field termsEnum:Lorg/apache/lucene/index/TermsEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    const-class v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(II)V
    .locals 4
    .param p1, "docCountCutoff"    # I
    .param p2, "termCountLimit"    # I

    .prologue
    const/16 v3, 0x10

    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;-><init>()V

    .line 149
    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    .line 150
    iput-boolean v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->hasCutOff:Z

    .line 154
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

    invoke-direct {v0, v3}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->array:Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

    .line 155
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash;

    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool;

    new-instance v2, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;

    invoke-direct {v2}, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;-><init>()V

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->array:Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Lorg/apache/lucene/util/BytesRefHash;

    .line 120
    iput p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docCountCutoff:I

    .line 121
    iput p2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termCountLimit:I

    .line 122
    return-void
.end method


# virtual methods
.method public collect(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 12
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;)I

    move-result v10

    .line 132
    .local v10, "pos":I
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v0

    iget v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termCountLimit:I

    if-ge v0, v2, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    iget v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docCountCutoff:I

    if-lt v0, v2, :cond_1

    .line 134
    :cond_0
    iput-boolean v11, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->hasCutOff:Z

    .line 135
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 138
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v1

    .line 139
    .local v1, "termState":Lorg/apache/lucene/index/TermState;
    sget-boolean v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 140
    :cond_2
    if-gez v10, :cond_3

    .line 141
    neg-int v0, v10

    add-int/lit8 v10, v0, -0x1

    .line 142
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->array:Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

    iget-object v0, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    aget-object v0, v0, v10

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v2, v2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    :goto_1
    move v0, v11

    .line 146
    goto :goto_0

    .line 144
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->array:Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;

    iget-object v0, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    new-instance v3, Lorg/apache/lucene/index/TermContext;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v6, v2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v7

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v8

    move-object v5, v1

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/TermState;IIJ)V

    aput-object v3, v0, v10

    goto :goto_1
.end method

.method public setNextEnum(Lorg/apache/lucene/index/TermsEnum;)V
    .locals 0
    .param p1, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 126
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 127
    return-void
.end method
