.class public Lorg/apache/lucene/search/payloads/PayloadNearQuery;
.super Lorg/apache/lucene/search/spans/SpanNearQuery;
.source "PayloadNearQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;,
        Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;
    }
.end annotation


# instance fields
.field protected fieldName:Ljava/lang/String;

.field protected function:Lorg/apache/lucene/search/payloads/PayloadFunction;


# direct methods
.method public constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V
    .locals 1
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "slop"    # I
    .param p3, "inOrder"    # Z

    .prologue
    .line 63
    new-instance v0, Lorg/apache/lucene/search/payloads/AveragePayloadFunction;

    invoke-direct {v0}, Lorg/apache/lucene/search/payloads/AveragePayloadFunction;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZLorg/apache/lucene/search/payloads/PayloadFunction;)V

    .line 64
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;IZLorg/apache/lucene/search/payloads/PayloadFunction;)V
    .locals 1
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "slop"    # I
    .param p3, "inOrder"    # Z
    .param p4, "function"    # Lorg/apache/lucene/search/payloads/PayloadFunction;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 69
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    .line 71
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/search/payloads/PayloadNearQuery;
    .locals 7

    .prologue
    .line 80
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 81
    .local v3, "sz":I
    new-array v2, v3, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 83
    .local v2, "newClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_0

    .line 86
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->slop:I

    .line 87
    iget-boolean v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->inOrder:Z

    iget-object v6, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    .line 86
    invoke-direct {v0, v2, v4, v5, v6}, Lorg/apache/lucene/search/payloads/PayloadNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZLorg/apache/lucene/search/payloads/PayloadFunction;)V

    .line 88
    .local v0, "boostingNearQuery":Lorg/apache/lucene/search/payloads/PayloadNearQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->getBoost()F

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->setBoost(F)V

    .line 89
    return-object v0

    .line 84
    .end local v0    # "boostingNearQuery":Lorg/apache/lucene/search/payloads/PayloadNearQuery;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    aput-object v4, v2, v1

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/spans/SpanNearQuery;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->clone()Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    move-result-object v0

    return-object v0
.end method

.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;

    invoke-direct {v0, p0, p0, p1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;-><init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    if-ne p0, p1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v1

    .line 126
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 127
    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 129
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 130
    check-cast v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    .line 131
    .local v0, "other":Lorg/apache/lucene/search/payloads/PayloadNearQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 132
    iget-object v3, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 133
    goto :goto_0

    .line 134
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 135
    goto :goto_0

    .line 136
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    if-nez v3, :cond_6

    .line 137
    iget-object v3, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    if-eqz v3, :cond_0

    move v1, v2

    .line 138
    goto :goto_0

    .line 139
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget-object v4, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/payloads/PayloadFunction;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 140
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 115
    const/16 v0, 0x1f

    .line 116
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->hashCode()I

    move-result v1

    .line 117
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 118
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 119
    return v1

    .line 117
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 118
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    invoke-virtual {v3}, Lorg/apache/lucene/search/payloads/PayloadFunction;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v3, "payloadNear(["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 97
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 104
    const-string v3, "], "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->slop:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    iget-boolean v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->inOrder:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 108
    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 98
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 99
    .local v1, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
