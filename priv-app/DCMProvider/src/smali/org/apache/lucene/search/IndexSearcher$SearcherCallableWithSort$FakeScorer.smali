.class final Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "IndexSearcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FakeScorer"
.end annotation


# instance fields
.field doc:I

.field score:F

.field final synthetic this$1:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;)V
    .locals 1

    .prologue
    .line 774
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;->this$1:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;

    .line 775
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 776
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 780
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FakeScorer doesn\'t support advance(int)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 805
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 785
    iget v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 2

    .prologue
    .line 790
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FakeScorer doesn\'t support freq()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 795
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FakeScorer doesn\'t support nextDoc()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public score()F
    .locals 1

    .prologue
    .line 800
    iget v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;->score:F

    return v0
.end method
