.class public Lorg/apache/lucene/search/spans/SpanWeight;
.super Lorg/apache/lucene/search/Weight;
.source "SpanWeight.java"


# instance fields
.field protected query:Lorg/apache/lucene/search/spans/SpanQuery;

.field protected similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field protected stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

.field protected termContexts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 10
    .param p1, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 45
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 48
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/search/spans/SpanWeight;->termContexts:Ljava/util/Map;

    .line 49
    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    .line 50
    .local v6, "terms":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p1, v6}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    .line 51
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    .line 52
    .local v0, "context":Lorg/apache/lucene/index/IndexReaderContext;
    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v7

    new-array v5, v7, [Lorg/apache/lucene/search/TermStatistics;

    .line 53
    .local v5, "termStats":[Lorg/apache/lucene/search/TermStatistics;
    const/4 v2, 0x0

    .line 54
    .local v2, "i":I
    invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 60
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "field":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 62
    iget-object v7, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getBoost()F

    move-result v8

    .line 63
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Lorg/apache/lucene/search/IndexSearcher;->collectionStatistics(Ljava/lang/String;)Lorg/apache/lucene/search/CollectionStatistics;

    move-result-object v9

    .line 62
    invoke-virtual {v7, v8, v9, v5}, Lorg/apache/lucene/search/similarities/Similarity;->computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 66
    :cond_0
    return-void

    .line 54
    .end local v1    # "field":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/Term;

    .line 55
    .local v4, "term":Lorg/apache/lucene/index/Term;
    const/4 v8, 0x1

    invoke-static {v0, v4, v8}, Lorg/apache/lucene/index/TermContext;->build(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/index/TermContext;

    move-result-object v3

    .line 56
    .local v3, "state":Lorg/apache/lucene/index/TermContext;
    invoke-virtual {p2, v4, v3}, Lorg/apache/lucene/search/IndexSearcher;->termStatistics(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)Lorg/apache/lucene/search/TermStatistics;

    move-result-object v8

    aput-object v8, v5, v2

    .line 57
    iget-object v8, p0, Lorg/apache/lucene/search/spans/SpanWeight;->termContexts:Ljava/util/Map;

    invoke-interface {v8, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 10
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 95
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    invoke-virtual {p0, p1, v9, v8, v6}, Lorg/apache/lucene/search/spans/SpanWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/spans/SpanScorer;

    .line 96
    .local v5, "scorer":Lorg/apache/lucene/search/spans/SpanScorer;
    if-eqz v5, :cond_0

    .line 97
    invoke-virtual {v5, p2}, Lorg/apache/lucene/search/spans/SpanScorer;->advance(I)I

    move-result v2

    .line 98
    .local v2, "newDoc":I
    if-ne v2, p2, :cond_0

    .line 99
    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/SpanScorer;->sloppyFreq()F

    move-result v1

    .line 100
    .local v1, "freq":F
    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v7, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v6, v7, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v0

    .line 101
    .local v0, "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 102
    .local v3, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "weight("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], result of:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 103
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "phraseFreq="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v1, v7}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, p2, v6}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    .line 104
    .local v4, "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 105
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 106
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 111
    .end local v0    # "docScorer":Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .end local v1    # "freq":F
    .end local v2    # "newDoc":I
    .end local v3    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    .end local v4    # "scoreExplanation":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v6, 0x0

    const-string v7, "no matching term"

    invoke-direct {v3, v8, v6, v7}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    goto :goto_0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->getValueForNormalization()F

    move-result v0

    goto :goto_0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;->normalize(FF)V

    .line 81
    :cond_0
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/spans/SpanScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanWeight;->termContexts:Ljava/util/Map;

    invoke-virtual {v1, p1, p4, v2}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanWeight;->stats:Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    invoke-virtual {v2, v3, p1}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lorg/apache/lucene/search/spans/SpanScorer;-><init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    goto :goto_0
.end method
