.class final Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;
.super Lorg/apache/lucene/search/FieldValueHitQueue;
.source "FieldValueHitQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldValueHitQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OneComparatorFieldValueHitQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
        ">",
        "Lorg/apache/lucene/search/FieldValueHitQueue",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final oneReverseMul:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lorg/apache/lucene/search/FieldValueHitQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/search/SortField;I)V
    .locals 4
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>.OneComparatorFieldValueHitQueue<TT;>;"
    const/4 v3, 0x0

    .line 63
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/search/FieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;ILorg/apache/lucene/search/FieldValueHitQueue;)V

    .line 65
    aget-object v0, p1, v3

    .line 66
    .local v0, "field":Lorg/apache/lucene/search/SortField;
    invoke-virtual {v0, p2, v3}, Lorg/apache/lucene/search/SortField;->getComparator(II)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->setComparator(ILorg/apache/lucene/search/FieldComparator;)V

    .line 67
    iget-boolean v1, v0, Lorg/apache/lucene/search/SortField;->reverse:Z

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    :goto_0
    iput v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->oneReverseMul:I

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->reverseMul:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->oneReverseMul:I

    aput v2, v1, v3

    .line 70
    return-void

    .line 67
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected lessThan(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Z
    .locals 7

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>.OneComparatorFieldValueHitQueue<TT;>;"
    .local p1, "hitA":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;, "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;"
    .local p2, "hitB":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;, "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    sget-boolean v3, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-ne p1, p2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 82
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget v3, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    iget v4, p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    if-ne v3, v4, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 84
    :cond_1
    iget v3, p0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->oneReverseMul:I

    iget-object v4, p0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;->firstComparator:Lorg/apache/lucene/search/FieldComparator;

    iget v5, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    iget v6, p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/search/FieldComparator;->compare(II)I

    move-result v4

    mul-int v0, v3, v4

    .line 85
    .local v0, "c":I
    if-eqz v0, :cond_4

    .line 86
    if-lez v0, :cond_3

    .line 90
    :cond_2
    :goto_0
    return v1

    :cond_3
    move v1, v2

    .line 86
    goto :goto_0

    .line 90
    :cond_4
    iget v3, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    iget v4, p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-gt v3, v4, :cond_2

    move v1, v2

    goto :goto_0
.end method
