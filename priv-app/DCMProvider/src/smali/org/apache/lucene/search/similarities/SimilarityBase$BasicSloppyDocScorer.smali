.class Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
.source "SimilarityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/SimilarityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BasicSloppyDocScorer"
.end annotation


# instance fields
.field private final norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final stats:Lorg/apache/lucene/search/similarities/BasicStats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/similarities/SimilarityBase;Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 0
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "norms"    # Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;-><init>()V

    .line 320
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->stats:Lorg/apache/lucene/search/similarities/BasicStats;

    .line 321
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 322
    return-void
.end method


# virtual methods
.method public computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 343
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public computeSlopFactor(I)F
    .locals 2
    .param p1, "distance"    # I

    .prologue
    .line 338
    const/high16 v0, 0x3f800000    # 1.0f

    add-int/lit8 v1, p1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 332
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->stats:Lorg/apache/lucene/search/similarities/BasicStats;

    .line 333
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 332
    :goto_0
    invoke-virtual {v1, v2, p1, p2, v0}, Lorg/apache/lucene/search/similarities/SimilarityBase;->explain(Lorg/apache/lucene/search/similarities/BasicStats;ILorg/apache/lucene/search/Explanation;F)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0

    .line 333
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v3, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v4

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->decodeNormValue(B)F

    move-result v0

    goto :goto_0
.end method

.method public score(IF)F
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # F

    .prologue
    .line 327
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->stats:Lorg/apache/lucene/search/similarities/BasicStats;

    .line 328
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 327
    :goto_0
    invoke-virtual {v1, v2, p2, v0}, Lorg/apache/lucene/search/similarities/SimilarityBase;->score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v0

    return v0

    .line 328
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v3, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicSloppyDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v4

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->decodeNormValue(B)F

    move-result v0

    goto :goto_0
.end method
