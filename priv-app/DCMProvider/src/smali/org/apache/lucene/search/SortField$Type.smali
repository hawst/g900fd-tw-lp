.class public final enum Lorg/apache/lucene/search/SortField$Type;
.super Ljava/lang/Enum;
.source "SortField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/SortField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/search/SortField$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BYTE:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum BYTES:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum CUSTOM:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum DOC:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum DOUBLE:Lorg/apache/lucene/search/SortField$Type;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/search/SortField$Type;

.field public static final enum FLOAT:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum INT:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum LONG:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum REWRITEABLE:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum SCORE:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum SHORT:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum STRING:Lorg/apache/lucene/search/SortField$Type;

.field public static final enum STRING_VAL:Lorg/apache/lucene/search/SortField$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "SCORE"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 48
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->SCORE:Lorg/apache/lucene/search/SortField$Type;

    .line 50
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "DOC"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 52
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->DOC:Lorg/apache/lucene/search/SortField$Type;

    .line 54
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 56
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->STRING:Lorg/apache/lucene/search/SortField$Type;

    .line 58
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "INT"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 60
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    .line 62
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 64
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->FLOAT:Lorg/apache/lucene/search/SortField$Type;

    .line 66
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "LONG"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 68
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->LONG:Lorg/apache/lucene/search/SortField$Type;

    .line 70
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "DOUBLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 72
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->DOUBLE:Lorg/apache/lucene/search/SortField$Type;

    .line 74
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "SHORT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 76
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->SHORT:Lorg/apache/lucene/search/SortField$Type;

    .line 78
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "CUSTOM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 80
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->CUSTOM:Lorg/apache/lucene/search/SortField$Type;

    .line 82
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "BYTE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 84
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->BYTE:Lorg/apache/lucene/search/SortField$Type;

    .line 86
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "STRING_VAL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 90
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->STRING_VAL:Lorg/apache/lucene/search/SortField$Type;

    .line 92
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "BYTES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 93
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->BYTES:Lorg/apache/lucene/search/SortField$Type;

    .line 95
    new-instance v0, Lorg/apache/lucene/search/SortField$Type;

    const-string v1, "REWRITEABLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField$Type;-><init>(Ljava/lang/String;I)V

    .line 97
    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->REWRITEABLE:Lorg/apache/lucene/search/SortField$Type;

    .line 44
    const/16 v0, 0xd

    new-array v0, v0, [Lorg/apache/lucene/search/SortField$Type;

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->SCORE:Lorg/apache/lucene/search/SortField$Type;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->DOC:Lorg/apache/lucene/search/SortField$Type;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->STRING:Lorg/apache/lucene/search/SortField$Type;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->FLOAT:Lorg/apache/lucene/search/SortField$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->LONG:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->DOUBLE:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->SHORT:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->CUSTOM:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->BYTE:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->STRING_VAL:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->BYTES:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->REWRITEABLE:Lorg/apache/lucene/search/SortField$Type;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/search/SortField$Type;->ENUM$VALUES:[Lorg/apache/lucene/search/SortField$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/search/SortField$Type;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/search/SortField$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/SortField$Type;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/search/SortField$Type;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/search/SortField$Type;->ENUM$VALUES:[Lorg/apache/lucene/search/SortField$Type;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/search/SortField$Type;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
