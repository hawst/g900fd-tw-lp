.class Lorg/apache/lucene/search/FieldCacheDocIdSet$5;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "FieldCacheDocIdSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheDocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private doc:I

.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    .line 131
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 152
    iput p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, v1, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    if-lt v0, v1, :cond_0

    .line 157
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    :goto_1
    return v0

    .line 153
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    goto :goto_1

    .line 152
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v0, v0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 142
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    .line 143
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, v1, Lorg/apache/lucene/search/FieldCacheDocIdSet;->maxDoc:I

    if-lt v0, v1, :cond_1

    .line 144
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    .line 147
    :goto_0
    return v0

    .line 146
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->acceptDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$5;->doc:I

    goto :goto_0
.end method
