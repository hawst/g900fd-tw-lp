.class public Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;
.super Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConstantScoreAutoRewrite"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getDocCountPercent()D
    .locals 2

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getDocCountPercent()D

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getTermCountCutoff()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTermCountCutoff()I

    move-result v0

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->hashCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setDocCountPercent(D)V
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->setDocCountPercent(D)V

    return-void
.end method

.method public bridge synthetic setTermCountCutoff(I)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->setTermCountCutoff(I)V

    return-void
.end method
