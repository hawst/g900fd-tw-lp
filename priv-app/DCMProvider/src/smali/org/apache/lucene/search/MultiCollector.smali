.class public Lorg/apache/lucene/search/MultiCollector;
.super Lorg/apache/lucene/search/Collector;
.source "MultiCollector.java"


# instance fields
.field private final collectors:[Lorg/apache/lucene/search/Collector;


# direct methods
.method private varargs constructor <init>([Lorg/apache/lucene/search/Collector;)V
    .locals 0
    .param p1, "collectors"    # [Lorg/apache/lucene/search/Collector;

    .prologue
    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 90
    iput-object p1, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    .line 91
    return-void
.end method

.method public static varargs wrap([Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;
    .locals 8
    .param p0, "collectors"    # [Lorg/apache/lucene/search/Collector;

    .prologue
    const/4 v5, 0x0

    .line 54
    const/4 v3, 0x0

    .line 55
    .local v3, "n":I
    array-length v7, p0

    move v6, v5

    :goto_0
    if-lt v6, v7, :cond_0

    .line 61
    if-nez v3, :cond_2

    .line 62
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "At least 1 collector must not be null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 55
    :cond_0
    aget-object v0, p0, v6

    .line 56
    .local v0, "c":Lorg/apache/lucene/search/Collector;
    if-eqz v0, :cond_1

    .line 57
    add-int/lit8 v3, v3, 0x1

    .line 55
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 63
    .end local v0    # "c":Lorg/apache/lucene/search/Collector;
    :cond_2
    const/4 v6, 0x1

    if-ne v3, v6, :cond_5

    .line 65
    const/4 v1, 0x0

    .line 66
    .local v1, "col":Lorg/apache/lucene/search/Collector;
    array-length v6, p0

    :goto_1
    if-lt v5, v6, :cond_3

    .line 83
    .end local v1    # "col":Lorg/apache/lucene/search/Collector;
    :goto_2
    return-object v1

    .line 66
    .restart local v1    # "col":Lorg/apache/lucene/search/Collector;
    :cond_3
    aget-object v0, p0, v5

    .line 67
    .restart local v0    # "c":Lorg/apache/lucene/search/Collector;
    if-eqz v0, :cond_4

    .line 68
    move-object v1, v0

    .line 69
    goto :goto_2

    .line 66
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 73
    .end local v0    # "c":Lorg/apache/lucene/search/Collector;
    .end local v1    # "col":Lorg/apache/lucene/search/Collector;
    :cond_5
    array-length v6, p0

    if-ne v3, v6, :cond_6

    .line 74
    new-instance v1, Lorg/apache/lucene/search/MultiCollector;

    invoke-direct {v1, p0}, Lorg/apache/lucene/search/MultiCollector;-><init>([Lorg/apache/lucene/search/Collector;)V

    goto :goto_2

    .line 76
    :cond_6
    new-array v2, v3, [Lorg/apache/lucene/search/Collector;

    .line 77
    .local v2, "colls":[Lorg/apache/lucene/search/Collector;
    const/4 v3, 0x0

    .line 78
    array-length v6, p0

    move v4, v3

    .end local v3    # "n":I
    .local v4, "n":I
    :goto_3
    if-lt v5, v6, :cond_7

    .line 83
    new-instance v1, Lorg/apache/lucene/search/MultiCollector;

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/MultiCollector;-><init>([Lorg/apache/lucene/search/Collector;)V

    move v3, v4

    .end local v4    # "n":I
    .restart local v3    # "n":I
    goto :goto_2

    .line 78
    .end local v3    # "n":I
    .restart local v4    # "n":I
    :cond_7
    aget-object v0, p0, v5

    .line 79
    .restart local v0    # "c":Lorg/apache/lucene/search/Collector;
    if-eqz v0, :cond_8

    .line 80
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "n":I
    .restart local v3    # "n":I
    aput-object v0, v2, v4

    .line 78
    :goto_4
    add-int/lit8 v5, v5, 0x1

    move v4, v3

    .end local v3    # "n":I
    .restart local v4    # "n":I
    goto :goto_3

    :cond_8
    move v3, v4

    .end local v4    # "n":I
    .restart local v3    # "n":I
    goto :goto_4
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v3, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_1

    .line 100
    const/4 v1, 0x1

    :cond_0
    return v1

    .line 95
    :cond_1
    aget-object v0, v3, v2

    .line 96
    .local v0, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public collect(I)V
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v2, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 108
    return-void

    .line 105
    :cond_0
    aget-object v0, v2, v1

    .line 106
    .local v0, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v2, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 115
    return-void

    .line 112
    :cond_0
    aget-object v0, v2, v1

    .line 113
    .local v0, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 4
    .param p1, "s"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v2, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 122
    return-void

    .line 119
    :cond_0
    aget-object v0, v2, v1

    .line 120
    .local v0, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
