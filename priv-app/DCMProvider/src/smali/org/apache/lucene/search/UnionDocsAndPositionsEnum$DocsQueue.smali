.class final Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/UnionDocsAndPositionsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DocsQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/index/DocsAndPositionsEnum;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/DocsAndPositionsEnum;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "docsEnums":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/DocsAndPositionsEnum;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 418
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 419
    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/DocsAndPositionsEnum;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 425
    return-void

    .line 420
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 421
    .local v1, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v2

    const v3, 0x7fffffff

    if-eq v2, v3, :cond_0

    .line 422
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    check-cast p2, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/UnionDocsAndPositionsEnum$DocsQueue;->lessThan(Lorg/apache/lucene/index/DocsAndPositionsEnum;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Z

    move-result v0

    return v0
.end method

.method public final lessThan(Lorg/apache/lucene/index/DocsAndPositionsEnum;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Z
    .locals 2
    .param p1, "a"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p2, "b"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .prologue
    .line 429
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
