.class public Lorg/apache/lucene/search/similarities/BasicModelIF;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelIF.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 12
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 33
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v2

    .line 34
    .local v2, "N":J
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v0

    .line 35
    .local v0, "F":J
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x1

    add-long/2addr v6, v2

    long-to-double v6, v6

    long-to-double v8, v0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    add-double/2addr v8, v10

    div-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v4, p2

    return v4
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "I(F)"

    return-object v0
.end method
