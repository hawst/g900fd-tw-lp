.class Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;
.super Lorg/apache/lucene/search/TopScoreDocCollector;
.source "TopScoreDocCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopScoreDocCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InOrderTopScoreDocCollector"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/search/TopScoreDocCollector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(I)V
    .locals 1
    .param p1, "numHits"    # I

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/TopScoreDocCollector;-><init>(ILorg/apache/lucene/search/TopScoreDocCollector;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(ILorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;-><init>(I)V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public collect(I)V
    .locals 3
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 50
    .local v0, "score":F
    sget-boolean v1, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 51
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 53
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->totalHits:I

    .line 54
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_2

    .line 63
    :goto_0
    return-void

    .line 60
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v2, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->docBase:I

    add-int/2addr v2, p1

    iput v2, v1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iput v0, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 62
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    iput-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    goto :goto_0
.end method
