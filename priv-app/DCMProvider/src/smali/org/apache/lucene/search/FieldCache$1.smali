.class Lorg/apache/lucene/search/FieldCache$1;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "FieldCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 140
    const/4 v0, -0x1

    return v0
.end method

.method public getValueCount()I
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "ord"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v1, 0x0

    .line 145
    sget-object v0, Lorg/apache/lucene/search/FieldCache$1;->MISSING:[B

    iput-object v0, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 146
    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 147
    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 148
    return-void
.end method
