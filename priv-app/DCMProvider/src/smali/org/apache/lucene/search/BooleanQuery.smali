.class public Lorg/apache/lucene/search/BooleanQuery;
.super Lorg/apache/lucene/search/Query;
.source "BooleanQuery.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;,
        Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/Query;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/search/BooleanClause;",
        ">;"
    }
.end annotation


# static fields
.field private static maxClauseCount:I


# instance fields
.field private clauses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;"
        }
    .end annotation
.end field

.field private final disableCoord:Z

.field protected minNrShouldMatch:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x400

    sput v0, Lorg/apache/lucene/search/BooleanQuery;->maxClauseCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    .line 117
    iput v1, p0, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    .line 75
    iput-boolean v1, p0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    .line 76
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "disableCoord"    # Z

    .prologue
    .line 87
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    .line 88
    iput-boolean p1, p0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    .line 89
    return-void
.end method

.method static synthetic access$1()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lorg/apache/lucene/search/BooleanQuery;->maxClauseCount:I

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getMaxClauseCount()I
    .locals 1

    .prologue
    .line 58
    sget v0, Lorg/apache/lucene/search/BooleanQuery;->maxClauseCount:I

    return v0
.end method

.method public static setMaxClauseCount(I)V
    .locals 2
    .param p0, "maxClauseCount"    # I

    .prologue
    .line 65
    const/4 v0, 0x1

    if-ge p0, v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxClauseCount must be >= 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    sput p0, Lorg/apache/lucene/search/BooleanQuery;->maxClauseCount:I

    .line 68
    return-void
.end method


# virtual methods
.method public add(Lorg/apache/lucene/search/BooleanClause;)V
    .locals 2
    .param p1, "clause"    # Lorg/apache/lucene/search/BooleanClause;

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, Lorg/apache/lucene/search/BooleanQuery;->maxClauseCount:I

    if-lt v0, v1, :cond_0

    .line 142
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;-><init>()V

    throw v0

    .line 144
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    return-void
.end method

.method public add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 133
    new-instance v0, Lorg/apache/lucene/search/BooleanClause;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/BooleanClause;)V

    .line 134
    return-void
.end method

.method public clauses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/search/BooleanQuery;
    .locals 2

    .prologue
    .line 441
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanQuery;

    .line 442
    .local v0, "clone":Lorg/apache/lucene/search/BooleanQuery;
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, v0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    .line 443
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->clone()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method

.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 2
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 384
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;

    iget-boolean v1, p0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;-><init>(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/IndexSearcher;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 499
    instance-of v2, p1, Lorg/apache/lucene/search/BooleanQuery;

    if-nez v2, :cond_1

    .line 502
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 501
    check-cast v0, Lorg/apache/lucene/search/BooleanQuery;

    .line 502
    .local v0, "other":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 503
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 504
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 505
    iget-boolean v2, p0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    iget-boolean v3, v0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    if-ne v2, v3, :cond_0

    .line 502
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 432
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 437
    return-void

    .line 432
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 433
    .local v0, "clause":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-eq v2, v3, :cond_0

    .line 434
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    goto :goto_0
.end method

.method public getClauses()[Lorg/apache/lucene/search/BooleanClause;
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/BooleanClause;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/BooleanClause;

    return-object v0
.end method

.method public getMinimumNumberShouldMatch()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 511
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    .line 512
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v2

    add-int/2addr v2, v0

    iget-boolean v0, p0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    :goto_0
    add-int/2addr v0, v2

    .line 511
    xor-int/2addr v0, v1

    return v0

    .line 512
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCoordDisabled()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lorg/apache/lucene/search/BooleanQuery;->disableCoord:Z

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->clauses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 389
    iget v4, p0, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 390
    iget-object v4, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 391
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v4

    if-nez v4, :cond_2

    .line 393
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v4

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 395
    .local v3, "query":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 396
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 397
    invoke-virtual {v3}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 402
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v4

    invoke-virtual {v3}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 426
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v3    # "query":Lorg/apache/lucene/search/Query;
    :cond_1
    :goto_0
    return-object v3

    .line 409
    :cond_2
    const/4 v1, 0x0

    .line 410
    .local v1, "clone":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_3

    .line 423
    if-eqz v1, :cond_6

    move-object v3, v1

    .line 424
    goto :goto_0

    .line 411
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 412
    .restart local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v4

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 413
    .restart local v3    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v4

    if-eq v3, v4, :cond_5

    .line 414
    if-nez v1, :cond_4

    .line 418
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->clone()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v1

    .line 420
    :cond_4
    iget-object v4, v1, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    new-instance v5, Lorg/apache/lucene/search/BooleanClause;

    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v6

    invoke-direct {v5, v3, v6}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-virtual {v4, v2, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 410
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v3    # "query":Lorg/apache/lucene/search/Query;
    :cond_6
    move-object v3, p0

    .line 426
    goto :goto_0
.end method

.method public setMinimumNumberShouldMatch(I)V
    .locals 0
    .param p1, "min"    # I

    .prologue
    .line 115
    iput p1, p0, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    .line 116
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 450
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v5

    float-to-double v6, v5

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v5

    if-gtz v5, :cond_4

    const/4 v3, 0x0

    .line 451
    .local v3, "needParens":Z
    :goto_0
    if-eqz v3, :cond_0

    .line 452
    const-string v5, "("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_5

    .line 479
    if-eqz v3, :cond_1

    .line 480
    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v5

    if-lez v5, :cond_2

    .line 484
    const/16 v5, 0x7e

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 485
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 488
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_3

    .line 490
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v5

    invoke-static {v5}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 450
    .end local v2    # "i":I
    .end local v3    # "needParens":Z
    :cond_4
    const/4 v3, 0x1

    goto :goto_0

    .line 456
    .restart local v2    # "i":I
    .restart local v3    # "needParens":Z
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/BooleanClause;

    .line 457
    .local v1, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 458
    const-string v5, "-"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    :cond_6
    :goto_2
    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 463
    .local v4, "subQuery":Lorg/apache/lucene/search/Query;
    if-eqz v4, :cond_a

    .line 464
    instance-of v5, v4, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v5, :cond_9

    .line 465
    const-string v5, "("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    :goto_3
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-eq v2, v5, :cond_7

    .line 476
    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 459
    .end local v4    # "subQuery":Lorg/apache/lucene/search/Query;
    :cond_8
    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 460
    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 469
    .restart local v4    # "subQuery":Lorg/apache/lucene/search/Query;
    :cond_9
    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 472
    :cond_a
    const-string v5, "null"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method
