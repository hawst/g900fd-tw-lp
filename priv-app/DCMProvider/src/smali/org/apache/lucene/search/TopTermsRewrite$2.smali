.class Lorg/apache/lucene/search/TopTermsRewrite$2;
.super Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
.source "TopTermsRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/TopTermsRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;"
    }
.end annotation


# instance fields
.field private boostAtt:Lorg/apache/lucene/search/BoostAttribute;

.field private lastTerm:Lorg/apache/lucene/util/BytesRef;

.field private final maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

.field private st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

.field private termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private termsEnum:Lorg/apache/lucene/index/TermsEnum;

.field final synthetic this$0:Lorg/apache/lucene/search/TopTermsRewrite;

.field private final synthetic val$maxSize:I

.field private final synthetic val$stQueue:Ljava/util/PriorityQueue;

.field private final visitedTerms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/TopTermsRewrite;Ljava/util/PriorityQueue;I)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->this$0:Lorg/apache/lucene/search/TopTermsRewrite;

    iput-object p2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    iput p3, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$maxSize:I

    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;-><init>()V

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->attributes:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->visitedTerms:Ljava/util/Map;

    return-void
.end method

.method private compareToLastTerm(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 94
    invoke-static {p1}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    .line 101
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 95
    :cond_0
    if-nez p1, :cond_1

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 98
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v1, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lastTerm="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 99
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method


# virtual methods
.method public collect(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 9
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 106
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/search/BoostAttribute;->getBoost()F

    move-result v6

    .line 110
    .local v6, "boost":F
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopTermsRewrite$2;->compareToLastTerm(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$maxSize:I

    if-ne v0, v2, :cond_3

    .line 115
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 116
    .local v7, "t":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    iget v0, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    cmpg-float v0, v6, v0

    if-gez v0, :cond_2

    .line 153
    :cond_1
    :goto_0
    return v8

    .line 118
    :cond_2
    iget v0, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    cmpl-float v0, v6, v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termComp:Ljava/util/Comparator;

    iget-object v2, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, p1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_1

    .line 121
    .end local v7    # "t":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->visitedTerms:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 122
    .restart local v7    # "t":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v1

    .line 123
    .local v1, "state":Lorg/apache/lucene/index/TermState;
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 124
    :cond_4
    if-eqz v7, :cond_6

    .line 126
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget v0, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "boost should be equal in all segment TermsEnums"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 127
    :cond_5
    iget-object v0, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v2, v2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    iget-object v3, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    goto :goto_0

    .line 130
    :cond_6
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v0, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iput v6, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->visitedTerms:Ljava/util/Map;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v2, v2, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v0, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 134
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v0, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v2, v2, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    iget-object v3, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    .line 135
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    invoke-virtual {v0, v2}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$maxSize:I

    if-le v0, v2, :cond_8

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->visitedTerms:Ljava/util/Map;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v2, v2, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v0, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermContext;->clear()V

    .line 144
    :goto_1
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$maxSize:I

    if-le v0, v2, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "the PQ size must be limited to maxSize"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 142
    :cond_8
    new-instance v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termComp:Ljava/util/Comparator;

    new-instance v3, Lorg/apache/lucene/index/TermContext;

    iget-object v4, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;-><init>(Ljava/util/Comparator;Lorg/apache/lucene/index/TermContext;)V

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    goto :goto_1

    .line 146
    :cond_9
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$maxSize:I

    if-ne v0, v2, :cond_1

    .line 147
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "t":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    check-cast v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 148
    .restart local v7    # "t":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    iget v2, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    invoke-interface {v0, v2}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->setMaxNonCompetitiveBoost(F)V

    .line 149
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->maxBoostAtt:Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;

    iget-object v2, v7, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v2}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;->setCompetitiveTerm(Lorg/apache/lucene/util/BytesRef;)V

    goto/16 :goto_0
.end method

.method public setNextEnum(Lorg/apache/lucene/index/TermsEnum;)V
    .locals 4
    .param p1, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 79
    iput-object p1, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 80
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termComp:Ljava/util/Comparator;

    .line 82
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/TopTermsRewrite$2;->compareToLastTerm(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    if-nez v0, :cond_1

    .line 86
    new-instance v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget-object v1, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->termComp:Ljava/util/Comparator;

    new-instance v2, Lorg/apache/lucene/index/TermContext;

    iget-object v3, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;-><init>(Ljava/util/Comparator;Lorg/apache/lucene/index/TermContext;)V

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 87
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    const-class v1, Lorg/apache/lucene/search/BoostAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BoostAttribute;

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$2;->boostAtt:Lorg/apache/lucene/search/BoostAttribute;

    .line 88
    return-void
.end method
