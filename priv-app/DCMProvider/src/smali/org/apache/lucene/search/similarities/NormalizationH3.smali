.class public Lorg/apache/lucene/search/similarities/NormalizationH3;
.super Lorg/apache/lucene/search/similarities/Normalization;
.source "NormalizationH3.java"


# instance fields
.field private final mu:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    const/high16 v0, 0x44480000    # 800.0f

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/similarities/NormalizationH3;-><init>(F)V

    .line 32
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0
    .param p1, "mu"    # F

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Normalization;-><init>()V

    .line 39
    iput p1, p0, Lorg/apache/lucene/search/similarities/NormalizationH3;->mu:F

    .line 40
    return-void
.end method


# virtual methods
.method public getMu()F
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/search/similarities/NormalizationH3;->mu:F

    return v0
.end method

.method public tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 5
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tf"    # F
    .param p3, "len"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 44
    iget v0, p0, Lorg/apache/lucene/search/similarities/NormalizationH3;->mu:F

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v2

    long-to-float v1, v2

    add-float/2addr v1, v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfFieldTokens()J

    move-result-wide v2

    long-to-float v2, v2

    add-float/2addr v2, v4

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, p2

    iget v1, p0, Lorg/apache/lucene/search/similarities/NormalizationH3;->mu:F

    add-float/2addr v1, p3

    div-float/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/similarities/NormalizationH3;->mu:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "3("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/similarities/NormalizationH3;->mu:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
