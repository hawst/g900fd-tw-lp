.class public Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;
.super Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
.source "SpanPositionRangeQuery.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected end:I

.field protected start:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;II)V
    .locals 1
    .param p1, "match"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    .line 36
    iput p2, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    .line 37
    iput p3, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    .line 38
    return-void
.end method


# virtual methods
.method protected acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;
    .locals 2
    .param p1, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    sget-boolean v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    if-lt v0, v1, :cond_1

    .line 45
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO_AND_ADVANCE:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    .line 49
    :goto_0
    return-object v0

    .line 46
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    if-gt v0, v1, :cond_2

    .line 47
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->YES:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0

    .line 49
    :cond_2
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->clone()Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;
    .locals 4

    .prologue
    .line 81
    new-instance v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    iget v3, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;II)V

    .line 82
    .local v0, "result":Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->setBoost(F)V

    .line 83
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 91
    check-cast v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;

    .line 92
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;
    iget v3, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    iget v4, v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    iget v4, v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    if-ne v3, v4, :cond_3

    .line 93
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 94
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 92
    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 99
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    .line 100
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0x8

    ushr-int/lit8 v2, v0, 0x19

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 101
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    xor-int/2addr v1, v2

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 102
    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v1, "spanPosRange("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->end:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
