.class final Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DocsWithFieldCache"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 680
    const-class v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 682
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 683
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 688
    iget-object v2, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    .line 689
    .local v2, "field":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    .line 692
    .local v3, "maxDoc":I
    const/4 v5, 0x0

    .line 693
    .local v5, "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v7

    .line 694
    .local v7, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v7, :cond_3

    .line 695
    invoke-virtual {v7}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v8

    .line 696
    .local v8, "termsDocCount":I
    sget-boolean v10, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;->$assertionsDisabled:Z

    if-nez v10, :cond_0

    if-le v8, v3, :cond_0

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 697
    :cond_0
    if-ne v8, v3, :cond_1

    .line 699
    new-instance v10, Lorg/apache/lucene/util/Bits$MatchAllBits;

    invoke-direct {v10, v3}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    .line 733
    .end local v8    # "termsDocCount":I
    :goto_0
    return-object v10

    .line 701
    .restart local v8    # "termsDocCount":I
    :cond_1
    invoke-virtual {v7, v11}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v9

    .line 702
    .local v9, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v1, 0x0

    .line 704
    .local v1, "docs":Lorg/apache/lucene/index/DocsEnum;
    :cond_2
    invoke-virtual {v9}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    .line 705
    .local v6, "term":Lorg/apache/lucene/util/BytesRef;
    if-nez v6, :cond_4

    .line 724
    .end local v1    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v6    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v8    # "termsDocCount":I
    .end local v9    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_3
    if-nez v5, :cond_6

    .line 725
    new-instance v10, Lorg/apache/lucene/util/Bits$MatchNoBits;

    invoke-direct {v10, v3}, Lorg/apache/lucene/util/Bits$MatchNoBits;-><init>(I)V

    goto :goto_0

    .line 708
    .restart local v1    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v6    # "term":Lorg/apache/lucene/util/BytesRef;
    .restart local v8    # "termsDocCount":I
    .restart local v9    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_4
    if-nez v5, :cond_5

    .line 710
    new-instance v5, Lorg/apache/lucene/util/FixedBitSet;

    .end local v5    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-direct {v5, v3}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 713
    .restart local v5    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :cond_5
    const/4 v10, 0x0

    invoke-virtual {v9, v11, v1, v10}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v1

    .line 716
    :goto_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .line 717
    .local v0, "docID":I
    const v10, 0x7fffffff

    if-eq v0, v10, :cond_2

    .line 720
    invoke-virtual {v5, v0}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_1

    .line 727
    .end local v0    # "docID":I
    .end local v1    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v6    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v8    # "termsDocCount":I
    .end local v9    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_6
    invoke-virtual {v5}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v4

    .line 728
    .local v4, "numSet":I
    if-lt v4, v3, :cond_8

    .line 730
    sget-boolean v10, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;->$assertionsDisabled:Z

    if-nez v10, :cond_7

    if-eq v4, v3, :cond_7

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 731
    :cond_7
    new-instance v10, Lorg/apache/lucene/util/Bits$MatchAllBits;

    invoke-direct {v10, v3}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    goto :goto_0

    :cond_8
    move-object v10, v5

    .line 733
    goto :goto_0
.end method
