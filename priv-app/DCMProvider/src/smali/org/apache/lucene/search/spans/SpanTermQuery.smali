.class public Lorg/apache/lucene/search/spans/SpanTermQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanTermQuery.java"


# instance fields
.field protected term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-ne p0, p1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 77
    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 79
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 80
    check-cast v0, Lorg/apache/lucene/search/spans/SpanTermQuery;

    .line 81
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanTermQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_4

    .line 82
    iget-object v3, v0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 83
    goto :goto_0

    .line 84
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 85
    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)",
            "Lorg/apache/lucene/search/spans/Spans;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    const/4 v8, 0x0

    .line 91
    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-interface {p3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/TermContext;

    .line 93
    .local v3, "termContext":Lorg/apache/lucene/index/TermContext;
    if-nez v3, :cond_3

    .line 96
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 97
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v0, :cond_2

    .line 98
    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v6}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v4

    .line 99
    .local v4, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v4, :cond_1

    .line 100
    invoke-virtual {v4, v8}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v5

    .line 101
    .local v5, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v6}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 102
    invoke-virtual {v5}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v2

    .line 116
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    .end local v4    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v5    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .local v2, "state":Lorg/apache/lucene/index/TermState;
    :goto_0
    if-nez v2, :cond_4

    .line 117
    sget-object v6, Lorg/apache/lucene/search/spans/TermSpans;->EMPTY_TERM_SPANS:Lorg/apache/lucene/search/spans/TermSpans;

    .line 126
    :goto_1
    return-object v6

    .line 104
    .end local v2    # "state":Lorg/apache/lucene/index/TermState;
    .restart local v0    # "fields":Lorg/apache/lucene/index/Fields;
    .restart local v4    # "terms":Lorg/apache/lucene/index/Terms;
    .restart local v5    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_0
    const/4 v2, 0x0

    .line 106
    .restart local v2    # "state":Lorg/apache/lucene/index/TermState;
    goto :goto_0

    .line 107
    .end local v2    # "state":Lorg/apache/lucene/index/TermState;
    .end local v5    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_1
    const/4 v2, 0x0

    .line 109
    .restart local v2    # "state":Lorg/apache/lucene/index/TermState;
    goto :goto_0

    .line 110
    .end local v2    # "state":Lorg/apache/lucene/index/TermState;
    .end local v4    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_2
    const/4 v2, 0x0

    .line 112
    .restart local v2    # "state":Lorg/apache/lucene/index/TermState;
    goto :goto_0

    .line 113
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    .end local v2    # "state":Lorg/apache/lucene/index/TermState;
    :cond_3
    iget v6, p1, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v3, v6}, Lorg/apache/lucene/index/TermContext;->get(I)Lorg/apache/lucene/index/TermState;

    move-result-object v2

    .restart local v2    # "state":Lorg/apache/lucene/index/TermState;
    goto :goto_0

    .line 120
    :cond_4
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v7}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v6

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v5

    .line 121
    .restart local v5    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v6}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V

    .line 123
    const/4 v6, 0x2

    invoke-virtual {v5, p2, v8, v6}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v1

    .line 125
    .local v1, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    if-eqz v1, :cond_5

    .line 126
    new-instance v6, Lorg/apache/lucene/search/spans/TermSpans;

    iget-object v7, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-direct {v6, v1, v7}, Lorg/apache/lucene/search/spans/TermSpans;-><init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;Lorg/apache/lucene/index/Term;)V

    goto :goto_1

    .line 129
    :cond_5
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "field \""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v8}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" was indexed without position data; cannot run SpanTermQuery (term="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v8}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 66
    const/16 v0, 0x1f

    .line 67
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v1

    .line 68
    .local v1, "result":I
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 69
    return v1

    .line 68
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanTermQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 59
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
