.class Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
.source "MultiSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/MultiSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiExactDocScorer"
.end annotation


# instance fields
.field private final subScorers:[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;


# direct methods
.method constructor <init>([Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V
    .locals 0
    .param p1, "subScorers"    # [Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .prologue
    .line 80
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;-><init>()V

    .line 81
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .line 82
    return-void
.end method


# virtual methods
.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 95
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;->score(II)F

    move-result v2

    const-string v3, "sum of:"

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 96
    .local v0, "expl":Lorg/apache/lucene/search/Explanation;
    iget-object v3, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 99
    return-object v0

    .line 96
    :cond_0
    aget-object v1, v3, v2

    .line 97
    .local v1, "subScorer":Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;->explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public score(II)F
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # I

    .prologue
    .line 86
    const/4 v1, 0x0

    .line 87
    .local v1, "sum":F
    iget-object v3, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;->subScorers:[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 90
    return v1

    .line 87
    :cond_0
    aget-object v0, v3, v2

    .line 88
    .local v0, "subScorer":Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;->score(II)F

    move-result v5

    add-float/2addr v1, v5

    .line 87
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
