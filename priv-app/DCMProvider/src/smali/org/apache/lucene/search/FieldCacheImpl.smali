.class Lorg/apache/lucene/search/FieldCacheImpl;
.super Ljava/lang/Object;
.source "FieldCacheImpl.java"

# interfaces
.implements Lorg/apache/lucene/search/FieldCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;,
        Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$BytesFromArray;,
        Lorg/apache/lucene/search/FieldCacheImpl$Cache;,
        Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;,
        Lorg/apache/lucene/search/FieldCacheImpl$DocTermOrdsCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$DoubleCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$DoublesFromArray;,
        Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$FloatsFromArray;,
        Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;,
        Lorg/apache/lucene/search/FieldCacheImpl$IntCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$IntsFromArray;,
        Lorg/apache/lucene/search/FieldCacheImpl$LongCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$LongsFromArray;,
        Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$ShortsFromArray;,
        Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;,
        Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private caches:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/lucene/search/FieldCacheImpl$Cache;",
            ">;"
        }
    .end annotation
.end field

.field private volatile infoStream:Ljava/io/PrintStream;

.field final purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

.field final purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    new-instance v0, Lorg/apache/lucene/search/FieldCacheImpl$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheImpl$1;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .line 121
    new-instance v0, Lorg/apache/lucene/search/FieldCacheImpl$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheImpl$2;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCacheImpl;->init()V

    .line 61
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/AtomicReader;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl;->initReader(Lorg/apache/lucene/index/AtomicReader;)V

    return-void
.end method

.method private declared-synchronized init()V
    .locals 3

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$DoubleCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$DoubleCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/index/BinaryDocValues;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/index/SortedDocValues;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/index/DocTermOrds;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$DocTermOrdsCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$DocTermOrdsCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private initReader(Lorg/apache/lucene/index/AtomicReader;)V
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;

    .prologue
    .line 130
    instance-of v1, p1, Lorg/apache/lucene/index/SegmentReader;

    if-eqz v1, :cond_0

    .line 131
    check-cast p1, Lorg/apache/lucene/index/SegmentReader;

    .end local p1    # "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/SegmentReader;->addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 143
    :goto_0
    return-void

    .line 135
    .restart local p1    # "reader":Lorg/apache/lucene/index/AtomicReader;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v0

    .line 136
    .local v0, "key":Ljava/lang/Object;
    instance-of v1, v0, Lorg/apache/lucene/index/AtomicReader;

    if-eqz v1, :cond_1

    .line 137
    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    .end local v0    # "key":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/AtomicReader;->addReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V

    goto :goto_0

    .line 140
    .restart local v0    # "key":Ljava/lang/Object;
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/AtomicReader;->addReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V

    goto :goto_0
.end method


# virtual methods
.method public getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)Lorg/apache/lucene/search/FieldCache$Bytes;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$ByteParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 366
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 367
    .local v1, "valuesIn":Lorg/apache/lucene/index/NumericDocValues;
    if-eqz v1, :cond_0

    .line 370
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$3;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$3;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 385
    :goto_0
    return-object v2

    .line 377
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 378
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 379
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Bytes;->EMPTY:Lorg/apache/lucene/search/FieldCache$Bytes;

    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 381
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 382
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 383
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Bytes;->EMPTY:Lorg/apache/lucene/search/FieldCache$Bytes;

    goto :goto_0

    .line 385
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-direct {v3, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$Bytes;

    goto :goto_0
.end method

.method public getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Bytes;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)Lorg/apache/lucene/search/FieldCache$Bytes;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .locals 18

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    new-instance v13, Ljava/util/ArrayList;

    const/16 v1, 0x11

    invoke-direct {v13, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 92
    .local v13, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    invoke-interface {v13, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/search/FieldCache$CacheEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit p0

    return-object v1

    .line 92
    :cond_0
    :try_start_1
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 93
    .local v8, "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    .line 94
    .local v7, "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Class;

    .line 95
    .local v4, "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v15, v7, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    monitor-enter v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 96
    :try_start_2
    iget-object v1, v7, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 95
    monitor-exit v15

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 91
    .end local v4    # "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    .end local v8    # "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    .end local v13    # "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 96
    .restart local v4    # "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v7    # "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    .restart local v8    # "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    .restart local v13    # "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    :cond_2
    :try_start_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 97
    .local v12, "readerCacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Ljava/lang/Object;>;>;"
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 98
    .local v2, "readerKey":Ljava/lang/Object;
    if-eqz v2, :cond_1

    .line 99
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map;

    .line 100
    .local v10, "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Ljava/lang/Object;>;"
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 101
    .local v11, "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Ljava/lang/Object;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    .line 102
    .local v9, "entry":Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    new-instance v1, Lorg/apache/lucene/search/FieldCache$CacheEntry;

    iget-object v3, v9, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    .line 103
    iget-object v5, v9, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    .line 104
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lorg/apache/lucene/search/FieldCache$CacheEntry;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 102
    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public getDocTermOrds(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1371
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    .line 1372
    .local v1, "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    if-eqz v1, :cond_0

    .line 1391
    .end local v1    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    :goto_0
    return-object v1

    .line 1376
    .restart local v1    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v3

    .line 1377
    .local v3, "sdv":Lorg/apache/lucene/index/SortedDocValues;
    if-eqz v3, :cond_1

    .line 1378
    new-instance v1, Lorg/apache/lucene/index/SingletonSortedSetDocValues;

    .end local v1    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    invoke-direct {v1, v3}, Lorg/apache/lucene/index/SingletonSortedSetDocValues;-><init>(Lorg/apache/lucene/index/SortedDocValues;)V

    goto :goto_0

    .line 1381
    .restart local v1    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    invoke-virtual {v4, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 1382
    .local v2, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v2, :cond_2

    .line 1383
    sget-object v1, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    goto :goto_0

    .line 1384
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1385
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Type mismatch: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " was indexed as "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1386
    :cond_3
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1387
    sget-object v1, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    goto :goto_0

    .line 1390
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v5, Lorg/apache/lucene/index/DocTermOrds;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v5, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    const/4 v6, 0x0

    invoke-direct {v5, p2, v6}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v6, 0x0

    invoke-virtual {v4, p1, v5, v6}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocTermOrds;

    .line 1391
    .local v0, "dto":Lorg/apache/lucene/index/DocTermOrds;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocTermOrds;->iterator(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 667
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 668
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_0

    .line 670
    new-instance v1, Lorg/apache/lucene/util/Bits$MatchNoBits;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/Bits$MatchNoBits;-><init>(I)V

    .line 677
    :goto_0
    return-object v1

    .line 671
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 673
    new-instance v1, Lorg/apache/lucene/util/Bits$MatchAllBits;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    goto :goto_0

    .line 674
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 675
    new-instance v1, Lorg/apache/lucene/util/Bits$MatchNoBits;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/Bits$MatchNoBits;-><init>(I)V

    goto :goto_0

    .line 677
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v2, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    const/4 v3, 0x0

    invoke-direct {v2, p2, v3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/Bits;

    goto :goto_0
.end method

.method public getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$DoubleParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 968
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 969
    .local v1, "valuesIn":Lorg/apache/lucene/index/NumericDocValues;
    if-eqz v1, :cond_0

    .line 972
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$8;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$8;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 987
    :goto_0
    return-object v2

    .line 979
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 980
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 981
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Doubles;->EMPTY:Lorg/apache/lucene/search/FieldCache$Doubles;

    goto :goto_0

    .line 982
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 983
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 984
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 985
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Doubles;->EMPTY:Lorg/apache/lucene/search/FieldCache$Doubles;

    goto :goto_0

    .line 987
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-direct {v3, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$Doubles;

    goto :goto_0
.end method

.method public getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Doubles;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 962
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;

    move-result-object v0

    return-object v0
.end method

.method public getFloats(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)Lorg/apache/lucene/search/FieldCache$Floats;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$FloatParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 746
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 747
    .local v1, "valuesIn":Lorg/apache/lucene/index/NumericDocValues;
    if-eqz v1, :cond_0

    .line 750
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$6;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$6;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 765
    :goto_0
    return-object v2

    .line 757
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 758
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 759
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Floats;->EMPTY:Lorg/apache/lucene/search/FieldCache$Floats;

    goto :goto_0

    .line 760
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 761
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 762
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 763
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Floats;->EMPTY:Lorg/apache/lucene/search/FieldCache$Floats;

    goto :goto_0

    .line 765
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-direct {v3, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$Floats;

    goto :goto_0
.end method

.method public getFloats(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Floats;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 740
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getFloats(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)Lorg/apache/lucene/search/FieldCache$Floats;

    move-result-object v0

    return-object v0
.end method

.method public getInfoStream()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 1413
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method public getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$IntParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 553
    .local v1, "valuesIn":Lorg/apache/lucene/index/NumericDocValues;
    if-eqz v1, :cond_0

    .line 556
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$5;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$5;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 571
    :goto_0
    return-object v2

    .line 563
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 564
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 565
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Ints;->EMPTY:Lorg/apache/lucene/search/FieldCache$Ints;

    goto :goto_0

    .line 566
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 567
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 568
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 569
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Ints;->EMPTY:Lorg/apache/lucene/search/FieldCache$Ints;

    goto :goto_0

    .line 571
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-direct {v3, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$Ints;

    goto :goto_0
.end method

.method public getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Ints;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 546
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;

    move-result-object v0

    return-object v0
.end method

.method public getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)Lorg/apache/lucene/search/FieldCache$Longs;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$LongParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 857
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 858
    .local v1, "valuesIn":Lorg/apache/lucene/index/NumericDocValues;
    if-eqz v1, :cond_0

    .line 861
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$7;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$7;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 876
    :goto_0
    return-object v2

    .line 868
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 869
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 870
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Longs;->EMPTY:Lorg/apache/lucene/search/FieldCache$Longs;

    goto :goto_0

    .line 871
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 872
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 873
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 874
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Longs;->EMPTY:Lorg/apache/lucene/search/FieldCache$Longs;

    goto :goto_0

    .line 876
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-direct {v3, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$Longs;

    goto :goto_0
.end method

.method public getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Longs;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 851
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)Lorg/apache/lucene/search/FieldCache$Longs;

    move-result-object v0

    return-object v0
.end method

.method public getShorts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)Lorg/apache/lucene/search/FieldCache$Shorts;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$ShortParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 460
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    .line 461
    .local v1, "valuesIn":Lorg/apache/lucene/index/NumericDocValues;
    if-eqz v1, :cond_0

    .line 464
    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$4;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$4;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 479
    :goto_0
    return-object v2

    .line 471
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 472
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 473
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Shorts;->EMPTY:Lorg/apache/lucene/search/FieldCache$Shorts;

    goto :goto_0

    .line 474
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 475
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 476
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 477
    sget-object v2, Lorg/apache/lucene/search/FieldCache$Shorts;->EMPTY:Lorg/apache/lucene/search/FieldCache$Shorts;

    goto :goto_0

    .line 479
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v3, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-direct {v3, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$Shorts;

    goto :goto_0
.end method

.method public getShorts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Z)Lorg/apache/lucene/search/FieldCache$Shorts;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 454
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/FieldCacheImpl;->getShorts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)Lorg/apache/lucene/search/FieldCache$Shorts;

    move-result-object v0

    return-object v0
.end method

.method public getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1261
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1265
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    .line 1266
    .local v1, "valuesIn":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v1, :cond_0

    .line 1267
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    .line 1270
    :cond_0
    if-eqz v1, :cond_1

    .line 1285
    .end local v1    # "valuesIn":Lorg/apache/lucene/index/BinaryDocValues;
    :goto_0
    return-object v1

    .line 1276
    .restart local v1    # "valuesIn":Lorg/apache/lucene/index/BinaryDocValues;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 1277
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_2

    .line 1278
    sget-object v1, Lorg/apache/lucene/index/BinaryDocValues;->EMPTY:Lorg/apache/lucene/index/BinaryDocValues;

    goto :goto_0

    .line 1279
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1280
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1281
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1282
    sget-object v1, Lorg/apache/lucene/index/BinaryDocValues;->EMPTY:Lorg/apache/lucene/index/BinaryDocValues;

    goto :goto_0

    .line 1285
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v3, Lorg/apache/lucene/index/BinaryDocValues;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/BinaryDocValues;

    move-object v1, v2

    goto :goto_0
.end method

.method public getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1106
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/SortedDocValues;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1110
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    .line 1111
    .local v1, "valuesIn":Lorg/apache/lucene/index/SortedDocValues;
    if-eqz v1, :cond_0

    .line 1126
    .end local v1    # "valuesIn":Lorg/apache/lucene/index/SortedDocValues;
    :goto_0
    return-object v1

    .line 1116
    .restart local v1    # "valuesIn":Lorg/apache/lucene/index/SortedDocValues;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 1117
    .local v0, "info":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 1118
    sget-object v1, Lorg/apache/lucene/search/FieldCacheImpl;->EMPTY_TERMSINDEX:Lorg/apache/lucene/index/SortedDocValues;

    goto :goto_0

    .line 1119
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1122
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type mismatch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was indexed as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1123
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1124
    sget-object v1, Lorg/apache/lucene/search/FieldCacheImpl;->EMPTY_TERMSINDEX:Lorg/apache/lucene/index/SortedDocValues;

    goto :goto_0

    .line 1126
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v3, Lorg/apache/lucene/index/SortedDocValues;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v3, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SortedDocValues;

    move-object v1, v2

    goto :goto_0
.end method

.method public declared-synchronized purge(Lorg/apache/lucene/index/AtomicReader;)V
    .locals 3
    .param p1, "r"    # Lorg/apache/lucene/index/AtomicReader;

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 87
    monitor-exit p0

    return-void

    .line 84
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    .line 85
    .local v0, "c":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->purge(Lorg/apache/lucene/index/AtomicReader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 84
    .end local v0    # "c":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized purgeAllCaches()V
    .locals 1

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCacheImpl;->init()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method setDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "docsWithField"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 339
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v1

    .line 341
    .local v1, "maxDoc":I
    if-nez p3, :cond_0

    .line 342
    new-instance v0, Lorg/apache/lucene/util/Bits$MatchNoBits;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/Bits$MatchNoBits;-><init>(I)V

    .line 355
    .local v0, "bits":Lorg/apache/lucene/util/Bits;
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v4, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v4, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    const/4 v5, 0x0

    invoke-direct {v4, p2, v5}, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, p1, v4, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->put(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Ljava/lang/Object;)V

    .line 356
    return-void

    .line 343
    .end local v0    # "bits":Lorg/apache/lucene/util/Bits;
    :cond_0
    instance-of v3, p3, Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v3, :cond_3

    move-object v3, p3

    .line 344
    check-cast v3, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v3}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v2

    .line 345
    .local v2, "numSet":I
    if-lt v2, v1, :cond_2

    .line 347
    sget-boolean v3, Lorg/apache/lucene/search/FieldCacheImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-eq v2, v1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 348
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/Bits$MatchAllBits;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    .line 349
    .restart local v0    # "bits":Lorg/apache/lucene/util/Bits;
    goto :goto_0

    .line 350
    .end local v0    # "bits":Lorg/apache/lucene/util/Bits;
    :cond_2
    move-object v0, p3

    .line 352
    .restart local v0    # "bits":Lorg/apache/lucene/util/Bits;
    goto :goto_0

    .line 353
    .end local v0    # "bits":Lorg/apache/lucene/util/Bits;
    .end local v2    # "numSet":I
    :cond_3
    move-object v0, p3

    .restart local v0    # "bits":Lorg/apache/lucene/util/Bits;
    goto :goto_0
.end method

.method public setInfoStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/PrintStream;

    .prologue
    .line 1409
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->infoStream:Ljava/io/PrintStream;

    .line 1410
    return-void
.end method
