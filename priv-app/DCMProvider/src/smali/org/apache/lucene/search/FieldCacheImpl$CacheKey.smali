.class Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
.super Ljava/lang/Object;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CacheKey"
.end annotation


# instance fields
.field final custom:Ljava/lang/Object;

.field final field:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "custom"    # Ljava/lang/Object;

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    .line 259
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    .line 260
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 265
    instance-of v2, p1, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 266
    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;

    .line 267
    .local v0, "other":Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    iget-object v2, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    iget-object v2, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 269
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    if-nez v2, :cond_2

    .line 275
    .end local v0    # "other":Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    :cond_0
    :goto_0
    return v1

    .line 270
    .restart local v0    # "other":Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    :cond_1
    iget-object v2, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    .end local v0    # "other":Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
