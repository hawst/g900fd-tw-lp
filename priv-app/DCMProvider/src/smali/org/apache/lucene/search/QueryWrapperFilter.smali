.class public Lorg/apache/lucene/search/QueryWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "QueryWrapperFilter.java"


# instance fields
.field private final query:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 42
    if-nez p1, :cond_0

    .line 43
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Query may not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    .line 45
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 74
    instance-of v0, p1, Lorg/apache/lucene/search/QueryWrapperFilter;

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 76
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    check-cast p1, Lorg/apache/lucene/search/QueryWrapperFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->getContext()Lorg/apache/lucene/index/AtomicReaderContext;

    move-result-object v0

    .line 56
    .local v0, "privateContext":Lorg/apache/lucene/index/AtomicReaderContext;
    new-instance v2, Lorg/apache/lucene/search/IndexSearcher;

    invoke-direct {v2, v0}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    iget-object v3, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    .line 57
    .local v1, "weight":Lorg/apache/lucene/search/Weight;
    new-instance v2, Lorg/apache/lucene/search/QueryWrapperFilter$1;

    invoke-direct {v2, p0, v1, v0, p2}, Lorg/apache/lucene/search/QueryWrapperFilter$1;-><init>(Lorg/apache/lucene/search/QueryWrapperFilter;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V

    return-object v2
.end method

.method public final getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    const v1, -0x6dc09b47

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "QueryWrapperFilter("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
