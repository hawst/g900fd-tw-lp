.class public Lorg/apache/lucene/search/similarities/LMSimilarity$DefaultCollectionModel;
.super Ljava/lang/Object;
.source "LMSimilarity.java"

# interfaces
.implements Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/LMSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DefaultCollectionModel"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public computeProbability(Lorg/apache/lucene/search/similarities/BasicStats;)F
    .locals 5
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 154
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v0

    long-to-float v0, v0

    add-float/2addr v0, v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfFieldTokens()J

    move-result-wide v2

    long-to-float v1, v2

    add-float/2addr v1, v4

    div-float/2addr v0, v1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return-object v0
.end method
