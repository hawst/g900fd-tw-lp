.class final Lorg/apache/lucene/search/SloppyPhraseScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "SloppyPhraseScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private checkedRpts:Z

.field private final cost:J

.field private final docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

.field private end:I

.field private hasMultiTermRpts:Z

.field private hasRpts:Z

.field private max:Lorg/apache/lucene/search/PhrasePositions;

.field private min:Lorg/apache/lucene/search/PhrasePositions;

.field private numMatches:I

.field private final numPostings:I

.field private final pq:Lorg/apache/lucene/search/PhraseQueue;

.field private rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

.field private rptStack:[Lorg/apache/lucene/search/PhrasePositions;

.field private final slop:I

.field private sloppyFreq:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/search/SloppyPhraseScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/SloppyPhraseScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;ILorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V
    .locals 8
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "postings"    # [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    .param p3, "slop"    # I
    .param p4, "docScorer"    # Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 57
    iput-object p4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .line 58
    iput p3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->slop:I

    .line 59
    if-nez p2, :cond_1

    move v2, v3

    :goto_0
    iput v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numPostings:I

    .line 60
    new-instance v2, Lorg/apache/lucene/search/PhraseQueue;

    array-length v4, p2

    invoke-direct {v2, v4}, Lorg/apache/lucene/search/PhraseQueue;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    .line 62
    aget-object v2, p2, v3

    iget-object v2, v2, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->cost:J

    .line 68
    array-length v2, p2

    if-lez v2, :cond_0

    .line 69
    new-instance v2, Lorg/apache/lucene/search/PhrasePositions;

    aget-object v4, p2, v3

    iget-object v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    aget-object v5, p2, v3

    iget v5, v5, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    aget-object v6, p2, v3

    iget-object v6, v6, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    invoke-direct {v2, v4, v5, v3, v6}, Lorg/apache/lucene/search/PhrasePositions;-><init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;II[Lorg/apache/lucene/index/Term;)V

    iput-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .line 70
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    .line 71
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iput v7, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 72
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    array-length v2, p2

    if-lt v0, v2, :cond_2

    .line 78
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget-object v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v3, v2, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    .line 80
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 59
    :cond_1
    array-length v2, p2

    goto :goto_0

    .line 73
    .restart local v0    # "i":I
    :cond_2
    new-instance v1, Lorg/apache/lucene/search/PhrasePositions;

    aget-object v2, p2, v0

    iget-object v2, v2, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    aget-object v3, p2, v0

    iget v3, v3, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    aget-object v4, p2, v0

    iget-object v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    invoke-direct {v1, v2, v3, v0, v4}, Lorg/apache/lucene/search/PhrasePositions;-><init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;II[Lorg/apache/lucene/index/Term;)V

    .line 74
    .local v1, "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v1, v2, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    .line 75
    iput-object v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    .line 76
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iput v7, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private advanceMin(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 548
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/PhrasePositions;->skipTo(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 549
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    const v1, 0x7fffffff

    iput v1, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 550
    const/4 v0, 0x0

    .line 554
    :goto_0
    return v0

    .line 552
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .line 553
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    .line 554
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z
    .locals 2
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p1}, Lorg/apache/lucene/search/PhrasePositions;->nextPosition()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 144
    :goto_0
    return v0

    .line 141
    :cond_0
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    if-le v0, v1, :cond_1

    .line 142
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iput v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 144
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private advanceRepeatGroups()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 286
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    array-length v10, v9

    move v8, v7

    :goto_0
    if-lt v8, v10, :cond_1

    .line 316
    const/4 v7, 0x1

    :cond_0
    return v7

    .line 286
    :cond_1
    aget-object v6, v9, v8

    .line 287
    .local v6, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    iget-boolean v11, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    if-eqz v11, :cond_6

    .line 290
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v11, v6

    if-lt v0, v11, :cond_3

    .line 286
    .end local v0    # "i":I
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 291
    .restart local v0    # "i":I
    :cond_3
    const/4 v1, 0x1

    .line 292
    .local v1, "incr":I
    aget-object v4, v6, v0

    .line 294
    .local v4, "pp":Lorg/apache/lucene/search/PhrasePositions;
    :cond_4
    invoke-direct {p0, v4}, Lorg/apache/lucene/search/SloppyPhraseScorer;->collide(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v3

    .local v3, "k":I
    if-gez v3, :cond_5

    .line 290
    :goto_2
    add-int/2addr v0, v1

    goto :goto_1

    .line 295
    :cond_5
    aget-object v11, v6, v3

    invoke-direct {p0, v4, v11}, Lorg/apache/lucene/search/SloppyPhraseScorer;->lesser(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Lorg/apache/lucene/search/PhrasePositions;

    move-result-object v5

    .line 296
    .local v5, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    invoke-direct {p0, v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 299
    iget v11, v5, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    if-ge v11, v0, :cond_4

    .line 300
    const/4 v1, 0x0

    .line 301
    goto :goto_2

    .line 307
    .end local v0    # "i":I
    .end local v1    # "incr":I
    .end local v3    # "k":I
    .end local v4    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    .end local v5    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_6
    const/4 v2, 0x1

    .local v2, "j":I
    :goto_3
    array-length v11, v6

    if-ge v2, v11, :cond_2

    .line 308
    const/4 v3, 0x0

    .restart local v3    # "k":I
    :goto_4
    if-lt v3, v2, :cond_7

    .line 307
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 309
    :cond_7
    aget-object v11, v6, v2

    invoke-virtual {v11}, Lorg/apache/lucene/search/PhrasePositions;->nextPosition()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 308
    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method private advanceRpts(Lorg/apache/lucene/search/PhrasePositions;)Z
    .locals 14
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 151
    iget v9, p1, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    if-gez v9, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v8

    .line 154
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    iget v10, p1, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    aget-object v7, v9, v10

    .line 155
    .local v7, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    new-instance v0, Lorg/apache/lucene/util/OpenBitSet;

    array-length v9, v7

    int-to-long v10, v9

    invoke-direct {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 156
    .local v0, "bits":Lorg/apache/lucene/util/OpenBitSet;
    iget v3, p1, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    .line 158
    .local v3, "k0":I
    :cond_2
    :goto_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->collide(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v2

    .local v2, "k":I
    if-gez v2, :cond_3

    .line 169
    const/4 v4, 0x0

    .line 170
    .local v4, "n":I
    :goto_2
    invoke-virtual {v0}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gtz v9, :cond_5

    .line 178
    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_3
    if-ltz v1, :cond_0

    .line 179
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    iget-object v10, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptStack:[Lorg/apache/lucene/search/PhrasePositions;

    aget-object v10, v10, v1

    invoke-virtual {v9, v10}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 159
    .end local v1    # "i":I
    .end local v4    # "n":I
    :cond_3
    aget-object v9, v7, v2

    invoke-direct {p0, p1, v9}, Lorg/apache/lucene/search/SloppyPhraseScorer;->lesser(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Lorg/apache/lucene/search/PhrasePositions;

    move-result-object p1

    .line 160
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 161
    const/4 v8, 0x0

    goto :goto_0

    .line 163
    :cond_4
    if-eq v2, v3, :cond_2

    .line 164
    int-to-long v10, v2

    invoke-virtual {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    goto :goto_1

    .line 171
    .restart local v4    # "n":I
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v9}, Lorg/apache/lucene/search/PhraseQueue;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/PhrasePositions;

    .line 172
    .local v6, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptStack:[Lorg/apache/lucene/search/PhrasePositions;

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "n":I
    .local v5, "n":I
    aput-object v6, v9, v4

    .line 173
    iget v9, v6, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    if-ltz v9, :cond_6

    iget v9, v6, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    invoke-virtual {v0, v9}, Lorg/apache/lucene/util/OpenBitSet;->get(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 174
    iget v9, v6, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    int-to-long v10, v9

    invoke-virtual {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->clear(J)V

    :cond_6
    move v4, v5

    .end local v5    # "n":I
    .restart local v4    # "n":I
    goto :goto_2
.end method

.method private collide(Lorg/apache/lucene/search/PhrasePositions;)I
    .locals 6
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v3

    .line 196
    .local v3, "tpPos":I
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    iget v5, p1, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    aget-object v2, v4, v5

    .line 197
    .local v2, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_0

    .line 203
    const/4 v4, -0x1

    :goto_1
    return v4

    .line 198
    :cond_0
    aget-object v1, v2, v0

    .line 199
    .local v1, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    if-eq v1, p1, :cond_1

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 200
    iget v4, v1, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    goto :goto_1

    .line 197
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private fillQueue()V
    .locals 4

    .prologue
    .line 267
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/PhraseQueue;->clear()V

    .line 268
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-ne v1, v2, :cond_0

    .line 274
    return-void

    .line 269
    :cond_0
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    if-le v2, v3, :cond_1

    .line 270
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iput v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 272
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0
.end method

.method private gatherRptGroups(Ljava/util/LinkedHashMap;)Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/PhrasePositions;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 376
    .local p1, "rptTerms":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->repeatingPPs(Ljava/util/HashMap;)[Lorg/apache/lucene/search/PhrasePositions;

    move-result-object v12

    .line 377
    .local v12, "rpp":[Lorg/apache/lucene/search/PhrasePositions;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 378
    .local v10, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    move/from16 v17, v0

    if-nez v17, :cond_7

    .line 380
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, v12

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v6, v0, :cond_1

    .line 429
    :cond_0
    return-object v10

    .line 381
    :cond_1
    aget-object v8, v12, v6

    .line 382
    .local v8, "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget v0, v8, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v17, v0

    if-ltz v17, :cond_3

    .line 380
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 383
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v16

    .line 384
    .local v16, "tpPos":I
    add-int/lit8 v7, v6, 0x1

    .local v7, "j":I
    :goto_1
    array-length v0, v12

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v7, v0, :cond_2

    .line 385
    aget-object v9, v12, v7

    .line 387
    .local v9, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    iget v0, v9, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v17, v0

    if-gez v17, :cond_4

    .line 388
    iget v0, v9, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    move/from16 v17, v0

    iget v0, v8, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    .line 389
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v17

    move/from16 v0, v17

    move/from16 v1, v16

    if-eq v0, v1, :cond_5

    .line 384
    :cond_4
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 393
    :cond_5
    iget v4, v8, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 394
    .local v4, "g":I
    if-gez v4, :cond_6

    .line 395
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 396
    iput v4, v8, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 397
    new-instance v11, Ljava/util/ArrayList;

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 398
    .local v11, "rl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;"
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    .end local v11    # "rl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;"
    :cond_6
    iput v4, v9, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 402
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 407
    .end local v4    # "g":I
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v8    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    .end local v9    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    .end local v16    # "tpPos":I
    :cond_7
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 408
    .local v15, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->ppTermsBitSets([Lorg/apache/lucene/search/PhrasePositions;Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v2

    .line 409
    .local v2, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/lucene/search/SloppyPhraseScorer;->unionTermGroups(Ljava/util/ArrayList;)V

    .line 410
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SloppyPhraseScorer;->termGroups(Ljava/util/LinkedHashMap;Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v14

    .line 411
    .local v14, "tg":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 412
    .local v3, "distinctGroupIDs":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_3
    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v6, v0, :cond_8

    .line 415
    array-length v0, v12

    move/from16 v20, v0

    const/16 v17, 0x0

    move/from16 v18, v17

    :goto_4
    move/from16 v0, v18

    move/from16 v1, v20

    if-lt v0, v1, :cond_9

    .line 425
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashSet;

    .line 426
    .local v5, "hs":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;"
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 413
    .end local v5    # "hs":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;"
    :cond_8
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 415
    :cond_9
    aget-object v8, v12, v18

    .line 416
    .restart local v8    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v0, v8, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    const/16 v17, 0x0

    move/from16 v19, v17

    :goto_6
    move/from16 v0, v19

    move/from16 v1, v22

    if-lt v0, v1, :cond_a

    .line 415
    add-int/lit8 v17, v18, 0x1

    move/from16 v18, v17

    goto :goto_4

    .line 416
    :cond_a
    aget-object v13, v21, v19

    .line 417
    .local v13, "t":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 418
    invoke-virtual {v14, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 419
    .restart local v4    # "g":I
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/HashSet;

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 420
    sget-boolean v17, Lorg/apache/lucene/search/SloppyPhraseScorer;->$assertionsDisabled:Z

    if-nez v17, :cond_b

    iget v0, v8, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v17, v0

    const/16 v23, -0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-eq v0, v1, :cond_b

    iget v0, v8, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v4, :cond_b

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 421
    :cond_b
    iput v4, v8, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 416
    .end local v4    # "g":I
    :cond_c
    add-int/lit8 v17, v19, 0x1

    move/from16 v19, v17

    goto :goto_6
.end method

.method private initComplex()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->placeFirstPositions()V

    .line 251
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceRepeatGroups()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    const/4 v0, 0x0

    .line 255
    :goto_0
    return v0

    .line 254
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->fillQueue()V

    .line 255
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initFirstTime()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 335
    iput-boolean v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->checkedRpts:Z

    .line 336
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->placeFirstPositions()V

    .line 338
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->repeatingTerms()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 339
    .local v1, "rptTerms":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    .line 341
    iget-boolean v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    if-eqz v2, :cond_1

    .line 342
    iget v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numPostings:I

    new-array v2, v2, [Lorg/apache/lucene/search/PhrasePositions;

    iput-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptStack:[Lorg/apache/lucene/search/PhrasePositions;

    .line 343
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->gatherRptGroups(Ljava/util/LinkedHashMap;)Ljava/util/ArrayList;

    move-result-object v0

    .line 344
    .local v0, "rgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->sortRptGroups(Ljava/util/ArrayList;)V

    .line 345
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceRepeatGroups()Z

    move-result v2

    if-nez v2, :cond_1

    .line 351
    .end local v0    # "rgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    :goto_1
    return v3

    :cond_0
    move v2, v4

    .line 339
    goto :goto_0

    .line 350
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->fillQueue()V

    move v3, v4

    .line 351
    goto :goto_1
.end method

.method private initPhrasePositions()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    const/high16 v0, -0x80000000

    iput v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 223
    iget-boolean v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->checkedRpts:Z

    if-nez v0, :cond_0

    .line 224
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initFirstTime()Z

    move-result v0

    .line 230
    :goto_0
    return v0

    .line 226
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    if-nez v0, :cond_1

    .line 227
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initSimple()V

    .line 228
    const/4 v0, 0x1

    goto :goto_0

    .line 230
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initComplex()Z

    move-result v0

    goto :goto_0
.end method

.method private initSimple()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/PhraseQueue;->clear()V

    .line 238
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-ne v1, v2, :cond_0

    .line 245
    return-void

    .line 239
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/PhrasePositions;->firstPosition()V

    .line 240
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    if-le v2, v3, :cond_1

    .line 241
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iput v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 243
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0
.end method

.method private lesser(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Lorg/apache/lucene/search/PhrasePositions;
    .locals 2
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;
    .param p2, "pp2"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 186
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-lt v0, v1, :cond_0

    .line 187
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    if-ge v0, v1, :cond_1

    :cond_0
    move-object p2, p1

    .line 190
    .end local p2    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_1
    return-object p2
.end method

.method private phraseFreq()F
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initPhrasePositions()Z

    move-result v5

    if-nez v5, :cond_1

    .line 102
    const/4 v0, 0x0

    .line 133
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    const/4 v0, 0x0

    .line 105
    .local v0, "freq":F
    const/4 v5, 0x0

    iput v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numMatches:I

    .line 106
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/PhrasePositions;

    .line 107
    .local v4, "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    iget v6, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    sub-int v1, v5, v6

    .line 108
    .local v1, "matchLength":I
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->top()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/PhrasePositions;

    iget v3, v5, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 109
    .local v3, "next":I
    :cond_2
    :goto_1
    invoke-direct {p0, v4}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 129
    :cond_3
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->slop:I

    if-gt v1, v5, :cond_0

    .line 130
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computeSlopFactor(I)F

    move-result v5

    add-float/2addr v0, v5

    .line 131
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numMatches:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numMatches:I

    goto :goto_0

    .line 110
    :cond_4
    iget-boolean v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    if-eqz v5, :cond_5

    invoke-direct {p0, v4}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceRpts(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 113
    :cond_5
    iget v5, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-le v5, v3, :cond_7

    .line 114
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->slop:I

    if-gt v1, v5, :cond_6

    .line 115
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->computeSlopFactor(I)F

    move-result v5

    add-float/2addr v0, v5

    .line 116
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numMatches:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numMatches:I

    .line 118
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->pop()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    check-cast v4, Lorg/apache/lucene/search/PhrasePositions;

    .line 120
    .restart local v4    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->top()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/PhrasePositions;

    iget v3, v5, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 121
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    iget v6, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    sub-int v1, v5, v6

    .line 122
    goto :goto_1

    .line 123
    :cond_7
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    iget v6, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    sub-int v2, v5, v6

    .line 124
    .local v2, "matchLength2":I
    if-ge v2, v1, :cond_2

    .line 125
    move v1, v2

    goto :goto_1
.end method

.method private placeFirstPositions()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-ne v1, v2, :cond_0

    .line 263
    return-void

    .line 261
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/PhrasePositions;->firstPosition()V

    .line 260
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0
.end method

.method private ppTermsBitSets([Lorg/apache/lucene/search/PhrasePositions;Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "rpp"    # [Lorg/apache/lucene/search/PhrasePositions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/lucene/search/PhrasePositions;",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/util/OpenBitSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471
    .local p2, "tord":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    array-length v5, p1

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 472
    .local v1, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    array-length v7, p1

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-lt v6, v7, :cond_0

    .line 482
    return-object v1

    .line 472
    :cond_0
    aget-object v3, p1, v6

    .line 473
    .local v3, "pp":Lorg/apache/lucene/search/PhrasePositions;
    new-instance v0, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result v5

    int-to-long v8, v5

    invoke-direct {v0, v8, v9}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 475
    .local v0, "b":Lorg/apache/lucene/util/OpenBitSet;
    iget-object v8, v3, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    array-length v9, v8

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v9, :cond_1

    .line 480
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 475
    :cond_1
    aget-object v4, v8, v5

    .line 476
    .local v4, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .local v2, "ord":Ljava/lang/Integer;
    if-eqz v2, :cond_2

    .line 477
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    .line 475
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private repeatingPPs(Ljava/util/HashMap;)[Lorg/apache/lucene/search/PhrasePositions;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;)[",
            "Lorg/apache/lucene/search/PhrasePositions;"
        }
    .end annotation

    .prologue
    .local p1, "rptTerms":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 456
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 457
    .local v2, "rp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-ne v1, v4, :cond_0

    .line 466
    new-array v4, v6, [Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/apache/lucene/search/PhrasePositions;

    return-object v4

    .line 458
    :cond_0
    iget-object v7, v0, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    array-length v8, v7

    move v4, v6

    :goto_1
    if-lt v4, v8, :cond_1

    .line 457
    :goto_2
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .line 458
    :cond_1
    aget-object v3, v7, v4

    .line 459
    .local v3, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 460
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    iget-boolean v7, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    iget-object v4, v0, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    array-length v4, v4

    if-le v4, v5, :cond_2

    move v4, v5

    :goto_3
    or-int/2addr v4, v7

    iput-boolean v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    goto :goto_2

    :cond_2
    move v4, v6

    goto :goto_3

    .line 458
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private repeatingTerms()Ljava/util/LinkedHashMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    .line 440
    .local v6, "tord":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 441
    .local v5, "tcnt":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v2, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v3, 0x0

    .local v3, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-ne v3, v7, :cond_0

    .line 451
    return-object v6

    .line 442
    :cond_0
    iget-object v8, v2, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    array-length v9, v8

    const/4 v7, 0x0

    :goto_1
    if-lt v7, v9, :cond_1

    .line 441
    move-object v3, v2

    iget-object v2, v2, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .line 442
    :cond_1
    aget-object v4, v8, v7

    .line 443
    .local v4, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 444
    .local v1, "cnt0":Ljava/lang/Integer;
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/Integer;

    const/4 v10, 0x1

    invoke-direct {v0, v10}, Ljava/lang/Integer;-><init>(I)V

    .line 445
    .local v0, "cnt":Ljava/lang/Integer;
    :goto_2
    invoke-virtual {v5, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 447
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v4, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 444
    .end local v0    # "cnt":Ljava/lang/Integer;
    :cond_3
    new-instance v0, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-direct {v0, v10}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_2
.end method

.method private sortRptGroups(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/PhrasePositions;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "rgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [[Lorg/apache/lucene/search/PhrasePositions;

    iput-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    .line 358
    new-instance v0, Lorg/apache/lucene/search/SloppyPhraseScorer$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/SloppyPhraseScorer$1;-><init>(Lorg/apache/lucene/search/SloppyPhraseScorer;)V

    .line 364
    .local v0, "cmprtr":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/search/PhrasePositions;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    array-length v4, v4

    if-lt v1, v4, :cond_0

    .line 372
    return-void

    .line 365
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    const/4 v5, 0x0

    new-array v5, v5, [Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/search/PhrasePositions;

    .line 366
    .local v3, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    invoke-static {v3, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 367
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    aput-object v3, v4, v1

    .line 368
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v4, v3

    if-lt v2, v4, :cond_1

    .line 364
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 369
    :cond_1
    aget-object v4, v3, v2

    iput v2, v4, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    .line 368
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private termGroups(Ljava/util/LinkedHashMap;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/util/OpenBitSet;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "tord":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    .local p2, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 506
    .local v4, "tg":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Lorg/apache/lucene/index/Term;

    invoke-interface {v5, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 507
    .local v3, "t":[Lorg/apache/lucene/index/Term;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_0

    .line 514
    return-object v4

    .line 508
    :cond_0
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v5}, Lorg/apache/lucene/util/OpenBitSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 510
    .local v0, "bits":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v2

    .local v2, "ord":I
    const v5, 0x7fffffff

    if-ne v2, v5, :cond_1

    .line 507
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 511
    :cond_1
    aget-object v5, v3, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private final tpPos(Lorg/apache/lucene/search/PhrasePositions;)I
    .locals 2
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 434
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    add-int/2addr v0, v1

    return v0
.end method

.method private unionTermGroups(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/util/OpenBitSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 488
    .local p1, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_0

    .line 501
    return-void

    .line 489
    :cond_0
    const/4 v1, 0x1

    .line 490
    .local v1, "incr":I
    add-int/lit8 v2, v0, 0x1

    .line 491
    .local v2, "j":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 488
    add-int/2addr v0, v1

    goto :goto_0

    .line 492
    :cond_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/OpenBitSet;->intersects(Lorg/apache/lucene/util/OpenBitSet;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 493
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/OpenBitSet;->union(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 494
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 495
    const/4 v1, 0x0

    .line 496
    goto :goto_1

    .line 497
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 574
    sget-boolean v1, Lorg/apache/lucene/search/SloppyPhraseScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->docID()I

    move-result v1

    if-gt p1, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 576
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceMin(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 590
    :cond_1
    :goto_0
    return v0

    .line 580
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v1, v1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceMin(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 579
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iget v1, v1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v2, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    if-lt v1, v2, :cond_2

    .line 585
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->phraseFreq()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->sloppyFreq:F

    .line 586
    iget-object v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iget v1, v1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    add-int/lit8 p1, v1, 0x1

    .line 587
    iget v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->sloppyFreq:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 590
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 595
    iget-wide v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->cost:J

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 519
    iget v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numMatches:I

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 564
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->docScorer:Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v1, v1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    iget v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->sloppyFreq:F

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;->score(IF)F

    move-result v0

    return v0
.end method

.method sloppyFreq()F
    .locals 1

    .prologue
    .line 523
    iget v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->sloppyFreq:F

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 599
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scorer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
