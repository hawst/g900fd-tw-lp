.class public abstract Lorg/apache/lucene/search/DocIdSet;
.super Ljava/lang/Object;
.source "DocIdSet.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/DocIdSet;->$assertionsDisabled:Z

    .line 30
    new-instance v0, Lorg/apache/lucene/search/DocIdSet$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/DocIdSet$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 74
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bits()Lorg/apache/lucene/util/Bits;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public abstract iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
