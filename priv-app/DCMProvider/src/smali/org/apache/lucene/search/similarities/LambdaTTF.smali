.class public Lorg/apache/lucene/search/similarities/LambdaTTF;
.super Lorg/apache/lucene/search/similarities/Lambda;
.source "LambdaTTF.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Lambda;-><init>()V

    return-void
.end method


# virtual methods
.method public final explain(Lorg/apache/lucene/search/similarities/BasicStats;)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;

    .prologue
    .line 38
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 39
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ", computed from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/similarities/LambdaTTF;->lambda(Lorg/apache/lucene/search/similarities/BasicStats;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 42
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v2

    long-to-float v2, v2

    const-string v3, "totalTermFreq"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 41
    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 44
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v2

    long-to-float v2, v2

    const-string v3, "numberOfDocuments"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 43
    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 45
    return-object v0
.end method

.method public final lambda(Lorg/apache/lucene/search/similarities/BasicStats;)F
    .locals 5
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 33
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v0

    long-to-float v0, v0

    add-float/2addr v0, v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v2

    long-to-float v1, v2

    add-float/2addr v1, v4

    div-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "L"

    return-object v0
.end method
