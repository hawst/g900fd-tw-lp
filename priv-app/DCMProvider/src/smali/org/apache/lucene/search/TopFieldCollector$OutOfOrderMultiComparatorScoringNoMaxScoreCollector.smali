.class final Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OutOfOrderMultiComparatorScoringNoMaxScoreCollector"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 774
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 775
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 839
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 779
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->totalHits:I

    .line 780
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_6

    .line 782
    const/4 v1, 0x0

    .line 783
    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->reverseMul:[I

    aget v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v5, v5, v1

    invoke-virtual {v5, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v5

    mul-int v0, v4, v5

    .line 784
    .local v0, "c":I
    if-gez v0, :cond_1

    .line 829
    .end local v0    # "c":I
    :cond_0
    :goto_1
    return-void

    .line 787
    .restart local v0    # "c":I
    :cond_1
    if-lez v0, :cond_3

    .line 801
    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-lt v1, v4, :cond_5

    .line 806
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 807
    .local v2, "score":F
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->updateBottom(IF)V

    .line 809
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 810
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 809
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 790
    .end local v2    # "score":F
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_4

    .line 792
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->docBase:I

    add-int/2addr v4, p1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v4, v5, :cond_2

    goto :goto_1

    .line 782
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 802
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 801
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 814
    .end local v0    # "c":I
    .end local v1    # "i":I
    :cond_6
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v3, v4, -0x1

    .line 816
    .local v3, "slot":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-lt v1, v4, :cond_7

    .line 821
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 822
    .restart local v2    # "score":F
    invoke-virtual {p0, v3, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->add(IIF)V

    .line 823
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_0

    .line 824
    const/4 v1, 0x0

    :goto_5
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 825
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 824
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 817
    .end local v2    # "score":F
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 816
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 833
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 834
    invoke-super {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 835
    return-void
.end method
