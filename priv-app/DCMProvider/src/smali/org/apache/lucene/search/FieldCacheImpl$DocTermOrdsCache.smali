.class final Lorg/apache/lucene/search/FieldCacheImpl$DocTermOrdsCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DocTermOrdsCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 1396
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 1397
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1402
    new-instance v0, Lorg/apache/lucene/index/DocTermOrds;

    const/4 v1, 0x0

    iget-object v2, p2, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/index/DocTermOrds;-><init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;)V

    return-object v0
.end method
