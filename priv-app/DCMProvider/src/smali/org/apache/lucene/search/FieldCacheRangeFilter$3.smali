.class Lorg/apache/lucene/search/FieldCacheRangeFilter$3;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newByteRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)V
    .locals 8
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "$anonymous2"    # Ljava/lang/Byte;
    .param p4, "$anonymous3"    # Ljava/lang/Byte;
    .param p5, "$anonymous4"    # Z
    .param p6, "$anonymous5"    # Z

    .prologue
    .line 212
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    .line 218
    .local v7, "i":B
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeLower:Z

    if-nez v0, :cond_0

    const/16 v0, 0x7f

    if-ne v7, v0, :cond_0

    .line 219
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 237
    .end local v7    # "i":B
    :goto_0
    return-object v0

    .line 220
    .restart local v7    # "i":B
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v7    # "i":B
    :goto_1
    int-to-byte v5, v7

    .line 224
    .local v5, "inclusiveLowerPoint":B
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 225
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    .line 226
    .restart local v7    # "i":B
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeUpper:Z

    if-nez v0, :cond_3

    const/16 v0, -0x80

    if-ne v7, v0, :cond_3

    .line 227
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 220
    .end local v5    # "inclusiveLowerPoint":B
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 222
    .end local v7    # "i":B
    :cond_2
    const/16 v5, -0x80

    .restart local v5    # "inclusiveLowerPoint":B
    goto :goto_2

    .line 228
    .restart local v7    # "i":B
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v7    # "i":B
    :goto_3
    int-to-byte v6, v7

    .line 233
    .local v6, "inclusiveUpperPoint":B
    :goto_4
    if-le v5, v6, :cond_6

    .line 234
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 228
    .end local v6    # "inclusiveUpperPoint":B
    .restart local v7    # "i":B
    :cond_4
    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    .line 230
    .end local v7    # "i":B
    :cond_5
    const/16 v6, 0x7f

    .restart local v6    # "inclusiveUpperPoint":B
    goto :goto_4

    .line 236
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$ByteParser;

    const/4 v8, 0x0

    invoke-interface {v1, v2, v3, v0, v8}, Lorg/apache/lucene/search/FieldCache;->getBytes(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)Lorg/apache/lucene/search/FieldCache$Bytes;

    move-result-object v4

    .line 237
    .local v4, "values":Lorg/apache/lucene/search/FieldCache$Bytes;
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$3;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Bytes;BB)V

    goto :goto_0
.end method
