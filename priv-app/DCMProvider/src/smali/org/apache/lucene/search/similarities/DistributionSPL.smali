.class public Lorg/apache/lucene/search/similarities/DistributionSPL;
.super Lorg/apache/lucene/search/similarities/Distribution;
.source "DistributionSPL.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Distribution;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 5
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F
    .param p3, "lambda"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 35
    cmpl-float v0, p3, v4

    if-nez v0, :cond_0

    .line 36
    const p3, 0x3f7d70a4    # 0.99f

    .line 39
    :cond_0
    float-to-double v0, p3

    add-float v2, p2, v4

    div-float v2, p2, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    float-to-double v2, p3

    sub-double/2addr v0, v2

    sub-float v2, v4, p3

    float-to-double v2, v2

    div-double/2addr v0, v2

    .line 38
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    neg-double v0, v0

    double-to-float v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string v0, "SPL"

    return-object v0
.end method
