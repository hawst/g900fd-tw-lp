.class Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;
.super Lorg/apache/lucene/search/TopFieldCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OneComparatorNonScoringCollector"
.end annotation


# instance fields
.field comparator:Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field final queue:Lorg/apache/lucene/search/FieldValueHitQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;"
        }
    .end annotation
.end field

.field final reverseMul:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 2
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    const/4 v1, 0x0

    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/TopFieldCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;IZLorg/apache/lucene/search/TopFieldCollector;)V

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    .line 57
    invoke-virtual {p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getComparators()[Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    .line 58
    invoke-virtual {p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->getReverseMul()[I

    move-result-object v0

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->reverseMul:I

    .line 59
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 3
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->totalHits:I

    .line 70
    iget-boolean v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->queueFull:Z

    if-eqz v1, :cond_2

    .line 71
    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->reverseMul:I

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v2

    mul-int/2addr v1, v2

    if-gtz v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v2, v2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v1, v2, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 80
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->updateBottom(I)V

    .line 81
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v2, v2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0

    .line 84
    :cond_2
    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v0, v1, -0x1

    .line 86
    .local v0, "slot":I
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v1, v0, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 87
    const/high16 v1, 0x7fc00000    # NaNf

    invoke-virtual {p0, v0, p1, v1}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->add(IIF)V

    .line 88
    iget-boolean v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->queueFull:Z

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v2, v2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget v0, p1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->docBase:I

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/FieldValueHitQueue;->setComparator(ILorg/apache/lucene/search/FieldComparator;)V

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->queue:Lorg/apache/lucene/search/FieldValueHitQueue;

    iget-object v0, v0, Lorg/apache/lucene/search/FieldValueHitQueue;->firstComparator:Lorg/apache/lucene/search/FieldComparator;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    .line 99
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldComparator;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 104
    return-void
.end method

.method final updateBottom(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 65
    return-void
.end method
