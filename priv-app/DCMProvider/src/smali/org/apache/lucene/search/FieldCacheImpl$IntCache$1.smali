.class Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;
.super Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheImpl$IntCache;->createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentValue:I

.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheImpl$IntCache;

.field private final synthetic val$parser:Lorg/apache/lucene/search/FieldCache$IntParser;

.field private final synthetic val$reader:Lorg/apache/lucene/index/AtomicReader;

.field private final synthetic val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

.field private values:[I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl$IntCache;Lorg/apache/lucene/search/FieldCache$IntParser;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->this$1:Lorg/apache/lucene/search/FieldCacheImpl$IntCache;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$IntParser;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$reader:Lorg/apache/lucene/index/AtomicReader;

    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    .line 625
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;)V

    return-void
.end method


# virtual methods
.method protected termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 649
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$IntParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$IntParser;->termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public visitDoc(I)V
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 644
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->values:[I

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->currentValue:I

    aput v1, v0, p1

    .line 645
    return-void
.end method

.method public visitTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 631
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$IntParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$IntParser;->parseInt(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->currentValue:I

    .line 632
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->values:[I

    if-nez v0, :cond_0

    .line 637
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$reader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->values:[I

    .line 638
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$IntCache$1;->values:[I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;->set(Ljava/lang/Object;)V

    .line 640
    :cond_0
    return-void
.end method
