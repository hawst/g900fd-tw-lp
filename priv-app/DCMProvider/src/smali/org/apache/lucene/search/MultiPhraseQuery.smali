.class public Lorg/apache/lucene/search/MultiPhraseQuery;
.super Lorg/apache/lucene/search/Query;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;
    }
.end annotation


# instance fields
.field private field:Ljava/lang/String;

.field private positions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private slop:I

.field private termArrays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    .line 51
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/search/MultiPhraseQuery;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    return v0
.end method

.method private termArraysEquals(Ljava/util/List;Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;",
            "Ljava/util/List",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "termArrays1":Ljava/util/List;, "Ljava/util/List<[Lorg/apache/lucene/index/Term;>;"
    .local p2, "termArrays2":Ljava/util/List;, "Ljava/util/List<[Lorg/apache/lucene/index/Term;>;"
    const/4 v4, 0x0

    .line 390
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-eq v5, v6, :cond_0

    .line 403
    :goto_0
    return v4

    .line 393
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 394
    .local v0, "iterator1":Ljava/util/ListIterator;, "Ljava/util/ListIterator<[Lorg/apache/lucene/index/Term;>;"
    invoke-interface {p2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 395
    .local v1, "iterator2":Ljava/util/ListIterator;, "Ljava/util/ListIterator<[Lorg/apache/lucene/index/Term;>;"
    :cond_1
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 403
    const/4 v4, 0x1

    goto :goto_0

    .line 396
    :cond_2
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/lucene/index/Term;

    .line 397
    .local v2, "termArray1":[Lorg/apache/lucene/index/Term;
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 398
    .local v3, "termArray2":[Lorg/apache/lucene/index/Term;
    if-nez v2, :cond_3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_3
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v5

    .line 399
    if-nez v5, :cond_1

    goto :goto_0
.end method

.method private termArraysHashCode()I
    .locals 5

    .prologue
    .line 380
    const/4 v0, 0x1

    .line 381
    .local v0, "hashCode":I
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 385
    return v0

    .line 381
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/index/Term;

    .line 382
    .local v1, "termArray":[Lorg/apache/lucene/index/Term;
    mul-int/lit8 v4, v0, 0x1f

    .line 383
    if-nez v1, :cond_1

    const/4 v2, 0x0

    .line 382
    :goto_1
    add-int v0, v4, v2

    goto :goto_0

    .line 383
    :cond_1
    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 71
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    return-void
.end method

.method public add([Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "position":I
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 81
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 83
    :cond_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    .line 84
    return-void
.end method

.method public add([Lorg/apache/lucene/index/Term;I)V
    .locals 4
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .param p2, "position"    # I

    .prologue
    .line 92
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 93
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    .line 95
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 103
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    return-void

    .line 96
    :cond_1
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 97
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "All phrase terms must be in the same field ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 99
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    new-instance v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;-><init>(Lorg/apache/lucene/search/MultiPhraseQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 360
    instance-of v2, p1, Lorg/apache/lucene/search/MultiPhraseQuery;

    if-nez v2, :cond_1

    .line 362
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 361
    check-cast v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    .line 362
    .local v0, "other":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 363
    iget v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    iget v3, v0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    if-ne v2, v3, :cond_0

    .line 364
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/search/MultiPhraseQuery;->termArraysEquals(Ljava/util/List;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 133
    return-void

    .line 128
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/Term;

    .line 129
    .local v0, "arr":[Lorg/apache/lucene/index/Term;
    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v0, v2

    .line 130
    .local v1, "term":Lorg/apache/lucene/index/Term;
    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getPositions()[I
    .locals 3

    .prologue
    .line 119
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [I

    .line 120
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 122
    return-object v1

    .line 121
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getSlop()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    return v0
.end method

.method public getTermArrays()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 372
    iget v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    .line 371
    xor-int/2addr v0, v1

    .line 373
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->termArraysHashCode()I

    move-result v1

    .line 371
    xor-int/2addr v0, v1

    .line 374
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    .line 371
    xor-int/2addr v0, v1

    .line 375
    const v1, 0x4ac65113    # 6498441.5f

    .line 371
    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    const/4 v6, 0x1

    .line 283
    iget-object v4, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 284
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 285
    .local v1, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v4

    invoke-virtual {v1, v4}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 296
    .end local v1    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    :goto_0
    return-object v1

    .line 287
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v6, :cond_2

    .line 288
    iget-object v4, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 289
    .local v3, "terms":[Lorg/apache/lucene/index/Term;
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0, v6}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 290
    .local v0, "boq":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v4, v3

    if-lt v2, v4, :cond_1

    .line 293
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    move-object v1, v0

    .line 294
    goto :goto_0

    .line 291
    :cond_1
    new-instance v4, Lorg/apache/lucene/search/TermQuery;

    aget-object v5, v3, v2

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 290
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "boq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v2    # "i":I
    .end local v3    # "terms":[Lorg/apache/lucene/index/Term;
    :cond_2
    move-object v1, p0

    .line 296
    goto :goto_0
.end method

.method public setSlop(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 61
    iput p1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    return-void
.end method

.method public final toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "f"    # Ljava/lang/String;

    .prologue
    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 310
    :cond_0
    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string v8, ":"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :cond_1
    const-string v8, "\""

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const/4 v4, 0x0

    .line 316
    .local v4, "k":I
    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 317
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<[Lorg/apache/lucene/index/Term;>;"
    const/4 v5, -0x1

    .line 318
    .local v5, "lastPos":I
    const/4 v1, 0x1

    .line 319
    .local v1, "first":Z
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 344
    const-string v8, "\""

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    iget v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    if-eqz v8, :cond_2

    .line 347
    const-string/jumbo v8, "~"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    iget v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 351
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v8

    invoke-static {v8}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8

    .line 320
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/index/Term;

    .line 321
    .local v7, "terms":[Lorg/apache/lucene/index/Term;
    iget-object v8, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 322
    .local v6, "position":I
    if-eqz v1, :cond_5

    .line 323
    const/4 v1, 0x0

    .line 330
    :cond_4
    array-length v8, v7

    const/4 v9, 0x1

    if-le v8, v9, :cond_8

    .line 331
    const-string v8, "("

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v8, v7

    if-lt v3, v8, :cond_6

    .line 337
    const-string v8, ")"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    .end local v3    # "j":I
    :goto_2
    move v5, v6

    .line 342
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 325
    :cond_5
    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    const/4 v3, 0x1

    .restart local v3    # "j":I
    :goto_3
    sub-int v8, v6, v5

    if-ge v3, v8, :cond_4

    .line 327
    const-string v8, "? "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 333
    :cond_6
    aget-object v8, v7, v3

    invoke-virtual {v8}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    array-length v8, v7

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_7

    .line 335
    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 339
    .end local v3    # "j":I
    :cond_8
    const/4 v8, 0x0

    aget-object v8, v7, v8

    invoke-virtual {v8}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
