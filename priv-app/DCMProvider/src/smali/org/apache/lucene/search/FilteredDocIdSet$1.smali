.class Lorg/apache/lucene/search/FilteredDocIdSet$1;
.super Ljava/lang/Object;
.source "FilteredDocIdSet.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FilteredDocIdSet;->bits()Lorg/apache/lucene/util/Bits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FilteredDocIdSet;

.field private final synthetic val$bits:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FilteredDocIdSet;Lorg/apache/lucene/util/Bits;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredDocIdSet$1;->this$0:Lorg/apache/lucene/search/FilteredDocIdSet;

    iput-object p2, p0, Lorg/apache/lucene/search/FilteredDocIdSet$1;->val$bits:Lorg/apache/lucene/util/Bits;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Z
    .locals 1
    .param p1, "docid"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSet$1;->val$bits:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSet$1;->this$0:Lorg/apache/lucene/search/FilteredDocIdSet;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FilteredDocIdSet;->match(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSet$1;->val$bits:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v0

    return v0
.end method
