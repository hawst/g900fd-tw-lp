.class Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;
.super Lorg/apache/lucene/search/ScoringRewrite;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ScoringRewrite",
        "<",
        "Lorg/apache/lucene/search/spans/SpanOrQuery;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;->this$1:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;

    .line 150
    invoke-direct {p0}, Lorg/apache/lucene/search/ScoringRewrite;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanOrQuery;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;->addClause(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    return-void
.end method

.method protected addClause(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .locals 1
    .param p1, "topLevel"    # Lorg/apache/lucene/search/spans/SpanOrQuery;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "docCount"    # I
    .param p4, "boost"    # F
    .param p5, "states"    # Lorg/apache/lucene/index/TermContext;

    .prologue
    .line 166
    new-instance v0, Lorg/apache/lucene/search/spans/SpanTermQuery;

    invoke-direct {v0, p2}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 167
    .local v0, "q":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-virtual {v0, p4}, Lorg/apache/lucene/search/spans/SpanTermQuery;->setBoost(F)V

    .line 168
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->addClause(Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 169
    return-void
.end method

.method protected checkMaxClauseCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 159
    return-void
.end method

.method protected bridge synthetic getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;->getTopLevelQuery()Lorg/apache/lucene/search/spans/SpanOrQuery;

    move-result-object v0

    return-object v0
.end method

.method protected getTopLevelQuery()Lorg/apache/lucene/search/spans/SpanOrQuery;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Lorg/apache/lucene/search/spans/SpanOrQuery;

    const/4 v1, 0x0

    new-array v1, v1, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    return-object v0
.end method
