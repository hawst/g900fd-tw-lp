.class Lorg/apache/lucene/search/DocIdSet$1$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "DocIdSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/DocIdSet$1;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field exhausted:Z

.field final synthetic this$1:Lorg/apache/lucene/search/DocIdSet$1;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/DocIdSet$1;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->this$1:Lorg/apache/lucene/search/DocIdSet$1;

    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->exhausted:Z

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 39
    sget-boolean v0, Lorg/apache/lucene/search/DocIdSet;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->exhausted:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/search/DocIdSet;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->exhausted:Z

    .line 42
    const v0, 0x7fffffff

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 59
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->exhausted:Z

    if-eqz v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lorg/apache/lucene/search/DocIdSet;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->exhausted:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/DocIdSet$1$1;->exhausted:Z

    .line 54
    const v0, 0x7fffffff

    return v0
.end method
