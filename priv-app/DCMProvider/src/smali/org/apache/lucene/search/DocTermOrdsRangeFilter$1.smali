.class Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;
.super Lorg/apache/lucene/search/DocTermOrdsRangeFilter;
.source "DocTermOrdsRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->newBytesRefRange(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)Lorg/apache/lucene/search/DocTermOrdsRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V
    .locals 7
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "$anonymous2"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "$anonymous3"    # Z
    .param p5, "$anonymous4"    # Z

    .prologue
    .line 59
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZLorg/apache/lucene/search/DocTermOrdsRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 18
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    sget-object v2, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->field:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/search/FieldCache;->getDocTermOrds(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v7

    .line 63
    .local v7, "docTermOrds":Lorg/apache/lucene/index/SortedSetDocValues;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_1

    const-wide/16 v12, -0x1

    .line 64
    .local v12, "lowerPoint":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->upperVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_2

    const-wide/16 v14, -0x1

    .line 72
    .local v14, "upperPoint":J
    :goto_1
    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_3

    .line 73
    const-wide/16 v10, 0x0

    .line 82
    .local v10, "inclusiveLowerPoint":J
    :goto_2
    const-wide/16 v2, -0x1

    cmp-long v2, v14, v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->upperVal:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_6

    .line 83
    const-wide v8, 0x7fffffffffffffffL

    .line 92
    .local v8, "inclusiveUpperPoint":J
    :goto_3
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-ltz v2, :cond_0

    cmp-long v2, v10, v8

    if-lez v2, :cond_9

    .line 93
    :cond_0
    sget-object v3, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 98
    :goto_4
    return-object v3

    .line 63
    .end local v8    # "inclusiveUpperPoint":J
    .end local v10    # "inclusiveLowerPoint":J
    .end local v12    # "lowerPoint":J
    .end local v14    # "upperPoint":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->lowerVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v7, v2}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v12

    goto :goto_0

    .line 64
    .restart local v12    # "lowerPoint":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->upperVal:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v7, v2}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v14

    goto :goto_1

    .line 74
    .restart local v14    # "upperPoint":J
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->includeLower:Z

    if-eqz v2, :cond_4

    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-ltz v2, :cond_4

    .line 75
    move-wide v10, v12

    .line 76
    .restart local v10    # "inclusiveLowerPoint":J
    goto :goto_2

    .end local v10    # "inclusiveLowerPoint":J
    :cond_4
    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-ltz v2, :cond_5

    .line 77
    const-wide/16 v2, 0x1

    add-long v10, v12, v2

    .line 78
    .restart local v10    # "inclusiveLowerPoint":J
    goto :goto_2

    .line 79
    .end local v10    # "inclusiveLowerPoint":J
    :cond_5
    const-wide/16 v2, 0x0

    neg-long v4, v12

    const-wide/16 v16, 0x1

    sub-long v4, v4, v16

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    .restart local v10    # "inclusiveLowerPoint":J
    goto :goto_2

    .line 84
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;->includeUpper:Z

    if-eqz v2, :cond_7

    const-wide/16 v2, 0x0

    cmp-long v2, v14, v2

    if-ltz v2, :cond_7

    .line 85
    move-wide v8, v14

    .line 86
    .restart local v8    # "inclusiveUpperPoint":J
    goto :goto_3

    .end local v8    # "inclusiveUpperPoint":J
    :cond_7
    const-wide/16 v2, 0x0

    cmp-long v2, v14, v2

    if-ltz v2, :cond_8

    .line 87
    const-wide/16 v2, 0x1

    sub-long v8, v14, v2

    .line 88
    .restart local v8    # "inclusiveUpperPoint":J
    goto :goto_3

    .line 89
    .end local v8    # "inclusiveUpperPoint":J
    :cond_8
    neg-long v2, v14

    const-wide/16 v4, 0x2

    sub-long v8, v2, v4

    .restart local v8    # "inclusiveUpperPoint":J
    goto :goto_3

    .line 96
    :cond_9
    sget-boolean v2, Lorg/apache/lucene/search/DocTermOrdsRangeFilter;->$assertionsDisabled:Z

    if-nez v2, :cond_b

    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-ltz v2, :cond_a

    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-gez v2, :cond_b

    :cond_a
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 98
    :cond_b
    new-instance v3, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v5

    move-object/from16 v4, p0

    move-object/from16 v6, p2

    invoke-direct/range {v3 .. v11}, Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1$1;-><init>(Lorg/apache/lucene/search/DocTermOrdsRangeFilter$1;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedSetDocValues;JJ)V

    goto :goto_4
.end method
