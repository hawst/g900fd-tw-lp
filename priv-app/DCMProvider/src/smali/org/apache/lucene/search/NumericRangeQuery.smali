.class public final Lorg/apache/lucene/search/NumericRangeQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "NumericRangeQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Number;",
        ">",
        "Lorg/apache/lucene/search/MultiTermQuery;"
    }
.end annotation


# static fields
.field static final INT_NEGATIVE_INFINITY:I

.field static final INT_POSITIVE_INFINITY:I

.field static final LONG_NEGATIVE_INFINITY:J

.field static final LONG_POSITIVE_INFINITY:J


# instance fields
.field final dataType:Lorg/apache/lucene/document/FieldType$NumericType;

.field final max:Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final maxInclusive:Z

.field final min:Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final minInclusive:Z

.field final precisionStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 372
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    invoke-static {v0, v1}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v0

    .line 371
    sput-wide v0, Lorg/apache/lucene/search/NumericRangeQuery;->LONG_NEGATIVE_INFINITY:J

    .line 374
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    invoke-static {v0, v1}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v0

    .line 373
    sput-wide v0, Lorg/apache/lucene/search/NumericRangeQuery;->LONG_POSITIVE_INFINITY:J

    .line 376
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v0

    .line 375
    sput v0, Lorg/apache/lucene/search/NumericRangeQuery;->INT_NEGATIVE_INFINITY:I

    .line 378
    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v0

    .line 377
    sput v0, Lorg/apache/lucene/search/NumericRangeQuery;->INT_POSITIVE_INFINITY:I

    .line 378
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "precisionStep"    # I
    .param p3, "dataType"    # Lorg/apache/lucene/document/FieldType$NumericType;
    .param p6, "minInclusive"    # Z
    .param p7, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lorg/apache/lucene/document/FieldType$NumericType;",
            "TT;TT;ZZ)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    .local p4, "min":Ljava/lang/Number;, "TT;"
    .local p5, "max":Ljava/lang/Number;, "TT;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 175
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "precisionStep must be >=1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    iput p2, p0, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    .line 178
    iput-object p3, p0, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 179
    iput-object p4, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    .line 180
    iput-object p5, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    .line 181
    iput-boolean p6, p0, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    .line 182
    iput-boolean p7, p0, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    .line 183
    return-void
.end method

.method public static newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Double;
    .param p3, "max"    # Ljava/lang/Double;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newDoubleRange(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Double;
    .param p2, "max"    # Ljava/lang/Double;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    const/4 v2, 0x4

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newFloatRange(Ljava/lang/String;ILjava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Float;
    .param p3, "max"    # Ljava/lang/Float;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newFloatRange(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Float;
    .param p2, "max"    # Ljava/lang/Float;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    const/4 v2, 0x4

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newIntRange(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Integer;
    .param p3, "max"    # Ljava/lang/Integer;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Integer;
    .param p2, "max"    # Ljava/lang/Integer;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    const/4 v2, 0x4

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newLongRange(Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Long;
    .param p3, "max"    # Ljava/lang/Long;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method

.method public static newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;
    .locals 8
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Long;
    .param p2, "max"    # Ljava/lang/Long;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery;

    const/4 v2, 0x4

    sget-object v3, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/NumericRangeQuery;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/FieldType$NumericType;Ljava/lang/Number;Ljava/lang/Number;ZZ)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 337
    if-ne p1, p0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return v1

    .line 338
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 339
    goto :goto_0

    .line 340
    :cond_2
    instance-of v3, p1, Lorg/apache/lucene/search/NumericRangeQuery;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 341
    check-cast v0, Lorg/apache/lucene/search/NumericRangeQuery;

    .line 343
    .local v0, "q":Lorg/apache/lucene/search/NumericRangeQuery;
    iget-object v3, v0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v3, :cond_3

    .line 344
    :goto_1
    iget-object v3, v0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v3, :cond_3

    .line 345
    :goto_2
    iget-boolean v3, p0, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    if-ne v3, v4, :cond_3

    .line 346
    iget-boolean v3, p0, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    if-ne v3, v4, :cond_3

    .line 347
    iget v3, p0, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    iget v4, v0, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    .line 342
    goto :goto_0

    .line 343
    :cond_4
    iget-object v3, v0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    iget-object v4, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    .line 344
    :cond_5
    iget-object v3, v0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    iget-object v4, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    .end local v0    # "q":Lorg/apache/lucene/search/NumericRangeQuery;
    :cond_6
    move v1, v2

    .line 350
    goto :goto_0
.end method

.method public getMax()Ljava/lang/Number;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 316
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    return-object v0
.end method

.method public getMin()Ljava/lang/Number;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 313
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    return-object v0
.end method

.method public getPrecisionStep()I
    .locals 1

    .prologue
    .line 319
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    iget v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    return v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 2
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Comparable;

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_0

    .line 301
    sget-object v0, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    .line 303
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;Lorg/apache/lucene/index/TermsEnum;)V

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    const v4, 0x733fa5fe

    const v3, 0x14fa55fb

    .line 355
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v0

    .line 356
    .local v0, "hash":I
    iget v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    const v2, 0x64365465

    xor-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 357
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 358
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 360
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    xor-int/2addr v1, v3

    .line 359
    add-int/2addr v1, v0

    .line 361
    iget-boolean v2, p0, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    xor-int/2addr v2, v4

    .line 359
    add-int/2addr v1, v2

    return v1
.end method

.method public includesMax()Z
    .locals 1

    .prologue
    .line 310
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    return v0
.end method

.method public includesMin()Z
    .locals 1

    .prologue
    .line 307
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 323
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 324
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/search/NumericRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/NumericRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 325
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x5b

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 326
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v1, :cond_2

    const-string v1, "*"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 327
    const-string v2, " TO "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 328
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v1, :cond_3

    const-string v1, "*"

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 329
    iget-boolean v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x5d

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 330
    invoke-virtual {p0}, Lorg/apache/lucene/search/NumericRangeQuery;->getBoost()F

    move-result v2

    invoke-static {v2}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 331
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 325
    return-object v1

    :cond_1
    const/16 v1, 0x7b

    goto :goto_0

    .line 326
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 328
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 329
    :cond_4
    const/16 v1, 0x7d

    goto :goto_3
.end method
