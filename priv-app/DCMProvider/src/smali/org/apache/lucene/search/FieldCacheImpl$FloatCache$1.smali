.class Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;
.super Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;->createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentValue:F

.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;

.field private final synthetic val$parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

.field private final synthetic val$reader:Lorg/apache/lucene/index/AtomicReader;

.field private final synthetic val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

.field private values:[F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;Lorg/apache/lucene/search/FieldCache$FloatParser;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->this$1:Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$reader:Lorg/apache/lucene/index/AtomicReader;

    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    .line 807
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;)V

    return-void
.end method


# virtual methods
.method protected termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 831
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$FloatParser;->termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public visitDoc(I)V
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 826
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->values:[F

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->currentValue:F

    aput v1, v0, p1

    .line 827
    return-void
.end method

.method public visitTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 813
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$FloatParser;->parseFloat(Lorg/apache/lucene/util/BytesRef;)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->currentValue:F

    .line 814
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->values:[F

    if-nez v0, :cond_0

    .line 819
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$reader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v0

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->values:[F

    .line 820
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->val$valuesRef:Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache$1;->values:[F

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl$HoldsOneThing;->set(Ljava/lang/Object;)V

    .line 822
    :cond_0
    return-void
.end method
