.class Lorg/apache/lucene/search/TopTermsRewrite$1;
.super Ljava/lang/Object;
.source "TopTermsRewrite.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopTermsRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    check-cast p2, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/TopTermsRewrite$1;->compare(Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;)I
    .locals 3

    .prologue
    .line 188
    .local p1, "st1":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    .local p2, "st2":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    sget-boolean v0, Lorg/apache/lucene/search/TopTermsRewrite;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termComp:Ljava/util/Comparator;

    iget-object v1, p2, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termComp:Ljava/util/Comparator;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    .line 189
    const-string v1, "term comparator should not change between segments"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 190
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termComp:Ljava/util/Comparator;

    iget-object v1, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p2, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
