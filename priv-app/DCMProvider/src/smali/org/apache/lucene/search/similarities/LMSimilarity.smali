.class public abstract Lorg/apache/lucene/search/similarities/LMSimilarity;
.super Lorg/apache/lucene/search/similarities/SimilarityBase;
.source "LMSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;,
        Lorg/apache/lucene/search/similarities/LMSimilarity$DefaultCollectionModel;,
        Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;
    }
.end annotation


# instance fields
.field protected final collectionModel:Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lorg/apache/lucene/search/similarities/LMSimilarity$DefaultCollectionModel;

    invoke-direct {v0}, Lorg/apache/lucene/search/similarities/LMSimilarity$DefaultCollectionModel;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/similarities/LMSimilarity;-><init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;)V
    .locals 0
    .param p1, "collectionModel"    # Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/SimilarityBase;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/LMSimilarity;->collectionModel:Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;

    .line 48
    return-void
.end method


# virtual methods
.method protected explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V
    .locals 3
    .param p1, "expl"    # Lorg/apache/lucene/search/Explanation;
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "doc"    # I
    .param p4, "freq"    # F
    .param p5, "docLen"    # F

    .prologue
    .line 74
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/LMSimilarity;->collectionModel:Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;

    invoke-interface {v1, p2}, Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;->computeProbability(Lorg/apache/lucene/search/similarities/BasicStats;)F

    move-result v1

    .line 75
    const-string v2, "collection probability"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 74
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 76
    return-void
.end method

.method protected fillBasicStats(Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)V
    .locals 2
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->fillBasicStats(Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)V

    move-object v0, p1

    .line 67
    check-cast v0, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;

    .line 68
    .local v0, "lmStats":Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/LMSimilarity;->collectionModel:Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;

    invoke-interface {v1, p1}, Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;->computeProbability(Lorg/apache/lucene/search/similarities/BasicStats;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;->setCollectionProbability(F)V

    .line 69
    return-void
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method protected newStats(Ljava/lang/String;F)Lorg/apache/lucene/search/similarities/BasicStats;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryBoost"    # F

    .prologue
    .line 57
    new-instance v0, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/search/similarities/LMSimilarity$LMStats;-><init>(Ljava/lang/String;F)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 94
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/LMSimilarity;->collectionModel:Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;

    invoke-interface {v1}, Lorg/apache/lucene/search/similarities/LMSimilarity$CollectionModel;->getName()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "coll":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 96
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "LM %s - %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/lucene/search/similarities/LMSimilarity;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 98
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "LM %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/lucene/search/similarities/LMSimilarity;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
