.class public Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;
.super Lorg/apache/lucene/search/Weight;
.source "BooleanQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "BooleanWeight"
.end annotation


# instance fields
.field private final disableCoord:Z

.field protected maxCoord:I

.field protected similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanQuery;

.field protected weights:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Weight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/IndexSearcher;Z)V
    .locals 5
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    .line 176
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 178
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 179
    iput-boolean p3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    .line 180
    new-instance v3, Ljava/util/ArrayList;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    .line 181
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 187
    return-void

    .line 182
    :cond_0
    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 183
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v3

    invoke-virtual {v3, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v2

    .line 184
    .local v2, "w":Lorg/apache/lucene/search/Weight;
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v3

    if-nez v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    .line 181
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public coord(II)F
    .locals 1
    .param p1, "overlap"    # I
    .param p2, "maxOverlap"    # I

    .prologue
    .line 213
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/similarities/Similarity;->coord(II)F

    move-result v0

    goto :goto_0
.end method

.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 22
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v11

    .line 230
    .local v11, "minShouldMatch":I
    new-instance v16, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct/range {v16 .. v16}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 231
    .local v16, "sumExpl":Lorg/apache/lucene/search/ComplexExplanation;
    const-string v19, "sum of:"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 232
    const/4 v7, 0x0

    .line 233
    .local v7, "coord":I
    const/4 v15, 0x0

    .line 234
    .local v15, "sum":F
    const/4 v10, 0x0

    .line 235
    .local v10, "fail":Z
    const/4 v14, 0x0

    .line 236
    .local v14, "shouldMatchCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    move-object/from16 v19, v0

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 237
    .local v6, "cIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/BooleanClause;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "wIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/Weight;>;"
    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_2

    .line 270
    if-eqz v10, :cond_6

    .line 271
    sget-object v19, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 272
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 274
    const-string v19, "Failure to meet condition(s) of required/prohibited clause(s)"

    .line 273
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 297
    .end local v16    # "sumExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_1
    :goto_1
    return-object v16

    .line 238
    .restart local v16    # "sumExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/search/Weight;

    .line 239
    .local v17, "w":Lorg/apache/lucene/search/Weight;
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/BooleanClause;

    .line 240
    .local v5, "c":Lorg/apache/lucene/search/BooleanClause;
    const/16 v19, 0x1

    const/16 v20, 0x1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v19

    if-nez v19, :cond_3

    .line 241
    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 242
    const/4 v10, 0x1

    .line 243
    new-instance v12, Lorg/apache/lucene/search/Explanation;

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "no match on required clause ("

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v12, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 244
    .local v12, "r":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    goto :goto_0

    .line 248
    .end local v12    # "r":Lorg/apache/lucene/search/Explanation;
    :cond_3
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v9

    .line 249
    .local v9, "e":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v19

    if-eqz v19, :cond_5

    .line 250
    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v19

    if-nez v19, :cond_4

    .line 251
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 252
    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v19

    add-float v15, v15, v19

    .line 253
    add-int/lit8 v7, v7, 0x1

    .line 261
    :goto_2
    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v19

    sget-object v20, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 262
    add-int/lit8 v14, v14, 0x1

    .line 263
    goto/16 :goto_0

    .line 256
    :cond_4
    new-instance v12, Lorg/apache/lucene/search/Explanation;

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "match on prohibited clause ("

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v12, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 257
    .restart local v12    # "r":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v12, v9}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 258
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 259
    const/4 v10, 0x1

    goto :goto_2

    .line 263
    .end local v12    # "r":Lorg/apache/lucene/search/Explanation;
    :cond_5
    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 264
    new-instance v12, Lorg/apache/lucene/search/Explanation;

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "no match on required clause ("

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v12, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 265
    .restart local v12    # "r":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v12, v9}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 266
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 267
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 276
    .end local v5    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v9    # "e":Lorg/apache/lucene/search/Explanation;
    .end local v12    # "r":Lorg/apache/lucene/search/Explanation;
    .end local v17    # "w":Lorg/apache/lucene/search/Weight;
    :cond_6
    if-ge v14, v11, :cond_7

    .line 277
    sget-object v19, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 278
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 279
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Failure to match minimum number of optional clauses: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 280
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 279
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 284
    :cond_7
    if-lez v7, :cond_8

    sget-object v19, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_3
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 285
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 287
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    move/from16 v19, v0

    if-eqz v19, :cond_9

    const/high16 v8, 0x3f800000    # 1.0f

    .line 288
    .local v8, "coordFactor":F
    :goto_4
    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v19, v8, v19

    if-eqz v19, :cond_1

    .line 291
    new-instance v13, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/ComplexExplanation;->isMatch()Z

    move-result v19

    .line 292
    mul-float v20, v15, v8

    .line 293
    const-string v21, "product of:"

    .line 291
    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 294
    .local v13, "result":Lorg/apache/lucene/search/ComplexExplanation;
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 295
    new-instance v19, Lorg/apache/lucene/search/Explanation;

    .line 296
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "coord("

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 295
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object/from16 v16, v13

    .line 297
    goto/16 :goto_1

    .line 284
    .end local v8    # "coordFactor":F
    .end local v13    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_8
    sget-object v19, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_3

    .line 287
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->coord(II)F

    move-result v8

    goto :goto_4
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    const/4 v2, 0x0

    .line 195
    .local v2, "sum":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 203
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v4

    mul-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 205
    return v2

    .line 197
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/Weight;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v1

    .line 198
    .local v1, "s":F
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/BooleanClause;

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v3

    if-nez v3, :cond_1

    .line 200
    add-float/2addr v2, v1

    .line 195
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public normalize(FF)V
    .locals 3
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 218
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v1

    mul-float/2addr p2, v1

    .line 219
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 223
    return-void

    .line 219
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Weight;

    .line 221
    .local v0, "w":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    goto :goto_0
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 23
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v13, "required":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 307
    .local v8, "prohibited":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v7, "optional":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .line 309
    .local v18, "cIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/BooleanClause;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 335
    if-nez p2, :cond_5

    if-eqz p3, :cond_5

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v3, v3, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_5

    .line 336
    new-instance v3, Lorg/apache/lucene/search/BooleanScorer;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v6, v4, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/search/BooleanScorer;-><init>(Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;ZILjava/util/List;Ljava/util/List;I)V

    .line 365
    :goto_1
    return-object v3

    .line 309
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/search/Weight;

    .line 310
    .local v22, "w":Lorg/apache/lucene/search/Weight;
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/search/BooleanClause;

    .line 311
    .local v17, "c":Lorg/apache/lucene/search/BooleanClause;
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v4, v5, v2}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v21

    .line 312
    .local v21, "subScorer":Lorg/apache/lucene/search/Scorer;
    if-nez v21, :cond_2

    .line 313
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 314
    const/4 v3, 0x0

    goto :goto_1

    .line 316
    :cond_2
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 317
    move-object/from16 v0, v21

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 318
    :cond_3
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 319
    move-object/from16 v0, v21

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 321
    :cond_4
    move-object/from16 v0, v21

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 339
    .end local v17    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v21    # "subScorer":Lorg/apache/lucene/search/Scorer;
    .end local v22    # "w":Lorg/apache/lucene/search/Weight;
    :cond_5
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_6

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_6

    .line 341
    const/4 v3, 0x0

    goto :goto_1

    .line 342
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v4, v4, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    if-ge v3, v4, :cond_7

    .line 346
    const/4 v3, 0x0

    goto :goto_1

    .line 350
    :cond_7
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_9

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_9

    .line 351
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    if-eqz v3, :cond_8

    const/high16 v19, 0x3f800000    # 1.0f

    .line 352
    .local v19, "coord":F
    :goto_2
    new-instance v9, Lorg/apache/lucene/search/ConjunctionScorer;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/search/Scorer;

    invoke-interface {v13, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/search/Scorer;

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v9, v0, v3, v1}, Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;F)V

    move-object v3, v9

    goto/16 :goto_1

    .line 351
    .end local v19    # "coord":F
    :cond_8
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->coord(II)F

    move-result v19

    goto :goto_2

    .line 356
    :cond_9
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_c

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v3, v3, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_c

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_c

    .line 357
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    new-array v0, v3, [F

    move-object/from16 v19, v0

    .line 358
    .local v19, "coord":[F
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_3
    move-object/from16 v0, v19

    array-length v3, v0

    move/from16 v0, v20

    if-lt v0, v3, :cond_a

    .line 361
    new-instance v9, Lorg/apache/lucene/search/DisjunctionSumScorer;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/search/Scorer;

    invoke-interface {v7, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/search/Scorer;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v9, v0, v3, v1}, Lorg/apache/lucene/search/DisjunctionSumScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;[F)V

    move-object v3, v9

    goto/16 :goto_1

    .line 359
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    if-eqz v3, :cond_b

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_4
    aput v3, v19, v20

    .line 358
    add-int/lit8 v20, v20, 0x1

    goto :goto_3

    .line 359
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->coord(II)F

    move-result v3

    goto :goto_4

    .line 365
    .end local v19    # "coord":[F
    .end local v20    # "i":I
    :cond_c
    new-instance v9, Lorg/apache/lucene/search/BooleanScorer2;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v12, v3, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move/from16 v16, v0

    move-object/from16 v10, p0

    move-object v14, v8

    move-object v15, v7

    invoke-direct/range {v9 .. v16}, Lorg/apache/lucene/search/BooleanScorer2;-><init>(Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;ZILjava/util/List;Ljava/util/List;Ljava/util/List;I)V

    move-object v3, v9

    goto/16 :goto_1
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 3

    .prologue
    .line 370
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {v1}, Lorg/apache/lucene/search/BooleanQuery;->access$2(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 377
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 370
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 371
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372
    const/4 v1, 0x0

    goto :goto_0
.end method
