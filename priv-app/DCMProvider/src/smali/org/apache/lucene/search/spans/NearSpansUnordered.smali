.class public Lorg/apache/lucene/search/spans/NearSpansUnordered;
.super Lorg/apache/lucene/search/spans/Spans;
.source "NearSpansUnordered.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;,
        Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    }
.end annotation


# instance fields
.field private first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private firstTime:Z

.field private last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private more:Z

.field private ordered:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;",
            ">;"
        }
    .end annotation
.end field

.field private query:Lorg/apache/lucene/search/spans/SpanNearQuery;

.field private queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

.field private slop:I

.field private subSpans:[Lorg/apache/lucene/search/spans/Spans;

.field private totalLength:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V
    .locals 5
    .param p1, "query"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/spans/SpanNearQuery;",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p4, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    const/4 v4, 0x1

    .line 143
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 43
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    .line 55
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 56
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    .line 145
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 146
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getSlop()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->slop:I

    .line 148
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v1

    .line 149
    .local v1, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    new-instance v3, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    array-length v4, v1

    invoke-direct {v3, p0, v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;-><init>(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)V

    iput-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    .line 150
    array-length v3, v1

    new-array v3, v3, [Lorg/apache/lucene/search/spans/Spans;

    iput-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    .line 151
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-lt v2, v3, :cond_0

    .line 157
    return-void

    .line 153
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    aget-object v3, v1, v2

    invoke-virtual {v3, p2, p3, p4}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v3

    invoke-direct {v0, p0, v3, v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;-><init>(Lorg/apache/lucene/search/spans/NearSpansUnordered;Lorg/apache/lucene/search/spans/Spans;I)V

    .line 154
    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$0(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v4

    aput-object v4, v3, v2

    .line 151
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/spans/NearSpansUnordered;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/spans/NearSpansUnordered;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/search/spans/NearSpansUnordered;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-void
.end method

.method static synthetic access$4(Lorg/apache/lucene/search/spans/NearSpansUnordered;Z)V
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    return-void
.end method

.method private addToList(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-static {v0, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$2(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    .line 307
    :goto_0
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 308
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$2(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    .line 309
    return-void

    .line 306
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    goto :goto_0
.end method

.method private atMatch()Z
    .locals 2

    .prologue
    .line 333
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 334
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v0

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->start()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->slop:I

    .line 333
    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private firstToLast()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-static {v0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$2(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    .line 313
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 314
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$1(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 315
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$2(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    .line 316
    return-void
.end method

.method private initList(Z)V
    .locals 3
    .param p1, "next"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 300
    :cond_0
    return-void

    .line 293
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 294
    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    if-eqz p1, :cond_2

    .line 295
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next()Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 296
    :cond_2
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_3

    .line 297
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->addToList(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    .line 292
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private listToQueue()V
    .locals 2

    .prologue
    .line 326
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->clear()V

    .line 327
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    if-nez v0, :cond_0

    .line 330
    return-void

    .line 328
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$1(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0
.end method

.method private min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object v0
.end method

.method private queueToList()V
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 320
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->top()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 323
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->addToList(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    goto :goto_0
.end method


# virtual methods
.method public cost()J
    .locals 6

    .prologue
    .line 278
    const-wide v2, 0x7fffffffffffffffL

    .line 279
    .local v2, "minCost":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 282
    return-wide v2

    .line 280
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->cost()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 254
    .local v1, "matchPayload":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    if-nez v0, :cond_0

    .line 259
    return-object v1

    .line 255
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->isPayloadAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 256
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->getPayload()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 254
    :cond_1
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$1(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0
.end method

.method public getSubSpans()[Lorg/apache/lucene/search/spans/Spans;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    .line 266
    .local v0, "pointer":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    if-nez v0, :cond_0

    .line 273
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 267
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    const/4 v1, 0x1

    goto :goto_1

    .line 270
    :cond_1
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$1(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0
.end method

.method public next()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 163
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    if-eqz v3, :cond_2

    .line 164
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->initList(Z)V

    .line 165
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->listToQueue()V

    .line 166
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    .line 175
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-nez v3, :cond_4

    .line 210
    :cond_1
    :goto_1
    return v1

    .line 167
    :cond_2
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_0

    .line 168
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 169
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0

    .line 171
    :cond_3
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    goto :goto_0

    .line 177
    :cond_4
    const/4 v0, 0x0

    .line 179
    .local v0, "queueStale":Z
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v4

    if-eq v3, v4, :cond_5

    .line 180
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queueToList()V

    .line 181
    const/4 v0, 0x1

    .line 186
    :cond_5
    :goto_2
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v4

    if-lt v3, v4, :cond_8

    .line 192
    :cond_6
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_1

    .line 196
    if-eqz v0, :cond_7

    .line 197
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->listToQueue()V

    .line 198
    const/4 v0, 0x0

    .line 201
    :cond_7
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->atMatch()Z

    move-result v3

    if-eqz v3, :cond_9

    move v1, v2

    .line 202
    goto :goto_1

    .line 187
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iget-object v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->skipTo(I)Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 188
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstToLast()V

    .line 189
    const/4 v0, 0x1

    goto :goto_2

    .line 205
    :cond_9
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next()Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 206
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_0

    .line 207
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    if-eqz v2, :cond_7

    .line 216
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->initList(Z)V

    .line 217
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_5

    .line 220
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_1

    .line 221
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->listToQueue()V

    .line 223
    :cond_1
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    .line 233
    .end local v0    # "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->atMatch()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->next()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1

    .line 218
    .restart local v0    # "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :cond_5
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->skipTo(I)Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 217
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$1(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0

    .line 226
    .end local v0    # "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :cond_6
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 227
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->updateTop()Ljava/lang/Object;

    .line 225
    :cond_7
    :goto_2
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v2

    if-lt v2, p1, :cond_6

    goto :goto_1

    .line 229
    :cond_8
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    goto :goto_2
.end method

.method public start()I
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 288
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    if-eqz v0, :cond_0

    const-string v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 288
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->doc()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->start()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->end()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "END"

    goto :goto_0
.end method
