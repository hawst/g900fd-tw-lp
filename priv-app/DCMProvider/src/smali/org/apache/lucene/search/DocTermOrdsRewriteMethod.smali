.class public final Lorg/apache/lucene/search/DocTermOrdsRewriteMethod;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "DocTermOrdsRewriteMethod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    if-ne p0, p1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 170
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 171
    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 173
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 179
    const/16 v0, 0x36d

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    new-instance v1, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;

    invoke-direct {v1, p2}, Lorg/apache/lucene/search/DocTermOrdsRewriteMethod$MultiTermQueryDocTermOrdsWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    .line 43
    .local v0, "result":Lorg/apache/lucene/search/Query;
    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 44
    return-object v0
.end method
