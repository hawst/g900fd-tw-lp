.class final Lorg/apache/lucene/search/PhraseQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "PhraseQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/PhrasePositions;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 25
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/PhrasePositions;

    check-cast p2, Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/PhraseQueue;->lessThan(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v0

    return v0
.end method

.method protected final lessThan(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Z
    .locals 4
    .param p1, "pp1"    # Lorg/apache/lucene/search/PhrasePositions;
    .param p2, "pp2"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 29
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    if-ne v2, v3, :cond_4

    .line 30
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-ne v2, v3, :cond_3

    .line 33
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    if-ne v2, v3, :cond_2

    .line 34
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->ord:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->ord:I

    if-ge v2, v3, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 34
    goto :goto_0

    .line 36
    :cond_2
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 39
    :cond_3
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 42
    :cond_4
    iget v2, p1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    iget v3, p2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
