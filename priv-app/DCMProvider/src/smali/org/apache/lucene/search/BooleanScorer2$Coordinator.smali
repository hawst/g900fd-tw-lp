.class Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
.super Ljava/lang/Object;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Coordinator"
.end annotation


# instance fields
.field final coordFactors:[F

.field nrMatchers:I

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;IZ)V
    .locals 3
    .param p2, "maxCoord"    # I
    .param p3, "disableCoord"    # Z

    .prologue
    .line 46
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanScorer2;->access$0(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanScorer2;->access$1(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [F

    iput-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 51
    return-void

    .line 49
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    if-eqz p3, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    aput v1, v2, v0

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    check-cast v1, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;

    invoke-virtual {v1, v0, p2}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->coord(II)F

    move-result v1

    goto :goto_1
.end method
