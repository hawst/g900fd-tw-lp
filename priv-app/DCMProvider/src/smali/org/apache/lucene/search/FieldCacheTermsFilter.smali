.class public Lorg/apache/lucene/search/FieldCacheTermsFilter;
.super Lorg/apache/lucene/search/Filter;
.source "FieldCacheTermsFilter.java"


# instance fields
.field private field:Ljava/lang/String;

.field private terms:[Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "terms"    # [Ljava/lang/String;

    .prologue
    .line 109
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 110
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->field:Ljava/lang/String;

    .line 111
    array-length v1, p2

    new-array v1, v1, [Lorg/apache/lucene/util/BytesRef;

    iput-object v1, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Lorg/apache/lucene/util/BytesRef;

    .line 112
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_0

    .line 114
    return-void

    .line 113
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Lorg/apache/lucene/util/BytesRef;

    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    aget-object v3, p2, v0

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    aput-object v2, v1, v0

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public varargs constructor <init>(Ljava/lang/String;[Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "terms"    # [Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 104
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 105
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->field:Ljava/lang/String;

    .line 106
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Lorg/apache/lucene/util/BytesRef;

    .line 107
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 8
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCacheTermsFilter;->getFieldCache()Lorg/apache/lucene/search/FieldCache;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->field:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/search/FieldCache;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v4

    .line 123
    .local v4, "fcsi":Lorg/apache/lucene/index/SortedDocValues;
    new-instance v5, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v0

    invoke-direct {v5, v0}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 124
    .local v5, "bits":Lorg/apache/lucene/util/FixedBitSet;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Lorg/apache/lucene/util/BytesRef;

    array-length v0, v0

    if-lt v6, v0, :cond_0

    .line 130
    new-instance v0, Lorg/apache/lucene/search/FieldCacheTermsFilter$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheTermsFilter$1;-><init>(Lorg/apache/lucene/search/FieldCacheTermsFilter;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/SortedDocValues;Lorg/apache/lucene/util/FixedBitSet;)V

    return-object v0

    .line 125
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Lorg/apache/lucene/util/BytesRef;

    aget-object v0, v0, v6

    invoke-virtual {v4, v0}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v7

    .line 126
    .local v7, "ord":I
    if-ltz v7, :cond_1

    .line 127
    invoke-virtual {v5, v7}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 124
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public getFieldCache()Lorg/apache/lucene/search/FieldCache;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    return-object v0
.end method
