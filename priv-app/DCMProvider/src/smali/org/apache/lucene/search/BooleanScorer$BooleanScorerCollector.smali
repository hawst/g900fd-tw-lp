.class final Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;
.super Lorg/apache/lucene/search/Collector;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BooleanScorerCollector"
.end annotation


# instance fields
.field private bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

.field private mask:I

.field private scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(ILorg/apache/lucene/search/BooleanScorer$BucketTable;)V
    .locals 0
    .param p1, "mask"    # I
    .param p2, "bucketTable"    # Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .prologue
    .line 68
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 69
    iput p1, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->mask:I

    .line 70
    iput-object p2, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .line 71
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 8
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .line 76
    .local v2, "table":Lorg/apache/lucene/search/BooleanScorer$BucketTable;
    and-int/lit16 v1, p1, 0x7ff

    .line 77
    .local v1, "i":I
    iget-object v3, v2, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->buckets:[Lorg/apache/lucene/search/BooleanScorer$Bucket;

    aget-object v0, v3, v1

    .line 79
    .local v0, "bucket":Lorg/apache/lucene/search/BooleanScorer$Bucket;
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    if-eq v3, p1, :cond_0

    .line 80
    iput p1, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    .line 81
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v3

    float-to-double v4, v3

    iput-wide v4, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:D

    .line 82
    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->mask:I

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    .line 83
    const/4 v3, 0x1

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    .line 85
    iget-object v3, v2, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 86
    iput-object v0, v2, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 92
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-wide v4, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:D

    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v3

    float-to-double v6, v3

    add-double/2addr v4, v6

    iput-wide v4, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:D

    .line 89
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    iget v4, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->mask:I

    or-int/2addr v3, v4

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    .line 90
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 0
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;

    .prologue
    .line 97
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 101
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 102
    return-void
.end method
