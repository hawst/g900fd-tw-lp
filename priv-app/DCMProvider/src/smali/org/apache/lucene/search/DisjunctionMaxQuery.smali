.class public Lorg/apache/lucene/search/DisjunctionMaxQuery;
.super Lorg/apache/lucene/search/Query;
.source "DisjunctionMaxQuery.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/Query;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/search/Query;",
        ">;"
    }
.end annotation


# instance fields
.field private disjuncts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;"
        }
    .end annotation
.end field

.field private tieBreakerMultiplier:F


# direct methods
.method public constructor <init>(F)V
    .locals 1
    .param p1, "tieBreakerMultiplier"    # F

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    .line 59
    iput p1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;F)V
    .locals 1
    .param p2, "tieBreakerMultiplier"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "disjuncts":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/search/Query;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    .line 68
    iput p2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    .line 69
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->add(Ljava/util/Collection;)V

    .line 70
    return-void
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/DisjunctionMaxQuery;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    return v0
.end method


# virtual methods
.method public add(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "disjuncts":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/search/Query;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 85
    return-void
.end method

.method public add(Lorg/apache/lucene/search/Query;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method public clone()Lorg/apache/lucene/search/DisjunctionMaxQuery;
    .locals 2

    .prologue
    .line 231
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    .line 232
    .local v0, "clone":Lorg/apache/lucene/search/DisjunctionMaxQuery;
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    .line 233
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->clone()Lorg/apache/lucene/search/DisjunctionMaxQuery;

    move-result-object v0

    return-object v0
.end method

.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    new-instance v0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;-><init>(Lorg/apache/lucene/search/DisjunctionMaxQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 281
    instance-of v2, p1, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    if-nez v2, :cond_1

    .line 283
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 282
    check-cast v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    .line 283
    .local v0, "other":Lorg/apache/lucene/search/DisjunctionMaxQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 284
    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    iget v3, v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 285
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    return-void

    .line 239
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    .line 240
    .local v0, "query":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    goto :goto_0
.end method

.method public getDisjuncts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTieBreakerMultiplier()F
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 294
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 293
    add-int/2addr v0, v1

    .line 295
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    .line 293
    add-int/2addr v0, v1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 205
    .local v3, "numDisjunctions":I
    const/4 v7, 0x1

    if-ne v3, v7, :cond_2

    .line 206
    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/Query;

    .line 207
    .local v6, "singleton":Lorg/apache/lucene/search/Query;
    invoke-virtual {v6, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 208
    .local v4, "result":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v7

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_1

    .line 209
    if-ne v4, v6, :cond_0

    invoke-virtual {v4}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 210
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v7

    invoke-virtual {v4}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v8

    mul-float/2addr v7, v8

    invoke-virtual {v4, v7}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 224
    .end local v4    # "result":Lorg/apache/lucene/search/Query;
    .end local v6    # "singleton":Lorg/apache/lucene/search/Query;
    :cond_1
    :goto_0
    return-object v4

    .line 214
    :cond_2
    const/4 v1, 0x0

    .line 215
    .local v1, "clone":Lorg/apache/lucene/search/DisjunctionMaxQuery;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v3, :cond_3

    .line 223
    if-eqz v1, :cond_6

    move-object v4, v1

    goto :goto_0

    .line 216
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    .line 217
    .local v0, "clause":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v5

    .line 218
    .local v5, "rewrite":Lorg/apache/lucene/search/Query;
    if-eq v5, v0, :cond_5

    .line 219
    if-nez v1, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->clone()Lorg/apache/lucene/search/DisjunctionMaxQuery;

    move-result-object v1

    .line 220
    :cond_4
    iget-object v7, v1, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v7, v2, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "clause":Lorg/apache/lucene/search/Query;
    .end local v5    # "rewrite":Lorg/apache/lucene/search/Query;
    :cond_6
    move-object v4, p0

    .line 224
    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v4, "("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    iget-object v4, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 253
    .local v2, "numDisjunctions":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_2

    .line 263
    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    iget v4, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_0

    .line 265
    const-string/jumbo v4, "~"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    iget v4, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 268
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_1

    .line 269
    const-string v4, "^"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 272
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 254
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/Query;

    .line 255
    .local v3, "subquery":Lorg/apache/lucene/search/Query;
    instance-of v4, v3, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_4

    .line 256
    const-string v4, "("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :goto_1
    add-int/lit8 v4, v2, -0x1

    if-eq v1, v4, :cond_3

    const-string v4, " | "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    :cond_4
    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
