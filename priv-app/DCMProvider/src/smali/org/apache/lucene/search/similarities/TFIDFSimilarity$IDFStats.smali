.class Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;
.super Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
.source "TFIDFSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IDFStats"
.end annotation


# instance fields
.field private final field:Ljava/lang/String;

.field private final idf:Lorg/apache/lucene/search/Explanation;

.field private final queryBoost:F

.field private queryNorm:F

.field private queryWeight:F

.field private value:F


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/Explanation;F)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "idf"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "queryBoost"    # F

    .prologue
    .line 840
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;-><init>()V

    .line 842
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->field:Ljava/lang/String;

    .line 843
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;

    .line 844
    iput p3, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryBoost:F

    .line 845
    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v0

    mul-float/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryWeight:F

    .line 846
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F
    .locals 1

    .prologue
    .line 838
    iget v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->value:F

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->field:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F
    .locals 1

    .prologue
    .line 837
    iget v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryBoost:F

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Lorg/apache/lucene/search/Explanation;
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;

    return-object v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F
    .locals 1

    .prologue
    .line 835
    iget v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryNorm:F

    return v0
.end method


# virtual methods
.method public getValueForNormalization()F
    .locals 2

    .prologue
    .line 851
    iget v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public normalize(FF)V
    .locals 2
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 856
    mul-float v0, p1, p2

    iput v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryNorm:F

    .line 857
    iget v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryWeight:F

    .line 858
    iget v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryWeight:F

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->value:F

    .line 859
    return-void
.end method
