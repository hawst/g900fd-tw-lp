.class Lorg/apache/lucene/search/FieldCacheImpl$8;
.super Lorg/apache/lucene/search/FieldCache$Doubles;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheImpl;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheImpl;

.field private final synthetic val$valuesIn:Lorg/apache/lucene/index/NumericDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$8;->this$0:Lorg/apache/lucene/search/FieldCacheImpl;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$8;->val$valuesIn:Lorg/apache/lucene/index/NumericDocValues;

    .line 972
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Doubles;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)D
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 975
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$8;->val$valuesIn:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method
