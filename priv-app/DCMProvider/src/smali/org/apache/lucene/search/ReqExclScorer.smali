.class Lorg/apache/lucene/search/ReqExclScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ReqExclScorer.java"


# instance fields
.field private doc:I

.field private exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

.field private reqScorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 1
    .param p1, "reqScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "exclDisi"    # Lorg/apache/lucene/search/DocIdSetIterator;

    .prologue
    .line 40
    iget-object v0, p1, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 43
    return-void
.end method

.method private toNonExcluded()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const v2, 0x7fffffff

    .line 73
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    .line 74
    .local v0, "exclDoc":I
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    .line 76
    .local v1, "reqDoc":I
    :cond_0
    if-ge v1, v0, :cond_1

    move v2, v1

    .line 90
    :goto_0
    return v2

    .line 78
    :cond_1
    if-le v1, v0, :cond_3

    .line 79
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    .line 80
    if-ne v0, v2, :cond_2

    .line 81
    iput-object v4, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    move v2, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_2
    if-le v0, v1, :cond_3

    move v2, v1

    .line 85
    goto :goto_0

    .line 88
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 89
    iput-object v4, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 119
    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v1, :cond_0

    .line 120
    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 129
    :goto_0
    return v0

    .line 122
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    if-nez v1, :cond_1

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 125
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 126
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 127
    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 129
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/ReqExclScorer;->toNonExcluded()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "FILTERED"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v0, :cond_0

    .line 48
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 58
    :goto_0
    return v0

    .line 50
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 51
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 53
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 55
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    if-nez v0, :cond_2

    .line 56
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 58
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/ReqExclScorer;->toNonExcluded()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    return v0
.end method
