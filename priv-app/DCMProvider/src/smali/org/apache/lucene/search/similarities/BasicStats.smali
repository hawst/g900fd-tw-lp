.class public Lorg/apache/lucene/search/similarities/BasicStats;
.super Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
.source "BasicStats.java"


# instance fields
.field protected avgFieldLength:F

.field protected docFreq:J

.field final field:Ljava/lang/String;

.field protected numberOfDocuments:J

.field protected numberOfFieldTokens:J

.field protected final queryBoost:F

.field protected topLevelBoost:F

.field protected totalBoost:F

.field protected totalTermFreq:J


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryBoost"    # F

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SimWeight;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/BasicStats;->field:Ljava/lang/String;

    .line 53
    iput p2, p0, Lorg/apache/lucene/search/similarities/BasicStats;->queryBoost:F

    .line 54
    iput p2, p0, Lorg/apache/lucene/search/similarities/BasicStats;->totalBoost:F

    .line 55
    return-void
.end method


# virtual methods
.method public getAvgFieldLength()F
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->avgFieldLength:F

    return v0
.end method

.method public getDocFreq()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->docFreq:J

    return-wide v0
.end method

.method public getNumberOfDocuments()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->numberOfDocuments:J

    return-wide v0
.end method

.method public getNumberOfFieldTokens()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->numberOfFieldTokens:J

    return-wide v0
.end method

.method public getTotalBoost()F
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->totalBoost:F

    return v0
.end method

.method public getTotalTermFreq()J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->totalTermFreq:J

    return-wide v0
.end method

.method public getValueForNormalization()F
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/search/similarities/BasicStats;->rawNormalizationValue()F

    move-result v0

    .line 122
    .local v0, "rawValue":F
    mul-float v1, v0, v0

    return v1
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "queryNorm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 138
    iput p2, p0, Lorg/apache/lucene/search/similarities/BasicStats;->topLevelBoost:F

    .line 139
    iget v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->queryBoost:F

    mul-float/2addr v0, p2

    iput v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->totalBoost:F

    .line 140
    return-void
.end method

.method protected rawNormalizationValue()F
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lorg/apache/lucene/search/similarities/BasicStats;->queryBoost:F

    return v0
.end method

.method public setAvgFieldLength(F)V
    .locals 0
    .param p1, "avgFieldLength"    # F

    .prologue
    .line 92
    iput p1, p0, Lorg/apache/lucene/search/similarities/BasicStats;->avgFieldLength:F

    .line 93
    return-void
.end method

.method public setDocFreq(J)V
    .locals 1
    .param p1, "docFreq"    # J

    .prologue
    .line 102
    iput-wide p1, p0, Lorg/apache/lucene/search/similarities/BasicStats;->docFreq:J

    .line 103
    return-void
.end method

.method public setNumberOfDocuments(J)V
    .locals 1
    .param p1, "numberOfDocuments"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lorg/apache/lucene/search/similarities/BasicStats;->numberOfDocuments:J

    .line 67
    return-void
.end method

.method public setNumberOfFieldTokens(J)V
    .locals 1
    .param p1, "numberOfFieldTokens"    # J

    .prologue
    .line 82
    iput-wide p1, p0, Lorg/apache/lucene/search/similarities/BasicStats;->numberOfFieldTokens:J

    .line 83
    return-void
.end method

.method public setTotalTermFreq(J)V
    .locals 1
    .param p1, "totalTermFreq"    # J

    .prologue
    .line 112
    iput-wide p1, p0, Lorg/apache/lucene/search/similarities/BasicStats;->totalTermFreq:J

    .line 113
    return-void
.end method
