.class public Lorg/apache/lucene/search/similarities/DefaultSimilarity;
.super Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
.source "DefaultSimilarity.java"


# instance fields
.field protected discountOverlaps:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;-><init>()V

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;->discountOverlaps:Z

    .line 27
    return-void
.end method


# virtual methods
.method public coord(II)F
    .locals 2
    .param p1, "overlap"    # I
    .param p2, "maxOverlap"    # I

    .prologue
    .line 32
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    return v0
.end method

.method public getDiscountOverlaps()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;->discountOverlaps:Z

    return v0
.end method

.method public idf(JJ)F
    .locals 5
    .param p1, "docFreq"    # J
    .param p3, "numDocs"    # J

    .prologue
    .line 80
    long-to-double v0, p3

    const-wide/16 v2, 0x1

    add-long/2addr v2, p1

    long-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public lengthNorm(Lorg/apache/lucene/index/FieldInvertState;)F
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 52
    iget-boolean v1, p0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;->discountOverlaps:Z

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getNumOverlap()I

    move-result v2

    sub-int v0, v1, v2

    .line 56
    .local v0, "numTerms":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getBoost()F

    move-result v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    return v1

    .line 55
    .end local v0    # "numTerms":I
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v0

    .restart local v0    # "numTerms":I
    goto :goto_0
.end method

.method public queryNorm(F)F
    .locals 4
    .param p1, "sumOfSquaredWeights"    # F

    .prologue
    .line 38
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public scorePayload(IIILorg/apache/lucene/util/BytesRef;)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 74
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public setDiscountOverlaps(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;->discountOverlaps:Z

    .line 100
    return-void
.end method

.method public sloppyFreq(I)F
    .locals 2
    .param p1, "distance"    # I

    .prologue
    .line 68
    const/high16 v0, 0x3f800000    # 1.0f

    add-int/lit8 v1, p1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public tf(F)F
    .locals 2
    .param p1, "freq"    # F

    .prologue
    .line 62
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const-string v0, "DefaultSimilarity"

    return-object v0
.end method
