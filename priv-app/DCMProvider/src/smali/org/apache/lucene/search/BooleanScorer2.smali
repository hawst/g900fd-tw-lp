.class Lorg/apache/lucene/search/BooleanScorer2;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/BooleanScorer2$Coordinator;,
        Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;
    }
.end annotation


# instance fields
.field private final coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

.field private final countingSumScorer:Lorg/apache/lucene/search/Scorer;

.field private doc:I

.field private final minNrShouldMatch:I

.field private final optionalScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field

.field private final prohibitedScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field

.field private final requiredScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;ZILjava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .locals 2
    .param p1, "weight"    # Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;
    .param p2, "disableCoord"    # Z
    .param p3, "minNrShouldMatch"    # I
    .param p7, "maxCoord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;",
            "ZI",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    .local p4, "required":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    .local p5, "prohibited":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    .local p6, "optional":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    .line 92
    if-gez p3, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Minimum number of optional scorers should not be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iput p3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    .line 97
    iput-object p6, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    .line 98
    iput-object p4, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    .line 99
    iput-object p5, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    .line 100
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    invoke-direct {v0, p0, p7, p2}, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;-><init>(Lorg/apache/lucene/search/BooleanScorer2;IZ)V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    .line 102
    invoke-direct {p0, p2}, Lorg/apache/lucene/search/BooleanScorer2;->makeCountingSumScorer(Z)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    .line 103
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    return-object v0
.end method

.method private addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "requiredCountingSumScorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .end local p1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :goto_0
    return-object p1

    .line 276
    .restart local p1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/ReqExclScorer;

    .line 277
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 278
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    .line 276
    :goto_1
    invoke-direct {v1, p1, v0}, Lorg/apache/lucene/search/ReqExclScorer;-><init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/DocIdSetIterator;)V

    move-object p1, v1

    goto :goto_0

    .line 279
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/MinShouldMatchSumScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/MinShouldMatchSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;)V

    goto :goto_1
.end method

.method private countingConjunctionSumScorer(ZLjava/util/List;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)",
            "Lorg/apache/lucene/search/Scorer;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "requiredScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 184
    .local v0, "requiredNrMatchers":I
    new-instance v2, Lorg/apache/lucene/search/BooleanScorer2$3;

    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/Scorer;

    invoke-interface {p2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/search/Scorer;

    invoke-direct {v2, p0, v3, v1, v0}, Lorg/apache/lucene/search/BooleanScorer2$3;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;I)V

    return-object v2
.end method

.method private countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p2, "minNrShouldMatch"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)",
            "Lorg/apache/lucene/search/Scorer;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "scorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    .line 161
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer2$1;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {v0, p0, v1, p1, p2}, Lorg/apache/lucene/search/BooleanScorer2$1;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V

    .line 170
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/lucene/search/BooleanScorer2$2;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/search/Scorer;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/Scorer;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lorg/apache/lucene/search/BooleanScorer2$2;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;[F)V

    move-object v0, v1

    goto :goto_0
.end method

.method private dualConjunctionSumScorer(ZLorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "disableCoord"    # Z
    .param p2, "req1"    # Lorg/apache/lucene/search/Scorer;
    .param p3, "req2"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    new-instance v0, Lorg/apache/lucene/search/ConjunctionScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    const/4 v2, 0x2

    new-array v2, v2, [Lorg/apache/lucene/search/Scorer;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;)V

    return-object v0
.end method

.method private makeCountingSumScorer(Z)Lorg/apache/lucene/search/Scorer;
    .locals 1
    .param p1, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 221
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/BooleanScorer2;->makeCountingSumScorerNoReq(Z)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    .line 222
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/BooleanScorer2;->makeCountingSumScorerSomeReq(Z)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    goto :goto_0
.end method

.method private makeCountingSumScorerNoReq(Z)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 227
    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    if-ge v3, v2, :cond_0

    move v0, v2

    .line 229
    .local v0, "nrOptRequired":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v0, :cond_1

    .line 230
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .line 236
    .local v1, "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :goto_1
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    return-object v2

    .line 227
    .end local v0    # "nrOptRequired":I
    .end local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    goto :goto_0

    .line 231
    .restart local v0    # "nrOptRequired":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_2

    .line 232
    new-instance v1, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    invoke-direct {v1, p0, v2}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V

    .restart local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    goto :goto_1

    .line 234
    .end local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLjava/util/List;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .restart local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    goto :goto_1
.end method

.method private makeCountingSumScorerSomeReq(Z)Lorg/apache/lucene/search/Scorer;
    .locals 8
    .param p1, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 240
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    if-ne v2, v3, :cond_0

    .line 241
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 242
    .local v0, "allReq":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 243
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLjava/util/List;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    .line 258
    .end local v0    # "allReq":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer;>;"
    :goto_0
    return-object v2

    .line 246
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 247
    new-instance v1, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    invoke-direct {v1, p0, v2}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V

    .line 249
    .local v1, "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :goto_1
    iget v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    if-lez v2, :cond_2

    .line 255
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    .line 256
    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    .line 254
    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    .line 251
    invoke-direct {p0, p1, v1, v2}, Lorg/apache/lucene/search/BooleanScorer2;->dualConjunctionSumScorer(ZLorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    .line 250
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    goto :goto_0

    .line 248
    .end local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLjava/util/List;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    goto :goto_1

    .line 258
    .restart local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_2
    new-instance v4, Lorg/apache/lucene/search/ReqOptSumScorer;

    .line 259
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 260
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v6, :cond_3

    .line 261
    new-instance v3, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    invoke-direct {v3, p0, v2}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V

    move-object v2, v3

    .line 258
    :goto_2
    invoke-direct {v4, v5, v2}, Lorg/apache/lucene/search/ReqOptSumScorer;-><init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)V

    move-object v2, v4

    goto :goto_0

    .line 263
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-direct {p0, v2, v6}, Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    goto :goto_2
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer$ChildScorer;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 342
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 345
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 348
    return-object v0

    .line 339
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 340
    .local v1, "s":Lorg/apache/lucene/search/Scorer;
    new-instance v3, Lorg/apache/lucene/search/Scorer$ChildScorer;

    const-string v4, "SHOULD"

    invoke-direct {v3, v1, v4}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 342
    .end local v1    # "s":Lorg/apache/lucene/search/Scorer;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 343
    .restart local v1    # "s":Lorg/apache/lucene/search/Scorer;
    new-instance v3, Lorg/apache/lucene/search/Scorer$ChildScorer;

    const-string v4, "MUST_NOT"

    invoke-direct {v3, v1, v4}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 345
    .end local v1    # "s":Lorg/apache/lucene/search/Scorer;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 346
    .restart local v1    # "s":Lorg/apache/lucene/search/Scorer;
    new-instance v3, Lorg/apache/lucene/search/Scorer$ChildScorer;

    const-string v4, "MUST"

    invoke-direct {v3, v1, v4}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    return v0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 317
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 318
    .local v0, "sum":F
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    iget-object v1, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    iget v2, v2, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    aget v1, v1, v2

    mul-float/2addr v1, v0

    return v1
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 288
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 291
    return-void

    .line 289
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0
.end method

.method public score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    iput p3, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    .line 296
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 297
    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    if-lt v0, p2, :cond_0

    .line 301
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 298
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 299
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    goto :goto_0

    .line 301
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
