.class public final Lorg/apache/lucene/search/BoostAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "BoostAttributeImpl.java"

# interfaces
.implements Lorg/apache/lucene/search/BoostAttribute;


# instance fields
.field private boost:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 26
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/search/BoostAttributeImpl;->boost:F

    .line 25
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 40
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/search/BoostAttributeImpl;->boost:F

    .line 41
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 1
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 45
    check-cast p1, Lorg/apache/lucene/search/BoostAttribute;

    .end local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    iget v0, p0, Lorg/apache/lucene/search/BoostAttributeImpl;->boost:F

    invoke-interface {p1, v0}, Lorg/apache/lucene/search/BoostAttribute;->setBoost(F)V

    .line 46
    return-void
.end method

.method public getBoost()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lorg/apache/lucene/search/BoostAttributeImpl;->boost:F

    return v0
.end method

.method public setBoost(F)V
    .locals 0
    .param p1, "boost"    # F

    .prologue
    .line 30
    iput p1, p0, Lorg/apache/lucene/search/BoostAttributeImpl;->boost:F

    .line 31
    return-void
.end method
