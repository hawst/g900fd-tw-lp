.class final Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
.super Ljava/lang/Object;
.source "TopTermsRewrite.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopTermsRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ScoreTerm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;",
        ">;"
    }
.end annotation


# instance fields
.field public boost:F

.field public final bytes:Lorg/apache/lucene/util/BytesRef;

.field public final termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field public final termState:Lorg/apache/lucene/index/TermContext;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;Lorg/apache/lucene/index/TermContext;)V
    .locals 1
    .param p2, "termState"    # Lorg/apache/lucene/index/TermContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Lorg/apache/lucene/index/TermContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 200
    iput-object p1, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termComp:Ljava/util/Comparator;

    .line 201
    iput-object p2, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termState:Lorg/apache/lucene/index/TermContext;

    .line 202
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->compareTo(Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;)I
    .locals 3

    .prologue
    .line 206
    .local p1, "other":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;, "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;"
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    iget v1, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->termComp:Ljava/util/Comparator;

    iget-object v1, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 209
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    iget v1, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    goto :goto_0
.end method
