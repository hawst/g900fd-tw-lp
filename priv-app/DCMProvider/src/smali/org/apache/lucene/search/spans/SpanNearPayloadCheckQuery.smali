.class public Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;
.super Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
.source "SpanNearPayloadCheckQuery.java"


# instance fields
.field protected final payloadToMatch:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Ljava/util/Collection;)V
    .locals 0
    .param p1, "match"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/spans/SpanNearQuery;",
            "Ljava/util/Collection",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "payloadToMatch":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 41
    iput-object p2, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    .line 42
    return-void
.end method


# virtual methods
.method protected acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;
    .locals 8
    .param p1, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v4

    .line 47
    .local v4, "result":Z
    if-eqz v4, :cond_5

    .line 48
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    .line 49
    .local v1, "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v5

    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    if-ne v5, v6, :cond_4

    .line 52
    const/4 v2, 0x0

    .line 53
    .local v2, "matches":I
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 62
    iget-object v5, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    if-ne v2, v5, :cond_3

    .line 64
    sget-object v5, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->YES:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    .line 72
    .end local v1    # "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    .end local v2    # "matches":I
    :goto_1
    return-object v5

    .line 53
    .restart local v1    # "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    .restart local v2    # "matches":I
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 55
    .local v0, "candBytes":[B
    iget-object v6, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 56
    .local v3, "payBytes":[B
    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 57
    add-int/lit8 v2, v2, 0x1

    .line 58
    goto :goto_0

    .line 66
    .end local v0    # "candBytes":[B
    .end local v3    # "payBytes":[B
    :cond_3
    sget-object v5, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_1

    .line 69
    .end local v2    # "matches":I
    :cond_4
    sget-object v5, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_1

    .line 72
    .end local v1    # "candidate":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    :cond_5
    sget-object v5, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_1
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->clone()Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;
    .locals 3

    .prologue
    .line 92
    new-instance v0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanNearQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Ljava/util/Collection;)V

    .line 93
    .local v0, "result":Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->setBoost(F)V

    .line 94
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    if-ne p0, p1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v1

    .line 100
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 102
    check-cast v0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;

    .line 103
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v3, v4}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 104
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 105
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 103
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 110
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    .line 111
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0x8

    ushr-int/lit8 v2, v0, 0x19

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 113
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 114
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    .line 115
    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v2, "spanPayCheck("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const-string v2, ", payloadRef: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->payloadToMatch:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 85
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearPayloadCheckQuery;->getBoost()F

    move-result v2

    invoke-static {v2}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 81
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 82
    .local v1, "bytes":[B
    invoke-static {v0, v1}, Lorg/apache/lucene/util/ToStringUtils;->byteArray(Ljava/lang/StringBuilder;[B)V

    .line 83
    const/16 v3, 0x3b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
