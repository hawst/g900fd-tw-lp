.class public final Lorg/apache/lucene/search/similarities/Normalization$NoNormalization;
.super Lorg/apache/lucene/search/similarities/Normalization;
.source "Normalization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/Normalization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoNormalization"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Normalization;-><init>()V

    return-void
.end method


# virtual methods
.method public final explain(Lorg/apache/lucene/search/similarities/BasicStats;FF)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tf"    # F
    .param p3, "len"    # F

    .prologue
    .line 71
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    const/high16 v1, 0x3f800000    # 1.0f

    const-string v2, "no normalization"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v0
.end method

.method public final tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 0
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tf"    # F
    .param p3, "len"    # F

    .prologue
    .line 66
    return p2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, ""

    return-object v0
.end method
