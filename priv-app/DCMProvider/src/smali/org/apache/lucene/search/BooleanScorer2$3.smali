.class Lorg/apache/lucene/search/BooleanScorer2$3;
.super Lorg/apache/lucene/search/ConjunctionScorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLjava/util/List;)Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private lastDocScore:F

.field private lastScoredDoc:I

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;

.field private final synthetic val$requiredNrMatchers:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;I)V
    .locals 1
    .param p2, "$anonymous0"    # Lorg/apache/lucene/search/Weight;
    .param p3, "$anonymous1"    # [Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    iput p4, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->val$requiredNrMatchers:I

    .line 184
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/Scorer;)V

    .line 185
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastScoredDoc:I

    .line 188
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastDocScore:F

    return-void
.end method


# virtual methods
.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanScorer2$3;->docID()I

    move-result v0

    .line 191
    .local v0, "doc":I
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastScoredDoc:I

    if-lt v0, v1, :cond_1

    .line 192
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastScoredDoc:I

    if-le v0, v1, :cond_0

    .line 193
    invoke-super {p0}, Lorg/apache/lucene/search/ConjunctionScorer;->score()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastDocScore:F

    .line 194
    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastScoredDoc:I

    .line 196
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    invoke-static {v1}, Lorg/apache/lucene/search/BooleanScorer2;->access$2(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    move-result-object v1

    iget v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->val$requiredNrMatchers:I

    add-int/2addr v2, v3

    iput v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 202
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$3;->lastDocScore:F

    return v1
.end method
