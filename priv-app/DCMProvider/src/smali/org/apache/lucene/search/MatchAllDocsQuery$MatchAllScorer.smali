.class Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "MatchAllDocsQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MatchAllDocsQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MatchAllScorer"
.end annotation


# instance fields
.field private doc:I

.field private final liveDocs:Lorg/apache/lucene/util/Bits;

.field private final maxDoc:I

.field final score:F

.field final synthetic this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/Weight;F)V
    .locals 1
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "w"    # Lorg/apache/lucene/search/Weight;
    .param p5, "score"    # F

    .prologue
    .line 41
    iput-object p1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    .line 42
    invoke-direct {p0, p4}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    .line 43
    iput-object p3, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 44
    iput p5, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->score:F

    .line 45
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->maxDoc:I

    .line 46
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    .line 78
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    .line 56
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->maxDoc:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->maxDoc:I

    if-ne v0, v1, :cond_1

    .line 60
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    .line 62
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    return v0

    .line 57
    :cond_2
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->score:F

    return v0
.end method
