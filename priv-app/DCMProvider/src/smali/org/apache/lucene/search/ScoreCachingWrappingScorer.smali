.class public Lorg/apache/lucene/search/ScoreCachingWrappingScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ScoreCachingWrappingScorer.java"


# instance fields
.field private curDoc:I

.field private curScore:F

.field private final scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 43
    iget-object v0, p1, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curDoc:I

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 45
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "CACHED"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 55
    .local v0, "doc":I
    iget v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curDoc:I

    if-eq v0, v1, :cond_0

    .line 56
    iget-object v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curScore:F

    .line 57
    iput v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curDoc:I

    .line 60
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curScore:F

    return v1
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 1
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    .line 81
    return-void
.end method

.method public score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 1
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v0

    return v0
.end method
