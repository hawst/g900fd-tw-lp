.class public Lorg/apache/lucene/search/TermStatistics;
.super Ljava/lang/Object;
.source "TermStatistics.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final docFreq:J

.field private final term:Lorg/apache/lucene/util/BytesRef;

.field private final totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/search/TermStatistics;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TermStatistics;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/BytesRef;JJ)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "docFreq"    # J
    .param p4, "totalTermFreq"    # J

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-boolean v0, Lorg/apache/lucene/search/TermStatistics;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/search/TermStatistics;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-eqz v0, :cond_1

    cmp-long v0, p4, p2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/search/TermStatistics;->term:Lorg/apache/lucene/util/BytesRef;

    .line 35
    iput-wide p2, p0, Lorg/apache/lucene/search/TermStatistics;->docFreq:J

    .line 36
    iput-wide p4, p0, Lorg/apache/lucene/search/TermStatistics;->totalTermFreq:J

    .line 37
    return-void
.end method


# virtual methods
.method public final docFreq()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lorg/apache/lucene/search/TermStatistics;->docFreq:J

    return-wide v0
.end method

.method public final term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/TermStatistics;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public final totalTermFreq()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lorg/apache/lucene/search/TermStatistics;->totalTermFreq:J

    return-wide v0
.end method
