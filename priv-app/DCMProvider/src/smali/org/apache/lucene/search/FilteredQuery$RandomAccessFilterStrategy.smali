.class public Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;
.super Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilteredQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RandomAccessFilterStrategy"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 513
    const-class v0, Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 513
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$FilterStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public filteredScorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSet;)Lorg/apache/lucene/search/Scorer;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p5, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 517
    invoke-virtual {p5}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 518
    .local v1, "filterIter":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v1, :cond_1

    .line 540
    :cond_0
    :goto_0
    return-object v5

    .line 523
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v2

    .line 524
    .local v2, "firstFilterDoc":I
    const v8, 0x7fffffff

    if-eq v2, v8, :cond_0

    .line 528
    invoke-virtual {p5}, Lorg/apache/lucene/search/DocIdSet;->bits()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    .line 530
    .local v0, "filterAcceptDocs":Lorg/apache/lucene/util/Bits;
    if-eqz v0, :cond_2

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;->useRandomAccess(Lorg/apache/lucene/util/Bits;I)Z

    move-result v8

    if-eqz v8, :cond_2

    move v4, v6

    .line 531
    .local v4, "useRandomAccess":Z
    :goto_1
    if-eqz v4, :cond_3

    .line 533
    invoke-virtual {p4, p1, p2, p3, v0}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    goto :goto_0

    .end local v4    # "useRandomAccess":Z
    :cond_2
    move v4, v7

    .line 530
    goto :goto_1

    .line 535
    .restart local v4    # "useRandomAccess":Z
    :cond_3
    sget-boolean v8, Lorg/apache/lucene/search/FilteredQuery$RandomAccessFilterStrategy;->$assertionsDisabled:Z

    if-nez v8, :cond_4

    const/4 v8, -0x1

    if-gt v2, v8, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 538
    :cond_4
    invoke-virtual {p4, p1, v6, v7, v5}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v3

    .line 540
    .local v3, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v3, :cond_0

    new-instance v5, Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;

    invoke-direct {v5, p4, v2, v1, v3}, Lorg/apache/lucene/search/FilteredQuery$PrimaryAdvancedLeapFrogScorer;-><init>(Lorg/apache/lucene/search/Weight;ILorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0
.end method

.method protected useRandomAccess(Lorg/apache/lucene/util/Bits;I)Z
    .locals 1
    .param p1, "bits"    # Lorg/apache/lucene/util/Bits;
    .param p2, "firstFilterDoc"    # I

    .prologue
    .line 558
    const/16 v0, 0x64

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
