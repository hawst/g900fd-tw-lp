.class abstract Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
.super Ljava/lang/Object;
.source "TermCollectingRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TermCollectingRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "TermCollector"
.end annotation


# instance fields
.field public final attributes:Lorg/apache/lucene/util/AttributeSource;

.field protected readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

.field protected topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lorg/apache/lucene/util/AttributeSource;

    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->attributes:Lorg/apache/lucene/util/AttributeSource;

    .line 85
    return-void
.end method


# virtual methods
.method public abstract collect(Lorg/apache/lucene/util/BytesRef;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setNextEnum(Lorg/apache/lucene/index/TermsEnum;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setReaderContext(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 0
    .param p1, "topReaderContext"    # Lorg/apache/lucene/index/IndexReaderContext;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;

    .prologue
    .line 91
    iput-object p2, p0, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 92
    iput-object p1, p0, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->topReaderContext:Lorg/apache/lucene/index/IndexReaderContext;

    .line 93
    return-void
.end method
