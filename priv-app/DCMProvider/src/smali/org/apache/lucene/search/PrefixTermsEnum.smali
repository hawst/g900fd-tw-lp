.class public Lorg/apache/lucene/search/PrefixTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "PrefixTermsEnum.java"


# instance fields
.field private final prefixRef:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "prefixText"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 38
    iput-object p2, p0, Lorg/apache/lucene/search/PrefixTermsEnum;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/PrefixTermsEnum;->setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/search/PrefixTermsEnum;->prefixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {p1, v0}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 46
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
