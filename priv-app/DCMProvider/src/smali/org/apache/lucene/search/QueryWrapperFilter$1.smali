.class Lorg/apache/lucene/search/QueryWrapperFilter$1;
.super Lorg/apache/lucene/search/DocIdSet;
.source "QueryWrapperFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/QueryWrapperFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/QueryWrapperFilter;

.field private final synthetic val$acceptDocs:Lorg/apache/lucene/util/Bits;

.field private final synthetic val$privateContext:Lorg/apache/lucene/index/AtomicReaderContext;

.field private final synthetic val$weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/QueryWrapperFilter;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->this$0:Lorg/apache/lucene/search/QueryWrapperFilter;

    iput-object p2, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->val$weight:Lorg/apache/lucene/search/Weight;

    iput-object p3, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->val$privateContext:Lorg/apache/lucene/index/AtomicReaderContext;

    iput-object p4, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->val$acceptDocs:Lorg/apache/lucene/util/Bits;

    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    return-void
.end method


# virtual methods
.method public isCacheable()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->val$weight:Lorg/apache/lucene/search/Weight;

    iget-object v1, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->val$privateContext:Lorg/apache/lucene/index/AtomicReaderContext;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/search/QueryWrapperFilter$1;->val$acceptDocs:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    return-object v0
.end method
