.class public abstract Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanPositionCheckQuery.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;,
        Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;
    }
.end annotation


# instance fields
.field protected match:Lorg/apache/lucene/search/spans/SpanQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 0
    .param p1, "match"    # Lorg/apache/lucene/search/spans/SpanQuery;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 43
    return-void
.end method


# virtual methods
.method protected abstract acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    .line 61
    return-void
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMatch()Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)",
            "Lorg/apache/lucene/search/spans/Spans;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    .local p3, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    new-instance v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;-><init>(Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    const/4 v0, 0x0

    .line 106
    .local v0, "clone":Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 107
    .local v1, "rewritten":Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    if-eq v1, v2, :cond_0

    .line 108
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
    check-cast v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    .line 109
    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
    iput-object v1, v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 112
    :cond_0
    if-eqz v0, :cond_1

    .line 115
    .end local v0    # "clone":Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
    :goto_0
    return-object v0

    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
    :cond_1
    move-object v0, p0

    goto :goto_0
.end method
