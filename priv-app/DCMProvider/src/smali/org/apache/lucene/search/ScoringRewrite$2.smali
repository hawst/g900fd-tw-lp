.class Lorg/apache/lucene/search/ScoringRewrite$2;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "ScoringRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ScoringRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    sget-object v2, Lorg/apache/lucene/search/ScoringRewrite;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/search/ScoringRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanQuery;

    .line 91
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->clauses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    .end local v0    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    :goto_0
    return-object v0

    .line 94
    .restart local v0    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {v1, v0}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 95
    .local v1, "result":Lorg/apache/lucene/search/Query;
    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    move-object v0, v1

    .line 96
    goto :goto_0
.end method
