.class Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;
.super Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
.source "BM25Similarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/BM25Similarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExactBM25DocScorerNoNorms"
.end annotation


# static fields
.field private static final SCORE_CACHE_SIZE:I = 0x20


# instance fields
.field private scoreCache:[F

.field private final stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

.field private final weightValue:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/similarities/BM25Similarity;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)V
    .locals 6
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    .prologue
    const/16 v5, 0x20

    .line 261
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;-><init>()V

    .line 259
    new-array v1, v5, [F

    iput-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->scoreCache:[F

    .line 262
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    .line 263
    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->weight:F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v1

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F
    invoke-static {p1}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    mul-float/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->weightValue:F

    .line 264
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 266
    return-void

    .line 265
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->scoreCache:[F

    iget v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->weightValue:F

    int-to-float v3, v0

    mul-float/2addr v2, v3

    int-to-float v3, v0

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F
    invoke-static {p1}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F

    move-result v4

    add-float/2addr v3, v4

    div-float/2addr v2, v3

    aput v2, v1, v0

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    const/4 v2, 0x0

    # invokes: Lorg/apache/lucene/search/similarities/BM25Similarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    invoke-static {v0, p1, p2, v1, v2}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$1(Lorg/apache/lucene/search/similarities/BM25Similarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public score(II)F
    .locals 3
    .param p1, "doc"    # I
    .param p2, "freq"    # I

    .prologue
    .line 271
    const/16 v0, 0x20

    if-ge p2, v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->scoreCache:[F

    aget v0, v0, p2

    .line 271
    :goto_0
    return v0

    .line 273
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->weightValue:F

    int-to-float v1, p2

    mul-float/2addr v0, v1

    int-to-float v1, p2

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$ExactBM25DocScorerNoNorms;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F
    invoke-static {v2}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F

    move-result v2

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method
