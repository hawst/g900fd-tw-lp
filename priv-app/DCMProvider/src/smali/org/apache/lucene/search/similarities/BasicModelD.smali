.class public Lorg/apache/lucene/search/similarities/BasicModelD;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelD.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 22
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 44
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v12

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    long-to-float v12, v12

    add-float v12, v12, p2

    float-to-double v4, v12

    .line 45
    .local v4, "F":D
    move/from16 v0, p2

    float-to-double v12, v0

    div-double v10, v12, v4

    .line 46
    .local v10, "phi":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v6, v12, v10

    .line 47
    .local v6, "nphi":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v14

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    long-to-double v14, v14

    div-double v8, v12, v14

    .line 48
    .local v8, "p":D
    div-double v12, v10, v8

    invoke-static {v12, v13}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v12

    mul-double/2addr v12, v10

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v14, v8

    div-double v14, v6, v14

    invoke-static {v14, v15}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v14

    mul-double/2addr v14, v6

    add-double v2, v12, v14

    .line 49
    .local v2, "D":D
    mul-double v12, v2, v4

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    const-wide v18, 0x401921fb54442d18L    # 6.283185307179586

    move/from16 v0, p2

    float-to-double v0, v0

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    mul-double v18, v18, v6

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v12, v12

    return v12
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "D"

    return-object v0
.end method
