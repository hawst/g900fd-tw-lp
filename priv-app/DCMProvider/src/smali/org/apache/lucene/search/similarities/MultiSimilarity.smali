.class public Lorg/apache/lucene/search/similarities/MultiSimilarity;
.super Lorg/apache/lucene/search/similarities/Similarity;
.source "MultiSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;,
        Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;,
        Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;
    }
.end annotation


# instance fields
.field protected final sims:[Lorg/apache/lucene/search/similarities/Similarity;


# direct methods
.method public constructor <init>([Lorg/apache/lucene/search/similarities/Similarity;)V
    .locals 0
    .param p1, "sims"    # [Lorg/apache/lucene/search/similarities/Similarity;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    .line 43
    return-void
.end method


# virtual methods
.method public computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/similarities/Similarity;->computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .locals 3
    .param p1, "queryBoost"    # F
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 52
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    array-length v2, v2

    new-array v1, v2, [Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    .line 53
    .local v1, "subStats":[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 56
    new-instance v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;-><init>([Lorg/apache/lucene/search/similarities/Similarity$SimWeight;)V

    return-object v2

    .line 54
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2, p3}, Lorg/apache/lucene/search/similarities/Similarity;->computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    move-result-object v2

    aput-object v2, v1, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    array-length v2, v2

    new-array v1, v2, [Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    .line 62
    .local v1, "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 65
    new-instance v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiExactDocScorer;-><init>([Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;)V

    return-object v2

    .line 63
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    aget-object v3, v2, v0

    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    iget-object v2, v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    aget-object v2, v2, v0

    invoke-virtual {v3, v2, p2}, Lorg/apache/lucene/search/similarities/Similarity;->exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    array-length v2, v2

    new-array v1, v2, [Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    .line 71
    .local v1, "subScorers":[Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 74
    new-instance v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiSloppyDocScorer;-><init>([Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;)V

    return-object v2

    .line 72
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/MultiSimilarity;->sims:[Lorg/apache/lucene/search/similarities/Similarity;

    aget-object v3, v2, v0

    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;

    iget-object v2, v2, Lorg/apache/lucene/search/similarities/MultiSimilarity$MultiStats;->subStats:[Lorg/apache/lucene/search/similarities/Similarity$SimWeight;

    aget-object v2, v2, v0

    invoke-virtual {v3, v2, p2}, Lorg/apache/lucene/search/similarities/Similarity;->sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
