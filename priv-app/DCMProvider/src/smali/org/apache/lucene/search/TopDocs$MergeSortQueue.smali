.class Lorg/apache/lucene/search/TopDocs$MergeSortQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "TopDocs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopDocs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MergeSortQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/TopDocs$ShardRef;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field final reverseMul:[I

.field final shardHits:[[Lorg/apache/lucene/search/ScoreDoc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    const-class v0, Lorg/apache/lucene/search/TopDocs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Sort;[Lorg/apache/lucene/search/TopDocs;)V
    .locals 11
    .param p1, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p2, "shardHits"    # [Lorg/apache/lucene/search/TopDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 128
    array-length v8, p2

    invoke-direct {p0, v8}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 129
    array-length v8, p2

    new-array v8, v8, [[Lorg/apache/lucene/search/ScoreDoc;

    iput-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    .line 130
    const/4 v5, 0x0

    .local v5, "shardIDX":I
    :goto_0
    array-length v8, p2

    if-lt v5, v8, :cond_0

    .line 149
    invoke-virtual {p1}, Lorg/apache/lucene/search/Sort;->getSort()[Lorg/apache/lucene/search/SortField;

    move-result-object v7

    .line 150
    .local v7, "sortFields":[Lorg/apache/lucene/search/SortField;
    array-length v8, v7

    new-array v8, v8, [Lorg/apache/lucene/search/FieldComparator;

    iput-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 151
    array-length v8, v7

    new-array v8, v8, [I

    iput-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->reverseMul:[I

    .line 152
    const/4 v0, 0x0

    .local v0, "compIDX":I
    :goto_1
    array-length v8, v7

    if-lt v0, v8, :cond_5

    .line 157
    return-void

    .line 131
    .end local v0    # "compIDX":I
    .end local v7    # "sortFields":[Lorg/apache/lucene/search/SortField;
    :cond_0
    aget-object v8, p2, v5

    iget-object v4, v8, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 133
    .local v4, "shard":[Lorg/apache/lucene/search/ScoreDoc;
    if-eqz v4, :cond_1

    .line 134
    iget-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    aput-object v4, v8, v5

    .line 136
    const/4 v2, 0x0

    .local v2, "hitIDX":I
    :goto_2
    array-length v8, v4

    if-lt v2, v8, :cond_2

    .line 130
    .end local v2    # "hitIDX":I
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 137
    .restart local v2    # "hitIDX":I
    :cond_2
    aget-object v3, v4, v2

    .line 138
    .local v3, "sd":Lorg/apache/lucene/search/ScoreDoc;
    instance-of v8, v3, Lorg/apache/lucene/search/FieldDoc;

    if-nez v8, :cond_3

    .line 139
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "shard "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " was not sorted by the provided Sort (expected FieldDoc but got ScoreDoc)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_3
    move-object v1, v3

    .line 141
    check-cast v1, Lorg/apache/lucene/search/FieldDoc;

    .line 142
    .local v1, "fd":Lorg/apache/lucene/search/FieldDoc;
    iget-object v8, v1, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    if-nez v8, :cond_4

    .line 143
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "shard "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " did not set sort field values (FieldDoc.fields is null); you must pass fillFields=true to IndexSearcher.search on each shard"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 136
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 153
    .end local v1    # "fd":Lorg/apache/lucene/search/FieldDoc;
    .end local v2    # "hitIDX":I
    .end local v3    # "sd":Lorg/apache/lucene/search/ScoreDoc;
    .end local v4    # "shard":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v0    # "compIDX":I
    .restart local v7    # "sortFields":[Lorg/apache/lucene/search/SortField;
    :cond_5
    aget-object v6, v7, v0

    .line 154
    .local v6, "sortField":Lorg/apache/lucene/search/SortField;
    iget-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v6, v9, v0}, Lorg/apache/lucene/search/SortField;->getComparator(II)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v10

    aput-object v10, v8, v0

    .line 155
    iget-object v10, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->reverseMul:[I

    invoke-virtual {v6}, Lorg/apache/lucene/search/SortField;->getReverse()Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, -0x1

    :goto_3
    aput v8, v10, v0

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    move v8, v9

    .line 155
    goto :goto_3
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/TopDocs$ShardRef;

    check-cast p2, Lorg/apache/lucene/search/TopDocs$ShardRef;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->lessThan(Lorg/apache/lucene/search/TopDocs$ShardRef;Lorg/apache/lucene/search/TopDocs$ShardRef;)Z

    move-result v0

    return v0
.end method

.method public lessThan(Lorg/apache/lucene/search/TopDocs$ShardRef;Lorg/apache/lucene/search/TopDocs$ShardRef;)Z
    .locals 10
    .param p1, "first"    # Lorg/apache/lucene/search/TopDocs$ShardRef;
    .param p2, "second"    # Lorg/apache/lucene/search/TopDocs$ShardRef;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 163
    sget-boolean v7, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    if-ne p1, p2, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 164
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    iget v8, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v7, v7, v8

    iget v8, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v3, v7, v8

    check-cast v3, Lorg/apache/lucene/search/FieldDoc;

    .line 165
    .local v3, "firstFD":Lorg/apache/lucene/search/FieldDoc;
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v7, v7, v8

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v4, v7, v8

    check-cast v4, Lorg/apache/lucene/search/FieldDoc;

    .line 168
    .local v4, "secondFD":Lorg/apache/lucene/search/FieldDoc;
    const/4 v2, 0x0

    .local v2, "compIDX":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v7, v7

    if-lt v2, v7, :cond_2

    .line 181
    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    if-ge v7, v8, :cond_4

    .line 192
    :cond_1
    :goto_1
    return v5

    .line 169
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v1, v7, v2

    .line 172
    .local v1, "comp":Lorg/apache/lucene/search/FieldComparator;
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->reverseMul:[I

    aget v7, v7, v2

    iget-object v8, v3, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v8, v8, v2

    iget-object v9, v4, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v9, v9, v2

    invoke-virtual {v1, v8, v9}, Lorg/apache/lucene/search/FieldComparator;->compareValues(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    mul-int v0, v7, v8

    .line 174
    .local v0, "cmp":I
    if-eqz v0, :cond_3

    .line 176
    if-ltz v0, :cond_1

    move v5, v6

    goto :goto_1

    .line 168
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "cmp":I
    .end local v1    # "comp":Lorg/apache/lucene/search/FieldComparator;
    :cond_4
    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    if-le v7, v8, :cond_5

    move v5, v6

    .line 186
    goto :goto_1

    .line 191
    :cond_5
    sget-boolean v7, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    if-ne v7, v8, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 192
    :cond_6
    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    if-lt v7, v8, :cond_1

    move v5, v6

    goto :goto_1
.end method
