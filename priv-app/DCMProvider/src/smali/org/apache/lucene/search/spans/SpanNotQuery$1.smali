.class Lorg/apache/lucene/search/spans/SpanNotQuery$1;
.super Lorg/apache/lucene/search/spans/Spans;
.source "SpanNotQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/spans/SpanNotQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private excludeSpans:Lorg/apache/lucene/search/spans/Spans;

.field private includeSpans:Lorg/apache/lucene/search/spans/Spans;

.field private moreExclude:Z

.field private moreInclude:Z

.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/spans/SpanNotQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 83
    # getter for: Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-static {p1}, Lorg/apache/lucene/search/spans/SpanNotQuery;->access$1(Lorg/apache/lucene/search/spans/SpanNotQuery;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    .line 86
    # getter for: Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-static {p1}, Lorg/apache/lucene/search/spans/SpanNotQuery;->access$2(Lorg/apache/lucene/search/spans/SpanNotQuery;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    return-void
.end method


# virtual methods
.method public cost()J
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 155
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_0
    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    .line 94
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-nez v0, :cond_2

    .line 112
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    return v0

    .line 96
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    .line 99
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_4

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 101
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    .line 99
    if-le v0, v1, :cond_5

    .line 105
    :cond_4
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 107
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 110
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    goto :goto_0

    .line 102
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    goto :goto_1
.end method

.method public skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    .line 120
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-nez v0, :cond_1

    .line 121
    const/4 v0, 0x0

    .line 138
    :goto_0
    return v0

    .line 123
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 125
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    .line 127
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    .line 127
    if-le v0, v1, :cond_5

    .line 133
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_4

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 135
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    if-gt v0, v1, :cond_6

    .line 136
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 130
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    goto :goto_1

    .line 138
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->next()Z

    move-result v0

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "spans("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanNotQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
