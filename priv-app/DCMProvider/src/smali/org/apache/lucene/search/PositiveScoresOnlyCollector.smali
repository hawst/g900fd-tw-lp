.class public Lorg/apache/lucene/search/PositiveScoresOnlyCollector;
.super Lorg/apache/lucene/search/Collector;
.source "PositiveScoresOnlyCollector.java"


# instance fields
.field private final c:Lorg/apache/lucene/search/Collector;

.field private scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Collector;)V
    .locals 0
    .param p1, "c"    # Lorg/apache/lucene/search/Collector;

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->c:Lorg/apache/lucene/search/Collector;

    .line 36
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->c:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    return v0
.end method

.method public collect(I)V
    .locals 2
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->c:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 43
    :cond_0
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->c:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 48
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;-><init>(Lorg/apache/lucene/search/Scorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->c:Lorg/apache/lucene/search/Collector;

    iget-object v1, p0, Lorg/apache/lucene/search/PositiveScoresOnlyCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 56
    return-void
.end method
