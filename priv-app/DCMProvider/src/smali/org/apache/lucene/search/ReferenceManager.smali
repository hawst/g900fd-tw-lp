.class public abstract Lorg/apache/lucene/search/ReferenceManager;
.super Ljava/lang/Object;
.source "ReferenceManager.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ReferenceManager$RefreshListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<G:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final REFERENCE_MANAGER_IS_CLOSED_MSG:Ljava/lang/String; = "this ReferenceManager is closed"


# instance fields
.field protected volatile current:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TG;"
        }
    .end annotation
.end field

.field private final refreshListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/ReferenceManager$RefreshListener;",
            ">;"
        }
    .end annotation
.end field

.field private final refreshLock:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/lucene/search/ReferenceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ReferenceManager;->$assertionsDisabled:Z

    .line 44
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    .line 50
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshListeners:Ljava/util/List;

    .line 42
    return-void
.end method

.method private doMaybeRefresh()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    iget-object v3, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 150
    const/4 v2, 0x0

    .line 152
    .local v2, "refreshed":Z
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReferenceManager;->acquire()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 154
    .local v1, "reference":Ljava/lang/Object;, "TG;"
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->notifyRefreshListenersBefore()V

    .line 155
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/ReferenceManager;->refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 156
    .local v0, "newReference":Ljava/lang/Object;, "TG;"
    if-eqz v0, :cond_1

    .line 157
    sget-boolean v3, Lorg/apache/lucene/search/ReferenceManager;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-ne v0, v1, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "refreshIfNeeded should return null if refresh wasn\'t needed"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    .end local v0    # "newReference":Ljava/lang/Object;, "TG;"
    :catchall_0
    move-exception v3

    .line 168
    :try_start_2
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    .line 169
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/ReferenceManager;->notifyRefreshListenersRefreshed(Z)V

    .line 170
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172
    .end local v1    # "reference":Ljava/lang/Object;, "TG;"
    :catchall_1
    move-exception v3

    .line 173
    iget-object v4, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 174
    throw v3

    .line 159
    .restart local v0    # "newReference":Ljava/lang/Object;, "TG;"
    .restart local v1    # "reference":Ljava/lang/Object;, "TG;"
    :cond_0
    :try_start_3
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->swapReference(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 160
    const/4 v2, 0x1

    .line 162
    if-nez v2, :cond_1

    .line 163
    :try_start_4
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 168
    :cond_1
    :try_start_5
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    .line 169
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/ReferenceManager;->notifyRefreshListenersRefreshed(Z)V

    .line 171
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReferenceManager;->afterMaybeRefresh()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 175
    return-void

    .line 161
    :catchall_2
    move-exception v3

    .line 162
    if-nez v2, :cond_2

    .line 163
    :try_start_6
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    .line 165
    :cond_2
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private ensureOpen()V
    .locals 2

    .prologue
    .line 53
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this ReferenceManager is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    return-void
.end method

.method private notifyRefreshListenersBefore()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 257
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 260
    return-void

    .line 257
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/ReferenceManager$RefreshListener;

    .line 258
    .local v0, "refreshListener":Lorg/apache/lucene/search/ReferenceManager$RefreshListener;, "Lorg/apache/lucene/search/ReferenceManager$RefreshListener;"
    invoke-interface {v0}, Lorg/apache/lucene/search/ReferenceManager$RefreshListener;->beforeRefresh()V

    goto :goto_0
.end method

.method private notifyRefreshListenersRefreshed(Z)V
    .locals 3
    .param p1, "didRefresh"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 266
    return-void

    .line 263
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/ReferenceManager$RefreshListener;

    .line 264
    .local v0, "refreshListener":Lorg/apache/lucene/search/ReferenceManager$RefreshListener;, "Lorg/apache/lucene/search/ReferenceManager$RefreshListener;"
    invoke-interface {v0, p1}, Lorg/apache/lucene/search/ReferenceManager$RefreshListener;->afterRefresh(Z)V

    goto :goto_0
.end method

.method private declared-synchronized swapReference(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    .local p1, "newReference":Ljava/lang/Object;, "TG;"
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->ensureOpen()V

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    .line 61
    .local v0, "oldReference":Ljava/lang/Object;, "TG;"
    iput-object p1, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    .line 62
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 59
    .end local v0    # "oldReference":Ljava/lang/Object;, "TG;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public final acquire()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TG;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    .local v0, "ref":Ljava/lang/Object;, "TG;"
    if-nez v0, :cond_1

    .line 97
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v2, "this ReferenceManager is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->tryIncRef(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    return-object v0
.end method

.method public addListener(Lorg/apache/lucene/search/ReferenceManager$RefreshListener;)V
    .locals 2

    .prologue
    .line 272
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    .local p1, "listener":Lorg/apache/lucene/search/ReferenceManager$RefreshListener;, "Lorg/apache/lucene/search/ReferenceManager$RefreshListener;"
    if-nez p1, :cond_0

    .line 273
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    return-void
.end method

.method protected afterClose()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    return-void
.end method

.method protected afterMaybeRefresh()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    return-void
.end method

.method public final declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->swapReference(Ljava/lang/Object;)V

    .line 131
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReferenceManager;->afterClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_0
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract decRef(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final maybeRefresh()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->ensureOpen()V

    .line 201
    iget-object v1, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    .line 202
    .local v0, "doTryRefresh":Z
    if-eqz v0, :cond_0

    .line 204
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->doMaybeRefresh()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    iget-object v1, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 210
    :cond_0
    return v0

    .line 205
    :catchall_0
    move-exception v1

    .line 206
    iget-object v2, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 207
    throw v1
.end method

.method public final maybeRefreshBlocking()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->ensureOpen()V

    .line 230
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 232
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->doMaybeRefresh()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 236
    return-void

    .line 233
    :catchall_0
    move-exception v0

    .line 234
    iget-object v1, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 235
    throw v0
.end method

.method protected abstract refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)TG;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final release(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    .local p1, "reference":Ljava/lang/Object;, "TG;"
    sget-boolean v0, Lorg/apache/lucene/search/ReferenceManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 253
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/ReferenceManager;->decRef(Ljava/lang/Object;)V

    .line 254
    return-void
.end method

.method public removeListener(Lorg/apache/lucene/search/ReferenceManager$RefreshListener;)V
    .locals 2

    .prologue
    .line 282
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    .local p1, "listener":Lorg/apache/lucene/search/ReferenceManager$RefreshListener;, "Lorg/apache/lucene/search/ReferenceManager$RefreshListener;"
    if-nez p1, :cond_0

    .line 283
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->refreshListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 286
    return-void
.end method

.method protected abstract tryIncRef(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
