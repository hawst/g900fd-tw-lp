.class public Lorg/apache/lucene/search/TermRangeQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "TermRangeQuery.java"


# instance fields
.field private includeLower:Z

.field private includeUpper:Z

.field private lowerTerm:Lorg/apache/lucene/util/BytesRef;

.field private upperTerm:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "upperTerm"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 74
    iput-object p2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    .line 75
    iput-object p3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    .line 76
    iput-boolean p4, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    .line 77
    iput-boolean p5, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    .line 78
    return-void
.end method

.method public static newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/TermRangeQuery;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerTerm"    # Ljava/lang/String;
    .param p2, "upperTerm"    # Ljava/lang/String;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z

    .prologue
    const/4 v3, 0x0

    .line 84
    if-nez p1, :cond_0

    move-object v2, v3

    .line 85
    .local v2, "lower":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-nez p2, :cond_1

    .line 86
    .local v3, "upper":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    new-instance v0, Lorg/apache/lucene/search/TermRangeQuery;

    move-object v1, p0

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-object v0

    .line 84
    .end local v2    # "lower":Lorg/apache/lucene/util/BytesRef;
    .end local v3    # "upper":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, p1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 85
    .restart local v2    # "lower":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, p2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 147
    if-ne p0, p1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v1

    .line 149
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 150
    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 152
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 153
    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    .line 154
    .local v0, "other":Lorg/apache/lucene/search/TermRangeQuery;
    iget-boolean v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 155
    goto :goto_0

    .line 156
    :cond_4
    iget-boolean v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 157
    goto :goto_0

    .line 158
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_6

    .line 159
    iget-object v3, v0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_7

    move v1, v2

    .line 160
    goto :goto_0

    .line 161
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 162
    goto :goto_0

    .line 163
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_8

    .line 164
    iget-object v3, v0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_0

    move v1, v2

    .line 165
    goto :goto_0

    .line 166
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 167
    goto :goto_0
.end method

.method public getLowerTerm()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 6
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    if-lez v0, :cond_1

    .line 104
    sget-object v1, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    .line 112
    :cond_0
    :goto_0
    return-object v1

    .line 107
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 109
    .local v1, "tenum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_0

    .line 112
    :cond_3
    new-instance v0, Lorg/apache/lucene/search/TermRangeTermsEnum;

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    iget-boolean v4, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    iget-boolean v5, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    .line 112
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    move-object v1, v0

    goto :goto_0
.end method

.method public getUpperTerm()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v5, 0x0

    .line 136
    const/16 v0, 0x1f

    .line 137
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 138
    .local v1, "result":I
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 139
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    if-eqz v6, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 140
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_2

    move v2, v5

    :goto_2
    add-int v1, v3, v2

    .line 141
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_3

    :goto_3
    add-int v1, v2, v5

    .line 142
    return v1

    :cond_0
    move v2, v4

    .line 138
    goto :goto_0

    :cond_1
    move v3, v4

    .line 139
    goto :goto_1

    .line 140
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v2

    goto :goto_2

    .line 141
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v5

    goto :goto_3
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x5b

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 126
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_3

    const-string v1, "*"

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/index/Term;->toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "\\*"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, " TO "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_5

    const-string v1, "*"

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/index/Term;->toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "\\*"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    iget-boolean v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x5d

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 124
    :cond_1
    const/16 v1, 0x7b

    goto :goto_0

    .line 126
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1}, Lorg/apache/lucene/index/Term;->toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v1, "*"

    goto :goto_1

    .line 128
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1}, Lorg/apache/lucene/index/Term;->toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_5
    const-string v1, "*"

    goto :goto_2

    .line 129
    :cond_6
    const/16 v1, 0x7d

    goto :goto_3
.end method
