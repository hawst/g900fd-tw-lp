.class Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
.source "BM25Similarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/BM25Similarity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SloppyBM25DocScorer"
.end annotation


# instance fields
.field private final cache:[F

.field private final norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

.field private final weightValue:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/similarities/BM25Similarity;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 3
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;
    .param p3, "norms"    # Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;-><init>()V

    .line 289
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    .line 290
    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->weight:F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)F

    move-result v0

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F
    invoke-static {p1}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->weightValue:F

    .line 291
    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->cache:[F
    invoke-static {p2}, Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;->access$1(Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;)[F

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->cache:[F

    .line 292
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 293
    return-void
.end method


# virtual methods
.method public computePayloadFactor(IIILorg/apache/lucene/util/BytesRef;)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/lucene/search/similarities/BM25Similarity;->scorePayload(IIILorg/apache/lucene/util/BytesRef;)F

    move-result v0

    return v0
.end method

.method public computeSlopFactor(I)F
    .locals 1
    .param p1, "distance"    # I

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/similarities/BM25Similarity;->sloppyFreq(I)F

    move-result v0

    return v0
.end method

.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 304
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->stats:Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    # invokes: Lorg/apache/lucene/search/similarities/BM25Similarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    invoke-static {v0, p1, p2, v1, v2}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$1(Lorg/apache/lucene/search/similarities/BM25Similarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BM25Similarity$BM25Stats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public score(IF)F
    .locals 4
    .param p1, "doc"    # I
    .param p2, "freq"    # F

    .prologue
    .line 298
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->this$0:Lorg/apache/lucene/search/similarities/BM25Similarity;

    # getter for: Lorg/apache/lucene/search/similarities/BM25Similarity;->k1:F
    invoke-static {v1}, Lorg/apache/lucene/search/similarities/BM25Similarity;->access$0(Lorg/apache/lucene/search/similarities/BM25Similarity;)F

    move-result v0

    .line 299
    .local v0, "norm":F
    :goto_0
    iget v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->weightValue:F

    mul-float/2addr v1, p2

    add-float v2, p2, v0

    div-float/2addr v1, v2

    return v1

    .line 298
    .end local v0    # "norm":F
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->cache:[F

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/BM25Similarity$SloppyBM25DocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-byte v2, v2

    and-int/lit16 v2, v2, 0xff

    aget v0, v1, v2

    goto :goto_0
.end method
