.class Lorg/apache/lucene/search/FieldCacheImpl$LongsFromArray;
.super Lorg/apache/lucene/search/FieldCache$Longs;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LongsFromArray"
.end annotation


# instance fields
.field private final values:[J


# direct methods
.method public constructor <init>([J)V
    .locals 0
    .param p1, "values"    # [J

    .prologue
    .line 883
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Longs;-><init>()V

    .line 884
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongsFromArray;->values:[J

    .line 885
    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 889
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$LongsFromArray;->values:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method
