.class public Lorg/apache/lucene/search/similarities/DFRSimilarity;
.super Lorg/apache/lucene/search/similarities/SimilarityBase;
.source "DFRSimilarity.java"


# instance fields
.field protected final afterEffect:Lorg/apache/lucene/search/similarities/AfterEffect;

.field protected final basicModel:Lorg/apache/lucene/search/similarities/BasicModel;

.field protected final normalization:Lorg/apache/lucene/search/similarities/Normalization;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/similarities/BasicModel;Lorg/apache/lucene/search/similarities/AfterEffect;Lorg/apache/lucene/search/similarities/Normalization;)V
    .locals 2
    .param p1, "basicModel"    # Lorg/apache/lucene/search/similarities/BasicModel;
    .param p2, "afterEffect"    # Lorg/apache/lucene/search/similarities/AfterEffect;
    .param p3, "normalization"    # Lorg/apache/lucene/search/similarities/Normalization;

    .prologue
    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/SimilarityBase;-><init>()V

    .line 99
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 100
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null parameters not allowed."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->basicModel:Lorg/apache/lucene/search/similarities/BasicModel;

    .line 103
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->afterEffect:Lorg/apache/lucene/search/similarities/AfterEffect;

    .line 104
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    .line 105
    return-void
.end method


# virtual methods
.method protected explain(Lorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/BasicStats;IFF)V
    .locals 5
    .param p1, "expl"    # Lorg/apache/lucene/search/Explanation;
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "doc"    # I
    .param p4, "freq"    # F
    .param p5, "docLen"    # F

    .prologue
    .line 117
    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 118
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v3

    const-string v4, "boost"

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 121
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    invoke-virtual {v2, p2, p4, p5}, Lorg/apache/lucene/search/similarities/Normalization;->explain(Lorg/apache/lucene/search/similarities/BasicStats;FF)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 122
    .local v0, "normExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    .line 123
    .local v1, "tfn":F
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 124
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->basicModel:Lorg/apache/lucene/search/similarities/BasicModel;

    invoke-virtual {v2, p2, v1}, Lorg/apache/lucene/search/similarities/BasicModel;->explain(Lorg/apache/lucene/search/similarities/BasicStats;F)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 125
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->afterEffect:Lorg/apache/lucene/search/similarities/AfterEffect;

    invoke-virtual {v2, p2, v1}, Lorg/apache/lucene/search/similarities/AfterEffect;->explain(Lorg/apache/lucene/search/similarities/BasicStats;F)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 126
    return-void
.end method

.method public getAfterEffect()Lorg/apache/lucene/search/similarities/AfterEffect;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->afterEffect:Lorg/apache/lucene/search/similarities/AfterEffect;

    return-object v0
.end method

.method public getBasicModel()Lorg/apache/lucene/search/similarities/BasicModel;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->basicModel:Lorg/apache/lucene/search/similarities/BasicModel;

    return-object v0
.end method

.method public getNormalization()Lorg/apache/lucene/search/similarities/Normalization;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    return-object v0
.end method

.method protected score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
    .locals 3
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "freq"    # F
    .param p3, "docLen"    # F

    .prologue
    .line 109
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    invoke-virtual {v1, p1, p2, p3}, Lorg/apache/lucene/search/similarities/Normalization;->tfn(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v0

    .line 110
    .local v0, "tfn":F
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalBoost()F

    move-result v1

    .line 111
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->basicModel:Lorg/apache/lucene/search/similarities/BasicModel;

    invoke-virtual {v2, p1, v0}, Lorg/apache/lucene/search/similarities/BasicModel;->score(Lorg/apache/lucene/search/similarities/BasicStats;F)F

    move-result v2

    .line 110
    mul-float/2addr v1, v2

    .line 111
    iget-object v2, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->afterEffect:Lorg/apache/lucene/search/similarities/AfterEffect;

    invoke-virtual {v2, p1, v0}, Lorg/apache/lucene/search/similarities/AfterEffect;->score(Lorg/apache/lucene/search/similarities/BasicStats;F)F

    move-result v2

    .line 110
    mul-float/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DFR "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->basicModel:Lorg/apache/lucene/search/similarities/BasicModel;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/BasicModel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->afterEffect:Lorg/apache/lucene/search/similarities/AfterEffect;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/AfterEffect;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/DFRSimilarity;->normalization:Lorg/apache/lucene/search/similarities/Normalization;

    invoke-virtual {v1}, Lorg/apache/lucene/search/similarities/Normalization;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
