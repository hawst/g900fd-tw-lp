.class public Lorg/apache/lucene/search/Sort;
.super Ljava/lang/Object;
.source "Sort.java"


# static fields
.field public static final INDEXORDER:Lorg/apache/lucene/search/Sort;

.field public static final RELEVANCE:Lorg/apache/lucene/search/Sort;


# instance fields
.field fields:[Lorg/apache/lucene/search/SortField;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lorg/apache/lucene/search/Sort;

    invoke-direct {v0}, Lorg/apache/lucene/search/Sort;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/Sort;->RELEVANCE:Lorg/apache/lucene/search/Sort;

    .line 111
    new-instance v0, Lorg/apache/lucene/search/Sort;

    sget-object v1, Lorg/apache/lucene/search/SortField;->FIELD_DOC:Lorg/apache/lucene/search/SortField;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    sput-object v0, Lorg/apache/lucene/search/Sort;->INDEXORDER:Lorg/apache/lucene/search/Sort;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lorg/apache/lucene/search/SortField;->FIELD_SCORE:Lorg/apache/lucene/search/SortField;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    .line 123
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/SortField;)V
    .locals 0
    .param p1, "field"    # Lorg/apache/lucene/search/SortField;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Sort;->setSort(Lorg/apache/lucene/search/SortField;)V

    .line 128
    return-void
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/search/SortField;)V
    .locals 0
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Sort;->setSort([Lorg/apache/lucene/search/SortField;)V

    .line 133
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 193
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 196
    :goto_0
    return v1

    .line 194
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/search/Sort;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 195
    check-cast v0, Lorg/apache/lucene/search/Sort;

    .line 196
    .local v0, "other":Lorg/apache/lucene/search/Sort;
    iget-object v1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    iget-object v2, v0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSort()[Lorg/apache/lucene/search/SortField;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 202
    const v0, 0x45aaf665

    iget-object v1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method needsScores()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 207
    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 212
    :goto_1
    return v1

    .line 207
    :cond_0
    aget-object v0, v3, v2

    .line 208
    .local v0, "sortField":Lorg/apache/lucene/search/SortField;
    invoke-virtual {v0}, Lorg/apache/lucene/search/SortField;->getType()Lorg/apache/lucene/search/SortField$Type;

    move-result-object v5

    sget-object v6, Lorg/apache/lucene/search/SortField$Type;->SCORE:Lorg/apache/lucene/search/SortField$Type;

    if-ne v5, v6, :cond_1

    .line 209
    const/4 v1, 0x1

    goto :goto_1

    .line 207
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public rewrite(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Sort;
    .locals 5
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 166
    .local v0, "changed":Z
    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v3, v3

    new-array v2, v3, [Lorg/apache/lucene/search/SortField;

    .line 167
    .local v2, "rewrittenSortFields":[Lorg/apache/lucene/search/SortField;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 174
    if-eqz v0, :cond_0

    new-instance p0, Lorg/apache/lucene/search/Sort;

    .end local p0    # "this":Lorg/apache/lucene/search/Sort;
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/Sort;-><init>([Lorg/apache/lucene/search/SortField;)V

    :cond_0
    return-object p0

    .line 168
    .restart local p0    # "this":Lorg/apache/lucene/search/Sort;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v3, v3, v1

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/SortField;->rewrite(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/SortField;

    move-result-object v3

    aput-object v3, v2, v1

    .line 169
    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v3, v3, v1

    aget-object v4, v2, v1

    if-eq v3, v4, :cond_2

    .line 170
    const/4 v0, 0x1

    .line 167
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setSort(Lorg/apache/lucene/search/SortField;)V
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/search/SortField;

    .prologue
    .line 137
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/search/SortField;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    .line 138
    return-void
.end method

.method public varargs setSort([Lorg/apache/lucene/search/SortField;)V
    .locals 0
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;

    .prologue
    .line 142
    iput-object p1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    .line 143
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 187
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 182
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/lucene/search/SortField;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    add-int/lit8 v2, v1, 0x1

    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 184
    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 181
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
