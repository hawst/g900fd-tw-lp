.class public Lorg/apache/lucene/search/BooleanClause;
.super Ljava/lang/Object;
.source "BooleanClause.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/BooleanClause$Occur;
    }
.end annotation


# instance fields
.field private occur:Lorg/apache/lucene/search/BooleanClause$Occur;

.field private query:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    .line 55
    iput-object p2, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 89
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/lucene/search/BooleanClause;

    if-nez v2, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 91
    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 92
    .local v0, "other":Lorg/apache/lucene/search/BooleanClause;
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 93
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v2, v3, :cond_0

    .line 92
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v2

    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v3, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    xor-int/2addr v0, v2

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v3, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x2

    :cond_0
    xor-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public isProhibited()Z
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRequired()Z
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOccur(Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 0
    .param p1, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 64
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 66
    return-void
.end method

.method public setQuery(Lorg/apache/lucene/search/Query;)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 73
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    .line 74
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause$Occur;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanClause;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
