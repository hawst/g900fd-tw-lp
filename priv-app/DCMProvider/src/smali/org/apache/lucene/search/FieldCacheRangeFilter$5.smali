.class Lorg/apache/lucene/search/FieldCacheRangeFilter$5;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newIntRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)V
    .locals 8
    .param p1, "$anonymous0"    # Ljava/lang/String;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "$anonymous2"    # Ljava/lang/Integer;
    .param p4, "$anonymous3"    # Ljava/lang/Integer;
    .param p5, "$anonymous4"    # Z
    .param p6, "$anonymous5"    # Z

    .prologue
    .line 314
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V

    .line 1
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 319
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 320
    .local v7, "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeLower:Z

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    if-ne v7, v0, :cond_0

    .line 321
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 339
    .end local v7    # "i":I
    :goto_0
    return-object v0

    .line 322
    .restart local v7    # "i":I
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeLower:Z

    if-eqz v0, :cond_1

    move v5, v7

    .line 326
    .end local v7    # "i":I
    .local v5, "inclusiveLowerPoint":I
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 327
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 328
    .restart local v7    # "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeUpper:Z

    if-nez v0, :cond_3

    const/high16 v0, -0x80000000

    if-ne v7, v0, :cond_3

    .line 329
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 322
    .end local v5    # "inclusiveLowerPoint":I
    :cond_1
    add-int/lit8 v5, v7, 0x1

    goto :goto_1

    .line 324
    .end local v7    # "i":I
    :cond_2
    const/high16 v5, -0x80000000

    .restart local v5    # "inclusiveLowerPoint":I
    goto :goto_1

    .line 330
    .restart local v7    # "i":I
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeUpper:Z

    if-eqz v0, :cond_4

    move v6, v7

    .line 335
    .end local v7    # "i":I
    .local v6, "inclusiveUpperPoint":I
    :goto_2
    if-le v5, v6, :cond_6

    .line 336
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 330
    .end local v6    # "inclusiveUpperPoint":I
    .restart local v7    # "i":I
    :cond_4
    add-int/lit8 v6, v7, -0x1

    goto :goto_2

    .line 332
    .end local v7    # "i":I
    :cond_5
    const v6, 0x7fffffff

    .restart local v6    # "inclusiveUpperPoint":I
    goto :goto_2

    .line 338
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$IntParser;

    const/4 v8, 0x0

    invoke-interface {v1, v2, v3, v0, v8}, Lorg/apache/lucene/search/FieldCache;->getInts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)Lorg/apache/lucene/search/FieldCache$Ints;

    move-result-object v4

    .line 339
    .local v4, "values":Lorg/apache/lucene/search/FieldCache$Ints;
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$5;ILorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/FieldCache$Ints;II)V

    goto :goto_0
.end method
