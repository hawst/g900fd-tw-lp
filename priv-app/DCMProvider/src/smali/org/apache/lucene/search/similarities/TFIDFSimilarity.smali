.class public abstract Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
.super Lorg/apache/lucene/search/similarities/Similarity;
.source "TFIDFSimilarity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;,
        Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;,
        Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;
    }
.end annotation


# static fields
.field private static final NORM_TABLE:[F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x100

    .line 689
    new-array v1, v3, [F

    sput-object v1, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->NORM_TABLE:[F

    .line 692
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 695
    return-void

    .line 693
    :cond_0
    sget-object v1, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->NORM_TABLE:[F

    int-to-byte v2, v0

    invoke-static {v2}, Lorg/apache/lucene/util/SmallFloat;->byte315ToFloat(B)F

    move-result v2

    aput v2, v1, v0

    .line 692
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 532
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    .locals 1

    .prologue
    .line 862
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method private explainScore(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)Lorg/apache/lucene/search/Explanation;
    .locals 10
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "stats"    # Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;
    .param p4, "norms"    # Lorg/apache/lucene/index/NumericDocValues;

    .prologue
    .line 863
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v6}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 864
    .local v6, "result":Lorg/apache/lucene/search/Explanation;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "score(doc="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",freq="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "), product of:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 867
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v4}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 868
    .local v4, "queryExpl":Lorg/apache/lucene/search/Explanation;
    const-string v8, "queryWeight, product of:"

    invoke-virtual {v4, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 870
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryBoost:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$2(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F

    move-result v8

    const-string v9, "boost"

    invoke-direct {v0, v8, v9}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 871
    .local v0, "boostExpl":Lorg/apache/lucene/search/Explanation;
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryBoost:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$2(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_0

    .line 872
    invoke-virtual {v4, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 873
    :cond_0
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$3(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 875
    new-instance v5, Lorg/apache/lucene/search/Explanation;

    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->queryNorm:F
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$4(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)F

    move-result v8

    const-string v9, "queryNorm"

    invoke-direct {v5, v8, v9}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 876
    .local v5, "queryNormExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 878
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v8

    .line 879
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$3(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    mul-float/2addr v8, v9

    .line 880
    invoke-virtual {v5}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    mul-float/2addr v8, v9

    .line 878
    invoke-virtual {v4, v8}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 882
    invoke-virtual {v6, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 885
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v1}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 886
    .local v1, "fieldExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "fieldWeight in "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 887
    const-string v9, ", product of:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 886
    invoke-virtual {v1, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 889
    new-instance v7, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v7}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 890
    .local v7, "tfExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->tf(F)F

    move-result v8

    invoke-virtual {v7, v8}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 891
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "tf(freq="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "), with freq of:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 892
    invoke-virtual {v7, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 893
    invoke-virtual {v1, v7}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 894
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$3(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v8

    invoke-virtual {v1, v8}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 896
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 898
    .local v3, "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    if-eqz p4, :cond_1

    invoke-virtual {p4, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->decodeNormValue(B)F

    move-result v2

    .line 899
    .local v2, "fieldNorm":F
    :goto_0
    invoke-virtual {v3, v2}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 900
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "fieldNorm(doc="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 901
    invoke-virtual {v1, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 903
    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v8

    .line 904
    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->idf:Lorg/apache/lucene/search/Explanation;
    invoke-static {p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$3(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Lorg/apache/lucene/search/Explanation;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    mul-float/2addr v8, v9

    .line 905
    invoke-virtual {v3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    mul-float/2addr v8, v9

    .line 903
    invoke-virtual {v1, v8}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 907
    invoke-virtual {v6, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 910
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v8

    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v9

    mul-float/2addr v8, v9

    invoke-virtual {v6, v8}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 912
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_2

    .line 915
    .end local v1    # "fieldExpl":Lorg/apache/lucene/search/Explanation;
    :goto_1
    return-object v1

    .line 898
    .end local v2    # "fieldNorm":F
    .restart local v1    # "fieldExpl":Lorg/apache/lucene/search/Explanation;
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0

    .restart local v2    # "fieldNorm":F
    :cond_2
    move-object v1, v6

    .line 915
    goto :goto_1
.end method


# virtual methods
.method public final computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 684
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->lengthNorm(Lorg/apache/lucene/index/FieldInvertState;)F

    move-result v0

    .line 685
    .local v0, "normValue":F
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->encodeNormValue(F)B

    move-result v1

    int-to-long v2, v1

    return-wide v2
.end method

.method public final varargs computeWeight(FLorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .locals 3
    .param p1, "queryBoost"    # F
    .param p2, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p3, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 751
    array-length v1, p3

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 752
    const/4 v1, 0x0

    aget-object v1, p3, v1

    invoke-virtual {p0, p2, v1}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idfExplain(Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 754
    .local v0, "idf":Lorg/apache/lucene/search/Explanation;
    :goto_0
    new-instance v1, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    invoke-virtual {p2}, Lorg/apache/lucene/search/CollectionStatistics;->field()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0, p1}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/Explanation;F)V

    return-object v1

    .line 753
    .end local v0    # "idf":Lorg/apache/lucene/search/Explanation;
    :cond_0
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idfExplain(Lorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract coord(II)F
.end method

.method public decodeNormValue(B)F
    .locals 2
    .param p1, "b"    # B

    .prologue
    .line 701
    sget-object v0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->NORM_TABLE:[F

    and-int/lit16 v1, p1, 0xff

    aget v0, v0, v1

    return v0
.end method

.method public encodeNormValue(F)B
    .locals 1
    .param p1, "f"    # F

    .prologue
    .line 718
    invoke-static {p1}, Lorg/apache/lucene/util/SmallFloat;->floatToByte315(F)B

    move-result v0

    return v0
.end method

.method public final exactSimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 759
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    .line 760
    .local v0, "idfstats":Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;
    new-instance v1, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->field:Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$1(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$ExactTFIDFDocScorer;-><init>(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)V

    return-object v1
.end method

.method public abstract idf(JJ)F
.end method

.method public idfExplain(Lorg/apache/lucene/search/CollectionStatistics;Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;
    .locals 8
    .param p1, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p2, "termStats"    # Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 622
    invoke-virtual {p2}, Lorg/apache/lucene/search/TermStatistics;->docFreq()J

    move-result-wide v0

    .line 623
    .local v0, "df":J
    invoke-virtual {p1}, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc()J

    move-result-wide v4

    .line 624
    .local v4, "max":J
    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idf(JJ)F

    move-result v2

    .line 625
    .local v2, "idf":F
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "idf(docFreq="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", maxDocs="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v2, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v3
.end method

.method public idfExplain(Lorg/apache/lucene/search/CollectionStatistics;[Lorg/apache/lucene/search/TermStatistics;)Lorg/apache/lucene/search/Explanation;
    .locals 13
    .param p1, "collectionStats"    # Lorg/apache/lucene/search/CollectionStatistics;
    .param p2, "termStats"    # [Lorg/apache/lucene/search/TermStatistics;

    .prologue
    .line 642
    invoke-virtual {p1}, Lorg/apache/lucene/search/CollectionStatistics;->maxDoc()J

    move-result-wide v4

    .line 643
    .local v4, "max":J
    const/4 v3, 0x0

    .line 644
    .local v3, "idf":F
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 645
    .local v2, "exp":Lorg/apache/lucene/search/Explanation;
    const-string v8, "idf(), sum of:"

    invoke-virtual {v2, v8}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 646
    array-length v9, p2

    const/4 v8, 0x0

    :goto_0
    if-lt v8, v9, :cond_0

    .line 652
    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 653
    return-object v2

    .line 646
    :cond_0
    aget-object v6, p2, v8

    .line 647
    .local v6, "stat":Lorg/apache/lucene/search/TermStatistics;
    invoke-virtual {v6}, Lorg/apache/lucene/search/TermStatistics;->docFreq()J

    move-result-wide v0

    .line 648
    .local v0, "df":J
    invoke-virtual {p0, v0, v1, v4, v5}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idf(JJ)F

    move-result v7

    .line 649
    .local v7, "termIdf":F
    new-instance v10, Lorg/apache/lucene/search/Explanation;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "idf(docFreq="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", maxDocs="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v7, v11}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v10}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 650
    add-float/2addr v3, v7

    .line 646
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public abstract lengthNorm(Lorg/apache/lucene/index/FieldInvertState;)F
.end method

.method public abstract queryNorm(F)F
.end method

.method public abstract scorePayload(IIILorg/apache/lucene/util/BytesRef;)F
.end method

.method public abstract sloppyFreq(I)F
.end method

.method public final sloppySimScorer(Lorg/apache/lucene/search/similarities/Similarity$SimWeight;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/similarities/Similarity$SloppySimScorer;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/Similarity$SimWeight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 765
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;

    .line 766
    .local v0, "idfstats":Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;
    new-instance v1, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->field:Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;->access$1(Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity$SloppyTFIDFDocScorer;-><init>(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;Lorg/apache/lucene/search/similarities/TFIDFSimilarity$IDFStats;Lorg/apache/lucene/index/NumericDocValues;)V

    return-object v1
.end method

.method public abstract tf(F)F
.end method

.method public tf(I)F
    .locals 1
    .param p1, "freq"    # I

    .prologue
    .line 581
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->tf(F)F

    move-result v0

    return v0
.end method
