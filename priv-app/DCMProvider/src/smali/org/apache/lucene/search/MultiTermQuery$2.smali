.class Lorg/apache/lucene/search/MultiTermQuery$2;
.super Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public setDocCountPercent(D)V
    .locals 2
    .param p1, "percent"    # D

    .prologue
    .line 242
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please create a private instance"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setTermCountCutoff(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please create a private instance"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
