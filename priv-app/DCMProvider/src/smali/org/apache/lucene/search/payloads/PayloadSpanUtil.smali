.class public Lorg/apache/lucene/search/payloads/PayloadSpanUtil;
.super Ljava/lang/Object;
.source "PayloadSpanUtil.java"


# instance fields
.field private context:Lorg/apache/lucene/index/IndexReaderContext;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReaderContext;)V
    .locals 0
    .param p1, "context"    # Lorg/apache/lucene/index/IndexReaderContext;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->context:Lorg/apache/lucene/index/IndexReaderContext;

    .line 67
    return-void
.end method

.method private getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 10
    .param p2, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<[B>;",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "payloads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 182
    .local v5, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    .line 183
    .local v6, "terms":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p2, v6}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    .line 184
    invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 187
    iget-object v7, p0, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->context:Lorg/apache/lucene/index/IndexReaderContext;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 198
    return-void

    .line 184
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/Term;

    .line 185
    .local v4, "term":Lorg/apache/lucene/index/Term;
    iget-object v8, p0, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->context:Lorg/apache/lucene/index/IndexReaderContext;

    const/4 v9, 0x1

    invoke-static {v8, v4, v9}, Lorg/apache/lucene/index/TermContext;->build(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/index/TermContext;

    move-result-object v8

    invoke-interface {v5, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 187
    .end local v4    # "term":Lorg/apache/lucene/index/Term;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 188
    .local v0, "atomicReaderContext":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v8

    invoke-virtual {p2, v0, v8, v5}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v3

    .line 189
    .local v3, "spans":Lorg/apache/lucene/search/spans/Spans;
    :cond_3
    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 190
    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 191
    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v2

    .line 192
    .local v2, "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 193
    .local v1, "bytes":[B
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V
    .locals 27
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Ljava/util/Collection",
            "<[B>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "payloads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/BooleanQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_3

    .line 85
    check-cast p1, Lorg/apache/lucene/search/BooleanQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v17

    .line 87
    .local v17, "queryClauses":[Lorg/apache/lucene/search/BooleanClause;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v7, v0, :cond_1

    .line 177
    .end local v7    # "i":I
    .end local v17    # "queryClauses":[Lorg/apache/lucene/search/BooleanClause;
    :cond_0
    :goto_1
    return-void

    .line 88
    .restart local v7    # "i":I
    .restart local v17    # "queryClauses":[Lorg/apache/lucene/search/BooleanClause;
    :cond_1
    aget-object v24, v17, v7

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v24

    if-nez v24, :cond_2

    .line 89
    aget-object v24, v17, v7

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    .line 87
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 93
    .end local v7    # "i":I
    .end local v17    # "queryClauses":[Lorg/apache/lucene/search/BooleanClause;
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_3
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/PhraseQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_6

    move-object/from16 v24, p1

    .line 94
    check-cast v24, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/PhraseQuery;->getTerms()[Lorg/apache/lucene/index/Term;

    move-result-object v12

    .line 95
    .local v12, "phraseQueryTerms":[Lorg/apache/lucene/index/Term;
    array-length v0, v12

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v3, v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 96
    .local v3, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    array-length v0, v12

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v7, v0, :cond_5

    move-object/from16 v24, p1

    .line 100
    check-cast v24, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/PhraseQuery;->getSlop()I

    move-result v18

    .line 101
    .local v18, "slop":I
    const/4 v8, 0x0

    .line 103
    .local v8, "inorder":Z
    if-nez v18, :cond_4

    .line 104
    const/4 v8, 0x1

    .line 107
    :cond_4
    new-instance v19, Lorg/apache/lucene/search/spans/SpanNearQuery;

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-direct {v0, v3, v1, v8}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 108
    .local v19, "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v24

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 109
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto :goto_1

    .line 97
    .end local v8    # "inorder":Z
    .end local v18    # "slop":I
    .end local v19    # "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_5
    new-instance v24, Lorg/apache/lucene/search/spans/SpanTermQuery;

    aget-object v25, v12, v7

    invoke-direct/range {v24 .. v25}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    aput-object v24, v3, v7

    .line 96
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 110
    .end local v3    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v7    # "i":I
    .end local v12    # "phraseQueryTerms":[Lorg/apache/lucene/index/Term;
    :cond_6
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/TermQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_7

    .line 111
    new-instance v20, Lorg/apache/lucene/search/spans/SpanTermQuery;

    move-object/from16 v24, p1

    check-cast v24, Lorg/apache/lucene/search/TermQuery;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v24

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 112
    .local v20, "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v24

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanTermQuery;->setBoost(F)V

    .line 113
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto/16 :goto_1

    .line 114
    .end local v20    # "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    :cond_7
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/spans/SpanQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_8

    .line 115
    check-cast p1, Lorg/apache/lucene/search/spans/SpanQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto/16 :goto_1

    .line 116
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_8
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/FilteredQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_9

    .line 117
    check-cast p1, Lorg/apache/lucene/search/FilteredQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/FilteredQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    goto/16 :goto_1

    .line 118
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_9
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_a

    .line 120
    check-cast p1, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 121
    .local v9, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/Query;>;"
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_0

    .line 122
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/lucene/search/Query;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    goto :goto_3

    .line 125
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/Query;>;"
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_a
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    move/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v11, p1

    .line 126
    check-cast v11, Lorg/apache/lucene/search/MultiPhraseQuery;

    .line 127
    .local v11, "mpq":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-virtual {v11}, Lorg/apache/lucene/search/MultiPhraseQuery;->getTermArrays()Ljava/util/List;

    move-result-object v23

    .line 128
    .local v23, "termArrays":Ljava/util/List;, "Ljava/util/List<[Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {v11}, Lorg/apache/lucene/search/MultiPhraseQuery;->getPositions()[I

    move-result-object v16

    .line 129
    .local v16, "positions":[I
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    if-lez v24, :cond_0

    .line 131
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    aget v10, v16, v24

    .line 132
    .local v10, "maxPosition":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    if-lt v7, v0, :cond_b

    .line 139
    add-int/lit8 v24, v10, 0x1

    move/from16 v0, v24

    new-array v4, v0, [Ljava/util/List;

    .line 140
    .local v4, "disjunctLists":[Ljava/util/List;
    const/4 v6, 0x0

    .line 142
    .local v6, "distinctPositions":I
    const/4 v7, 0x0

    :goto_5
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v24

    move/from16 v0, v24

    if-lt v7, v0, :cond_d

    .line 155
    const/4 v15, 0x0

    .line 156
    .local v15, "positionGaps":I
    const/4 v13, 0x0

    .line 157
    .local v13, "position":I
    new-array v3, v6, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 158
    .restart local v3    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v7, 0x0

    :goto_6
    array-length v0, v4

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v7, v0, :cond_10

    .line 168
    invoke-virtual {v11}, Lorg/apache/lucene/search/MultiPhraseQuery;->getSlop()I

    move-result v18

    .line 169
    .restart local v18    # "slop":I
    if-nez v18, :cond_12

    const/4 v8, 0x1

    .line 171
    .restart local v8    # "inorder":Z
    :goto_7
    new-instance v19, Lorg/apache/lucene/search/spans/SpanNearQuery;

    add-int v24, v18, v15

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-direct {v0, v3, v1, v8}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 173
    .restart local v19    # "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v24

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 174
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto/16 :goto_1

    .line 133
    .end local v3    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v4    # "disjunctLists":[Ljava/util/List;
    .end local v6    # "distinctPositions":I
    .end local v8    # "inorder":Z
    .end local v13    # "position":I
    .end local v15    # "positionGaps":I
    .end local v18    # "slop":I
    .end local v19    # "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_b
    aget v24, v16, v7

    move/from16 v0, v24

    if-le v0, v10, :cond_c

    .line 134
    aget v10, v16, v7

    .line 132
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 143
    .restart local v4    # "disjunctLists":[Ljava/util/List;
    .restart local v6    # "distinctPositions":I
    :cond_d
    move-object/from16 v0, v23

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [Lorg/apache/lucene/index/Term;

    .line 144
    .local v22, "termArray":[Lorg/apache/lucene/index/Term;
    aget v24, v16, v7

    aget-object v5, v4, v24

    .line 145
    .local v5, "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    if-nez v5, :cond_e

    .line 146
    aget v24, v16, v7

    new-instance v5, Ljava/util/ArrayList;

    .line 147
    .end local v5    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 146
    aput-object v5, v4, v24

    .line 148
    .restart local v5    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    add-int/lit8 v6, v6, 0x1

    .line 150
    :cond_e
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_8
    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_f

    .line 142
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 150
    :cond_f
    aget-object v21, v22, v24

    .line 151
    .local v21, "term":Lorg/apache/lucene/index/Term;
    new-instance v26, Lorg/apache/lucene/search/spans/SpanTermQuery;

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v24, v24, 0x1

    goto :goto_8

    .line 159
    .end local v5    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    .end local v21    # "term":Lorg/apache/lucene/index/Term;
    .end local v22    # "termArray":[Lorg/apache/lucene/index/Term;
    .restart local v3    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    .restart local v13    # "position":I
    .restart local v15    # "positionGaps":I
    :cond_10
    aget-object v5, v4, v7

    .line 160
    .restart local v5    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    if-eqz v5, :cond_11

    .line 161
    add-int/lit8 v14, v13, 0x1

    .end local v13    # "position":I
    .local v14, "position":I
    new-instance v25, Lorg/apache/lucene/search/spans/SpanOrQuery;

    .line 162
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v24

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/search/spans/SpanQuery;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 161
    aput-object v25, v3, v13

    move v13, v14

    .line 158
    .end local v14    # "position":I
    .restart local v13    # "position":I
    :goto_9
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_6

    .line 164
    :cond_11
    add-int/lit8 v15, v15, 0x1

    goto :goto_9

    .line 169
    .end local v5    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    .restart local v18    # "slop":I
    :cond_12
    const/4 v8, 0x0

    goto/16 :goto_7
.end method


# virtual methods
.method public getPayloadsForQuery(Lorg/apache/lucene/search/Query;)Ljava/util/Collection;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            ")",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v0, "payloads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    .line 79
    return-object v0
.end method
