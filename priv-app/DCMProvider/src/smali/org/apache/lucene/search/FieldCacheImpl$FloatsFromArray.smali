.class Lorg/apache/lucene/search/FieldCacheImpl$FloatsFromArray;
.super Lorg/apache/lucene/search/FieldCache$Floats;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FloatsFromArray"
.end annotation


# instance fields
.field private final values:[F


# direct methods
.method public constructor <init>([F)V
    .locals 0
    .param p1, "values"    # [F

    .prologue
    .line 772
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Floats;-><init>()V

    .line 773
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatsFromArray;->values:[F

    .line 774
    return-void
.end method


# virtual methods
.method public get(I)F
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 778
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$FloatsFromArray;->values:[F

    aget v0, v0, p1

    return v0
.end method
