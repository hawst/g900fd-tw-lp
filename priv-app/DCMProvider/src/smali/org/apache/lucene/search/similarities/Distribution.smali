.class public abstract Lorg/apache/lucene/search/similarities/Distribution;
.super Ljava/lang/Object;
.source "Distribution.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/search/similarities/BasicStats;FF)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F
    .param p3, "lambda"    # F

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    .line 43
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/search/similarities/Distribution;->score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v0
.end method

.method public abstract score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F
.end method

.method public abstract toString()Ljava/lang/String;
.end method
