.class final Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;
.super Lorg/apache/lucene/search/CachingCollector;
.source "CachingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/CachingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NoScoreCachingCollector"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Collector;D)V
    .locals 6
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .param p2, "maxRAMMB"    # D

    .prologue
    .line 223
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/CachingCollector;-><init>(Lorg/apache/lucene/search/Collector;DZLorg/apache/lucene/search/CachingCollector;)V

    .line 224
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/search/Collector;I)V
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .param p2, "maxDocsToCache"    # I

    .prologue
    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/CachingCollector;-><init>(Lorg/apache/lucene/search/Collector;ILorg/apache/lucene/search/CachingCollector;)V

    .line 228
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 3
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    if-nez v1, :cond_0

    .line 235
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 270
    :goto_0
    return-void

    .line 240
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    array-length v2, v2

    if-ne v1, v2, :cond_3

    .line 241
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->base:I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->base:I

    .line 244
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    array-length v1, v1

    mul-int/lit8 v0, v1, 0x8

    .line 245
    .local v0, "nextLength":I
    const/high16 v1, 0x80000

    if-le v0, v1, :cond_1

    .line 246
    const/high16 v0, 0x80000

    .line 249
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->base:I

    add-int/2addr v1, v0

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->maxDocsToCache:I

    if-le v1, v2, :cond_2

    .line 251
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->maxDocsToCache:I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->base:I

    sub-int v0, v1, v2

    .line 252
    if-gtz v0, :cond_2

    .line 254
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    .line 255
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->cachedSegs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 256
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->cachedDocs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 257
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0

    .line 262
    :cond_2
    new-array v1, v0, [I

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    .line 263
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->cachedDocs:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    .line 267
    .end local v0    # "nextLength":I
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    aput p1, v1, v2

    .line 268
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    .line 269
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0
.end method

.method public replay(Lorg/apache/lucene/search/Collector;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->replayInit(Lorg/apache/lucene/search/Collector;)V

    .line 276
    const/4 v1, 0x0

    .line 277
    .local v1, "curUpto":I
    const/4 v3, 0x0

    .line 278
    .local v3, "curbase":I
    const/4 v0, 0x0

    .line 279
    .local v0, "chunkUpto":I
    # getter for: Lorg/apache/lucene/search/CachingCollector;->EMPTY_INT_ARRAY:[I
    invoke-static {}, Lorg/apache/lucene/search/CachingCollector;->access$2()[I

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    .line 280
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->cachedSegs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 292
    return-void

    .line 280
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/CachingCollector$SegStart;

    .line 281
    .local v4, "seg":Lorg/apache/lucene/search/CachingCollector$SegStart;
    iget-object v5, v4, Lorg/apache/lucene/search/CachingCollector$SegStart;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 282
    :goto_0
    add-int v5, v3, v1

    iget v7, v4, Lorg/apache/lucene/search/CachingCollector$SegStart;->end:I

    if-ge v5, v7, :cond_0

    .line 283
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    array-length v5, v5

    if-ne v1, v5, :cond_2

    .line 284
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    array-length v5, v5

    add-int/2addr v3, v5

    .line 285
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->cachedDocs:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    iput-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    .line 286
    add-int/lit8 v0, v0, 0x1

    .line 287
    const/4 v1, 0x0

    .line 289
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->curDocs:[I

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "curUpto":I
    .local v2, "curUpto":I
    aget v5, v5, v1

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->collect(I)V

    move v1, v2

    .end local v2    # "curUpto":I
    .restart local v1    # "curUpto":I
    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 297
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 301
    invoke-virtual {p0}, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->isCached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CachingCollector ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->base:I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;->upto:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " docs cached)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CachingCollector (cache was cleared)"

    goto :goto_0
.end method
