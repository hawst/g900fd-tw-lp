.class public final Lorg/apache/lucene/search/FieldComparator$DoubleComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DoubleComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:D

.field private currentReaderValues:Lorg/apache/lucene/search/FieldCache$Doubles;

.field private final parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

.field private final values:[D


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Double;

    .prologue
    .line 302
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 303
    new-array v0, p1, [D

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    .line 304
    check-cast p3, Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .line 305
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 4
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v0, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v2, v2, p2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method public compareBottom(I)I
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 314
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v0

    .line 317
    .local v0, "v2":D
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 318
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 321
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->bottom:D

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    return v2
.end method

.method public compareDocToValue(ILjava/lang/Double;)I
    .locals 6
    .param p1, "doc"    # I
    .param p2, "valueObj"    # Ljava/lang/Double;

    .prologue
    .line 356
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 357
    .local v2, "value":D
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v0

    .line 360
    .local v0, "docValue":D
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v4, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 361
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 363
    :cond_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v4

    return v4
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->compareDocToValue(ILjava/lang/Double;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 4
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 326
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v0

    .line 329
    .local v0, "v2":D
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 330
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 333
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aput-wide v0, v2, p1

    .line 334
    return-void
.end method

.method public setBottom(I)V
    .locals 2
    .param p1, "bottom"    # I

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->bottom:D

    .line 347
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 340
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v3, v4, v0}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:Lorg/apache/lucene/search/FieldCache$Doubles;

    .line 341
    invoke-super {p0, p1}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    return-object v0

    .line 340
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Double;
    .locals 2
    .param p1, "slot"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v0, v0, p1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->value(I)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method
