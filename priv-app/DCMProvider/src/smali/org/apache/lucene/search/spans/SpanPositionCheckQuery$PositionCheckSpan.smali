.class public Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;
.super Lorg/apache/lucene/search/spans/Spans;
.source "SpanPositionCheckQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PositionCheckSpan"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$search$spans$SpanPositionCheckQuery$AcceptStatus:[I


# instance fields
.field private spans:Lorg/apache/lucene/search/spans/Spans;

.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$search$spans$SpanPositionCheckQuery$AcceptStatus()[I
    .locals 3

    .prologue
    .line 119
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->$SWITCH_TABLE$org$apache$lucene$search$spans$SpanPositionCheckQuery$AcceptStatus:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->values()[Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO_AND_ADVANCE:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->YES:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->$SWITCH_TABLE$org$apache$lucene$search$spans$SpanPositionCheckQuery$AcceptStatus:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V
    .locals 1
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    .local p4, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 123
    iget-object v0, p1, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p2, p3, p4}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 124
    return-void
.end method


# virtual methods
.method public cost()J
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method protected doNext()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 144
    :cond_0
    :goto_0
    invoke-static {}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->$SWITCH_TABLE$org$apache$lucene$search$spans$SpanPositionCheckQuery$AcceptStatus()[I

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    invoke-virtual {v2, p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 145
    :pswitch_0
    const/4 v0, 0x1

    .line 152
    :goto_1
    return v0

    .line 147
    :pswitch_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 151
    :pswitch_2
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 174
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_0
    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 131
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->doNext()Z

    move-result v0

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 139
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->doNext()Z

    move-result v0

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "spans("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
