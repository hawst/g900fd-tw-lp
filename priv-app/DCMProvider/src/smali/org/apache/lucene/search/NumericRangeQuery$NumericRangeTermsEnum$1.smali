.class Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$1;
.super Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
.source "NumericRangeQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;Lorg/apache/lucene/index/TermsEnum;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$1;->this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;

    .line 430
    invoke-direct {p0}, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public final addRange(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "minPrefixCoded"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "maxPrefixCoded"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 433
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$1;->this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;

    # getter for: Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;
    invoke-static {v0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->access$0(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 434
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$1;->this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;

    # getter for: Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;
    invoke-static {v0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->access$0(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 435
    return-void
.end method
