.class public abstract Lorg/apache/lucene/search/DocIdSetIterator;
.super Ljava/lang/Object;
.source "DocIdSetIterator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final NO_MORE_DOCS:I = 0x7fffffff


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/DocIdSetIterator;->$assertionsDisabled:Z

    .line 35
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract advance(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract cost()J
.end method

.method public abstract docID()I
.end method

.method public abstract nextDoc()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final slowAdvance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    sget-boolean v1, Lorg/apache/lucene/search/DocIdSetIterator;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 100
    invoke-virtual {p0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v1

    if-lt v1, p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 103
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .line 102
    .local v0, "doc":I
    if-lt v0, p1, :cond_0

    .line 105
    return v0
.end method
