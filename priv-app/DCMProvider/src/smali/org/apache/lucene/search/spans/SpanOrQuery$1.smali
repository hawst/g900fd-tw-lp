.class Lorg/apache/lucene/search/spans/SpanOrQuery$1;
.super Lorg/apache/lucene/search/spans/Spans;
.source "SpanOrQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/spans/SpanOrQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private cost:J

.field private queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

.field private final synthetic val$acceptDocs:Lorg/apache/lucene/util/Bits;

.field private final synthetic val$context:Lorg/apache/lucene/index/AtomicReaderContext;

.field private final synthetic val$termContexts:Ljava/util/Map;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    iput-object p2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$context:Lorg/apache/lucene/index/AtomicReaderContext;

    iput-object p3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$acceptDocs:Lorg/apache/lucene/util/Bits;

    iput-object p4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$termContexts:Ljava/util/Map;

    .line 173
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    return-void
.end method

.method private initSpanQueue(I)Z
    .locals 7
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 178
    new-instance v2, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    # getter for: Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/lucene/search/spans/SpanOrQuery;->access$1(Lorg/apache/lucene/search/spans/SpanOrQuery;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;-><init>(Lorg/apache/lucene/search/spans/SpanOrQuery;I)V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    .line 179
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    # getter for: Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;
    invoke-static {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery;->access$1(Lorg/apache/lucene/search/spans/SpanOrQuery;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 180
    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 188
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 181
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$acceptDocs:Lorg/apache/lucene/util/Bits;

    iget-object v5, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$termContexts:Ljava/util/Map;

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    .line 182
    .local v1, "spans":Lorg/apache/lucene/search/spans/Spans;
    iget-wide v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->cost:J

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->cost()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->cost:J

    .line 183
    if-ne p1, v6, :cond_2

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    if-nez v2, :cond_3

    .line 184
    :cond_2
    if-eq p1, v6, :cond_0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 185
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 188
    .end local v1    # "spans":Lorg/apache/lucene/search/spans/Spans;
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private top()Lorg/apache/lucene/search/spans/Spans;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method


# virtual methods
.method public cost()J
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->cost:J

    return-wide v0
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    .line 245
    .local v1, "theTop":Lorg/apache/lucene/search/spans/Spans;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 248
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_0
    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    .line 254
    .local v0, "top":Lorg/apache/lucene/search/spans/Spans;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 193
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    if-nez v2, :cond_1

    .line 194
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->initSpanQueue(I)Z

    move-result v0

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 202
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->updateTop()Ljava/lang/Object;

    move v0, v1

    .line 203
    goto :goto_0

    .line 206
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->pop()Ljava/lang/Object;

    .line 207
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    if-nez v1, :cond_0

    .line 215
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->initSpanQueue(I)Z

    move-result v1

    .line 231
    :goto_0
    return v1

    .line 218
    :cond_0
    const/4 v0, 0x0

    .line 219
    .local v0, "skipCalled":Z
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-lt v1, p1, :cond_2

    .line 228
    :cond_1
    if-eqz v0, :cond_5

    .line 229
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    .line 220
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 221
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->updateTop()Ljava/lang/Object;

    .line 225
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 223
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->pop()Ljava/lang/Object;

    goto :goto_2

    .line 229
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 231
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->next()Z

    move-result v1

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "spans("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 260
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    if-nez v0, :cond_0

    const-string v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 261
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->doc()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->start()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->end()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "END"

    goto :goto_0
.end method
