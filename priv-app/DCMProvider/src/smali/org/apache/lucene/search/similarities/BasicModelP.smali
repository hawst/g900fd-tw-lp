.class public Lorg/apache/lucene/search/similarities/BasicModelP;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelP.java"


# static fields
.field protected static LOG2_E:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide v0, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v0, v1}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v0

    sput-wide v0, Lorg/apache/lucene/search/similarities/BasicModelP;->LOG2_E:D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 10
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    const-wide/16 v4, 0x1

    .line 39
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v2

    add-long/2addr v2, v4

    long-to-float v1, v2

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v2

    add-long/2addr v2, v4

    long-to-float v2, v2

    div-float v0, v1, v2

    .line 40
    .local v0, "lambda":F
    float-to-double v2, p2

    div-float v1, p2, v0

    float-to-double v4, v1

    invoke-static {v4, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    .line 41
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v4, 0x41400000    # 12.0f

    mul-float/2addr v4, p2

    div-float/2addr v1, v4

    add-float/2addr v1, v0

    sub-float/2addr v1, p2

    float-to-double v4, v1

    sget-wide v6, Lorg/apache/lucene/search/similarities/BasicModelP;->LOG2_E:D

    mul-double/2addr v4, v6

    .line 40
    add-double/2addr v2, v4

    .line 42
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v8, p2

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    .line 40
    add-double/2addr v2, v4

    double-to-float v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "P"

    return-object v0
.end method
