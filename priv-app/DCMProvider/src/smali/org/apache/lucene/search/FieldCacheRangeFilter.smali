.class public abstract Lorg/apache/lucene/search/FieldCacheRangeFilter;
.super Lorg/apache/lucene/search/Filter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/search/Filter;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final field:Ljava/lang/String;

.field final includeLower:Z

.field final includeUpper:Z

.field final lowerVal:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final parser:Lorg/apache/lucene/search/FieldCache$Parser;

.field final upperVal:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZ)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p5, "includeLower"    # Z
    .param p6, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$Parser;",
            "TT;TT;ZZ)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    .local p3, "lowerVal":Ljava/lang/Object;, "TT;"
    .local p4, "upperVal":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 72
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->field:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    .line 74
    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    .line 75
    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    .line 76
    iput-boolean p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeLower:Z

    .line 77
    iput-boolean p6, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeUpper:Z

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct/range {p0 .. p6}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZ)V

    return-void
.end method

.method public static newByteRange(Ljava/lang/String;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/Byte;
    .param p2, "upperVal"    # Ljava/lang/Byte;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            "Ljava/lang/Byte;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter;->newByteRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;

    move-result-object v0

    return-object v0
.end method

.method public static newByteRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "parser"    # Lorg/apache/lucene/search/FieldCache$ByteParser;
    .param p2, "lowerVal"    # Ljava/lang/Byte;
    .param p3, "upperVal"    # Ljava/lang/Byte;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$ByteParser;",
            "Ljava/lang/Byte;",
            "Ljava/lang/Byte;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)V

    return-object v0
.end method

.method public static newBytesRefRange(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "upperVal"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/util/BytesRef;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;ZZ)V

    return-object v0
.end method

.method public static newDoubleRange(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/Double;
    .param p2, "upperVal"    # Ljava/lang/Double;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter;->newDoubleRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;

    move-result-object v0

    return-object v0
.end method

.method public static newDoubleRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "parser"    # Lorg/apache/lucene/search/FieldCache$DoubleParser;
    .param p2, "lowerVal"    # Ljava/lang/Double;
    .param p3, "upperVal"    # Ljava/lang/Double;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$DoubleParser;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$8;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;Ljava/lang/Double;ZZ)V

    return-object v0
.end method

.method public static newFloatRange(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/Float;
    .param p2, "upperVal"    # Ljava/lang/Float;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter;->newFloatRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;

    move-result-object v0

    return-object v0
.end method

.method public static newFloatRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "parser"    # Lorg/apache/lucene/search/FieldCache$FloatParser;
    .param p2, "lowerVal"    # Ljava/lang/Float;
    .param p3, "upperVal"    # Ljava/lang/Float;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$FloatParser;",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Float;Ljava/lang/Float;ZZ)V

    return-object v0
.end method

.method public static newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/Integer;
    .param p2, "upperVal"    # Ljava/lang/Integer;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter;->newIntRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;

    move-result-object v0

    return-object v0
.end method

.method public static newIntRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "parser"    # Lorg/apache/lucene/search/FieldCache$IntParser;
    .param p2, "lowerVal"    # Ljava/lang/Integer;
    .param p3, "upperVal"    # Ljava/lang/Integer;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$IntParser;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)V

    return-object v0
.end method

.method public static newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/Long;
    .param p2, "upperVal"    # Ljava/lang/Long;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter;->newLongRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;

    move-result-object v0

    return-object v0
.end method

.method public static newLongRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "parser"    # Lorg/apache/lucene/search/FieldCache$LongParser;
    .param p2, "lowerVal"    # Ljava/lang/Long;
    .param p3, "upperVal"    # Ljava/lang/Long;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$LongParser;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Long;Ljava/lang/Long;ZZ)V

    return-object v0
.end method

.method public static newShortRange(Ljava/lang/String;Ljava/lang/Short;Ljava/lang/Short;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 6
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/Short;
    .param p2, "upperVal"    # Ljava/lang/Short;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Short;",
            "Ljava/lang/Short;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter;->newShortRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Ljava/lang/Short;Ljava/lang/Short;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;

    move-result-object v0

    return-object v0
.end method

.method public static newShortRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Ljava/lang/Short;Ljava/lang/Short;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "parser"    # Lorg/apache/lucene/search/FieldCache$ShortParser;
    .param p2, "lowerVal"    # Ljava/lang/Short;
    .param p3, "upperVal"    # Ljava/lang/Short;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/search/FieldCache$ShortParser;",
            "Ljava/lang/Short;",
            "Ljava/lang/Short;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Short;Ljava/lang/Short;ZZ)V

    return-object v0
.end method

.method public static newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
    .locals 7
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "lowerVal"    # Ljava/lang/String;
    .param p2, "upperVal"    # Ljava/lang/String;
    .param p3, "includeLower"    # Z
    .param p4, "includeUpper"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lorg/apache/lucene/search/FieldCacheRangeFilter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 526
    if-ne p0, p1, :cond_1

    .line 537
    :cond_0
    :goto_0
    return v1

    .line 527
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/FieldCacheRangeFilter;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 528
    check-cast v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;

    .line 530
    .local v0, "other":Lorg/apache/lucene/search/FieldCacheRangeFilter;
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 531
    iget-boolean v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeLower:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeLower:Z

    if-ne v3, v4, :cond_3

    .line 532
    iget-boolean v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeUpper:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeUpper:Z

    if-eq v3, v4, :cond_4

    :cond_3
    move v1, v2

    .line 533
    goto :goto_0

    .line 534
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    iget-object v4, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    iget-object v3, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    if-nez v3, :cond_5

    .line 535
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    iget-object v4, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    :cond_8
    move v1, v2

    goto :goto_0

    :cond_9
    iget-object v3, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    if-nez v3, :cond_8

    .line 536
    :cond_a
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v4, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_b
    iget-object v3, v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public abstract getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 552
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getLowerVal()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 561
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    return-object v0
.end method

.method public getParser()Lorg/apache/lucene/search/FieldCache$Parser;
    .locals 1

    .prologue
    .line 567
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    return-object v0
.end method

.method public getUpperVal()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 564
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 542
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 543
    .local v0, "h":I
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_0
    xor-int/2addr v0, v1

    .line 544
    shl-int/lit8 v1, v0, 0x1

    ushr-int/lit8 v2, v0, 0x1f

    or-int v0, v1, v2

    .line 545
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_1
    xor-int/2addr v0, v1

    .line 546
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_2
    xor-int/2addr v0, v1

    .line 547
    iget-boolean v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeLower:Z

    if-eqz v1, :cond_3

    const v1, 0x5c586ea0

    :goto_3
    iget-boolean v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeUpper:Z

    if-eqz v2, :cond_4

    const v2, 0x6695b902

    :goto_4
    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 548
    return v0

    .line 543
    :cond_0
    const v1, 0x20cdc4ec

    goto :goto_0

    .line 545
    :cond_1
    const v1, -0x63cd9023

    goto :goto_1

    .line 546
    :cond_2
    const v1, -0x5db9cb6c

    goto :goto_2

    .line 547
    :cond_3
    const v1, -0x15c209ca

    goto :goto_3

    :cond_4
    const v2, 0x742608b5

    goto :goto_4
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 555
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeLower:Z

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 558
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeUpper:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 514
    .local p0, "this":Lorg/apache/lucene/search/FieldCacheRangeFilter;, "Lorg/apache/lucene/search/FieldCacheRangeFilter<TT;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->field:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 515
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeLower:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x5b

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 516
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    if-nez v1, :cond_1

    const-string v1, "*"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 517
    const-string v2, " TO "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 518
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    if-nez v1, :cond_2

    const-string v1, "*"

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 519
    iget-boolean v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->includeUpper:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x5d

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 520
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 515
    return-object v1

    :cond_0
    const/16 v1, 0x7b

    goto :goto_0

    .line 516
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->lowerVal:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 518
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter;->upperVal:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 519
    :cond_3
    const/16 v1, 0x7d

    goto :goto_3
.end method
