.class Lorg/apache/lucene/search/FieldCacheImpl$ShortsFromArray;
.super Lorg/apache/lucene/search/FieldCache$Shorts;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ShortsFromArray"
.end annotation


# instance fields
.field private final values:[S


# direct methods
.method public constructor <init>([S)V
    .locals 0
    .param p1, "values"    # [S

    .prologue
    .line 486
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$Shorts;-><init>()V

    .line 487
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortsFromArray;->values:[S

    .line 488
    return-void
.end method


# virtual methods
.method public get(I)S
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 492
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ShortsFromArray;->values:[S

    aget-short v0, v0, p1

    return v0
.end method
