.class final Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "NumericRangeQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/NumericRangeQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "NumericRangeTermsEnum"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private currentLowerBound:Lorg/apache/lucene/util/BytesRef;

.field private currentUpperBound:Lorg/apache/lucene/util/BytesRef;

.field private final rangeBounds:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/search/NumericRangeQuery;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I
    .locals 3

    .prologue
    .line 390
    sget-object v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/FieldType$NumericType;->values()[Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390
    const-class v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/NumericRangeQuery;Lorg/apache/lucene/index/TermsEnum;)V
    .locals 12
    .param p2, "tenum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const-wide/16 v10, 0x1

    const-wide/high16 v0, -0x8000000000000000L

    const v7, 0x7fffffff

    const/high16 v6, -0x80000000

    .line 397
    iput-object p1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->this$0:Lorg/apache/lucene/search/NumericRangeQuery;

    .line 398
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 394
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    iput-object v8, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    .line 399
    invoke-static {}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I

    move-result-object v8

    iget-object v9, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v9}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 482
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid NumericType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    :pswitch_0
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v7, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    if-ne v6, v7, :cond_2

    .line 405
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v6, :cond_1

    move-wide v2, v0

    .line 411
    .local v2, "minBound":J
    :goto_0
    iget-boolean v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    if-nez v6, :cond_6

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-eqz v6, :cond_6

    .line 412
    cmp-long v6, v2, v4

    if-nez v6, :cond_5

    .line 485
    .end local v2    # "minBound":J
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->termComp:Ljava/util/Comparator;

    .line 486
    return-void

    .line 405
    :cond_1
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    goto :goto_0

    .line 407
    :cond_2
    sget-boolean v6, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v7, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eq v6, v7, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 408
    :cond_3
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v6, :cond_4

    sget-wide v2, Lorg/apache/lucene/search/NumericRangeQuery;->LONG_NEGATIVE_INFINITY:J

    .restart local v2    # "minBound":J
    :goto_2
    goto :goto_0

    .line 409
    .end local v2    # "minBound":J
    :cond_4
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v2

    goto :goto_2

    .line 413
    .restart local v2    # "minBound":J
    :cond_5
    add-long/2addr v2, v10

    .line 418
    :cond_6
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v7, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    if-ne v6, v7, :cond_9

    .line 419
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v6, :cond_8

    .line 425
    .local v4, "maxBound":J
    :goto_3
    iget-boolean v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    if-nez v6, :cond_7

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-eqz v6, :cond_7

    .line 426
    cmp-long v0, v4, v0

    if-eqz v0, :cond_0

    .line 427
    sub-long/2addr v4, v10

    .line 430
    :cond_7
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$1;-><init>(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;)V

    .line 436
    iget v1, p1, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    .line 430
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/NumericUtils;->splitLongRange(Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;IJJ)V

    goto :goto_1

    .line 419
    .end local v4    # "maxBound":J
    :cond_8
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    goto :goto_3

    .line 421
    :cond_9
    sget-boolean v6, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_a

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v7, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eq v6, v7, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 422
    :cond_a
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v6, :cond_b

    sget-wide v4, Lorg/apache/lucene/search/NumericRangeQuery;->LONG_POSITIVE_INFINITY:J

    .restart local v4    # "maxBound":J
    :goto_4
    goto :goto_3

    .line 423
    .end local v4    # "maxBound":J
    :cond_b
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v4

    goto :goto_4

    .line 444
    .end local v2    # "minBound":J
    :pswitch_1
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    if-ne v0, v1, :cond_f

    .line 445
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v0, :cond_e

    move v2, v6

    .line 451
    .local v2, "minBound":I
    :goto_5
    iget-boolean v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    if-nez v0, :cond_c

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-eqz v0, :cond_c

    .line 452
    if-eq v2, v7, :cond_0

    .line 453
    add-int/lit8 v2, v2, 0x1

    .line 458
    :cond_c
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    if-ne v0, v1, :cond_13

    .line 459
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v0, :cond_12

    move v4, v7

    .line 465
    .local v4, "maxBound":I
    :goto_6
    iget-boolean v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    if-nez v0, :cond_d

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-eqz v0, :cond_d

    .line 466
    if-eq v4, v6, :cond_0

    .line 467
    add-int/lit8 v4, v4, -0x1

    .line 470
    :cond_d
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum$2;-><init>(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;)V

    .line 476
    iget v1, p1, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    .line 470
    invoke-static {v0, v1, v2, v4}, Lorg/apache/lucene/util/NumericUtils;->splitIntRange(Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;III)V

    goto/16 :goto_1

    .line 445
    .end local v2    # "minBound":I
    .end local v4    # "maxBound":I
    :cond_e
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v2

    goto :goto_5

    .line 447
    :cond_f
    sget-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_10

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eq v0, v1, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 448
    :cond_10
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v0, :cond_11

    sget v2, Lorg/apache/lucene/search/NumericRangeQuery;->INT_NEGATIVE_INFINITY:I

    .restart local v2    # "minBound":I
    :goto_7
    goto :goto_5

    .line 449
    .end local v2    # "minBound":I
    :cond_11
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v2

    goto :goto_7

    .line 459
    .restart local v2    # "minBound":I
    :cond_12
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v4

    goto :goto_6

    .line 461
    :cond_13
    sget-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_14

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eq v0, v1, :cond_14

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 462
    :cond_14
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v0, :cond_15

    sget v4, Lorg/apache/lucene/search/NumericRangeQuery;->INT_POSITIVE_INFINITY:I

    .restart local v4    # "maxBound":I
    :goto_8
    goto :goto_6

    .line 463
    .end local v4    # "maxBound":I
    :cond_15
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v4

    goto :goto_8

    .line 399
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$0(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    return-object v0
.end method

.method private nextRange()V
    .locals 3

    .prologue
    .line 489
    sget-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 491
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentLowerBound:Lorg/apache/lucene/util/BytesRef;

    .line 492
    sget-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentLowerBound:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    .line 493
    const-string v1, "The current upper bound must be <= the new lower bound"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 495
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    .line 496
    return-void
.end method


# virtual methods
.method protected final accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 519
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 528
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    :goto_1
    return-object v0

    .line 520
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_1

    .line 523
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v1, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_2

    .line 524
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_1

    .line 526
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->nextRange()V

    goto :goto_0
.end method

.method protected final nextSeekTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v0, 0x0

    .line 500
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 512
    sget-boolean v1, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 501
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->nextRange()V

    .line 504
    if-eqz p1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v2, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v1, p1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 507
    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentLowerBound:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3

    .line 514
    .end local p1    # "term":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    return-object p1

    .line 508
    .restart local p1    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    iget-object p1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentLowerBound:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 513
    :cond_4
    iput-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentUpperBound:Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermsEnum;->currentLowerBound:Lorg/apache/lucene/util/BytesRef;

    move-object p1, v0

    .line 514
    goto :goto_0
.end method
