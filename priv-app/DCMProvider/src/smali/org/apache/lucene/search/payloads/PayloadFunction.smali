.class public abstract Lorg/apache/lucene/search/payloads/PayloadFunction;
.super Ljava/lang/Object;
.source "PayloadFunction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract currentScore(ILjava/lang/String;IIIFF)F
.end method

.method public abstract docScore(ILjava/lang/String;IF)F
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public explain(ILjava/lang/String;IF)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "docId"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "numPayloadsSeen"    # I
    .param p4, "payloadScore"    # F

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 60
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".docScore()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/payloads/PayloadFunction;->docScore(ILjava/lang/String;IF)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 62
    return-object v0
.end method

.method public abstract hashCode()I
.end method
