.class Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;
.super Lorg/apache/lucene/search/Collector;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

.field private final synthetic val$collector:Lorg/apache/lucene/search/Collector;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;Lorg/apache/lucene/search/Collector;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iput-object p2, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    .line 215
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    return v0
.end method

.method public collect(I)V
    .locals 1
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 225
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 230
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 5
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    new-instance v1, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    # getter for: Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;
    invoke-static {v2}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->access$0(Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;)Lorg/apache/lucene/search/ConstantScoreQuery;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget v4, v4, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->theScore:F

    invoke-direct {v1, v2, p1, v3, v4}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Weight;F)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 220
    return-void
.end method
