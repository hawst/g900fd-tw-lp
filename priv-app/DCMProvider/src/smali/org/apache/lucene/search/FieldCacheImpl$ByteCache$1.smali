.class Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;
.super Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;->createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentValue:B

.field final synthetic this$1:Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;

.field private final synthetic val$parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

.field private final synthetic val$values:[B


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;Lorg/apache/lucene/search/FieldCache$ByteParser;[B)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->this$1:Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->val$values:[B

    .line 423
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;-><init>(Lorg/apache/lucene/search/FieldCacheImpl$Uninvert;)V

    return-void
.end method


# virtual methods
.method protected termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$ByteParser;->termsEnum(Lorg/apache/lucene/index/Terms;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public visitDoc(I)V
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->val$values:[B

    iget-byte v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->currentValue:B

    aput-byte v1, v0, p1

    .line 434
    return-void
.end method

.method public visitTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 428
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->val$parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-interface {v0, p1}, Lorg/apache/lucene/search/FieldCache$ByteParser;->parseByte(Lorg/apache/lucene/util/BytesRef;)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache$1;->currentValue:B

    .line 429
    return-void
.end method
