.class public Lorg/apache/lucene/search/similarities/BasicModelBE;
.super Lorg/apache/lucene/search/similarities/BasicModel;
.source "BasicModelBE.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/BasicModel;-><init>()V

    return-void
.end method

.method private final f(DD)D
    .locals 7
    .param p1, "n"    # D
    .param p3, "m"    # D

    .prologue
    .line 48
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, p3

    div-double v2, p1, p3

    invoke-static {v2, v3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sub-double v2, p1, p3

    invoke-static {p1, p2}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 12
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 39
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    long-to-float v4, v4

    add-float/2addr v4, p2

    float-to-double v0, v4

    .line 41
    .local v0, "F":D
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getNumberOfDocuments()J

    move-result-wide v4

    long-to-double v4, v4

    add-double v2, v0, v4

    .line 42
    .local v2, "N":D
    sub-double v4, v2, v8

    const-wide v6, 0x4005bf0a8b145769L    # Math.E

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Lorg/apache/lucene/search/similarities/SimilarityBase;->log2(D)D

    move-result-wide v4

    neg-double v4, v4

    .line 43
    add-double v6, v2, v0

    sub-double/2addr v6, v8

    add-double v8, v2, v0

    float-to-double v10, p2

    sub-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    sub-double/2addr v8, v10

    invoke-direct {p0, v6, v7, v8, v9}, Lorg/apache/lucene/search/similarities/BasicModelBE;->f(DD)D

    move-result-wide v6

    .line 42
    add-double/2addr v4, v6

    .line 43
    float-to-double v6, p2

    sub-double v6, v0, v6

    invoke-direct {p0, v0, v1, v6, v7}, Lorg/apache/lucene/search/similarities/BasicModelBE;->f(DD)D

    move-result-wide v6

    .line 42
    sub-double/2addr v4, v6

    double-to-float v4, v4

    return v4
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "Be"

    return-object v0
.end method
