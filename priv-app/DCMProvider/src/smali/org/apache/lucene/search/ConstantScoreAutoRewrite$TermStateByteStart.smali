.class final Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;
.super Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;
.source "ConstantScoreAutoRewrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TermStateByteStart"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field termState:[Lorg/apache/lucene/index/TermContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    const-class v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "initSize"    # I

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;-><init>(I)V

    .line 191
    return-void
.end method


# virtual methods
.method public clear()[I
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    .line 216
    invoke-super {p0}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->clear()[I

    move-result-object v0

    return-object v0
.end method

.method public grow()[I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 203
    invoke-super {p0}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->grow()[I

    move-result-object v0

    .line 204
    .local v0, "ord":[I
    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    array-length v2, v2

    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 205
    array-length v2, v0

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v1, v2, [Lorg/apache/lucene/index/TermContext;

    .line 206
    .local v1, "tmpTermState":[Lorg/apache/lucene/index/TermContext;
    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    iput-object v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    .line 209
    .end local v1    # "tmpTermState":[Lorg/apache/lucene/index/TermContext;
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    array-length v2, v2

    array-length v3, v0

    if-ge v2, v3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 210
    :cond_1
    return-object v0
.end method

.method public init()[I
    .locals 3

    .prologue
    .line 195
    invoke-super {p0}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->init()[I

    move-result-object v0

    .line 196
    .local v0, "ord":[I
    array-length v1, v0

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/index/TermContext;

    iput-object v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    .line 197
    sget-boolean v1, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$TermStateByteStart;->termState:[Lorg/apache/lucene/index/TermContext;

    array-length v1, v1

    array-length v2, v0

    if-ge v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 198
    :cond_0
    return-object v0
.end method
