.class abstract Lorg/apache/lucene/search/TermCollectingRewrite;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "TermCollectingRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/Query;",
        ">",
        "Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/search/TermCollectingRewrite;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TermCollectingRewrite;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    .local p0, "this":Lorg/apache/lucene/search/TermCollectingRewrite;, "Lorg/apache/lucene/search/TermCollectingRewrite<TQ;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    return-void
.end method


# virtual methods
.method protected final addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IF)V
    .locals 6
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "docCount"    # I
    .param p4, "boost"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;",
            "Lorg/apache/lucene/index/Term;",
            "IF)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lorg/apache/lucene/search/TermCollectingRewrite;, "Lorg/apache/lucene/search/TermCollectingRewrite<TQ;>;"
    .local p1, "topLevel":Lorg/apache/lucene/search/Query;, "TQ;"
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/TermCollectingRewrite;->addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V

    .line 43
    return-void
.end method

.method protected abstract addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;IFLorg/apache/lucene/index/TermContext;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;",
            "Lorg/apache/lucene/index/Term;",
            "IF",
            "Lorg/apache/lucene/index/TermContext;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method final collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V
    .locals 11
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lorg/apache/lucene/search/TermCollectingRewrite;, "Lorg/apache/lucene/search/TermCollectingRewrite<TQ;>;"
    .local p3, "collector":Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;, "Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v7

    .line 50
    .local v7, "topReaderContext":Lorg/apache/lucene/index/IndexReaderContext;
    const/4 v3, 0x0

    .line 51
    .local v3, "lastTermComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 83
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 52
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v2

    .line 53
    .local v2, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v2, :cond_0

    .line 58
    iget-object v9, p2, Lorg/apache/lucene/search/MultiTermQuery;->field:Ljava/lang/String;

    invoke-virtual {v2, v9}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 59
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v5, :cond_0

    .line 64
    iget-object v9, p3, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->attributes:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {p0, p2, v5, v9}, Lorg/apache/lucene/search/TermCollectingRewrite;->getTermsEnum(Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v6

    .line 65
    .local v6, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    sget-boolean v9, Lorg/apache/lucene/search/TermCollectingRewrite;->$assertionsDisabled:Z

    if-nez v9, :cond_2

    if-nez v6, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 67
    :cond_2
    sget-object v9, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    if-eq v6, v9, :cond_0

    .line 71
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v4

    .line 72
    .local v4, "newTermComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    if-eq v4, v3, :cond_3

    .line 73
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "term comparator should not change between segments: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " != "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 74
    :cond_3
    move-object v3, v4

    .line 75
    invoke-virtual {p3, v7, v1}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->setReaderContext(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 76
    invoke-virtual {p3, v6}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->setNextEnum(Lorg/apache/lucene/index/TermsEnum;)V

    .line 78
    :cond_4
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p3, v0}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->collect(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v9

    if-nez v9, :cond_4

    goto :goto_0
.end method

.method protected abstract getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TQ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
