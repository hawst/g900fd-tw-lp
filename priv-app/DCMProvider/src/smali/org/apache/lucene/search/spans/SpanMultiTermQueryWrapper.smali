.class public Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;,
        Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/MultiTermQuery;",
        ">",
        "Lorg/apache/lucene/search/spans/SpanQuery;"
    }
.end annotation


# static fields
.field public static final SCORING_SPAN_QUERY_REWRITE:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;


# instance fields
.field protected final query:Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TQ;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->SCORING_SPAN_QUERY_REWRITE:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    .line 176
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/MultiTermQuery;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    .local p1, "query":Lorg/apache/lucene/search/MultiTermQuery;, "TQ;"
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 65
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    .line 67
    invoke-virtual {p1}, Lorg/apache/lucene/search/MultiTermQuery;->getRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    move-result-object v0

    .line 68
    .local v0, "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    instance-of v2, v0, Lorg/apache/lucene/search/TopTermsRewrite;

    if-eqz v2, :cond_0

    .line 69
    check-cast v0, Lorg/apache/lucene/search/TopTermsRewrite;

    .end local v0    # "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    invoke-virtual {v0}, Lorg/apache/lucene/search/TopTermsRewrite;->getSize()I

    move-result v1

    .line 70
    .local v1, "pqsize":I
    new-instance v2, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;

    invoke-direct {v2, v1}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;-><init>(I)V

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->setRewriteMethod(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;)V

    .line 74
    .end local v1    # "pqsize":I
    :goto_0
    return-void

    .line 72
    .restart local v0    # "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :cond_0
    sget-object v2, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->SCORING_SPAN_QUERY_REWRITE:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->setRewriteMethod(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    const/4 v1, 0x0

    .line 129
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 133
    :cond_0
    :goto_0
    return v1

    .line 130
    :cond_1
    if-eqz p1, :cond_0

    .line 131
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 132
    check-cast v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;

    .line 133
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    iget-object v2, v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRewriteMethod()Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;
    .locals 3

    .prologue
    .line 80
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/MultiTermQuery;->getRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    move-result-object v0

    .line 81
    .local v0, "m":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    instance-of v1, v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    if-nez v1, :cond_0

    .line 82
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "You can only use SpanMultiTermQueryWrapper with a suitable SpanRewriteMethod."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_0
    check-cast v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    .end local v0    # "m":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Ljava/util/Map;)Lorg/apache/lucene/search/spans/Spans;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            "Lorg/apache/lucene/util/Bits;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Lorg/apache/lucene/index/TermContext;",
            ">;)",
            "Lorg/apache/lucene/search/spans/Spans;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    .local p3, "termContexts":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Query should have been rewritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/MultiTermQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 116
    .local v0, "q":Lorg/apache/lucene/search/Query;
    instance-of v1, v0, Lorg/apache/lucene/search/spans/SpanQuery;

    if-nez v1, :cond_0

    .line 117
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "You can only use SpanMultiTermQueryWrapper with a suitable SpanRewriteMethod."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 118
    :cond_0
    return-object v0
.end method

.method public final setRewriteMethod(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;)V
    .locals 1

    .prologue
    .line 91
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    .local p1, "rewriteMethod":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 92
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 106
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "SpanMultiTermQueryWrapper("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/MultiTermQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
