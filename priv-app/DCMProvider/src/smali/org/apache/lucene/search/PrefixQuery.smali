.class public Lorg/apache/lucene/search/PrefixQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "PrefixQuery.java"


# instance fields
.field private prefix:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "prefix"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 39
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>(Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    .line 41
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p0, p1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v1

    .line 83
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 84
    goto :goto_0

    .line 85
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 86
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 87
    check-cast v0, Lorg/apache/lucene/search/PrefixQuery;

    .line 88
    .local v0, "other":Lorg/apache/lucene/search/PrefixQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_4

    .line 89
    iget-object v3, v0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 90
    goto :goto_0

    .line 91
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 92
    goto :goto_0
.end method

.method public getPrefix()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method protected getTermsEnum(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/util/AttributeSource;)Lorg/apache/lucene/index/TermsEnum;
    .locals 3
    .param p1, "terms"    # Lorg/apache/lucene/index/Terms;
    .param p2, "atts"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    .line 50
    .local v0, "tenum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v1, :cond_0

    .line 54
    .end local v0    # "tenum":Lorg/apache/lucene/index/TermsEnum;
    :goto_0
    return-object v0

    .restart local v0    # "tenum":Lorg/apache/lucene/index/TermsEnum;
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/PrefixTermsEnum;

    iget-object v2, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/lucene/search/PrefixTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 73
    const/16 v0, 0x1f

    .line 74
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 75
    .local v1, "result":I
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 76
    return v1

    .line 75
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PrefixQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/search/PrefixQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/search/PrefixQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
