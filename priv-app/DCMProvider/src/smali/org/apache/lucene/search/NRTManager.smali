.class public final Lorg/apache/lucene/search/NRTManager;
.super Lorg/apache/lucene/search/ReferenceManager;
.source "NRTManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;,
        Lorg/apache/lucene/search/NRTManager$WaitingListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ReferenceManager",
        "<",
        "Lorg/apache/lucene/search/IndexSearcher;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MAX_SEARCHER_GEN:J = 0x7fffffffffffffffL


# instance fields
.field private final genLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private lastRefreshGen:J

.field private final newGeneration:Ljava/util/concurrent/locks/Condition;

.field private final searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

.field private volatile searchingGen:J

.field private final waitingListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/NRTManager$WaitingListener;",
            ">;"
        }
    .end annotation
.end field

.field private final writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    .line 72
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;
    .param p2, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/NRTManager;-><init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;Z)V

    .line 92
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;Z)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;
    .param p2, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .param p3, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 74
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 76
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    .line 102
    iput-object p1, p0, Lorg/apache/lucene/search/NRTManager;->writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    .line 103
    if-nez p2, :cond_0

    .line 104
    new-instance p2, Lorg/apache/lucene/search/SearcherFactory;

    .end local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p2}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 106
    .restart local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/NRTManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 107
    invoke-virtual {p1}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getIndexWriter()Lorg/apache/lucene/index/IndexWriter;

    move-result-object v0

    invoke-static {v0, p3}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    invoke-static {p2, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->current:Ljava/lang/Object;

    .line 108
    return-void
.end method

.method private waitOnGenCondition(JLjava/util/concurrent/TimeUnit;)Z
    .locals 3
    .param p1, "time"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 327
    sget-boolean v0, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 328
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 329
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V

    .line 330
    const/4 v0, 0x1

    .line 332
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public addWaitingListener(Lorg/apache/lucene/search/NRTManager$WaitingListener;)V
    .locals 1
    .param p1, "l"    # Lorg/apache/lucene/search/NRTManager$WaitingListener;

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-void
.end method

.method protected declared-synchronized afterClose()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 379
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 382
    const-wide v0, 0x7fffffffffffffffL

    :try_start_1
    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    .line 383
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387
    monitor-exit p0

    return-void

    .line 384
    :catchall_0
    move-exception v0

    .line 385
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 386
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 379
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected afterMaybeRefresh()V
    .locals 4

    .prologue
    .line 363
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 365
    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 367
    sget-boolean v0, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->lastRefreshGen:J

    iget-wide v2, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    :catchall_0
    move-exception v0

    .line 373
    iget-object v1, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 374
    throw v0

    .line 368
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->lastRefreshGen:J

    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    .line 371
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 375
    return-void
.end method

.method protected bridge synthetic decRef(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/NRTManager;->decRef(Lorg/apache/lucene/search/IndexSearcher;)V

    return-void
.end method

.method protected decRef(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 113
    return-void
.end method

.method public getCurrentSearchingGen()J
    .locals 2

    .prologue
    .line 338
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    return-wide v0
.end method

.method public isSearcherCurrent()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    invoke-virtual {p0}, Lorg/apache/lucene/search/NRTManager;->acquire()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/IndexSearcher;

    .line 397
    .local v1, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 398
    .local v0, "r":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v2, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    instance-of v2, v0, Lorg/apache/lucene/index/DirectoryReader;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "searcher\'s IndexReader should be a DirectoryReader, but got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    .end local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v2

    .line 401
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/NRTManager;->release(Ljava/lang/Object;)V

    .line 402
    throw v2

    .line 399
    .restart local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    :try_start_1
    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;

    .end local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->isCurrent()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 401
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/NRTManager;->release(Ljava/lang/Object;)V

    .line 399
    return v2
.end method

.method protected bridge synthetic refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/NRTManager;->refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    return-object v0
.end method

.method protected refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 7
    .param p1, "referenceToRefresh"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getAndIncrementGeneration()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/search/NRTManager;->lastRefreshGen:J

    .line 347
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v3

    .line 348
    .local v3, "r":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v4, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    instance-of v4, v3, Lorg/apache/lucene/index/DirectoryReader;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "searcher\'s IndexReader should be a DirectoryReader, but got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    :cond_0
    move-object v0, v3

    .line 349
    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;

    .line 350
    .local v0, "dirReader":Lorg/apache/lucene/index/DirectoryReader;
    const/4 v2, 0x0

    .line 351
    .local v2, "newSearcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->isCurrent()Z

    move-result v4

    if-nez v4, :cond_1

    .line 352
    invoke-static {v0}, Lorg/apache/lucene/index/DirectoryReader;->openIfChanged(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v1

    .line 353
    .local v1, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v1, :cond_1

    .line 354
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    invoke-static {v4, v1}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v2

    .line 358
    .end local v1    # "newReader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    return-object v2
.end method

.method public removeWaitingListener(Lorg/apache/lucene/search/NRTManager$WaitingListener;)V
    .locals 1
    .param p1, "l"    # Lorg/apache/lucene/search/NRTManager$WaitingListener;

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 138
    return-void
.end method

.method protected bridge synthetic tryIncRef(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/NRTManager;->tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z

    move-result v0

    return v0
.end method

.method protected tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 117
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v0

    return v0
.end method

.method public waitForGeneration(J)V
    .locals 7
    .param p1, "targetGen"    # J

    .prologue
    .line 278
    const-wide/16 v4, -0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/search/NRTManager;->waitForGeneration(JJLjava/util/concurrent/TimeUnit;)V

    .line 279
    return-void
.end method

.method public waitForGeneration(JJLjava/util/concurrent/TimeUnit;)V
    .locals 7
    .param p1, "targetGen"    # J
    .param p3, "time"    # J
    .param p5, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 301
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getGeneration()J

    move-result-wide v0

    .line 302
    .local v0, "curGen":J
    cmp-long v4, p1, v0

    if-lez v4, :cond_0

    .line 303
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "targetGen="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " was never returned by this NRTManager instance (current gen="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    .end local v0    # "curGen":J
    :catch_0
    move-exception v2

    .line 321
    .local v2, "ie":Ljava/lang/InterruptedException;
    new-instance v4, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v4, v2}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v4

    .line 305
    .end local v2    # "ie":Ljava/lang/InterruptedException;
    .restart local v0    # "curGen":J
    :cond_0
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 307
    :try_start_2
    iget-wide v4, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    cmp-long v4, p1, v4

    if-lez v4, :cond_2

    .line 308
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 311
    :cond_1
    iget-wide v4, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    cmp-long v4, p1, v4

    if-gtz v4, :cond_4

    .line 318
    :cond_2
    :try_start_3
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 323
    :goto_1
    return-void

    .line 308
    :cond_3
    :try_start_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/NRTManager$WaitingListener;

    .line 309
    .local v3, "listener":Lorg/apache/lucene/search/NRTManager$WaitingListener;
    invoke-interface {v3, p1, p2}, Lorg/apache/lucene/search/NRTManager$WaitingListener;->waiting(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 317
    .end local v3    # "listener":Lorg/apache/lucene/search/NRTManager$WaitingListener;
    :catchall_0
    move-exception v4

    .line 318
    :try_start_5
    iget-object v5, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 319
    throw v4
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 312
    :cond_4
    :try_start_6
    invoke-direct {p0, p3, p4, p5}, Lorg/apache/lucene/search/NRTManager;->waitOnGenCondition(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 318
    :try_start_7
    iget-object v4, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_1
.end method
