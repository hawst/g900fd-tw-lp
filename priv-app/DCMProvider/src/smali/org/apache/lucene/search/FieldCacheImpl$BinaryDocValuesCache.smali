.class final Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "BinaryDocValuesCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 1290
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 1291
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
    .locals 24
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1301
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v9

    .line 1302
    .local v9, "maxDoc":I
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v19

    .line 1304
    .local v19, "terms":Lorg/apache/lucene/index/Terms;
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    move-object/from16 v21, v0

    check-cast v21, Ljava/lang/Float;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 1306
    .local v4, "acceptableOverheadRatio":F
    move/from16 v18, v9

    .line 1309
    .local v18, "termCountHardLimit":I
    new-instance v5, Lorg/apache/lucene/util/PagedBytes;

    const/16 v21, 0xf

    move/from16 v0, v21

    invoke-direct {v5, v0}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 1313
    .local v5, "bytes":Lorg/apache/lucene/util/PagedBytes;
    if-eqz v19, :cond_3

    .line 1317
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/Terms;->size()J

    move-result-wide v10

    .line 1318
    .local v10, "numUniqueTerms":J
    const-wide/16 v22, -0x1

    cmp-long v21, v10, v22

    if-eqz v21, :cond_2

    .line 1319
    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v22, v0

    cmp-long v21, v10, v22

    if-lez v21, :cond_0

    .line 1320
    move/from16 v0, v18

    int-to-long v10, v0

    .line 1322
    :cond_0
    const-wide/16 v22, 0x4

    mul-long v22, v22, v10

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v14

    .line 1330
    .end local v10    # "numUniqueTerms":J
    .local v14, "startBPV":I
    :goto_0
    new-instance v7, Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-direct {v7, v14, v9, v4}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    .line 1333
    .local v7, "docToOffset":Lorg/apache/lucene/util/packed/GrowableWriter;
    new-instance v21, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lorg/apache/lucene/util/PagedBytes;->copyUsingLengthPrefix(Lorg/apache/lucene/util/BytesRef;)J

    .line 1335
    if-eqz v19, :cond_1

    .line 1336
    const/16 v16, 0x0

    .line 1337
    .local v16, "termCount":I
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v20

    .line 1338
    .local v20, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v8, 0x0

    .local v8, "docs":Lorg/apache/lucene/index/DocsEnum;
    move/from16 v17, v16

    .line 1340
    .end local v16    # "termCount":I
    .local v17, "termCount":I
    :goto_1
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "termCount":I
    .restart local v16    # "termCount":I
    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 1364
    .end local v8    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v16    # "termCount":I
    .end local v20    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_1
    new-instance v21, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;

    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v22

    invoke-virtual {v7}, Lorg/apache/lucene/util/packed/GrowableWriter;->getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v23

    invoke-direct/range {v21 .. v23}, Lorg/apache/lucene/search/FieldCacheImpl$BinaryDocValuesImpl;-><init>(Lorg/apache/lucene/util/PagedBytes$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;)V

    return-object v21

    .line 1324
    .end local v7    # "docToOffset":Lorg/apache/lucene/util/packed/GrowableWriter;
    .end local v14    # "startBPV":I
    .restart local v10    # "numUniqueTerms":J
    :cond_2
    const/4 v14, 0x1

    .line 1326
    .restart local v14    # "startBPV":I
    goto :goto_0

    .line 1327
    .end local v10    # "numUniqueTerms":J
    .end local v14    # "startBPV":I
    :cond_3
    const/4 v14, 0x1

    .restart local v14    # "startBPV":I
    goto :goto_0

    .line 1347
    .restart local v7    # "docToOffset":Lorg/apache/lucene/util/packed/GrowableWriter;
    .restart local v8    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v16    # "termCount":I
    .restart local v20    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_4
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v15

    .line 1348
    .local v15, "term":Lorg/apache/lucene/util/BytesRef;
    if-eqz v15, :cond_1

    .line 1351
    invoke-virtual {v5, v15}, Lorg/apache/lucene/util/PagedBytes;->copyUsingLengthPrefix(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v12

    .line 1352
    .local v12, "pointer":J
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v8, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v8

    .line 1354
    :goto_2
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v6

    .line 1355
    .local v6, "docID":I
    const v21, 0x7fffffff

    move/from16 v0, v21

    if-ne v6, v0, :cond_5

    move/from16 v17, v16

    .line 1356
    .end local v16    # "termCount":I
    .restart local v17    # "termCount":I
    goto :goto_1

    .line 1358
    .end local v17    # "termCount":I
    .restart local v16    # "termCount":I
    :cond_5
    invoke-virtual {v7, v6, v12, v13}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    goto :goto_2
.end method
