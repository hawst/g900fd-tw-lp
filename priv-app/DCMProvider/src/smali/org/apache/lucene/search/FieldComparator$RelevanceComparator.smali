.class public final Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RelevanceComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bottom:F

.field private scorer:Lorg/apache/lucene/search/Scorer;

.field private final scores:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 722
    const-class v0, Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "numHits"    # I

    .prologue
    .line 727
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 728
    new-array v0, p1, [F

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    .line 729
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 2
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 733
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v0, p2

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v1, v1, p1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 738
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 739
    .local v0, "score":F
    sget-boolean v1, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 740
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->bottom:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    return v1
.end method

.method public compareDocToValue(ILjava/lang/Float;)I
    .locals 3
    .param p1, "doc"    # I
    .param p2, "valueObj"    # Ljava/lang/Float;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 786
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 787
    .local v1, "value":F
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 788
    .local v0, "docValue":F
    sget-boolean v2, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 789
    :cond_0
    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->compareDocToValue(ILjava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public compareValues(Ljava/lang/Float;Ljava/lang/Float;)I
    .locals 1
    .param p1, "first"    # Ljava/lang/Float;
    .param p2, "second"    # Ljava/lang/Float;

    .prologue
    .line 781
    invoke-virtual {p2, p1}, Ljava/lang/Float;->compareTo(Ljava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Float;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->compareValues(Ljava/lang/Float;Ljava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 745
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    aput v1, v0, p1

    .line 746
    sget-boolean v0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 747
    :cond_0
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 756
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->bottom:F

    .line 757
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 0
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 751
    return-object p0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 764
    instance-of v0, p1, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;

    if-nez v0, :cond_0

    .line 765
    new-instance v0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;-><init>(Lorg/apache/lucene/search/Scorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 769
    :goto_0
    return-void

    .line 767
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Float;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 773
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->value(I)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
