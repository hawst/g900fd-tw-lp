.class final Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;
.super Ljava/lang/Object;
.source "IndexSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/IndexSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SearcherCallableWithSort"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/lucene/search/TopFieldDocs;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final after:Lorg/apache/lucene/search/FieldDoc;

.field private final doDocScores:Z

.field private final doMaxScore:Z

.field private final fakeScorer:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;

.field private final hq:Lorg/apache/lucene/search/TopFieldCollector;

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private final nDocs:I

.field private final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field private final slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

.field private final sort:Lorg/apache/lucene/search/Sort;

.field private final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 742
    const-class v0, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/IndexSearcher$LeafSlice;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/TopFieldCollector;Lorg/apache/lucene/search/Sort;ZZ)V
    .locals 1
    .param p1, "lock"    # Ljava/util/concurrent/locks/Lock;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "slice"    # Lorg/apache/lucene/search/IndexSearcher$LeafSlice;
    .param p4, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p5, "after"    # Lorg/apache/lucene/search/FieldDoc;
    .param p6, "nDocs"    # I
    .param p7, "hq"    # Lorg/apache/lucene/search/TopFieldCollector;
    .param p8, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p9, "doDocScores"    # Z
    .param p10, "doMaxScore"    # Z

    .prologue
    .line 755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 809
    new-instance v0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;-><init>(Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;)V

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;

    .line 758
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    .line 759
    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 760
    iput-object p4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->weight:Lorg/apache/lucene/search/Weight;

    .line 761
    iput p6, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->nDocs:I

    .line 762
    iput-object p7, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    .line 763
    iput-object p8, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    .line 764
    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    .line 765
    iput-object p5, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->after:Lorg/apache/lucene/search/FieldDoc;

    .line 766
    iput-boolean p9, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->doDocScores:Z

    .line 767
    iput-boolean p10, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->doMaxScore:Z

    .line 768
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->call()Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/apache/lucene/search/TopFieldDocs;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v13, 0x0

    .line 813
    sget-boolean v0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    iget-object v0, v0, Lorg/apache/lucene/search/IndexSearcher$LeafSlice;->leaves:[Lorg/apache/lucene/index/AtomicReaderContext;

    array-length v0, v0

    if-eq v0, v6, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 814
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    iget-object v1, v1, Lorg/apache/lucene/search/IndexSearcher$LeafSlice;->leaves:[Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 815
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->after:Lorg/apache/lucene/search/FieldDoc;

    iget v4, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->nDocs:I

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    iget-boolean v7, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->doDocScores:Z

    if-nez v7, :cond_2

    iget-object v7, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    invoke-virtual {v7}, Lorg/apache/lucene/search/Sort;->needsScores()Z

    move-result v7

    if-nez v7, :cond_2

    move v7, v13

    :goto_0
    iget-boolean v8, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->doMaxScore:Z

    .line 814
    invoke-virtual/range {v0 .. v8}, Lorg/apache/lucene/search/IndexSearcher;->search(Ljava/util/List;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZZ)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v11

    .line 816
    .local v11, "docs":Lorg/apache/lucene/search/TopFieldDocs;
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 818
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->slice:Lorg/apache/lucene/search/IndexSearcher$LeafSlice;

    iget-object v0, v0, Lorg/apache/lucene/search/IndexSearcher$LeafSlice;->leaves:[Lorg/apache/lucene/index/AtomicReaderContext;

    const/4 v1, 0x0

    aget-object v10, v0, v1

    .line 819
    .local v10, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    iget v9, v10, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 820
    .local v9, "base":I
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    invoke-virtual {v0, v10}, Lorg/apache/lucene/search/TopFieldCollector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 821
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/TopFieldCollector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 822
    iget-object v0, v11, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v1, v0

    :goto_1
    if-lt v13, v1, :cond_3

    .line 829
    iget-boolean v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->doMaxScore:Z

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    iget v1, v1, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 830
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    invoke-virtual {v11}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v1

    iput v1, v0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 833
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 835
    return-object v11

    .end local v9    # "base":I
    .end local v10    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v11    # "docs":Lorg/apache/lucene/search/TopFieldDocs;
    :cond_2
    move v7, v6

    .line 815
    goto :goto_0

    .line 822
    .restart local v9    # "base":I
    .restart local v10    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .restart local v11    # "docs":Lorg/apache/lucene/search/TopFieldDocs;
    :cond_3
    :try_start_1
    aget-object v12, v0, v13

    .line 823
    .local v12, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;

    iget v3, v12, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    sub-int/2addr v3, v9

    iput v3, v2, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;->doc:I

    .line 824
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;

    iget v3, v12, Lorg/apache/lucene/search/ScoreDoc;->score:F

    iput v3, v2, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort$FakeScorer;->score:F

    .line 825
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    iget v3, v12, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    sub-int/2addr v3, v9

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/TopFieldCollector;->collect(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 822
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 832
    .end local v9    # "base":I
    .end local v10    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v12    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catchall_0
    move-exception v0

    .line 833
    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher$SearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 834
    throw v0
.end method
