.class Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OneComparatorScoringNoMaxScoreCollector"
.end annotation


# instance fields
.field scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 166
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->totalHits:I

    .line 177
    iget-boolean v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v2, :cond_2

    .line 178
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->reverseMul:I

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v3

    mul-int/2addr v2, v3

    if-gtz v2, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 189
    .local v0, "score":F
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 190
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->updateBottom(IF)V

    .line 191
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0

    .line 194
    .end local v0    # "score":F
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 197
    .restart local v0    # "score":F
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v1, v2, -0x1

    .line 199
    .local v1, "slot":I
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v2, v1, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 200
    invoke-virtual {p0, v1, p1, v0}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->add(IIF)V

    .line 201
    iget-boolean v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v2, :cond_0

    .line 202
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 210
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldComparator;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 211
    return-void
.end method

.method final updateBottom(IF)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 170
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput p2, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    .line 171
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 172
    return-void
.end method
