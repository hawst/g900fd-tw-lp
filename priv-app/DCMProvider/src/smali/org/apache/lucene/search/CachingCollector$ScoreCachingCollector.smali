.class final Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;
.super Lorg/apache/lucene/search/CachingCollector;
.source "CachingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/CachingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ScoreCachingCollector"
.end annotation


# instance fields
.field private final cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

.field private final cachedScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[F>;"
        }
    .end annotation
.end field

.field private curScores:[F

.field private scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Collector;D)V
    .locals 6
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .param p2, "maxRAMMB"    # D

    .prologue
    const/4 v5, 0x0

    .line 107
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/CachingCollector;-><init>(Lorg/apache/lucene/search/Collector;DZLorg/apache/lucene/search/CachingCollector;)V

    .line 109
    new-instance v0, Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    invoke-direct {v0, v5}, Lorg/apache/lucene/search/CachingCollector$CachedScorer;-><init>(Lorg/apache/lucene/search/CachingCollector$CachedScorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    .line 111
    const/16 v0, 0x80

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/search/Collector;I)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .param p2, "maxDocsToCache"    # I

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/search/CachingCollector;-><init>(Lorg/apache/lucene/search/Collector;ILorg/apache/lucene/search/CachingCollector;)V

    .line 118
    new-instance v0, Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/CachingCollector$CachedScorer;-><init>(Lorg/apache/lucene/search/CachingCollector$CachedScorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    .line 120
    const/16 v0, 0x80

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    if-nez v1, :cond_0

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    iput v2, v1, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->score:F

    .line 130
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iput p1, v1, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->doc:I

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 174
    :goto_0
    return-void

    .line 136
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    array-length v2, v2

    if-ne v1, v2, :cond_3

    .line 137
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->base:I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->base:I

    .line 140
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    array-length v1, v1

    mul-int/lit8 v0, v1, 0x8

    .line 141
    .local v0, "nextLength":I
    const/high16 v1, 0x80000

    if-le v0, v1, :cond_1

    .line 142
    const/high16 v0, 0x80000

    .line 145
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->base:I

    add-int/2addr v1, v0

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->maxDocsToCache:I

    if-le v1, v2, :cond_2

    .line 147
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->maxDocsToCache:I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->base:I

    sub-int v0, v1, v2

    .line 148
    if-gtz v0, :cond_2

    .line 150
    iput-object v3, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    .line 151
    iput-object v3, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    .line 152
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedSegs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 153
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedDocs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 154
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 155
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    iput v2, v1, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->score:F

    .line 156
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iput p1, v1, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->doc:I

    .line 157
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0

    .line 162
    :cond_2
    new-array v1, v0, [I

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    .line 163
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedDocs:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    new-array v1, v0, [F

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    .line 165
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    .line 169
    .end local v0    # "nextLength":I
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    aput p1, v1, v2

    .line 170
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    iget v3, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    iget-object v4, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v4

    aput v4, v2, v3

    iput v4, v1, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->score:F

    .line 171
    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iput p1, v1, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->doc:I

    .line 173
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto/16 :goto_0
.end method

.method public replay(Lorg/apache/lucene/search/Collector;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->replayInit(Lorg/apache/lucene/search/Collector;)V

    .line 180
    const/4 v2, 0x0

    .line 181
    .local v2, "curUpto":I
    const/4 v1, 0x0

    .line 182
    .local v1, "curBase":I
    const/4 v0, 0x0

    .line 183
    .local v0, "chunkUpto":I
    # getter for: Lorg/apache/lucene/search/CachingCollector;->EMPTY_INT_ARRAY:[I
    invoke-static {}, Lorg/apache/lucene/search/CachingCollector;->access$2()[I

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    .line 184
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedSegs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 200
    return-void

    .line 184
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/CachingCollector$SegStart;

    .line 185
    .local v4, "seg":Lorg/apache/lucene/search/CachingCollector$SegStart;
    iget-object v5, v4, Lorg/apache/lucene/search/CachingCollector$SegStart;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 186
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 187
    :goto_0
    add-int v5, v1, v2

    iget v7, v4, Lorg/apache/lucene/search/CachingCollector$SegStart;->end:I

    if-ge v5, v7, :cond_0

    .line 188
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    array-length v5, v5

    if-ne v2, v5, :cond_2

    .line 189
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    array-length v5, v5

    add-int/2addr v1, v5

    .line 190
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedDocs:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    iput-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    .line 191
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScores:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [F

    iput-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    .line 192
    add-int/lit8 v0, v0, 0x1

    .line 193
    const/4 v2, 0x0

    .line 195
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iget-object v7, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curScores:[F

    aget v7, v7, v2

    iput v7, v5, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->score:F

    .line 196
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    iget-object v7, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    aget v7, v7, v2

    iput v7, v5, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->doc:I

    .line 197
    iget-object v5, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->curDocs:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "curUpto":I
    .local v3, "curUpto":I
    aget v5, v5, v2

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->collect(I)V

    move v2, v3

    .end local v3    # "curUpto":I
    .restart local v2    # "curUpto":I
    goto :goto_0
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iput-object p1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 205
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->other:Lorg/apache/lucene/search/Collector;

    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->cachedScorer:Lorg/apache/lucene/search/CachingCollector$CachedScorer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 206
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 210
    invoke-virtual {p0}, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->isCached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CachingCollector ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->base:I

    iget v2, p0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;->upto:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " docs & scores cached)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CachingCollector (cache was cleared)"

    goto :goto_0
.end method
