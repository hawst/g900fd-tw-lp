.class public final Lorg/apache/lucene/search/NumericRangeFilter;
.super Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.source "NumericRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Number;",
        ">",
        "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter",
        "<",
        "Lorg/apache/lucene/search/NumericRangeQuery",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lorg/apache/lucene/search/NumericRangeQuery;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/NumericRangeQuery",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeFilter;, "Lorg/apache/lucene/search/NumericRangeFilter<TT;>;"
    .local p1, "query":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    .line 52
    return-void
.end method

.method public static newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Double;
    .param p3, "max"    # Ljava/lang/Double;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 127
    invoke-static/range {p0 .. p5}, Lorg/apache/lucene/search/NumericRangeQuery;->newDoubleRange(Ljava/lang/String;ILjava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 126
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newDoubleRange(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Double;
    .param p2, "max"    # Ljava/lang/Double;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 144
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/NumericRangeQuery;->newDoubleRange(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 143
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newFloatRange(Ljava/lang/String;ILjava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Float;
    .param p3, "max"    # Ljava/lang/Float;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 161
    invoke-static/range {p0 .. p5}, Lorg/apache/lucene/search/NumericRangeQuery;->newFloatRange(Ljava/lang/String;ILjava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 160
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newFloatRange(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Float;
    .param p2, "max"    # Ljava/lang/Float;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 178
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/NumericRangeQuery;->newFloatRange(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 177
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newIntRange(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Integer;
    .param p3, "max"    # Ljava/lang/Integer;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 95
    invoke-static/range {p0 .. p5}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 94
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Integer;
    .param p2, "max"    # Ljava/lang/Integer;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 110
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 109
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newLongRange(Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "precisionStep"    # I
    .param p2, "min"    # Ljava/lang/Long;
    .param p3, "max"    # Ljava/lang/Long;
    .param p4, "minInclusive"    # Z
    .param p5, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 65
    invoke-static/range {p0 .. p5}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 64
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method

.method public static newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;
    .locals 2
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "min"    # Ljava/lang/Long;
    .param p2, "max"    # Ljava/lang/Long;
    .param p3, "minInclusive"    # Z
    .param p4, "maxInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "ZZ)",
            "Lorg/apache/lucene/search/NumericRangeFilter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lorg/apache/lucene/search/NumericRangeFilter;

    .line 80
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 79
    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NumericRangeFilter;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;)V

    return-object v0
.end method


# virtual methods
.method public getMax()Ljava/lang/Number;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 192
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeFilter;, "Lorg/apache/lucene/search/NumericRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/NumericRangeQuery;->getMax()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public getMin()Ljava/lang/Number;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 189
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeFilter;, "Lorg/apache/lucene/search/NumericRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/NumericRangeQuery;->getMin()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public getPrecisionStep()I
    .locals 1

    .prologue
    .line 195
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeFilter;, "Lorg/apache/lucene/search/NumericRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/NumericRangeQuery;->getPrecisionStep()I

    move-result v0

    return v0
.end method

.method public includesMax()Z
    .locals 1

    .prologue
    .line 186
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeFilter;, "Lorg/apache/lucene/search/NumericRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/NumericRangeQuery;->includesMax()Z

    move-result v0

    return v0
.end method

.method public includesMin()Z
    .locals 1

    .prologue
    .line 183
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeFilter;, "Lorg/apache/lucene/search/NumericRangeFilter<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/NumericRangeQuery;->includesMin()Z

    move-result v0

    return v0
.end method
