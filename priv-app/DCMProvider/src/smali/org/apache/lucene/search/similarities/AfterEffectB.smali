.class public Lorg/apache/lucene/search/similarities/AfterEffectB;
.super Lorg/apache/lucene/search/similarities/AfterEffect;
.source "AfterEffectB.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/AfterEffect;-><init>()V

    return-void
.end method


# virtual methods
.method public final explain(Lorg/apache/lucene/search/similarities/BasicStats;F)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 41
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ", computed from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/similarities/AfterEffectB;->score(Lorg/apache/lucene/search/similarities/BasicStats;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 43
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    const-string v2, "tfn"

    invoke-direct {v1, p2, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 44
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v2

    long-to-float v2, v2

    const-string v3, "totalTermFreq"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 45
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getDocFreq()J

    move-result-wide v2

    long-to-float v2, v2

    const-string v3, "docFreq"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 46
    return-object v0
.end method

.method public final score(Lorg/apache/lucene/search/similarities/BasicStats;F)F
    .locals 8
    .param p1, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p2, "tfn"    # F

    .prologue
    const-wide/16 v6, 0x1

    .line 33
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getTotalTermFreq()J

    move-result-wide v4

    add-long v0, v4, v6

    .line 34
    .local v0, "F":J
    invoke-virtual {p1}, Lorg/apache/lucene/search/similarities/BasicStats;->getDocFreq()J

    move-result-wide v4

    add-long v2, v4, v6

    .line 35
    .local v2, "n":J
    add-long v4, v0, v6

    long-to-float v4, v4

    long-to-float v5, v2

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v6, p2

    mul-float/2addr v5, v6

    div-float/2addr v4, v5

    return v4
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "B"

    return-object v0
.end method
