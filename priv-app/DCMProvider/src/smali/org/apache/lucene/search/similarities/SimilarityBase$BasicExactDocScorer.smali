.class Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;
.super Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;
.source "SimilarityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/similarities/SimilarityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BasicExactDocScorer"
.end annotation


# instance fields
.field private final norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final stats:Lorg/apache/lucene/search/similarities/BasicStats;

.field final synthetic this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/similarities/SimilarityBase;Lorg/apache/lucene/search/similarities/BasicStats;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 0
    .param p2, "stats"    # Lorg/apache/lucene/search/similarities/BasicStats;
    .param p3, "norms"    # Lorg/apache/lucene/index/NumericDocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    iput-object p1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    invoke-direct {p0}, Lorg/apache/lucene/search/similarities/Similarity$ExactSimScorer;-><init>()V

    .line 291
    iput-object p2, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->stats:Lorg/apache/lucene/search/similarities/BasicStats;

    .line 292
    iput-object p3, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 293
    return-void
.end method


# virtual methods
.method public explain(ILorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 304
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->stats:Lorg/apache/lucene/search/similarities/BasicStats;

    .line 305
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 304
    :goto_0
    invoke-virtual {v1, v2, p1, p2, v0}, Lorg/apache/lucene/search/similarities/SimilarityBase;->explain(Lorg/apache/lucene/search/similarities/BasicStats;ILorg/apache/lucene/search/Explanation;F)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0

    .line 305
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v3, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v4

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/similarities/SimilarityBase;->decodeNormValue(B)F

    move-result v0

    goto :goto_0
.end method

.method public score(II)F
    .locals 6
    .param p1, "doc"    # I
    .param p2, "freq"    # I

    .prologue
    .line 298
    iget-object v1, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v2, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->stats:Lorg/apache/lucene/search/similarities/BasicStats;

    int-to-float v3, p2

    .line 299
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 298
    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lorg/apache/lucene/search/similarities/SimilarityBase;->score(Lorg/apache/lucene/search/similarities/BasicStats;FF)F

    move-result v0

    return v0

    .line 299
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->this$0:Lorg/apache/lucene/search/similarities/SimilarityBase;

    iget-object v4, p0, Lorg/apache/lucene/search/similarities/SimilarityBase$BasicExactDocScorer;->norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/similarities/SimilarityBase;->decodeNormValue(B)F

    move-result v0

    goto :goto_0
.end method
