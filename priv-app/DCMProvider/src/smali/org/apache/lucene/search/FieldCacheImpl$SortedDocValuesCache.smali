.class Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SortedDocValuesCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 1132
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 1133
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;Z)Ljava/lang/Object;
    .locals 27
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1139
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v11

    .line 1141
    .local v11, "maxDoc":I
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->field:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v21

    .line 1143
    .local v21, "terms":Lorg/apache/lucene/index/Terms;
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$CacheKey;->custom:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 1145
    .local v6, "acceptableOverheadRatio":F
    new-instance v7, Lorg/apache/lucene/util/PagedBytes;

    const/16 v23, 0xf

    move/from16 v0, v23

    invoke-direct {v7, v0}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 1152
    .local v7, "bytes":Lorg/apache/lucene/util/PagedBytes;
    const v23, 0x7fffffff

    move/from16 v0, v23

    if-ne v11, v0, :cond_3

    .line 1153
    const v18, 0x7fffffff

    .line 1159
    .local v18, "termCountHardLimit":I
    :goto_0
    if-eqz v21, :cond_5

    .line 1163
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/Terms;->size()J

    move-result-wide v12

    .line 1164
    .local v12, "numUniqueTerms":J
    const-wide/16 v24, -0x1

    cmp-long v23, v12, v24

    if-eqz v23, :cond_4

    .line 1165
    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v24, v0

    cmp-long v23, v12, v24

    if-lez v23, :cond_0

    .line 1169
    move/from16 v0, v18

    int-to-long v12, v0

    .line 1172
    :cond_0
    const-wide/16 v24, 0x4

    mul-long v24, v24, v12

    invoke-static/range {v24 .. v25}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v14

    .line 1173
    .local v14, "startBytesBPV":I
    invoke-static {v12, v13}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v16

    .line 1175
    .local v16, "startTermsBPV":I
    long-to-int v15, v12

    .line 1187
    .end local v12    # "numUniqueTerms":J
    .local v15, "startNumUniqueTerms":I
    :goto_1
    new-instance v20, Lorg/apache/lucene/util/packed/GrowableWriter;

    add-int/lit8 v23, v15, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-direct {v0, v14, v1, v6}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    .line 1188
    .local v20, "termOrdToBytesOffset":Lorg/apache/lucene/util/packed/GrowableWriter;
    new-instance v9, Lorg/apache/lucene/util/packed/GrowableWriter;

    move/from16 v0, v16

    invoke-direct {v9, v0, v11, v6}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    .line 1190
    .local v9, "docToTermOrd":Lorg/apache/lucene/util/packed/GrowableWriter;
    const/16 v19, 0x0

    .line 1194
    .local v19, "termOrd":I
    if-eqz v21, :cond_2

    .line 1195
    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v22

    .line 1196
    .local v22, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v10, 0x0

    .line 1199
    .local v10, "docs":Lorg/apache/lucene/index/DocsEnum;
    :goto_2
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v17

    .line 1200
    .local v17, "term":Lorg/apache/lucene/util/BytesRef;
    if-nez v17, :cond_6

    .line 1226
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v19

    if-le v0, v1, :cond_2

    .line 1227
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->resize(I)Lorg/apache/lucene/util/packed/GrowableWriter;

    move-result-object v20

    .line 1232
    .end local v10    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v17    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v22    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    new-instance v23, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;

    const/16 v24, 0x1

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v24

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/packed/GrowableWriter;->getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v25

    invoke-virtual {v9}, Lorg/apache/lucene/util/packed/GrowableWriter;->getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move-object/from16 v3, v26

    move/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/search/FieldCacheImpl$SortedDocValuesImpl;-><init>(Lorg/apache/lucene/util/PagedBytes$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;I)V

    return-object v23

    .line 1155
    .end local v9    # "docToTermOrd":Lorg/apache/lucene/util/packed/GrowableWriter;
    .end local v14    # "startBytesBPV":I
    .end local v15    # "startNumUniqueTerms":I
    .end local v16    # "startTermsBPV":I
    .end local v18    # "termCountHardLimit":I
    .end local v19    # "termOrd":I
    .end local v20    # "termOrdToBytesOffset":Lorg/apache/lucene/util/packed/GrowableWriter;
    :cond_3
    add-int/lit8 v18, v11, 0x1

    .restart local v18    # "termCountHardLimit":I
    goto :goto_0

    .line 1177
    .restart local v12    # "numUniqueTerms":J
    :cond_4
    const/4 v14, 0x1

    .line 1178
    .restart local v14    # "startBytesBPV":I
    const/16 v16, 0x1

    .line 1179
    .restart local v16    # "startTermsBPV":I
    const/4 v15, 0x1

    .line 1181
    .restart local v15    # "startNumUniqueTerms":I
    goto :goto_1

    .line 1182
    .end local v12    # "numUniqueTerms":J
    .end local v14    # "startBytesBPV":I
    .end local v15    # "startNumUniqueTerms":I
    .end local v16    # "startTermsBPV":I
    :cond_5
    const/4 v14, 0x1

    .line 1183
    .restart local v14    # "startBytesBPV":I
    const/16 v16, 0x1

    .line 1184
    .restart local v16    # "startTermsBPV":I
    const/4 v15, 0x1

    .restart local v15    # "startNumUniqueTerms":I
    goto :goto_1

    .line 1203
    .restart local v9    # "docToTermOrd":Lorg/apache/lucene/util/packed/GrowableWriter;
    .restart local v10    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v17    # "term":Lorg/apache/lucene/util/BytesRef;
    .restart local v19    # "termOrd":I
    .restart local v20    # "termOrdToBytesOffset":Lorg/apache/lucene/util/packed/GrowableWriter;
    .restart local v22    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_6
    move/from16 v0, v19

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    .line 1207
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v23

    move/from16 v0, v19

    move/from16 v1, v23

    if-ne v0, v1, :cond_7

    .line 1211
    add-int/lit8 v23, v19, 0x1

    const/16 v24, 0x1

    invoke-static/range {v23 .. v24}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v23

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->resize(I)Lorg/apache/lucene/util/packed/GrowableWriter;

    move-result-object v20

    .line 1213
    :cond_7
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/apache/lucene/util/PagedBytes;->copyUsingLengthPrefix(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v24

    move-object/from16 v0, v20

    move/from16 v1, v19

    move-wide/from16 v2, v24

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 1214
    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v10, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v10

    .line 1216
    :goto_3
    invoke-virtual {v10}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v8

    .line 1217
    .local v8, "docID":I
    const v23, 0x7fffffff

    move/from16 v0, v23

    if-ne v8, v0, :cond_8

    .line 1223
    add-int/lit8 v19, v19, 0x1

    .line 1198
    goto/16 :goto_2

    .line 1221
    :cond_8
    add-int/lit8 v23, v19, 0x1

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v8, v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    goto :goto_3
.end method
