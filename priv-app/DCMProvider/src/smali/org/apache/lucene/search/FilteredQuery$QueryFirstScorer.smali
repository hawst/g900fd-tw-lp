.class final Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilteredQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryFirstScorer"
.end annotation


# instance fields
.field private filterbits:Lorg/apache/lucene/util/Bits;

.field private final scorer:Lorg/apache/lucene/search/Scorer;

.field private scorerDoc:I


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filterBits"    # Lorg/apache/lucene/util/Bits;
    .param p3, "other"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 147
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorerDoc:I

    .line 152
    iput-object p3, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 153
    iput-object p2, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->filterbits:Lorg/apache/lucene/util/Bits;

    .line 154
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    .line 188
    .local v0, "doc":I
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->filterbits:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->nextDoc()I

    move-result v0

    .end local v0    # "doc":I
    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorerDoc:I

    .line 191
    :goto_0
    return v0

    .restart local v0    # "doc":I
    :cond_0
    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorerDoc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorerDoc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "FILTERED"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .line 178
    .local v0, "doc":I
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->filterbits:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    :cond_1
    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorerDoc:I

    return v0
.end method

.method public score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    return v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 163
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .line 164
    .local v0, "scorerDoc":I
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 171
    return-void

    .line 167
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$QueryFirstScorer;->filterbits:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0
.end method
