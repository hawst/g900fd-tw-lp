.class public final Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "MaxNonCompetitiveBoostAttributeImpl.java"

# interfaces
.implements Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttribute;


# instance fields
.field private competitiveTerm:Lorg/apache/lucene/util/BytesRef;

.field private maxNonCompetitiveBoost:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 27
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->maxNonCompetitiveBoost:F

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->competitiveTerm:Lorg/apache/lucene/util/BytesRef;

    .line 26
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 52
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->maxNonCompetitiveBoost:F

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->competitiveTerm:Lorg/apache/lucene/util/BytesRef;

    .line 54
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 58
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;

    .line 59
    .local v0, "t":Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;
    iget v1, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->maxNonCompetitiveBoost:F

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->setMaxNonCompetitiveBoost(F)V

    .line 60
    iget-object v1, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->competitiveTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->setCompetitiveTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 61
    return-void
.end method

.method public getCompetitiveTerm()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->competitiveTerm:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public getMaxNonCompetitiveBoost()F
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->maxNonCompetitiveBoost:F

    return v0
.end method

.method public setCompetitiveTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "competitiveTerm"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->competitiveTerm:Lorg/apache/lucene/util/BytesRef;

    .line 43
    return-void
.end method

.method public setMaxNonCompetitiveBoost(F)V
    .locals 0
    .param p1, "maxNonCompetitiveBoost"    # F

    .prologue
    .line 32
    iput p1, p0, Lorg/apache/lucene/search/MaxNonCompetitiveBoostAttributeImpl;->maxNonCompetitiveBoost:F

    .line 33
    return-void
.end method
