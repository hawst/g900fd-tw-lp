.class Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "SpanOrQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanOrQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpanQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/spans/Spans;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanOrQuery;I)V
    .locals 0
    .param p2, "size"    # I

    .prologue
    .line 150
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    .line 151
    invoke-direct {p0, p2}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 152
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/spans/Spans;

    check-cast p2, Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->lessThan(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z

    move-result v0

    return v0
.end method

.method protected final lessThan(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z
    .locals 4
    .param p1, "spans1"    # Lorg/apache/lucene/search/spans/Spans;
    .param p2, "spans2"    # Lorg/apache/lucene/search/spans/Spans;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 157
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 158
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v2

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 158
    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 163
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v3

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
