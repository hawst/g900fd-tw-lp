.class public final Lorg/apache/lucene/search/SearcherManager;
.super Lorg/apache/lucene/search/ReferenceManager;
.source "SearcherManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ReferenceManager",
        "<",
        "Lorg/apache/lucene/search/IndexSearcher;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final searcherFactory:Lorg/apache/lucene/search/SearcherFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lorg/apache/lucene/search/SearcherManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/SearcherManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;ZLorg/apache/lucene/search/SearcherFactory;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .param p3, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 85
    if-nez p3, :cond_0

    .line 86
    new-instance p3, Lorg/apache/lucene/search/SearcherFactory;

    .end local p3    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p3}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 88
    .restart local p3    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/search/SearcherManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 89
    invoke-static {p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    invoke-static {p3, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SearcherManager;->current:Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/search/SearcherFactory;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 102
    if-nez p2, :cond_0

    .line 103
    new-instance p2, Lorg/apache/lucene/search/SearcherFactory;

    .end local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p2}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 105
    .restart local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/SearcherManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 106
    invoke-static {p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    invoke-static {p2, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SearcherManager;->current:Ljava/lang/Object;

    .line 107
    return-void
.end method

.method public static getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 5
    .param p0, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    const/4 v1, 0x0

    .line 155
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherFactory;->newSearcher(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    .line 156
    .local v0, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    if-eq v2, p1, :cond_1

    .line 157
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SearcherFactory must wrap exactly the provided reader (got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " but expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    .end local v0    # "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :catchall_0
    move-exception v2

    .line 161
    if-nez v1, :cond_0

    .line 162
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 164
    :cond_0
    throw v2

    .line 159
    .restart local v0    # "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :cond_1
    const/4 v1, 0x1

    .line 161
    if-nez v1, :cond_2

    .line 162
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 165
    :cond_2
    return-object v0
.end method


# virtual methods
.method protected bridge synthetic decRef(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherManager;->decRef(Lorg/apache/lucene/search/IndexSearcher;)V

    return-void
.end method

.method protected decRef(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 112
    return-void
.end method

.method public isSearcherCurrent()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/search/SearcherManager;->acquire()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/IndexSearcher;

    .line 139
    .local v1, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 140
    .local v0, "r":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v2, Lorg/apache/lucene/search/SearcherManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    instance-of v2, v0, Lorg/apache/lucene/index/DirectoryReader;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "searcher\'s IndexReader should be a DirectoryReader, but got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    .end local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v2

    .line 143
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/SearcherManager;->release(Ljava/lang/Object;)V

    .line 144
    throw v2

    .line 141
    .restart local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    :try_start_1
    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;

    .end local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->isCurrent()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 143
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/SearcherManager;->release(Ljava/lang/Object;)V

    .line 141
    return v2
.end method

.method protected bridge synthetic refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherManager;->refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    return-object v0
.end method

.method protected refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 5
    .param p1, "referenceToRefresh"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    .line 117
    .local v1, "r":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v2, Lorg/apache/lucene/search/SearcherManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    instance-of v2, v1, Lorg/apache/lucene/index/DirectoryReader;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "searcher\'s IndexReader should be a DirectoryReader, but got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 118
    :cond_0
    check-cast v1, Lorg/apache/lucene/index/DirectoryReader;

    .end local v1    # "r":Lorg/apache/lucene/index/IndexReader;
    invoke-static {v1}, Lorg/apache/lucene/index/DirectoryReader;->openIfChanged(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 119
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_1

    .line 120
    const/4 v2, 0x0

    .line 122
    :goto_0
    return-object v2

    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/SearcherManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    invoke-static {v2, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v2

    goto :goto_0
.end method

.method protected bridge synthetic tryIncRef(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherManager;->tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z

    move-result v0

    return v0
.end method

.method protected tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 128
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v0

    return v0
.end method
