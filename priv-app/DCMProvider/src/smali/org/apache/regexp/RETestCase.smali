.class final Lorg/apache/regexp/RETestCase;
.super Ljava/lang/Object;
.source "RETest.java"


# instance fields
.field private final badPattern:Z

.field private final log:Ljava/lang/StringBuffer;

.field private final number:I

.field private final parens:[Ljava/lang/String;

.field private final pattern:Ljava/lang/String;

.field private regexp:Lorg/apache/regexp/RE;

.field private final shouldMatch:Z

.field private final tag:Ljava/lang/String;

.field private final test:Lorg/apache/regexp/RETest;

.field private final toMatch:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/regexp/RETest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;)V
    .locals 3
    .param p1, "test"    # Lorg/apache/regexp/RETest;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "pattern"    # Ljava/lang/String;
    .param p4, "toMatch"    # Ljava/lang/String;
    .param p5, "badPattern"    # Z
    .param p6, "shouldMatch"    # Z
    .param p7, "parens"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 682
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    .line 697
    iget v0, p1, Lorg/apache/regexp/RETest;->testCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lorg/apache/regexp/RETest;->testCount:I

    iput v0, p0, Lorg/apache/regexp/RETestCase;->number:I

    .line 698
    iput-object p1, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    .line 699
    iput-object p2, p0, Lorg/apache/regexp/RETestCase;->tag:Ljava/lang/String;

    .line 700
    iput-object p3, p0, Lorg/apache/regexp/RETestCase;->pattern:Ljava/lang/String;

    .line 701
    iput-object p4, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    .line 702
    iput-boolean p5, p0, Lorg/apache/regexp/RETestCase;->badPattern:Z

    .line 703
    iput-boolean p6, p0, Lorg/apache/regexp/RETestCase;->shouldMatch:Z

    .line 704
    if-eqz p7, :cond_0

    .line 705
    array-length v0, p7

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/regexp/RETestCase;->parens:[Ljava/lang/String;

    .line 706
    iget-object v0, p0, Lorg/apache/regexp/RETestCase;->parens:[Ljava/lang/String;

    array-length v1, p7

    invoke-static {p7, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 710
    :goto_0
    return-void

    .line 708
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/regexp/RETestCase;->parens:[Ljava/lang/String;

    goto :goto_0
.end method

.method private checkParens()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 833
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Paren count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v3}, Lorg/apache/regexp/RE;->getParenCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 834
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "Wrong number of parens"

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->parens:[Ljava/lang/String;

    array-length v4, v4

    iget-object v5, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v5}, Lorg/apache/regexp/RE;->getParenCount()I

    move-result v5

    invoke-virtual {p0, v2, v3, v4, v5}, Lorg/apache/regexp/RETestCase;->assertEquals(Ljava/lang/StringBuffer;Ljava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 856
    :goto_0
    return v1

    .line 840
    :cond_0
    const/4 v0, 0x0

    .local v0, "p":I
    :goto_1
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v2}, Lorg/apache/regexp/RE;->getParenCount()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 856
    const/4 v1, 0x1

    goto :goto_0

    .line 842
    :cond_1
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Paren "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v3, v0}, Lorg/apache/regexp/RE;->getParen(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 845
    const-string v2, "null"

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->parens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v2, v0}, Lorg/apache/regexp/RE;->getParen(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 840
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 850
    :cond_3
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Wrong register "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->parens:[Ljava/lang/String;

    aget-object v4, v4, v0

    iget-object v5, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    invoke-virtual {v5, v0}, Lorg/apache/regexp/RE;->getParen(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v2, v3, v4, v5}, Lorg/apache/regexp/RETestCase;->assertEquals(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method private checkResult(Z)Z
    .locals 4
    .param p1, "result"    # Z

    .prologue
    .line 811
    iget-boolean v0, p0, Lorg/apache/regexp/RETestCase;->shouldMatch:Z

    if-ne p1, v0, :cond_1

    .line 812
    new-instance v1, Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lorg/apache/regexp/RETestCase;->shouldMatch:Z

    if-eqz v0, :cond_0

    const-string v0, "Matched"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 813
    const-string v0, " \""

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", as expected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 812
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETestCase;->success(Ljava/lang/String;)V

    .line 814
    const/4 v0, 0x1

    .line 821
    :goto_1
    return v0

    .line 812
    :cond_0
    const-string v0, "Did not match"

    goto :goto_0

    .line 816
    :cond_1
    iget-boolean v0, p0, Lorg/apache/regexp/RETestCase;->shouldMatch:Z

    if-eqz v0, :cond_2

    .line 817
    iget-object v0, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v1, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Did not match \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\", when expected to."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 821
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 819
    :cond_2
    iget-object v0, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v1, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Matched \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\", when not expected to."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private testMatch()V
    .locals 6

    .prologue
    .line 764
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Match against: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\'\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 769
    :try_start_0
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v1

    .line 770
    .local v1, "result":Z
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Matched: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    if-eqz v1, :cond_2

    const-string v2, "YES"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 773
    invoke-direct {p0, v1}, Lorg/apache/regexp/RETestCase;->checkResult(Z)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lorg/apache/regexp/RETestCase;->shouldMatch:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lorg/apache/regexp/RETestCase;->checkParens()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 777
    :cond_0
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Match using StringCharacterIterator\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 778
    new-instance v2, Lorg/apache/regexp/StringCharacterIterator;

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/apache/regexp/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RETestCase;->tryMatchUsingCI(Lorg/apache/regexp/CharacterIterator;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 806
    .end local v1    # "result":Z
    :cond_1
    :goto_1
    return-void

    .line 770
    .restart local v1    # "result":Z
    :cond_2
    const-string v2, "NO"

    goto :goto_0

    .line 781
    :cond_3
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Match using CharacterArrayCharacterIterator\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 782
    new-instance v2, Lorg/apache/regexp/CharacterArrayCharacterIterator;

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/regexp/CharacterArrayCharacterIterator;-><init>([CII)V

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RETestCase;->tryMatchUsingCI(Lorg/apache/regexp/CharacterIterator;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 785
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Match using StreamCharacterIterator\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 786
    new-instance v2, Lorg/apache/regexp/StreamCharacterIterator;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Lorg/apache/regexp/StreamCharacterIterator;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RETestCase;->tryMatchUsingCI(Lorg/apache/regexp/CharacterIterator;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 789
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v3, "   Match using ReaderCharacterIterator\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 790
    new-instance v2, Lorg/apache/regexp/ReaderCharacterIterator;

    new-instance v3, Ljava/io/StringReader;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->toMatch:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lorg/apache/regexp/ReaderCharacterIterator;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RETestCase;->tryMatchUsingCI(Lorg/apache/regexp/CharacterIterator;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 795
    .end local v1    # "result":Z
    :catch_0
    move-exception v0

    .line 797
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Matcher threw exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 798
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 801
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 803
    .local v0, "e":Ljava/lang/Error;
    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Matcher threw fatal error \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 804
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto/16 :goto_1
.end method


# virtual methods
.method public assertEquals(Ljava/lang/StringBuffer;Ljava/lang/String;II)Z
    .locals 3
    .param p1, "log"    # Ljava/lang/StringBuffer;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "expected"    # I
    .param p4, "actual"    # I

    .prologue
    .line 895
    if-eq p3, p4, :cond_0

    .line 896
    iget-object v0, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " (expected \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 897
    const-string v2, "\", actual \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 896
    invoke-virtual {v0, p1, v1}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 898
    const/4 v0, 0x0

    .line 900
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public assertEquals(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "log"    # Ljava/lang/StringBuffer;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "expected"    # Ljava/lang/String;
    .param p4, "actual"    # Ljava/lang/String;

    .prologue
    .line 883
    if-eqz p3, :cond_0

    invoke-virtual {p3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 884
    :cond_0
    if-eqz p4, :cond_2

    invoke-virtual {p4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 886
    :cond_1
    iget-object v0, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " (expected \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 887
    const-string v2, "\", actual \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 886
    invoke-virtual {v0, p1, v1}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 888
    const/4 v0, 0x0

    .line 890
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public runTest()V
    .locals 3

    .prologue
    .line 714
    iget-object v0, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->tag:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/regexp/RETestCase;->number:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/regexp/RETestCase;->pattern:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 715
    invoke-virtual {p0}, Lorg/apache/regexp/RETestCase;->testCreation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    invoke-direct {p0}, Lorg/apache/regexp/RETestCase;->testMatch()V

    .line 718
    :cond_0
    return-void
.end method

.method success(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 915
    return-void
.end method

.method testCreation()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 725
    :try_start_0
    new-instance v3, Lorg/apache/regexp/RE;

    invoke-direct {v3}, Lorg/apache/regexp/RE;-><init>()V

    iput-object v3, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    .line 726
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v4, v4, Lorg/apache/regexp/RETest;->compiler:Lorg/apache/regexp/REDebugCompiler;

    iget-object v5, p0, Lorg/apache/regexp/RETestCase;->pattern:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lorg/apache/regexp/REDebugCompiler;->compile(Ljava/lang/String;)Lorg/apache/regexp/REProgram;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->setProgram(Lorg/apache/regexp/REProgram;)V

    .line 728
    iget-boolean v3, p0, Lorg/apache/regexp/RETestCase;->badPattern:Z

    if-eqz v3, :cond_0

    .line 730
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v5, "Was expected to be an error, but wasn\'t."

    invoke-virtual {v3, v4, v5}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 759
    :goto_0
    return v2

    .line 734
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 737
    :catch_0
    move-exception v0

    .line 740
    .local v0, "e":Ljava/lang/Exception;
    iget-boolean v3, p0, Lorg/apache/regexp/RETestCase;->badPattern:Z

    if-eqz v3, :cond_1

    .line 742
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v4, "   Match: ERR\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 743
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Produces an error ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), as expected."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETestCase;->success(Ljava/lang/String;)V

    goto :goto_0

    .line 748
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    .line 749
    .local v1, "message":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Produces an unexpected exception \""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 750
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 748
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 752
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 755
    .local v0, "e":Ljava/lang/Error;
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Compiler threw fatal error \""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 756
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method

.method tryMatchUsingCI(Lorg/apache/regexp/CharacterIterator;)Z
    .locals 7
    .param p1, "matchAgainst"    # Lorg/apache/regexp/CharacterIterator;

    .prologue
    const/4 v2, 0x0

    .line 862
    :try_start_0
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->regexp:Lorg/apache/regexp/RE;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/regexp/RE;->match(Lorg/apache/regexp/CharacterIterator;I)Z

    move-result v1

    .line 863
    .local v1, "result":Z
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    const-string v4, "   Match: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    if-eqz v1, :cond_2

    const-string v3, "YES"

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 864
    invoke-direct {p0, v1}, Lorg/apache/regexp/RETestCase;->checkResult(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lorg/apache/regexp/RETestCase;->shouldMatch:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lorg/apache/regexp/RETestCase;->checkParens()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 878
    .end local v1    # "result":Z
    :cond_1
    :goto_1
    return v2

    .line 863
    .restart local v1    # "result":Z
    :cond_2
    const-string v3, "NO"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 867
    .end local v1    # "result":Z
    :catch_0
    move-exception v0

    .line 869
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Matcher threw exception: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 870
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 873
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 875
    .local v0, "e":Ljava/lang/Error;
    iget-object v3, p0, Lorg/apache/regexp/RETestCase;->test:Lorg/apache/regexp/RETest;

    iget-object v4, p0, Lorg/apache/regexp/RETestCase;->log:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Matcher threw fatal error \""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 876
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_1
.end method
