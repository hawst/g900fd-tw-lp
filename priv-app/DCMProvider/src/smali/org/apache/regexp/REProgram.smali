.class public Lorg/apache/regexp/REProgram;
.super Ljava/lang/Object;
.source "REProgram.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final OPT_HASBACKREFS:I = 0x1

.field static final OPT_HASBOL:I = 0x2


# instance fields
.field flags:I

.field instruction:[C

.field lenInstruction:I

.field maxParens:I

.field prefix:[C


# direct methods
.method public constructor <init>(I[C)V
    .locals 1
    .param p1, "parens"    # I
    .param p2, "instruction"    # [C

    .prologue
    .line 61
    array-length v0, p2

    invoke-direct {p0, p2, v0}, Lorg/apache/regexp/REProgram;-><init>([CI)V

    .line 62
    iput p1, p0, Lorg/apache/regexp/REProgram;->maxParens:I

    .line 63
    return-void
.end method

.method public constructor <init>([C)V
    .locals 1
    .param p1, "instruction"    # [C

    .prologue
    .line 51
    array-length v0, p1

    invoke-direct {p0, p1, v0}, Lorg/apache/regexp/REProgram;-><init>([CI)V

    .line 52
    return-void
.end method

.method public constructor <init>([CI)V
    .locals 1
    .param p1, "instruction"    # [C
    .param p2, "lenInstruction"    # I

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/regexp/REProgram;->maxParens:I

    .line 72
    invoke-virtual {p0, p1, p2}, Lorg/apache/regexp/REProgram;->setInstructions([CI)V

    .line 73
    return-void
.end method


# virtual methods
.method public getInstructions()[C
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 84
    iget v1, p0, Lorg/apache/regexp/REProgram;->lenInstruction:I

    if-eqz v1, :cond_0

    .line 87
    iget v1, p0, Lorg/apache/regexp/REProgram;->lenInstruction:I

    new-array v0, v1, [C

    .line 88
    .local v0, "ret":[C
    iget-object v1, p0, Lorg/apache/regexp/REProgram;->instruction:[C

    iget v2, p0, Lorg/apache/regexp/REProgram;->lenInstruction:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    .end local v0    # "ret":[C
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrefix()[C
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 173
    iget-object v1, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    array-length v1, v1

    new-array v0, v1, [C

    .line 177
    .local v0, "ret":[C
    iget-object v1, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    iget-object v2, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    .end local v0    # "ret":[C
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInstructions([CI)V
    .locals 9
    .param p1, "instruction"    # [C
    .param p2, "lenInstruction"    # I

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 107
    iput-object p1, p0, Lorg/apache/regexp/REProgram;->instruction:[C

    .line 108
    iput p2, p0, Lorg/apache/regexp/REProgram;->lenInstruction:I

    .line 111
    iput v6, p0, Lorg/apache/regexp/REProgram;->flags:I

    .line 112
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    .line 115
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 118
    if-lt p2, v7, :cond_0

    aget-char v4, p1, v6

    const/16 v5, 0x7c

    if-ne v4, v5, :cond_0

    .line 121
    const/4 v4, 0x2

    aget-char v4, p1, v4

    int-to-short v2, v4

    .line 122
    .local v2, "next":I
    add-int/lit8 v4, v2, 0x0

    aget-char v4, p1, v4

    const/16 v5, 0x45

    if-ne v4, v5, :cond_0

    if-lt p2, v8, :cond_0

    .line 124
    aget-char v3, p1, v7

    .line 126
    .local v3, "nextOp":C
    const/16 v4, 0x41

    if-ne v3, v4, :cond_2

    .line 129
    const/4 v4, 0x4

    aget-char v1, p1, v4

    .line 130
    .local v1, "lenAtom":I
    new-array v4, v1, [C

    iput-object v4, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    .line 131
    iget-object v4, p0, Lorg/apache/regexp/REProgram;->prefix:[C

    invoke-static {p1, v8, v4, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 145
    .end local v1    # "lenAtom":I
    .end local v2    # "next":I
    .end local v3    # "nextOp":C
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, p2, :cond_3

    .line 163
    .end local v0    # "i":I
    :cond_1
    :goto_2
    return-void

    .line 134
    .restart local v2    # "next":I
    .restart local v3    # "nextOp":C
    :cond_2
    const/16 v4, 0x5e

    if-ne v3, v4, :cond_0

    .line 137
    iget v4, p0, Lorg/apache/regexp/REProgram;->flags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lorg/apache/regexp/REProgram;->flags:I

    goto :goto_0

    .line 147
    .end local v2    # "next":I
    .end local v3    # "nextOp":C
    .restart local v0    # "i":I
    :cond_3
    add-int/lit8 v4, v0, 0x0

    aget-char v4, p1, v4

    sparse-switch v4, :sswitch_data_0

    .line 145
    :goto_3
    add-int/lit8 v0, v0, 0x3

    goto :goto_1

    .line 150
    :sswitch_0
    add-int/lit8 v4, v0, 0x1

    aget-char v4, p1, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 151
    goto :goto_3

    .line 154
    :sswitch_1
    add-int/lit8 v4, v0, 0x1

    aget-char v4, p1, v4

    add-int/2addr v0, v4

    .line 155
    goto :goto_3

    .line 158
    :sswitch_2
    iget v4, p0, Lorg/apache/regexp/REProgram;->flags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/regexp/REProgram;->flags:I

    goto :goto_2

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x23 -> :sswitch_2
        0x41 -> :sswitch_1
        0x5b -> :sswitch_0
    .end sparse-switch
.end method
