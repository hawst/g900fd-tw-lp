.class public final Lorg/apache/regexp/StringCharacterIterator;
.super Ljava/lang/Object;
.source "StringCharacterIterator.java"

# interfaces
.implements Lorg/apache/regexp/CharacterIterator;


# instance fields
.field private final src:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/apache/regexp/StringCharacterIterator;->src:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/regexp/StringCharacterIterator;->src:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method public isEnd(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/regexp/StringCharacterIterator;->src:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public substring(I)Ljava/lang/String;
    .locals 1
    .param p1, "beginIndex"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/regexp/StringCharacterIterator;->src:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public substring(II)Ljava/lang/String;
    .locals 1
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/regexp/StringCharacterIterator;->src:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
