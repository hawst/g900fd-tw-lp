.class public final Lorg/apache/regexp/StreamCharacterIterator;
.super Ljava/lang/Object;
.source "StreamCharacterIterator.java"

# interfaces
.implements Lorg/apache/regexp/CharacterIterator;


# instance fields
.field private final buff:Ljava/lang/StringBuffer;

.field private closed:Z

.field private final is:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/regexp/StreamCharacterIterator;->is:Ljava/io/InputStream;

    .line 44
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/regexp/StreamCharacterIterator;->closed:Z

    .line 46
    return-void
.end method

.method private ensure(I)V
    .locals 2
    .param p1, "idx"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    iget-boolean v0, p0, Lorg/apache/regexp/StreamCharacterIterator;->closed:Z

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 157
    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lorg/apache/regexp/StreamCharacterIterator;->read(I)I

    goto :goto_0
.end method

.method private read(I)I
    .locals 4
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    iget-boolean v2, p0, Lorg/apache/regexp/StreamCharacterIterator;->closed:Z

    if-eqz v2, :cond_0

    .line 117
    const/4 v2, 0x0

    .line 132
    :goto_0
    return v2

    .line 121
    :cond_0
    move v1, p1

    .line 122
    .local v1, "i":I
    :goto_1
    add-int/lit8 v1, v1, -0x1

    if-gez v1, :cond_1

    .line 132
    :goto_2
    sub-int v2, p1, v1

    goto :goto_0

    .line 124
    :cond_1
    iget-object v2, p0, Lorg/apache/regexp/StreamCharacterIterator;->is:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 125
    .local v0, "c":I
    if-gez v0, :cond_2

    .line 127
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/regexp/StreamCharacterIterator;->closed:Z

    goto :goto_2

    .line 130
    :cond_2
    iget-object v2, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    int-to-char v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method private readAll()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    :goto_0
    iget-boolean v0, p0, Lorg/apache/regexp/StreamCharacterIterator;->closed:Z

    if-eqz v0, :cond_0

    .line 142
    return-void

    .line 140
    :cond_0
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lorg/apache/regexp/StreamCharacterIterator;->read(I)I

    goto :goto_0
.end method


# virtual methods
.method public charAt(I)C
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 82
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/regexp/StreamCharacterIterator;->ensure(I)V

    .line 83
    iget-object v1, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->charAt(I)C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 85
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isEnd(I)Z
    .locals 3
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v2, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-le v2, p1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/regexp/StreamCharacterIterator;->ensure(I)V

    .line 103
    iget-object v2, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-gt v2, p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public substring(I)Ljava/lang/String;
    .locals 3
    .param p1, "beginIndex"    # I

    .prologue
    .line 67
    :try_start_0
    invoke-direct {p0}, Lorg/apache/regexp/StreamCharacterIterator;->readAll()V

    .line 68
    iget-object v1, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 70
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public substring(II)Ljava/lang/String;
    .locals 3
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    .line 53
    :try_start_0
    invoke-direct {p0, p2}, Lorg/apache/regexp/StreamCharacterIterator;->ensure(I)V

    .line 54
    iget-object v1, p0, Lorg/apache/regexp/StreamCharacterIterator;->buff:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 56
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
