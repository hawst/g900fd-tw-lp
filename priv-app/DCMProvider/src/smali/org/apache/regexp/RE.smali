.class public Lorg/apache/regexp/RE;
.super Ljava/lang/Object;
.source "RE.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final E_ALNUM:C = 'w'

.field static final E_BOUND:C = 'b'

.field static final E_DIGIT:C = 'd'

.field static final E_NALNUM:C = 'W'

.field static final E_NBOUND:C = 'B'

.field static final E_NDIGIT:C = 'D'

.field static final E_NSPACE:C = 'S'

.field static final E_SPACE:C = 's'

.field public static final MATCH_CASEINDEPENDENT:I = 0x1

.field public static final MATCH_MULTILINE:I = 0x2

.field public static final MATCH_NORMAL:I = 0x0

.field public static final MATCH_SINGLELINE:I = 0x4

.field static final MAX_PAREN:I = 0x10

.field static final OP_ANY:C = '.'

.field static final OP_ANYOF:C = '['

.field static final OP_ATOM:C = 'A'

.field static final OP_BACKREF:C = '#'

.field static final OP_BOL:C = '^'

.field static final OP_BRANCH:C = '|'

.field static final OP_CLOSE:C = ')'

.field static final OP_CLOSE_CLUSTER:C = '>'

.field static final OP_CONTINUE:C = 'C'

.field static final OP_END:C = 'E'

.field static final OP_EOL:C = '$'

.field static final OP_ESCAPE:C = '\\'

.field static final OP_GOTO:C = 'G'

.field static final OP_MAYBE:C = '?'

.field static final OP_NOTHING:C = 'N'

.field static final OP_OPEN:C = '('

.field static final OP_OPEN_CLUSTER:C = '<'

.field static final OP_PLUS:C = '+'

.field static final OP_POSIXCLASS:C = 'P'

.field static final OP_RELUCTANTMAYBE:C = '/'

.field static final OP_RELUCTANTPLUS:C = '='

.field static final OP_RELUCTANTSTAR:C = '8'

.field static final OP_STAR:C = '*'

.field static final POSIX_CLASS_ALNUM:C = 'w'

.field static final POSIX_CLASS_ALPHA:C = 'a'

.field static final POSIX_CLASS_BLANK:C = 'b'

.field static final POSIX_CLASS_CNTRL:C = 'c'

.field static final POSIX_CLASS_DIGIT:C = 'd'

.field static final POSIX_CLASS_GRAPH:C = 'g'

.field static final POSIX_CLASS_JPART:C = 'k'

.field static final POSIX_CLASS_JSTART:C = 'j'

.field static final POSIX_CLASS_LOWER:C = 'l'

.field static final POSIX_CLASS_PRINT:C = 'p'

.field static final POSIX_CLASS_PUNCT:C = '!'

.field static final POSIX_CLASS_SPACE:C = 's'

.field static final POSIX_CLASS_UPPER:C = 'u'

.field static final POSIX_CLASS_XDIGIT:C = 'x'

.field public static final REPLACE_ALL:I = 0x0

.field public static final REPLACE_BACKREFERENCES:I = 0x2

.field public static final REPLACE_FIRSTONLY:I = 0x1

.field static final maxNode:I = 0x10000

.field static final nodeSize:I = 0x3

.field static final offsetNext:I = 0x2

.field static final offsetOpcode:I = 0x0

.field static final offsetOpdata:I = 0x1


# instance fields
.field transient end0:I

.field transient end1:I

.field transient end2:I

.field transient endBackref:[I

.field transient endn:[I

.field matchFlags:I

.field maxParen:I

.field transient parenCount:I

.field program:Lorg/apache/regexp/REProgram;

.field transient search:Lorg/apache/regexp/CharacterIterator;

.field transient start0:I

.field transient start1:I

.field transient start2:I

.field transient startBackref:[I

.field transient startn:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 487
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/regexp/RE;-><init>(Lorg/apache/regexp/REProgram;I)V

    .line 488
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 425
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    .line 426
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "matchFlags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 441
    new-instance v0, Lorg/apache/regexp/RECompiler;

    invoke-direct {v0}, Lorg/apache/regexp/RECompiler;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/regexp/RECompiler;->compile(Ljava/lang/String;)Lorg/apache/regexp/REProgram;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/regexp/RE;-><init>(Lorg/apache/regexp/REProgram;I)V

    .line 442
    return-void
.end method

.method public constructor <init>(Lorg/apache/regexp/REProgram;)V
    .locals 1
    .param p1, "program"    # Lorg/apache/regexp/REProgram;

    .prologue
    .line 478
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/regexp/RE;-><init>(Lorg/apache/regexp/REProgram;I)V

    .line 479
    return-void
.end method

.method public constructor <init>(Lorg/apache/regexp/REProgram;I)V
    .locals 1
    .param p1, "program"    # Lorg/apache/regexp/REProgram;
    .param p2, "matchFlags"    # I

    .prologue
    .line 462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/regexp/RE;->maxParen:I

    .line 464
    invoke-virtual {p0, p1}, Lorg/apache/regexp/RE;->setProgram(Lorg/apache/regexp/REProgram;)V

    .line 465
    invoke-virtual {p0, p2}, Lorg/apache/regexp/RE;->setMatchFlags(I)V

    .line 466
    return-void
.end method

.method private allocParens()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 784
    iget v1, p0, Lorg/apache/regexp/RE;->maxParen:I

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/regexp/RE;->startn:[I

    .line 785
    iget v1, p0, Lorg/apache/regexp/RE;->maxParen:I

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/regexp/RE;->endn:[I

    .line 788
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/regexp/RE;->maxParen:I

    if-lt v0, v1, :cond_0

    .line 793
    return-void

    .line 790
    :cond_0
    iget-object v1, p0, Lorg/apache/regexp/RE;->startn:[I

    aput v2, v1, v0

    .line 791
    iget-object v1, p0, Lorg/apache/regexp/RE;->endn:[I

    aput v2, v1, v0

    .line 788
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private compareChars(CCZ)I
    .locals 1
    .param p1, "c1"    # C
    .param p2, "c2"    # C
    .param p3, "caseIndependent"    # Z

    .prologue
    .line 1796
    if-eqz p3, :cond_0

    .line 1798
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    .line 1799
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p2

    .line 1801
    :cond_0
    sub-int v0, p1, p2

    return v0
.end method

.method private isNewline(I)Z
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 1779
    iget-object v1, p0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    invoke-interface {v1, p1}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v0

    .line 1781
    .local v0, "nextChar":C
    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0x85

    if-eq v0, v1, :cond_0

    .line 1782
    const/16 v1, 0x2028

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2029

    if-eq v0, v1, :cond_0

    .line 1781
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static simplePatternToFullRegularExpression(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "pattern"    # Ljava/lang/String;

    .prologue
    .line 498
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 499
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 527
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 501
    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 502
    .local v1, "c":C
    sparse-switch v1, :sswitch_data_0

    .line 523
    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 499
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 505
    :sswitch_0
    const-string v3, ".*"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 521
    :sswitch_1
    const/16 v3, 0x5c

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 502
    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_1
        0x28 -> :sswitch_1
        0x29 -> :sswitch_1
        0x2a -> :sswitch_0
        0x2b -> :sswitch_1
        0x2e -> :sswitch_1
        0x3f -> :sswitch_1
        0x5b -> :sswitch_1
        0x5c -> :sswitch_1
        0x5d -> :sswitch_1
        0x5e -> :sswitch_1
        0x7b -> :sswitch_1
        0x7c -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public getMatchFlags()I
    .locals 1

    .prologue
    .line 559
    iget v0, p0, Lorg/apache/regexp/RE;->matchFlags:I

    return v0
.end method

.method public getParen(I)Ljava/lang/String;
    .locals 3
    .param p1, "which"    # I

    .prologue
    .line 610
    iget v1, p0, Lorg/apache/regexp/RE;->parenCount:I

    if-ge p1, v1, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/regexp/RE;->getParenStart(I)I

    move-result v0

    .local v0, "start":I
    if-ltz v0, :cond_0

    .line 612
    iget-object v1, p0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    invoke-virtual {p0, p1}, Lorg/apache/regexp/RE;->getParenEnd(I)I

    move-result v2

    invoke-interface {v1, v0, v2}, Lorg/apache/regexp/CharacterIterator;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 614
    .end local v0    # "start":I
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getParenCount()I
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lorg/apache/regexp/RE;->parenCount:I

    return v0
.end method

.method public final getParenEnd(I)I
    .locals 1
    .param p1, "which"    # I

    .prologue
    .line 657
    iget v0, p0, Lorg/apache/regexp/RE;->parenCount:I

    if-ge p1, v0, :cond_1

    .line 659
    packed-switch p1, :pswitch_data_0

    .line 671
    iget-object v0, p0, Lorg/apache/regexp/RE;->endn:[I

    if-nez v0, :cond_0

    .line 673
    invoke-direct {p0}, Lorg/apache/regexp/RE;->allocParens()V

    .line 675
    :cond_0
    iget-object v0, p0, Lorg/apache/regexp/RE;->endn:[I

    aget v0, v0, p1

    .line 678
    :goto_0
    return v0

    .line 662
    :pswitch_0
    iget v0, p0, Lorg/apache/regexp/RE;->end0:I

    goto :goto_0

    .line 665
    :pswitch_1
    iget v0, p0, Lorg/apache/regexp/RE;->end1:I

    goto :goto_0

    .line 668
    :pswitch_2
    iget v0, p0, Lorg/apache/regexp/RE;->end2:I

    goto :goto_0

    .line 678
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 659
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getParenLength(I)I
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 689
    iget v0, p0, Lorg/apache/regexp/RE;->parenCount:I

    if-ge p1, v0, :cond_0

    .line 691
    invoke-virtual {p0, p1}, Lorg/apache/regexp/RE;->getParenEnd(I)I

    move-result v0

    invoke-virtual {p0, p1}, Lorg/apache/regexp/RE;->getParenStart(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 693
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final getParenStart(I)I
    .locals 1
    .param p1, "which"    # I

    .prologue
    .line 625
    iget v0, p0, Lorg/apache/regexp/RE;->parenCount:I

    if-ge p1, v0, :cond_1

    .line 627
    packed-switch p1, :pswitch_data_0

    .line 639
    iget-object v0, p0, Lorg/apache/regexp/RE;->startn:[I

    if-nez v0, :cond_0

    .line 641
    invoke-direct {p0}, Lorg/apache/regexp/RE;->allocParens()V

    .line 643
    :cond_0
    iget-object v0, p0, Lorg/apache/regexp/RE;->startn:[I

    aget v0, v0, p1

    .line 646
    :goto_0
    return v0

    .line 630
    :pswitch_0
    iget v0, p0, Lorg/apache/regexp/RE;->start0:I

    goto :goto_0

    .line 633
    :pswitch_1
    iget v0, p0, Lorg/apache/regexp/RE;->start1:I

    goto :goto_0

    .line 636
    :pswitch_2
    iget v0, p0, Lorg/apache/regexp/RE;->start2:I

    goto :goto_0

    .line 646
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 627
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getProgram()Lorg/apache/regexp/REProgram;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    return-object v0
.end method

.method public grep([Ljava/lang/Object;)[Ljava/lang/String;
    .locals 5
    .param p1, "search"    # [Ljava/lang/Object;

    .prologue
    .line 1753
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 1756
    .local v3, "v":Ljava/util/Vector;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-lt v0, v4, :cond_0

    .line 1769
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v1, v4, [Ljava/lang/String;

    .line 1770
    .local v1, "ret":[Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 1771
    return-object v1

    .line 1759
    .end local v1    # "ret":[Ljava/lang/String;
    :cond_0
    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1762
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1764
    invoke-virtual {v3, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1756
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected internalError(Ljava/lang/String;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 775
    new-instance v0, Ljava/lang/Error;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RE internal error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public match(Ljava/lang/String;)Z
    .locals 1
    .param p1, "search"    # Ljava/lang/String;

    .prologue
    .line 1530
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public match(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "i"    # I

    .prologue
    .line 1417
    new-instance v0, Lorg/apache/regexp/StringCharacterIterator;

    invoke-direct {v0, p1}, Lorg/apache/regexp/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lorg/apache/regexp/RE;->match(Lorg/apache/regexp/CharacterIterator;I)Z

    move-result v0

    return v0
.end method

.method public match(Lorg/apache/regexp/CharacterIterator;I)Z
    .locals 11
    .param p1, "search"    # Lorg/apache/regexp/CharacterIterator;
    .param p2, "i"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1431
    iget-object v9, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    if-nez v9, :cond_0

    .line 1435
    const-string v9, "No RE program to run!"

    invoke-virtual {p0, v9}, Lorg/apache/regexp/RE;->internalError(Ljava/lang/String;)V

    .line 1439
    :cond_0
    iput-object p1, p0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    .line 1442
    iget-object v9, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    iget v9, v9, Lorg/apache/regexp/REProgram;->flags:I

    and-int/lit8 v9, v9, 0x2

    const/4 v10, 0x2

    if-ne v9, v10, :cond_7

    .line 1445
    iget v9, p0, Lorg/apache/regexp/RE;->matchFlags:I

    and-int/lit8 v9, v9, 0x2

    if-nez v9, :cond_5

    .line 1447
    if-nez p2, :cond_2

    invoke-virtual {p0, p2}, Lorg/apache/regexp/RE;->matchAt(I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1518
    :cond_1
    :goto_0
    return v7

    :cond_2
    move v7, v8

    .line 1447
    goto :goto_0

    .line 1454
    :cond_3
    invoke-direct {p0, p2}, Lorg/apache/regexp/RE;->isNewline(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1451
    :cond_4
    add-int/lit8 p2, p2, 0x1

    :cond_5
    invoke-interface {p1, p2}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v9

    if-eqz v9, :cond_3

    move v7, v8

    .line 1475
    goto :goto_0

    .line 1460
    :cond_6
    invoke-virtual {p0, p2}, Lorg/apache/regexp/RE;->matchAt(I)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1466
    :goto_1
    invoke-interface {p1, p2}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1468
    invoke-direct {p0, p2}, Lorg/apache/regexp/RE;->isNewline(I)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1466
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 1479
    :cond_7
    iget-object v9, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    iget-object v9, v9, Lorg/apache/regexp/REProgram;->prefix:[C

    if-nez v9, :cond_9

    .line 1482
    :goto_2
    add-int/lit8 v9, p2, -0x1

    invoke-interface {p1, v9}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v9

    if-eqz v9, :cond_8

    move v7, v8

    .line 1490
    goto :goto_0

    .line 1485
    :cond_8
    invoke-virtual {p0, p2}, Lorg/apache/regexp/RE;->matchAt(I)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1482
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    .line 1495
    :cond_9
    iget v9, p0, Lorg/apache/regexp/RE;->matchFlags:I

    and-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_a

    move v0, v7

    .line 1496
    .local v0, "caseIndependent":Z
    :goto_3
    iget-object v9, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    iget-object v6, v9, Lorg/apache/regexp/REProgram;->prefix:[C

    .line 1497
    .local v6, "prefix":[C
    :goto_4
    array-length v9, v6

    add-int/2addr v9, p2

    add-int/lit8 v9, v9, -0x1

    invoke-interface {p1, v9}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v9

    if-eqz v9, :cond_b

    move v7, v8

    .line 1518
    goto :goto_0

    .end local v0    # "caseIndependent":Z
    .end local v6    # "prefix":[C
    :cond_a
    move v0, v8

    .line 1495
    goto :goto_3

    .line 1499
    .restart local v0    # "caseIndependent":Z
    .restart local v6    # "prefix":[C
    :cond_b
    move v1, p2

    .line 1500
    .local v1, "j":I
    const/4 v3, 0x0

    .line 1505
    .local v3, "k":I
    :goto_5
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "j":I
    .local v2, "j":I
    invoke-interface {p1, v1}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v9

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "k":I
    .local v4, "k":I
    aget-char v10, v6, v3

    invoke-direct {p0, v9, v10, v0}, Lorg/apache/regexp/RE;->compareChars(CCZ)I

    move-result v9

    if-nez v9, :cond_e

    move v5, v7

    .line 1506
    .local v5, "match":Z
    :goto_6
    if-eqz v5, :cond_c

    array-length v9, v6

    .line 1503
    if-lt v4, v9, :cond_f

    .line 1509
    :cond_c
    array-length v9, v6

    if-ne v4, v9, :cond_d

    .line 1512
    invoke-virtual {p0, p2}, Lorg/apache/regexp/RE;->matchAt(I)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1497
    :cond_d
    add-int/lit8 p2, p2, 0x1

    goto :goto_4

    .end local v5    # "match":Z
    :cond_e
    move v5, v8

    .line 1505
    goto :goto_6

    .restart local v5    # "match":Z
    :cond_f
    move v3, v4

    .end local v4    # "k":I
    .restart local v3    # "k":I
    move v1, v2

    .end local v2    # "j":I
    .restart local v1    # "j":I
    goto :goto_5
.end method

.method protected matchAt(I)Z
    .locals 5
    .param p1, "i"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 1376
    iput v4, p0, Lorg/apache/regexp/RE;->start0:I

    .line 1377
    iput v4, p0, Lorg/apache/regexp/RE;->end0:I

    .line 1378
    iput v4, p0, Lorg/apache/regexp/RE;->start1:I

    .line 1379
    iput v4, p0, Lorg/apache/regexp/RE;->end1:I

    .line 1380
    iput v4, p0, Lorg/apache/regexp/RE;->start2:I

    .line 1381
    iput v4, p0, Lorg/apache/regexp/RE;->end2:I

    .line 1382
    iput-object v3, p0, Lorg/apache/regexp/RE;->startn:[I

    .line 1383
    iput-object v3, p0, Lorg/apache/regexp/RE;->endn:[I

    .line 1384
    iput v1, p0, Lorg/apache/regexp/RE;->parenCount:I

    .line 1385
    invoke-virtual {p0, v2, p1}, Lorg/apache/regexp/RE;->setParenStart(II)V

    .line 1388
    iget-object v3, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    iget v3, v3, Lorg/apache/regexp/REProgram;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 1390
    iget v3, p0, Lorg/apache/regexp/RE;->maxParen:I

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/regexp/RE;->startBackref:[I

    .line 1391
    iget v3, p0, Lorg/apache/regexp/RE;->maxParen:I

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/regexp/RE;->endBackref:[I

    .line 1396
    :cond_0
    const/high16 v3, 0x10000

    invoke-virtual {p0, v2, v3, p1}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v0

    .local v0, "idx":I
    if-eq v0, v4, :cond_1

    .line 1398
    invoke-virtual {p0, v2, v0}, Lorg/apache/regexp/RE;->setParenEnd(II)V

    .line 1404
    :goto_0
    return v1

    .line 1403
    :cond_1
    iput v2, p0, Lorg/apache/regexp/RE;->parenCount:I

    move v1, v2

    .line 1404
    goto :goto_0
.end method

.method protected matchNodes(III)I
    .locals 30
    .param p1, "firstNode"    # I
    .param p2, "lastNode"    # I
    .param p3, "idxStart"    # I

    .prologue
    .line 807
    move/from16 v10, p3

    .line 812
    .local v10, "idx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v15, v0, Lorg/apache/regexp/REProgram;->instruction:[C

    .line 813
    .local v15, "instruction":[C
    move/from16 v21, p1

    .local v21, "node":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, p2

    if-lt v0, v1, :cond_1

    .line 1361
    const-string v27, "Corrupt program"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RE;->internalError(Ljava/lang/String;)V

    .line 1362
    const/4 v13, -0x1

    :cond_0
    :goto_1
    return v13

    .line 815
    :cond_1
    aget-char v22, v15, v21

    .line 816
    .local v22, "opcode":I
    add-int/lit8 v27, v21, 0x2

    aget-char v27, v15, v27

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    add-int v19, v21, v27

    .line 817
    .local v19, "next":I
    add-int/lit8 v27, v21, 0x1

    aget-char v23, v15, v27

    .line 819
    .local v23, "opdata":I
    sparse-switch v22, :sswitch_data_0

    .line 1353
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Invalid opcode \'"

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\'"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RE;->internalError(Ljava/lang/String;)V

    .line 1357
    :cond_2
    :goto_2
    :sswitch_0
    move/from16 v21, v19

    goto :goto_0

    .line 827
    :sswitch_1
    add-int/lit8 v27, v21, 0x3

    const/high16 v28, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .local v13, "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v13, v0, :cond_2

    goto :goto_1

    .line 839
    .end local v13    # "idxNew":I
    :sswitch_2
    const/high16 v27, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .restart local v13    # "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-ne v13, v0, :cond_0

    .line 847
    add-int/lit8 v27, v19, 0x2

    aget-char v27, v15, v27

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    add-int v21, v19, v27

    .line 848
    goto :goto_0

    .line 855
    .end local v13    # "idxNew":I
    :sswitch_3
    const/high16 v27, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .restart local v13    # "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-ne v13, v0, :cond_0

    .line 863
    add-int/lit8 v27, v21, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    goto/16 :goto_1

    .line 869
    .end local v13    # "idxNew":I
    :sswitch_4
    add-int/lit8 v27, v19, 0x2

    aget-char v27, v15, v27

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    add-int v27, v27, v19

    const/high16 v28, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .restart local v13    # "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v13, v0, :cond_2

    goto/16 :goto_1

    .line 881
    .end local v13    # "idxNew":I
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/regexp/REProgram;->flags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x1

    if-eqz v27, :cond_3

    .line 883
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->startBackref:[I

    move-object/from16 v27, v0

    aput v10, v27, v23

    .line 885
    :cond_3
    const/high16 v27, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .restart local v13    # "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v13, v0, :cond_0

    .line 888
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->parenCount:I

    move/from16 v27, v0

    move/from16 v0, v23

    move/from16 v1, v27

    if-lt v0, v1, :cond_4

    .line 890
    add-int/lit8 v27, v23, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RE;->parenCount:I

    .line 894
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RE;->getParenStart(I)I

    move-result v27

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_0

    .line 896
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1, v10}, Lorg/apache/regexp/RE;->setParenStart(II)V

    goto/16 :goto_1

    .line 904
    .end local v13    # "idxNew":I
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/regexp/REProgram;->flags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x1

    if-eqz v27, :cond_5

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->endBackref:[I

    move-object/from16 v27, v0

    aput v10, v27, v23

    .line 908
    :cond_5
    const/high16 v27, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .restart local v13    # "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v13, v0, :cond_0

    .line 911
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->parenCount:I

    move/from16 v27, v0

    move/from16 v0, v23

    move/from16 v1, v27

    if-lt v0, v1, :cond_6

    .line 913
    add-int/lit8 v27, v23, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RE;->parenCount:I

    .line 917
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RE;->getParenEnd(I)I

    move-result v27

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_0

    .line 919
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1, v10}, Lorg/apache/regexp/RE;->setParenEnd(II)V

    goto/16 :goto_1

    .line 927
    .end local v13    # "idxNew":I
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->startBackref:[I

    move-object/from16 v27, v0

    aget v24, v27, v23

    .line 928
    .local v24, "s":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->endBackref:[I

    move-object/from16 v27, v0

    aget v7, v27, v23

    .line 931
    .local v7, "e":I
    const/16 v27, -0x1

    move/from16 v0, v24

    move/from16 v1, v27

    if-eq v0, v1, :cond_7

    const/16 v27, -0x1

    move/from16 v0, v27

    if-ne v7, v0, :cond_8

    .line 933
    :cond_7
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 937
    :cond_8
    move/from16 v0, v24

    if-eq v0, v7, :cond_2

    .line 943
    sub-int v17, v7, v24

    .line 946
    .local v17, "l":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    add-int v28, v10, v17

    add-int/lit8 v28, v28, -0x1

    invoke-interface/range {v27 .. v28}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 948
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 953
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->matchFlags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x1

    if-eqz v27, :cond_a

    const/4 v6, 0x1

    .line 955
    .local v6, "caseFold":Z
    :goto_3
    const/4 v8, 0x0

    .local v8, "i":I
    move v11, v10

    .end local v10    # "idx":I
    .local v11, "idx":I
    :goto_4
    move/from16 v0, v17

    if-lt v8, v0, :cond_b

    move v10, v11

    .line 963
    .end local v11    # "idx":I
    .restart local v10    # "idx":I
    goto/16 :goto_2

    .line 953
    .end local v6    # "caseFold":Z
    .end local v8    # "i":I
    :cond_a
    const/4 v6, 0x0

    goto :goto_3

    .line 957
    .end local v10    # "idx":I
    .restart local v6    # "caseFold":Z
    .restart local v8    # "i":I
    .restart local v11    # "idx":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "idx":I
    .restart local v10    # "idx":I
    move-object/from16 v0, v27

    invoke-interface {v0, v11}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v28, v0

    add-int v29, v24, v8

    invoke-interface/range {v28 .. v29}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/regexp/RE;->compareChars(CCZ)I

    move-result v27

    if-eqz v27, :cond_c

    .line 959
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 955
    :cond_c
    add-int/lit8 v8, v8, 0x1

    move v11, v10

    .end local v10    # "idx":I
    .restart local v11    # "idx":I
    goto :goto_4

    .line 968
    .end local v6    # "caseFold":Z
    .end local v7    # "e":I
    .end local v8    # "i":I
    .end local v11    # "idx":I
    .end local v17    # "l":I
    .end local v24    # "s":I
    .restart local v10    # "idx":I
    :sswitch_8
    if-eqz v10, :cond_2

    .line 971
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->matchFlags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x2

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_d

    .line 974
    add-int/lit8 v27, v10, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/regexp/RE;->isNewline(I)Z

    move-result v27

    if-nez v27, :cond_2

    .line 979
    :cond_d
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 986
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-interface/range {v27 .. v28}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-nez v27, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-nez v27, :cond_2

    .line 989
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->matchFlags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x2

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_e

    .line 992
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/apache/regexp/RE;->isNewline(I)Z

    move-result v27

    if-nez v27, :cond_2

    .line 997
    :cond_e
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1004
    :sswitch_a
    sparse-switch v23, :sswitch_data_1

    .line 1066
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Unrecognized escape \'"

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\'"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RE;->internalError(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1010
    :sswitch_b
    if-nez v10, :cond_f

    const/16 v4, 0xa

    .line 1011
    .local v4, "cLast":C
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_10

    const/16 v5, 0xa

    .line 1012
    .local v5, "cNext":C
    :goto_6
    invoke-static {v4}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v27

    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_11

    const/16 v27, 0x1

    move/from16 v28, v27

    :goto_7
    const/16 v27, 0x62

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_12

    const/16 v27, 0x1

    :goto_8
    move/from16 v0, v28

    move/from16 v1, v27

    if-ne v0, v1, :cond_2

    .line 1014
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1010
    .end local v4    # "cLast":C
    .end local v5    # "cNext":C
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    add-int/lit8 v28, v10, -0x1

    invoke-interface/range {v27 .. v28}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v4

    goto :goto_5

    .line 1011
    .restart local v4    # "cLast":C
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v5

    goto :goto_6

    .line 1012
    .restart local v5    # "cNext":C
    :cond_11
    const/16 v27, 0x0

    move/from16 v28, v27

    goto :goto_7

    :cond_12
    const/16 v27, 0x0

    goto :goto_8

    .line 1028
    .end local v4    # "cLast":C
    .end local v5    # "cNext":C
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 1030
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1033
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v3

    .line 1036
    .local v3, "c":C
    sparse-switch v23, :sswitch_data_2

    .line 1062
    :cond_14
    add-int/lit8 v10, v10, 0x1

    .line 1063
    goto/16 :goto_2

    .line 1040
    :sswitch_d
    invoke-static {v3}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v27

    if-nez v27, :cond_15

    const/16 v27, 0x5f

    move/from16 v0, v27

    if-eq v3, v0, :cond_15

    const/16 v27, 0x0

    move/from16 v28, v27

    :goto_9
    const/16 v27, 0x77

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_16

    const/16 v27, 0x1

    :goto_a
    move/from16 v0, v28

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    .line 1042
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1040
    :cond_15
    const/16 v27, 0x1

    move/from16 v28, v27

    goto :goto_9

    :cond_16
    const/16 v27, 0x0

    goto :goto_a

    .line 1048
    :sswitch_e
    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v28

    const/16 v27, 0x64

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_17

    const/16 v27, 0x1

    :goto_b
    move/from16 v0, v28

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    .line 1050
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1048
    :cond_17
    const/16 v27, 0x0

    goto :goto_b

    .line 1056
    :sswitch_f
    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v28

    const/16 v27, 0x73

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_18

    const/16 v27, 0x1

    :goto_c
    move/from16 v0, v28

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    .line 1058
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1056
    :cond_18
    const/16 v27, 0x0

    goto :goto_c

    .line 1072
    .end local v3    # "c":C
    :sswitch_10
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->matchFlags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x4

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_19

    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_1b

    .line 1076
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1082
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-nez v27, :cond_1a

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/apache/regexp/RE;->isNewline(I)Z

    move-result v27

    if-eqz v27, :cond_1b

    .line 1084
    :cond_1a
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1087
    :cond_1b
    add-int/lit8 v10, v10, 0x1

    .line 1088
    goto/16 :goto_2

    .line 1093
    :sswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 1095
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1100
    :cond_1c
    add-int/lit8 v25, v21, 0x3

    .line 1103
    .local v25, "startAtom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    add-int v28, v23, v10

    add-int/lit8 v28, v28, -0x1

    invoke-interface/range {v27 .. v28}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_1d

    .line 1105
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1110
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->matchFlags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x1

    if-eqz v27, :cond_1e

    const/4 v6, 0x1

    .line 1112
    .restart local v6    # "caseFold":Z
    :goto_d
    const/4 v8, 0x0

    .restart local v8    # "i":I
    move v11, v10

    .end local v10    # "idx":I
    .restart local v11    # "idx":I
    :goto_e
    move/from16 v0, v23

    if-lt v8, v0, :cond_1f

    move v10, v11

    .line 1120
    .end local v11    # "idx":I
    .restart local v10    # "idx":I
    goto/16 :goto_2

    .line 1110
    .end local v6    # "caseFold":Z
    .end local v8    # "i":I
    :cond_1e
    const/4 v6, 0x0

    goto :goto_d

    .line 1114
    .end local v10    # "idx":I
    .restart local v6    # "caseFold":Z
    .restart local v8    # "i":I
    .restart local v11    # "idx":I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "idx":I
    .restart local v10    # "idx":I
    move-object/from16 v0, v27

    invoke-interface {v0, v11}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    add-int v28, v25, v8

    aget-char v28, v15, v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/regexp/RE;->compareChars(CCZ)I

    move-result v27

    if-eqz v27, :cond_20

    .line 1116
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1112
    :cond_20
    add-int/lit8 v8, v8, 0x1

    move v11, v10

    .end local v10    # "idx":I
    .restart local v11    # "idx":I
    goto :goto_e

    .line 1125
    .end local v6    # "caseFold":Z
    .end local v8    # "i":I
    .end local v11    # "idx":I
    .end local v25    # "startAtom":I
    .restart local v10    # "idx":I
    :sswitch_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_21

    .line 1127
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1130
    :cond_21
    sparse-switch v23, :sswitch_data_3

    .line 1254
    const-string v27, "Bad posix class"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RE;->internalError(Ljava/lang/String;)V

    .line 1259
    :cond_22
    :pswitch_0
    add-int/lit8 v10, v10, 0x1

    .line 1261
    goto/16 :goto_2

    .line 1133
    :sswitch_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1135
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1140
    :sswitch_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isLetter(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1142
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1147
    :sswitch_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isDigit(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1149
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1154
    :sswitch_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1156
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1161
    :sswitch_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1163
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1168
    :sswitch_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->getType(C)I

    move-result v27

    const/16 v28, 0xf

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_22

    .line 1170
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1175
    :sswitch_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->getType(C)I

    move-result v27

    packed-switch v27, :pswitch_data_0

    .line 1184
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1189
    :sswitch_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->getType(C)I

    move-result v27

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_22

    .line 1191
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1196
    :sswitch_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->getType(C)I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_22

    .line 1198
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1203
    :sswitch_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->getType(C)I

    move-result v27

    const/16 v28, 0xf

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_22

    .line 1205
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1211
    :sswitch_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->getType(C)I

    move-result v26

    .line 1212
    .local v26, "type":I
    packed-switch v26, :pswitch_data_1

    .line 1222
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1229
    .end local v26    # "type":I
    :sswitch_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    const/16 v28, 0x30

    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    const/16 v28, 0x39

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_26

    .line 1230
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    const/16 v28, 0x61

    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    const/16 v28, 0x66

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_26

    .line 1231
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    const/16 v28, 0x41

    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    const/16 v28, 0x46

    .line 1229
    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_26

    :cond_25
    const/16 v16, 0x0

    .line 1232
    .local v16, "isXDigit":Z
    :goto_f
    if-nez v16, :cond_22

    .line 1234
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1229
    .end local v16    # "isXDigit":Z
    :cond_26
    const/16 v16, 0x1

    goto :goto_f

    .line 1240
    :sswitch_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isJavaIdentifierStart(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1242
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1247
    :sswitch_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v27

    if-nez v27, :cond_22

    .line 1249
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1266
    :sswitch_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->isEnd(I)Z

    move-result v27

    if-eqz v27, :cond_27

    .line 1268
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1272
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RE;->search:Lorg/apache/regexp/CharacterIterator;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Lorg/apache/regexp/CharacterIterator;->charAt(I)C

    move-result v3

    .line 1273
    .restart local v3    # "c":C
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RE;->matchFlags:I

    move/from16 v27, v0

    and-int/lit8 v27, v27, 0x1

    if-eqz v27, :cond_29

    const/4 v6, 0x1

    .line 1275
    .restart local v6    # "caseFold":Z
    :goto_10
    add-int/lit8 v14, v21, 0x3

    .line 1276
    .local v14, "idxRange":I
    mul-int/lit8 v27, v23, 0x2

    add-int v12, v14, v27

    .line 1277
    .local v12, "idxEnd":I
    const/16 v18, 0x0

    .line 1278
    .local v18, "match":Z
    move v8, v14

    .restart local v8    # "i":I
    move v9, v8

    .end local v8    # "i":I
    .local v9, "i":I
    :goto_11
    if-nez v18, :cond_28

    if-lt v9, v12, :cond_2a

    .line 1289
    :cond_28
    if-nez v18, :cond_2c

    .line 1291
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1273
    .end local v6    # "caseFold":Z
    .end local v9    # "i":I
    .end local v12    # "idxEnd":I
    .end local v14    # "idxRange":I
    .end local v18    # "match":Z
    :cond_29
    const/4 v6, 0x0

    goto :goto_10

    .line 1281
    .restart local v6    # "caseFold":Z
    .restart local v9    # "i":I
    .restart local v12    # "idxEnd":I
    .restart local v14    # "idxRange":I
    .restart local v18    # "match":Z
    :cond_2a
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "i":I
    .restart local v8    # "i":I
    aget-char v24, v15, v9

    .line 1282
    .local v24, "s":C
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "i":I
    .restart local v9    # "i":I
    aget-char v7, v15, v8

    .line 1284
    .local v7, "e":C
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v3, v1, v6}, Lorg/apache/regexp/RE;->compareChars(CCZ)I

    move-result v27

    if-ltz v27, :cond_2b

    .line 1285
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7, v6}, Lorg/apache/regexp/RE;->compareChars(CCZ)I

    move-result v27

    .line 1284
    if-gtz v27, :cond_2b

    const/16 v18, 0x1

    :goto_12
    goto :goto_11

    :cond_2b
    const/16 v18, 0x0

    goto :goto_12

    .line 1293
    .end local v7    # "e":C
    .end local v24    # "s":C
    :cond_2c
    add-int/lit8 v10, v10, 0x1

    .line 1295
    goto/16 :goto_2

    .line 1301
    .end local v3    # "c":C
    .end local v6    # "caseFold":Z
    .end local v9    # "i":I
    .end local v12    # "idxEnd":I
    .end local v14    # "idxRange":I
    .end local v18    # "match":Z
    :sswitch_22
    aget-char v27, v15, v19

    const/16 v28, 0x7c

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_2d

    .line 1304
    add-int/lit8 v21, v21, 0x3

    .line 1305
    goto/16 :goto_0

    .line 1313
    :cond_2d
    add-int/lit8 v27, v21, 0x3

    const/high16 v28, 0x10000

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/regexp/RE;->matchNodes(III)I

    move-result v13

    .restart local v13    # "idxNew":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-ne v13, v0, :cond_0

    .line 1319
    add-int/lit8 v27, v21, 0x2

    aget-char v27, v15, v27

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v20, v0

    .line 1320
    .local v20, "nextBranch":I
    add-int v21, v21, v20

    .line 1322
    if-eqz v20, :cond_2e

    aget-char v27, v15, v21

    const/16 v28, 0x7c

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_2d

    .line 1325
    :cond_2e
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 1341
    .end local v13    # "idxNew":I
    .end local v20    # "nextBranch":I
    :sswitch_23
    add-int/lit8 v21, v21, 0x3

    .line 1342
    goto/16 :goto_0

    .line 1347
    :sswitch_24
    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1, v10}, Lorg/apache/regexp/RE;->setParenEnd(II)V

    move v13, v10

    .line 1348
    goto/16 :goto_1

    .line 819
    :sswitch_data_0
    .sparse-switch
        0x23 -> :sswitch_7
        0x24 -> :sswitch_9
        0x28 -> :sswitch_5
        0x29 -> :sswitch_6
        0x2a -> :sswitch_1
        0x2b -> :sswitch_2
        0x2e -> :sswitch_10
        0x2f -> :sswitch_3
        0x38 -> :sswitch_3
        0x3c -> :sswitch_0
        0x3d -> :sswitch_4
        0x3e -> :sswitch_0
        0x3f -> :sswitch_1
        0x41 -> :sswitch_11
        0x43 -> :sswitch_23
        0x45 -> :sswitch_24
        0x47 -> :sswitch_0
        0x4e -> :sswitch_0
        0x50 -> :sswitch_12
        0x5b -> :sswitch_21
        0x5c -> :sswitch_a
        0x5e -> :sswitch_8
        0x7c -> :sswitch_22
    .end sparse-switch

    .line 1004
    :sswitch_data_1
    .sparse-switch
        0x42 -> :sswitch_b
        0x44 -> :sswitch_c
        0x53 -> :sswitch_c
        0x57 -> :sswitch_c
        0x62 -> :sswitch_b
        0x64 -> :sswitch_c
        0x73 -> :sswitch_c
        0x77 -> :sswitch_c
    .end sparse-switch

    .line 1036
    :sswitch_data_2
    .sparse-switch
        0x44 -> :sswitch_e
        0x53 -> :sswitch_f
        0x57 -> :sswitch_d
        0x64 -> :sswitch_e
        0x73 -> :sswitch_f
        0x77 -> :sswitch_d
    .end sparse-switch

    .line 1130
    :sswitch_data_3
    .sparse-switch
        0x21 -> :sswitch_1d
        0x61 -> :sswitch_14
        0x62 -> :sswitch_16
        0x63 -> :sswitch_18
        0x64 -> :sswitch_15
        0x67 -> :sswitch_19
        0x6a -> :sswitch_1f
        0x6b -> :sswitch_20
        0x6c -> :sswitch_1a
        0x70 -> :sswitch_1c
        0x73 -> :sswitch_17
        0x75 -> :sswitch_1b
        0x77 -> :sswitch_13
        0x78 -> :sswitch_1e
    .end sparse-switch

    .line 1175
    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1212
    :pswitch_data_1
    .packed-switch 0x14
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setMatchFlags(I)V
    .locals 0
    .param p1, "matchFlags"    # I

    .prologue
    .line 542
    iput p1, p0, Lorg/apache/regexp/RE;->matchFlags:I

    .line 543
    return-void
.end method

.method protected final setParenEnd(II)V
    .locals 1
    .param p1, "which"    # I
    .param p2, "i"    # I

    .prologue
    .line 739
    iget v0, p0, Lorg/apache/regexp/RE;->parenCount:I

    if-ge p1, v0, :cond_1

    .line 741
    packed-switch p1, :pswitch_data_0

    .line 756
    iget-object v0, p0, Lorg/apache/regexp/RE;->endn:[I

    if-nez v0, :cond_0

    .line 758
    invoke-direct {p0}, Lorg/apache/regexp/RE;->allocParens()V

    .line 760
    :cond_0
    iget-object v0, p0, Lorg/apache/regexp/RE;->endn:[I

    aput p2, v0, p1

    .line 764
    :cond_1
    :goto_0
    return-void

    .line 744
    :pswitch_0
    iput p2, p0, Lorg/apache/regexp/RE;->end0:I

    goto :goto_0

    .line 748
    :pswitch_1
    iput p2, p0, Lorg/apache/regexp/RE;->end1:I

    goto :goto_0

    .line 752
    :pswitch_2
    iput p2, p0, Lorg/apache/regexp/RE;->end2:I

    goto :goto_0

    .line 741
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final setParenStart(II)V
    .locals 1
    .param p1, "which"    # I
    .param p2, "i"    # I

    .prologue
    .line 704
    iget v0, p0, Lorg/apache/regexp/RE;->parenCount:I

    if-ge p1, v0, :cond_1

    .line 706
    packed-switch p1, :pswitch_data_0

    .line 721
    iget-object v0, p0, Lorg/apache/regexp/RE;->startn:[I

    if-nez v0, :cond_0

    .line 723
    invoke-direct {p0}, Lorg/apache/regexp/RE;->allocParens()V

    .line 725
    :cond_0
    iget-object v0, p0, Lorg/apache/regexp/RE;->startn:[I

    aput p2, v0, p1

    .line 729
    :cond_1
    :goto_0
    return-void

    .line 709
    :pswitch_0
    iput p2, p0, Lorg/apache/regexp/RE;->start0:I

    goto :goto_0

    .line 713
    :pswitch_1
    iput p2, p0, Lorg/apache/regexp/RE;->start1:I

    goto :goto_0

    .line 717
    :pswitch_2
    iput p2, p0, Lorg/apache/regexp/RE;->start2:I

    goto :goto_0

    .line 706
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setProgram(Lorg/apache/regexp/REProgram;)V
    .locals 2
    .param p1, "program"    # Lorg/apache/regexp/REProgram;

    .prologue
    .line 572
    iput-object p1, p0, Lorg/apache/regexp/RE;->program:Lorg/apache/regexp/REProgram;

    .line 573
    if-eqz p1, :cond_0

    iget v0, p1, Lorg/apache/regexp/REProgram;->maxParens:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 574
    iget v0, p1, Lorg/apache/regexp/REProgram;->maxParens:I

    iput v0, p0, Lorg/apache/regexp/RE;->maxParen:I

    .line 578
    :goto_0
    return-void

    .line 576
    :cond_0
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/regexp/RE;->maxParen:I

    goto :goto_0
.end method

.method public split(Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 1550
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 1553
    .local v6, "v":Ljava/util/Vector;
    const/4 v2, 0x0

    .line 1554
    .local v2, "pos":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1557
    .local v0, "len":I
    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1, v2}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;I)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1581
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1582
    .local v3, "remainder":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    .line 1584
    invoke-virtual {v6, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1588
    :cond_1
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v7

    new-array v4, v7, [Ljava/lang/String;

    .line 1589
    .local v4, "ret":[Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 1590
    return-object v4

    .line 1560
    .end local v3    # "remainder":Ljava/lang/String;
    .end local v4    # "ret":[Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v8}, Lorg/apache/regexp/RE;->getParenStart(I)I

    move-result v5

    .line 1563
    .local v5, "start":I
    invoke-virtual {p0, v8}, Lorg/apache/regexp/RE;->getParenEnd(I)I

    move-result v1

    .line 1566
    .local v1, "newpos":I
    if-ne v1, v2, :cond_3

    .line 1568
    add-int/lit8 v7, v5, 0x1

    invoke-virtual {p1, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1569
    add-int/lit8 v1, v1, 0x1

    .line 1577
    :goto_1
    move v2, v1

    goto :goto_0

    .line 1573
    :cond_3
    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public subst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "substituteIn"    # Ljava/lang/String;
    .param p2, "substitution"    # Ljava/lang/String;

    .prologue
    .line 1626
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p1, "substituteIn"    # Ljava/lang/String;
    .param p2, "substitution"    # Ljava/lang/String;
    .param p3, "flags"    # I

    .prologue
    const/4 v11, 0x0

    .line 1660
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 1663
    .local v7, "ret":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 1664
    .local v6, "pos":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 1667
    .local v4, "len":I
    :cond_0
    if-ge v6, v4, :cond_1

    invoke-virtual {p0, p1, v6}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;I)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1732
    :cond_1
    :goto_0
    if-ge v6, v4, :cond_2

    .line 1734
    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1738
    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 1670
    :cond_3
    invoke-virtual {p0, v11}, Lorg/apache/regexp/RE;->getParenStart(I)I

    move-result v9

    invoke-virtual {p1, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1672
    and-int/lit8 v9, p3, 0x2

    if-eqz v9, :cond_9

    .line 1675
    const/4 v1, 0x0

    .line 1676
    .local v1, "lCurrentPosition":I
    const/4 v2, -0x2

    .line 1677
    .local v2, "lLastPosition":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    .line 1679
    .local v3, "lLength":I
    :goto_1
    const-string v9, "$"

    invoke-virtual {p2, v9, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    if-gez v1, :cond_5

    .line 1704
    add-int/lit8 v9, v2, 0x2

    invoke-virtual {p2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1713
    .end local v1    # "lCurrentPosition":I
    .end local v2    # "lLastPosition":I
    .end local v3    # "lLength":I
    :goto_2
    invoke-virtual {p0, v11}, Lorg/apache/regexp/RE;->getParenEnd(I)I

    move-result v5

    .line 1716
    .local v5, "newpos":I
    if-ne v5, v6, :cond_4

    .line 1718
    add-int/lit8 v5, v5, 0x1

    .line 1722
    :cond_4
    move v6, v5

    .line 1725
    and-int/lit8 v9, p3, 0x1

    if-eqz v9, :cond_0

    goto :goto_0

    .line 1681
    .end local v5    # "newpos":I
    .restart local v1    # "lCurrentPosition":I
    .restart local v2    # "lLastPosition":I
    .restart local v3    # "lLength":I
    :cond_5
    if-eqz v1, :cond_6

    add-int/lit8 v9, v1, -0x1

    invoke-virtual {p2, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x5c

    if-eq v9, v10, :cond_8

    .line 1682
    :cond_6
    add-int/lit8 v9, v1, 0x1

    if-ge v9, v3, :cond_8

    .line 1684
    add-int/lit8 v9, v1, 0x1

    invoke-virtual {p2, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1685
    .local v0, "c":C
    const/16 v9, 0x30

    if-lt v0, v9, :cond_8

    const/16 v9, 0x39

    if-gt v0, v9, :cond_8

    .line 1688
    add-int/lit8 v9, v2, 0x2

    invoke-virtual {p2, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1691
    add-int/lit8 v9, v0, -0x30

    invoke-virtual {p0, v9}, Lorg/apache/regexp/RE;->getParen(I)Ljava/lang/String;

    move-result-object v8

    .line 1692
    .local v8, "val":Ljava/lang/String;
    if-eqz v8, :cond_7

    .line 1693
    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1695
    :cond_7
    move v2, v1

    .line 1700
    .end local v0    # "c":C
    .end local v8    # "val":Ljava/lang/String;
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1709
    .end local v1    # "lCurrentPosition":I
    .end local v2    # "lLastPosition":I
    .end local v3    # "lLength":I
    :cond_9
    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2
.end method
