.class public Lorg/apache/regexp/REDebugCompiler;
.super Lorg/apache/regexp/RECompiler;
.source "REDebugCompiler.java"


# static fields
.field static hashOpcode:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    .line 38
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x38

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_RELUCTANTSTAR"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x3d

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_RELUCTANTPLUS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_RELUCTANTMAYBE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x45

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_END"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x5e

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_BOL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_EOL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x2e

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_ANY"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x5b

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_ANYOF"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x7c

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_BRANCH"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x41

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_ATOM"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_STAR"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x2b

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_PLUS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x3f

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_MAYBE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x4e

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_NOTHING"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x47

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_GOTO"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x43

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_CONTINUE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x5c

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_ESCAPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_OPEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_CLOSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_BACKREF"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x50

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_POSIXCLASS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x3c

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_OPEN_CLUSTER"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x3e

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const-string v2, "OP_CLOSE_CLUSTER"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/regexp/RECompiler;-><init>()V

    return-void
.end method


# virtual methods
.method charToString(C)Ljava/lang/String;
    .locals 2
    .param p1, "c"    # C

    .prologue
    .line 91
    const/16 v0, 0x20

    if-lt p1, v0, :cond_0

    const/16 v0, 0x7f

    if-le p1, v0, :cond_1

    .line 93
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public dumpProgram()V
    .locals 2

    .prologue
    .line 261
    new-instance v0, Ljava/io/PrintWriter;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 262
    .local v0, "w":Ljava/io/PrintWriter;
    invoke-virtual {p0, v0}, Lorg/apache/regexp/REDebugCompiler;->dumpProgram(Ljava/io/PrintWriter;)V

    .line 263
    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    .line 264
    return-void
.end method

.method public dumpProgram(Ljava/io/PrintWriter;)V
    .locals 12
    .param p1, "p"    # Ljava/io/PrintWriter;

    .prologue
    .line 183
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v10, p0, Lorg/apache/regexp/REDebugCompiler;->lenInstruction:I

    if-lt v2, v10, :cond_0

    .line 254
    return-void

    .line 186
    :cond_0
    iget-object v10, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    aget-char v7, v10, v2

    .line 187
    .local v7, "opcode":C
    iget-object v10, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    add-int/lit8 v11, v2, 0x1

    aget-char v8, v10, v11

    .line 188
    .local v8, "opdata":C
    iget-object v10, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    add-int/lit8 v11, v2, 0x2

    aget-char v10, v10, v11

    int-to-short v6, v10

    .line 191
    .local v6, "next":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ". "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0, v2}, Lorg/apache/regexp/REDebugCompiler;->nodeToString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", next = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 194
    if-nez v6, :cond_3

    .line 196
    const-string v10, "none"

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 204
    :goto_1
    add-int/lit8 v2, v2, 0x3

    .line 207
    const/16 v10, 0x5b

    if-ne v7, v10, :cond_1

    .line 210
    const-string v10, ", ["

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 214
    const/4 v9, 0x0

    .local v9, "r":I
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_2
    if-lt v9, v8, :cond_4

    .line 232
    const-string v10, "]"

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    move v2, v3

    .line 236
    .end local v3    # "i":I
    .end local v9    # "r":I
    .restart local v2    # "i":I
    :cond_1
    const/16 v10, 0x41

    if-ne v7, v10, :cond_2

    .line 239
    const-string v10, ", \""

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 242
    move v4, v8

    .local v4, "len":I
    move v5, v4

    .end local v4    # "len":I
    .local v5, "len":I
    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    :goto_3
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "len":I
    .restart local v4    # "len":I
    if-nez v5, :cond_6

    .line 248
    const-string v10, "\""

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    move v2, v3

    .line 252
    .end local v3    # "i":I
    .end local v4    # "len":I
    .restart local v2    # "i":I
    :cond_2
    const-string v10, ""

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :cond_3
    add-int v10, v2, v6

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(I)V

    goto :goto_1

    .line 217
    .end local v2    # "i":I
    .restart local v3    # "i":I
    .restart local v9    # "r":I
    :cond_4
    iget-object v10, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-char v0, v10, v3

    .line 218
    .local v0, "charFirst":C
    iget-object v10, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .restart local v3    # "i":I
    aget-char v1, v10, v2

    .line 221
    .local v1, "charLast":C
    if-ne v0, v1, :cond_5

    .line 223
    invoke-virtual {p0, v0}, Lorg/apache/regexp/REDebugCompiler;->charToString(C)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 214
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 227
    :cond_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Lorg/apache/regexp/REDebugCompiler;->charToString(C)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0, v1}, Lorg/apache/regexp/REDebugCompiler;->charToString(C)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_4

    .line 244
    .end local v0    # "charFirst":C
    .end local v1    # "charLast":C
    .end local v9    # "r":I
    .restart local v4    # "len":I
    :cond_6
    iget-object v10, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-char v10, v10, v3

    invoke-virtual {p0, v10}, Lorg/apache/regexp/REDebugCompiler;->charToString(C)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    move v5, v4

    .end local v4    # "len":I
    .restart local v5    # "len":I
    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_3
.end method

.method nodeToString(I)Ljava/lang/String;
    .locals 4
    .param p1, "node"    # I

    .prologue
    .line 108
    iget-object v2, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    aget-char v0, v2, p1

    .line 109
    .local v0, "opcode":C
    iget-object v2, p0, Lorg/apache/regexp/REDebugCompiler;->instruction:[C

    add-int/lit8 v3, p1, 0x1

    aget-char v1, v2, v3

    .line 112
    .local v1, "opdata":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Lorg/apache/regexp/REDebugCompiler;->opcodeToString(C)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ", opdata = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method opcodeToString(C)Ljava/lang/String;
    .locals 3
    .param p1, "opcode"    # C

    .prologue
    .line 72
    sget-object v1, Lorg/apache/regexp/REDebugCompiler;->hashOpcode:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    .local v0, "ret":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 77
    const-string v0, "OP_????"

    .line 79
    :cond_0
    return-object v0
.end method
