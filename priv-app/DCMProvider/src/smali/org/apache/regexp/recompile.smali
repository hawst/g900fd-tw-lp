.class public Lorg/apache/regexp/recompile;
.super Ljava/lang/Object;
.source "recompile.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 14
    .param p0, "arg"    # [Ljava/lang/String;

    .prologue
    .line 67
    new-instance v10, Lorg/apache/regexp/RECompiler;

    invoke-direct {v10}, Lorg/apache/regexp/RECompiler;-><init>()V

    .line 70
    .local v10, "r":Lorg/apache/regexp/RECompiler;
    array-length v11, p0

    if-lez v11, :cond_0

    array-length v11, p0

    rem-int/lit8 v11, v11, 0x2

    if-eqz v11, :cond_1

    .line 72
    :cond_0
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "Usage: recompile <patternname> <pattern>"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 73
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/System;->exit(I)V

    .line 77
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v11, p0

    if-lt v2, v11, :cond_2

    .line 132
    return-void

    .line 82
    :cond_2
    :try_start_0
    aget-object v5, p0, v2

    .line 83
    .local v5, "name":Ljava/lang/String;
    add-int/lit8 v11, v2, 0x1

    aget-object v8, p0, v11

    .line 84
    .local v8, "pattern":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "Instructions"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, "instructions":Ljava/lang/String;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\n    // Pre-compiled regular expression \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 88
    const-string v13, "    private static final char[] "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " = \n    {"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 87
    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v10, v8}, Lorg/apache/regexp/RECompiler;->compile(Ljava/lang/String;)Lorg/apache/regexp/REProgram;

    move-result-object v9

    .line 94
    .local v9, "program":Lorg/apache/regexp/REProgram;
    const/4 v6, 0x7

    .line 97
    .local v6, "numColumns":I
    invoke-virtual {v9}, Lorg/apache/regexp/REProgram;->getInstructions()[C

    move-result-object v7

    .line 98
    .local v7, "p":[C
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    array-length v11, v7

    if-lt v4, v11, :cond_3

    .line 116
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "\n    };"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 117
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\n    private static final REProgram "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " = new REProgram("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ");"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 77
    .end local v3    # "instructions":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "numColumns":I
    .end local v7    # "p":[C
    .end local v8    # "pattern":Ljava/lang/String;
    .end local v9    # "program":Lorg/apache/regexp/REProgram;
    :goto_2
    add-int/lit8 v2, v2, 0x2

    goto/16 :goto_0

    .line 101
    .restart local v3    # "instructions":Ljava/lang/String;
    .restart local v4    # "j":I
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v6    # "numColumns":I
    .restart local v7    # "p":[C
    .restart local v8    # "pattern":Ljava/lang/String;
    .restart local v9    # "program":Lorg/apache/regexp/REProgram;
    :cond_3
    rem-int v11, v4, v6

    if-nez v11, :cond_4

    .line 103
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "\n        "

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 107
    :cond_4
    aget-char v11, v7, v4

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "hex":Ljava/lang/String;
    :goto_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x4

    if-lt v11, v12, :cond_5

    .line 112
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "0x"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 98
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 110
    :cond_5
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "0"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/regexp/RESyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    goto :goto_3

    .line 119
    .end local v1    # "hex":Ljava/lang/String;
    .end local v3    # "instructions":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "numColumns":I
    .end local v7    # "p":[C
    .end local v8    # "pattern":Ljava/lang/String;
    .end local v9    # "program":Lorg/apache/regexp/REProgram;
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Lorg/apache/regexp/RESyntaxException;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Syntax error in expression \""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v13, p0, v2

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v0}, Lorg/apache/regexp/RESyntaxException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 123
    .end local v0    # "e":Lorg/apache/regexp/RESyntaxException;
    :catch_1
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unexpected exception: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Error;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Internal error: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2
.end method
