.class public Lorg/apache/regexp/RECompiler;
.super Ljava/lang/Object;
.source "RECompiler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/regexp/RECompiler$RERange;
    }
.end annotation


# static fields
.field static final ESC_BACKREF:I = 0xfffff

.field static final ESC_CLASS:I = 0xffffd

.field static final ESC_COMPLEX:I = 0xffffe

.field static final ESC_MASK:I = 0xffff0

.field static final NODE_NORMAL:I = 0x0

.field static final NODE_NULLABLE:I = 0x1

.field static final NODE_TOPLEVEL:I = 0x2

.field static final bracketUnbounded:I = -0x1

.field static final hashPOSIX:Ljava/util/Hashtable;


# instance fields
.field bracketMin:I

.field bracketOpt:I

.field idx:I

.field instruction:[C

.field len:I

.field lenInstruction:I

.field parens:I

.field pattern:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 65
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    .line 68
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "alnum"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x77

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "alpha"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x61

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "blank"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x62

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "cntrl"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x63

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "digit"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "graph"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x67

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "lower"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x6c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "print"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x70

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "punct"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "space"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x73

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "upper"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x75

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string/jumbo v1, "xdigit"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x78

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "javastart"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x6a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    const-string v1, "javapart"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x6b

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/16 v0, 0x80

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    .line 92
    return-void
.end method


# virtual methods
.method atom()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    const v8, 0xffff0

    .line 707
    const/16 v5, 0x41

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v4

    .line 710
    .local v4, "ret":I
    const/4 v3, 0x0

    .line 716
    .local v3, "lenAtom":I
    :goto_0
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-lt v5, v6, :cond_2

    .line 809
    :cond_0
    :goto_1
    :sswitch_0
    if-nez v3, :cond_1

    .line 811
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->internalError()V

    .line 815
    :cond_1
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v6, v4, 0x1

    int-to-char v7, v3

    aput-char v7, v5, v6

    .line 816
    return v4

    .line 719
    :cond_2
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v5, v5, 0x1

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_5

    .line 721
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 724
    .local v0, "c":C
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x5c

    if-ne v5, v6, :cond_4

    .line 726
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 727
    .local v2, "idxEscape":I
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->escape()I

    .line 728
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_3

    .line 730
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 732
    :cond_3
    iput v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 736
    .end local v2    # "idxEscape":I
    :cond_4
    sparse-switch v0, :sswitch_data_0

    .line 753
    .end local v0    # "c":C
    :cond_5
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    sparse-switch v5, :sswitch_data_1

    .line 802
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RECompiler;->emit(C)V

    .line 803
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 745
    .restart local v0    # "c":C
    :sswitch_1
    if-eqz v3, :cond_5

    goto :goto_1

    .line 771
    .end local v0    # "c":C
    :sswitch_2
    if-nez v3, :cond_0

    .line 774
    const-string v5, "Missing operand to closure"

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto :goto_1

    .line 782
    :sswitch_3
    iget v1, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 783
    .local v1, "idxBeforeEscape":I
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->escape()I

    move-result v0

    .line 786
    .local v0, "c":I
    and-int v5, v0, v8

    if-ne v5, v8, :cond_6

    .line 789
    iput v1, p0, Lorg/apache/regexp/RECompiler;->idx:I

    goto :goto_1

    .line 794
    :cond_6
    int-to-char v5, v0

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RECompiler;->emit(C)V

    .line 795
    add-int/lit8 v3, v3, 0x1

    .line 797
    goto/16 :goto_0

    .line 736
    nop

    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_1
        0x2b -> :sswitch_1
        0x3f -> :sswitch_1
        0x7b -> :sswitch_1
    .end sparse-switch

    .line 753
    :sswitch_data_1
    .sparse-switch
        0x24 -> :sswitch_0
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0x2e -> :sswitch_0
        0x3f -> :sswitch_2
        0x5b -> :sswitch_0
        0x5c -> :sswitch_3
        0x5d -> :sswitch_0
        0x5e -> :sswitch_0
        0x7b -> :sswitch_2
        0x7c -> :sswitch_0
    .end sparse-switch
.end method

.method bracket()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x7d

    .line 242
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x7b

    if-eq v2, v3, :cond_1

    .line 244
    :cond_0
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->internalError()V

    .line 248
    :cond_1
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_3

    .line 250
    :cond_2
    const-string v2, "Expected digit"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 254
    :cond_3
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 255
    .local v1, "number":Ljava/lang/StringBuffer;
    :goto_0
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_4

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_7

    .line 261
    :cond_4
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lorg/apache/regexp/RECompiler;->bracketMin:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_1
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-lt v2, v3, :cond_5

    .line 271
    const-string v2, "Expected comma or right bracket"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 275
    :cond_5
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_8

    .line 277
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 278
    iput v6, p0, Lorg/apache/regexp/RECompiler;->bracketOpt:I

    .line 334
    :cond_6
    :goto_2
    return-void

    .line 257
    :cond_7
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "Expected valid number"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto :goto_1

    .line 283
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_8
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_9

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_a

    .line 285
    :cond_9
    const-string v2, "Expected comma"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 289
    :cond_a
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-lt v2, v3, :cond_b

    .line 291
    const-string v2, "Expected comma or right bracket"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 295
    :cond_b
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_c

    .line 297
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 298
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/regexp/RECompiler;->bracketOpt:I

    goto :goto_2

    .line 303
    :cond_c
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_d

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_e

    .line 305
    :cond_d
    const-string v2, "Expected digit"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 309
    :cond_e
    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 310
    :goto_3
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_f

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_12

    .line 316
    :cond_f
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget v3, p0, Lorg/apache/regexp/RECompiler;->bracketMin:I

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/regexp/RECompiler;->bracketOpt:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 324
    :goto_4
    iget v2, p0, Lorg/apache/regexp/RECompiler;->bracketOpt:I

    if-gez v2, :cond_10

    .line 326
    const-string v2, "Bad range"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 330
    :cond_10
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v2, v3, :cond_11

    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_6

    .line 332
    :cond_11
    const-string v2, "Missing close brace"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 312
    :cond_12
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 318
    :catch_1
    move-exception v0

    .line 320
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    const-string v2, "Expected valid number"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto :goto_4
.end method

.method branch([I)I
    .locals 9
    .param p1, "flags"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 1106
    const/4 v4, -0x1

    .line 1107
    .local v4, "ret":I
    const/4 v0, -0x1

    .line 1108
    .local v0, "chain":I
    const/4 v5, 0x1

    new-array v1, v5, [I

    .line 1109
    .local v1, "closureFlags":[I
    const/4 v3, 0x1

    .line 1110
    .local v3, "nullable":Z
    :cond_0
    :goto_0
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_1

    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x7c

    if-eq v5, v6, :cond_1

    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x29

    if-ne v5, v6, :cond_4

    .line 1134
    :cond_1
    if-ne v4, v8, :cond_2

    .line 1136
    const/16 v5, 0x4e

    invoke-virtual {p0, v5, v7}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v4

    .line 1140
    :cond_2
    if-eqz v3, :cond_3

    .line 1142
    aget v5, p1, v7

    or-int/lit8 v5, v5, 0x1

    aput v5, p1, v7

    .line 1145
    :cond_3
    return v4

    .line 1113
    :cond_4
    aput v7, v1, v7

    .line 1114
    invoke-virtual {p0, v1}, Lorg/apache/regexp/RECompiler;->closure([I)I

    move-result v2

    .line 1115
    .local v2, "node":I
    aget v5, v1, v7

    if-nez v5, :cond_5

    .line 1117
    const/4 v3, 0x0

    .line 1121
    :cond_5
    if-eq v0, v8, :cond_6

    .line 1123
    invoke-virtual {p0, v0, v2}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1127
    :cond_6
    move v0, v2

    .line 1128
    if-ne v4, v8, :cond_0

    .line 1129
    move v4, v2

    goto :goto_0
.end method

.method characterClass()I
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x5b

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    .line 474
    invoke-virtual/range {p0 .. p0}, Lorg/apache/regexp/RECompiler;->internalError()V

    .line 478
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x5d

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 480
    :cond_1
    const-string v17, "Empty or unterminated class"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 484
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x3a

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    .line 487
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    .line 490
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 491
    .local v9, "idxStart":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x61

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x7a

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_5

    .line 497
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x3a

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x5d

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 503
    .local v5, "charClass":Ljava/lang/String;
    sget-object v17, Lorg/apache/regexp/RECompiler;->hashPOSIX:Ljava/util/Hashtable;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Character;

    .line 504
    .local v7, "i":Ljava/lang/Character;
    if-eqz v7, :cond_6

    .line 507
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    .line 510
    const/16 v17, 0x50

    invoke-virtual {v7}, Ljava/lang/Character;->charValue()C

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v15

    .line 693
    .end local v5    # "charClass":Ljava/lang/String;
    .end local v7    # "i":Ljava/lang/Character;
    .end local v9    # "idxStart":I
    :cond_4
    return v15

    .line 493
    .restart local v9    # "idxStart":I
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    goto/16 :goto_0

    .line 512
    .restart local v5    # "charClass":Ljava/lang/String;
    .restart local v7    # "i":Ljava/lang/Character;
    :cond_6
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Invalid POSIX character class \'"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\'"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 514
    .end local v5    # "charClass":Ljava/lang/String;
    .end local v7    # "i":Ljava/lang/Character;
    :cond_7
    const-string v17, "Invalid POSIX character class syntax"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 518
    .end local v9    # "idxStart":I
    :cond_8
    const/16 v17, 0x5b

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v15

    .line 521
    .local v15, "ret":I
    const v3, 0xffff

    .line 522
    .local v3, "CHAR_INVALID":C
    move v11, v3

    .line 524
    .local v11, "last":I
    const/4 v10, 0x1

    .line 525
    .local v10, "include":Z
    const/4 v6, 0x0

    .line 526
    .local v6, "definingRange":Z
    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 527
    .local v8, "idxFirst":I
    const/4 v14, 0x0

    .line 529
    .local v14, "rangeStart":C
    new-instance v12, Lorg/apache/regexp/RECompiler$RERange;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lorg/apache/regexp/RECompiler$RERange;-><init>(Lorg/apache/regexp/RECompiler;)V

    .line 530
    .end local v11    # "last":I
    .local v12, "range":Lorg/apache/regexp/RECompiler$RERange;
    :cond_9
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x5d

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 678
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 680
    const-string v17, "Unterminated character class"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 684
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->instruction:[C

    move-object/from16 v17, v0

    add-int/lit8 v18, v15, 0x1

    iget v0, v12, Lorg/apache/regexp/RECompiler$RERange;->num:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    aput-char v19, v17, v18

    .line 688
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    iget v0, v12, Lorg/apache/regexp/RECompiler$RERange;->num:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v7, v0, :cond_4

    .line 690
    iget-object v0, v12, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    move-object/from16 v17, v0

    aget v17, v17, v7

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->emit(C)V

    .line 691
    iget-object v0, v12, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    move-object/from16 v17, v0

    aget v17, v17, v7

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->emit(C)V

    .line 688
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 536
    .end local v7    # "i":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    sparse-switch v17, :sswitch_data_0

    .line 645
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    add-int/lit8 v19, v18, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v16

    .line 650
    .local v16, "simpleChar":C
    :goto_3
    if-eqz v6, :cond_13

    .line 653
    move/from16 v13, v16

    .line 656
    .local v13, "rangeEnd":C
    if-lt v14, v13, :cond_d

    .line 658
    const-string v17, "Bad character class"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 660
    :cond_d
    invoke-virtual {v12, v14, v13, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 663
    move v11, v3

    .line 664
    .restart local v11    # "last":I
    const/4 v6, 0x0

    .line 665
    goto/16 :goto_1

    .line 539
    .end local v11    # "last":I
    .end local v13    # "rangeEnd":C
    .end local v16    # "simpleChar":C
    :sswitch_0
    if-eqz v10, :cond_f

    const/4 v10, 0x0

    .line 540
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v8, :cond_e

    .line 542
    const/16 v17, 0x0

    const v18, 0xffff

    const/16 v19, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 544
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    goto/16 :goto_1

    .line 539
    :cond_f
    const/4 v10, 0x1

    goto :goto_4

    .line 551
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/regexp/RECompiler;->escape()I

    move-result v4

    .local v4, "c":I
    packed-switch v4, :pswitch_data_0

    .line 618
    int-to-char v0, v4

    move/from16 v16, v0

    .line 619
    .restart local v16    # "simpleChar":C
    goto :goto_3

    .line 557
    .end local v16    # "simpleChar":C
    :pswitch_0
    const-string v17, "Bad character class"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 562
    :pswitch_1
    if-eqz v6, :cond_10

    .line 564
    const-string v17, "Bad character class"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 568
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    sparse-switch v17, :sswitch_data_1

    .line 612
    :goto_5
    move v11, v3

    .line 613
    .restart local v11    # "last":I
    goto/16 :goto_1

    .line 571
    .end local v11    # "last":I
    :sswitch_2
    const/16 v17, 0x0

    const/16 v18, 0x7

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 572
    const/16 v17, 0xb

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 573
    const/16 v17, 0xe

    const/16 v18, 0x1f

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 574
    const/16 v17, 0x21

    const v18, 0xffff

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    goto :goto_5

    .line 578
    :sswitch_3
    const/16 v17, 0x0

    const/16 v18, 0x2f

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 579
    const/16 v17, 0x3a

    const/16 v18, 0x40

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 580
    const/16 v17, 0x5b

    const/16 v18, 0x5e

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 581
    const/16 v17, 0x60

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 582
    const/16 v17, 0x7b

    const v18, 0xffff

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    goto :goto_5

    .line 586
    :sswitch_4
    const/16 v17, 0x0

    const/16 v18, 0x2f

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 587
    const/16 v17, 0x3a

    const v18, 0xffff

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    goto :goto_5

    .line 591
    :sswitch_5
    const/16 v17, 0x9

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 592
    const/16 v17, 0xd

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 593
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 594
    const/16 v17, 0xa

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 595
    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 596
    const/16 v17, 0x20

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    goto/16 :goto_5

    .line 600
    :sswitch_6
    const/16 v17, 0x61

    const/16 v18, 0x7a

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 601
    const/16 v17, 0x41

    const/16 v18, 0x5a

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 602
    const/16 v17, 0x5f

    move/from16 v0, v17

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 607
    :sswitch_7
    const/16 v17, 0x30

    const/16 v18, 0x39

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    goto/16 :goto_5

    .line 627
    .end local v4    # "c":I
    :sswitch_8
    if-eqz v6, :cond_11

    .line 629
    const-string v17, "Bad class range"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 631
    :cond_11
    const/4 v6, 0x1

    .line 634
    if-ne v11, v3, :cond_12

    const/4 v14, 0x0

    .line 637
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x5d

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 639
    const v16, 0xffff

    .line 640
    .restart local v16    # "simpleChar":C
    goto/16 :goto_3

    .end local v16    # "simpleChar":C
    :cond_12
    move v14, v11

    .line 634
    goto :goto_6

    .line 669
    .restart local v16    # "simpleChar":C
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v18, 0x2d

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_15

    .line 671
    :cond_14
    move/from16 v0, v16

    invoke-virtual {v12, v0, v10}, Lorg/apache/regexp/RECompiler$RERange;->include(CZ)V

    .line 673
    :cond_15
    move/from16 v11, v16

    .local v11, "last":C
    goto/16 :goto_1

    .line 536
    nop

    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_8
        0x5c -> :sswitch_1
        0x5e -> :sswitch_0
    .end sparse-switch

    .line 551
    :pswitch_data_0
    .packed-switch 0xffffd
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 568
    :sswitch_data_1
    .sparse-switch
        0x44 -> :sswitch_4
        0x53 -> :sswitch_2
        0x57 -> :sswitch_3
        0x64 -> :sswitch_7
        0x73 -> :sswitch_5
        0x77 -> :sswitch_6
    .end sparse-switch
.end method

.method closure([I)I
    .locals 22
    .param p1, "flags"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 908
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 911
    .local v11, "idxBeforeTerminal":I
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v18, v0

    .line 914
    .local v18, "terminalFlags":[I
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->terminal([I)I

    move-result v17

    .line 917
    .local v17, "ret":I
    const/16 v19, 0x0

    aget v20, p1, v19

    const/16 v21, 0x0

    aget v21, v18, v21

    or-int v20, v20, v21

    aput v20, p1, v19

    .line 920
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_0

    .line 1092
    :goto_0
    return v17

    .line 925
    :cond_0
    const/4 v10, 0x1

    .line 926
    .local v10, "greedy":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 927
    .local v8, "closureType":C
    sparse-switch v8, :sswitch_data_0

    .line 959
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->len:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->charAt(I)C

    move-result v19

    const/16 v20, 0x3f

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 961
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    .line 962
    const/4 v10, 0x0

    .line 965
    :cond_2
    if-eqz v10, :cond_a

    .line 968
    sparse-switch v8, :sswitch_data_1

    goto :goto_0

    .line 1045
    :sswitch_0
    const/16 v19, 0x2a

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1046
    add-int/lit8 v19, v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto :goto_0

    .line 933
    :sswitch_1
    const/16 v19, 0x0

    aget v20, p1, v19

    or-int/lit8 v20, v20, 0x1

    aput v20, p1, v19

    .line 940
    :sswitch_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/regexp/RECompiler;->idx:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->idx:I

    .line 947
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/regexp/RECompiler;->instruction:[C

    move-object/from16 v19, v0

    aget-char v13, v19, v17

    .line 948
    .local v13, "opcode":I
    const/16 v19, 0x5e

    move/from16 v0, v19

    if-eq v13, v0, :cond_3

    const/16 v19, 0x24

    move/from16 v0, v19

    if-ne v13, v0, :cond_4

    .line 950
    :cond_3
    const-string v19, "Bad closure operand"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 952
    :cond_4
    const/16 v19, 0x0

    aget v19, v18, v19

    and-int/lit8 v19, v19, 0x1

    if-eqz v19, :cond_1

    .line 954
    const-string v19, "Closure operand can\'t be nullable"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 972
    .end local v13    # "opcode":I
    :sswitch_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/regexp/RECompiler;->bracket()V

    .line 973
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 974
    .local v4, "bracketEnd":I
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/regexp/RECompiler;->bracketMin:I

    .line 975
    .local v5, "bracketMin":I
    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/regexp/RECompiler;->bracketOpt:I

    .line 978
    .local v6, "bracketOpt":I
    move/from16 v15, v17

    .line 981
    .local v15, "pos":I
    const/4 v7, 0x0

    .local v7, "c":I
    move/from16 v16, v15

    .end local v15    # "pos":I
    .local v16, "pos":I
    :goto_2
    if-lt v7, v5, :cond_5

    .line 989
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v6, v0, :cond_6

    .line 993
    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 994
    const/16 v19, 0x2a

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 995
    add-int/lit8 v19, v16, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto/16 :goto_0

    .line 984
    :cond_5
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 985
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->terminal([I)I

    move-result v15

    .end local v16    # "pos":I
    .restart local v15    # "pos":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 981
    add-int/lit8 v7, v7, 0x1

    move/from16 v16, v15

    .end local v15    # "pos":I
    .restart local v16    # "pos":I
    goto :goto_2

    .line 998
    :cond_6
    if-lez v6, :cond_9

    .line 1000
    add-int/lit8 v19, v6, 0x1

    move/from16 v0, v19

    new-array v14, v0, [I

    .line 1002
    .local v14, "opt":[I
    const/16 v19, 0x3f

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1003
    const/16 v19, 0x0

    aput v16, v14, v19

    .line 1006
    const/4 v7, 0x1

    :goto_3
    if-lt v7, v6, :cond_7

    .line 1015
    const/16 v19, 0x4e

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v9

    aput v9, v14, v6

    .line 1016
    .local v9, "end":I
    const/4 v7, 0x0

    :goto_4
    if-lt v7, v6, :cond_8

    .line 1030
    .end local v9    # "end":I
    .end local v14    # "opt":[I
    :goto_5
    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/regexp/RECompiler;->idx:I

    goto/16 :goto_0

    .line 1008
    .restart local v14    # "opt":[I
    :cond_7
    const/16 v19, 0x3f

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v19

    aput v19, v14, v7

    .line 1010
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 1011
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/regexp/RECompiler;->terminal([I)I

    .line 1006
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1018
    .restart local v9    # "end":I
    :cond_8
    aget v19, v14, v7

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1019
    aget v19, v14, v7

    add-int/lit8 v19, v19, 0x3

    add-int/lit8 v20, v7, 0x1

    aget v20, v14, v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1016
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1025
    .end local v9    # "end":I
    .end local v14    # "opt":[I
    :cond_9
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    .line 1026
    const/16 v19, 0x4e

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    goto :goto_5

    .line 1036
    .end local v4    # "bracketEnd":I
    .end local v5    # "bracketMin":I
    .end local v6    # "bracketOpt":I
    .end local v7    # "c":I
    .end local v16    # "pos":I
    :sswitch_5
    const/16 v19, 0x3f

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1037
    const/16 v19, 0x4e

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v12

    .line 1038
    .local v12, "n":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v12}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1039
    add-int/lit8 v19, v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto/16 :goto_0

    .line 1052
    .end local v12    # "n":I
    :sswitch_6
    const/16 v19, 0x43

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1053
    const/16 v19, 0x2b

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v12

    .line 1054
    .restart local v12    # "n":I
    add-int/lit8 v19, v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1055
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v12, v1}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto/16 :goto_0

    .line 1063
    .end local v12    # "n":I
    :cond_a
    sparse-switch v8, :sswitch_data_2

    goto/16 :goto_0

    .line 1076
    :sswitch_7
    const/16 v19, 0x38

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1077
    add-int/lit8 v19, v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto/16 :goto_0

    .line 1067
    :sswitch_8
    const/16 v19, 0x2f

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1068
    const/16 v19, 0x4e

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v12

    .line 1069
    .restart local v12    # "n":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v12}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1070
    add-int/lit8 v19, v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto/16 :goto_0

    .line 1083
    .end local v12    # "n":I
    :sswitch_9
    const/16 v19, 0x43

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1084
    const/16 v19, 0x3d

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v12

    .line 1085
    .restart local v12    # "n":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v12, v1}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1086
    add-int/lit8 v19, v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto/16 :goto_0

    .line 927
    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_1
        0x2b -> :sswitch_2
        0x3f -> :sswitch_1
        0x7b -> :sswitch_3
    .end sparse-switch

    .line 968
    :sswitch_data_1
    .sparse-switch
        0x2a -> :sswitch_0
        0x2b -> :sswitch_6
        0x3f -> :sswitch_5
        0x7b -> :sswitch_4
    .end sparse-switch

    .line 1063
    :sswitch_data_2
    .sparse-switch
        0x2a -> :sswitch_7
        0x2b -> :sswitch_9
        0x3f -> :sswitch_8
    .end sparse-switch
.end method

.method public compile(Ljava/lang/String;)Lorg/apache/regexp/REProgram;
    .locals 5
    .param p1, "pattern"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1267
    iput-object p1, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    .line 1268
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    iput v2, p0, Lorg/apache/regexp/RECompiler;->len:I

    .line 1269
    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 1270
    iput v4, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    .line 1271
    iput v3, p0, Lorg/apache/regexp/RECompiler;->parens:I

    .line 1274
    new-array v0, v3, [I

    const/4 v2, 0x2

    aput v2, v0, v4

    .line 1277
    .local v0, "flags":[I
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler;->expr([I)I

    .line 1280
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v3, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-eq v2, v3, :cond_1

    .line 1282
    iget v2, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_0

    .line 1284
    const-string v2, "Unmatched close paren"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 1286
    :cond_0
    const-string v2, "Unexpected input remains"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 1290
    :cond_1
    iget v2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    new-array v1, v2, [C

    .line 1291
    .local v1, "ins":[C
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget v3, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1292
    new-instance v2, Lorg/apache/regexp/REProgram;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->parens:I

    invoke-direct {v2, v3, v1}, Lorg/apache/regexp/REProgram;-><init>(I[C)V

    return-object v2
.end method

.method emit(C)V
    .locals 3
    .param p1, "c"    # C

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler;->ensure(I)V

    .line 130
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget v1, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    aput-char p1, v0, v1

    .line 131
    return-void
.end method

.method ensure(I)V
    .locals 5
    .param p1, "n"    # I

    .prologue
    const/4 v4, 0x0

    .line 102
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    array-length v0, v2

    .line 105
    .local v0, "curlen":I
    iget v2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/2addr v2, p1

    if-lt v2, v0, :cond_0

    .line 108
    :goto_0
    iget v2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/2addr v2, p1

    if-ge v2, v0, :cond_1

    .line 114
    new-array v1, v0, [C

    .line 115
    .local v1, "newInstruction":[C
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget v3, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    iput-object v1, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    .line 118
    .end local v1    # "newInstruction":[C
    :cond_0
    return-void

    .line 110
    :cond_1
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method escape()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x30

    .line 348
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x5c

    if-eq v5, v6, :cond_0

    .line 350
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->internalError()V

    .line 354
    :cond_0
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v5, v5, 0x1

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ne v5, v6, :cond_1

    .line 356
    const-string v5, "Escape terminates string"

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 360
    :cond_1
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v5, v5, 0x2

    iput v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 361
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 362
    .local v1, "escapeChar":C
    sparse-switch v1, :sswitch_data_0

    move v4, v1

    .line 460
    :cond_2
    :goto_0
    return v4

    .line 366
    :sswitch_0
    const v4, 0xffffe

    goto :goto_0

    .line 374
    :sswitch_1
    const v4, 0xffffd

    goto :goto_0

    .line 380
    :sswitch_2
    const/16 v5, 0x75

    if-ne v1, v5, :cond_3

    const/4 v2, 0x4

    .line 383
    .local v2, "hexDigits":I
    :goto_1
    const/4 v4, 0x0

    .line 384
    .local v4, "val":I
    :goto_2
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_2

    add-int/lit8 v3, v2, -0x1

    .end local v2    # "hexDigits":I
    .local v3, "hexDigits":I
    if-gtz v2, :cond_4

    move v2, v3

    .end local v3    # "hexDigits":I
    .restart local v2    # "hexDigits":I
    goto :goto_0

    .line 380
    .end local v2    # "hexDigits":I
    .end local v4    # "val":I
    :cond_3
    const/4 v2, 0x2

    goto :goto_1

    .line 387
    .restart local v3    # "hexDigits":I
    .restart local v4    # "val":I
    :cond_4
    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 390
    .local v0, "c":C
    if-lt v0, v7, :cond_5

    const/16 v5, 0x39

    if-gt v0, v5, :cond_5

    .line 393
    shl-int/lit8 v5, v4, 0x4

    add-int/2addr v5, v0

    add-int/lit8 v4, v5, -0x30

    .line 384
    :goto_3
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    move v2, v3

    .end local v3    # "hexDigits":I
    .restart local v2    # "hexDigits":I
    goto :goto_2

    .line 398
    .end local v2    # "hexDigits":I
    .restart local v3    # "hexDigits":I
    :cond_5
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    .line 399
    const/16 v5, 0x61

    if-lt v0, v5, :cond_6

    const/16 v5, 0x66

    if-gt v0, v5, :cond_6

    .line 402
    shl-int/lit8 v5, v4, 0x4

    add-int/lit8 v6, v0, -0x61

    add-int/2addr v5, v6

    add-int/lit8 v4, v5, 0xa

    .line 403
    goto :goto_3

    .line 408
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Expected "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " hexadecimal digits after \\"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto :goto_3

    .line 416
    .end local v0    # "c":C
    .end local v3    # "hexDigits":I
    .end local v4    # "val":I
    :sswitch_3
    const/16 v4, 0x9

    goto :goto_0

    .line 419
    :sswitch_4
    const/16 v4, 0xa

    goto :goto_0

    .line 422
    :sswitch_5
    const/16 v4, 0xd

    goto :goto_0

    .line 425
    :sswitch_6
    const/16 v4, 0xc

    goto :goto_0

    .line 439
    :sswitch_7
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_7

    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-nez v5, :cond_8

    :cond_7
    if-ne v1, v7, :cond_9

    .line 442
    :cond_8
    add-int/lit8 v4, v1, -0x30

    .line 443
    .restart local v4    # "val":I
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_2

    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 445
    shl-int/lit8 v5, v4, 0x3

    iget-object v6, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v7, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    add-int/lit8 v6, v6, -0x30

    add-int v4, v5, v6

    .line 446
    iget v5, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v6, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v5, v6, :cond_2

    iget-object v5, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v6, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 448
    shl-int/lit8 v5, v4, 0x3

    iget-object v6, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v7, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    add-int/lit8 v6, v6, -0x30

    add-int v4, v5, v6

    goto/16 :goto_0

    .line 455
    .end local v4    # "val":I
    :cond_9
    const v4, 0xfffff

    goto/16 :goto_0

    .line 362
    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_7
        0x31 -> :sswitch_7
        0x32 -> :sswitch_7
        0x33 -> :sswitch_7
        0x34 -> :sswitch_7
        0x35 -> :sswitch_7
        0x36 -> :sswitch_7
        0x37 -> :sswitch_7
        0x38 -> :sswitch_7
        0x39 -> :sswitch_7
        0x42 -> :sswitch_0
        0x44 -> :sswitch_1
        0x53 -> :sswitch_1
        0x57 -> :sswitch_1
        0x62 -> :sswitch_0
        0x64 -> :sswitch_1
        0x66 -> :sswitch_6
        0x6e -> :sswitch_4
        0x72 -> :sswitch_5
        0x73 -> :sswitch_1
        0x74 -> :sswitch_3
        0x75 -> :sswitch_2
        0x77 -> :sswitch_1
        0x78 -> :sswitch_2
    .end sparse-switch
.end method

.method expr([I)I
    .locals 12
    .param p1, "flags"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 1159
    const/4 v7, -0x1

    .line 1160
    .local v7, "paren":I
    const/4 v8, -0x1

    .line 1161
    .local v8, "ret":I
    iget v2, p0, Lorg/apache/regexp/RECompiler;->parens:I

    .line 1162
    .local v2, "closeParens":I
    const/4 v9, 0x0

    aget v9, p1, v9

    and-int/lit8 v9, v9, 0x2

    if-nez v9, :cond_0

    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v10, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x28

    if-ne v9, v10, :cond_0

    .line 1165
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v9, v9, 0x2

    iget v10, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v9, v10, :cond_3

    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v10, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3f

    if-ne v9, v10, :cond_3

    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v10, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v10, v10, 0x2

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3a

    if-ne v9, v10, :cond_3

    .line 1167
    const/4 v7, 0x2

    .line 1168
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v9, v9, 0x3

    iput v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 1169
    const/16 v9, 0x3c

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v8

    .line 1178
    :cond_0
    :goto_0
    const/4 v9, 0x0

    aget v10, p1, v9

    and-int/lit8 v10, v10, -0x3

    aput v10, p1, v9

    .line 1181
    const/4 v6, 0x0

    .line 1182
    .local v6, "open":Z
    invoke-virtual {p0, p1}, Lorg/apache/regexp/RECompiler;->branch([I)I

    move-result v0

    .line 1183
    .local v0, "branch":I
    const/4 v9, -0x1

    if-ne v8, v9, :cond_4

    .line 1185
    move v8, v0

    .line 1193
    :goto_1
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v10, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v9, v10, :cond_1

    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v10, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x7c

    if-eq v9, v10, :cond_5

    .line 1208
    :cond_1
    if-lez v7, :cond_9

    .line 1210
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    iget v10, p0, Lorg/apache/regexp/RECompiler;->len:I

    if-ge v9, v10, :cond_7

    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v10, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x29

    if-ne v9, v10, :cond_7

    .line 1212
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 1218
    :goto_2
    const/4 v9, 0x1

    if-ne v7, v9, :cond_8

    .line 1220
    const/16 v9, 0x29

    invoke-virtual {p0, v9, v2}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v4

    .line 1233
    .local v4, "end":I
    :goto_3
    invoke-virtual {p0, v8, v4}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1236
    move v3, v8

    .line 1237
    .local v3, "currentNode":I
    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v10, v3, 0x2

    aget-char v5, v9, v10

    .line 1239
    .local v5, "nextNodeOffset":I
    :goto_4
    if-eqz v5, :cond_2

    iget v9, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    if-lt v3, v9, :cond_a

    .line 1251
    :cond_2
    return v8

    .line 1173
    .end local v0    # "branch":I
    .end local v3    # "currentNode":I
    .end local v4    # "end":I
    .end local v5    # "nextNodeOffset":I
    .end local v6    # "open":Z
    :cond_3
    const/4 v7, 0x1

    .line 1174
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 1175
    const/16 v9, 0x28

    iget v10, p0, Lorg/apache/regexp/RECompiler;->parens:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lorg/apache/regexp/RECompiler;->parens:I

    invoke-virtual {p0, v9, v10}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v8

    goto :goto_0

    .line 1189
    .restart local v0    # "branch":I
    .restart local v6    # "open":Z
    :cond_4
    invoke-virtual {p0, v8, v0}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    goto :goto_1

    .line 1196
    :cond_5
    if-nez v6, :cond_6

    .line 1197
    const/16 v9, 0x7c

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10, v0}, Lorg/apache/regexp/RECompiler;->nodeInsert(CII)V

    .line 1198
    const/4 v6, 0x1

    .line 1201
    :cond_6
    iget v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 1202
    const/16 v9, 0x7c

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v1

    .end local v0    # "branch":I
    .local v1, "branch":I
    invoke-virtual {p0, v0, v1}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1203
    invoke-virtual {p0, p1}, Lorg/apache/regexp/RECompiler;->branch([I)I

    move v0, v1

    .end local v1    # "branch":I
    .restart local v0    # "branch":I
    goto :goto_1

    .line 1216
    :cond_7
    const-string v9, "Missing close paren"

    invoke-virtual {p0, v9}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    goto :goto_2

    .line 1224
    :cond_8
    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v4

    .line 1226
    .restart local v4    # "end":I
    goto :goto_3

    .line 1229
    .end local v4    # "end":I
    :cond_9
    const/16 v9, 0x45

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v4

    .restart local v4    # "end":I
    goto :goto_3

    .line 1242
    .restart local v3    # "currentNode":I
    .restart local v5    # "nextNodeOffset":I
    :cond_a
    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    aget-char v9, v9, v3

    const/16 v10, 0x7c

    if-ne v9, v10, :cond_b

    .line 1244
    add-int/lit8 v9, v3, 0x3

    invoke-virtual {p0, v9, v4}, Lorg/apache/regexp/RECompiler;->setNextOfEnd(II)V

    .line 1246
    :cond_b
    iget-object v9, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v10, v3, 0x2

    aget-char v5, v9, v10

    .line 1247
    add-int/2addr v3, v5

    goto :goto_4
.end method

.method internalError()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 223
    new-instance v0, Ljava/lang/Error;

    const-string v1, "Internal error!"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method node(CI)I
    .locals 3
    .param p1, "opcode"    # C
    .param p2, "opdata"    # I

    .prologue
    .line 204
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler;->ensure(I)V

    .line 207
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget v1, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    aput-char p1, v0, v1

    .line 208
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget v1, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/lit8 v1, v1, 0x1

    int-to-char v2, p2

    aput-char v2, v0, v1

    .line 209
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget v1, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    aput-char v2, v0, v1

    .line 210
    iget v0, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    .line 213
    iget v0, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/lit8 v0, v0, -0x3

    return v0
.end method

.method nodeInsert(CII)V
    .locals 4
    .param p1, "opcode"    # C
    .param p2, "opdata"    # I
    .param p3, "insertAt"    # I

    .prologue
    .line 143
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler;->ensure(I)V

    .line 146
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    iget-object v1, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v2, p3, 0x3

    iget v3, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    sub-int/2addr v3, p3

    invoke-static {v0, p3, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    aput-char p1, v0, p3

    .line 148
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v1, p3, 0x1

    int-to-char v2, p2

    aput-char v2, v0, v1

    .line 149
    iget-object v0, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v1, p3, 0x2

    const/4 v2, 0x0

    aput-char v2, v0, v1

    .line 150
    iget v0, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    .line 151
    return-void
.end method

.method setNextOfEnd(II)V
    .locals 5
    .param p1, "node"    # I
    .param p2, "pointTo"    # I

    .prologue
    .line 161
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v3, p1, 0x2

    aget-char v0, v2, v3

    .line 164
    .local v0, "next":I
    :goto_0
    if-eqz v0, :cond_0

    iget v2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    if-lt p1, v2, :cond_1

    .line 181
    :cond_0
    iget v2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    if-ge p1, v2, :cond_4

    .line 185
    sub-int v1, p2, p1

    .line 186
    .local v1, "offset":I
    int-to-short v2, v1

    if-eq v1, v2, :cond_3

    .line 187
    new-instance v2, Lorg/apache/regexp/RESyntaxException;

    const-string v3, "Exceeded short jump range."

    invoke-direct {v2, v3}, Lorg/apache/regexp/RESyntaxException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 172
    .end local v1    # "offset":I
    :cond_1
    if-ne p1, p2, :cond_2

    .line 173
    iget p2, p0, Lorg/apache/regexp/RECompiler;->lenInstruction:I

    .line 175
    :cond_2
    add-int/2addr p1, v0

    .line 176
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v3, p1, 0x2

    aget-char v0, v2, v3

    goto :goto_0

    .line 191
    .restart local v1    # "offset":I
    :cond_3
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->instruction:[C

    add-int/lit8 v3, p1, 0x2

    int-to-short v4, v1

    int-to-char v4, v4

    aput-char v4, v2, v3

    .line 193
    .end local v1    # "offset":I
    :cond_4
    return-void
.end method

.method syntaxError(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 232
    new-instance v0, Lorg/apache/regexp/RESyntaxException;

    invoke-direct {v0, p1}, Lorg/apache/regexp/RESyntaxException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method terminal([I)I
    .locals 6
    .param p1, "flags"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 827
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 895
    :goto_0
    aget v2, p1, v5

    and-int/lit8 v2, v2, -0x2

    aput v2, p1, v5

    .line 896
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->atom()I

    move-result v2

    :goto_1
    return v2

    .line 832
    :sswitch_0
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2, v5}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v2

    goto :goto_1

    .line 835
    :sswitch_1
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->characterClass()I

    move-result v2

    goto :goto_1

    .line 838
    :sswitch_2
    invoke-virtual {p0, p1}, Lorg/apache/regexp/RECompiler;->expr([I)I

    move-result v2

    goto :goto_1

    .line 841
    :sswitch_3
    const-string v2, "Unexpected close paren"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 844
    :sswitch_4
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->internalError()V

    .line 847
    :sswitch_5
    const-string v2, "Mismatched class"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 850
    :sswitch_6
    const-string v2, "Unexpected end of input"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 856
    :sswitch_7
    const-string v2, "Missing operand to closure"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 861
    :sswitch_8
    iget v1, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 864
    .local v1, "idxBeforeEscape":I
    invoke-virtual {p0}, Lorg/apache/regexp/RECompiler;->escape()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 886
    iput v1, p0, Lorg/apache/regexp/RECompiler;->idx:I

    .line 887
    aget v2, p1, v5

    and-int/lit8 v2, v2, -0x2

    aput v2, p1, v5

    goto :goto_0

    .line 868
    :pswitch_0
    aget v2, p1, v5

    and-int/lit8 v2, v2, -0x2

    aput v2, p1, v5

    .line 869
    const/16 v2, 0x5c

    iget-object v3, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v4, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p0, v2, v3}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v2

    goto :goto_1

    .line 873
    :pswitch_1
    iget-object v2, p0, Lorg/apache/regexp/RECompiler;->pattern:Ljava/lang/String;

    iget v3, p0, Lorg/apache/regexp/RECompiler;->idx:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    add-int/lit8 v2, v2, -0x30

    int-to-char v0, v2

    .line 874
    .local v0, "backreference":C
    iget v2, p0, Lorg/apache/regexp/RECompiler;->parens:I

    if-gt v2, v0, :cond_0

    .line 876
    const-string v2, "Bad backreference"

    invoke-virtual {p0, v2}, Lorg/apache/regexp/RECompiler;->syntaxError(Ljava/lang/String;)V

    .line 878
    :cond_0
    aget v2, p1, v5

    or-int/lit8 v2, v2, 0x1

    aput v2, p1, v5

    .line 879
    const/16 v2, 0x23

    invoke-virtual {p0, v2, v0}, Lorg/apache/regexp/RECompiler;->node(CI)I

    move-result v2

    goto :goto_1

    .line 827
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0x24 -> :sswitch_0
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_7
        0x2b -> :sswitch_7
        0x2e -> :sswitch_0
        0x3f -> :sswitch_7
        0x5b -> :sswitch_1
        0x5c -> :sswitch_8
        0x5d -> :sswitch_5
        0x5e -> :sswitch_0
        0x7b -> :sswitch_7
        0x7c -> :sswitch_4
    .end sparse-switch

    .line 864
    :pswitch_data_0
    .packed-switch 0xffffd
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
