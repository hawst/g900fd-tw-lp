.class Lorg/apache/regexp/RECompiler$RERange;
.super Ljava/lang/Object;
.source "RECompiler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/regexp/RECompiler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RERange"
.end annotation


# instance fields
.field maxRange:[I

.field minRange:[I

.field num:I

.field size:I

.field final synthetic this$0:Lorg/apache/regexp/RECompiler;


# direct methods
.method constructor <init>(Lorg/apache/regexp/RECompiler;)V
    .locals 1

    .prologue
    .line 1298
    iput-object p1, p0, Lorg/apache/regexp/RECompiler$RERange;->this$0:Lorg/apache/regexp/RECompiler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    .line 1301
    iget v0, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    .line 1302
    iget v0, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    .line 1303
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    return-void
.end method


# virtual methods
.method delete(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1312
    iget v0, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    if-lt p1, v0, :cond_2

    .line 1329
    :cond_0
    :goto_0
    return-void

    .line 1320
    :cond_1
    add-int/lit8 v0, p1, -0x1

    if-ltz v0, :cond_2

    .line 1322
    iget-object v0, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    add-int/lit8 v1, p1, -0x1

    iget-object v2, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v2, v2, p1

    aput v2, v0, v1

    .line 1323
    iget-object v0, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    add-int/lit8 v1, p1, -0x1

    iget-object v2, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v2, v2, p1

    aput v2, v0, v1

    .line 1318
    :cond_2
    add-int/lit8 p1, p1, 0x1

    iget v0, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    if-lt p1, v0, :cond_1

    .line 1328
    iget v0, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    goto :goto_0
.end method

.method include(CZ)V
    .locals 0
    .param p1, "minmax"    # C
    .param p2, "include"    # Z

    .prologue
    .line 1465
    invoke-virtual {p0, p1, p1, p2}, Lorg/apache/regexp/RECompiler$RERange;->include(IIZ)V

    .line 1466
    return-void
.end method

.method include(IIZ)V
    .locals 0
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "include"    # Z

    .prologue
    .line 1448
    if-eqz p3, :cond_0

    .line 1450
    invoke-virtual {p0, p1, p2}, Lorg/apache/regexp/RECompiler$RERange;->merge(II)V

    .line 1456
    :goto_0
    return-void

    .line 1454
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/regexp/RECompiler$RERange;->remove(II)V

    goto :goto_0
.end method

.method merge(II)V
    .locals 6
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v5, 0x0

    .line 1339
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    if-lt v0, v3, :cond_2

    .line 1375
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    iget v4, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    if-lt v3, v4, :cond_0

    .line 1377
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    mul-int/lit8 v3, v3, 0x2

    iput v3, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    .line 1378
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    new-array v2, v3, [I

    .line 1379
    .local v2, "newMin":[I
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->size:I

    new-array v1, v3, [I

    .line 1380
    .local v1, "newMax":[I
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    iget v4, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1381
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    iget v4, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1382
    iput-object v2, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    .line 1383
    iput-object v1, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    .line 1385
    .end local v1    # "newMax":[I
    .end local v2    # "newMin":[I
    :cond_0
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    iget v4, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    aput p1, v3, v4

    .line 1386
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    iget v4, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    aput p2, v3, v4

    .line 1387
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    .line 1388
    :cond_1
    :goto_1
    return-void

    .line 1342
    :cond_2
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-lt p1, v3, :cond_3

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-le p2, v3, :cond_1

    .line 1348
    :cond_3
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-gt p1, v3, :cond_4

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-lt p2, v3, :cond_4

    .line 1350
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler$RERange;->delete(I)V

    .line 1351
    invoke-virtual {p0, p1, p2}, Lorg/apache/regexp/RECompiler$RERange;->merge(II)V

    goto :goto_1

    .line 1356
    :cond_4
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-lt p1, v3, :cond_5

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-gt p1, v3, :cond_5

    .line 1358
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget p1, v3, v0

    .line 1359
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler$RERange;->delete(I)V

    .line 1360
    invoke-virtual {p0, p1, p2}, Lorg/apache/regexp/RECompiler$RERange;->merge(II)V

    goto :goto_1

    .line 1365
    :cond_5
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-lt p2, v3, :cond_6

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-gt p2, v3, :cond_6

    .line 1367
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget p2, v3, v0

    .line 1368
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler$RERange;->delete(I)V

    .line 1369
    invoke-virtual {p0, p1, p2}, Lorg/apache/regexp/RECompiler$RERange;->merge(II)V

    goto :goto_1

    .line 1339
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method remove(II)V
    .locals 5
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 1398
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/regexp/RECompiler$RERange;->num:I

    if-lt v0, v3, :cond_1

    .line 1438
    :cond_0
    :goto_1
    return-void

    .line 1401
    :cond_1
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-lt v3, p1, :cond_2

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-gt v3, p2, :cond_2

    .line 1403
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler$RERange;->delete(I)V

    goto :goto_1

    .line 1408
    :cond_2
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-lt p1, v3, :cond_4

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-gt p2, v3, :cond_4

    .line 1410
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v2, v3, v0

    .line 1411
    .local v2, "minr":I
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v1, v3, v0

    .line 1412
    .local v1, "maxr":I
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RECompiler$RERange;->delete(I)V

    .line 1413
    if-ge v2, p1, :cond_3

    .line 1415
    add-int/lit8 v3, p1, -0x1

    invoke-virtual {p0, v2, v3}, Lorg/apache/regexp/RECompiler$RERange;->merge(II)V

    .line 1417
    :cond_3
    if-ge p2, v1, :cond_0

    .line 1419
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {p0, v3, v1}, Lorg/apache/regexp/RECompiler$RERange;->merge(II)V

    goto :goto_1

    .line 1425
    .end local v1    # "maxr":I
    .end local v2    # "minr":I
    :cond_4
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-lt v3, p1, :cond_5

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    aget v3, v3, v0

    if-gt v3, p2, :cond_5

    .line 1427
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->minRange:[I

    add-int/lit8 v4, p2, 0x1

    aput v4, v3, v0

    goto :goto_1

    .line 1432
    :cond_5
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-lt v3, p1, :cond_6

    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    aget v3, v3, v0

    if-gt v3, p2, :cond_6

    .line 1434
    iget-object v3, p0, Lorg/apache/regexp/RECompiler$RERange;->maxRange:[I

    add-int/lit8 v4, p1, -0x1

    aput v4, v3, v0

    goto :goto_1

    .line 1398
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
