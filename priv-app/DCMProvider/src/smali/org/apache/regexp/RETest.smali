.class public Lorg/apache/regexp/RETest;
.super Ljava/lang/Object;
.source "RETest.java"


# static fields
.field static final NEW_LINE:Ljava/lang/String;

.field static final showSuccesses:Z


# instance fields
.field final compiler:Lorg/apache/regexp/REDebugCompiler;

.field failures:I

.field testCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lorg/apache/regexp/REDebugCompiler;

    invoke-direct {v0}, Lorg/apache/regexp/REDebugCompiler;-><init>()V

    iput-object v0, p0, Lorg/apache/regexp/RETest;->compiler:Lorg/apache/regexp/REDebugCompiler;

    .line 235
    iput v1, p0, Lorg/apache/regexp/RETest;->testCount:I

    .line 240
    iput v1, p0, Lorg/apache/regexp/RETest;->failures:I

    .line 106
    return-void
.end method

.method private findNextTest(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 3
    .param p1, "br"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 610
    const-string v0, ""

    .line 612
    .local v0, "number":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->ready()Z

    move-result v1

    if-nez v1, :cond_2

    .line 634
    :cond_1
    return-object v0

    .line 614
    :cond_2
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 615
    if-eqz v0, :cond_1

    .line 619
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 620
    const-string v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 624
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 628
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 630
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Script error.  Line = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 631
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    goto :goto_0
.end method

.method private getExpectedResult(Ljava/lang/String;)Z
    .locals 2
    .param p1, "yesno"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 586
    const-string v1, "NO"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 598
    :goto_0
    return v0

    .line 590
    :cond_0
    const-string v1, "YES"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 592
    const/4 v0, 0x1

    goto :goto_0

    .line 597
    :cond_1
    const-string v1, "Test script error!"

    invoke-virtual {p0, v1}, Lorg/apache/regexp/RETest;->die(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getNextTestCase(Ljava/io/BufferedReader;)Lorg/apache/regexp/RETestCase;
    .locals 10
    .param p1, "br"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 646
    invoke-direct {p0, p1}, Lorg/apache/regexp/RETest;->findNextTest(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v2

    .line 649
    .local v2, "tag":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/BufferedReader;->ready()Z

    move-result v0

    if-nez v0, :cond_0

    .line 651
    const/4 v0, 0x0

    .line 675
    :goto_0
    return-object v0

    .line 655
    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 658
    .local v3, "expr":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 659
    .local v4, "matchAgainst":Ljava/lang/String;
    const-string v0, "ERR"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 660
    .local v5, "badPattern":Z
    const/4 v6, 0x0

    .line 662
    .local v6, "shouldMatch":Z
    const/4 v7, 0x0

    .line 664
    .local v7, "expectedParens":[Ljava/lang/String;
    if-nez v5, :cond_1

    .line 665
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/regexp/RETest;->getExpectedResult(Ljava/lang/String;)Z

    move-result v6

    .line 666
    if-eqz v6, :cond_1

    .line 667
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 668
    .local v8, "expectedParenCount":I
    new-array v7, v8, [Ljava/lang/String;

    .line 669
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-lt v9, v8, :cond_2

    .line 675
    .end local v8    # "expectedParenCount":I
    .end local v9    # "i":I
    :cond_1
    new-instance v0, Lorg/apache/regexp/RETestCase;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/regexp/RETestCase;-><init>(Lorg/apache/regexp/RETest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;)V

    goto :goto_0

    .line 670
    .restart local v8    # "expectedParenCount":I
    .restart local v9    # "i":I
    :cond_2
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v9

    .line 669
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 62
    :try_start_0
    invoke-static {p0}, Lorg/apache/regexp/RETest;->test([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 69
    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    goto :goto_0
.end method

.method public static test([Ljava/lang/String;)Z
    .locals 5
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 80
    new-instance v0, Lorg/apache/regexp/RETest;

    invoke-direct {v0}, Lorg/apache/regexp/RETest;-><init>()V

    .line 82
    .local v0, "test":Lorg/apache/regexp/RETest;
    array-length v3, p0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 84
    aget-object v3, p0, v1

    invoke-virtual {v0, v3}, Lorg/apache/regexp/RETest;->runInteractiveTests(Ljava/lang/String;)V

    .line 98
    :goto_0
    iget v3, v0, Lorg/apache/regexp/RETest;->failures:I

    if-nez v3, :cond_2

    :goto_1
    return v1

    .line 86
    :cond_0
    array-length v3, p0

    if-ne v3, v1, :cond_1

    .line 89
    aget-object v3, p0, v2

    invoke-virtual {v0, v3}, Lorg/apache/regexp/RETest;->runAutomatedTests(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_1
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Usage: RETest ([-i] [regex]) ([/path/to/testfile.txt])"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 94
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "By Default will run automated tests from file \'docs/RETest.txt\' ..."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 95
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3}, Ljava/io/PrintStream;->println()V

    .line 96
    const-string v3, "docs/RETest.txt"

    invoke-virtual {v0, v3}, Lorg/apache/regexp/RETest;->runAutomatedTests(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 98
    goto :goto_1
.end method

.method private testPrecompiledRE()V
    .locals 11

    .prologue
    const/16 v10, 0xd

    const/4 v9, 0x4

    const/4 v8, 0x1

    const/16 v7, 0x7c

    const/4 v6, 0x3

    .line 440
    const/16 v4, 0x1d

    new-array v2, v4, [C

    const/4 v4, 0x0

    .line 441
    aput-char v7, v2, v4

    const/4 v4, 0x2

    const/16 v5, 0x1a

    aput-char v5, v2, v4

    aput-char v7, v2, v6

    const/4 v4, 0x5

    aput-char v10, v2, v4

    const/4 v4, 0x6

    const/16 v5, 0x41

    aput-char v5, v2, v4

    const/4 v4, 0x7

    .line 442
    aput-char v8, v2, v4

    const/16 v4, 0x8

    aput-char v9, v2, v4

    const/16 v4, 0x9

    const/16 v5, 0x61

    aput-char v5, v2, v4

    const/16 v4, 0xa

    aput-char v7, v2, v4

    const/16 v4, 0xc

    aput-char v6, v2, v4

    const/16 v4, 0x47

    aput-char v4, v2, v10

    const/16 v4, 0xf

    .line 443
    const v5, 0xfff6

    aput-char v5, v2, v4

    const/16 v4, 0x10

    aput-char v7, v2, v4

    const/16 v4, 0x12

    aput-char v6, v2, v4

    const/16 v4, 0x13

    const/16 v5, 0x4e

    aput-char v5, v2, v4

    const/16 v4, 0x15

    .line 444
    aput-char v6, v2, v4

    const/16 v4, 0x16

    const/16 v5, 0x41

    aput-char v5, v2, v4

    const/16 v4, 0x17

    aput-char v8, v2, v4

    const/16 v4, 0x18

    aput-char v9, v2, v4

    const/16 v4, 0x19

    const/16 v5, 0x62

    aput-char v5, v2, v4

    const/16 v4, 0x1a

    const/16 v5, 0x45

    aput-char v5, v2, v4

    .line 448
    .local v2, "re1Instructions":[C
    new-instance v1, Lorg/apache/regexp/REProgram;

    invoke-direct {v1, v2}, Lorg/apache/regexp/REProgram;-><init>([C)V

    .line 451
    .local v1, "re1":Lorg/apache/regexp/REProgram;
    new-instance v0, Lorg/apache/regexp/RE;

    invoke-direct {v0, v1}, Lorg/apache/regexp/RE;-><init>(Lorg/apache/regexp/REProgram;)V

    .line 452
    .local v0, "r":Lorg/apache/regexp/RE;
    const-string v4, "a*b"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 453
    const-string v4, "aaab"

    invoke-virtual {v0, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v3

    .line 454
    .local v3, "result":Z
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "aaab = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    .line 456
    if-nez v3, :cond_0

    .line 457
    const-string v4, "\"aaab\" doesn\'t match to precompiled \"a*b\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 460
    :cond_0
    const-string v4, "b"

    invoke-virtual {v0, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v3

    .line 461
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "b = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 462
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    .line 463
    if-nez v3, :cond_1

    .line 464
    const-string v4, "\"b\" doesn\'t match to precompiled \"a*b\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 467
    :cond_1
    const-string v4, "c"

    invoke-virtual {v0, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v3

    .line 468
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "c = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 469
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    .line 470
    if-eqz v3, :cond_2

    .line 471
    const-string v4, "\"c\" matches to precompiled \"a*b\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 474
    :cond_2
    const-string v4, "ccccaaaaab"

    invoke-virtual {v0, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v3

    .line 475
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ccccaaaaab = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 476
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    .line 477
    if-nez v3, :cond_3

    .line 478
    const-string v4, "\"ccccaaaaab\" doesn\'t match to precompiled \"a*b\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 480
    :cond_3
    return-void
.end method

.method private testSplitAndGrep()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 484
    const/4 v4, 0x4

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v4, "xxxx"

    aput-object v4, v0, v7

    const-string/jumbo v4, "xxxx"

    aput-object v4, v0, v8

    const-string/jumbo v4, "yyyy"

    aput-object v4, v0, v9

    const/4 v4, 0x3

    const-string/jumbo v5, "zzz"

    aput-object v5, v0, v4

    .line 485
    .local v0, "expected":[Ljava/lang/String;
    new-instance v2, Lorg/apache/regexp/RE;

    const-string v4, "a*b"

    invoke-direct {v2, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 486
    .local v2, "r":Lorg/apache/regexp/RE;
    const-string/jumbo v4, "xxxxaabxxxxbyyyyaaabzzz"

    invoke-virtual {v2, v4}, Lorg/apache/regexp/RE;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 487
    .local v3, "s":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    array-length v4, v3

    if-lt v1, v4, :cond_1

    .line 490
    :cond_0
    const-string v4, "Wrong number of splitted parts"

    array-length v5, v0

    .line 491
    array-length v6, v3

    .line 490
    invoke-virtual {p0, v4, v5, v6}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;II)V

    .line 493
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string/jumbo v4, "x+"

    invoke-direct {v2, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 494
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    new-array v0, v9, [Ljava/lang/String;

    .end local v0    # "expected":[Ljava/lang/String;
    const-string/jumbo v4, "xxxx"

    aput-object v4, v0, v7

    const-string/jumbo v4, "xxxx"

    aput-object v4, v0, v8

    .line 495
    .restart local v0    # "expected":[Ljava/lang/String;
    invoke-virtual {v2, v3}, Lorg/apache/regexp/RE;->grep([Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v3

    .line 496
    const/4 v1, 0x0

    :goto_1
    array-length v4, v3

    if-lt v1, v4, :cond_2

    .line 501
    const-string v4, "Wrong number of string found by grep"

    array-length v5, v0

    .line 502
    array-length v6, v3

    .line 501
    invoke-virtual {p0, v4, v5, v6}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;II)V

    .line 503
    return-void

    .line 488
    :cond_1
    const-string v4, "Wrong splitted part"

    aget-object v5, v0, v1

    aget-object v6, v3, v1

    invoke-virtual {p0, v4, v5, v6}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 498
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "s["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 499
    const-string v4, "Grep fails"

    aget-object v5, v0, v1

    aget-object v6, v3, v1

    invoke-virtual {p0, v4, v5, v6}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private testSubst()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 507
    new-instance v2, Lorg/apache/regexp/RE;

    const-string v3, "a*b"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 508
    .local v2, "r":Lorg/apache/regexp/RE;
    const-string v1, "-foo-garply-wacky-"

    .line 509
    .local v1, "expected":Ljava/lang/String;
    const-string v3, "aaaabfooaaabgarplyaaabwackyb"

    const-string v4, "-"

    invoke-virtual {v2, v3, v4}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "actual":Ljava/lang/String;
    const-string v3, "Wrong result of substitution in \"a*b\""

    invoke-virtual {p0, v3, v1, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "http://[\\.\\w\\-\\?/~_@&=%]+"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 514
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string/jumbo v3, "visit us: http://www.apache.org!"

    .line 515
    const-string v4, "1234<a href=\"$0\">$0</a>"

    .line 514
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 516
    const-string v3, "Wrong subst() result"

    const-string/jumbo v4, "visit us: 1234<a href=\"http://www.apache.org\">http://www.apache.org</a>!"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "(.*?)=(.*)"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 521
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "variable=value"

    .line 522
    const-string v4, "$1_test_$212"

    .line 521
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 523
    const-string v3, "Wrong subst() result"

    const-string v4, "variable_test_value12"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "^a$"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 527
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "a"

    .line 528
    const-string v4, "b"

    .line 527
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 529
    const-string v3, "Wrong subst() result"

    const-string v4, "b"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "^a$"

    invoke-direct {v2, v3, v5}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    .line 533
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "\r\na\r\n"

    .line 534
    const-string v4, "b"

    .line 533
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 535
    const-string v3, "Wrong subst() result"

    const-string v4, "\r\nb\r\n"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "fo(o)"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 539
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "foo"

    .line 540
    const-string v4, "$1"

    .line 539
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 541
    const-string v3, "Wrong subst() result"

    const-string v4, "o"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "^(.*?)(x)?$"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 545
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "abc"

    .line 546
    const-string v4, "$1-$2"

    .line 545
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 547
    const-string v3, "Wrong subst() result"

    const-string v4, "abc-"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "^(.*?)(x)?$"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 550
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "abcx"

    .line 551
    const-string v4, "$1-$2"

    .line 550
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 552
    const-string v3, "Wrong subst() result"

    const-string v4, "abc-x"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    new-instance v2, Lorg/apache/regexp/RE;

    .end local v2    # "r":Lorg/apache/regexp/RE;
    const-string v3, "([a-b]+?)([c-d]+)"

    invoke-direct {v2, v3}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 555
    .restart local v2    # "r":Lorg/apache/regexp/RE;
    const-string/jumbo v3, "zzabcdzz"

    .line 556
    const-string v4, "$1-$2"

    .line 555
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/regexp/RE;->subst(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 557
    const-string v3, "Wrong subst() result"

    const-string/jumbo v4, "zzab-cdzz"

    invoke-virtual {p0, v3, v4, v0}, Lorg/apache/regexp/RETest;->assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    return-void
.end method


# virtual methods
.method public assertEquals(Ljava/lang/String;II)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "expected"    # I
    .param p3, "actual"    # I

    .prologue
    .line 572
    if-eq p2, p3, :cond_0

    .line 573
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " (expected \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 574
    const-string v1, "\", actual \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 573
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 576
    :cond_0
    return-void
.end method

.method public assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "expected"    # Ljava/lang/String;
    .param p3, "actual"    # Ljava/lang/String;

    .prologue
    .line 562
    if-eqz p2, :cond_0

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    :cond_0
    if-eqz p3, :cond_2

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 565
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " (expected \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 566
    const-string v1, "\", actual \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 565
    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 568
    :cond_2
    return-void
.end method

.method die(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FATAL ERROR: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 174
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 175
    return-void
.end method

.method fail(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 196
    iget v0, p0, Lorg/apache/regexp/RETest;->failures:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/regexp/RETest;->failures:I

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 198
    const-string v0, "*******************************************************"

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 199
    const-string v0, "*********************  FAILURE!  **********************"

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 200
    const-string v0, "*******************************************************"

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0, p1}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 203
    const-string v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lorg/apache/regexp/RETest;->compiler:Lorg/apache/regexp/REDebugCompiler;

    invoke-virtual {v0}, Lorg/apache/regexp/REDebugCompiler;->dumpProgram()V

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method fail(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    .locals 2
    .param p1, "log"    # Ljava/lang/StringBuffer;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 185
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0, p2}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method runAutomatedTests(Ljava/lang/String;)V
    .locals 8
    .param p1, "testDocument"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 248
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 251
    .local v2, "ms":J
    invoke-direct {p0}, Lorg/apache/regexp/RETest;->testPrecompiledRE()V

    .line 252
    invoke-direct {p0}, Lorg/apache/regexp/RETest;->testSplitAndGrep()V

    .line 253
    invoke-direct {p0}, Lorg/apache/regexp/RETest;->testSubst()V

    .line 254
    invoke-virtual {p0}, Lorg/apache/regexp/RETest;->testOther()V

    .line 257
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 258
    .local v1, "testInput":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 259
    new-instance v5, Ljava/lang/Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Could not find: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 262
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 266
    .local v0, "br":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->ready()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_3

    .line 276
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 280
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Match time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 283
    iget v5, p0, Lorg/apache/regexp/RETest;->failures:I

    if-lez v5, :cond_2

    .line 284
    const-string v5, "*************** THERE ARE FAILURES! *******************"

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 286
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Tests complete.  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/regexp/RETest;->testCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tests, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/regexp/RETest;->failures:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failure(s)."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 287
    return-void

    .line 268
    :cond_3
    :try_start_1
    invoke-direct {p0, v0}, Lorg/apache/regexp/RETest;->getNextTestCase(Ljava/io/BufferedReader;)Lorg/apache/regexp/RETestCase;

    move-result-object v4

    .line 269
    .local v4, "testcase":Lorg/apache/regexp/RETestCase;
    if-eqz v4, :cond_1

    .line 270
    invoke-virtual {v4}, Lorg/apache/regexp/RETestCase;->runTest()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 275
    .end local v4    # "testcase":Lorg/apache/regexp/RETestCase;
    :catchall_0
    move-exception v5

    .line 276
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 277
    throw v5
.end method

.method runInteractiveTests(Ljava/lang/String;)V
    .locals 8
    .param p1, "expr"    # Ljava/lang/String;

    .prologue
    .line 114
    new-instance v3, Lorg/apache/regexp/RE;

    invoke-direct {v3}, Lorg/apache/regexp/RE;-><init>()V

    .line 118
    .local v3, "r":Lorg/apache/regexp/RE;
    :try_start_0
    iget-object v6, p0, Lorg/apache/regexp/RETest;->compiler:Lorg/apache/regexp/REDebugCompiler;

    invoke-virtual {v6, p1}, Lorg/apache/regexp/REDebugCompiler;->compile(Ljava/lang/String;)Lorg/apache/regexp/REProgram;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/regexp/RE;->setProgram(Lorg/apache/regexp/REProgram;)V

    .line 121
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lorg/apache/regexp/RETest;->NEW_LINE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 124
    new-instance v5, Ljava/io/PrintWriter;

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v5, v6}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 125
    .local v5, "writer":Ljava/io/PrintWriter;
    iget-object v6, p0, Lorg/apache/regexp/RETest;->compiler:Lorg/apache/regexp/REDebugCompiler;

    invoke-virtual {v6, v5}, Lorg/apache/regexp/REDebugCompiler;->dumpProgram(Ljava/io/PrintWriter;)V

    .line 126
    invoke-virtual {v5}, Ljava/io/PrintWriter;->flush()V

    .line 128
    const/4 v4, 0x1

    .line 130
    .local v4, "running":Z
    :goto_0
    if-nez v4, :cond_0

    .line 165
    .end local v4    # "running":Z
    .end local v5    # "writer":Ljava/io/PrintWriter;
    :goto_1
    return-void

    .line 133
    .restart local v4    # "running":Z
    .restart local v5    # "writer":Ljava/io/PrintWriter;
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    sget-object v7, Ljava/lang/System;->in:Ljava/io/InputStream;

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 134
    .local v0, "br":Ljava/io/BufferedReader;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "> "

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 135
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v6}, Ljava/io/PrintStream;->flush()V

    .line 136
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "match":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 141
    invoke-virtual {v3, v2}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 143
    const-string v6, "Match successful."

    invoke-virtual {p0, v6}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 151
    :goto_2
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 160
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "match":Ljava/lang/String;
    .end local v4    # "running":Z
    .end local v5    # "writer":Ljava/io/PrintWriter;
    :catch_0
    move-exception v1

    .line 162
    .local v1, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 163
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 147
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "match":Ljava/lang/String;
    .restart local v4    # "running":Z
    .restart local v5    # "writer":Ljava/io/PrintWriter;
    :cond_1
    :try_start_1
    const-string v6, "Match failed."

    invoke-virtual {p0, v6}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    goto :goto_2

    .line 155
    :cond_2
    const/4 v4, 0x0

    .line 156
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v6}, Ljava/io/PrintStream;->println()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method say(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 215
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method showParens(Lorg/apache/regexp/RE;)V
    .locals 3
    .param p1, "r"    # Lorg/apache/regexp/RE;

    .prologue
    .line 225
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/regexp/RE;->getParenCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 230
    return-void

    .line 228
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "$"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1, v0}, Lorg/apache/regexp/RE;->getParen(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method testOther()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 296
    new-instance v3, Lorg/apache/regexp/RE;

    const-string v4, "(a*)b"

    invoke-direct {v3, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 297
    .local v3, "r":Lorg/apache/regexp/RE;
    const-string v4, "Serialized/deserialized (a*)b"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 298
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x80

    invoke-direct {v2, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 299
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v4, v3}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 300
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 301
    .local v1, "in":Ljava/io/ByteArrayInputStream;
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "r":Lorg/apache/regexp/RE;
    check-cast v3, Lorg/apache/regexp/RE;

    .line 302
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "aaab"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 304
    const-string v4, "Did not match \'aaab\' with deserialized RE."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 311
    :goto_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 312
    const-string v4, "Deserialized (a*)b"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 313
    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v4, v3}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 314
    new-instance v1, Ljava/io/ByteArrayInputStream;

    .end local v1    # "in":Ljava/io/ByteArrayInputStream;
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 315
    .restart local v1    # "in":Ljava/io/ByteArrayInputStream;
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "r":Lorg/apache/regexp/RE;
    check-cast v3, Lorg/apache/regexp/RE;

    .line 316
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    invoke-virtual {v3}, Lorg/apache/regexp/RE;->getParenCount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 318
    const-string v4, "Has parens after deserialization."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 320
    :cond_0
    const-string v4, "aaab"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 322
    const-string v4, "Did not match \'aaab\' with deserialized RE."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 329
    :goto_1
    new-instance v3, Lorg/apache/regexp/RE;

    .end local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "abc(\\w*)"

    invoke-direct {v3, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 330
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "MATCH_CASEINDEPENDENT abc(\\w*)"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 331
    invoke-virtual {v3, v5}, Lorg/apache/regexp/RE;->setMatchFlags(I)V

    .line 332
    const-string v4, "abc(d*)"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 333
    const-string v4, "abcddd"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 335
    const-string v4, "Did not match \'abcddd\'."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 341
    :goto_2
    const-string v4, "aBcDDdd"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 343
    const-string v4, "Did not match \'aBcDDdd\'."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 349
    :goto_3
    const-string v4, "ABCDDDDD"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 351
    const-string v4, "Did not match \'ABCDDDDD\'."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 357
    :goto_4
    new-instance v3, Lorg/apache/regexp/RE;

    .end local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "(A*)b\\1"

    invoke-direct {v3, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 358
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    invoke-virtual {v3, v5}, Lorg/apache/regexp/RE;->setMatchFlags(I)V

    .line 359
    const-string v4, "AaAaaaBAAAAAA"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 361
    const-string v4, "Did not match \'AaAaaaBAAAAAA\'."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 367
    :goto_5
    new-instance v3, Lorg/apache/regexp/RE;

    .end local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "[A-Z]*"

    invoke-direct {v3, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 368
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    invoke-virtual {v3, v5}, Lorg/apache/regexp/RE;->setMatchFlags(I)V

    .line 369
    const-string v4, "CaBgDe12"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_14

    .line 371
    const-string v4, "Did not match \'CaBgDe12\'."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 378
    :goto_6
    new-instance v3, Lorg/apache/regexp/RE;

    .end local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "^abc$"

    invoke-direct {v3, v4}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;)V

    .line 379
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "\nabc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 380
    const-string v4, "\"\\nabc\" matches \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 384
    :cond_1
    new-instance v3, Lorg/apache/regexp/RE;

    .end local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "^abc$"

    invoke-direct {v3, v4, v6}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    .line 385
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "\nabc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 386
    const-string v4, "\"\\nabc\" doesn\'t match \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 388
    :cond_2
    const-string v4, "\rabc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 389
    const-string v4, "\"\\rabc\" doesn\'t match \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 391
    :cond_3
    const-string v4, "\r\nabc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 392
    const-string v4, "\"\\r\\nabc\" doesn\'t match \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 394
    :cond_4
    const-string/jumbo v4, "\u0085abc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 395
    const-string v4, "\"\\u0085abc\" doesn\'t match \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 397
    :cond_5
    const-string/jumbo v4, "\u2028abc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 398
    const-string v4, "\"\\u2028abc\" doesn\'t match \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 400
    :cond_6
    const-string/jumbo v4, "\u2029abc"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 401
    const-string v4, "\"\\u2029abc\" doesn\'t match \"^abc$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 405
    :cond_7
    new-instance v3, Lorg/apache/regexp/RE;

    .end local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "^a.*b$"

    invoke-direct {v3, v4, v6}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    .line 406
    .restart local v3    # "r":Lorg/apache/regexp/RE;
    const-string v4, "a\nb"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 407
    const-string v4, "\"a\\nb\" matches \"^a.*b$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 409
    :cond_8
    const-string v4, "a\rb"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 410
    const-string v4, "\"a\\rb\" matches \"^a.*b$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 412
    :cond_9
    const-string v4, "a\r\nb"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 413
    const-string v4, "\"a\\r\\nb\" matches \"^a.*b$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 415
    :cond_a
    const-string v4, "a\u0085b"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 416
    const-string v4, "\"a\\u0085b\" matches \"^a.*b$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 418
    :cond_b
    const-string v4, "a\u2028b"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 419
    const-string v4, "\"a\\u2028b\" matches \"^a.*b$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 421
    :cond_c
    const-string v4, "a\u2029b"

    invoke-virtual {v3, v4}, Lorg/apache/regexp/RE;->match(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 422
    const-string v4, "\"a\\u2029b\" matches \"^a.*b$\""

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 427
    :cond_d
    :try_start_0
    new-instance v0, Lorg/apache/regexp/REDebugCompiler;

    invoke-direct {v0}, Lorg/apache/regexp/REDebugCompiler;-><init>()V

    .line 428
    .local v0, "c":Lorg/apache/regexp/REDebugCompiler;
    const-string v4, "(a{8192})?"

    invoke-virtual {v0, v4}, Lorg/apache/regexp/REDebugCompiler;->compile(Ljava/lang/String;)Lorg/apache/regexp/REProgram;

    .line 429
    const-string v4, "(a{8192})? should fail to compile."

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->fail(Ljava/lang/String;)V

    .line 430
    invoke-virtual {v0}, Lorg/apache/regexp/REDebugCompiler;->dumpProgram()V
    :try_end_0
    .catch Lorg/apache/regexp/RESyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    .end local v0    # "c":Lorg/apache/regexp/REDebugCompiler;
    :goto_7
    return-void

    .line 306
    :cond_e
    const-string v4, "aaaab = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_0

    .line 324
    :cond_f
    const-string v4, "aaaab = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_1

    .line 337
    :cond_10
    const-string v4, "abcddd = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_2

    .line 345
    :cond_11
    const-string v4, "aBcDDdd = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_3

    .line 353
    :cond_12
    const-string v4, "ABCDDDDD = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_4

    .line 363
    :cond_13
    const-string v4, "AaAaaaBAAAAAA = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_5

    .line 373
    :cond_14
    const-string v4, "CaBgDe12 = true"

    invoke-virtual {p0, v4}, Lorg/apache/regexp/RETest;->say(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0, v3}, Lorg/apache/regexp/RETest;->showParens(Lorg/apache/regexp/RE;)V

    goto/16 :goto_6

    .line 431
    :catch_0
    move-exception v4

    goto :goto_7
.end method
