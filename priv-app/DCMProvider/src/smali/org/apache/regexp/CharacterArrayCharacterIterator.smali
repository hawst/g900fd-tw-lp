.class public final Lorg/apache/regexp/CharacterArrayCharacterIterator;
.super Ljava/lang/Object;
.source "CharacterArrayCharacterIterator.java"

# interfaces
.implements Lorg/apache/regexp/CharacterIterator;


# instance fields
.field private final len:I

.field private final off:I

.field private final src:[C


# direct methods
.method public constructor <init>([CII)V
    .locals 0
    .param p1, "src"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->src:[C

    .line 39
    iput p2, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->off:I

    .line 40
    iput p3, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->len:I

    .line 41
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->src:[C

    iget v1, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->off:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public isEnd(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->len:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public substring(I)Ljava/lang/String;
    .locals 1
    .param p1, "beginIndex"    # I

    .prologue
    .line 60
    iget v0, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->len:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/regexp/CharacterArrayCharacterIterator;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public substring(II)Ljava/lang/String;
    .locals 4
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->len:I

    if-le p2, v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endIndex="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 48
    const-string v2, "; sequence size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->len:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    if-ltz p1, :cond_1

    if-le p1, p2, :cond_2

    .line 51
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "beginIndex="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 52
    const-string v2, "; endIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_2
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->src:[C

    iget v2, p0, Lorg/apache/regexp/CharacterArrayCharacterIterator;->off:I

    add-int/2addr v2, p1

    sub-int v3, p2, p1

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
