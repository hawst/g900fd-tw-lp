.class public Lorg/apache/regexp/REUtil;
.super Ljava/lang/Object;
.source "REUtil.java"


# static fields
.field private static final complexPrefix:Ljava/lang/String; = "complex:"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createRE(Ljava/lang/String;)Lorg/apache/regexp/RE;
    .locals 1
    .param p0, "expression"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/regexp/REUtil;->createRE(Ljava/lang/String;I)Lorg/apache/regexp/RE;

    move-result-object v0

    return-object v0
.end method

.method public static createRE(Ljava/lang/String;I)Lorg/apache/regexp/RE;
    .locals 2
    .param p0, "expression"    # Ljava/lang/String;
    .param p1, "matchFlags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/regexp/RESyntaxException;
        }
    .end annotation

    .prologue
    .line 41
    const-string v0, "complex:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Lorg/apache/regexp/RE;

    const-string v1, "complex:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    .line 45
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/regexp/RE;

    invoke-static {p0}, Lorg/apache/regexp/RE;->simplePatternToFullRegularExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/apache/regexp/RE;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method
