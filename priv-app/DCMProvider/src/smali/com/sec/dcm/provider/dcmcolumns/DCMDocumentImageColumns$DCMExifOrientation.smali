.class public final enum Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;
.super Ljava/lang/Enum;
.source "DCMDocumentImageColumns.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DCMExifOrientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

.field public static final enum ORIENTATION_LANDSCAPE:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

.field public static final enum ORIENTATION_PORTRAIT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;


# instance fields
.field private final orientation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 112
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    const-string v1, "ORIENTATION_LANDSCAPE"

    const-string v2, "landscape"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->ORIENTATION_LANDSCAPE:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    .line 113
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    const-string v1, "ORIENTATION_PORTRAIT"

    const-string v2, "portrait"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->ORIENTATION_PORTRAIT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    .line 111
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->ORIENTATION_LANDSCAPE:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->ORIENTATION_PORTRAIT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "orienrtation_type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 118
    iput-object p3, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->orientation:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    const-class v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    return-object v0
.end method

.method public static values()[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    invoke-virtual {v0}, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;

    return-object v0
.end method


# virtual methods
.method public type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;->orientation:Ljava/lang/String;

    return-object v0
.end method
