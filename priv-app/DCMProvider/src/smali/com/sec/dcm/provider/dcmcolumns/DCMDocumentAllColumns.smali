.class public final enum Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
.super Ljava/lang/Enum;
.source "DCMDocumentAllColumns.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final FULL_PROJECTION:[Ljava/lang/String;

.field public static final enum addr_bcc:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum addr_cc:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum addr_from:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum addr_to:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum album:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum artist:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum attachment_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum author:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum bitrate:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum bucket:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum bucket_id:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum calendar_event:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum creator:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum date:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum description:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum duration:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum event_uri:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum file_size:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum format:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum genre:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum id:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum keyword:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum language:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum last_updated_time:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum location__x:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum location__y:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum message_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum mimetype:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum modification_date:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum named_location:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum page_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum path:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum person_names:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum radiuskm:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum readable:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum resolution:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum scene_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum subscene_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum text:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum text_body:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum text_temp_path:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum title:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum uri:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum user_tags:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum word_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

.field public static final enum year:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 29
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "id"

    const-string v2, "id"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->id:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 30
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "title"

    const-string v2, "title"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->title:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 31
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "date"

    const-string v2, "date"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->date:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 32
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "modification_date"

    const-string v2, "modification_date"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->modification_date:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 33
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "path"

    const-string v2, "path"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->path:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 34
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "uri"

    const/4 v2, 0x5

    const-string v3, "uri"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->uri:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 35
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "mimetype"

    const/4 v2, 0x6

    const-string v3, "mimetype"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->mimetype:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 36
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "user_tags"

    const/4 v2, 0x7

    const-string v3, "user_tags"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->user_tags:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 37
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "artist"

    const/16 v2, 0x8

    const-string v3, "artist"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->artist:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 38
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "album"

    const/16 v2, 0x9

    const-string v3, "album"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->album:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 39
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string/jumbo v1, "year"

    const/16 v2, 0xa

    const-string/jumbo v3, "year"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->year:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 40
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "genre"

    const/16 v2, 0xb

    const-string v3, "genre"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->genre:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 41
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "location__x"

    const/16 v2, 0xc

    const-string v3, "location__x"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->location__x:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 42
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "location__y"

    const/16 v2, 0xd

    const-string v3, "location__y"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->location__y:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 43
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "radiuskm"

    const/16 v2, 0xe

    const-string v3, "radiuskm"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->radiuskm:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 44
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "event_uri"

    const/16 v2, 0xf

    const-string v3, "event_uri"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->event_uri:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 45
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "named_location"

    const/16 v2, 0x10

    const-string v3, "named_location"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->named_location:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 46
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "person_names"

    const/16 v2, 0x11

    const-string v3, "person_names"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->person_names:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 47
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "calendar_event"

    const/16 v2, 0x12

    const-string v3, "calendar_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->calendar_event:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 48
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "scene_type"

    const/16 v2, 0x13

    const-string v3, "scene_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->scene_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 49
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "subscene_type"

    const/16 v2, 0x14

    const-string v3, "subscene_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->subscene_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 50
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "description"

    const/16 v2, 0x15

    const-string v3, "description"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->description:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 51
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "duration"

    const/16 v2, 0x16

    const-string v3, "duration"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->duration:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 52
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "language"

    const/16 v2, 0x17

    const-string v3, "language"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->language:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 53
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "resolution"

    const/16 v2, 0x18

    const-string v3, "resolution"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->resolution:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 54
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "bucket"

    const/16 v2, 0x19

    const-string v3, "bucket"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->bucket:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 55
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "addr_from"

    const/16 v2, 0x1a

    const-string v3, "addr_from"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_from:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 56
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "addr_to"

    const/16 v2, 0x1b

    const-string v3, "addr_to"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_to:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 57
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "addr_cc"

    const/16 v2, 0x1c

    const-string v3, "addr_cc"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_cc:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 58
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "addr_bcc"

    const/16 v2, 0x1d

    const-string v3, "addr_bcc"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_bcc:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 59
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "text"

    const/16 v2, 0x1e

    const-string v3, "text"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->text:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 60
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "attachment_count"

    const/16 v2, 0x1f

    const-string v3, "attachment_count"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->attachment_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 61
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "message_type"

    const/16 v2, 0x20

    const-string v3, "message_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->message_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 62
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "author"

    const/16 v2, 0x21

    const-string v3, "author"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->author:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 63
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "creator"

    const/16 v2, 0x22

    const-string v3, "creator"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->creator:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 64
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "format"

    const/16 v2, 0x23

    const-string v3, "format"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->format:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 65
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "keyword"

    const/16 v2, 0x24

    const-string v3, "keyword"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->keyword:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 66
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "page_count"

    const/16 v2, 0x25

    const-string v3, "page_count"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->page_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 67
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "readable"

    const/16 v2, 0x26

    const-string v3, "readable"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->readable:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 68
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "text_temp_path"

    const/16 v2, 0x27

    const-string v3, "text_temp_path"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->text_temp_path:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 69
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string/jumbo v1, "word_count"

    const/16 v2, 0x28

    const-string/jumbo v3, "word_count"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->word_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 70
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "file_size"

    const/16 v2, 0x29

    const-string v3, "file_size"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->file_size:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 71
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "bitrate"

    const/16 v2, 0x2a

    const-string v3, "bitrate"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->bitrate:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 72
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "text_body"

    const/16 v2, 0x2b

    const-string v3, "text_body"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->text_body:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 73
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "last_updated_time"

    const/16 v2, 0x2c

    const-string v3, "last_updated_time"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->last_updated_time:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 74
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    const-string v1, "bucket_id"

    const/16 v2, 0x2d

    const-string v3, "bucket_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->bucket_id:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 28
    const/16 v0, 0x2e

    new-array v0, v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->id:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->title:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->date:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->modification_date:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->path:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->uri:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->mimetype:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->user_tags:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->artist:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->album:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->year:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->genre:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->location__x:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->location__y:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->radiuskm:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->event_uri:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->named_location:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->person_names:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->calendar_event:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->scene_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->subscene_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->description:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->duration:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->language:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->resolution:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->bucket:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_from:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_to:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_cc:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->addr_bcc:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->text:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->attachment_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->message_type:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->author:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->creator:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->format:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->keyword:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->page_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->readable:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->text_temp_path:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->word_count:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->file_size:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->bitrate:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->text_body:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->last_updated_time:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->bucket_id:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    .line 96
    const/16 v0, 0x2d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "modification_date"

    aput-object v1, v0, v6

    const-string v1, "path"

    aput-object v1, v0, v7

    const-string v1, "uri"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "genre"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "location__x"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "location__y"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "radiuskm"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "event_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "named_location"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "person_names"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "calendar_event"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "scene_type"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "subscene_type"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "language"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "bucket"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "addr_from"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "addr_to"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "addr_cc"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "addr_bcc"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "text"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "attachment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "author"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "creator"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "format"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "keyword"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "page_count"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "readable"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "text_temp_path"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "word_count"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "file_size"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "bitrate"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "text_body"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "last_updated_time"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput-object p3, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->mName:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 87
    const-string v4, "DCMDocumentAllColumns fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-static {}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->values()[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v1

    .local v1, "arr$":[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 89
    .local v0, "aa":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    iget-object v4, v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 93
    .end local v0    # "aa":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :goto_1
    return-object v0

    .line 88
    .restart local v0    # "aa":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 93
    .end local v0    # "aa":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    return-object v0
.end method

.method public static values()[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    invoke-virtual {v0}, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->mName:Ljava/lang/String;

    return-object v0
.end method
