.class public final enum Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;
.super Ljava/lang/Enum;
.source "DCMDocumentImageColumns.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DCMSceneType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_BIKE:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_CAR:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_DESERT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_DOCUMENT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_FLOWER:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_FOOD:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_INDOOR:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_PERSON:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_PET:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_SCENERY:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

.field public static final enum SCENE_TYPE_STREET:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;


# instance fields
.field private final scene_type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 73
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_SCENERY"

    const-string v2, "scenery"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_SCENERY:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 74
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_BIKE"

    const-string v2, "bike"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_BIKE:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 75
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_CAR"

    const-string v2, "car"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_CAR:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 76
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_DESERT"

    const-string v2, "desert"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_DESERT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 77
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_DOCUMENT"

    const-string v2, "document"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_DOCUMENT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 78
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_FLOWER"

    const/4 v2, 0x5

    const-string v3, "flower"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_FLOWER:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 79
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_FOOD"

    const/4 v2, 0x6

    const-string v3, "food"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_FOOD:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 80
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_INDOOR"

    const/4 v2, 0x7

    const-string v3, "indoor"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_INDOOR:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 81
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_PERSON"

    const/16 v2, 0x8

    const-string v3, "person"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_PERSON:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 82
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_PET"

    const/16 v2, 0x9

    const-string v3, "pet"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_PET:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 83
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    const-string v1, "SCENE_TYPE_STREET"

    const/16 v2, 0xa

    const-string v3, "street"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_STREET:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    .line 72
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_SCENERY:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_BIKE:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_CAR:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_DESERT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_DOCUMENT:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_FLOWER:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_FOOD:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_INDOOR:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_PERSON:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_PET:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->SCENE_TYPE_STREET:Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 88
    iput-object p3, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->scene_type:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    return-object v0
.end method

.method public static values()[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    invoke-virtual {v0}, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;

    return-object v0
.end method


# virtual methods
.method type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;->scene_type:Ljava/lang/String;

    return-object v0
.end method
