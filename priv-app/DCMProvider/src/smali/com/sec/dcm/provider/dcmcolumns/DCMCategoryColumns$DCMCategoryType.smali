.class public final enum Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
.super Ljava/lang/Enum;
.source "DCMCategoryColumns.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DCMCategoryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public static final enum CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public static final enum CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public static final enum CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public static final enum CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public static final enum CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public static final enum CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;


# instance fields
.field cat_type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    const-string v1, "CATEGORY_NAMED_LOCATION"

    const-string v2, "0"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 45
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    const-string v1, "CATEGORY_PERSON_NAME"

    const-string v2, "1"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 46
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    const-string v1, "CATEGORY_CALENDAR_EVENT"

    const-string v2, "2"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 47
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    const-string v1, "CATEGORY_USER_TAG"

    const-string v2, "3"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 48
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    const-string v1, "CATEGORY_SCENE_TYPE"

    const-string v2, "4"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 49
    new-instance v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    const-string v1, "CATEGORY_SUBSCENE_TYPE"

    const/4 v2, 0x5

    const-string v3, "5"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 43
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    iput-object p3, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->cat_type:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    return-object v0
.end method

.method public static values()[Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->$VALUES:[Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v0}, [Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    return-object v0
.end method


# virtual methods
.method public type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->cat_type:Ljava/lang/String;

    return-object v0
.end method
