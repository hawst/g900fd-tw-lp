.class public Lcom/sec/dcm/provider/db/tokenparser/MessageTokenParser;
.super Lcom/sec/dcm/provider/db/tokenparser/TokenParser;
.source "MessageTokenParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/tokenparser/MessageTokenParser$1;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/dcm/provider/db/tokenparser/MessageTokenParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/MessageTokenParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;-><init>()V

    .line 79
    return-void
.end method


# virtual methods
.method public processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "token"    # Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .prologue
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    const/4 v5, 0x1

    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/MessageTokenParser$1;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 79
    invoke-super {p0, p1, p2}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 81
    :goto_0
    return-object v2

    .line 55
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_1

    .line 56
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    :cond_0
    :goto_1
    move-object v2, v1

    .line 81
    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 59
    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto :goto_1

    .line 72
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 74
    .local v0, "arg":I
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4, v5, v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 76
    goto :goto_1

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
