.class public Lcom/sec/dcm/provider/db/DCMUpdateRunner;
.super Ljava/lang/Object;
.source "DCMUpdateRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/DCMUpdateRunner$2;
    }
.end annotation


# static fields
.field public static final MAX_NUM_RESULTS:I = 0x1388

.field public static final TAG:Ljava/lang/String;

.field private static auxDoc:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field private mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private mNRTSearcheId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    .line 79
    new-instance v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner$1;

    invoke-direct {v0}, Lcom/sec/dcm/provider/db/DCMUpdateRunner$1;-><init>()V

    sput-object v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->auxDoc:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/configuration/DcmTransport;)V
    .locals 4
    .param p1, "transport"    # Lcom/samsung/dcm/framework/configuration/DcmTransport;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTSearcheId:J

    .line 75
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 86
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 87
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTSearcheId:J

    .line 99
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v2, "DCMQueryRunner "

    invoke-static {v1, v2, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 97
    .end local v0    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :cond_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
.end method

.method private isBulkUpdate(Ljava/lang/String;)Z
    .locals 6
    .param p1, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v5, -0x1

    .line 566
    const-string v1, "uri"

    .line 567
    .local v1, "findStr":Ljava/lang/String;
    const/4 v2, 0x0

    .line 568
    .local v2, "lastIndex":I
    const/4 v0, 0x0

    .line 569
    .local v0, "count":I
    :cond_0
    if-eq v2, v5, :cond_2

    .line 570
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 571
    if-eq v2, v5, :cond_1

    .line 572
    add-int/lit8 v0, v0, 0x1

    .line 573
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v2, v4

    .line 575
    :cond_1
    if-le v0, v3, :cond_0

    .line 579
    :goto_0
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private updateCategoryInDoc(Lorg/apache/lucene/document/Document;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V
    .locals 37
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "contentValues"    # Landroid/content/ContentValues;
    .param p5, "newDoc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Landroid/content/ContentValues;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;",
            "Lorg/apache/lucene/document/Document;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150
    .local p3, "categoryToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .local p4, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v8

    .line 151
    .local v8, "columnNameset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 152
    .local v11, "existingSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 153
    .local v12, "existingSubSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 154
    .local v13, "existingUserTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 155
    .local v10, "existingNamedLocation":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 157
    .local v9, "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/16 v32, 0x0

    move/from16 v0, v32

    new-array v0, v0, [Z

    move-object/from16 v32, v0

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, v32

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 159
    const-string v32, "scene_type"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v24

    .line 160
    .local v24, "sceneTypeFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v14, v0, :cond_0

    .line 161
    aget-object v17, v24, v14

    .line 162
    .local v17, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v11, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 165
    .end local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_0
    const-string v32, "subscene_type"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    .line 166
    .local v27, "subsceneTypeFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v14, 0x0

    :goto_1
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v14, v0, :cond_1

    .line 167
    aget-object v17, v27, v14

    .line 168
    .restart local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 171
    .end local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_1
    const-string v32, "user_tags"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v31

    .line 172
    .local v31, "usertagFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v14, 0x0

    :goto_2
    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v14, v0, :cond_2

    .line 173
    aget-object v17, v31, v14

    .line 174
    .restart local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 179
    .end local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_2
    const-string v32, "named_location"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v20

    .line 180
    .local v20, "namedLocationFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v14, 0x0

    :goto_3
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v14, v0, :cond_3

    .line 181
    aget-object v17, v20, v14

    .line 182
    .restart local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 187
    .end local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_3
    const-string v32, "categories"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v6

    .line 188
    .local v6, "categoryFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v14, 0x0

    :goto_4
    array-length v0, v6

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v14, v0, :cond_4

    .line 189
    aget-object v17, v6, v14

    .line 190
    .restart local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 188
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 193
    .end local v17    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_4
    if-eqz p3, :cond_7

    .line 194
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 195
    .local v29, "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v7

    .line 196
    .local v7, "colName":Ljava/lang/String;
    if-eqz v7, :cond_5

    .line 197
    invoke-static {v7}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->fromString(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v19

    .line 198
    .local v19, "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    if-nez v19, :cond_6

    .line 199
    sget-object v32, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v33, "Invalid column in CP updation "

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    aput-object v7, v34, v35

    invoke-static/range {v32 .. v34}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 202
    :cond_6
    sget-object v32, Lcom/sec/dcm/provider/db/DCMUpdateRunner$2;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-virtual/range {v19 .. v19}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v33

    aget v32, v32, v33

    packed-switch v32, :pswitch_data_0

    .line 221
    sget-object v32, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v33, "Not supported column for CP updation colName = "

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    aput-object v7, v34, v35

    invoke-static/range {v32 .. v34}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    sget-object v32, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v33, "DCMDocumentAllColumns colName = "

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-static {v7}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v36

    aput-object v36, v34, v35

    invoke-static/range {v32 .. v34}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 205
    :pswitch_0
    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v11, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 206
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_scene_type;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 209
    :pswitch_1
    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 210
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_subscene_type;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 213
    :pswitch_2
    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v13, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 214
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_user_tag;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 217
    :pswitch_3
    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v10, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 218
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_named_location;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v29 .. v29}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 229
    .end local v7    # "colName":Ljava/lang/String;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v19    # "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    .end local v29    # "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    :cond_7
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_8
    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 230
    .restart local v7    # "colName":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 231
    .local v22, "newValue":Ljava/lang/String;
    if-eqz v22, :cond_9

    .line 232
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    .line 234
    :cond_9
    if-eqz v22, :cond_8

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v32

    if-nez v32, :cond_8

    .line 236
    sget-object v32, Lcom/sec/dcm/provider/db/DCMUpdateRunner$2;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-static {v7}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v33

    aget v32, v32, v33

    packed-switch v32, :pswitch_data_1

    .line 271
    sget-object v32, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v33, "Not supported column for CP updation"

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    aput-object v7, v34, v35

    invoke-static/range {v32 .. v34}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 238
    :pswitch_4
    const-string v32, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 239
    .local v23, "sceneNames":[Ljava/lang/String;
    move-object/from16 v4, v23

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_7
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    aget-object v25, v4, v16

    .line 240
    .local v25, "str":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-interface {v11, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 241
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_scene_type;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 239
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 247
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .end local v23    # "sceneNames":[Ljava/lang/String;
    .end local v25    # "str":Ljava/lang/String;
    :pswitch_5
    const-string v32, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 248
    .local v26, "subSceneNames":[Ljava/lang/String;
    move-object/from16 v4, v26

    .restart local v4    # "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/16 v16, 0x0

    .restart local v16    # "i$":I
    :goto_8
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    aget-object v25, v4, v16

    .line 249
    .restart local v25    # "str":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 250
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_subscene_type;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 248
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    .line 255
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .end local v25    # "str":Ljava/lang/String;
    .end local v26    # "subSceneNames":[Ljava/lang/String;
    :pswitch_6
    const-string v32, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 256
    .local v30, "userTagNames":[Ljava/lang/String;
    move-object/from16 v4, v30

    .restart local v4    # "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/16 v16, 0x0

    .restart local v16    # "i$":I
    :goto_9
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    aget-object v25, v4, v16

    .line 257
    .restart local v25    # "str":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 258
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_user_tag;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 256
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 263
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .end local v25    # "str":Ljava/lang/String;
    .end local v30    # "userTagNames":[Ljava/lang/String;
    :pswitch_7
    const-string v32, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 264
    .local v21, "namedLocationNames":[Ljava/lang/String;
    move-object/from16 v4, v21

    .restart local v4    # "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/16 v16, 0x0

    .restart local v16    # "i$":I
    :goto_a
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    aget-object v25, v4, v16

    .line 265
    .restart local v25    # "str":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 266
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "root_named_location;"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v16, v16, 0x1

    goto :goto_a

    .line 278
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v7    # "colName":Ljava/lang/String;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .end local v21    # "namedLocationNames":[Ljava/lang/String;
    .end local v22    # "newValue":Ljava/lang/String;
    .end local v25    # "str":Ljava/lang/String;
    :cond_a
    const-string v32, "scene_type"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 279
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .restart local v15    # "i$":Ljava/util/Iterator;
    :goto_b
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_b

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 280
    .local v28, "tag":Ljava/lang/String;
    new-instance v32, Lorg/apache/lucene/document/StringField;

    const-string v33, "scene_type"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v34

    sget-object v35, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v32 .. v35}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_b

    .line 284
    .end local v28    # "tag":Ljava/lang/String;
    :cond_b
    const-string v32, "subscene_type"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 285
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_c
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 286
    .restart local v28    # "tag":Ljava/lang/String;
    new-instance v32, Lorg/apache/lucene/document/StringField;

    const-string v33, "subscene_type"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v34

    sget-object v35, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v32 .. v35}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_c

    .line 290
    .end local v28    # "tag":Ljava/lang/String;
    :cond_c
    const-string v32, "user_tags"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 291
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_d
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_d

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 292
    .restart local v28    # "tag":Ljava/lang/String;
    new-instance v32, Lorg/apache/lucene/document/StringField;

    const-string v33, "user_tags"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v34

    sget-object v35, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v32 .. v35}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_d

    .line 295
    .end local v28    # "tag":Ljava/lang/String;
    :cond_d
    const-string v32, "named_location"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 296
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_e
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_e

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 297
    .restart local v28    # "tag":Ljava/lang/String;
    new-instance v32, Lorg/apache/lucene/document/StringField;

    const-string v33, "named_location"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v34

    sget-object v35, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v32 .. v35}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_e

    .line 301
    .end local v28    # "tag":Ljava/lang/String;
    :cond_e
    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v32

    if-lez v32, :cond_f

    .line 302
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->clear()V

    .line 303
    const-string v32, "categories"

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 304
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_f
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_f

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 305
    .local v5, "category":Ljava/lang/String;
    new-instance v32, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v33, ";"

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p4

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    new-instance v32, Lorg/apache/lucene/document/StoredField;

    const-string v33, "categories"

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p5

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_f

    .line 309
    .end local v5    # "category":Ljava/lang/String;
    :cond_f
    return-void

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 236
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private updateDocInDB(JLorg/apache/lucene/document/Document;Landroid/content/ContentValues;Ljava/util/ArrayList;)Z
    .locals 23
    .param p1, "useSearchId"    # J
    .param p3, "oldDoc"    # Lorg/apache/lucene/document/Document;
    .param p4, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lorg/apache/lucene/document/Document;",
            "Landroid/content/ContentValues;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 328
    .local p5, "categoryToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    const-string v2, "Old document cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    const-string v2, "ContentValues cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    const-string v2, "DCMToken cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    new-instance v7, Lorg/apache/lucene/document/Document;

    invoke-direct {v7}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 336
    .local v7, "newDoc":Lorg/apache/lucene/document/Document;
    const/16 v21, 0x1

    .line 337
    .local v21, "updateStatus":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const-string v3, "Manager not intialized"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    const/16 v17, 0x0

    .line 340
    .local v17, "newnrtSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .local v6, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 341
    invoke-direct/range {v2 .. v7}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->updateCategoryInDoc(Lorg/apache/lucene/document/Document;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V

    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;

    move-result-object v2

    invoke-virtual {v2, v7, v6}, Lorg/apache/lucene/facet/index/FacetFields;->addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V

    .line 344
    new-instance v20, Lorg/apache/lucene/index/Term;

    const-string v2, "path"

    const-string v3, "path"

    invoke-virtual {v7, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    .local v20, "term":Lorg/apache/lucene/index/Term;
    const-string v2, "id"

    invoke-virtual {v7, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v8

    .line 349
    .local v8, "contentId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v7, v3, v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Ljava/lang/Long;Ljava/lang/Integer;)J

    move-result-wide v18

    .line 354
    .local v18, "newGenId":J
    cmp-long v2, v18, p1

    if-eqz v2, :cond_2

    .line 355
    sget-object v2, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v3, "Race Condition generationid ="

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x1

    const-string v9, " newgenid = "

    aput-object v9, v4, v5

    const/4 v5, 0x2

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v17

    .line 359
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 360
    .local v16, "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    const-string v2, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V

    .line 363
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 365
    const-wide v10, 0x7fffffffffffffffL

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/document/Document;

    move-object/from16 v9, p0

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    invoke-direct/range {v9 .. v14}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->updateDocInDB(JLorg/apache/lucene/document/Document;Landroid/content/ContentValues;Ljava/util/ArrayList;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 377
    if-eqz v17, :cond_0

    .line 378
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 379
    const/16 v17, 0x0

    .line 382
    .end local v6    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v8    # "contentId":I
    .end local v16    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v18    # "newGenId":J
    .end local v20    # "term":Lorg/apache/lucene/index/Term;
    :cond_0
    :goto_0
    return v2

    .line 368
    .restart local v6    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .restart local v8    # "contentId":I
    .restart local v16    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v18    # "newGenId":J
    .restart local v20    # "term":Lorg/apache/lucene/index/Term;
    :cond_1
    const/16 v21, 0x0

    .line 377
    .end local v16    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    :cond_2
    if-eqz v17, :cond_3

    .line 378
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 379
    const/16 v17, 0x0

    .end local v6    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v8    # "contentId":I
    .end local v18    # "newGenId":J
    .end local v20    # "term":Lorg/apache/lucene/index/Term;
    :cond_3
    :goto_1
    move/from16 v2, v21

    .line 382
    goto :goto_0

    .line 371
    :catch_0
    move-exception v15

    .line 372
    .local v15, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v3, "updateDocInDB Exception:"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374
    const/16 v21, 0x0

    .line 377
    if-eqz v17, :cond_3

    .line 378
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 379
    const/16 v17, 0x0

    goto :goto_1

    .line 377
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v17, :cond_4

    .line 378
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 379
    const/16 v17, 0x0

    :cond_4
    throw v2
.end method

.method private updateDocs(Landroid/util/SparseArray;Lorg/apache/lucene/search/Query;Landroid/content/ContentValues;Ljava/util/ArrayList;)I
    .locals 22
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/apache/lucene/search/Query;",
            "Landroid/content/ContentValues;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "uriArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .local p4, "categoryToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    const/4 v13, 0x0

    .line 105
    .local v13, "count":I
    sget-object v3, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/document/Document;

    .line 106
    .local v19, "newDoc":Lorg/apache/lucene/document/Document;
    const/16 v20, 0x0

    .line 107
    .local v20, "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "uri"

    aput-object v4, v6, v3

    .line 108
    .local v6, "projection":[Ljava/lang/String;
    const/4 v15, 0x0

    .line 109
    .local v15, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v3, v4}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v5

    .line 112
    .local v5, "dcmQuery":Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    :try_start_0
    new-instance v2, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lcom/sec/dcm/provider/db/DCMLuceneQuery;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    .end local v20    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .local v2, "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_1
    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getDocuments()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v15

    .line 119
    if-eqz v2, :cond_0

    .line 120
    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    .line 123
    :cond_0
    if-eqz v15, :cond_4

    .line 124
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/document/Document;

    .line 125
    .local v10, "doc":Lorg/apache/lucene/document/Document;
    const-string v3, "id"

    invoke-virtual {v10, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v18

    .line 126
    .local v18, "key":I
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 127
    invoke-static/range {v19 .. v19}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 128
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTSearcheId:J

    move-object/from16 v7, p0

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    invoke-direct/range {v7 .. v12}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->updateDocInDB(JLorg/apache/lucene/document/Document;Landroid/content/ContentValues;Ljava/util/ArrayList;)Z

    move-result v21

    .line 130
    .local v21, "updateStatus":Z
    const/4 v3, 0x1

    move/from16 v0, v21

    if-ne v0, v3, :cond_2

    .line 131
    add-int/lit8 v13, v13, 0x1

    .line 133
    :cond_2
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 135
    .end local v21    # "updateStatus":Z
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 139
    .end local v10    # "doc":Lorg/apache/lucene/document/Document;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "key":I
    :cond_4
    sget-object v3, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->remove()V

    move v14, v13

    .line 140
    .end local v13    # "count":I
    .local v14, "count":I
    :goto_0
    return v14

    .line 115
    .end local v2    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .end local v14    # "count":I
    .restart local v13    # "count":I
    .restart local v20    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_0
    move-exception v16

    move-object/from16 v2, v20

    .line 116
    .end local v20    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v2    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .local v16, "e":Ljava/io/IOException;
    :goto_1
    :try_start_2
    sget-object v3, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v4, "Exception in creating query runner"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v3, v4, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119
    if-eqz v2, :cond_5

    .line 120
    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    :cond_5
    move v14, v13

    .end local v13    # "count":I
    .restart local v14    # "count":I
    goto :goto_0

    .line 119
    .end local v2    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .end local v14    # "count":I
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v13    # "count":I
    .restart local v20    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catchall_0
    move-exception v3

    move-object/from16 v2, v20

    .end local v20    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v2    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :goto_2
    if-eqz v2, :cond_6

    .line 120
    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    :cond_6
    throw v3

    .line 119
    :catchall_1
    move-exception v3

    goto :goto_2

    .line 115
    :catch_1
    move-exception v16

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 589
    :try_start_0
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v1, :cond_0

    .line 590
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 591
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 592
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 593
    sget-object v1, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v2, " NRTSearcher released"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 598
    :cond_0
    :goto_0
    return-void

    .line 595
    :catch_0
    move-exception v0

    .line 596
    .local v0, "ioe":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v2, "Exception"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public update(ILandroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)I
    .locals 56
    .param p1, "table"    # I
    .param p2, "contentValues"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "quickInsertAppend"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v25

    .line 401
    .local v25, "columnNameset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/16 v40, 0x0

    .line 402
    .local v40, "nDocUpdated":I
    if-eqz v25, :cond_0

    invoke-interface/range {v25 .. v25}, Ljava/util/Set;->size()I

    move-result v7

    if-gtz v7, :cond_2

    .line 404
    :cond_0
    const/4 v7, 0x0

    .line 560
    :cond_1
    :goto_0
    return v7

    .line 406
    :cond_2
    if-eqz p3, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->isBulkUpdate(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 409
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v46

    .line 410
    .local v46, "startBulkUpdateTime":J
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "Updation for multiple uris:Change/Remove category"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v7, v8, v11}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    new-instance v54, Landroid/util/SparseArray;

    invoke-direct/range {v54 .. v54}, Landroid/util/SparseArray;-><init>()V

    .line 412
    .local v54, "uriArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    new-instance v26, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct/range {v26 .. v26}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 414
    .local v26, "complexQuery":Lorg/apache/lucene/search/BooleanQuery;
    const-string v7, "deleted"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-static {v7, v8, v11, v12, v13}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v20

    .line 415
    .local v20, "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 417
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 423
    .local v23, "categoryToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    const-string v7, "(?!\'[\\p{L}0-9\\s]*)AND(?![\\p{L}0-9\\s]*\')"

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v45

    .line 424
    .local v45, "splitString":[Ljava/lang/String;
    move-object/from16 v18, v45

    .local v18, "arr$":[Ljava/lang/String;
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v38, v0

    .local v38, "len$":I
    const/16 v31, 0x0

    .local v31, "i$":I
    move/from16 v36, v31

    .end local v18    # "arr$":[Ljava/lang/String;
    .end local v31    # "i$":I
    .end local v38    # "len$":I
    .local v36, "i$":I
    :goto_1
    move/from16 v0, v36

    move/from16 v1, v38

    if-ge v0, v1, :cond_5

    aget-object v51, v18, v36

    .line 425
    .local v51, "str":Ljava/lang/String;
    const-string v7, "uri"

    move-object/from16 v0, v51

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 426
    move-object/from16 v55, v51

    .line 427
    .local v55, "uriClause":Ljava/lang/String;
    sget-object v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v7}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v55

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v8}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v55

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v55

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v55

    .line 432
    const-string v7, "(?!\'[\\p{L}0-9\\s]*)OR(?![\\p{L}0-9\\s]*\')"

    move-object/from16 v0, v55

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .local v19, "arr$":[Ljava/lang/String;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v39, v0

    .local v39, "len$":I
    const/16 v31, 0x0

    .end local v36    # "i$":I
    .restart local v31    # "i$":I
    :goto_2
    move/from16 v0, v31

    move/from16 v1, v39

    if-ge v0, v1, :cond_4

    aget-object v53, v19, v31

    .line 433
    .local v53, "uri":Ljava/lang/String;
    sget-object v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v7}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v53

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, v53

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    .line 434
    .local v17, "argument":Ljava/lang/String;
    const-string v7, "/"

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v37

    .line 435
    .local v37, "id":I
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v54

    move/from16 v1, v37

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 432
    add-int/lit8 v31, v31, 0x1

    goto :goto_2

    .line 438
    .end local v17    # "argument":Ljava/lang/String;
    .end local v19    # "arr$":[Ljava/lang/String;
    .end local v31    # "i$":I
    .end local v37    # "id":I
    .end local v39    # "len$":I
    .end local v53    # "uri":Ljava/lang/String;
    .end local v55    # "uriClause":Ljava/lang/String;
    .restart local v36    # "i$":I
    :cond_3
    const-string v7, "scene_type"

    move-object/from16 v0, v51

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 439
    new-instance v52, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    sget-object v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-object/from16 v0, v52

    move-object/from16 v1, v51

    invoke-direct {v0, v1, v7}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V

    .line 440
    .local v52, "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual/range {v52 .. v52}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v17

    .line 441
    .restart local v17    # "argument":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "root_scene_type;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 443
    new-instance v29, Lorg/apache/lucene/facet/search/DrillDownQuery;

    new-instance v7, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v7}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    const/4 v8, 0x0

    move-object/from16 v0, v29

    invoke-direct {v0, v7, v8}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .line 444
    .local v29, "drillQuery":Lorg/apache/lucene/facet/search/DrillDownQuery;
    const/4 v7, 0x1

    new-array v7, v7, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const/4 v8, 0x0

    new-instance v11, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v12, ";"

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v11, v7, v8

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 445
    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v26

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v7}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 424
    .end local v17    # "argument":Ljava/lang/String;
    .end local v29    # "drillQuery":Lorg/apache/lucene/facet/search/DrillDownQuery;
    .end local v36    # "i$":I
    .end local v52    # "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    :cond_4
    add-int/lit8 v31, v36, 0x1

    .restart local v31    # "i$":I
    move/from16 v36, v31

    .end local v31    # "i$":I
    .restart local v36    # "i$":I
    goto/16 :goto_1

    .line 448
    .end local v51    # "str":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move-object/from16 v2, v26

    move-object/from16 v3, p2

    move-object/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->updateDocs(Landroid/util/SparseArray;Lorg/apache/lucene/search/Query;Landroid/content/ContentValues;Ljava/util/ArrayList;)I

    move-result v40

    .line 449
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v32, v12, v46

    .line 450
    .local v32, "endBulkUpdateTime":J
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "Time taken to bulk update:"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v7, v8, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move/from16 v7, v40

    .line 451
    goto/16 :goto_0

    .line 458
    .end local v20    # "atomicQuery":Lorg/apache/lucene/search/Query;
    .end local v23    # "categoryToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .end local v26    # "complexQuery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v32    # "endBulkUpdateTime":J
    .end local v36    # "i$":I
    .end local v45    # "splitString":[Ljava/lang/String;
    .end local v46    # "startBulkUpdateTime":J
    .end local v54    # "uriArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_6
    const/16 v16, 0x0

    .line 459
    .local v16, "selections":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    move-object/from16 v43, p3

    .line 460
    .local v43, "oldSelction":Ljava/lang/String;
    if-eqz p3, :cond_7

    .line 461
    invoke-static/range {p3 .. p3}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->tokenizeSelectionClause(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    .line 464
    :cond_7
    const/16 v42, 0x0

    .line 465
    .local v42, "new_uri":Ljava/lang/String;
    const/16 v27, 0x0

    .line 466
    .local v27, "count":I
    if-eqz v16, :cond_a

    .line 467
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v31

    .local v31, "i$":Ljava/util/Iterator;
    :cond_8
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v52

    check-cast v52, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 468
    .restart local v52    # "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual/range {v52 .. v52}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v24

    .line 469
    .local v24, "colName":Ljava/lang/String;
    if-eqz v24, :cond_9

    const-string v7, "uri"

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 470
    invoke-virtual/range {v52 .. v52}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v42

    .line 472
    :cond_9
    invoke-virtual/range {v52 .. v52}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_8

    .line 473
    add-int/lit8 v27, v27, 0x1

    const/4 v7, 0x1

    move/from16 v0, v27

    if-le v0, v7, :cond_8

    .line 474
    const/16 v42, 0x0

    .line 481
    .end local v24    # "colName":Ljava/lang/String;
    .end local v31    # "i$":Ljava/util/Iterator;
    .end local v52    # "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v48

    .line 483
    .local v48, "startSearchTime":J
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "query"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v7, v8, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 485
    const/16 v28, 0x0

    .line 487
    .local v28, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    const/16 v21, 0x0

    .line 489
    .local v21, "bRepeatAgain":Z
    :cond_b
    const/4 v9, 0x0

    .line 490
    .local v9, "dcmQuery":Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    const/16 v44, 0x0

    .line 491
    .local v44, "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    const/4 v7, 0x2

    new-array v10, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "uri"

    aput-object v8, v10, v7

    const/4 v7, 0x1

    const-string v8, "id"

    aput-object v8, v10, v7

    .line 492
    .local v10, "projection":[Ljava/lang/String;
    new-instance v22, Lcom/sec/dcm/provider/db/DCMQueryBuilder;

    move-object/from16 v0, v22

    move/from16 v1, p1

    move-object/from16 v2, v43

    invoke-direct {v0, v1, v2}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;-><init>(ILjava/lang/String;)V

    .line 493
    .local v22, "builder":Lcom/sec/dcm/provider/db/DCMQueryBuilder;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->build()Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v9

    .line 494
    if-eqz v9, :cond_15

    .line 497
    :try_start_0
    new-instance v6, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    const/4 v11, 0x0

    invoke-direct/range {v6 .. v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lcom/sec/dcm/provider/db/DCMLuceneQuery;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    .end local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .local v6, "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_1
    invoke-virtual {v6}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getDocuments()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v28

    .line 506
    if-eqz v6, :cond_c

    .line 507
    invoke-virtual {v6}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    .line 511
    :cond_c
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v34, v12, v48

    .line 512
    .local v34, "endSearchTime":J
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "Time taken to search docs:"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v7, v8, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 514
    if-eqz v28, :cond_12

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_12

    if-eqz v42, :cond_12

    if-nez v21, :cond_12

    .line 515
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "No documents found to update, try to do IndexAndGet"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v7, v8, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 519
    move-object/from16 v0, v42

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v53

    .line 520
    .local v53, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v0, v53

    invoke-virtual {v7, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->indexAndGet(Landroid/net/Uri;)Z

    move-result v21

    .line 521
    if-eqz v21, :cond_e

    .line 522
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 523
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v7, :cond_d

    .line 524
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 525
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 528
    :cond_d
    :try_start_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_end_2
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_2 .. :try_end_2} :catch_1

    .line 533
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v7, :cond_11

    .line 534
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTSearcheId:J

    .line 538
    const/16 v21, 0x1

    .line 545
    .end local v53    # "uri":Landroid/net/Uri;
    :cond_e
    :goto_5
    if-nez v21, :cond_b

    .line 547
    if-eqz v28, :cond_14

    .line 549
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lorg/apache/lucene/document/Document;

    .line 550
    .local v41, "newDoc":Lorg/apache/lucene/document/Document;
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v31

    .restart local v31    # "i$":Ljava/util/Iterator;
    :cond_f
    :goto_6
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_13

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/document/Document;

    .line 552
    .local v14, "doc":Lorg/apache/lucene/document/Document;
    invoke-static/range {v41 .. v41}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 553
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->mNRTSearcheId:J

    move-object/from16 v11, p0

    move-object/from16 v15, p2

    invoke-direct/range {v11 .. v16}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->updateDocInDB(JLorg/apache/lucene/document/Document;Landroid/content/ContentValues;Ljava/util/ArrayList;)Z

    move-result v50

    .line 554
    .local v50, "status":Z
    const/4 v7, 0x1

    move/from16 v0, v50

    if-ne v0, v7, :cond_f

    .line 555
    add-int/lit8 v40, v40, 0x1

    goto :goto_6

    .line 502
    .end local v6    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .end local v14    # "doc":Lorg/apache/lucene/document/Document;
    .end local v31    # "i$":Ljava/util/Iterator;
    .end local v34    # "endSearchTime":J
    .end local v41    # "newDoc":Lorg/apache/lucene/document/Document;
    .end local v50    # "status":Z
    .restart local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_0
    move-exception v30

    move-object/from16 v6, v44

    .line 503
    .end local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v6    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .local v30, "e":Ljava/io/IOException;
    :goto_7
    :try_start_3
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "Exception in creating query runner"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual/range {v30 .. v30}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v7, v8, v11}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 504
    const/4 v7, 0x0

    .line 506
    if-eqz v6, :cond_1

    .line 507
    invoke-virtual {v6}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    goto/16 :goto_0

    .line 506
    .end local v6    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .end local v30    # "e":Ljava/io/IOException;
    .restart local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catchall_0
    move-exception v7

    move-object/from16 v6, v44

    .end local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v6    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :goto_8
    if-eqz v6, :cond_10

    .line 507
    invoke-virtual {v6}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    :cond_10
    throw v7

    .line 529
    .restart local v34    # "endSearchTime":J
    .restart local v53    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v30

    .line 530
    .local v30, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->TAG:Ljava/lang/String;

    const-string v8, "DCMUpdateRunner "

    move-object/from16 v0, v30

    invoke-static {v7, v8, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 536
    .end local v30    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :cond_11
    new-instance v7, Ljava/io/IOException;

    invoke-direct {v7}, Ljava/io/IOException;-><init>()V

    throw v7

    .line 542
    .end local v53    # "uri":Landroid/net/Uri;
    :cond_12
    const/16 v21, 0x0

    goto :goto_5

    .line 558
    .restart local v31    # "i$":Ljava/util/Iterator;
    .restart local v41    # "newDoc":Lorg/apache/lucene/document/Document;
    :cond_13
    sget-object v7, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->remove()V

    .end local v31    # "i$":Ljava/util/Iterator;
    .end local v41    # "newDoc":Lorg/apache/lucene/document/Document;
    :cond_14
    move/from16 v7, v40

    .line 560
    goto/16 :goto_0

    .line 506
    .end local v34    # "endSearchTime":J
    :catchall_1
    move-exception v7

    goto :goto_8

    .line 502
    :catch_2
    move-exception v30

    goto :goto_7

    .end local v6    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :cond_15
    move-object/from16 v6, v44

    .end local v44    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v6    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_3
.end method
