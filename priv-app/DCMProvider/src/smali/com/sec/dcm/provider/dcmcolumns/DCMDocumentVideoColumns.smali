.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentVideoColumns;
.super Ljava/lang/Object;
.source "DCMDocumentVideoColumns.java"

# interfaces
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMAVBaseColumns;


# static fields
.field public static final FIELD_BITRATE:Ljava/lang/String; = "bitrate"

.field public static final FIELD_BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket"

.field public static final FIELD_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FIELD_DURATION:Ljava/lang/String; = "duration"

.field public static final FIELD_FILE_SIZE:Ljava/lang/String; = "file_size"

.field public static final FIELD_GENRE:Ljava/lang/String; = "genre"

.field public static final FIELD_LANGUAGE:Ljava/lang/String; = "language"

.field public static final FIELD_LATITUDE:Ljava/lang/String; = "location__y"

.field public static final FIELD_LONGITUDE:Ljava/lang/String; = "location__x"

.field public static final FIELD_NAMED_LOCATION:Ljava/lang/String; = "named_location"

.field public static final FIELD_RADIUS_KM:Ljava/lang/String; = "radiuskm"

.field public static final FIELD_RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final FIELD_YEAR:Ljava/lang/String; = "year"

.field public static final URI:Landroid/net/Uri;

.field public static final VIDEO_FULL_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 8
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/videodocument"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentVideoColumns;->URI:Landroid/net/Uri;

    .line 25
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "language"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "bucket"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "named_location"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "location__x"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "location__y"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "file_size"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "bitrate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentVideoColumns;->VIDEO_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
