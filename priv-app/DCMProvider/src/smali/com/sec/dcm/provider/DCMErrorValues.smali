.class public Lcom/sec/dcm/provider/DCMErrorValues;
.super Ljava/lang/Object;
.source "DCMErrorValues.java"


# static fields
.field public static final DCM_ERR_ALL_DOCUMENT_N_CATEGORY_SEARCH_MADE_WITHOUT_CAT_SELECTION:Ljava/lang/String; = "All Document and Category Search Made with out Category Selection"

.field public static final DCM_ERR_BOTH_LAT_LONG_REQUIRED:Ljava/lang/String; = "Both Latitude and Longitude Required for Spatial Query"

.field public static final DCM_ERR_BOTH_PROJECTION_AND_SELECTION_NULL:Ljava/lang/String; = "Both Projection and Selection are NULL, atleast DOC_TYPE must be present in selection"

.field public static final DCM_ERR_BOTH_PROJECTION_NULL_NO_DOC_TYPE:Ljava/lang/String; = "Projection is NULL and DOC_TYPE not present in Selection"

.field public static final DCM_ERR_DOCUMENT_QUERY_ERROR:Ljava/lang/String; = "Document Query Error"

.field public static final DCM_ERR_IMAGE_N_CATEGORY_SEARCH_MADE_WITH_OUT_CAT_SELECTION:Ljava/lang/String; = "Image and Category Search Made with out Category Selection"

.field public static final DCM_ERR_INVALID_FACECOUNT_IN_SELECTION:Ljava/lang/String; = "Invalid Facecount in selection"

.field public static final DCM_ERR_INVALID_LOCATION_IN_SELECTION:Ljava/lang/String; = "Invalid Location in selection"

.field public static final DCM_ERR_INVALID_SEL:Ljava/lang/String; = "Invalid Selection"

.field public static final DCM_ERR_INVALID_SEL_ARGS:Ljava/lang/String; = "Invlaid Selection Arguments"

.field public static final DCM_ERR_INVALID_TABLE_URI:Ljava/lang/String; = "Invalid Table URI"

.field public static final DCM_ERR_INVALID_YEAR_IN_SELECTION:Ljava/lang/String; = "Invalid Year in selection"

.field public static final DCM_ERR_MIMETYPE_NOT_ALLOWED_IN_SELECTION:Ljava/lang/String; = "FIELD_MIME not allowed in Selection"

.field public static final DCM_ERR_MINUMUM_TWO_DATES_REQUIRED:Ljava/lang/String; = "From Date and To Date required for Date Range Query"

.field public static final DCM_ERR_MISSING_SEL_ARGUMENTS:Ljava/lang/String; = "Selection Arguments Missing"

.field public static final DCM_ERR_PATH_NOT_ALLOWED_IN_SELECTION:Ljava/lang/String; = "FIELD_PATH not allowed in Selection"

.field public static final DCM_ERR_TWO_DURATION_REQUIRED:Ljava/lang/String; = "From Duration and To Duration required for Duration Range Query"

.field public static final DCM_ERR_TWO_YEARS_REQUIRED:Ljava/lang/String; = "From Year and To Year required for Year Range Query"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_ALBUM_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Album Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_ARTIST_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Artist Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_BUCKET_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Bucket Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_CALENDAR_EVENT_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Calendar Event Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_DESC_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Description Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_DURATION_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Duration Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_EVENT_URI_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Event URI Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_FC_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Face Count Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_GENRE_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Genre Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_LANG_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Language Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_LOCATION_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Location Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_NAMED_LOCATION_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Named Location Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_PERSON_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Person Name Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_RESOLUTION_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Resolution Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_SCENE_TYPE_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Scene Type Selection"

.field public static final DCM_ERR_WRONG_DOC_TYPE_FOR_YEAR_IN_SELECTION:Ljava/lang/String; = "Wrong Document Type Requested for Year Selection"

.field public static final DCM_ERR_WRONG_SELECTION_COLUMN:Ljava/lang/String; = "Wrong Selection Column"

.field public static final DCM_ERR_WRONG_TABLE_URI:Ljava/lang/String; = "Wrong Table URI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
