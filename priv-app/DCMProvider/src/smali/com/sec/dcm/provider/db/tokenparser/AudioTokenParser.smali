.class public Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser;
.super Lcom/sec/dcm/provider/db/tokenparser/TokenParser;
.source "AudioTokenParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser$1;
    }
.end annotation


# static fields
.field public static final MIN_YEAR:I

.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;-><init>()V

    .line 108
    return-void
.end method

.method private getMaxYear()I
    .locals 3

    .prologue
    .line 116
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 117
    .local v0, "c":Ljava/util/Calendar;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 118
    .local v1, "now":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 119
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    return v2
.end method


# virtual methods
.method public processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;
    .locals 10
    .param p1, "token"    # Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    const/4 v2, 0x0

    .line 55
    .local v2, "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v5, Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser$1;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 108
    invoke-super {p0, p1, p2}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;

    move-result-object v5

    .line 111
    :goto_0
    return-object v5

    .line 60
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_1

    .line 61
    new-instance v2, Lorg/apache/lucene/search/WildcardQuery;

    .end local v2    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v2    # "atomicQuery":Lorg/apache/lucene/search/Query;
    :cond_0
    :goto_1
    move-object v5, v2

    .line 111
    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_0

    .line 63
    new-instance v2, Lorg/apache/lucene/search/TermQuery;

    .end local v2    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v2    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto :goto_1

    .line 67
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_2

    .line 68
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 69
    .local v4, "year":I
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 71
    goto :goto_1

    .end local v4    # "year":I
    :cond_2
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_3

    .line 72
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser;->getMaxYear()I

    move-result v3

    .line 73
    .local v3, "maxYear":I
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 75
    goto :goto_1

    .end local v3    # "maxYear":I
    :cond_3
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_4

    .line 76
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/tokenparser/AudioTokenParser;->getMaxYear()I

    move-result v3

    .line 77
    .restart local v3    # "maxYear":I
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 79
    goto/16 :goto_1

    .end local v3    # "maxYear":I
    :cond_4
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_5

    .line 80
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto/16 :goto_1

    .line 82
    :cond_5
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_0

    .line 83
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto/16 :goto_1

    .line 89
    :pswitch_2
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_6

    .line 90
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-long v0, v5

    .line 91
    .local v0, "arg":J
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 93
    goto/16 :goto_1

    .end local v0    # "arg":J
    :cond_6
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_7

    .line 94
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-wide v8, 0x7fffffffffffffffL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto/16 :goto_1

    .line 96
    :cond_7
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_8

    .line 97
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-wide v8, 0x7fffffffffffffffL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto/16 :goto_1

    .line 99
    :cond_8
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_9

    .line 100
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto/16 :goto_1

    .line 102
    :cond_9
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v6, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    goto/16 :goto_1

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
