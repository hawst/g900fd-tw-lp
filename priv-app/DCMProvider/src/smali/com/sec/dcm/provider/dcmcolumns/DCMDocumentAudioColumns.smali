.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAudioColumns;
.super Ljava/lang/Object;
.source "DCMDocumentAudioColumns.java"

# interfaces
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMAVBaseColumns;


# static fields
.field public static final AUDIO_FULL_PROJECTION:[Ljava/lang/String;

.field public static final FIELD_BITRATE:Ljava/lang/String; = "bitrate"

.field public static final FIELD_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FIELD_DURATION:Ljava/lang/String; = "duration"

.field public static final FIELD_FILE_SIZE:Ljava/lang/String; = "file_size"

.field public static final FIELD_GENRE:Ljava/lang/String; = "genre"

.field public static final FIELD_YEAR:Ljava/lang/String; = "year"

.field public static final URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/audiodocument"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAudioColumns;->URI:Landroid/net/Uri;

    .line 40
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "album"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "genre"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "file_size"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "bitrate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAudioColumns;->AUDIO_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
