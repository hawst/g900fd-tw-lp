.class public Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;
.super Ljava/lang/Object;
.source "TokenParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dcm/provider/db/tokenparser/TokenParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Tokenizer"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertToPostfix(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 302
    .local p0, "infixTokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 303
    .local v1, "postfixTokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    new-instance v5, Ljava/util/Stack;

    invoke-direct {v5}, Ljava/util/Stack;-><init>()V

    .line 304
    .local v5, "tokenStack":Ljava/util/Stack;, "Ljava/util/Stack<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 305
    .local v4, "tokenIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    new-instance v0, Ljava/lang/Exception;

    const-string v7, "Invalid Query"

    invoke-direct {v0, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 306
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 307
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 308
    .local v3, "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_1

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_1

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_1

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_1

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_1

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_2

    .line 311
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_2
    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_3

    .line 313
    invoke-virtual {v5, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 314
    :cond_3
    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_4

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_9

    .line 315
    :cond_4
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    .line 316
    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 317
    .local v2, "stackTop":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_5

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_5

    .line 318
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nInvalid expression!!"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 319
    throw v0

    .line 320
    :cond_5
    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_6

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_6

    .line 321
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nInvalid expression!!"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 322
    throw v0

    .line 324
    :cond_6
    :goto_1
    invoke-static {v2}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->priority(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;)I

    move-result v7

    invoke-static {v3}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->priority(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;)I

    move-result v8

    if-lt v7, v8, :cond_7

    .line 325
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    .line 327
    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "stackTop":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    check-cast v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .restart local v2    # "stackTop":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    goto :goto_1

    .line 332
    :cond_7
    invoke-virtual {v5, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 334
    .end local v2    # "stackTop":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    :cond_8
    invoke-virtual {v5, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 336
    :cond_9
    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v7, v8, :cond_0

    .line 338
    :goto_2
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 339
    .local v6, "top":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual {v6}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v7

    sget-object v8, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v7, v8, :cond_0

    .line 340
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 348
    .end local v3    # "token":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .end local v6    # "top":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    :cond_a
    :goto_3
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_b

    .line 349
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 352
    :cond_b
    return-object v1
.end method

.method private static priority(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;)I
    .locals 2
    .param p0, "token"    # Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v0

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v0

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v0, v1, :cond_1

    .line 357
    :cond_0
    const/4 v0, 0x1

    .line 361
    :goto_0
    return v0

    .line 358
    :cond_1
    invoke-virtual {p0}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v0

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v0

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v0, v1, :cond_3

    .line 359
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 361
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static tokenize(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 75
    const-string v3, "Selection is null"

    invoke-static {p0, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const/4 v1, 0x0

    .line 78
    .local v1, "postfixTokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->TAG:Ljava/lang/String;

    const-string v4, "Query :"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    invoke-static {p0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->tokenizeSelectionClause(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 81
    .local v2, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    :try_start_0
    invoke-static {v2}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->convertToPostfix(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v3, v1

    .line 87
    :goto_0
    return-object v3

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->TAG:Ljava/lang/String;

    const-string v4, "Invalid SQL Selection Clause Exception"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static tokenizeCategoryClause(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p0, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    if-eqz p0, :cond_9

    .line 103
    const/4 v3, 0x0

    .line 104
    .local v3, "columnName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 109
    .local v0, "argument":Ljava/lang/String;
    const-string v10, " AND "

    invoke-virtual {p0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-object v2, v9

    .line 158
    .end local v0    # "argument":Ljava/lang/String;
    .end local v2    # "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    .end local v3    # "columnName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 112
    .restart local v0    # "argument":Ljava/lang/String;
    .restart local v2    # "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    .restart local v3    # "columnName":Ljava/lang/String;
    :cond_1
    const-string v10, " OR "

    invoke-virtual {p0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 113
    .local v4, "eachSelection":[Ljava/lang/String;
    move-object v1, v4

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v7, v1, v5

    .line 114
    .local v7, "sel":Ljava/lang/String;
    const-string v10, "="

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 115
    .local v8, "select":[Ljava/lang/String;
    const/4 v10, 0x0

    aget-object v10, v8, v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 116
    const-string v10, "category_type"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 117
    const/4 v10, 0x1

    aget-object v10, v8, v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 118
    sget-object v10, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->type()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 119
    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_named_location"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 121
    :cond_2
    sget-object v10, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->type()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 122
    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_person"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 124
    :cond_3
    sget-object v10, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->type()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 125
    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_calendar_event"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 127
    :cond_4
    sget-object v10, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->type()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 128
    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_user_tag"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 130
    :cond_5
    sget-object v10, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->type()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 131
    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_scene_type"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 133
    :cond_6
    sget-object v10, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->type()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 134
    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_subscene_type"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    move-object v2, v9

    .line 139
    goto/16 :goto_0

    :cond_8
    move-object v2, v9

    .line 144
    goto/16 :goto_0

    .line 151
    .end local v0    # "argument":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "columnName":Ljava/lang/String;
    .end local v4    # "eachSelection":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "sel":Ljava/lang/String;
    .end local v8    # "select":[Ljava/lang/String;
    :cond_9
    new-instance v9, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v10, "root_named_location"

    sget-object v11, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    new-instance v9, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v10, "root_person"

    sget-object v11, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v9, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v10, "root_calendar_event"

    sget-object v11, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v9, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v10, "root_user_tag"

    sget-object v11, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v9, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v10, "root_scene_type"

    sget-object v11, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v9, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v10, "root_subscene_type"

    sget-object v11, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static tokenizeSelectionClause(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .param p0, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v13, 0x28

    const/16 v12, 0x27

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/16 v9, 0x3d

    .line 167
    const/4 v2, 0x0

    .line 172
    .local v2, "index":I
    const/4 v6, 0x0

    .line 173
    .local v6, "tokenType":Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v5, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    const-string v7, "\\+"

    const-string v8, "\\\\\\+"

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 181
    const-string v7, "\\|"

    const-string v8, "\\\\\\|"

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 182
    const-string v7, "(?!\'[\\p{L}0-9\\s]*)AND(?![\\p{L}0-9\\s]*\')"

    const-string v8, "+"

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 183
    const-string v7, "(?!\'[\\p{L}0-9\\s]*)OR(?![\\p{L}0-9\\s]*\')"

    const-string/jumbo v8, "|"

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 185
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v2, v7, :cond_f

    .line 186
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 224
    move v3, v2

    .line 225
    .local v3, "prevIndex":I
    const/4 v1, 0x0

    .line 227
    .local v1, "cnt":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v2, v7, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v13, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x29

    if-eq v7, v8, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x2b

    if-eq v7, v8, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x7c

    if-eq v7, v8, :cond_4

    .line 228
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v12, :cond_3

    .line 229
    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v2, v7, :cond_2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v12, :cond_2

    .line 230
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x5c

    if-ne v7, v8, :cond_1

    .line 231
    add-int/lit8 v2, v2, 0x1

    .line 233
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 188
    .end local v1    # "cnt":I
    .end local v3    # "prevIndex":I
    :sswitch_0
    new-array v0, v11, [C

    .line 189
    .local v0, "ch":[C
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v10

    .line 190
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    .line 191
    .local v4, "token":Ljava/lang/String;
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 192
    new-instance v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    invoke-direct {v7, v4, v6}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    add-int/lit8 v2, v2, 0x1

    .line 194
    goto :goto_0

    .line 196
    .end local v0    # "ch":[C
    .end local v4    # "token":Ljava/lang/String;
    :sswitch_1
    new-array v0, v11, [C

    .line 197
    .restart local v0    # "ch":[C
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v10

    .line 198
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    .line 199
    .restart local v4    # "token":Ljava/lang/String;
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 200
    new-instance v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    invoke-direct {v7, v4, v6}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v2, v2, 0x1

    .line 202
    goto/16 :goto_0

    .line 204
    .end local v0    # "ch":[C
    .end local v4    # "token":Ljava/lang/String;
    :sswitch_2
    new-array v0, v11, [C

    .line 205
    .restart local v0    # "ch":[C
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v10

    .line 206
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    .line 207
    .restart local v4    # "token":Ljava/lang/String;
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 208
    new-instance v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    invoke-direct {v7, v4, v6}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    add-int/lit8 v2, v2, 0x1

    .line 210
    goto/16 :goto_0

    .line 212
    .end local v0    # "ch":[C
    .end local v4    # "token":Ljava/lang/String;
    :sswitch_3
    new-array v0, v11, [C

    .line 213
    .restart local v0    # "ch":[C
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v10

    .line 214
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    .line 215
    .restart local v4    # "token":Ljava/lang/String;
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 216
    new-instance v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    invoke-direct {v7, v4, v6}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    add-int/lit8 v2, v2, 0x1

    .line 218
    goto/16 :goto_0

    .line 220
    .end local v0    # "ch":[C
    .end local v4    # "token":Ljava/lang/String;
    :sswitch_4
    add-int/lit8 v2, v2, 0x1

    .line 221
    goto/16 :goto_0

    .line 235
    .restart local v1    # "cnt":I
    .restart local v3    # "prevIndex":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 238
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 243
    :cond_4
    new-array v0, v1, [C

    .line 245
    .restart local v0    # "ch":[C
    move v2, v3

    .line 246
    const/4 v1, 0x0

    .line 248
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v2, v7, :cond_d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v13, :cond_d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x29

    if-eq v7, v8, :cond_d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x2b

    if-eq v7, v8, :cond_d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x7c

    if-eq v7, v8, :cond_d

    .line 249
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v12, :cond_7

    .line 250
    :goto_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v2, v7, :cond_6

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v12, :cond_6

    .line 251
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x5c

    if-ne v7, v8, :cond_5

    .line 252
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v1

    .line 253
    add-int/lit8 v2, v2, 0x1

    .line 258
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 255
    :cond_5
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v1

    goto :goto_5

    .line 260
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 262
    :cond_7
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v1

    .line 263
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v9, :cond_9

    .line 264
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 280
    :cond_8
    :goto_6
    add-int/lit8 v2, v2, 0x1

    .line 281
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 265
    :cond_9
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x3e

    if-ne v7, v8, :cond_a

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v9, :cond_a

    .line 266
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    goto :goto_6

    .line 267
    :cond_a
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x3e

    if-ne v7, v8, :cond_b

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v9, :cond_b

    .line 268
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 269
    add-int/lit8 v2, v2, 0x1

    .line 270
    add-int/lit8 v1, v1, 0x1

    .line 271
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v1

    goto :goto_6

    .line 272
    :cond_b
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x3c

    if-ne v7, v8, :cond_c

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v9, :cond_c

    .line 273
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    goto :goto_6

    .line 274
    :cond_c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x3c

    if-ne v7, v8, :cond_8

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v9, :cond_8

    .line 275
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 276
    add-int/lit8 v2, v2, 0x1

    .line 277
    add-int/lit8 v1, v1, 0x1

    .line 278
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aput-char v7, v0, v1

    goto :goto_6

    .line 285
    :cond_d
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    .line 288
    .restart local v4    # "token":Ljava/lang/String;
    sget-object v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v7}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 289
    sget-object v6, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 291
    :cond_e
    if-eqz v6, :cond_0

    .line 292
    new-instance v7, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    invoke-direct {v7, v4, v6}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 298
    .end local v0    # "ch":[C
    .end local v1    # "cnt":I
    .end local v3    # "prevIndex":I
    .end local v4    # "token":Ljava/lang/String;
    :cond_f
    return-object v5

    .line 186
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_4
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2b -> :sswitch_2
        0x7c -> :sswitch_3
    .end sparse-switch
.end method
