.class public Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
.super Ljava/lang/Object;
.source "DCMToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/tokenparser/DCMToken$1;,
        Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private inValidToken:Z

.field private mColumnArgument:Ljava/lang/String;

.field private mColumnName:Ljava/lang/String;

.field private mTokenType:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;)V
    .locals 1
    .param p1, "token"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    .line 67
    iput-object p2, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mTokenType:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 68
    invoke-direct {p0, p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->splitToken(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method private splitToken(Ljava/lang/String;)V
    .locals 6
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 97
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$1;->$SwitchMap$com$sec$dcm$provider$db$tokenparser$DCMToken$TokenType:[I

    iget-object v2, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mTokenType:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 193
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->TAG:Ljava/lang/String;

    const-string v2, "Not splitting token:"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mTokenType:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    :goto_0
    return-void

    .line 99
    :pswitch_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "select":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_0

    .line 103
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    goto :goto_0

    .line 107
    :cond_0
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    .line 109
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    const-string v2, "profile/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    iget-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    const-string v2, "%"

    const-string v3, "*"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    goto :goto_0

    .line 124
    .end local v0    # "select":[Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 127
    .restart local v0    # "select":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_2

    .line 128
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    goto/16 :goto_0

    .line 132
    :cond_2
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    .line 134
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    goto/16 :goto_0

    .line 138
    .end local v0    # "select":[Ljava/lang/String;
    :pswitch_2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 141
    .restart local v0    # "select":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_3

    .line 142
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    goto/16 :goto_0

    .line 145
    :cond_3
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    .line 147
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    goto/16 :goto_0

    .line 151
    .end local v0    # "select":[Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 154
    .restart local v0    # "select":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_4

    .line 155
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    goto/16 :goto_0

    .line 159
    :cond_4
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    .line 161
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    goto/16 :goto_0

    .line 165
    .end local v0    # "select":[Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 168
    .restart local v0    # "select":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_5

    .line 169
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    goto/16 :goto_0

    .line 173
    :cond_5
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    .line 175
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    goto/16 :goto_0

    .line 179
    .end local v0    # "select":[Ljava/lang/String;
    :pswitch_5
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 182
    .restart local v0    # "select":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_6

    .line 183
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    goto/16 :goto_0

    .line 187
    :cond_6
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    .line 189
    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    goto/16 :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public getColumnArgument()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnArgument:Ljava/lang/String;

    return-object v0
.end method

.method public getColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mColumnName:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->mTokenType:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    return-object v0
.end method

.method public isValidToken()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 222
    iget-boolean v1, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->inValidToken:Z

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method
