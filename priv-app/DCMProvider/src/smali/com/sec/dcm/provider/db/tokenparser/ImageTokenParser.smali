.class public Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;
.super Lcom/sec/dcm/provider/db/tokenparser/TokenParser;
.source "ImageTokenParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser$1;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;-><init>()V

    .line 137
    return-void
.end method


# virtual methods
.method public processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "token"    # Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .prologue
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 54
    const/4 v1, 0x0

    .line 56
    .local v1, "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser$1;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 137
    invoke-super {p0, p1, p2}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 140
    :goto_0
    return-object v2

    .line 59
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_1

    .line 60
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    :cond_0
    :goto_1
    move-object v2, v1

    .line 140
    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 63
    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto :goto_1

    .line 69
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_2

    .line 70
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto :goto_1

    .line 72
    :cond_2
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "arg":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "root_calendar_event;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    new-instance v1, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    invoke-direct {v1, v2, v5}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    move-object v2, v1

    .line 76
    check-cast v2, Lorg/apache/lucene/facet/search/DrillDownQuery;

    new-array v3, v4, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    new-instance v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 78
    if-eqz p2, :cond_0

    .line 79
    new-instance v2, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 86
    .end local v0    # "arg":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_3

    .line 87
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto/16 :goto_1

    .line 89
    :cond_3
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 90
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v0

    .line 91
    .restart local v0    # "arg":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "root_person;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 92
    new-instance v1, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    invoke-direct {v1, v2, v5}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    move-object v2, v1

    .line 93
    check-cast v2, Lorg/apache/lucene/facet/search/DrillDownQuery;

    new-array v3, v4, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    new-instance v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 95
    if-eqz p2, :cond_0

    .line 96
    new-instance v2, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 103
    .end local v0    # "arg":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_4

    .line 104
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto/16 :goto_1

    .line 106
    :cond_4
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v0

    .line 108
    .restart local v0    # "arg":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "root_scene_type;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    new-instance v1, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    invoke-direct {v1, v2, v5}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    move-object v2, v1

    .line 110
    check-cast v2, Lorg/apache/lucene/facet/search/DrillDownQuery;

    new-array v3, v4, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    new-instance v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 112
    if-eqz p2, :cond_0

    .line 113
    new-instance v2, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 120
    .end local v0    # "arg":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_5

    .line 121
    new-instance v1, Lorg/apache/lucene/search/WildcardQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto/16 :goto_1

    .line 123
    :cond_5
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v2

    sget-object v3, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v2, v3, :cond_0

    .line 124
    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v0

    .line 125
    .restart local v0    # "arg":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "root_subscene_type;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    new-instance v1, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    invoke-direct {v1, v2, v5}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .restart local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    move-object v2, v1

    .line 127
    check-cast v2, Lorg/apache/lucene/facet/search/DrillDownQuery;

    new-array v3, v4, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    new-instance v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 129
    if-eqz p2, :cond_0

    .line 130
    new-instance v2, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    invoke-virtual {p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
