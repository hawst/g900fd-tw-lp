.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;
.super Ljava/lang/Object;
.source "DCMDocumentImageColumns.java"

# interfaces
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMBaseColumns;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifOrientation;,
        Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMExifFlash;,
        Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns$DCMSceneType;
    }
.end annotation


# static fields
.field public static final FIELD_CALENDAR_EVENT:Ljava/lang/String; = "calendar_event"

.field public static final FIELD_EVENT_URI:Ljava/lang/String; = "event_uri"

.field public static final FIELD_FACECOUNT:Ljava/lang/String; = "faceCount"

.field public static final FIELD_LATITUDE:Ljava/lang/String; = "location__y"

.field public static final FIELD_LONGITUDE:Ljava/lang/String; = "location__x"

.field public static final FIELD_MODIFICATION_DATE:Ljava/lang/String; = "modification_date"

.field public static final FIELD_NAMED_LOCATION:Ljava/lang/String; = "named_location"

.field public static final FIELD_PERSON_NAME:Ljava/lang/String; = "person_names"

.field public static final FIELD_RADIUS_KM:Ljava/lang/String; = "radiuskm"

.field public static final FIELD_SCENE_TYPE:Ljava/lang/String; = "scene_type"

.field public static final FIELD_SUBSCENE_TYPE:Ljava/lang/String; = "subscene_type"

.field public static final IMAGE_FULL_PROJECTION:[Ljava/lang/String;

.field public static final URI:Landroid/net/Uri;

.field public static final URI_STRING:Ljava/lang/String; = "content://com.sec.dcm.provider.DCMProvider.data/imagedocument"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/imagedocument"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;->URI:Landroid/net/Uri;

    .line 53
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "faceCount"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "location__x"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "location__y"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "event_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "named_location"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "person_names"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "calendar_event"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "scene_type"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "subscene_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;->IMAGE_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
