.class public abstract Lcom/sec/dcm/provider/db/tokenparser/TokenParser;
.super Ljava/lang/Object;
.source "TokenParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/tokenparser/TokenParser$1;,
        Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;
    }
.end annotation


# static fields
.field public static final MIN_CREATION_DATE:J

.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method private getMidNightTimeInMS()J
    .locals 7

    .prologue
    const/16 v6, 0x3b

    .line 494
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 495
    .local v0, "c":Ljava/util/Calendar;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 496
    .local v1, "now":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 498
    const/16 v4, 0xb

    const/16 v5, 0x18

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 499
    const/16 v4, 0xc

    invoke-virtual {v0, v4, v6}, Ljava/util/Calendar;->set(II)V

    .line 500
    const/16 v4, 0xd

    invoke-virtual {v0, v4, v6}, Ljava/util/Calendar;->set(II)V

    .line 501
    const/16 v4, 0xe

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 502
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 503
    .local v2, "midnightMax":J
    return-wide v2
.end method


# virtual methods
.method public processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;
    .locals 16
    .param p1, "token"    # Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .prologue
    .line 375
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    const/4 v4, 0x0

    .line 376
    .local v4, "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v5, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$1;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v12

    aget v5, v5, v12

    packed-switch v5, :pswitch_data_0

    .line 487
    sget-object v5, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->TAG:Ljava/lang/String;

    const-string v12, "Not processing the token:"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v5, v12, v13}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    :cond_0
    :goto_0
    return-object v4

    .line 378
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_1

    .line 379
    new-instance v4, Lorg/apache/lucene/search/WildcardQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    const-string v12, "title_lower"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_0

    .line 381
    new-instance v4, Lorg/apache/lucene/search/TermQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .line 386
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_2

    .line 387
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 388
    .local v8, "date":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 389
    goto :goto_0

    .end local v8    # "date":J
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_3

    .line 390
    invoke-direct/range {p0 .. p0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->getMidNightTimeInMS()J

    move-result-wide v10

    .line 391
    .local v10, "midnightMax":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 393
    goto/16 :goto_0

    .end local v10    # "midnightMax":J
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_4

    .line 394
    invoke-direct/range {p0 .. p0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->getMidNightTimeInMS()J

    move-result-wide v10

    .line 395
    .restart local v10    # "midnightMax":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 397
    goto/16 :goto_0

    .end local v10    # "midnightMax":J
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_5

    .line 398
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 400
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_0

    .line 401
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 408
    :pswitch_2
    new-instance v4, Lorg/apache/lucene/search/TermQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 409
    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto/16 :goto_0

    .line 412
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 413
    .local v6, "bucket_id":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 414
    goto/16 :goto_0

    .line 417
    .end local v6    # "bucket_id":J
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_6

    .line 418
    new-instance v4, Lorg/apache/lucene/search/WildcardQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto/16 :goto_0

    .line 419
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_0

    .line 420
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v2

    .line 421
    .local v2, "arg":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "root_named_location;"

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 422
    new-instance v4, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v5}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    const/4 v12, 0x0

    invoke-direct {v4, v5, v12}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    move-object v5, v4

    .line 423
    check-cast v5, Lorg/apache/lucene/facet/search/DrillDownQuery;

    const/4 v12, 0x1

    new-array v12, v12, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const/4 v13, 0x0

    new-instance v14, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v15, ";"

    invoke-virtual {v2, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v14, v12, v13

    invoke-virtual {v5, v12}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 424
    if-eqz p2, :cond_0

    .line 425
    new-instance v5, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v5, v12, v13}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 431
    .end local v2    # "arg":Ljava/lang/String;
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_7

    .line 432
    new-instance v4, Lorg/apache/lucene/search/WildcardQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/index/Term;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    goto/16 :goto_0

    .line 433
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_0

    .line 434
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v2

    .line 435
    .restart local v2    # "arg":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "root_user_tag;"

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 436
    new-instance v4, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v5}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    const/4 v12, 0x0

    invoke-direct {v4, v5, v12}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .restart local v4    # "atomicQuery":Lorg/apache/lucene/search/Query;
    move-object v5, v4

    .line 437
    check-cast v5, Lorg/apache/lucene/facet/search/DrillDownQuery;

    const/4 v12, 0x1

    new-array v12, v12, [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const/4 v13, 0x0

    new-instance v14, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v15, ";"

    invoke-virtual {v2, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    aput-object v14, v12, v13

    invoke-virtual {v5, v12}, Lorg/apache/lucene/facet/search/DrillDownQuery;->add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V

    .line 438
    if-eqz p2, :cond_0

    .line 439
    new-instance v5, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v5, v12, v13}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 446
    .end local v2    # "arg":Ljava/lang/String;
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_8

    .line 447
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-long v2, v5

    .line 448
    .local v2, "arg":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 450
    goto/16 :goto_0

    .end local v2    # "arg":J
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_9

    .line 451
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    const-wide v14, 0x7fffffffffffffffL

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 453
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_a

    .line 454
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    const-wide v14, 0x7fffffffffffffffL

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 456
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_b

    .line 457
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 459
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_0

    .line 460
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 466
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_c

    .line 467
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 468
    .local v2, "arg":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 470
    goto/16 :goto_0

    .end local v2    # "arg":I
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_d

    .line 471
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const v13, 0x7fffffff

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 473
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_e

    .line 474
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const v13, 0x7fffffff

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 476
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_f

    .line 477
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 479
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v5

    sget-object v12, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    if-ne v5, v12, :cond_0

    .line 480
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v5

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v5, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    goto/16 :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method
