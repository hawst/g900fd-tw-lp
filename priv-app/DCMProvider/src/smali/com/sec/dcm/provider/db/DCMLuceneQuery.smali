.class public final Lcom/sec/dcm/provider/db/DCMLuceneQuery;
.super Ljava/lang/Object;
.source "DCMLuceneQuery.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mCategoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Lorg/apache/lucene/search/Filter;

.field private mQuery:Lorg/apache/lucene/search/Query;

.field private mSelectedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Filter;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    .local p4, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mQuery:Lorg/apache/lucene/search/Query;

    .line 41
    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mFilter:Lorg/apache/lucene/search/Filter;

    .line 42
    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mCategoryList:Ljava/util/ArrayList;

    .line 44
    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mSelectedList:Ljava/util/ArrayList;

    .line 48
    iput-object p1, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mQuery:Lorg/apache/lucene/search/Query;

    .line 49
    iput-object p2, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mFilter:Lorg/apache/lucene/search/Filter;

    .line 50
    iput-object p3, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mCategoryList:Ljava/util/ArrayList;

    .line 51
    iput-object p4, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mSelectedList:Ljava/util/ArrayList;

    .line 52
    return-void
.end method

.method public static create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    .locals 2
    .param p0, "query"    # Lorg/apache/lucene/search/Query;
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Filter;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lcom/sec/dcm/provider/db/DCMLuceneQuery;"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    new-instance v0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public static create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    .locals 1
    .param p0, "query"    # Lorg/apache/lucene/search/Query;
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Filter;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lcom/sec/dcm/provider/db/DCMLuceneQuery;"
        }
    .end annotation

    .prologue
    .line 78
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    .local p3, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    new-instance v0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0
.end method


# virtual methods
.method public getCategoryList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mCategoryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFilter()Lorg/apache/lucene/search/Filter;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mFilter:Lorg/apache/lucene/search/Filter;

    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mQuery:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public getSelectedList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->mSelectedList:Ljava/util/ArrayList;

    return-object v0
.end method
