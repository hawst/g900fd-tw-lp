.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMBaseColumns;
.super Ljava/lang/Object;
.source "DCMBaseColumns.java"


# static fields
.field public static final FIELD_BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final FIELD_CREATION_DATE:Ljava/lang/String; = "date"

.field public static final FIELD_ID:Ljava/lang/String; = "id"

.field public static final FIELD_MIME:Ljava/lang/String; = "mimetype"

.field public static final FIELD_MODIFICATION_DATE:Ljava/lang/String; = "modification_date"

.field public static final FIELD_PATH:Ljava/lang/String; = "path"

.field public static final FIELD_TITLE:Ljava/lang/String; = "title"

.field public static final FIELD_URI:Ljava/lang/String; = "uri"

.field public static final FIELD_USER_TAGS:Ljava/lang/String; = "user_tags"
