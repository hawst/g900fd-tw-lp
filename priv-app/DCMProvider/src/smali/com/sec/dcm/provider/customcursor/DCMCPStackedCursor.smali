.class public Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;
.super Landroid/database/AbstractCursor;
.source "DCMCPStackedCursor.java"


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mCursors:[Landroid/database/Cursor;

.field private mObserver:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursors"    # [Landroid/database/Cursor;

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 53
    new-instance v1, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor$1;

    invoke-direct {v1, p0}, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor$1;-><init>(Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;)V

    iput-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mObserver:Landroid/database/DataSetObserver;

    .line 70
    iput-object p1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    .line 71
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 73
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_1

    .line 79
    :cond_1
    return-void
.end method

.method static synthetic access$002(Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mPos:I

    return p1
.end method

.method static synthetic access$102(Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mPos:I

    return p1
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 215
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 216
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 217
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-nez v2, :cond_0

    .line 216
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_0
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 221
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 222
    return-void
.end method

.method public deactivate()V
    .locals 3

    .prologue
    .line 204
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 205
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 206
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 207
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->deactivate()V

    .line 205
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->deactivate()V

    .line 211
    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 97
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 7

    .prologue
    .line 287
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 288
    .local v3, "merge_bundle":Landroid/os/Bundle;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 290
    .local v5, "tables":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v2, v6

    .line 291
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 292
    iget-object v6, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v6, v6, v1

    if-nez v6, :cond_0

    .line 291
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296
    :cond_0
    iget-object v6, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v6, v6, v1

    invoke-interface {v6}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 297
    .local v0, "b":Landroid/os/Bundle;
    const-string v6, "table_name"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 300
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 301
    .local v4, "table_names":Ljava/lang/String;
    const-string v6, "table_name"

    invoke-virtual {v3, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    return-object v3
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 1
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 134
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 226
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 227
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 228
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 227
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_1
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 246
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 247
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 248
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 249
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 247
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 252
    :cond_1
    return-void
.end method

.method public requery()Z
    .locals 3

    .prologue
    .line 268
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 269
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 270
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 269
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274
    :cond_1
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->requery()Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    const/4 v2, 0x0

    .line 279
    :goto_1
    return v2

    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 310
    const/4 v4, 0x0

    .line 311
    .local v4, "targetTableName":Ljava/lang/String;
    const-string v5, "table_name"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 313
    if-eqz v4, :cond_2

    .line 314
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v2, v5

    .line 315
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 316
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v5, v5, v1

    if-nez v5, :cond_1

    .line 315
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 320
    :cond_1
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v5, v5, v1

    invoke-interface {v5}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 321
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "table_name"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 322
    .local v3, "table_name":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 323
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 325
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v5, v5, v1

    iput-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    .line 327
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 334
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "table_name":Ljava/lang/String;
    :goto_1
    return-object v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 236
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 237
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 238
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 239
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 237
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_1
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 256
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 257
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 258
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 259
    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 257
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_1
    return-void
.end method
