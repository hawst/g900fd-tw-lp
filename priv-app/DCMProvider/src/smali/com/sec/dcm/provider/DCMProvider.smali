.class public Lcom/sec/dcm/provider/DCMProvider;
.super Landroid/content/ContentProvider;
.source "DCMProvider.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.dcm.provider.DCMProvider.data"

.field private static final TAG:Ljava/lang/String;

.field public static final _DCM_ALL_DOCUMENT_:I = 0x9

.field public static final _DCM_CATEGORY_:I = 0x4

.field public static final _DCM_IMAGE_AND_CATEGORY_:I = 0x5

.field public static final _DCM_IMAGE_DOCUMENT_:I = 0x1

.field public static final _DCM_IMAGE_MEDIA_ID_:I = 0x6

.field public static final _DCM_PENDING_PROCESSING_:I = 0xc

.field public static final _DCM_VIDEO_DOCUMENT_:I = 0x2

.field public static final _DCM_VIDEO_MEDIA_ID_:I = 0x7

.field private static final mMediaStateReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

.field private static mProviderRefCount:I

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 72
    const-class v0, Lcom/sec/dcm/provider/DCMProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    .line 86
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 99
    const/4 v0, 0x0

    sput v0, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    .line 105
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string v2, "imagedocument"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 106
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string/jumbo v2, "videodocument"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string v2, "category"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 111
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string v2, "imagedocument_n_category"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string v2, "imagedocument/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 113
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string/jumbo v2, "videodocument/#"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string v2, "alldocument"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.dcm.provider.DCMProvider.data"

    const-string v2, "pendingOSC"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 124
    new-instance v0, Lcom/sec/dcm/provider/DCMProvider$1;

    invoke-direct {v0}, Lcom/sec/dcm/provider/DCMProvider$1;-><init>()V

    sput-object v0, Lcom/sec/dcm/provider/DCMProvider;->mMediaStateReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getDefaultProjection(I)[Ljava/lang/String;
    .locals 1
    .param p1, "table"    # I

    .prologue
    .line 416
    packed-switch p1, :pswitch_data_0

    .line 438
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 418
    :pswitch_1
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;->IMAGE_FULL_PROJECTION:[Ljava/lang/String;

    goto :goto_0

    .line 420
    :pswitch_2
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentVideoColumns;->VIDEO_FULL_PROJECTION:[Ljava/lang/String;

    goto :goto_0

    .line 428
    :pswitch_3
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->CATEGORY_FULL_PROJECTION:[Ljava/lang/String;

    goto :goto_0

    .line 430
    :pswitch_4
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageNCategoryColumns;->IMAGE_N_CATEGORY_FULL_PROJECTION:[Ljava/lang/String;

    goto :goto_0

    .line 432
    :pswitch_5
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->FULL_PROJECTION:[Ljava/lang/String;

    goto :goto_0

    .line 434
    :pswitch_6
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->PENDING_FULL_PROJECTION:[Ljava/lang/String;

    goto :goto_0

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 142
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v1, "delete Not Supported"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    return v3
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 443
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 444
    sget v1, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    .line 445
    sget-object v1, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v2, "finalize - Provider Ref Count:"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    sget v4, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, " @this:"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 447
    sget v1, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    if-nez v1, :cond_0

    .line 448
    sget-object v1, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v2, "Provider Ref Count is 0, So Performing real Provider DeInit"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 450
    .local v0, "c":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-eqz v0, :cond_0

    .line 451
    sget-object v1, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v2, "finalize DCMConfig onConfigDestroyed Called"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getIntentReceiver()Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/DCMProvider;->mMediaStateReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->unRegisterListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;)V

    .line 453
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->onConfigDestroyed()V

    .line 456
    .end local v0    # "c":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    :cond_0
    return-void
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "contentValues"    # Landroid/content/ContentValues;

    .prologue
    .line 153
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v1, "insert Not Supported"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 159
    sget v1, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    .line 160
    sget-object v1, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v2, "onCreate this Instance ="

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v6

    const-string v4, " Provider Ref Count:"

    aput-object v4, v3, v7

    const/4 v4, 0x2

    sget v5, Lcom/sec/dcm/provider/DCMProvider;->mProviderRefCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 164
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-nez v0, :cond_0

    .line 165
    new-instance v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .end local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    invoke-virtual {p0}, Lcom/sec/dcm/provider/DCMProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/configuration/DCMConfig;-><init>(Landroid/content/Context;)V

    .line 168
    .restart local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0, v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->bind(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 171
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->createAndStart(Lcom/samsung/dcm/framework/configuration/DcmTransport;Lcom/samsung/dcm/framework/configuration/DCMConfig;)V

    .line 172
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getIntentReceiver()Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    move-result-object v1

    sget-object v2, Lcom/sec/dcm/provider/DCMProvider;->mMediaStateReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    new-array v3, v7, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V

    .line 181
    :goto_0
    sget-object v1, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v2, "onCreate end"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    return v7

    .line 177
    :cond_0
    sget-object v1, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v2, "onCreate is called multiple times, so Ignoring, DCMConfig is alreadt existing(This is Application Framework Problem) "

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 5
    .param p1, "level"    # I

    .prologue
    .line 460
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->onTrimMemory(I)V

    .line 461
    sget-object v0, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v1, "onTrimMemory Level:"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 462
    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 36
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 269
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "query for uri:"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " selection:"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " projection:"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 272
    .local v28, "startQuery":J
    const-class v31, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static/range {v31 .. v31}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 275
    .local v10, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getSuccessState()Z

    move-result v31

    if-nez v31, :cond_2

    .line 276
    :cond_0
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "DCM Initialization was not success, so cannot proceed"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    const/4 v11, 0x0

    .line 411
    :cond_1
    :goto_0
    return-object v11

    .line 280
    :cond_2
    invoke-virtual {v10}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v30

    .line 281
    .local v30, "transport":Lcom/samsung/dcm/framework/configuration/DcmTransport;
    if-eqz v30, :cond_3

    .line 282
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v13

    .line 283
    .local v13, "dcmController":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    if-eqz v13, :cond_3

    .line 284
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->postOscStartIntent(Z)V

    .line 288
    .end local v13    # "dcmController":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    :cond_3
    const/16 v27, -0x1

    .line 289
    .local v27, "table":I
    if-eqz p1, :cond_4

    .line 290
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v27

    .line 293
    :cond_4
    const/16 v31, -0x1

    move/from16 v0, v27

    move/from16 v1, v31

    if-ne v0, v1, :cond_5

    .line 294
    const/4 v11, 0x0

    goto :goto_0

    .line 297
    :cond_5
    if-nez p2, :cond_6

    .line 298
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/sec/dcm/provider/DCMProvider;->getDefaultProjection(I)[Ljava/lang/String;

    move-result-object p2

    .line 306
    :cond_6
    const/16 v31, 0x5

    move/from16 v0, v27

    move/from16 v1, v31

    if-ne v0, v1, :cond_7

    .line 308
    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v31

    const-string v32, "--------"

    invoke-interface/range {v31 .. v32}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_7

    .line 310
    const/4 v11, 0x0

    goto :goto_0

    .line 315
    :cond_7
    if-eqz p3, :cond_9

    .line 316
    if-eqz p4, :cond_8

    .line 317
    move-object/from16 v5, p4

    .local v5, "arr$":[Ljava/lang/String;
    array-length v0, v5

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_1
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_8

    aget-object v4, v5, v22

    .line 318
    .local v4, "arg":Ljava/lang/String;
    const-string v31, "\\?"

    move-object/from16 v0, p3

    move-object/from16 v1, v31

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 317
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 321
    .end local v4    # "arg":Ljava/lang/String;
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v22    # "i$":I
    .end local v24    # "len$":I
    :cond_8
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, " >> selection after replacing arguments:"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    aput-object p3, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    :cond_9
    const/4 v11, 0x0

    .line 324
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 325
    .local v14, "dcmQuery":Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    new-instance v6, Lcom/sec/dcm/provider/db/DCMQueryBuilder;

    move/from16 v0, v27

    move-object/from16 v1, p3

    invoke-direct {v6, v0, v1}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;-><init>(ILjava/lang/String;)V

    .line 326
    .local v6, "builder":Lcom/sec/dcm/provider/db/DCMQueryBuilder;
    invoke-virtual {v6}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->build()Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v14

    .line 327
    const/16 v25, 0x0

    .line 329
    .local v25, "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    if-eqz v14, :cond_a

    .line 330
    sparse-switch v27, :sswitch_data_0

    .line 403
    :cond_a
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v32

    sub-long v20, v32, v28

    .line 405
    .local v20, "endQuery":J
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "[DCM_Performance] Real searchTime :"

    const/16 v33, 0x2

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    aput-object v35, v33, v34

    const/16 v34, 0x1

    const-string v35, "ms"

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 406
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "[DCM] end query"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    if-eqz v11, :cond_1

    .line 408
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, ">> end query result count :"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 337
    .end local v20    # "endQuery":J
    :sswitch_0
    :try_start_0
    new-instance v26, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    move-object/from16 v2, v30

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v14, v2, v3}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>([Ljava/lang/String;Lcom/sec/dcm/provider/db/DCMLuceneQuery;Lcom/samsung/dcm/framework/configuration/DcmTransport;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .local v26, "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_1
    new-instance v12, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;

    move-object/from16 v0, v26

    invoke-direct {v12, v0}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;-><init>(Lcom/sec/dcm/provider/db/DCMQueryRunner;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9

    .end local v11    # "cursor":Landroid/database/Cursor;
    .local v12, "cursor":Landroid/database/Cursor;
    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    move-object v11, v12

    .line 341
    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    goto :goto_2

    .line 339
    :catch_0
    move-exception v19

    .line 340
    .local v19, "e":Ljava/io/IOException;
    :goto_3
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "Exception in creating query runner"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 345
    .end local v19    # "e":Ljava/io/IOException;
    :sswitch_1
    :try_start_2
    new-instance v26, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    move-object/from16 v2, v30

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v14, v2, v3}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>([Ljava/lang/String;Lcom/sec/dcm/provider/db/DCMLuceneQuery;Lcom/samsung/dcm/framework/configuration/DcmTransport;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 346
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_3
    invoke-virtual/range {v26 .. v26}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getCategories()Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_7

    move-result-object v11

    .line 350
    if-eqz v26, :cond_f

    .line 351
    invoke-virtual/range {v26 .. v26}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_2

    .line 347
    :catch_1
    move-exception v19

    .line 348
    .restart local v19    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_4
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "Exception in creating query runner"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 350
    if-eqz v25, :cond_a

    .line 351
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    goto/16 :goto_2

    .line 350
    .end local v19    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v31

    :goto_5
    if-eqz v25, :cond_b

    .line 351
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    :cond_b
    throw v31

    .line 356
    :sswitch_2
    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v31

    const-string v32, "--------"

    invoke-interface/range {v31 .. v32}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v15

    .line 358
    .local v15, "delimiterIndex":I
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v31, v0

    sub-int v31, v31, v15

    add-int/lit8 v8, v31, -0x1

    .line 361
    .local v8, "catProjLength":I
    new-array v0, v15, [Ljava/lang/String;

    move-object/from16 v18, v0

    .line 362
    .local v18, "docProjection":[Ljava/lang/String;
    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v31

    move-object/from16 v2, v18

    move/from16 v3, v32

    invoke-static {v0, v1, v2, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 365
    new-array v9, v8, [Ljava/lang/String;

    .line 366
    .local v9, "catProjection":[Ljava/lang/String;
    add-int/lit8 v31, v15, 0x1

    const/16 v32, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-static {v0, v1, v9, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 368
    const/16 v16, 0x0

    .line 370
    .local v16, "docCursor":Landroid/database/Cursor;
    :try_start_5
    new-instance v26, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    move-object/from16 v2, v30

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v14, v2, v3}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>([Ljava/lang/String;Lcom/sec/dcm/provider/db/DCMLuceneQuery;Lcom/samsung/dcm/framework/configuration/DcmTransport;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 371
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_6
    new-instance v17, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;-><init>(Lcom/sec/dcm/provider/db/DCMQueryRunner;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 372
    .end local v16    # "docCursor":Landroid/database/Cursor;
    .local v17, "docCursor":Landroid/database/Cursor;
    :try_start_7
    new-instance v25, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    move-object/from16 v2, p5

    invoke-direct {v0, v9, v14, v1, v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>([Ljava/lang/String;Lcom/sec/dcm/provider/db/DCMLuceneQuery;Lcom/samsung/dcm/framework/configuration/DcmTransport;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 373
    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_8
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getCategories()Landroid/database/Cursor;

    move-result-object v7

    .line 374
    .local v7, "catCursor":Landroid/database/Cursor;
    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v23, v0

    const/16 v31, 0x0

    aput-object v17, v23, v31

    const/16 v31, 0x1

    aput-object v7, v23, v31

    .line 377
    .local v23, "imgCatCursor":[Landroid/database/Cursor;
    new-instance v12, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;

    move-object/from16 v0, v23

    invoke-direct {v12, v0}, Lcom/sec/dcm/provider/customcursor/DCMCPStackedCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 384
    .end local v11    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    if-eqz v25, :cond_10

    .line 385
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    move-object/from16 v16, v17

    .end local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    move-object v11, v12

    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    goto/16 :goto_2

    .line 378
    .end local v7    # "catCursor":Landroid/database/Cursor;
    .end local v23    # "imgCatCursor":[Landroid/database/Cursor;
    :catch_2
    move-exception v19

    .line 379
    .restart local v19    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_9
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "Exception in creating query runner"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 380
    if-eqz v16, :cond_c

    .line 381
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 384
    :cond_c
    if-eqz v25, :cond_a

    .line 385
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    goto/16 :goto_2

    .line 384
    .end local v19    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v31

    :goto_7
    if-eqz v25, :cond_d

    .line 385
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    :cond_d
    throw v31

    .line 391
    .end local v8    # "catProjLength":I
    .end local v9    # "catProjection":[Ljava/lang/String;
    .end local v15    # "delimiterIndex":I
    .end local v16    # "docCursor":Landroid/database/Cursor;
    .end local v18    # "docProjection":[Ljava/lang/String;
    :sswitch_3
    :try_start_a
    new-instance v26, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    move-object/from16 v2, v30

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v14, v2, v3}, Lcom/sec/dcm/provider/db/DCMQueryRunner;-><init>([Ljava/lang/String;Lcom/sec/dcm/provider/db/DCMLuceneQuery;Lcom/samsung/dcm/framework/configuration/DcmTransport;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 392
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :try_start_b
    invoke-virtual/range {v26 .. v26}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getPendingCount()Landroid/database/Cursor;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-result-object v11

    .line 396
    if-eqz v26, :cond_f

    .line 397
    invoke-virtual/range {v26 .. v26}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_2

    .line 393
    :catch_3
    move-exception v19

    .line 394
    .restart local v19    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_c
    sget-object v31, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v32, "Exception in creating query runner"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 396
    if-eqz v25, :cond_a

    .line 397
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    goto/16 :goto_2

    .line 396
    .end local v19    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v31

    :goto_9
    if-eqz v25, :cond_e

    .line 397
    invoke-virtual/range {v25 .. v25}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    :cond_e
    throw v31

    .line 396
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catchall_3
    move-exception v31

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto :goto_9

    .line 393
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_4
    move-exception v19

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto :goto_8

    .line 384
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v8    # "catProjLength":I
    .restart local v9    # "catProjection":[Ljava/lang/String;
    .restart local v15    # "delimiterIndex":I
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    .restart local v18    # "docProjection":[Ljava/lang/String;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catchall_4
    move-exception v31

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto :goto_7

    .end local v16    # "docCursor":Landroid/database/Cursor;
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catchall_5
    move-exception v31

    move-object/from16 v16, v17

    .end local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto :goto_7

    .end local v16    # "docCursor":Landroid/database/Cursor;
    .restart local v17    # "docCursor":Landroid/database/Cursor;
    :catchall_6
    move-exception v31

    move-object/from16 v16, v17

    .end local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    goto :goto_7

    .line 378
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_5
    move-exception v19

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_6

    .end local v16    # "docCursor":Landroid/database/Cursor;
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_6
    move-exception v19

    move-object/from16 v16, v17

    .end local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_6

    .end local v16    # "docCursor":Landroid/database/Cursor;
    .restart local v17    # "docCursor":Landroid/database/Cursor;
    :catch_7
    move-exception v19

    move-object/from16 v16, v17

    .end local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    goto/16 :goto_6

    .line 350
    .end local v8    # "catProjLength":I
    .end local v9    # "catProjection":[Ljava/lang/String;
    .end local v15    # "delimiterIndex":I
    .end local v16    # "docCursor":Landroid/database/Cursor;
    .end local v18    # "docProjection":[Ljava/lang/String;
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catchall_7
    move-exception v31

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_5

    .line 347
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_8
    move-exception v19

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_4

    .line 339
    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :catch_9
    move-exception v19

    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_3

    .end local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    :cond_f
    move-object/from16 v25, v26

    .end local v26    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    .restart local v25    # "runner":Lcom/sec/dcm/provider/db/DCMQueryRunner;
    goto/16 :goto_2

    .end local v11    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "catCursor":Landroid/database/Cursor;
    .restart local v8    # "catProjLength":I
    .restart local v9    # "catProjection":[Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v15    # "delimiterIndex":I
    .restart local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v18    # "docProjection":[Ljava/lang/String;
    .restart local v23    # "imgCatCursor":[Landroid/database/Cursor;
    :cond_10
    move-object/from16 v16, v17

    .end local v17    # "docCursor":Landroid/database/Cursor;
    .restart local v16    # "docCursor":Landroid/database/Cursor;
    move-object v11, v12

    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    goto/16 :goto_2

    .line 330
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0xc -> :sswitch_3
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 27
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "contentValues"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 188
    sget-object v22, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v23, "update:selction="

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object p3, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    const-class v22, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static/range {v22 .. v22}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 192
    .local v6, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getSuccessState()Z

    move-result v22

    if-nez v22, :cond_2

    .line 193
    :cond_0
    sget-object v22, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v23, "DCM Initialization was not success, so cannot proceed"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    const/4 v14, 0x0

    .line 261
    :cond_1
    :goto_0
    return v14

    .line 198
    :cond_2
    if-eqz p3, :cond_3

    .line 199
    if-eqz p4, :cond_3

    .line 200
    move-object/from16 v5, p4

    .local v5, "arr$":[Ljava/lang/String;
    array-length v11, v5

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v11, :cond_3

    aget-object v4, v5, v10

    .line 201
    .local v4, "arg":Ljava/lang/String;
    const-string v22, "\\?"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 200
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 206
    .end local v4    # "arg":Ljava/lang/String;
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    :cond_3
    sget-object v22, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v23, "update , selection:"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object p3, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    sget-object v22, Lcom/sec/dcm/provider/DCMProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v18

    .line 208
    .local v18, "table":I
    const/4 v14, 0x0

    .line 210
    .local v14, "ret":I
    invoke-virtual {v6}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v19

    .line 211
    .local v19, "transport":Lcom/samsung/dcm/framework/configuration/DcmTransport;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v12

    .line 212
    .local v12, "nrtManager":Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 213
    .local v16, "startUpdate":J
    const/16 v20, 0x0

    .line 215
    .local v20, "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    const-string v13, ""

    .line 217
    .local v13, "quickInsertAppend":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    const-string v23, "imagedocument"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 218
    const-string v22, ";images"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 227
    :cond_4
    :goto_2
    invoke-static {}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isAlive()Z

    move-result v15

    .line 231
    .local v15, "sendPauseResume":Z
    if-eqz v15, :cond_5

    .line 232
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v22

    sget-object v23, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_UPDATE:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 236
    :cond_5
    new-instance v21, Lcom/sec/dcm/provider/db/DCMUpdateRunner;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;-><init>(Lcom/samsung/dcm/framework/configuration/DcmTransport;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    .end local v20    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    .local v21, "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    :try_start_1
    move-object/from16 v0, v21

    move/from16 v1, v18

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3, v13}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->update(ILandroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v14

    .line 242
    if-eqz v21, :cond_6

    .line 243
    invoke-virtual/range {v21 .. v21}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->close()V

    .line 245
    :cond_6
    if-eqz v15, :cond_7

    .line 246
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v22

    sget-object v23, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_UPDATE:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 255
    :cond_7
    invoke-virtual {v12}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    sub-long v8, v22, v16

    .line 258
    .local v8, "endUpdate":J
    sget-object v22, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v23, "[DCM_Performance] Real updateTime : ["

    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "]ms"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "No of Docs Updated:"

    aput-object v26, v24, v25

    const/16 v25, 0x3

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 220
    .end local v8    # "endUpdate":J
    .end local v15    # "sendPauseResume":Z
    .end local v21    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    .restart local v20    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, "videodocument"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 221
    const-string v22, ";video"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto :goto_2

    .line 238
    .restart local v15    # "sendPauseResume":Z
    :catch_0
    move-exception v7

    .line 239
    .local v7, "e":Ljava/io/IOException;
    :goto_3
    :try_start_2
    sget-object v22, Lcom/sec/dcm/provider/DCMProvider;->TAG:Ljava/lang/String;

    const-string v23, "Exception in update:"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v7}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 242
    if-eqz v20, :cond_9

    .line 243
    invoke-virtual/range {v20 .. v20}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->close()V

    .line 245
    :cond_9
    if-eqz v15, :cond_1

    .line 246
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v22

    sget-object v23, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_UPDATE:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    goto/16 :goto_0

    .line 242
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v22

    :goto_4
    if-eqz v20, :cond_a

    .line 243
    invoke-virtual/range {v20 .. v20}, Lcom/sec/dcm/provider/db/DCMUpdateRunner;->close()V

    .line 245
    :cond_a
    if-eqz v15, :cond_b

    .line 246
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v23

    sget-object v24, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_UPDATE:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    :cond_b
    throw v22

    .line 242
    .end local v20    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    .restart local v21    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    :catchall_1
    move-exception v22

    move-object/from16 v20, v21

    .end local v21    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    .restart local v20    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    goto :goto_4

    .line 238
    .end local v20    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    .restart local v21    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    :catch_1
    move-exception v7

    move-object/from16 v20, v21

    .end local v21    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    .restart local v20    # "updateRunner":Lcom/sec/dcm/provider/db/DCMUpdateRunner;
    goto :goto_3
.end method
