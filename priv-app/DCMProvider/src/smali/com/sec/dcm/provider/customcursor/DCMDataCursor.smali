.class public Lcom/sec/dcm/provider/customcursor/DCMDataCursor;
.super Landroid/database/AbstractWindowedCursor;
.source "DCMDataCursor.java"


# static fields
.field private static final NO_COUNT:I = -0x1

.field public static final TAG:Ljava/lang/String;

.field private static final WINDOW_RATIO:I = 0x64

.field public static final WINDOW_SIZE:I = 0x12c


# instance fields
.field private mColumnNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mColumns:[Ljava/lang/String;

.field private mCount:I

.field private mQueryRunner:Lcom/sec/dcm/provider/db/DCMQueryRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/dcm/provider/db/DCMQueryRunner;)V
    .locals 6
    .param p1, "runner"    # Lcom/sec/dcm/provider/db/DCMQueryRunner;

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/database/AbstractWindowedCursor;-><init>()V

    .line 59
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    .line 70
    iput-object p1, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mQueryRunner:Lcom/sec/dcm/provider/db/DCMQueryRunner;

    .line 71
    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mQueryRunner:Lcom/sec/dcm/provider/db/DCMQueryRunner;

    invoke-virtual {v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getColumnNames()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumns:[Ljava/lang/String;

    .line 73
    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    if-nez v4, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumns:[Ljava/lang/String;

    .line 75
    .local v1, "columns":[Ljava/lang/String;
    array-length v0, v1

    .line 76
    .local v0, "columnCount":I
    new-instance v3, Ljava/util/HashMap;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v3, v0, v4}, Ljava/util/HashMap;-><init>(IF)V

    .line 77
    .local v3, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 78
    aget-object v4, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    :cond_0
    iput-object v3, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    .line 83
    .end local v0    # "columnCount":I
    .end local v1    # "columns":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_1
    return-void
.end method

.method private addRowToWindow(Lorg/apache/lucene/document/Document;Landroid/database/CursorWindow;)V
    .locals 32
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "window"    # Landroid/database/CursorWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 206
    invoke-virtual/range {p2 .. p2}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v23

    .line 208
    .local v23, "status":Z
    const/16 v27, 0x1

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_37

    .line 209
    invoke-virtual/range {p2 .. p2}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v27

    add-int/lit8 v21, v27, -0x1

    .line 210
    .local v21, "row":I
    sget-object v27, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v28, "Row:"

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-static/range {v27 .. v29}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    const/16 v16, 0x0

    .line 213
    .local v16, "iF":Lorg/apache/lucene/index/IndexableField;
    new-instance v9, Ljava/util/ArrayList;

    const-string v27, "categories"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 218
    .local v9, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumns:[Ljava/lang/String;

    .local v6, "arr$":[Ljava/lang/String;
    array-length v0, v6

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    move v15, v14

    .end local v14    # "i$":I
    .local v15, "i$":I
    :goto_0
    move/from16 v0, v18

    if-ge v15, v0, :cond_38

    aget-object v10, v6, v15

    .line 221
    .local v10, "columnName":Ljava/lang/String;
    const-string v27, "id"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 222
    const-string v27, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Number;->longValue()J

    move-result-wide v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-wide/from16 v1, v28

    move/from16 v3, v21

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    .line 218
    .end local v15    # "i$":I
    :cond_0
    :goto_1
    add-int/lit8 v14, v15, 0x1

    .restart local v14    # "i$":I
    move v15, v14

    .end local v14    # "i$":I
    .restart local v15    # "i$":I
    goto :goto_0

    .line 225
    :cond_1
    const-string v27, "title"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 226
    const-string v27, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto :goto_1

    .line 229
    :cond_2
    const-string v27, "date"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 230
    const-string v27, "date"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v11

    .line 231
    .local v11, "date":Lorg/apache/lucene/index/IndexableField;
    if-eqz v11, :cond_3

    invoke-interface {v11}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Number;->longValue()J

    move-result-wide v28

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-wide/from16 v1, v28

    move/from16 v3, v21

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    goto :goto_1

    :cond_3
    const-wide/16 v28, 0x0

    goto :goto_2

    .line 234
    .end local v11    # "date":Lorg/apache/lucene/index/IndexableField;
    :cond_4
    const-string v27, "path"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 235
    const-string v27, "path"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 236
    sget-object v27, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v28, "Path fetched:"

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-static/range {v27 .. v29}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    if-eqz v16, :cond_5

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_5
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_3

    .line 240
    :cond_6
    const-string v27, "uri"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 241
    const-string v27, "uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 244
    :cond_7
    const-string v27, "mimetype"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 245
    const-string v27, "mimetype"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 248
    :cond_8
    const-string v27, "faceCount"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 249
    const-string v27, "faceCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    if-eqz v27, :cond_9

    const-string v27, "faceCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Number;->intValue()I

    move-result v27

    sget-object v28, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->DEFAULT_FACE_COUNT_VALUE:Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_9

    .line 254
    const-string v27, "faceCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Number;->intValue()I

    move-result v27

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-wide/from16 v1, v28

    move/from16 v3, v21

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    goto/16 :goto_1

    .line 259
    :cond_9
    const-wide/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-wide/from16 v1, v28

    move/from16 v3, v21

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    goto/16 :goto_1

    .line 262
    :cond_a
    const-string v27, "location__x"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 263
    const-string v27, "location__x"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    if-eqz v27, :cond_b

    .line 266
    const-string v27, "location__x"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 273
    :cond_b
    const-string v28, "null"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 276
    :cond_c
    const-string v27, "location__y"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 277
    const-string v27, "location__y"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v27

    if-eqz v27, :cond_d

    .line 280
    const-string v27, "location__y"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 287
    :cond_d
    const-string v28, "null"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 290
    :cond_e
    const-string v27, "event_uri"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 291
    new-instance v13, Ljava/util/ArrayList;

    const-string v27, "event_uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 293
    .local v13, "eventUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v12, ""

    .line 294
    .local v12, "eventURI":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .end local v15    # "i$":I
    .local v14, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_10

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 295
    .local v24, "uri":Ljava/lang/String;
    const-string v27, "\\s"

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v25

    .line 296
    .local v25, "uriSplit":[Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_f

    .line 297
    const/16 v27, 0x0

    aget-object v12, v25, v27

    goto :goto_4

    .line 299
    :cond_f
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const/16 v28, 0x0

    aget-object v28, v25, v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_4

    .line 303
    .end local v24    # "uri":Ljava/lang/String;
    .end local v25    # "uriSplit":[Ljava/lang/String;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move/from16 v1, v21

    move/from16 v2, v27

    invoke-virtual {v0, v12, v1, v2}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 306
    .end local v12    # "eventURI":Ljava/lang/String;
    .end local v13    # "eventUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "i$":I
    :cond_11
    const-string v27, "named_location"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_15

    .line 314
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 315
    .local v17, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const-string v19, ""

    .line 317
    .local v19, "named_location":Ljava/lang/String;
    :cond_12
    :goto_5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_14

    .line 318
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 319
    .local v8, "cat":Ljava/lang/String;
    const-string v27, "root_named_location"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 320
    const-string v27, ";"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v27

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 321
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_13

    .line 322
    move-object/from16 v19, v8

    .line 327
    :goto_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .line 325
    :cond_13
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_6

    .line 330
    .end local v8    # "cat":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 332
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v19    # "named_location":Ljava/lang/String;
    :cond_15
    const-string v27, "calendar_event"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_19

    .line 340
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 341
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const-string v7, ""

    .line 342
    .local v7, "calendarEvent":Ljava/lang/String;
    :cond_16
    :goto_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_18

    .line 343
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 344
    .restart local v8    # "cat":Ljava/lang/String;
    const-string v27, "root_calendar_event"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_16

    .line 345
    const-string v27, ";"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v27

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 346
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_17

    .line 347
    move-object v7, v8

    .line 353
    :goto_8
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_7

    .line 350
    :cond_17
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_8

    .line 356
    .end local v8    # "cat":Ljava/lang/String;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move/from16 v1, v21

    move/from16 v2, v27

    invoke-virtual {v0, v7, v1, v2}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 359
    .end local v7    # "calendarEvent":Ljava/lang/String;
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_19
    const-string v27, "person_names"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1d

    .line 367
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 368
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const-string v20, ""

    .line 369
    .local v20, "personName":Ljava/lang/String;
    :cond_1a
    :goto_9
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 370
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 371
    .restart local v8    # "cat":Ljava/lang/String;
    const-string v27, "root_person"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_1a

    .line 372
    const-string v27, ";"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v27

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 373
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_1b

    .line 374
    move-object/from16 v20, v8

    .line 378
    :goto_a
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_9

    .line 376
    :cond_1b
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    goto :goto_a

    .line 381
    .end local v8    # "cat":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 383
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v20    # "personName":Ljava/lang/String;
    :cond_1d
    const-string v27, "user_tags"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_21

    .line 391
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 392
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const-string v26, ""

    .line 393
    .local v26, "userTag":Ljava/lang/String;
    :cond_1e
    :goto_b
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_20

    .line 394
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 395
    .restart local v8    # "cat":Ljava/lang/String;
    const-string v27, "root_user_tag"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_1e

    .line 396
    const-string v27, ";"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v27

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 397
    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_1f

    .line 398
    move-object/from16 v26, v8

    .line 402
    :goto_c
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_b

    .line 400
    :cond_1f
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto :goto_c

    .line 405
    .end local v8    # "cat":Ljava/lang/String;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 407
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v26    # "userTag":Ljava/lang/String;
    :cond_21
    const-string v27, "scene_type"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_25

    .line 415
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 416
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const-string v22, ""

    .line 417
    .local v22, "sceneType":Ljava/lang/String;
    :cond_22
    :goto_d
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_24

    .line 418
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 419
    .restart local v8    # "cat":Ljava/lang/String;
    const-string v27, "root_scene_type"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_22

    .line 420
    const-string v27, ";"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v27

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 421
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_23

    .line 422
    move-object/from16 v22, v8

    .line 427
    :goto_e
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_d

    .line 425
    :cond_23
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    goto :goto_e

    .line 430
    .end local v8    # "cat":Ljava/lang/String;
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 432
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v22    # "sceneType":Ljava/lang/String;
    :cond_25
    const-string v27, "subscene_type"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_29

    .line 440
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 441
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const-string v22, ""

    .line 442
    .restart local v22    # "sceneType":Ljava/lang/String;
    :cond_26
    :goto_f
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_28

    .line 443
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 444
    .restart local v8    # "cat":Ljava/lang/String;
    const-string v27, "root_subscene_type"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_26

    .line 445
    const-string v27, ";"

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v27

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 446
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_27

    .line 447
    move-object/from16 v22, v8

    .line 452
    :goto_10
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_f

    .line 450
    :cond_27
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    goto :goto_10

    .line 455
    .end local v8    # "cat":Ljava/lang/String;
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    .line 457
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v22    # "sceneType":Ljava/lang/String;
    :cond_29
    const-string v27, "artist"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2b

    .line 458
    const-string v27, "artist"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 459
    if-eqz v16, :cond_2a

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_2a
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_11

    .line 462
    :cond_2b
    const-string v27, "album"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2d

    .line 463
    const-string v27, "album"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 464
    if-eqz v16, :cond_2c

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_2c
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_12

    .line 477
    :cond_2d
    const-string v27, "description"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2f

    .line 478
    const-string v27, "description"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 479
    if-eqz v16, :cond_2e

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_2e
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_13

    .line 482
    :cond_2f
    const-string v27, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_31

    .line 483
    const-string v27, "duration"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 484
    if-eqz v16, :cond_30

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Number;->intValue()I

    move-result v27

    :goto_14
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-wide/from16 v1, v28

    move/from16 v3, v21

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    goto/16 :goto_1

    :cond_30
    const/16 v27, -0x1

    goto :goto_14

    .line 487
    :cond_31
    const-string v27, "language"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_33

    .line 488
    const-string v27, "language"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 489
    if-eqz v16, :cond_32

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_32
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_15

    .line 492
    :cond_33
    const-string v27, "resolution"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_35

    .line 493
    const-string v27, "resolution"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 494
    if-eqz v16, :cond_34

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_34
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_16

    .line 497
    :cond_35
    const-string v27, "bucket"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 499
    const-string v27, "bucket"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v16

    .line 500
    if-eqz v16, :cond_36

    invoke-interface/range {v16 .. v16}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v28, v27

    :goto_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    move/from16 v2, v21

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/16 :goto_1

    :cond_36
    const-string v27, "null"

    move-object/from16 v28, v27

    goto :goto_17

    .line 592
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v9    # "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "columnName":Ljava/lang/String;
    .end local v15    # "i$":I
    .end local v16    # "iF":Lorg/apache/lucene/index/IndexableField;
    .end local v18    # "len$":I
    .end local v21    # "row":I
    :cond_37
    sget-object v27, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v28, "Out of memory Error in allocating row to cursor window"

    const/16 v29, 0x0

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    invoke-static/range {v27 .. v29}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 593
    new-instance v27, Ljava/lang/OutOfMemoryError;

    invoke-direct/range {v27 .. v27}, Ljava/lang/OutOfMemoryError;-><init>()V

    throw v27

    .line 595
    .restart local v6    # "arr$":[Ljava/lang/String;
    .restart local v9    # "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v15    # "i$":I
    .restart local v16    # "iF":Lorg/apache/lucene/index/IndexableField;
    .restart local v18    # "len$":I
    .restart local v21    # "row":I
    :cond_38
    return-void
.end method

.method private cursorPickFillWindowStartPosition(I)I
    .locals 2
    .param p1, "cursorPosition"    # I

    .prologue
    .line 202
    add-int/lit8 v0, p1, -0x64

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private fillDocsinWindow(I)V
    .locals 7
    .param p1, "startPos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x12c

    .line 190
    :try_start_0
    iget v5, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    sub-int/2addr v5, p1

    if-ge v5, v4, :cond_0

    iget v5, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    sub-int v4, v5, p1

    .line 191
    .local v4, "windowSize":I
    :cond_0
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mQueryRunner:Lcom/sec/dcm/provider/db/DCMQueryRunner;

    invoke-virtual {v5, p1, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getLuceneDocuments(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 192
    .local v1, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Document;

    .line 193
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-direct {p0, v0, v5}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->addRowToWindow(Lorg/apache/lucene/document/Document;Landroid/database/CursorWindow;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 195
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    .end local v1    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "windowSize":I
    :catch_0
    move-exception v2

    .line 196
    .local v2, "e":Ljava/lang/Throwable;
    sget-object v5, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v6, "Exception in fillDocsinWindow:"

    invoke-static {v5, v6, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 197
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v2}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 199
    .end local v2    # "e":Ljava/lang/Throwable;
    .restart local v1    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "windowSize":I
    :cond_1
    return-void
.end method

.method private fillWindow(I)V
    .locals 2
    .param p1, "reqPos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    const-string v0, "DCM CursorWindow"

    invoke-virtual {p0, v0}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->clearOrCreateWindow(Ljava/lang/String;)V

    .line 180
    iget v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mQueryRunner:Lcom/sec/dcm/provider/db/DCMQueryRunner;

    invoke-virtual {v0}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->run()I

    move-result v0

    iput v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    iget-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumns:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {v0, v1}, Landroid/database/CursorWindow;->setNumColumns(I)Z

    .line 184
    invoke-direct {p0, p1}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->fillDocsinWindow(I)V

    .line 185
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0, p1}, Landroid/database/CursorWindow;->setStartPosition(I)V

    .line 186
    return-void
.end method

.method private processStringFieldList(Lorg/apache/lucene/document/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 599
    const-string v0, ","

    .line 600
    .local v0, "delimiter":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lorg/apache/lucene/document/Document;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 602
    .local v3, "listOfStrings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, ""

    .line 603
    .local v2, "listAsString":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 604
    .local v4, "string":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 605
    move-object v2, v4

    goto :goto_0

    .line 607
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 610
    .end local v4    # "string":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v2, "null"

    .end local v2    # "listAsString":Ljava/lang/String;
    :cond_2
    return-object v2
.end method


# virtual methods
.method protected clearOrCreateWindow(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Landroid/database/CursorWindow;

    invoke-direct {v0, p1}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    .line 176
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->clear()V

    goto :goto_0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 152
    sget-object v0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v1, "Inside close()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 154
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mQueryRunner:Lcom/sec/dcm/provider/db/DCMQueryRunner;

    invoke-virtual {v0}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    .line 155
    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->close()V

    .line 162
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    :cond_0
    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->finalize()V

    .line 167
    return-void

    .line 165
    :catchall_0
    move-exception v0

    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->finalize()V

    throw v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumnNameMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 94
    .local v0, "i":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 97
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mColumns:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 138
    iget v2, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 140
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v2}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->fillWindow(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_0
    sget-object v2, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v3, "getCount="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    iget v1, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mCount:I

    :goto_0
    return v1

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v3, "fill Window thrown excpetion inside getCount()"

    invoke-static {v2, v3, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onMove(II)Z
    .locals 9
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    sget-object v4, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v5, "onMove called with oldPosition:"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    const-string v7, " and newPosition:"

    aput-object v7, v6, v3

    const/4 v7, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v4}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v4

    if-lt p2, v4, :cond_0

    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v4}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v4

    iget-object v5, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v5}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v5

    add-int/2addr v4, v5

    if-lt p2, v4, :cond_1

    .line 118
    :cond_0
    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v4}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v4

    if-ge p2, v4, :cond_2

    .line 119
    invoke-direct {p0, p2}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->cursorPickFillWindowStartPosition(I)I

    move-result v1

    .line 124
    .local v1, "startPos":I
    :goto_0
    :try_start_0
    sget-object v4, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v5, "Calling fill window inside onMove"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    invoke-direct {p0, v1}, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->fillWindow(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    .end local v1    # "startPos":I
    :cond_1
    iput p2, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mPos:I

    move v2, v3

    .line 133
    :goto_1
    return v2

    .line 121
    :cond_2
    move v1, p2

    .restart local v1    # "startPos":I
    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v3, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->TAG:Ljava/lang/String;

    const-string v4, "fill Window thrown excpetion inside onMove()"

    invoke-static {v3, v4, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/dcm/provider/customcursor/DCMDataCursor;->mPos:I

    goto :goto_1
.end method
