.class public Lcom/sec/dcm/provider/db/DCMQueryBuilder;
.super Ljava/lang/Object;
.source "DCMQueryBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/DCMQueryBuilder$1;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mCPDocType:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Lorg/apache/lucene/search/Filter;

.field private mQuery:Lorg/apache/lucene/search/BooleanQuery;

.field private mSelection:Ljava/lang/String;

.field private mTable:I

.field private mTokenParser:Lcom/sec/dcm/provider/db/tokenparser/TokenParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "table"    # I
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mFilter:Lorg/apache/lucene/search/Filter;

    .line 81
    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    .line 86
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCPDocType:Lcom/google/common/base/Optional;

    .line 89
    iput p1, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTable:I

    .line 90
    iput-object p2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mSelection:Ljava/lang/String;

    .line 91
    return-void
.end method

.method private buildBaseQuery()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 144
    const/4 v0, 0x0

    .line 145
    .local v0, "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    iput-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    .line 147
    iget v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTable:I

    invoke-direct {p0, v2}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->getDocType(I)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCPDocType:Lcom/google/common/base/Optional;

    .line 152
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCPDocType:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    const-string v3, "doctype"

    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCPDocType:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCPDocType:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v4, v2, v5, v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    .line 156
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 159
    :cond_0
    const-string v2, "deleted"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4, v5, v5}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    .line 161
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 163
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v1

    .line 164
    .local v1, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v1, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 165
    return-void
.end method

.method private buildCategoryQuery()Lcom/google/common/base/Optional;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    .line 272
    .local v0, "retError":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    iput-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    .line 273
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    new-instance v3, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 274
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v1

    .line 275
    .local v1, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v1, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 276
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mSelection:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->tokenizeCategoryClause(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    .line 277
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 278
    const-string v2, "Invalid Selection"

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 281
    .end local v0    # "retError":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    :cond_0
    return-object v0
.end method

.method private buildLuceneFilter(Ljava/util/ArrayList;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v13

    .line 361
    .local v13, "latitude":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Double;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v14

    .line 362
    .local v14, "longitude":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Double;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v16

    .line 363
    .local v16, "radiusKm":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Double;>;"
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 364
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 365
    .local v12, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 366
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 368
    .local v10, "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual {v10}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->fromString(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v15

    .line 369
    .local v15, "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    if-nez v15, :cond_1

    .line 370
    sget-object v4, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v5, "Invalid Column Name in Selection Clause in making filter query"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 371
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mFilter:Lorg/apache/lucene/search/Filter;

    .line 421
    .end local v10    # "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .end local v12    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .end local v15    # "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :cond_0
    :goto_1
    return-void

    .line 374
    .restart local v10    # "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .restart local v12    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .restart local v15    # "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :cond_1
    sget-object v4, Lcom/sec/dcm/provider/db/DCMQueryBuilder$1;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMDocumentAllColumns:[I

    invoke-virtual {v15}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 401
    sget-object v4, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v5, "Skipping token in buildFilter()"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v10}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 377
    :pswitch_0
    :try_start_0
    invoke-virtual {v10}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    goto :goto_0

    .line 379
    :catch_0
    move-exception v11

    .line 380
    .local v11, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v5, "Exception in buildFilter()"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 385
    .end local v11    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_1
    invoke-virtual {v10}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    goto :goto_0

    .line 387
    :catch_1
    move-exception v11

    .line 388
    .restart local v11    # "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v5, "Exception in buildFilter()"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 393
    .end local v11    # "e":Ljava/lang/Exception;
    :pswitch_2
    :try_start_2
    invoke-virtual {v10}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnArgument()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v16

    goto/16 :goto_0

    .line 395
    :catch_2
    move-exception v11

    .line 396
    .restart local v11    # "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v5, "Exception in buildFilter()"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 405
    .end local v10    # "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .end local v15    # "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :cond_2
    invoke-virtual {v14}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v13}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 406
    sget-object v3, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    .line 407
    .local v3, "ctx":Lcom/spatial4j/core/context/SpatialContext;
    invoke-virtual/range {v16 .. v16}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    if-nez v4, :cond_3

    .line 408
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v16

    .line 410
    :cond_3
    new-instance v17, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    const-string v4, "location"

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    .line 413
    .local v17, "strategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    new-instance v2, Lorg/apache/lucene/spatial/query/SpatialArgs;

    sget-object v18, Lorg/apache/lucene/spatial/query/SpatialOperation;->Intersects:Lorg/apache/lucene/spatial/query/SpatialOperation;

    invoke-virtual {v14}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v13}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual/range {v16 .. v16}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide v20, 0x40b8e3023ed7ac24L    # 6371.0087714

    move-wide/from16 v0, v20

    invoke-static {v8, v9, v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->dist2Degrees(DD)D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Lcom/spatial4j/core/context/SpatialContext;->makeCircle(DDD)Lcom/spatial4j/core/shape/Circle;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v2, v0, v4}, Lorg/apache/lucene/spatial/query/SpatialArgs;-><init>(Lorg/apache/lucene/spatial/query/SpatialOperation;Lcom/spatial4j/core/shape/Shape;)V

    .line 418
    .local v2, "args":Lorg/apache/lucene/spatial/query/SpatialArgs;
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/apache/lucene/spatial/SpatialStrategy;->makeFilter(Lorg/apache/lucene/spatial/query/SpatialArgs;)Lorg/apache/lucene/search/Filter;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mFilter:Lorg/apache/lucene/search/Filter;

    goto/16 :goto_1

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private buildLuceneQuery(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/google/common/base/Optional;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/tokenparser/DCMToken;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;",
            ">;)",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .local p2, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v9

    .line 287
    .local v9, "retError":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/Stack;

    invoke-direct {v8}, Ljava/util/Stack;-><init>()V

    .line 290
    .local v8, "queryStack":Ljava/util/Stack;, "Ljava/util/Stack<Lorg/apache/lucene/search/Query;>;"
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_0

    .line 291
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 292
    .local v4, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 293
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;

    .line 295
    .local v2, "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->isValidToken()Z

    move-result v10

    if-nez v10, :cond_1

    .line 296
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Invalid Selection Clause"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    const-string v10, "Invalid Selection"

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v9

    .line 352
    .end local v2    # "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .end local v4    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .end local v9    # "retError":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    :cond_0
    :goto_1
    return-object v9

    .line 299
    .restart local v2    # "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    .restart local v4    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    .restart local v9    # "retError":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    :cond_1
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder$1;->$SwitchMap$com$sec$dcm$provider$db$tokenparser$DCMToken$TokenType:[I

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 343
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Skipping token in buildLuceneQuery:"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getTokenType()Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 306
    :pswitch_0
    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken;->getColumnName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;->fromString(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;

    move-result-object v5

    .line 307
    .local v5, "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    if-nez v5, :cond_2

    .line 308
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Invalid Column Name in Selection Clause"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    const-string v10, "Invalid Selection"

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v9

    goto :goto_1

    .line 311
    :cond_2
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTokenParser:Lcom/sec/dcm/provider/db/tokenparser/TokenParser;

    move-object/from16 v0, p2

    invoke-virtual {v10, v2, v0}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser;->processToken(Lcom/sec/dcm/provider/db/tokenparser/DCMToken;Ljava/util/ArrayList;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 312
    .local v1, "atomicQuery":Lorg/apache/lucene/search/Query;
    if-nez v1, :cond_3

    .line 313
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Invalid token"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    const-string v10, "Invalid Selection"

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v9

    goto :goto_1

    .line 316
    :cond_3
    invoke-virtual {v8, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 320
    .end local v1    # "atomicQuery":Lorg/apache/lucene/search/Query;
    .end local v5    # "name":Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentAllColumns;
    :pswitch_1
    invoke-virtual {v8}, Ljava/util/Stack;->size()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_4

    .line 321
    new-instance v3, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 322
    .local v3, "internalQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/Query;

    .line 323
    .local v6, "q1":Lorg/apache/lucene/search/Query;
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/search/Query;

    .line 324
    .local v7, "q2":Lorg/apache/lucene/search/Query;
    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v6, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 325
    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v7, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 326
    invoke-virtual {v8, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    .end local v3    # "internalQuery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v6    # "q1":Lorg/apache/lucene/search/Query;
    .end local v7    # "q2":Lorg/apache/lucene/search/Query;
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 331
    :pswitch_2
    invoke-virtual {v8}, Ljava/util/Stack;->size()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_5

    .line 332
    new-instance v3, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 333
    .restart local v3    # "internalQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/Query;

    .line 334
    .restart local v6    # "q1":Lorg/apache/lucene/search/Query;
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/search/Query;

    .line 335
    .restart local v7    # "q2":Lorg/apache/lucene/search/Query;
    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v6, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 336
    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v7, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 337
    invoke-virtual {v8, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    .end local v3    # "internalQuery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v6    # "q1":Lorg/apache/lucene/search/Query;
    .end local v7    # "q2":Lorg/apache/lucene/search/Query;
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 348
    .end local v2    # "dcmToken":Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
    :cond_6
    invoke-virtual {v8}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 349
    iget-object v11, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/search/Query;

    sget-object v12, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v11, v10, v12}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto/16 :goto_1

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private createTokenParser()V
    .locals 2

    .prologue
    .line 123
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryBuilder$1;->$SwitchMap$com$samsung$dcm$documents$MediaDoc$DocType:[I

    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCPDocType:Lcom/google/common/base/Optional;

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 138
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;

    invoke-direct {v0}, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;-><init>()V

    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTokenParser:Lcom/sec/dcm/provider/db/tokenparser/TokenParser;

    .line 141
    :goto_0
    return-void

    .line 125
    :pswitch_0
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;

    invoke-direct {v0}, Lcom/sec/dcm/provider/db/tokenparser/ImageTokenParser;-><init>()V

    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTokenParser:Lcom/sec/dcm/provider/db/tokenparser/TokenParser;

    goto :goto_0

    .line 131
    :pswitch_1
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/VideoTokenParser;

    invoke-direct {v0}, Lcom/sec/dcm/provider/db/tokenparser/VideoTokenParser;-><init>()V

    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTokenParser:Lcom/sec/dcm/provider/db/tokenparser/TokenParser;

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getDocType(I)Lcom/google/common/base/Optional;
    .locals 2
    .param p1, "table"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 95
    .local v0, "docType":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Lcom/samsung/dcm/documents/MediaDoc$DocType;>;"
    sparse-switch p1, :sswitch_data_0

    .line 119
    :goto_0
    return-object v0

    .line 98
    :sswitch_0
    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 99
    goto :goto_0

    .line 101
    :sswitch_1
    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 102
    goto :goto_0

    .line 113
    :sswitch_2
    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 114
    goto :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x5 -> :sswitch_0
        0xc -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public build()Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v9, 0x0

    .line 177
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v6

    .line 178
    .local v6, "retError":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 179
    .local v0, "dcmQuery":Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    const/4 v8, 0x0

    .line 180
    .local v8, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/tokenparser/DCMToken;>;"
    iget v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mTable:I

    sparse-switch v10, :sswitch_data_0

    :cond_0
    :goto_0
    move-object v9, v0

    .line 267
    :goto_1
    return-object v9

    .line 186
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->buildBaseQuery()V

    .line 188
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mSelection:Ljava/lang/String;

    if-eqz v10, :cond_1

    .line 189
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->createTokenParser()V

    .line 190
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mSelection:Ljava/lang/String;

    invoke-static {v10}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->tokenize(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 198
    invoke-direct {p0, v8, v9}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->buildLuceneQuery(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/google/common/base/Optional;

    move-result-object v6

    .line 199
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 200
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Building lucene query failed!"

    new-array v12, v13, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 204
    :cond_1
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    iget-object v11, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mFilter:Lorg/apache/lucene/search/Filter;

    invoke-static {v10, v11, v9}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v0

    .line 205
    goto :goto_0

    .line 208
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->buildCategoryQuery()Lcom/google/common/base/Optional;

    move-result-object v6

    .line 209
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 210
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Building lucene query failed!"

    new-array v12, v13, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 213
    :cond_2
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mFilter:Lorg/apache/lucene/search/Filter;

    iget-object v11, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    invoke-static {v9, v10, v11}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v0

    .line 214
    goto :goto_0

    .line 217
    :sswitch_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    .line 218
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v7, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->buildBaseQuery()V

    .line 221
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mSelection:Ljava/lang/String;

    if-eqz v10, :cond_4

    .line 222
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->createTokenParser()V

    .line 223
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mSelection:Ljava/lang/String;

    invoke-static {v10}, Lcom/sec/dcm/provider/db/tokenparser/TokenParser$Tokenizer;->tokenize(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 225
    invoke-direct {p0, v8, v7}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->buildLuceneQuery(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/google/common/base/Optional;

    move-result-object v6

    .line 226
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 227
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Building lucene query failed!"

    new-array v12, v13, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 231
    :cond_3
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_named_location"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_person"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_calendar_event"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_user_tag"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_scene_type"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    const-string v11, "root_subscene_type"

    sget-object v12, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-direct {v10, v11, v12}, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;-><init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    :cond_4
    iget-object v9, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mFilter:Lorg/apache/lucene/search/Filter;

    iget-object v11, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mCategoryList:Ljava/util/ArrayList;

    invoke-static {v9, v10, v11, v7}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v0

    .line 245
    goto/16 :goto_0

    .line 247
    .end local v7    # "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->buildBaseQuery()V

    .line 250
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v10

    const-class v11, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v10, v11}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v2

    .line 253
    .local v2, "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v10

    if-lez v10, :cond_0

    .line 254
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 255
    .local v1, "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 256
    .local v4, "name":Ljava/lang/String;
    new-instance v5, Lorg/apache/lucene/search/TermQuery;

    new-instance v10, Lorg/apache/lucene/index/Term;

    const-string v11, "false"

    invoke-direct {v10, v4, v11}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v10}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 258
    .local v5, "pluginquery":Lorg/apache/lucene/search/Query;
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v10, v5, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 259
    sget-object v10, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->TAG:Ljava/lang/String;

    const-string v11, "Lucene being queried for"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v4, v12, v13

    invoke-static {v10, v11, v12}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 262
    .end local v1    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "pluginquery":Lorg/apache/lucene/search/Query;
    :cond_5
    iget-object v10, p0, Lcom/sec/dcm/provider/db/DCMQueryBuilder;->mQuery:Lorg/apache/lucene/search/BooleanQuery;

    invoke-static {v10, v9, v9}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->create(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Ljava/util/ArrayList;)Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    move-result-object v0

    goto/16 :goto_0

    .line 180
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0xc -> :sswitch_3
    .end sparse-switch
.end method
