.class public Lcom/sec/dcm/provider/db/DCMQueryRunner;
.super Ljava/lang/Object;
.source "DCMQueryRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/db/DCMQueryRunner$1;,
        Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;
    }
.end annotation


# static fields
.field public static final MAX_NUM_RESULTS:I = 0x1f4

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

.field private mFieldsToLoad:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mISNeedFileCheck:Z

.field private mISOwner:Z

.field private mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private mProjection:[Ljava/lang/String;

.field private mScoreDocList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mTaxoReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

.field private mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lcom/sec/dcm/provider/db/DCMLuceneQuery;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "nrtManager"    # Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    .param p2, "indexSearcher"    # Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .param p3, "dcmQuery"    # Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISOwner:Z

    .line 84
    iput-boolean v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISNeedFileCheck:Z

    .line 198
    iput-object p2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 199
    iput-object p3, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    .line 200
    iput-object p1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 201
    iput-object p4, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    .line 203
    iput-boolean v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISOwner:Z

    .line 204
    iput-boolean v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISNeedFileCheck:Z

    .line 206
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Lcom/sec/dcm/provider/db/DCMLuceneQuery;Lcom/samsung/dcm/framework/configuration/DcmTransport;Ljava/lang/String;)V
    .locals 5
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "query"    # Lcom/sec/dcm/provider/db/DCMLuceneQuery;
    .param p3, "transport"    # Lcom/samsung/dcm/framework/configuration/DcmTransport;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 83
    iput-boolean v4, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISOwner:Z

    .line 84
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISNeedFileCheck:Z

    .line 162
    iput-object p1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    .line 163
    iput-object p2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    .line 165
    iput-object p3, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 166
    invoke-virtual {p3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 167
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getTaxoReader()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTaxoReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    .line 168
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    .line 172
    :try_start_0
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-nez v1, :cond_0

    .line 177
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "DCMQueryRunner "

    invoke-static {v1, v2, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 180
    .end local v0    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :cond_0
    iput-boolean v4, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISOwner:Z

    .line 181
    iput-boolean v3, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISNeedFileCheck:Z

    .line 183
    return-void
.end method

.method private addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V
    .locals 19
    .param p1, "catlistCur"    # Landroid/database/MatrixCursor;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "category"    # Ljava/lang/String;
    .param p4, "media_count"    # I

    .prologue
    .line 327
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->getCatType(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    move-result-object v11

    .line 328
    .local v11, "type":Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
    const-string v14, ";"

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 329
    .local v6, "indexof":I
    if-ltz v6, :cond_0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    if-ne v6, v14, :cond_2

    .line 330
    :cond_0
    sget-object v14, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v15, "Ignoring if the data has only category roots, category:"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p3, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    :cond_1
    :goto_0
    return-void

    .line 334
    :cond_2
    add-int/lit8 v14, v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 342
    sget-object v14, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    if-ne v11, v14, :cond_4

    .line 343
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 344
    .local v12, "startQuery":J
    sget-object v14, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v15, "Document search in index dir for scenetype:"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p3, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->buildQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->validateCategory(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lorg/apache/lucene/search/Query;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 346
    sget-object v14, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v15, "Document count in index dir for scene type:"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p3, v16, v17

    const/16 v17, 0x1

    const-string v18, "is <= 0"

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 349
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long v4, v14, v12

    .line 350
    .local v4, "endQuery":J
    sget-object v14, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v15, "Query for Doc in category search :"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    const-string v18, "ms"

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    .end local v4    # "endQuery":J
    .end local v12    # "startQuery":J
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v14}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v14

    if-eqz v14, :cond_6

    .line 356
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v14}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    .line 357
    .local v10, "selCat":Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;
    iget-object v14, v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCatType:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v14, v11}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    iget-object v14, v10, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    goto/16 :goto_0

    .line 364
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v10    # "selCat":Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v9

    .line 365
    .local v9, "row":Landroid/database/MatrixCursor$RowBuilder;
    sget-object v14, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v15, "addCategoryListRow: Stripped Category:"

    const/16 v16, 0x5

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p3, v16, v17

    const/16 v17, 0x1

    const-string v18, " Type:"

    aput-object v18, v16, v17

    const/16 v17, 0x2

    aput-object v11, v16, v17

    const/16 v17, 0x3

    const-string v18, " media_count:"

    aput-object v18, v16, v17

    const/16 v17, 0x4

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 368
    move-object/from16 v2, p2

    .local v2, "arr$":[Ljava/lang/String;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v7, :cond_1

    aget-object v8, v2, v3

    .line 369
    .local v8, "pro":Ljava/lang/String;
    const-string v14, "category_name"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 370
    move-object/from16 v0, p3

    invoke-virtual {v9, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 368
    :cond_7
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 371
    :cond_8
    const-string v14, "category_type"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 372
    invoke-virtual {v11}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->ordinal()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    .line 373
    :cond_9
    const-string v14, "media_count"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 374
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2
.end method

.method private buildQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 285
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 286
    .local v1, "query":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v0, 0x0

    .line 287
    .local v0, "atomicQuery":Lorg/apache/lucene/search/Query;
    const-string v3, "deleted"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5, v6, v6}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    .line 289
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 291
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    .end local v0    # "atomicQuery":Lorg/apache/lucene/search/Query;
    new-instance v3, Lorg/apache/lucene/index/Term;

    const-string v4, "scene_type"

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 292
    .restart local v0    # "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 294
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v2

    .line 295
    .local v2, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 296
    return-object v1
.end method

.method private closeCursorSilently(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 638
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 639
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 641
    :catch_0
    move-exception v0

    .line 642
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "Cursor Close failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private collectFacets(Landroid/database/MatrixCursor;Lorg/apache/lucene/facet/search/FacetsCollector;)V
    .locals 26
    .param p1, "cursor"    # Landroid/database/MatrixCursor;
    .param p2, "facetsCollector"    # Lorg/apache/lucene/facet/search/FacetsCollector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 381
    if-eqz p2, :cond_5

    .line 386
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/facet/search/FacetsCollector;->getFacetResults()Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 391
    .local v6, "cats":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/facet/search/FacetResult;

    .line 393
    .local v8, "fr":Lorg/apache/lucene/facet/search/FacetResult;
    invoke-virtual {v8}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v9

    .line 395
    .local v9, "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    move-object/from16 v21, v0

    iget-object v0, v9, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v22, v0

    const-string v23, ";"

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v22

    iget-wide v0, v9, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V

    .line 399
    iget-object v0, v9, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_0

    .line 400
    iget-object v0, v9, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 401
    .local v16, "sfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v22, v0

    const-string v23, ";"

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V

    .line 406
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_1

    .line 407
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 408
    .local v17, "ssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v22, v0

    const-string v23, ";"

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V

    .line 414
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_2

    .line 415
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 416
    .local v18, "sssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v22, v0

    const-string v23, ";"

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    iget-wide v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V

    .line 423
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_3

    .line 424
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 425
    .local v19, "ssssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v22, v0

    const-string v23, ";"

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    iget-wide v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V

    .line 435
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_4

    .line 436
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 437
    .local v20, "sssssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v22, v0

    const-string v23, ";"

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    iget-wide v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->addCategoryListRow(Landroid/database/MatrixCursor;[Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 387
    .end local v6    # "cats":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .end local v8    # "fr":Lorg/apache/lucene/facet/search/FacetResult;
    .end local v9    # "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "sfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v17    # "ssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v18    # "sssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v19    # "ssssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v20    # "sssssfrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    :catch_0
    move-exception v7

    .line 388
    .local v7, "e":Ljava/io/IOException;
    new-instance v21, Ljava/io/IOException;

    move-object/from16 v0, v21

    invoke-direct {v0, v7}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v21

    .line 456
    .end local v7    # "e":Ljava/io/IOException;
    :cond_5
    return-void
.end method

.method private getCatType(Ljava/lang/String;)Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
    .locals 5
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 260
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 261
    .local v0, "type":Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
    const-string v1, "root_named_location"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_NAMED_LOCATION:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 280
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "getCatType: Stripped Category:"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    return-object v0

    .line 264
    :cond_1
    const-string v1, "root_person"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 265
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_PERSON_NAME:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    goto :goto_0

    .line 267
    :cond_2
    const-string v1, "root_calendar_event"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 268
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_CALENDAR_EVENT:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    goto :goto_0

    .line 270
    :cond_3
    const-string v1, "root_user_tag"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 271
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_USER_TAG:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    goto :goto_0

    .line 273
    :cond_4
    const-string v1, "root_scene_type"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 274
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    goto :goto_0

    .line 276
    :cond_5
    const-string v1, "root_subscene_type"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->CATEGORY_SUBSCENE_TYPE:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    goto :goto_0
.end method

.method private processStringFieldList(Lorg/apache/lucene/document/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 665
    const-string v0, ","

    .line 666
    .local v0, "delimiter":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lorg/apache/lucene/document/Document;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 668
    .local v3, "listOfStrings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, ""

    .line 669
    .local v2, "listAsString":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 670
    .local v4, "string":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 671
    move-object v2, v4

    goto :goto_0

    .line 673
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 676
    .end local v4    # "string":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v2, "null"

    .end local v2    # "listAsString":Ljava/lang/String;
    :cond_2
    return-object v2
.end method

.method private refineFieldsToLoadList(Ljava/util/ArrayList;Z)V
    .locals 5
    .param p2, "bDefault"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "fieldsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 224
    const/4 v0, 0x0

    .line 225
    .local v0, "isCategoryRequired":Z
    const/4 v1, 0x0

    .line 228
    .local v1, "result":Z
    const-string v4, "named_location"

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 229
    if-nez v0, :cond_0

    if-eqz v1, :cond_8

    :cond_0
    move v0, v3

    .line 232
    :goto_0
    const-string v4, "person_names"

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 233
    if-nez v0, :cond_1

    if-eqz v1, :cond_9

    :cond_1
    move v0, v3

    .line 236
    :goto_1
    const-string v4, "calendar_event"

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 237
    if-nez v0, :cond_2

    if-eqz v1, :cond_a

    :cond_2
    move v0, v3

    .line 240
    :goto_2
    const-string v4, "user_tags"

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 241
    if-nez v0, :cond_3

    if-eqz v1, :cond_b

    :cond_3
    move v0, v3

    .line 244
    :goto_3
    const-string v4, "scene_type"

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 245
    if-nez v0, :cond_4

    if-eqz v1, :cond_c

    :cond_4
    move v0, v3

    .line 248
    :goto_4
    const-string v4, "subscene_type"

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 249
    if-nez v0, :cond_5

    if-eqz v1, :cond_d

    :cond_5
    move v0, v3

    .line 250
    :goto_5
    if-nez v0, :cond_6

    if-eqz p2, :cond_e

    :cond_6
    move v0, v3

    .line 254
    :goto_6
    if-eqz v0, :cond_7

    .line 255
    const-string v2, "categories"

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    :cond_7
    return-void

    :cond_8
    move v0, v2

    .line 229
    goto :goto_0

    :cond_9
    move v0, v2

    .line 233
    goto :goto_1

    :cond_a
    move v0, v2

    .line 237
    goto :goto_2

    :cond_b
    move v0, v2

    .line 241
    goto :goto_3

    :cond_c
    move v0, v2

    .line 245
    goto :goto_4

    :cond_d
    move v0, v2

    .line 249
    goto :goto_5

    :cond_e
    move v0, v2

    .line 250
    goto :goto_6
.end method

.method private setFieldsToLoad()V
    .locals 2

    .prologue
    .line 213
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 214
    .local v0, "fieldsToLoadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->refineFieldsToLoadList(Ljava/util/ArrayList;Z)V

    .line 215
    const-string v1, "uri"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    const-string v1, "path"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISNeedFileCheck:Z

    .line 219
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mFieldsToLoad:Ljava/util/Set;

    .line 220
    return-void
.end method

.method private validateCategory(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lorg/apache/lucene/search/Query;)Z
    .locals 17
    .param p1, "indexSearcher"    # Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 300
    const/4 v13, 0x0

    .line 301
    .local v13, "isValidCat":Z
    const/16 v16, 0x0

    .line 302
    .local v16, "topDocs":Lorg/apache/lucene/search/TopDocs;
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 303
    .local v11, "field":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v1, "path"

    invoke-interface {v11, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 305
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1f4

    const/4 v6, 0x0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v16

    .line 306
    move-object/from16 v0, v16

    iget v1, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    if-lez v1, :cond_0

    .line 307
    move-object/from16 v0, v16

    iget-object v7, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v7, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v14, v7

    .local v14, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_0
    if-ge v12, v14, :cond_0

    aget-object v15, v7, v12

    .line 308
    .local v15, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget v1, v15, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v8

    .line 309
    .local v8, "doc":Lorg/apache/lucene/document/Document;
    const-string v1, "path"

    invoke-virtual {v8, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 310
    .local v9, "docPath":Ljava/lang/String;
    if-eqz v9, :cond_1

    invoke-static {v9}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isValidPath(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_1

    .line 311
    const/4 v13, 0x1

    .line 321
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v8    # "doc":Lorg/apache/lucene/document/Document;
    .end local v9    # "docPath":Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_0
    :goto_1
    return v13

    .line 307
    .restart local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v8    # "doc":Lorg/apache/lucene/document/Document;
    .restart local v9    # "docPath":Ljava/lang/String;
    .restart local v12    # "i$":I
    .restart local v14    # "len$":I
    .restart local v15    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 316
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v8    # "doc":Lorg/apache/lucene/document/Document;
    .end local v9    # "docPath":Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catch_0
    move-exception v10

    .line 317
    .local v10, "e":Ljava/io/IOException;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "Exception caught! "

    invoke-static {v1, v2, v10}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 318
    .end local v10    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v10

    .line 319
    .local v10, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "SearchingException caught! "

    invoke-static {v1, v2, v10}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 683
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 684
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 685
    iput-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    .line 689
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v1, :cond_2

    .line 690
    iget-boolean v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISOwner:Z

    if-eqz v1, :cond_1

    .line 691
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 694
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 695
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 696
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, " NRTSearcher released"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 698
    :cond_2
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTaxoReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    if-eqz v1, :cond_3

    .line 699
    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTaxoReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->close()V

    .line 700
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTaxoReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    :cond_3
    :goto_0
    return-void

    .line 702
    :catch_0
    move-exception v0

    .line 703
    .local v0, "ioe":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getCategories()Landroid/database/Cursor;
    .locals 17

    .prologue
    .line 590
    new-instance v2, Lcom/sec/dcm/provider/customcursor/DCMMatrixCursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    const-string v13, "category"

    invoke-direct {v2, v12, v13}, Lcom/sec/dcm/provider/customcursor/DCMMatrixCursor;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    .local v2, "catListCountCursor":Landroid/database/MatrixCursor;
    const/4 v8, 0x0

    .line 592
    .local v8, "facetsCollector":Lorg/apache/lucene/facet/search/FacetsCollector;
    const/4 v4, 0x0

    .line 593
    .local v4, "docsCollector":Lorg/apache/lucene/search/Collector;
    const/4 v7, 0x0

    .line 594
    .local v7, "facetSearchParams":Lorg/apache/lucene/facet/params/FacetSearchParams;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 595
    .local v6, "facetRequests":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v12}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getCategoryList()Ljava/util/ArrayList;

    move-result-object v1

    .line 597
    .local v1, "catList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v9, v12, :cond_0

    .line 598
    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    iget-object v3, v12, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    .line 599
    .local v3, "category_root":Ljava/lang/String;
    new-instance v10, Lorg/apache/lucene/facet/search/CountFacetRequest;

    new-instance v12, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v13, ";"

    invoke-virtual {v3, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    const v13, 0x7fffffff

    invoke-direct {v10, v12, v13}, Lorg/apache/lucene/facet/search/CountFacetRequest;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    .line 601
    .local v10, "mReq":Lorg/apache/lucene/facet/search/CountFacetRequest;
    const/4 v12, 0x5

    invoke-virtual {v10, v12}, Lorg/apache/lucene/facet/search/CountFacetRequest;->setDepth(I)V

    .line 602
    sget-object v12, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    invoke-virtual {v10, v12}, Lorg/apache/lucene/facet/search/CountFacetRequest;->setResultMode(Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;)V

    .line 604
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 607
    .end local v3    # "category_root":Ljava/lang/String;
    .end local v10    # "mReq":Lorg/apache/lucene/facet/search/CountFacetRequest;
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_1

    .line 608
    new-instance v7, Lorg/apache/lucene/facet/params/FacetSearchParams;

    .end local v7    # "facetSearchParams":Lorg/apache/lucene/facet/params/FacetSearchParams;
    invoke-direct {v7, v6}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Ljava/util/List;)V

    .line 609
    .restart local v7    # "facetSearchParams":Lorg/apache/lucene/facet/params/FacetSearchParams;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTaxoReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-static {v7, v12, v13}, Lorg/apache/lucene/facet/search/FacetsCollector;->create(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/FacetsCollector;

    move-result-object v8

    .line 611
    invoke-virtual {v8}, Lorg/apache/lucene/facet/search/FacetsCollector;->reset()V

    .line 615
    :cond_1
    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lorg/apache/lucene/search/TopScoreDocCollector;->create(IZ)Lorg/apache/lucene/search/TopScoreDocCollector;

    move-result-object v4

    .line 619
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v12}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/search/BooleanQuery;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v14}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v14

    const/4 v15, 0x2

    new-array v15, v15, [Lorg/apache/lucene/search/Collector;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    const/16 v16, 0x1

    aput-object v8, v15, v16

    invoke-static {v15}, Lorg/apache/lucene/search/MultiCollector;->wrap([Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->search(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 623
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->collectFacets(Landroid/database/MatrixCursor;Lorg/apache/lucene/facet/search/FacetsCollector;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 633
    :goto_1
    return-object v2

    .line 625
    :catch_0
    move-exception v5

    .line 626
    .local v5, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->closeCursorSilently(Landroid/database/Cursor;)V

    .line 627
    const/4 v2, 0x0

    .line 632
    goto :goto_1

    .line 628
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 629
    .local v11, "t":Ljava/lang/Throwable;
    sget-object v12, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v13, "Exception at throwable:"

    invoke-static {v12, v13, v11}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 630
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->closeCursorSilently(Landroid/database/Cursor;)V

    .line 631
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    return-object v0
.end method

.method public getDocuments()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 548
    const/4 v14, 0x0

    .line 549
    .local v14, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v1, 0x0

    .line 550
    .local v1, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 552
    .local v8, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    const/4 v12, -0x1

    .line 555
    .local v12, "pendingDocs":I
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v3

    const/16 v4, 0x1f4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v14

    .line 557
    const/4 v1, 0x0

    .line 559
    if-gez v12, :cond_1

    .line 560
    iget v12, v14, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    .line 563
    :cond_1
    iget-object v6, v14, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v6, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v11, v6

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v11, :cond_2

    aget-object v13, v6, v10

    .line 564
    .local v13, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    iget v2, v13, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v7

    .line 565
    .local v7, "doc":Lorg/apache/lucene/document/Document;
    add-int/lit8 v12, v12, -0x1

    .line 566
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    move-object v1, v13

    .line 563
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 571
    .end local v7    # "doc":Lorg/apache/lucene/document/Document;
    .end local v13    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_2
    if-nez v12, :cond_3

    .line 572
    sget-object v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "getDocuments NO REPEATED "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    const/4 v1, 0x0

    .line 575
    :cond_3
    if-nez v1, :cond_0

    .line 580
    return-object v8

    .line 576
    .end local v6    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    :catch_0
    move-exception v9

    .line 577
    .local v9, "e":Ljava/lang/Throwable;
    sget-object v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v2, "Exception in fetchScoreDocs:"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v9}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v9}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public getLuceneDocuments(II)Ljava/util/ArrayList;
    .locals 4
    .param p1, "startPos"    # I
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 656
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 657
    .local v0, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, p1

    .end local p1    # "startPos":I
    .local v2, "startPos":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 658
    iget-object v3, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    add-int/lit8 p1, v2, 0x1

    .end local v2    # "startPos":I
    .restart local p1    # "startPos":I
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    add-int/lit8 v1, v1, 0x1

    move v2, p1

    .end local p1    # "startPos":I
    .restart local v2    # "startPos":I
    goto :goto_0

    .line 660
    :cond_0
    return-object v0
.end method

.method public getPendingCount()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 708
    new-instance v7, Landroid/database/MatrixCursor;

    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mProjection:[Ljava/lang/String;

    invoke-direct {v7, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 710
    .local v7, "pendingCountCursor":Landroid/database/MatrixCursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v2}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0x1f4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v9

    .line 711
    .local v9, "topdoc":Lorg/apache/lucene/search/TopDocs;
    invoke-virtual {v7}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v8

    .line 712
    .local v8, "row":Landroid/database/MatrixCursor$RowBuilder;
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->PENDING_FULL_PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v1, v9, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 713
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->PENDING_FULL_PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VA running"

    :goto_0
    invoke-virtual {v8, v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-object v0, v7

    .line 722
    .end local v8    # "row":Landroid/database/MatrixCursor$RowBuilder;
    .end local v9    # "topdoc":Lorg/apache/lucene/search/TopDocs;
    :goto_1
    return-object v0

    .line 713
    .restart local v8    # "row":Landroid/database/MatrixCursor$RowBuilder;
    .restart local v9    # "topdoc":Lorg/apache/lucene/search/TopDocs;
    :cond_0
    const-string v0, "VA not running"
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 715
    .end local v8    # "row":Landroid/database/MatrixCursor$RowBuilder;
    .end local v9    # "topdoc":Lorg/apache/lucene/search/TopDocs;
    :catch_0
    move-exception v6

    .line 716
    .local v6, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    invoke-direct {p0, v7}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->closeCursorSilently(Landroid/database/Cursor;)V

    .line 717
    const/4 v7, 0x0

    .end local v6    # "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    :goto_2
    move-object v0, v10

    .line 722
    goto :goto_1

    .line 718
    :catch_1
    move-exception v6

    .line 719
    .local v6, "e":Ljava/io/IOException;
    invoke-direct {p0, v7}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->closeCursorSilently(Landroid/database/Cursor;)V

    .line 720
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public run()I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    const/16 v17, 0x0

    .line 467
    .local v17, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v2, 0x0

    .line 470
    .local v2, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    const/4 v14, -0x1

    .line 471
    .local v14, "pendingDocs":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mFieldsToLoad:Ljava/util/Set;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mFieldsToLoad:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 472
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->setFieldsToLoad()V

    .line 474
    :cond_1
    invoke-static {}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isAlive()Z

    move-result v16

    .line 479
    .local v16, "sendPauseResume":Z
    if-eqz v16, :cond_2

    .line 480
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    sget-object v3, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_QUERY:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 486
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v3}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mDCMQuery:Lcom/sec/dcm/provider/db/DCMLuceneQuery;

    invoke-virtual {v4}, Lcom/sec/dcm/provider/db/DCMLuceneQuery;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v4

    const/16 v5, 0x1f4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v17

    .line 488
    const/4 v2, 0x0

    .line 490
    if-gez v14, :cond_3

    .line 491
    move-object/from16 v0, v17

    iget v14, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    .line 494
    :cond_3
    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v7, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v12, v7

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_8

    aget-object v15, v7, v11
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496
    .local v15, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    const/4 v8, 0x1

    .line 497
    .local v8, "canAddDoc":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    iget v3, v15, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mFieldsToLoad:Ljava/util/Set;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v9

    .line 498
    .local v9, "doc":Lorg/apache/lucene/document/Document;
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mISNeedFileCheck:Z

    if-eqz v1, :cond_4

    .line 499
    const-string v1, "path"

    invoke-virtual {v9, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 500
    .local v13, "path":Ljava/lang/String;
    if-eqz v13, :cond_4

    invoke-static {v13}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isValidPath(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 501
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v3, "Not Valid ,skipping adding to List"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 502
    const/4 v8, 0x0

    .line 505
    .end local v13    # "path":Ljava/lang/String;
    :cond_4
    if-eqz v8, :cond_5

    .line 506
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 513
    .end local v9    # "doc":Lorg/apache/lucene/document/Document;
    :cond_5
    :goto_1
    move-object v2, v15

    .line 514
    add-int/lit8 v14, v14, -0x1

    .line 494
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 508
    :catch_0
    move-exception v10

    .line 510
    .local v10, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    :try_start_2
    invoke-virtual {v10}, Lcom/samsung/dcm/framework/exceptions/SearchingException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 523
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v8    # "canAddDoc":Z
    .end local v10    # "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catch_1
    move-exception v10

    .line 524
    .local v10, "e":Ljava/lang/Throwable;
    :try_start_3
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v3, "Exception in fetchScoreDocs:"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v10}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 525
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dcm/provider/db/DCMQueryRunner;->close()V

    .line 526
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v10}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 528
    .end local v10    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v3, :cond_6

    .line 529
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 530
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 531
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 533
    :cond_6
    if-eqz v16, :cond_7

    .line 534
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_QUERY:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    :cond_7
    throw v1

    .line 518
    .restart local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v11    # "i$":I
    .restart local v12    # "len$":I
    :cond_8
    if-nez v14, :cond_9

    .line 519
    :try_start_4
    sget-object v1, Lcom/sec/dcm/provider/db/DCMQueryRunner;->TAG:Ljava/lang/String;

    const-string v3, "NO REPEATED "

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 520
    const/4 v2, 0x0

    .line 522
    :cond_9
    if-nez v2, :cond_2

    .line 528
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v1, :cond_a

    .line 529
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 530
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 531
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 533
    :cond_a
    if-eqz v16, :cond_b

    .line 534
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    sget-object v3, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_QUERY:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 537
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner;->mScoreDocList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    return v1
.end method
