.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;
.super Ljava/lang/Object;
.source "DCMCategoryColumns.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;
    }
.end annotation


# static fields
.field public static final CATEGORY_FULL_PROJECTION:[Ljava/lang/String;

.field public static final FIELD_CATEGORY_NAME:Ljava/lang/String; = "category_name"

.field public static final FIELD_CATEGORY_TYPE:Ljava/lang/String; = "category_type"

.field public static final FIELD_MEDIA_COUNT:Ljava/lang/String; = "media_count"

.field public static final PENDING_FULL_PROJECTION:[Ljava/lang/String;

.field public static final URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->URI:Landroid/net/Uri;

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "category_name"

    aput-object v1, v0, v2

    const-string v1, "category_type"

    aput-object v1, v0, v3

    const-string v1, "media_count"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->CATEGORY_FULL_PROJECTION:[Ljava/lang/String;

    .line 62
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "PendingCount"

    aput-object v1, v0, v2

    const-string v1, "HeavyTaskRunning"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;->PENDING_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
