.class public final enum Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;
.super Ljava/lang/Enum;
.source "DCMToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dcm/provider/db/tokenparser/DCMToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

.field public static final enum OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;


# instance fields
.field cat_type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 40
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "LIKE"

    const-string v2, "LIKE"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 41
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "EQUAL"

    const-string v2, "="

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 42
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "GREATER_THAN"

    const-string v2, ">"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 43
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "LESS_THAN"

    const-string v2, "<"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 44
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "GREATER_THAN_EQUAL"

    const-string v2, ">="

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 45
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "LESS_THAN_EQUAL"

    const/4 v2, 0x5

    const-string v3, "<="

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 46
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "AND"

    const/4 v2, 0x6

    const-string v3, "AND"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 47
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "OR"

    const/4 v2, 0x7

    const-string v3, "OR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 48
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "OPEN_PARENTHESIS"

    const/16 v2, 0x8

    const-string v3, "("

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 49
    new-instance v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    const-string v1, "CLOSE_PARENTHESIS"

    const/16 v2, 0x9

    const-string v3, ")"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    .line 39
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LIKE:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->GREATER_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->LESS_THAN_EQUAL:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->AND:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OR:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->OPEN_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->CLOSE_PARENTHESIS:Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->$VALUES:[Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput-object p3, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->cat_type:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    return-object v0
.end method

.method public static values()[Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->$VALUES:[Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    invoke-virtual {v0}, [Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;

    return-object v0
.end method


# virtual methods
.method public type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/dcm/provider/db/tokenparser/DCMToken$TokenType;->cat_type:Ljava/lang/String;

    return-object v0
.end method
