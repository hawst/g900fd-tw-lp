.class public Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;
.super Ljava/lang/Object;
.source "DCMQueryRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dcm/provider/db/DCMQueryRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CategoryData"
.end annotation


# instance fields
.field mCatType:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

.field public mCategoryValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;)V
    .locals 0
    .param p1, "catValue"    # Ljava/lang/String;
    .param p2, "catType"    # Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCatType:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    .line 105
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 116
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 117
    check-cast v0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;

    .line 118
    .local v0, "catData":Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;
    iget-object v2, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    const/4 v1, 0x1

    .line 123
    .end local v0    # "catData":Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;
    :cond_0
    return v1
.end method

.method public getCategoryPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lcom/sec/dcm/provider/db/DCMQueryRunner$1;->$SwitchMap$com$sec$dcm$provider$dcmcolumns$DCMCategoryColumns$DCMCategoryType:[I

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCatType:Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;

    invoke-virtual {v1}, Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns$DCMCategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 147
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "root_named_location;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "root_person;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 135
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "root_calendar_event;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 138
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "root_user_tag;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 141
    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "root_scene_type;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 144
    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "root_subscene_type;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/dcm/provider/db/DCMQueryRunner$CategoryData;->mCategoryValue:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
