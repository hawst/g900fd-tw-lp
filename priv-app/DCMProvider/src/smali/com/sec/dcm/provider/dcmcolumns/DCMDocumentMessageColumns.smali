.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentMessageColumns;
.super Ljava/lang/Object;
.source "DCMDocumentMessageColumns.java"

# interfaces
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMBaseColumns;


# static fields
.field public static final FIELD_ATTACHMENT_COUNT:Ljava/lang/String; = "attachment_count"

.field public static final FIELD_BCC_LIST:Ljava/lang/String; = "bcc_list"

.field public static final FIELD_CC_LIST:Ljava/lang/String; = "cc_list"

.field public static final FIELD_FROM_LIST:Ljava/lang/String; = "from_list"

.field public static final FIELD_MESSAGE_TYPE:Ljava/lang/String; = "message_type"

.field public static final FIELD_TEXT:Ljava/lang/String; = "text"

.field public static final FIELD_TO_LIST:Ljava/lang/String; = "to_list"

.field public static final MESSAGE_FULL_PROJECTION:[Ljava/lang/String;

.field public static final URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/messagedocument"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentMessageColumns;->URI:Landroid/net/Uri;

    .line 21
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "from_list"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "to_list"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "cc_list"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bcc_list"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "text"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "attachment_count"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "message_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentMessageColumns;->MESSAGE_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
