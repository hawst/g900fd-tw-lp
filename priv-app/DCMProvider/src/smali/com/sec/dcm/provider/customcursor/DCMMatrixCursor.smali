.class public Lcom/sec/dcm/provider/customcursor/DCMMatrixCursor;
.super Landroid/database/MatrixCursor;
.source "DCMMatrixCursor.java"


# instance fields
.field mTableName:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "columnNames"    # [Ljava/lang/String;
    .param p2, "table_name"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dcm/provider/customcursor/DCMMatrixCursor;->mTableName:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/sec/dcm/provider/customcursor/DCMMatrixCursor;->mTableName:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public getExtras()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 42
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "table_name"

    iget-object v2, p0, Lcom/sec/dcm/provider/customcursor/DCMMatrixCursor;->mTableName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    return-object v0
.end method
