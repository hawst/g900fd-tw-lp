.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageNCategoryColumns;
.super Ljava/lang/Object;
.source "DCMDocumentImageNCategoryColumns.java"

# interfaces
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMCategoryColumns;
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageColumns;


# static fields
.field public static final FIELD_CURSOR_DELIMITER:Ljava/lang/String; = "--------"

.field public static final IMAGE_N_CATEGORY_FULL_PROJECTION:[Ljava/lang/String;

.field public static final URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/imagedocument_n_category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageNCategoryColumns;->URI:Landroid/net/Uri;

    .line 43
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "faceCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "location__x"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "location__y"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "event_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "named_location"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "person_names"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "calendar_event"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "scene_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "subscene_type"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "--------"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "category_name"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "category_type"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "media_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentImageNCategoryColumns;->IMAGE_N_CATEGORY_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
