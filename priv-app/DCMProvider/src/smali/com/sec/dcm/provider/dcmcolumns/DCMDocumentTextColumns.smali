.class public interface abstract Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentTextColumns;
.super Ljava/lang/Object;
.source "DCMDocumentTextColumns.java"

# interfaces
.implements Lcom/sec/dcm/provider/dcmcolumns/DCMBaseColumns;


# static fields
.field public static final FIELD_ATTACHMENT_COUNT:Ljava/lang/String; = "attachment_count"

.field public static final FIELD_AUTHOR:Ljava/lang/String; = "author"

.field public static final FIELD_CREATOR:Ljava/lang/String; = "creator"

.field public static final FIELD_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FIELD_DOC_FORMAT:Ljava/lang/String; = "format"

.field public static final FIELD_KEYWORDS:Ljava/lang/String; = "keywords"

.field public static final FIELD_LANGUAGE:Ljava/lang/String; = "language"

.field public static final FIELD_PAGE_COUNT:Ljava/lang/String; = "page_count"

.field public static final FIELD_READABLE:Ljava/lang/String; = "readable"

.field public static final FIELD_TEXT_TEMP_PATH:Ljava/lang/String; = "text_temp_path"

.field public static final FIELD_WORD_COUNT:Ljava/lang/String; = "word_count"

.field public static final TEXT_FULL_PROJECTION:[Ljava/lang/String;

.field public static final URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/textdocument"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentTextColumns;->URI:Landroid/net/Uri;

    .line 22
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "user_tags"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "attachment_count"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "author"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "creator"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "format"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "keywords"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "language"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "page_count"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "readable"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "text_temp_path"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "word_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentTextColumns;->TEXT_FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method
