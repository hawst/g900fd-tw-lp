.class public Lcom/arcsoft/provider/Columns;
.super Ljava/lang/Object;
.source "Columns.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/provider/Columns$KeyFaceColumns;,
        Lcom/arcsoft/provider/Columns$FaceColumns;,
        Lcom/arcsoft/provider/Columns$PersonColumns;,
        Lcom/arcsoft/provider/Columns$FileColumns;,
        Lcom/arcsoft/provider/Columns$BaseColumns;
    }
.end annotation


# static fields
.field public static final ACTION_FACE_AUTO_GROUP_FINISHED:Ljava/lang/String; = "com.android.media.FACE_SCANNER_FINISHED"

.field public static final ACTION_FACE_AUTO_GROUP_STARTED:Ljava/lang/String; = "com.android.media.FACE_SCANNER_STARTED"

.field public static final ACTION_FACE_GET_SIMILAR_PERSONS_FINISHED:Ljava/lang/String; = "com.android.media.FACE_GET_SIMILAR_PERSONS_FINISHED"

.field public static final ACTION_FACE_SCANNER_FINISHED:Ljava/lang/String; = "com.android.media.FACE_SCANNER_FINISHED"

.field public static final ACTION_FACE_SCANNER_PROGRESS:Ljava/lang/String; = "com.android.media.FACE_SCANNER_PROGRESS"

.field public static final ACTION_FACE_SCANNER_STARTED:Ljava/lang/String; = "com.android.media.FACE_SCANNER_STARTED"

.field public static final ACTION_FACE_SCANNER_STOPPED:Ljava/lang/String; = "com.android.media.FACE_SCANNER_STOPPED"

.field public static final FACES_URI:Landroid/net/Uri;

.field protected static final FACE_COUNT:Ljava/lang/String; = "face_count"

.field public static final FACE_SCANNER_PROGRESS_URI:Ljava/lang/String; = "content://media/face_scanning_progress"

.field public static final FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

.field public static final GET_SIMILAR_PERSONS_URI:Landroid/net/Uri;

.field protected static IGNORE_NOTIFY:Ljava/lang/String; = null

.field public static final KEY_FACES_URI:Landroid/net/Uri;

.field public static final PERSONS_URI:Landroid/net/Uri;

.field public static final RAW_SQL_MAIN_DB:Ljava/lang/String; = "main"

.field public static final RAW_SQL_PERSON_DB:Ljava/lang/String; = "person"

.field public static final RAW_SQL_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "content://media/external/raw_sql"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/provider/Columns;->RAW_SQL_URI:Landroid/net/Uri;

    .line 31
    const-string v0, "content://media/external/faces"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/provider/Columns;->FACES_URI:Landroid/net/Uri;

    .line 32
    const-string v0, "content://media/internal/faces"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/provider/Columns;->KEY_FACES_URI:Landroid/net/Uri;

    .line 33
    const-string v0, "content://media/internal/persons"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/provider/Columns;->PERSONS_URI:Landroid/net/Uri;

    .line 34
    const-string v0, "content://media/external/face_scanner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/provider/Columns;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    .line 36
    const-string v0, "content://media/external/similar_persons"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/provider/Columns;->GET_SIMILAR_PERSONS_URI:Landroid/net/Uri;

    .line 40
    const-string v0, "ignoreNotify"

    sput-object v0, Lcom/arcsoft/provider/Columns;->IGNORE_NOTIFY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    return-void
.end method
