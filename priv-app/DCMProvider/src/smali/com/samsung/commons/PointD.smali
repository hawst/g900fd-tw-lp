.class public Lcom/samsung/commons/PointD;
.super Ljava/lang/Object;
.source "PointD.java"


# instance fields
.field public final x:D

.field public final y:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-wide p1, p0, Lcom/samsung/commons/PointD;->x:D

    .line 10
    iput-wide p3, p0, Lcom/samsung/commons/PointD;->y:D

    .line 11
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 20
    instance-of v2, p1, Lcom/samsung/commons/PointD;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 21
    check-cast v0, Lcom/samsung/commons/PointD;

    .line 22
    .local v0, "other":Lcom/samsung/commons/PointD;
    iget-wide v2, p0, Lcom/samsung/commons/PointD;->x:D

    iget-wide v4, v0, Lcom/samsung/commons/PointD;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/samsung/commons/PointD;->y:D

    iget-wide v4, v0, Lcom/samsung/commons/PointD;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 25
    .end local v0    # "other":Lcom/samsung/commons/PointD;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/samsung/commons/PointD;->x:D

    const-wide v2, 0x40dff24000000000L    # 32713.0

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/samsung/commons/PointD;->y:D

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method
