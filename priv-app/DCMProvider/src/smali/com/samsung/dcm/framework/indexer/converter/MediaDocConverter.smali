.class public Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;
.super Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
.source "MediaDocConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V
    .locals 1
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-virtual {p0, p1, p2}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->convertCommon2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V

    .line 162
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->storeDocType(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc$DocType;)V

    .line 164
    return-void
.end method

.method public convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V
    .locals 8
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;

    .prologue
    .line 173
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const-string v6, "uri"

    invoke-virtual {p1, v6}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v6, "doctype"

    invoke-virtual {p1, v6}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    invoke-virtual {p1}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v0

    .line 179
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexableField;>;"
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 181
    .local v5, "tags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexableField;

    .line 182
    .local v2, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 183
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_1
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "date"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 186
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setCreationDate(Ljava/lang/Long;)V

    goto :goto_0

    .line 188
    :cond_2
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "path"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 189
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setPath(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_3
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 192
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setUri(Landroid/net/Uri;)V

    goto :goto_0

    .line 194
    :cond_4
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mimetype"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 195
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setMimeType(Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_5
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "categories"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 198
    new-instance v6, Ljava/util/ArrayList;

    const-string v7, "categories"

    invoke-virtual {p1, v7}, Lorg/apache/lucene/document/Document;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setCategories(Ljava/util/List;)V

    goto/16 :goto_0

    .line 201
    :cond_6
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "user_tags"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 202
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    .line 203
    .local v4, "tag":Ljava/lang/String;
    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 205
    .end local v4    # "tag":Ljava/lang/String;
    :cond_7
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "storage_id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 206
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 207
    .local v3, "sId":I
    invoke-virtual {p2, v3}, Lcom/samsung/dcm/documents/MediaDoc;->setStorageId(I)V

    goto/16 :goto_0

    .line 209
    .end local v3    # "sId":I
    :cond_8
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "fatvolume_id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 210
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 211
    .restart local v3    # "sId":I
    invoke-virtual {p2, v3}, Lcom/samsung/dcm/documents/MediaDoc;->setFatVolumeId(I)V

    goto/16 :goto_0

    .line 213
    .end local v3    # "sId":I
    :cond_9
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "deleted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 214
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    invoke-virtual {p2, v6}, Lcom/samsung/dcm/documents/MediaDoc;->setDeleted(I)V

    goto/16 :goto_0

    .line 216
    :cond_a
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "bucket_id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 217
    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-virtual {p2, v6, v7}, Lcom/samsung/dcm/documents/MediaDoc;->setBucketId(J)V

    goto/16 :goto_0

    .line 221
    .end local v2    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_b
    invoke-virtual {p2, v5}, Lcom/samsung/dcm/documents/MediaDoc;->setUserTags(Ljava/util/Set;)V

    .line 222
    return-void
.end method

.method protected convertCommon2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V
    .locals 12
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 64
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getTitle()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 69
    new-instance v8, Lorg/apache/lucene/document/StringField;

    const-string v9, "title"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getTitle()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v8, v9, v5, v10}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v8}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 71
    new-instance v8, Lorg/apache/lucene/document/StringField;

    const-string v9, "title_lower"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getTitle()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v8, v9, v5, v10}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v8}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 76
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    new-instance v8, Lorg/apache/lucene/document/LongField;

    const-string v9, "date"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sget-object v5, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v8, v9, v10, v11, v5}, Lorg/apache/lucene/document/LongField;-><init>(Ljava/lang/String;JLorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v8}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 82
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getPath()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 83
    new-instance v8, Lorg/apache/lucene/document/StringField;

    const-string v9, "path"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getPath()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v8, v9, v5, v10}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v8}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 87
    :cond_2
    new-instance v5, Lorg/apache/lucene/document/StringField;

    const-string v8, "uri"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v9, v10}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 90
    new-instance v5, Lorg/apache/lucene/document/IntField;

    const-string v8, "id"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getId()I

    move-result v9

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v9, v10}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 93
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getMimeType()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 94
    new-instance v8, Lorg/apache/lucene/document/StoredField;

    const-string v9, "mimetype"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getMimeType()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v8, v9, v5}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v8}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 98
    :cond_3
    new-instance v5, Lorg/apache/lucene/document/IntField;

    const-string v8, "storage_id"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getStorageId()I

    move-result v9

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v9, v10}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 101
    new-instance v5, Lorg/apache/lucene/document/IntField;

    const-string v8, "fatvolume_id"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getFatVolumeId()I

    move-result v9

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v9, v10}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 103
    new-instance v5, Lorg/apache/lucene/document/IntField;

    const-string v8, "deleted"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getDeleted()I

    move-result v9

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v9, v10}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 106
    new-instance v5, Lorg/apache/lucene/document/LongField;

    const-string v8, "bucket_id"

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getBucketId()J

    move-result-wide v10

    sget-object v9, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v10, v11, v9}, Lorg/apache/lucene/document/LongField;-><init>(Ljava/lang/String;JLorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 108
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getUserTags()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 109
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getUserTags()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 110
    .local v4, "tag":Ljava/lang/String;
    new-instance v5, Lorg/apache/lucene/document/StringField;

    const-string v8, "user_tags"

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v5, v8, v9, v10}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 117
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "tag":Ljava/lang/String;
    :cond_4
    sget-object v8, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->TAG:Ljava/lang/String;

    const-string v9, "Media doc categories are present="

    new-array v10, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getCategoriesCount()I

    move-result v5

    if-lez v5, :cond_5

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v10, v7

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getCategoriesCount()I

    move-result v5

    if-lez v5, :cond_7

    iget-object v5, p0, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    if-eqz v5, :cond_7

    .line 120
    iget-object v5, p0, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getCategories()Ljava/util/List;

    move-result-object v0

    .line 122
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v3, "mCategories":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 126
    .local v1, "category":Ljava/lang/String;
    new-instance v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v6, ";"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v5, Lorg/apache/lucene/document/StoredField;

    const-string v6, "categories"

    invoke-direct {v5, v6, v1}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_2

    .end local v0    # "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "category":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mCategories":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_5
    move v5, v7

    .line 117
    goto :goto_1

    .line 129
    .restart local v0    # "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "mCategories":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_6
    iget-object v5, p0, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    invoke-virtual {v5, p2, v3}, Lorg/apache/lucene/facet/index/FacetFields;->addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V

    .line 131
    .end local v0    # "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mCategories":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_7
    return-void
.end method

.method protected storeDocType(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc$DocType;)V
    .locals 4
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "doctype"    # Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .prologue
    .line 140
    new-instance v0, Lorg/apache/lucene/document/IntField;

    const-string v1, "doctype"

    invoke-virtual {p2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v2

    sget-object v3, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 141
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string v0, "MediaDocConverter"

    return-object v0
.end method
