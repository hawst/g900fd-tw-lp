.class public final Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
.super Ljava/lang/Object;
.source "DCMIndexSearcher.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGenerationId:J

.field private mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

.field private mLifestart:J

.field private final mParent:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private mbIndexSearcherIsValid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lorg/apache/lucene/search/IndexSearcher;J)V
    .locals 5
    .param p1, "parent"    # Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    .param p2, "indexSearcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "generationId"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 48
    iput-wide v2, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mGenerationId:J

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mbIndexSearcherIsValid:Z

    .line 51
    iput-wide v2, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mLifestart:J

    .line 55
    const-string v0, "DCMNRTManager cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mParent:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 57
    const-string v0, "IndexSearcher cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 59
    iput-wide p3, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mGenerationId:J

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mLifestart:J

    .line 61
    return-void
.end method


# virtual methods
.method public getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/document/Document;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/SearchingException;
        }
    .end annotation

    .prologue
    .line 87
    .local p2, "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mbIndexSearcherIsValid:Z

    if-nez v1, :cond_0

    .line 88
    new-instance v1, Lcom/samsung/dcm/framework/exceptions/SearchingException;

    const-string v2, "Must recreate IndexSearcher"

    invoke-direct {v1, v2}, Lcom/samsung/dcm/framework/exceptions/SearchingException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_0
    if-eqz p2, :cond_1

    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->doc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v1

    .line 95
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/IndexSearcher;->doc(I)Lorg/apache/lucene/document/Document;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "general":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mParent:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    .line 99
    new-instance v1, Lcom/samsung/dcm/framework/exceptions/SearchingException;

    const-string v2, "Must recreate IndexSearcher"

    invoke-direct {v1, v2}, Lcom/samsung/dcm/framework/exceptions/SearchingException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getGenerationId()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mGenerationId:J

    return-wide v0
.end method

.method public getIndexReader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method protected getIndexSearcher()Lorg/apache/lucene/search/IndexSearcher;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    return-object v0
.end method

.method protected invalidateIndexSearcher()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mbIndexSearcherIsValid:Z

    .line 128
    return-void
.end method

.method protected reintiatize(Lorg/apache/lucene/search/IndexSearcher;J)V
    .locals 2
    .param p1, "indexSearcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p2, "generationId"    # J

    .prologue
    .line 104
    const-string v0, "IndexSearcher cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 106
    iput-wide p2, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mGenerationId:J

    .line 107
    return-void
.end method

.method public release()V
    .locals 7

    .prologue
    .line 131
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mParent:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2, p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->releaseIndexSearcher(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;)V

    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mLifestart:J

    sub-long v0, v2, v4

    .line 133
    .local v0, "end":J
    sget-object v2, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    const-string v3, "[DCM_Performance] Searcher alive for : ["

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "]ms"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    return-void
.end method

.method public search(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 12
    .param p1, "query"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/SearchingException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 142
    iget-boolean v3, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mbIndexSearcherIsValid:Z

    if-nez v3, :cond_0

    .line 143
    new-instance v3, Lcom/samsung/dcm/framework/exceptions/SearchingException;

    const-string v6, "Must recreate IndexSearcher"

    invoke-direct {v3, v6}, Lcom/samsung/dcm/framework/exceptions/SearchingException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 145
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 147
    .local v4, "start":J
    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v3, p1, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v4

    .line 153
    .local v0, "end":J
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real search : ["

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    const-string v8, "]ms"

    aput-object v8, v7, v11

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    return-void

    .line 148
    .end local v0    # "end":J
    :catch_0
    move-exception v2

    .line 149
    .local v2, "general":Ljava/lang/Exception;
    :try_start_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mParent:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3, v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    .line 150
    new-instance v3, Lcom/samsung/dcm/framework/exceptions/SearchingException;

    const-string v6, "Must recreate IndexSearcher"

    invoke-direct {v3, v6}, Lcom/samsung/dcm/framework/exceptions/SearchingException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    .end local v2    # "general":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v4

    .line 153
    .restart local v0    # "end":J
    sget-object v6, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    const-string v7, "[DCM_Performance] Real search : ["

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v10

    const-string v9, "]ms"

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    throw v3
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;
    .locals 16
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p4, "n"    # I
    .param p5, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/SearchingException;
        }
    .end annotation

    .prologue
    .line 65
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mbIndexSearcherIsValid:Z

    if-nez v4, :cond_0

    .line 66
    new-instance v4, Lcom/samsung/dcm/framework/exceptions/SearchingException;

    const-string v5, "Must recreate IndexSearcher"

    invoke-direct {v4, v5}, Lcom/samsung/dcm/framework/exceptions/SearchingException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 68
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 70
    .local v14, "start":J
    if-eqz p5, :cond_1

    .line 71
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, Lorg/apache/lucene/search/IndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v10, v6, v14

    .line 81
    .local v10, "end":J
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real searchAfter : ["

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "]ms"

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    :goto_0
    return-object v4

    .line 74
    .end local v10    # "end":J
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mIndexSearcher:Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, Lorg/apache/lucene/search/IndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v10, v6, v14

    .line 81
    .restart local v10    # "end":J
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real searchAfter : ["

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "]ms"

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    .end local v10    # "end":J
    :catch_0
    move-exception v12

    .line 77
    .local v12, "general":Ljava/lang/Exception;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->mParent:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4, v12}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    .line 78
    new-instance v4, Lcom/samsung/dcm/framework/exceptions/SearchingException;

    const-string v5, "Must recreate IndexSearcher"

    invoke-direct {v4, v5}, Lcom/samsung/dcm/framework/exceptions/SearchingException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 80
    .end local v12    # "general":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v10, v6, v14

    .line 81
    .restart local v10    # "end":J
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real searchAfter : ["

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "]ms"

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    throw v4
.end method
