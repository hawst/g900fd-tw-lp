.class public Lcom/samsung/dcm/framework/extractormanager/Command;
.super Ljava/lang/Object;
.source "Command.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/Command$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBusy:Z

.field private mCallbacks:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/CommandListener;",
            ">;"
        }
    .end annotation
.end field

.field private mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

.field private mData:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mDocType:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
            ">;"
        }
    .end annotation
.end field

.field private mExtractors:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation
.end field

.field private mIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mInterrupted:Z

.field private mIsSdCardAction:Z

.field private mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

.field private mStorageId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;)V
    .locals 2
    .param p1, "dcmId"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "operation"    # Lcom/samsung/dcm/framework/extractormanager/Operation;

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mInterrupted:Z

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIds:Ljava/util/HashSet;

    .line 56
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    .line 66
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mBusy:Z

    .line 72
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    .line 77
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDocType:Lcom/google/common/base/Optional;

    .line 82
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIsSdCardAction:Z

    .line 85
    iput v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mStorageId:I

    .line 93
    const-string v0, "DCM ID must not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .line 94
    const-string v0, "Operation must not be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;I)V
    .locals 2
    .param p1, "dcmId"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "operation"    # Lcom/samsung/dcm/framework/extractormanager/Operation;
    .param p3, "startId"    # I

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/samsung/dcm/framework/extractormanager/Command;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIds:Ljava/util/HashSet;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 106
    return-void
.end method


# virtual methods
.method public addCommandCallback(Lcom/samsung/dcm/framework/extractormanager/CommandListener;)V
    .locals 1
    .param p1, "callback"    # Lcom/samsung/dcm/framework/extractormanager/CommandListener;

    .prologue
    .line 214
    if-eqz p1, :cond_0

    .line 215
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_0
    return-void
.end method

.method public consolidate(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 10
    .param p1, "c"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 258
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    const-string v3, "Consolidating "

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, " with "

    aput-object v5, v4, v7

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    const-string v3, "Consolidating "

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, " with "

    aput-object v5, v4, v7

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    const/4 v1, 0x0

    .line 284
    .local v1, "operationChanged":Z
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isHighPriority()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isHighPriority()Z

    move-result v2

    if-nez v2, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->increasePriority()V

    .line 286
    const/4 v1, 0x1

    .line 289
    :cond_0
    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-eq v2, v3, :cond_1

    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-ne v2, v3, :cond_2

    .line 293
    :cond_1
    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 294
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    const-string v3, "Consolidating incoming INSERT with existing UPDATE"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    :cond_2
    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-eq v2, v3, :cond_3

    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-ne v2, v3, :cond_3

    .line 312
    :cond_3
    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 313
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 314
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    iget-object v3, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    invoke-static {v2, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->mergeBundles(Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 315
    .local v0, "newData":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    .line 325
    .end local v0    # "newData":Landroid/os/Bundle;
    :cond_4
    :goto_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIds:Ljava/util/HashSet;

    iget-object v3, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mIds:Ljava/util/HashSet;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 329
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    const-string v3, "final Consolidating "

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, " with "

    aput-object v5, v4, v7

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    iget-object v3, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 344
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    iget-object v3, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 347
    return v1

    .line 317
    :cond_5
    iget-object v2, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 373
    if-ne p0, p1, :cond_1

    .line 374
    const/4 v0, 0x1

    .line 380
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 375
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_0

    .line 377
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 380
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    check-cast p1, Lcom/samsung/dcm/framework/extractormanager/Command;

    .end local p1    # "obj":Ljava/lang/Object;
    iget-object v1, p1, Lcom/samsung/dcm/framework/extractormanager/Command;->mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public fireCommandCallback_onCommandAborted()V
    .locals 7

    .prologue
    .line 245
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    const-string v3, "Aborted: "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/CommandListener;

    .line 247
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/CommandListener;
    invoke-interface {v0, p0}, Lcom/samsung/dcm/framework/extractormanager/CommandListener;->onAborted(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    goto :goto_0

    .line 249
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/CommandListener;
    :cond_0
    return-void
.end method

.method public fireCommandCallback_onCommandCompleted()V
    .locals 7

    .prologue
    .line 238
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Command;->TAG:Ljava/lang/String;

    const-string v3, "Completed: "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/CommandListener;

    .line 240
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/CommandListener;
    invoke-interface {v0, p0}, Lcom/samsung/dcm/framework/extractormanager/CommandListener;->onCompleted(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    goto :goto_0

    .line 242
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/CommandListener;
    :cond_0
    return-void
.end method

.method public getCommandIds()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIds:Ljava/util/HashSet;

    return-object v0
.end method

.method public getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    return-object v0
.end method

.method public getData()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getDocType()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDocType:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getExtractors()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    return-object v0
.end method

.method public getOperation()Lcom/samsung/dcm/framework/extractormanager/Operation;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    return-object v0
.end method

.method public getStorageId()I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mStorageId:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->hashCode()I

    move-result v0

    return v0
.end method

.method public increasePriority()V
    .locals 2

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Command$1;->$SwitchMap$com$samsung$dcm$framework$extractormanager$Operation:[I

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Operation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 141
    :goto_0
    return-void

    .line 131
    :pswitch_0
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0

    .line 134
    :pswitch_1
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0

    .line 137
    :pswitch_2
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public interrupt()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mInterrupted:Z

    .line 118
    return-void
.end method

.method public isBeingProcessed()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mBusy:Z

    return v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mStorageId:I

    if-lez v0, :cond_0

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v0

    iget v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mStorageId:I

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/configuration/StorageController;->getVolumeStatus(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mBusy:Z

    goto :goto_0
.end method

.method public isDeleteFamilyCommand()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Operation;->isDeleteFamilyOperation()Z

    move-result v0

    return v0
.end method

.method public isHighPriority()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Operation;->isHighPriority()Z

    move-result v0

    return v0
.end method

.method public isInsertFamilyCommand()Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Operation;->isInsertFamilyOperation()Z

    move-result v0

    return v0
.end method

.method public isInterrupted()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mInterrupted:Z

    return v0
.end method

.method public isLowPriority()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Operation;->isLowPriority()Z

    move-result v0

    return v0
.end method

.method public isSdCardAction()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIsSdCardAction:Z

    return v0
.end method

.method public isUpdateFamilyCommand()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Operation;->isUpdateFamilyOperation()Z

    move-result v0

    return v0
.end method

.method public removeCommandCallback(Lcom/samsung/dcm/framework/extractormanager/CommandListener;)V
    .locals 1
    .param p1, "callback"    # Lcom/samsung/dcm/framework/extractormanager/CommandListener;

    .prologue
    .line 232
    if-eqz p1, :cond_0

    .line 233
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mCallbacks:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 235
    :cond_0
    return-void
.end method

.method public setBusy(Z)V
    .locals 0
    .param p1, "busy"    # Z

    .prologue
    .line 210
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mBusy:Z

    .line 211
    return-void
.end method

.method public setData(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 197
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    .line 198
    return-void
.end method

.method public setDocType(Lcom/samsung/dcm/documents/MediaDoc$DocType;)V
    .locals 1
    .param p1, "doctype"    # Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .prologue
    .line 201
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDocType:Lcom/google/common/base/Optional;

    .line 202
    return-void
.end method

.method public setExtractors(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 184
    if-eqz p1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 187
    :cond_0
    return-void
.end method

.method public varargs setExtractors([Lcom/samsung/dcm/framework/extractors/Extractor;)V
    .locals 5
    .param p1, "extractors"    # [Lcom/samsung/dcm/framework/extractors/Extractor;

    .prologue
    .line 190
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 191
    move-object v0, p1

    .local v0, "arr$":[Lcom/samsung/dcm/framework/extractors/Extractor;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 192
    .local v1, "e":Lcom/samsung/dcm/framework/extractors/Extractor;
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mExtractors:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 191
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 194
    .end local v1    # "e":Lcom/samsung/dcm/framework/extractors/Extractor;
    :cond_0
    return-void
.end method

.method public setIsSdCardAction(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 224
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mIsSdCardAction:Z

    .line 225
    return-void
.end method

.method public setOperation(Lcom/samsung/dcm/framework/extractormanager/Operation;)V
    .locals 0
    .param p1, "operation"    # Lcom/samsung/dcm/framework/extractormanager/Operation;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 207
    return-void
.end method

.method public setStorageId(I)V
    .locals 0
    .param p1, "storageId"    # I

    .prologue
    .line 406
    iput p1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mStorageId:I

    .line 407
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 386
    .local v0, "s":Ljava/lang/StringBuilder;
    const-string v1, "DCM ID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mDCM_ID:Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 388
    const-string v1, ", operation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mOperation:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 390
    const-string v1, ", interrupted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mInterrupted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 392
    const-string v1, ", busy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 394
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 395
    const-string v1, ", bundle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Command;->mData:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 398
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
