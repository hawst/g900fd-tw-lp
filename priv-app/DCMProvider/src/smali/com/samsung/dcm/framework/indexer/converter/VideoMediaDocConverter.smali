.class public Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;
.super Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;
.source "VideoMediaDocConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V
    .locals 18
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->convertCommon2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V

    .line 115
    move-object/from16 v0, p1

    instance-of v11, v0, Lcom/samsung/dcm/documents/VideoMediaDoc;

    if-nez v11, :cond_1

    .line 116
    sget-object v11, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->TAG:Ljava/lang/String;

    const-string v12, "mediadoc not instance of VideoMediaDoc, Cannot convert"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    :cond_0
    return-void

    :cond_1
    move-object/from16 v10, p1

    .line 120
    check-cast v10, Lcom/samsung/dcm/documents/VideoMediaDoc;

    .line 122
    .local v10, "videoDoc":Lcom/samsung/dcm/documents/VideoMediaDoc;
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v11

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v11}, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->storeDocType(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc$DocType;)V

    .line 124
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getAlbum()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 125
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "album"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getAlbum()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 127
    :cond_2
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getArtist()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 128
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "artist"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getArtist()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 130
    :cond_3
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getDescription()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 131
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "description"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getDescription()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 134
    :cond_4
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getDuration()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 135
    new-instance v12, Lorg/apache/lucene/document/LongField;

    const-string v13, "duration"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getDuration()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sget-object v11, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v14, v15, v11}, Lorg/apache/lucene/document/LongField;-><init>(Ljava/lang/String;JLorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 137
    :cond_5
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLanguage()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 138
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "language"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLanguage()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 140
    :cond_6
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getResolution()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 141
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "resolution"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getResolution()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 144
    :cond_7
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getBucketDisplayName()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 145
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "bucket"

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getBucketDisplayName()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 149
    :cond_8
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 150
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->mSpatialContext:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/location/Location;

    invoke-virtual {v11}, Landroid/location/Location;->getLongitude()D

    move-result-wide v14

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/location/Location;

    invoke-virtual {v11}, Landroid/location/Location;->getLatitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v12, v14, v15, v0, v1}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v9

    .line 153
    .local v9, "point":Lcom/spatial4j/core/shape/Shape;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->mSpatialStrategy:Lorg/apache/lucene/spatial/SpatialStrategy;

    invoke-virtual {v11, v9}, Lorg/apache/lucene/spatial/SpatialStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;

    move-result-object v2

    .local v2, "arr$":[Lorg/apache/lucene/document/Field;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_9

    aget-object v5, v2, v6

    .line 155
    .local v5, "f":Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 153
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 159
    .end local v2    # "arr$":[Lorg/apache/lucene/document/Field;
    .end local v5    # "f":Lorg/apache/lucene/index/IndexableField;
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "point":Lcom/spatial4j/core/shape/Shape;
    :cond_9
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getNamedPlace()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 160
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getNamedPlace()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/dcm/documents/LocationData;

    .line 162
    .local v7, "lData":Lcom/samsung/dcm/documents/LocationData;
    new-instance v12, Lorg/apache/lucene/document/StringField;

    const-string v13, "named_location"

    invoke-virtual {v7}, Lcom/samsung/dcm/documents/LocationData;->getLocality()Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v12, v13, v11, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 167
    .end local v7    # "lData":Lcom/samsung/dcm/documents/LocationData;
    :cond_a
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getEvents()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_0

    .line 168
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getEvents()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/documents/EventData;

    .line 169
    .local v3, "event":Lcom/samsung/dcm/documents/EventData;
    invoke-virtual {v3}, Lcom/samsung/dcm/documents/EventData;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "eventTitle":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_b

    .line 171
    new-instance v11, Lorg/apache/lucene/document/StringField;

    const-string v12, "calendar_event"

    sget-object v13, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v11, v12, v4, v13}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 173
    :cond_b
    new-instance v11, Lorg/apache/lucene/document/TextField;

    const-string v12, "event_uri"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/samsung/dcm/documents/EventData;->getUri()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v11, v12, v13, v14}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_1
.end method

.method public convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V
    .locals 11
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V

    .line 51
    instance-of v8, p2, Lcom/samsung/dcm/documents/VideoMediaDoc;

    if-nez v8, :cond_1

    .line 52
    sget-object v8, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;->TAG:Ljava/lang/String;

    const-string v9, "mediadoc not instance of VideoMediaDoc, Cannot convert"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    :cond_0
    return-void

    :cond_1
    move-object v7, p2

    .line 56
    check-cast v7, Lcom/samsung/dcm/documents/VideoMediaDoc;

    .line 58
    .local v7, "vmd":Lcom/samsung/dcm/documents/VideoMediaDoc;
    invoke-virtual {p1}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v0

    .line 59
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexableField;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexableField;

    .line 60
    .local v3, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "artist"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 61
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setArtist(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_3
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "album"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 63
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setAlbum(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_4
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "description"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 65
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_5
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "duration"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 67
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setDuration(Ljava/lang/Long;)V

    goto :goto_0

    .line 68
    :cond_6
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "language"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 69
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setLanguage(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_7
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "resolution"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 71
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setResolution(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 72
    :cond_8
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "bucket"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 73
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setBucketDisplayName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :cond_9
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "location__x"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 76
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    .line 78
    .local v6, "lon":Ljava/lang/String;
    const-string v8, "location__y"

    invoke-virtual {p1, v8}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "lat":Ljava/lang/String;
    const/4 v1, 0x0

    .line 81
    .local v1, "geo":Landroid/location/Location;
    if-eqz v4, :cond_2

    if-eqz v6, :cond_2

    .line 82
    new-instance v1, Landroid/location/Location;

    .end local v1    # "geo":Landroid/location/Location;
    const-string v8, "DCM_Provider"

    invoke-direct {v1, v8}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v1    # "geo":Landroid/location/Location;
    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    .line 84
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 85
    invoke-virtual {v7, v1}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setLocation(Landroid/location/Location;)V

    goto/16 :goto_0

    .line 88
    .end local v1    # "geo":Landroid/location/Location;
    .end local v4    # "lat":Ljava/lang/String;
    .end local v6    # "lon":Ljava/lang/String;
    :cond_a
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "named_location"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 93
    new-instance v5, Lcom/samsung/dcm/documents/LocationData;

    invoke-direct {v5}, Lcom/samsung/dcm/documents/LocationData;-><init>()V

    .line 94
    .local v5, "location":Lcom/samsung/dcm/documents/LocationData;
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/samsung/dcm/documents/LocationData;->setLocality(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v7, v5}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setNamedPlace(Lcom/samsung/dcm/documents/LocationData;)V

    goto/16 :goto_0

    .line 98
    .end local v5    # "location":Lcom/samsung/dcm/documents/LocationData;
    :cond_b
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 99
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setId(I)V

    goto/16 :goto_0
.end method
