.class Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
.super Ljava/lang/Object;
.source "StorageController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/configuration/StorageController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StorageInformation"
.end annotation


# instance fields
.field protected mFatVolumeId:I

.field protected mPath:Ljava/lang/String;

.field protected mSubsystem:Ljava/lang/String;

.field protected mbIsReady:Z

.field protected mbMounted:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 1
    .param p1, "mPath"    # Ljava/lang/String;
    .param p2, "mbMounted"    # Z
    .param p3, "mSubsystem"    # Ljava/lang/String;
    .param p4, "fatVolumeId"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mPath:Ljava/lang/String;

    .line 56
    iput-boolean p2, p0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbMounted:Z

    .line 57
    iput-object p3, p0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mSubsystem:Ljava/lang/String;

    .line 58
    iput p4, p0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mFatVolumeId:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbIsReady:Z

    .line 60
    return-void
.end method
