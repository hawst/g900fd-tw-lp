.class public Lcom/samsung/dcm/framework/extractormanager/DCMController;
.super Ljava/lang/Object;
.source "DCMController.java"

# interfaces
.implements Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/DCMController$4;,
        Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;
    }
.end annotation


# static fields
.field private static final CALENDAR_PROVIDER:Ljava/lang/String; = "package:com.android.providers.calendar"

.field private static final CONTACT_PROVIDER:Ljava/lang/String; = "package:com.android.providers.contacts"

.field public static final HEAVY_EX_THREAD_COUNT:I = 0x1

.field public static final HEAVY_TASKS_STOP_THRESHOLD:I = 0x0

.field private static final SLEEP_FOR_SETUP_WIZARD:I = 0x64

.field public static final SYNCH_THREAD_COUNT:I = 0x1

.field public static final TAG:Ljava/lang/String;

.field public static final WORKER_THREAD_COUNT:I = 0x3


# instance fields
.field private mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

.field private mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

.field private mContainerListener:Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;

.field private mContext:Landroid/content/Context;

.field private mDCMCPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

.field private mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

.field private mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

.field private mExecutorListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

.field private mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

.field private mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

.field private mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

.field mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field mRegisteredSetupWizEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field private mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

.field private mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

.field private mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

.field private mWorkerTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mbManagerReady:Z

.field private mbManagerSetupDone:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "policyManager"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    .param p3, "receiver"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_NETWORK_CHANGED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 95
    new-array v0, v5, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE2:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mRegisteredSetupWizEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 110
    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mWorkerTasks:Ljava/util/List;

    .line 115
    iput-object v7, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    .line 117
    iput-object v7, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    .line 124
    iput-boolean v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerReady:Z

    .line 126
    const-string v0, "FINISH"

    const-string v1, "persist.sys.setupwizard"

    const-string v2, "FINISH"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    .line 134
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mExecutorListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    .line 173
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContainerListener:Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;

    .line 251
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    .line 252
    const-string v0, "Policy manager is null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMCPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 253
    const-string v0, "SystemBroadcastReceiver is null"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 254
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    .line 255
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContainerListener:Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->addStatusListener(Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;)V

    .line 256
    new-instance v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    .line 257
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->setContentResolver(Landroid/content/ContentResolver;)V

    .line 258
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/DCMContainer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/dcm/framework/extractormanager/DCMController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .param p1, "x1"    # Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    .param p2, "x2"    # Ljava/lang/Throwable;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->markTaskAsFinished(Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V

    return-void
.end method

.method private createAndSetupExecutors()V
    .locals 4

    .prologue
    .line 334
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mExecutorListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getPrimaryFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    move-result-object v2

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getPrimaryQueue()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;Ljava/util/concurrent/LinkedBlockingQueue;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    .line 337
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mExecutorListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getSyncFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    move-result-object v2

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getSyncQueue()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;Ljava/util/concurrent/LinkedBlockingQueue;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    .line 340
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mExecutorListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getHeavyFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    move-result-object v2

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getHeavyQueue()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;Ljava/util/concurrent/LinkedBlockingQueue;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    .line 343
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mExecutorListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getCommandProcessorFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    move-result-object v2

    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getCommandProcessorQueue()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;Ljava/util/concurrent/LinkedBlockingQueue;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    .line 345
    return-void
.end method

.method private createWorkerTasks()V
    .locals 5

    .prologue
    .line 349
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v3, "Creating tasks"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 351
    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker;

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    invoke-direct {v1, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;)V

    .line 352
    .local v1, "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/Worker;
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mWorkerTasks:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 354
    .end local v1    # "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/Worker;
    :cond_0
    return-void
.end method

.method private handleCleanup()V
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->cleanUp()V

    .line 681
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 682
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 683
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 684
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 686
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->close()V

    .line 687
    return-void
.end method

.method private markTaskAsFinished(Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "task"    # Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    if-nez p2, :cond_0

    .line 208
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setCalendarProcessingStarted(Landroid/content/Context;Z)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->doStorageDBCleanup()V

    goto :goto_0
.end method

.method private onNetworkStatusChanged()V
    .locals 7

    .prologue
    .line 739
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v5, "setIsConnectedForExtractors"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 740
    const/4 v3, 0x0

    .line 742
    .local v3, "mbInternetStatus":Z
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 745
    .local v0, "connectivity":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 746
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v2

    .line 747
    .local v2, "inf":[Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 748
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_0

    .line 749
    aget-object v4, v2, v1

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    sget-object v5, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v4, v5, :cond_1

    .line 750
    const/4 v3, 0x1

    .line 757
    .end local v1    # "i":I
    .end local v2    # "inf":[Landroid/net/NetworkInfo;
    :cond_0
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 758
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onConnectionEnabled()V

    .line 763
    :goto_1
    return-void

    .line 748
    .restart local v1    # "i":I
    .restart local v2    # "inf":[Landroid/net/NetworkInfo;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 761
    .end local v1    # "i":I
    .end local v2    # "inf":[Landroid/net/NetworkInfo;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onConnectionDisabled()V

    goto :goto_1
.end method

.method private sendRebuildRequest(Z)V
    .locals 1
    .param p1, "rebuildLocation"    # Z

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->postDBrebuildIntent(Z)Z

    .line 330
    :cond_0
    return-void
.end method

.method private setExecutorState(Z)V
    .locals 5
    .param p1, "pause"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 308
    if-eqz p1, :cond_0

    .line 309
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "setExecutorState "

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "pause"

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pause()V

    .line 311
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pause()V

    .line 312
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pause()V

    .line 313
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pause()V

    .line 323
    :goto_0
    return-void

    .line 316
    :cond_0
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "setExecutorState "

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "resume"

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->resume()V

    .line 318
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->resume()V

    .line 319
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->resume()V

    .line 320
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->resume()V

    goto :goto_0
.end method


# virtual methods
.method public appendCommands(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 779
    .local p1, "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    const-string v0, "List cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-direct {v1, p0, p1, v2}, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)V

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 782
    return-void
.end method

.method public execute(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V
    .locals 27
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "startId"    # I
    .param p3, "listener"    # Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .prologue
    .line 375
    const-string v3, "bundle is null"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 378
    .local v10, "actionValue":Ljava/lang/String;
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Package:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 379
    .local v25, "packageName":Ljava/lang/String;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "package name in controller : "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v25, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    invoke-static {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    move-result-object v9

    .line 382
    .local v9, "action":Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    if-nez v9, :cond_1

    .line 383
    if-eqz p3, :cond_0

    .line 384
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "action is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "execute() : startId : "

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, ", action : "

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object v9, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController$4;->$SwitchMap$com$samsung$dcm$framework$IntentHandler$ValidAction:[I

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 394
    :pswitch_0
    const-string v3, "package:com.android.providers.calendar"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "package:com.android.providers.contacts"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 395
    :cond_2
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 396
    .local v21, "mRemoveField":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v24, Lcom/samsung/dcm/documents/PackageFieldMapping;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/dcm/documents/PackageFieldMapping;-><init>()V

    .line 397
    .local v24, "packageFieldObject":Lcom/samsung/dcm/documents/PackageFieldMapping;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/dcm/documents/PackageFieldMapping;->getMapping()Ljava/util/HashMap;

    move-result-object v20

    .line 398
    .local v20, "mHmap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 399
    .local v15, "fields":Ljava/lang/String;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "fields in controller : "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    const-string v3, "#"

    invoke-virtual {v15, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 401
    .local v16, "fields_to_remove":[Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->clear()V

    .line 402
    move-object/from16 v12, v16

    .local v12, "arr$":[Ljava/lang/String;
    array-length v0, v12

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_3

    aget-object v14, v12, v17

    .line 403
    .local v14, "field":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 406
    .end local v14    # "field":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    new-instance v4, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-direct {v4, v5, v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 409
    if-eqz p3, :cond_0

    .line 410
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    goto/16 :goto_0

    .line 416
    .end local v12    # "arr$":[Ljava/lang/String;
    .end local v15    # "fields":Ljava/lang/String;
    .end local v16    # "fields_to_remove":[Ljava/lang/String;
    .end local v17    # "i$":I
    .end local v18    # "len$":I
    .end local v20    # "mHmap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v21    # "mRemoveField":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v24    # "packageFieldObject":Lcom/samsung/dcm/documents/PackageFieldMapping;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    .line 417
    .local v13, "context":Landroid/content/Context;
    const-string v3, "activity"

    invoke-virtual {v13, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/ActivityManager;

    .line 420
    .local v11, "am":Landroid/app/ActivityManager;
    invoke-virtual {v11}, Landroid/app/ActivityManager;->clearApplicationUserData()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 421
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "Successfully requested AM to clear the User Data"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    :goto_2
    if-eqz p3, :cond_5

    .line 427
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    .line 429
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->shutdown()V

    goto/16 :goto_0

    .line 423
    :cond_6
    const/4 v3, 0x0

    invoke-static {v13, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto :goto_2

    .line 435
    .end local v11    # "am":Landroid/app/ActivityManager;
    .end local v13    # "context":Landroid/content/Context;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 436
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v22

    .line 439
    .local v22, "options":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Landroid/os/Bundle;>;"
    const/16 v19, 0x0

    .line 440
    .local v19, "locationRebuild":Z
    invoke-virtual/range {v22 .. v22}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 441
    invoke-virtual/range {v22 .. v22}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->RebuildLocation:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    .line 444
    :cond_7
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "Starting DbRebuild  with locationRebuild="

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->setLocationRebuild(Z)V

    .line 447
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->setStartId(I)V

    .line 448
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->clearListeners()V

    .line 449
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->addListener(Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    .line 450
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 452
    .end local v19    # "locationRebuild":Z
    .end local v22    # "options":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Landroid/os/Bundle;>;"
    :cond_8
    if-eqz p3, :cond_0

    .line 453
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "DbRebuilder is already running"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 459
    :pswitch_2
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "active threads = "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->getActiveCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 460
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v3

    if-gtz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 462
    :cond_9
    if-eqz p3, :cond_0

    .line 463
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Tasks at work; NOT starting OSC"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 468
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->clearListeners()V

    .line 470
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->addListener(Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    .line 471
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    new-instance v4, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;)V

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->addListener(Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    .line 472
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->setStartId(I)V

    .line 473
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    .line 474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMCPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 476
    :cond_b
    if-eqz p3, :cond_0

    .line 477
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "OSC is already running; NOT starting OSC"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 484
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setCalendarProcessingStarted(Landroid/content/Context;Z)V

    .line 485
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->addNewBundle(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)Z

    .line 486
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    .line 487
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->cancelTasks([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)V

    goto/16 :goto_0

    .line 491
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    const v4, 0x7fffffff

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->setHeavyLimit(I)V

    .line 492
    if-eqz p3, :cond_0

    .line 493
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    goto/16 :goto_0

    .line 499
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCommandProcessorExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-object/from16 v26, v0

    new-instance v3, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    const-string v4, "storage_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v4, p0

    move-object/from16 v7, p3

    move/from16 v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/DCMContainer;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 505
    :pswitch_6
    new-instance v23, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    const-string v4, "storage_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "isExternalSDCard"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    move-object/from16 v0, v23

    invoke-direct {v0, v3, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;-><init>(Landroid/content/Context;IZ)V

    .line 508
    .local v23, "osUpgradeDBRebuilderTask":Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;
    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->setStartId(I)V

    .line 509
    move-object/from16 v0, v23

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->addListener(Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    .line 510
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 391
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public execute(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 8
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 360
    const-string v2, "command is null"

    invoke-static {p1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v3, "execute() : "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v2, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 366
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mWorkerTasks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    .line 367
    .local v1, "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    new-array v3, v7, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    aput-object v1, v3, v6

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 368
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 372
    .end local v1    # "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    :cond_1
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public indexAndGet(Landroid/net/Uri;)Z
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 522
    const-string v8, "uri is null"

    invoke-static {p1, v8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    sget-object v8, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v9, "indexAndGet(), uri : "

    new-array v10, v11, [Ljava/lang/Object;

    aput-object p1, v10, v7

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 525
    iget-boolean v8, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    if-nez v8, :cond_0

    .line 526
    sget-object v8, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v9, "setup not done cannot proceed indexAndGet(), uri : "

    new-array v10, v11, [Ljava/lang/Object;

    aput-object p1, v10, v7

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 567
    :goto_0
    return v7

    .line 530
    :cond_0
    new-instance v3, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-direct {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;)V

    .line 531
    .local v3, "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v4, v11}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 532
    .local v4, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v6, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 533
    .local v6, "result":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/Boolean;>;"
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 536
    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;-><init>(Lcom/samsung/dcm/framework/configuration/ExtractorsController;)V

    .line 537
    .local v1, "commandFactory":Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
    sget-object v8, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    invoke-virtual {v1, v3, v8}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 538
    .local v0, "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    new-instance v8, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;

    invoke-direct {v8, p0, v6, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v0, v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->addCommandCallback(Lcom/samsung/dcm/framework/extractormanager/CommandListener;)V

    .line 554
    iget-object v8, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-static {v8, v7}, Lcom/samsung/dcm/framework/utils/SystemUtils;->setHeavySharedPref(Landroid/content/Context;Z)V

    .line 555
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 556
    const/4 v5, 0x0

    .line 558
    .local v5, "latchsucess":Z
    const-wide/16 v8, 0x1388

    :try_start_0
    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v8, v9, v10}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v5

    .line 560
    if-nez v5, :cond_1

    .line 561
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :cond_1
    :goto_1
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    goto :goto_0

    .line 563
    :catch_0
    move-exception v2

    .line 564
    .local v2, "e":Ljava/lang/InterruptedException;
    sget-object v8, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v9, "latch await exception"

    invoke-static {v8, v9, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 565
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public declared-synchronized intialize()V
    .locals 8

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerReady:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 305
    :goto_0
    monitor-exit p0

    return-void

    .line 270
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->createAndSetupExecutors()V

    .line 271
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->createWorkerTasks()V

    .line 273
    new-instance v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    invoke-direct {v3}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    .line 275
    new-instance v3, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    .line 277
    new-instance v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    invoke-direct {v3}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    .line 278
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    new-instance v4, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    invoke-direct {v4, p0, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;)V

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->addListener(Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    .line 280
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerReady:Z

    .line 282
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->isCommitOk(Landroid/content/Context;)Z

    move-result v0

    .line 283
    .local v0, "isCommitOk":Z
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->isIntentProcessingFinished(Landroid/content/Context;)Z

    move-result v1

    .line 284
    .local v1, "isIntentFinished":Z
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    move-result v2

    .line 286
    .local v2, "isRebuildDone":Z
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "isCommitOk: "

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, ", isIntentFinished: "

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "isRebuildDone = "

    aput-object v7, v5, v6

    const/4 v6, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-nez v2, :cond_2

    .line 290
    :cond_1
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->sendRebuildRequest(Z)V

    .line 294
    :cond_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->setContext(Landroid/content/Context;)V

    .line 296
    iget-boolean v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    if-eqz v3, :cond_3

    .line 297
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v3, p0, v4}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 266
    .end local v0    # "isCommitOk":Z
    .end local v1    # "isIntentFinished":Z
    .end local v2    # "isRebuildDone":Z
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 299
    .restart local v0    # "isCommitOk":Z
    .restart local v1    # "isIntentFinished":Z
    .restart local v2    # "isRebuildDone":Z
    :cond_3
    :try_start_2
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v4, "Pausing all executors"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " Setup not yet complete"

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->setExecutorState(Z)V

    .line 301
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mRegisteredSetupWizEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v3, p0, v4}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public isShutDownStarted()Z
    .locals 1

    .prologue
    .line 596
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->isSystemOnShutdown()Z

    move-result v0

    return v0
.end method

.method public onBulkDeleteCompleted()V
    .locals 4

    .prologue
    .line 769
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mWorkerTasks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    .line 770
    .local v1, "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 772
    .end local v1    # "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    :cond_0
    return-void
.end method

.method public onConnectionDisabled()V
    .locals 3

    .prologue
    .line 663
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "onConnectionDisabled()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 664
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->onLostInternetConnection()V

    .line 666
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->getLocationRebuild()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->finishNow()V

    .line 669
    :cond_0
    return-void
.end method

.method public onConnectionEnabled()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 653
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "onConnectionEnabled()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 654
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->onReconnectToInternet()V

    .line 655
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "sending database rebuild intent (rebuild location=true)"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 656
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->sendRebuildRequest(Z)V

    .line 657
    return-void
.end method

.method public onIntentReceived(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "eventid"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 692
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v2, "onIntentReceived eventid ="

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 694
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerReady:Z

    if-nez v1, :cond_1

    .line 695
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v2, "MediaManager is not yet ready !"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 699
    :cond_1
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE2:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 702
    :cond_2
    :try_start_0
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v2, "SLEEP_FOR_SETUP_WIZARD "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, " starting"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 703
    const-wide/16 v2, 0x64

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 704
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v2, "SLEEP_FOR_SETUP_WIZARD "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, " Done"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 708
    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->setExecutorState(Z)V

    goto :goto_0

    .line 705
    :catch_0
    move-exception v0

    .line 706
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v2, "Thread.sleep crashed"

    invoke-static {v1, v2, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 708
    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->setExecutorState(Z)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->setExecutorState(Z)V

    throw v1

    .line 711
    :cond_3
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 712
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onScreenOn()V

    goto :goto_0

    .line 714
    :cond_4
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 715
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onScreenOff()V

    goto :goto_0

    .line 717
    :cond_5
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 718
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->shutdown()V

    goto :goto_0

    .line 720
    :cond_6
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 721
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onScreenOff()V

    goto :goto_0

    .line 723
    :cond_7
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 724
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onScreenOn()V

    goto/16 :goto_0

    .line 726
    :cond_8
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_NETWORK_CHANGED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 727
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onNetworkStatusChanged()V

    goto/16 :goto_0
.end method

.method public onOOM()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 624
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "onOOM()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 625
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->handleCleanup()V

    .line 626
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 628
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    .line 629
    return-void
.end method

.method public onScreenOff()V
    .locals 3

    .prologue
    .line 644
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "onScreenOff(), pause executors"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 645
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pause()V

    .line 646
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pause()V

    .line 647
    return-void
.end method

.method public onScreenOn()V
    .locals 3

    .prologue
    .line 635
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "onScreenOn(), resume executors"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 636
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->resume()V

    .line 637
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSyncExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->resume()V

    .line 638
    return-void
.end method

.method public pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V
    .locals 3
    .param p1, "policy"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    .prologue
    .line 600
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pausing heavy executors by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 601
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMCPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 602
    return-void
.end method

.method public postOscStartIntent(Z)V
    .locals 6
    .param p1, "reset"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 571
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "postOscStartIntent(), reset : "

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 573
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    if-nez v0, :cond_1

    .line 574
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "postOscStartIntent(), setupwaiting = : "

    new-array v2, v5, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 579
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    new-array v1, v5, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->postHeavyStartIntent(Z)Z

    goto :goto_0

    .line 583
    :cond_2
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "postOscStartIntent(), reset : "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, " NOT SENT"

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V
    .locals 3
    .param p1, "policy"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    .prologue
    .line 605
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resuming heavy executors by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 606
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMCPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 607
    return-void
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 614
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "shutdown()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 615
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setSystemOnShutdown(Z)V

    .line 616
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->handleCleanup()V

    .line 617
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    .line 618
    return-void
.end method

.method public startWorkerThreads()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 785
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v3, "startWorkerThreads called by "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 786
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mWorkerTasks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    .line 787
    .local v1, "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    new-array v3, v7, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    aput-object v1, v3, v6

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 788
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 791
    .end local v1    # "workerTask":Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
    :cond_1
    return-void
.end method
