.class public final Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;
.super Ljava/lang/Object;
.source "BundleFieldsMapping.java"


# static fields
.field public static final MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    .line 43
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "event_uri"

    const-string v2, "event_uri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "event_title"

    const-string v2, "calendar_event"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "location"

    const-string v2, "named_location"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "scene_type"

    const-string v2, "scene_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "subscene_type"

    const-string v2, "subscene_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "user_tag"

    const-string v2, "user_tags"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_DCM_FIELDS:Ljava/util/Map;

    const-string v1, "person"

    const-string v2, "person_names"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    const-string v1, "event_title"

    const-string v2, "root_calendar_event"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    const-string v1, "location"

    const-string v2, "root_named_location"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    const-string v1, "scene_type"

    const-string v2, "root_scene_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    const-string v1, "subscene_type"

    const-string v2, "root_subscene_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    const-string v1, "user_tag"

    const-string v2, "root_user_tag"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/BundleFieldsMapping;->MAP_BUNDLE_TO_CATEGORY_FIELDS:Ljava/util/Map;

    const-string v1, "person"

    const-string v2, "root_person"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
