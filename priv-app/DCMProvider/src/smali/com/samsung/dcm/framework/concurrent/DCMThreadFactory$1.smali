.class Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory$1;
.super Ljava/lang/Object;
.source "DCMThreadFactory.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory$1;->this$0:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 91
    const-class v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Thread crashed "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 93
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
