.class public Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;
.super Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;
.source "AudioMediaDocConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V
    .locals 5
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0, p1, p2}, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;->convertCommon2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V

    .line 74
    instance-of v1, p1, Lcom/samsung/dcm/documents/AudioMediaDoc;

    if-nez v1, :cond_1

    .line 75
    sget-object v1, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;->TAG:Ljava/lang/String;

    const-string v2, "mediadoc not instance of AudioMediaDoc, Cannot convert"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 78
    check-cast v0, Lcom/samsung/dcm/documents/AudioMediaDoc;

    .line 80
    .local v0, "audioDoc":Lcom/samsung/dcm/documents/AudioMediaDoc;
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;->storeDocType(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc$DocType;)V

    .line 82
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getAlbum()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 83
    new-instance v2, Lorg/apache/lucene/document/StringField;

    const-string v3, "album"

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getAlbum()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v2, v3, v1, v4}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 85
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getArtist()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 86
    new-instance v2, Lorg/apache/lucene/document/StringField;

    const-string v3, "artist"

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getArtist()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v2, v3, v1, v4}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 88
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getGenre()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 89
    new-instance v2, Lorg/apache/lucene/document/StringField;

    const-string v3, "genre"

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getGenre()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v2, v3, v1, v4}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 91
    :cond_4
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getYear()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    new-instance v2, Lorg/apache/lucene/document/IntField;

    const-string/jumbo v3, "year"

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getYear()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v4, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v2, v3, v1, v4}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p2, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0
.end method

.method public convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V
    .locals 7
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V

    .line 39
    instance-of v4, p2, Lcom/samsung/dcm/documents/AudioMediaDoc;

    if-nez v4, :cond_1

    .line 40
    sget-object v4, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;->TAG:Ljava/lang/String;

    const-string v5, "mediadoc not instance of AudioMediaDoc, Cannot Convert"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    :cond_0
    return-void

    :cond_1
    move-object v0, p2

    .line 44
    check-cast v0, Lcom/samsung/dcm/documents/AudioMediaDoc;

    .line 46
    .local v0, "amd":Lcom/samsung/dcm/documents/AudioMediaDoc;
    invoke-virtual {p1}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v1

    .line 47
    .local v1, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexableField;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexableField;

    .line 48
    .local v3, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "artist"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 49
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setArtist(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_3
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "album"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 51
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setAlbum(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_4
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "year"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 53
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setYear(I)V

    goto :goto_0

    .line 54
    :cond_5
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "genre"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 55
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setGenre(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_6
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 57
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setId(I)V

    goto :goto_0
.end method
