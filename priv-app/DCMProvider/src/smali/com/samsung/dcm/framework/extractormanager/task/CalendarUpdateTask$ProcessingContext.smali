.class Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;
.super Ljava/lang/Object;
.source "CalendarUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProcessingContext"
.end annotation


# instance fields
.field currentId:I

.field endTime:J

.field startTime:J

.field title:Ljava/lang/String;

.field uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(IJJLandroid/net/Uri;Ljava/lang/String;)V
    .locals 4
    .param p1, "currentId"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .param p6, "uri"    # Landroid/net/Uri;
    .param p7, "title"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->currentId:I

    .line 166
    iput-wide v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    .line 167
    iput-wide v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    .line 168
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    .line 169
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->title:Ljava/lang/String;

    .line 158
    iput p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->currentId:I

    .line 159
    iput-wide p2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    .line 160
    iput-wide p4, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    .line 161
    iput-object p6, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    .line 162
    iput-object p7, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->title:Ljava/lang/String;

    .line 163
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProcessingContext<curId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->currentId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", endTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
