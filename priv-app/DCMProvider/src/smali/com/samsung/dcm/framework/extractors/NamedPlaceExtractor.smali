.class public Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;
.super Lcom/samsung/dcm/framework/extractors/Extractor;
.source "NamedPlaceExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;
    }
.end annotation


# static fields
.field private static final CAPACITY:I = 0x64

.field private static final TAG:Ljava/lang/String;

.field public static mCount:I

.field static sCache:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->sCache:Ljava/util/concurrent/atomic/AtomicReference;

    .line 46
    const/4 v0, 0x0

    sput v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractors/Extractor;-><init>()V

    .line 48
    return-void
.end method

.method static synthetic access$000(Landroid/location/Address;)Lcom/samsung/dcm/documents/LocationData;
    .locals 1
    .param p0, "x0"    # Landroid/location/Address;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->getLocationDataFromAddress(Landroid/location/Address;)Lcom/samsung/dcm/documents/LocationData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getFromLocation(Lcom/samsung/dcm/documents/LocationData;)Ljava/lang/String;
    .locals 7
    .param p1, "locationData"    # Lcom/samsung/dcm/documents/LocationData;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 281
    const/4 v0, 0x0

    .line 282
    .local v0, "namedLocation":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/LocationData;->getLocality()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/LocationData;->getLocality()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "namedLocation":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 296
    .restart local v0    # "namedLocation":Ljava/lang/String;
    :goto_0
    sget-object v1, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v2, "nameplace_to_be_added = "

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    return-object v0

    .line 285
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/LocationData;->getAdminArea()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/LocationData;->getAdminArea()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "namedLocation":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "namedLocation":Ljava/lang/String;
    goto :goto_0

    .line 288
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/LocationData;->getCountryName()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 289
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/LocationData;->getCountryName()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "namedLocation":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "namedLocation":Ljava/lang/String;
    goto :goto_0

    .line 292
    :cond_2
    sget-object v1, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v2, "nameplace_to_be_added"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "dummy data"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    const-string v0, ""

    goto :goto_0
.end method

.method private static getLocationDataFromAddress(Landroid/location/Address;)Lcom/samsung/dcm/documents/LocationData;
    .locals 2
    .param p0, "address"    # Landroid/location/Address;

    .prologue
    .line 179
    new-instance v0, Lcom/samsung/dcm/documents/LocationData;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/LocationData;-><init>()V

    .line 180
    .local v0, "locationData":Lcom/samsung/dcm/documents/LocationData;
    invoke-virtual {p0}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/documents/LocationData;->setCountryName(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/documents/LocationData;->setAdminArea(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/documents/LocationData;->setLocality(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/documents/LocationData;->setStreetName(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Landroid/location/Address;->getSubThoroughfare()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/documents/LocationData;->setStreetNumber(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/documents/LocationData;->setPostalCode(Ljava/lang/String;)V

    .line 187
    return-object v0
.end method

.method private process_doc(Lorg/apache/lucene/document/Document;)Landroid/os/Bundle;
    .locals 17
    .param p1, "lucenedoc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 234
    const/4 v2, 0x0

    .line 235
    .local v2, "OUT":Landroid/os/Bundle;
    const-wide/16 v4, 0x0

    .line 236
    .local v4, "latitude":D
    const-wide/16 v10, 0x0

    .line 238
    .local v10, "longitude":D
    const-string v12, "location__x"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v7

    .line 247
    .local v7, "locationField":Lorg/apache/lucene/index/IndexableField;
    if-nez v7, :cond_2

    .line 248
    new-instance v2, Landroid/os/Bundle;

    .end local v2    # "OUT":Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 261
    .restart local v2    # "OUT":Landroid/os/Bundle;
    :cond_0
    :goto_0
    const-wide/16 v12, 0x0

    cmpl-double v12, v4, v12

    if-eqz v12, :cond_1

    const-wide/16 v12, 0x0

    cmpl-double v12, v10, v12

    if-eqz v12, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->isOnline()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 262
    sget-object v12, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v13, "Latitude ="

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, ", Longitude = "

    aput-object v16, v14, v15

    const/4 v15, 0x2

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    new-instance v9, Lcom/samsung/commons/PointD;

    invoke-direct {v9, v4, v5, v10, v11}, Lcom/samsung/commons/PointD;-><init>(DD)V

    .line 264
    .local v9, "point":Lcom/samsung/commons/PointD;
    sget-object v12, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->sCache:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;

    invoke-virtual {v12, v9}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/dcm/documents/LocationData;

    .line 266
    .local v6, "locationData":Lcom/samsung/dcm/documents/LocationData;
    if-eqz v6, :cond_1

    .line 267
    new-instance v2, Landroid/os/Bundle;

    .end local v2    # "OUT":Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 268
    .restart local v2    # "OUT":Landroid/os/Bundle;
    const-string v12, "nameplace_to_be_added"

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->getFromLocation(Lcom/samsung/dcm/documents/LocationData;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .end local v6    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .end local v9    # "point":Lcom/samsung/commons/PointD;
    :cond_1
    return-object v2

    .line 250
    :cond_2
    if-eqz v7, :cond_0

    .line 251
    invoke-interface {v7}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    .line 253
    .local v8, "lon":Ljava/lang/String;
    const-string v12, "location__y"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 255
    .local v3, "lat":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v8, :cond_0

    .line 256
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 257
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    goto :goto_0
.end method

.method private process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 14
    .param p1, "doc"    # Lcom/samsung/dcm/documents/ImageMediaDoc;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 101
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->isOnline()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 102
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "Begin image processing ..."

    new-array v7, v13, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    .line 104
    .local v2, "loc":Landroid/location/Location;
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "GEO data = "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v7, v12

    const-string v8, " "

    aput-object v8, v7, v13

    const/4 v8, 0x2

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    new-instance v4, Lcom/samsung/commons/PointD;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/samsung/commons/PointD;-><init>(DD)V

    .line 106
    .local v4, "point":Lcom/samsung/commons/PointD;
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->sCache:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;

    invoke-virtual {v5, v4}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/documents/LocationData;

    .line 108
    .local v3, "locationData":Lcom/samsung/dcm/documents/LocationData;
    if-nez v3, :cond_0

    .line 109
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "Could not determine location data"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    sget v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    .line 136
    .end local v2    # "loc":Landroid/location/Location;
    .end local v3    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .end local v4    # "point":Lcom/samsung/commons/PointD;
    :goto_0
    return-object p1

    .line 114
    .restart local v2    # "loc":Landroid/location/Location;
    .restart local v3    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .restart local v4    # "point":Lcom/samsung/commons/PointD;
    :cond_0
    sput v12, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    .line 115
    const/4 v0, 0x0

    .line 118
    .local v0, "cat":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->getFromLocation(Lcom/samsung/dcm/documents/LocationData;)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "cityName":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 120
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 121
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "root_named_location;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-virtual {p1, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->addCategory(Ljava/lang/String;)V

    .line 127
    :cond_1
    invoke-virtual {v3, v1}, Lcom/samsung/dcm/documents/LocationData;->setLocality(Ljava/lang/String;)V

    .line 129
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "City Name: "

    new-array v7, v13, [Ljava/lang/Object;

    aput-object v0, v7, v12

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    invoke-virtual {p1, v3}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setNamedPlace(Lcom/samsung/dcm/documents/LocationData;)V

    .line 132
    :cond_2
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "End image processing"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    .end local v0    # "cat":Ljava/lang/String;
    .end local v1    # "cityName":Ljava/lang/String;
    .end local v2    # "loc":Landroid/location/Location;
    .end local v3    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .end local v4    # "point":Lcom/samsung/commons/PointD;
    :cond_3
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "No location data"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private process_video(Lcom/samsung/dcm/documents/VideoMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 14
    .param p1, "doc"    # Lcom/samsung/dcm/documents/VideoMediaDoc;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 140
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->isOnline()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 141
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "Processing ..."

    new-array v7, v13, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    .line 143
    .local v2, "loc":Landroid/location/Location;
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "Location data = "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v7, v12

    const-string v8, " "

    aput-object v8, v7, v13

    const/4 v8, 0x2

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    new-instance v4, Lcom/samsung/commons/PointD;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/samsung/commons/PointD;-><init>(DD)V

    .line 145
    .local v4, "point":Lcom/samsung/commons/PointD;
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->sCache:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;

    invoke-virtual {v5, v4}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/documents/LocationData;

    .line 147
    .local v3, "locationData":Lcom/samsung/dcm/documents/LocationData;
    if-nez v3, :cond_0

    .line 148
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "Could not determine location data"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    sget v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    .line 174
    .end local v2    # "loc":Landroid/location/Location;
    .end local v3    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .end local v4    # "point":Lcom/samsung/commons/PointD;
    :goto_0
    return-object p1

    .line 153
    .restart local v2    # "loc":Landroid/location/Location;
    .restart local v3    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .restart local v4    # "point":Lcom/samsung/commons/PointD;
    :cond_0
    sput v12, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    .line 154
    const/4 v0, 0x0

    .line 157
    .local v0, "cat":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->getFromLocation(Lcom/samsung/dcm/documents/LocationData;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "cityName":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 159
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 160
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "root_named_location;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-virtual {p1, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->addCategory(Ljava/lang/String;)V

    .line 165
    :cond_1
    invoke-virtual {v3, v1}, Lcom/samsung/dcm/documents/LocationData;->setLocality(Ljava/lang/String;)V

    .line 167
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "City Name: "

    new-array v7, v13, [Ljava/lang/Object;

    aput-object v0, v7, v12

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    invoke-virtual {p1, v3}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setNamedPlace(Lcom/samsung/dcm/documents/LocationData;)V

    .line 170
    :cond_2
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "End video processing"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 172
    .end local v0    # "cat":Ljava/lang/String;
    .end local v1    # "cityName":Ljava/lang/String;
    .end local v2    # "loc":Landroid/location/Location;
    .end local v3    # "locationData":Lcom/samsung/dcm/documents/LocationData;
    .end local v4    # "point":Lcom/samsung/commons/PointD;
    :cond_3
    sget-object v5, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v6, "No location data"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public hashCode()I
    .locals 2

    .prologue
    .line 303
    const/16 v0, 0x11

    .line 304
    .local v0, "prime":I
    const/16 v1, 0x11

    return v1
.end method

.method public initialize()V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method protected initializeSupportedClasses()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mSupportedExtractors:Ljava/util/LinkedList;

    const-class v1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mSupportedExtractors:Ljava/util/LinkedList;

    const-class v1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public isOnline()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 191
    sget-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v1, "checking if device is online...."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mIsConnected:Z

    if-ne v0, v3, :cond_0

    sget v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    if-le v0, v3, :cond_0

    .line 194
    iput-boolean v4, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mIsConnected:Z

    .line 197
    :cond_0
    sget-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v1, "Is device online? "

    new-array v2, v3, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mIsConnected:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mIsConnected:Z

    return v0
.end method

.method public process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 209
    instance-of v1, p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    if-eqz v1, :cond_0

    .line 210
    check-cast p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v1

    .line 220
    :goto_0
    return-object v1

    .line 212
    .restart local p1    # "data":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    if-eqz v1, :cond_1

    .line 213
    check-cast p1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->process_video(Lcom/samsung/dcm/documents/VideoMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v1

    goto :goto_0

    .line 215
    .restart local p1    # "data":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/document/Document;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 217
    check-cast v0, Lorg/apache/lucene/document/Document;

    .line 218
    .local v0, "lucenedoc":Lorg/apache/lucene/document/Document;
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->process_doc(Lorg/apache/lucene/document/Document;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0

    .line 220
    .end local v0    # "lucenedoc":Lorg/apache/lucene/document/Document;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    const-string v0, "Context cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->sCache:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;

    const/16 v3, 0x64

    new-instance v4, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    invoke-direct {v2, v3, v4}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;-><init>(ILandroid/location/Geocoder;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method public setIsConnected(Z)V
    .locals 5
    .param p1, "flag"    # Z

    .prologue
    const/4 v4, 0x0

    .line 202
    sget-object v0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;

    const-string v1, "setIsConnected state ="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    sput v4, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mCount:I

    .line 204
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->mIsConnected:Z

    .line 205
    return-void
.end method
