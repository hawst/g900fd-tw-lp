.class Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;
.super Ljava/lang/Object;
.source "IntentHandler.java"

# interfaces
.implements Lcom/samsung/dcm/framework/extractormanager/CommandListener;
.implements Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CommandCallback"
.end annotation


# instance fields
.field private mPendingNotifications:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mUnfinishedIntents:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/dcm/framework/IntentHandler;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/IntentHandler;)V
    .locals 1

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->this$0:Lcom/samsung/dcm/framework/IntentHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mPendingNotifications:Ljava/util/HashMap;

    return-void
.end method

.method private declared-synchronized cleanFinished(I)V
    .locals 6
    .param p1, "startId"    # I

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cleanFinished(): startId="

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cleanFinished(): mUnfinishedIntents="

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    invoke-direct {p0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->tryToSweep()I

    move-result v0

    .line 211
    .local v0, "biggestFinishedId":I
    if-lez v0, :cond_1

    .line 212
    iget-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Last intent has been processed, setting finished flag to true"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->setProcessingIntentFinished(Z)V

    .line 217
    :cond_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Stopping service with ids up to "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    iget-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->this$0:Lcom/samsung/dcm/framework/IntentHandler;

    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->mService:Lcom/samsung/dcm/framework/FrameworkService;
    invoke-static {v1}, Lcom/samsung/dcm/framework/IntentHandler;->access$100(Lcom/samsung/dcm/framework/IntentHandler;)Lcom/samsung/dcm/framework/FrameworkService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/FrameworkService;->stopSelfResult(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :cond_1
    monitor-exit p0

    return-void

    .line 208
    .end local v0    # "biggestFinishedId":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized markFinished(Ljava/util/Set;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "tasks":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    monitor-enter p0

    const/4 v2, -0x1

    .local v2, "maxId":I
    const/4 v3, 0x0

    .line 173
    .local v3, "val":I
    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CommandCallback.markFinished called with tasks ="

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 176
    .local v1, "key":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-virtual {v4, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v3, v4, -0x1

    .line 177
    iget-object v4, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    if-nez v3, :cond_0

    .line 179
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    goto :goto_0

    .line 182
    .end local v1    # "key":Ljava/lang/Integer;
    :cond_1
    monitor-exit p0

    return v2

    .line 171
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private notify(I)V
    .locals 8
    .param p1, "startId"    # I

    .prologue
    .line 102
    monitor-enter p0

    .line 103
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mPendingNotifications:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 104
    .local v1, "notificationUri":Landroid/net/Uri;
    if-eqz v1, :cond_2

    .line 105
    const-class v2, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v2}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 106
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-nez v0, :cond_0

    .line 107
    monitor-exit p0

    .line 119
    .end local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    :goto_0
    return-void

    .line 108
    .restart local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    :cond_0
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->isVAEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v2

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateListner()Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->addRefreshListener(Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;)V

    .line 111
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "VA model, Notifying Provider with URI="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mPendingNotifications:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    :goto_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mPendingNotifications:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    .end local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    :goto_2
    monitor-exit p0

    goto :goto_0

    .end local v1    # "notificationUri":Landroid/net/Uri;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 114
    .restart local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    .restart local v1    # "notificationUri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Non-VA model, not notifying Provider with URI="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mPendingNotifications:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 117
    .end local v0    # "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    :cond_2
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NOT notifying provider as startId ="

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, " was not registered for notifications"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method private setProcessingIntentFinished(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->this$0:Lcom/samsung/dcm/framework/IntentHandler;

    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->mService:Lcom/samsung/dcm/framework/FrameworkService;
    invoke-static {v0}, Lcom/samsung/dcm/framework/IntentHandler;->access$100(Lcom/samsung/dcm/framework/IntentHandler;)Lcom/samsung/dcm/framework/FrameworkService;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIntentProcessingFinished(Landroid/content/Context;Z)V

    .line 224
    return-void
.end method

.method private declared-synchronized tryToSweep()I
    .locals 7

    .prologue
    .line 186
    monitor-enter p0

    const/4 v2, -0x1

    .line 188
    .local v2, "iRet":I
    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 190
    .local v3, "toSweep":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 191
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_0

    .line 192
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 193
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->notify(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 186
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "toSweep":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 199
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "toSweep":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_0
    :try_start_1
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 200
    .local v4, "val":I
    iget-object v5, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 202
    goto :goto_1

    .line 204
    .end local v4    # "val":I
    :cond_1
    monitor-exit p0

    return v2
.end method


# virtual methods
.method public onAborted(ILjava/lang/Throwable;)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 159
    monitor-enter p0

    .line 160
    if-eqz p2, :cond_0

    .line 161
    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CommandCallback.onAborted(): Task ("

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, ") has been aborted with exception : "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    :goto_0
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->markFinished(Ljava/util/Set;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->cleanFinished(I)V

    .line 167
    monitor-exit p0

    .line 168
    return-void

    .line 164
    :cond_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CommandCallback.onAborted: Task("

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, ") has been aborted"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onAborted(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 5
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 142
    monitor-enter p0

    .line 143
    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CommandCallback.onAborted called with command.startIds="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->markFinished(Ljava/util/Set;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->cleanFinished(I)V

    .line 146
    monitor-exit p0

    .line 147
    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCompleted(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 5
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 133
    monitor-enter p0

    .line 134
    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CommandCallback.onCompleted called with command.startIds="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->markFinished(Ljava/util/Set;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->cleanFinished(I)V

    .line 137
    monitor-exit p0

    .line 138
    return-void

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onFinished(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 151
    monitor-enter p0

    .line 152
    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CommandCallback.onFinished called with id="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->markFinished(Ljava/util/Set;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->cleanFinished(I)V

    .line 154
    monitor-exit p0

    .line 155
    return-void

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public watch(IILandroid/net/Uri;)V
    .locals 5
    .param p1, "startId"    # I
    .param p2, "cnt"    # I
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 82
    monitor-enter p0

    .line 83
    :try_start_0
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "watch(startId="

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, ", cmdNum="

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, ")"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "First intent to be processed, setting finished flag to false"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->setProcessingIntentFinished(Z)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    # getter for: Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "watch(): mUnfinishedIntents="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mUnfinishedIntents:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    if-eqz p3, :cond_1

    .line 94
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->mPendingNotifications:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    :cond_1
    monitor-exit p0

    .line 98
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public watch(ILandroid/net/Uri;)V
    .locals 1
    .param p1, "startId"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->watch(IILandroid/net/Uri;)V

    .line 129
    return-void
.end method
