.class public Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
.super Ljava/lang/Object;
.source "DCMPolicyManager.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProcessingRule"
.end annotation


# instance fields
.field public mImagesToProcess:I

.field public mMinTimeoutInMs:I

.field public mbStopProcessing:Z


# direct methods
.method public constructor <init>(IIZ)V
    .locals 0
    .param p1, "mImagesToProcess"    # I
    .param p2, "mMinTimeoutInMs"    # I
    .param p3, "mbStopOSC"    # Z

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iput p1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    .line 329
    iput p2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    .line 330
    iput-boolean p3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mbStopProcessing:Z

    .line 331
    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    .locals 4

    .prologue
    .line 315
    new-instance v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    iget v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    iget v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    iget-boolean v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mbStopProcessing:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;-><init>(IIZ)V

    .line 317
    .local v0, "clone":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->clone()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProcessingRule [mImagesToProcess="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMinTimeoutInMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
