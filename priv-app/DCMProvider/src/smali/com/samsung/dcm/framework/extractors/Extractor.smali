.class public abstract Lcom/samsung/dcm/framework/extractors/Extractor;
.super Ljava/lang/Object;
.source "Extractor.java"


# instance fields
.field private mExceptionHandlers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/dcm/framework/IExceptionHandler;",
            ">;"
        }
    .end annotation
.end field

.field protected mIsConnected:Z

.field protected mIsEnabled:Z

.field protected mSupportedExtractors:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mSupportedExtractors:Ljava/util/LinkedList;

    .line 20
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mExceptionHandlers:Ljava/util/HashSet;

    .line 22
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsConnected:Z

    .line 23
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsEnabled:Z

    .line 26
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractors/Extractor;->initializeSupportedClasses()V

    .line 27
    return-void
.end method


# virtual methods
.method public declared-synchronized addExceptionHandler(Lcom/samsung/dcm/framework/IExceptionHandler;)V
    .locals 1
    .param p1, "handler"    # Lcom/samsung/dcm/framework/IExceptionHandler;

    .prologue
    .line 30
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mExceptionHandlers:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    monitor-exit p0

    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public disableExtractor()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsEnabled:Z

    .line 72
    return-void
.end method

.method public enableExtractor()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsEnabled:Z

    .line 68
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 97
    if-nez p1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 103
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected declared-synchronized fireExceptionHandlers(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Throwable;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "exception":Ljava/lang/Throwable;, "TT;"
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mExceptionHandlers:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IExceptionHandler;

    .line 39
    .local v0, "h":Lcom/samsung/dcm/framework/IExceptionHandler;
    invoke-interface {v0, p1}, Lcom/samsung/dcm/framework/IExceptionHandler;->onHandleException(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 38
    .end local v0    # "h":Lcom/samsung/dcm/framework/IExceptionHandler;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 41
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public getIsConnected()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsConnected:Z

    return v0
.end method

.method public getSupportedMediaDocClasses()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mSupportedExtractors:Ljava/util/LinkedList;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 108
    const/16 v0, 0xb

    .line 109
    .local v0, "prime":I
    const/16 v1, 0xb

    return v1
.end method

.method public abstract initialize()V
.end method

.method protected abstract initializeSupportedClasses()V
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsEnabled:Z

    return v0
.end method

.method public abstract process(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract release()V
.end method

.method public declared-synchronized removeExceptionHandler(Lcom/samsung/dcm/framework/IExceptionHandler;)V
    .locals 1
    .param p1, "handler"    # Lcom/samsung/dcm/framework/IExceptionHandler;

    .prologue
    .line 34
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mExceptionHandlers:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    monitor-exit p0

    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setIsConnected(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/extractors/Extractor;->mIsConnected:Z

    .line 64
    return-void
.end method
