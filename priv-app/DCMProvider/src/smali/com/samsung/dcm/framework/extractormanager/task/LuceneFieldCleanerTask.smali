.class public Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "LuceneFieldCleanerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    }
.end annotation


# static fields
.field private static final CALENDAR_PROVIDER:Ljava/lang/String; = "package:com.android.providers.calendar"

.field private static final MAX_EVENTS_SUPPORTED:I = 0x19

.field private static final TAG:Ljava/lang/String;

.field private static auxDoc:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mMediaStoreClient:Landroid/content/ContentProviderClient;

.field private mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

.field private final mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private mNRTSearcherId:J

.field private mPackage:Ljava/lang/String;

.field mRemoveField:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    .line 123
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$1;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->auxDoc:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "package_name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "fields_to_remove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 68
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mPackage:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    .line 90
    const-string v1, "DatabaseRebuilderTask needs context"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mContext:Landroid/content/Context;

    .line 92
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 93
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "ContentResolver cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 95
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    const-string v1, "Config cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 97
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 98
    iput-object p2, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mPackage:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 100
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 102
    return-void
.end method

.method private cleanDoc(Lorg/apache/lucene/document/Document;J)V
    .locals 34
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "searchId"    # J

    .prologue
    .line 179
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v25, "mExistingcategories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v26, Ljava/util/HashSet;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashSet;-><init>()V

    .line 181
    .local v26, "mUniqueFinalCatogories":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v5, "mFacetCateogries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 183
    .local v18, "eventsToAdd":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v3, "date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    .line 185
    .local v15, "event_date":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v30

    .line 186
    .local v30, "uriList":[Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v2, v30

    .local v2, "arr$":[Lorg/apache/lucene/index/IndexableField;
    array-length v0, v2

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v21, 0x0

    .local v21, "i$":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, v24

    if-ge v0, v1, :cond_1

    aget-object v27, v2, v21

    .line 187
    .local v27, "uri":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v27 .. v27}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v31

    .line 188
    .local v31, "uri_string":Ljava/lang/String;
    const-string v3, " "

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 189
    .local v13, "eventUri_string":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v13, v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 190
    .local v12, "eventUri":Landroid/net/Uri;
    const/4 v3, 0x1

    aget-object v17, v13, v3

    .line 191
    .local v17, "event_title":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->validateEventUri(Landroid/net/Uri;)Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;

    move-result-object v16

    .line 192
    .local v16, "event_object":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    if-eqz v16, :cond_0

    .line 193
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;->eventTitle:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;->startTime:J

    move-wide/from16 v32, v0

    cmp-long v3, v6, v32

    if-ltz v3, :cond_0

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;->endTime:J

    move-wide/from16 v32, v0

    cmp-long v3, v6, v32

    if-gtz v3, :cond_0

    .line 195
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_0
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 199
    .end local v12    # "eventUri":Landroid/net/Uri;
    .end local v13    # "eventUri_string":[Ljava/lang/String;
    .end local v16    # "event_object":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .end local v17    # "event_title":Ljava/lang/String;
    .end local v27    # "uri":Lorg/apache/lucene/index/IndexableField;
    .end local v31    # "uri_string":Ljava/lang/String;
    :cond_1
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/document/Document;

    .line 200
    .local v4, "newdoc":Lorg/apache/lucene/document/Document;
    invoke-static {v4}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 201
    const/4 v3, 0x1

    new-array v3, v3, [Z

    const/4 v6, 0x0

    const/4 v7, 0x1

    aput-boolean v7, v3, v6

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1, v4, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 203
    const/16 v19, 0x1

    .local v19, "field":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v19

    if-ge v0, v3, :cond_2

    .line 204
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 203
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 206
    :cond_2
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .line 207
    .local v23, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 208
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 209
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 212
    :cond_4
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 214
    const/4 v14, 0x0

    .line 216
    .local v14, "event_count":I
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 217
    .local v20, "fulluri":Ljava/lang/String;
    const-string v3, " "

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 218
    .local v22, "indexof":I
    if-lez v22, :cond_6

    .line 220
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "root_calendar_event;"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v6, v22, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 224
    new-instance v3, Lorg/apache/lucene/document/StringField;

    const-string v6, "calendar_event"

    add-int/lit8 v7, v22, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v3, v6, v7, v8}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 227
    new-instance v3, Lorg/apache/lucene/document/TextField;

    const-string v6, "event_uri"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v20

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 230
    add-int/lit8 v14, v14, 0x1

    .line 233
    :cond_6
    const/16 v3, 0x19

    if-ne v14, v3, :cond_5

    .line 234
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v6, "Events supported = "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/16 v32, 0x19

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    .end local v20    # "fulluri":Ljava/lang/String;
    .end local v22    # "indexof":I
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v5, v4}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->updateCategorgyForFacet(Ljava/util/HashSet;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V

    .line 241
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-wide/from16 v6, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    move-result-wide v28

    .line 242
    .local v28, "newid":J
    cmp-long v3, v28, p2

    if-eqz v3, :cond_9

    .line 243
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v6, " race condition"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    const/4 v9, 0x0

    .line 245
    .local v9, "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_1

    .line 247
    .local v10, "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 248
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v9

    .line 249
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 250
    const-string v3, "uri"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v3, v10}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V

    .line 254
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 255
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/document/Document;

    const-wide v6, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->cleanDoc(Lorg/apache/lucene/document/Document;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    :cond_8
    if-eqz v9, :cond_9

    .line 262
    :try_start_2
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V
    :try_end_2
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_2 .. :try_end_2} :catch_1

    .line 270
    .end local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v10    # "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v28    # "newid":J
    :cond_9
    :goto_3
    return-void

    .line 258
    .restart local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v10    # "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v28    # "newid":J
    :catch_0
    move-exception v11

    .line 259
    .local v11, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v6, "Calendar race update "

    invoke-static {v3, v6, v11}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 261
    if-eqz v9, :cond_9

    .line 262
    :try_start_4
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V
    :try_end_4
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 266
    .end local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v10    # "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v11    # "e":Ljava/io/IOException;
    .end local v28    # "newid":J
    :catch_1
    move-exception v11

    .line 267
    .local v11, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v6, "Calendar update "

    invoke-static {v3, v6, v11}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 261
    .end local v11    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    .restart local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v10    # "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v28    # "newid":J
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_a

    .line 262
    :try_start_5
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_a
    throw v3
    :try_end_5
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_5 .. :try_end_5} :catch_1
.end method

.method private createLuceneReader()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 132
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTSearcherId:J

    .line 133
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v1, "createLuceneReader done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method private releaseLuceneReader()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 337
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 338
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTSearcherId:J

    .line 339
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v1, "releaseLuceneReader done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private resync()V
    .locals 15

    .prologue
    const/16 v14, 0x3e8

    const/4 v13, 0x1

    const/4 v5, 0x0

    .line 138
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mPackage:Ljava/lang/String;

    const-string v3, "package:com.android.providers.calendar"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v3, "calendar storage clear data resync case"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    const/4 v12, 0x0

    .line 141
    .local v12, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v1, 0x0

    .line 143
    .local v1, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v2

    .line 146
    .local v2, "query":Lorg/apache/lucene/search/BooleanQuery;
    const-string v0, "deleted"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v3, v4, v13, v13}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 152
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    const/4 v3, 0x0

    const/16 v4, 0x3e8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v12

    .line 153
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v3, "topDocs hits ="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v13, v12, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v4, v5

    invoke-static {v0, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    const/4 v1, 0x0

    .line 155
    iget-object v6, v12, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v6, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_4

    aget-object v11, v6, v9

    .line 156
    .local v11, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    iget v3, v11, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v7

    .line 157
    .local v7, "doc":Lorg/apache/lucene/document/Document;
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mRemoveField:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 158
    iget-wide v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mNRTSearcherId:J

    invoke-direct {p0, v7, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->cleanDoc(Lorg/apache/lucene/document/Document;J)V

    .line 160
    :cond_1
    move-object v1, v11

    .line 161
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->isFinishedNow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 176
    .end local v1    # "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v2    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .end local v6    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v7    # "doc":Lorg/apache/lucene/document/Document;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v12    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_2
    :goto_1
    return-void

    .line 155
    .restart local v1    # "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v2    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v6    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v7    # "doc":Lorg/apache/lucene/document/Document;
    .restart local v9    # "i$":I
    .restart local v10    # "len$":I
    .restart local v11    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v12    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 166
    .end local v7    # "doc":Lorg/apache/lucene/document/Document;
    .end local v11    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_4
    iget v0, v12, Lorg/apache/lucene/search/TopDocs;->totalHits:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_1

    if-ge v0, v14, :cond_5

    .line 167
    const/4 v1, 0x0

    .line 169
    :cond_5
    if-nez v1, :cond_0

    goto :goto_1

    .line 170
    .end local v6    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    :catch_0
    move-exception v8

    .line 171
    .local v8, "e":Ljava/io/IOException;
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v0, v3, v8}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 172
    .end local v8    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 173
    .local v8, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v3, "SearchingException"

    invoke-static {v0, v3, v8}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private updateCategorgyForFacet(Ljava/util/HashSet;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V
    .locals 4
    .param p3, "newDoc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;",
            "Lorg/apache/lucene/document/Document;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281
    .local p1, "existingCategories":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .local p2, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 282
    const-string v2, "categories"

    invoke-virtual {p3, v2}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 283
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 284
    .local v0, "category":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    new-instance v2, Lorg/apache/lucene/document/StoredField;

    const-string v3, "categories"

    invoke-direct {v2, v3, v0}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 290
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method private validateEventUri(Landroid/net/Uri;)Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .locals 20
    .param p1, "eventUri"    # Landroid/net/Uri;

    .prologue
    .line 299
    const/16 v16, 0x0

    .line 300
    .local v16, "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v18

    .line 301
    .local v18, "id":J
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    .line 302
    .local v17, "idAsString":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v17, v6, v2

    .line 305
    .local v6, "args":[Ljava/lang/String;
    const-string v13, "_id = ? AND deleted = 0"

    .line 307
    .local v13, "CALENDAR_SELECTION":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "dtstart"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "dtend"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "title"

    aput-object v3, v4, v2

    .line 313
    .local v4, "CALENDAR_PROJECTION":[Ljava/lang/String;
    const/4 v14, 0x0

    .line 316
    .local v14, "c":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id = ? AND deleted = 0"

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 318
    if-eqz v14, :cond_3

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 319
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 321
    new-instance v7, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;

    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;-><init>(JJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    .end local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .local v7, "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    :goto_0
    if-eqz v14, :cond_0

    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 328
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_0
    :goto_1
    return-object v7

    .line 324
    .end local v7    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .restart local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    :catch_0
    move-exception v15

    .line 325
    .local v15, "e1":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->TAG:Ljava/lang/String;

    const-string v3, "Exception: Validate Event Uri"

    invoke-static {v2, v3, v15}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    if-eqz v14, :cond_2

    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 328
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    move-object/from16 v7, v16

    .end local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .restart local v7    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    goto :goto_1

    .line 327
    .end local v7    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .end local v15    # "e1":Ljava/lang/Exception;
    .restart local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    :catchall_0
    move-exception v2

    if-eqz v14, :cond_1

    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 328
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .restart local v15    # "e1":Ljava/lang/Exception;
    :cond_2
    move-object/from16 v7, v16

    .end local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .restart local v7    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    goto :goto_1

    .end local v7    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .end local v15    # "e1":Ljava/lang/Exception;
    .restart local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    :cond_3
    move-object/from16 v7, v16

    .end local v16    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    .restart local v7    # "eventObject":Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
    goto :goto_0
.end method


# virtual methods
.method protected internalRun()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 110
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->createLuceneReader()Z

    .line 111
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->resync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->releaseLuceneReader()Z

    .line 114
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 116
    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 118
    :cond_0
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 121
    return-void

    .line 113
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->releaseLuceneReader()Z

    .line 114
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    .line 116
    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 118
    :cond_1
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    throw v0
.end method
