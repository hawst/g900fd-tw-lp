.class abstract Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;
.super Ljava/lang/Object;
.source "MediaProviderScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Importer"
.end annotation


# static fields
.field static final MEDIA_STORE_CONTENT_URI:Ljava/lang/String; = "external"


# instance fields
.field mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;->mContentResolver:Landroid/content/ContentResolver;

    .line 47
    return-void
.end method


# virtual methods
.method public abstract scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;
.end method
