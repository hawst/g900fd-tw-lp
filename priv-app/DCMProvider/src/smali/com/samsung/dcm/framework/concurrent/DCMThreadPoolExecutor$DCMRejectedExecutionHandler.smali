.class Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;
.super Ljava/lang/Object;
.source "DCMThreadPoolExecutor.java"

# interfaces
.implements Ljava/util/concurrent/RejectedExecutionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DCMRejectedExecutionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;


# direct methods
.method private constructor <init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;->this$0:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
    .param p2, "x1"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$1;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;)V

    return-void
.end method


# virtual methods
.method public rejectedExecution(Ljava/lang/Runnable;Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 5
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    .line 77
    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Task "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, " was rejected."

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;->this$0:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    iget-object v0, v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;->this$0:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    iget-object v0, v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    invoke-interface {v0, p1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;->onTaskRejected(Ljava/lang/Runnable;)V

    .line 82
    :cond_0
    return-void
.end method
