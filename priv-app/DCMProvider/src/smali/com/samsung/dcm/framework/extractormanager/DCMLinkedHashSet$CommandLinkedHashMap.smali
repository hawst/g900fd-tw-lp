.class final Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;
.super Ljava/util/LinkedHashMap;
.source "DCMLinkedHashSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CommandLinkedHashMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/samsung/dcm/framework/extractormanager/Command;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 63
    return-void
.end method


# virtual methods
.method public add(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    .local p1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "uriKey":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    invoke-virtual {p0, v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->put(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 74
    const/4 v1, 0x1

    .line 76
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    .local p1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    const-string v0, "Key should not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    instance-of v0, p1, Ljava/lang/String;

    const-string v1, "Key should be instance of String"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 35
    invoke-super {p0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    invoke-virtual {p0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->get(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    .local p2, "value":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    const-string v0, "Key should not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string v0, "value should not be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-super {p0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    return-object v0
.end method

.method public bridge synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 25
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/dcm/framework/extractormanager/Command;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->put(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    const-string v0, "Key should not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    instance-of v0, p1, Ljava/lang/String;

    const-string v1, "Key should be instance of String"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 55
    invoke-super {p0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    return-object v0
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap<TT;>;"
    invoke-virtual {p0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->remove(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0
.end method
