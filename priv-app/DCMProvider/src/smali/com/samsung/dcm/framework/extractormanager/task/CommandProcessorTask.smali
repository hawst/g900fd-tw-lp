.class public Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "CommandProcessorTask.java"


# static fields
.field private static final SELECTION_ARGS:[Ljava/lang/String;


# instance fields
.field private mCommandsToProcess:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation
.end field

.field private mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

.field private mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field private final mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

.field private mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

.field private mUnmountedMedia:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->SELECTION_ARGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)V
    .locals 6
    .param p1, "mDCMController"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .param p3, "mDCMContainer"    # Lcom/samsung/dcm/framework/extractormanager/DCMContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCMController;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/DCMContainer;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "mCommandsToProcess":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 49
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 61
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 62
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mCommandsToProcess:Ljava/lang/Iterable;

    .line 70
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1, v5}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mUnmountedMedia:Ljava/util/concurrent/ConcurrentHashMap;

    .line 76
    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask$1;

    invoke-direct {v1, p0}, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask$1;-><init>(Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    .line 50
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 51
    iput-object p2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mCommandsToProcess:Ljava/lang/Iterable;

    .line 52
    iput-object p3, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    .line 53
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 54
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getIntentReceiver()Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 56
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V

    .line 58
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mUnmountedMedia:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method


# virtual methods
.method protected internalRun()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v12, 0x0

    .line 97
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 99
    .local v9, "contentresolver":Landroid/content/ContentResolver;
    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 101
    .local v7, "baseUri":Landroid/net/Uri;
    invoke-virtual {v9, v7}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 103
    .local v0, "mediaStoreClient":Landroid/content/ContentProviderClient;
    const-string v3, "media_type = ? OR  media_type = ?"

    .line 106
    .local v3, "selection":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->TAG:Ljava/lang/String;

    const-string v4, "appendCommands "

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, " started "

    aput-object v6, v5, v12

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mCommandsToProcess:Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 110
    .local v8, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->isUpdateFamilyCommand()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 112
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ";"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 113
    .local v1, "external_uri":Landroid/net/Uri;
    const/4 v10, 0x0

    .line 115
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x1

    :try_start_1
    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "storage_id"

    aput-object v5, v2, v4

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->SELECTION_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v10

    .line 119
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v8, v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->setStorageId(I)V

    .line 121
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->TAG:Ljava/lang/String;

    const-string v4, "storage id set as "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v5, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v2, v8}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    :goto_1
    if-eqz v10, :cond_0

    .line 129
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 130
    const/4 v10, 0x0

    goto :goto_0

    .line 124
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->TAG:Ljava/lang/String;

    const-string v4, "storage id failed aborted"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 128
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_2

    .line 129
    :try_start_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 130
    const/4 v10, 0x0

    :cond_2
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 149
    .end local v1    # "external_uri":Landroid/net/Uri;
    .end local v8    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v2

    if-eqz v0, :cond_3

    .line 150
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 151
    const/4 v0, 0x0

    .line 153
    :cond_3
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    if-eqz v4, :cond_4

    .line 154
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->unRegisterListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;)V

    :cond_4
    throw v2

    .line 135
    .restart local v8    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v11    # "i$":Ljava/util/Iterator;
    :cond_5
    :try_start_5
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mUnmountedMedia:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 136
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v2, v8}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    goto/16 :goto_0

    .line 139
    :cond_6
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->TAG:Ljava/lang/String;

    const-string v4, "storage id unmounted"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto/16 :goto_0

    .line 145
    .end local v8    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_7
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->startWorkerThreads()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 149
    if-eqz v0, :cond_8

    .line 150
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 151
    const/4 v0, 0x0

    .line 153
    :cond_8
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    if-eqz v2, :cond_9

    .line 154
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    invoke-virtual {v2, v4}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->unRegisterListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;)V

    .line 158
    :cond_9
    return-void
.end method
