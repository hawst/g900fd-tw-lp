.class Lcom/samsung/dcm/framework/extractormanager/DCMContainer$2;
.super Ljava/lang/Object;
.source "DCMContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->notifyOnProcessingFinished()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 498
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->access$000(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)Ljava/util/Set;

    move-result-object v2

    monitor-enter v2

    .line 499
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->access$000(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 500
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;

    invoke-interface {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;->onProcessingFinished()V

    goto :goto_0

    .line 503
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;>;"
    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    return-void
.end method
