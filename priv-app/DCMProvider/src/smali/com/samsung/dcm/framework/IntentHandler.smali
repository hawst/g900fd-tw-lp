.class public Lcom/samsung/dcm/framework/IntentHandler;
.super Ljava/lang/Object;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/IntentHandler$1;,
        Lcom/samsung/dcm/framework/IntentHandler$OperationType;,
        Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;,
        Lcom/samsung/dcm/framework/IntentHandler$ValidData;,
        Lcom/samsung/dcm/framework/IntentHandler$ValidOption;,
        Lcom/samsung/dcm/framework/IntentHandler$ValidAction;,
        Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;,
        Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;
    }
.end annotation


# static fields
.field public static final DCM_INTENT_NAME:Ljava/lang/String; = "com.samsung.dcm.action.DCM_EXECUTE"

.field public static final EXTRAS_FILE_NAME:Ljava/lang/String; = "file_name"

.field public static final EXTRAS_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final EXTRAS_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final EXTRAS_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final EXTRAS_NAMEPLACE_TO_BE_ADDED:Ljava/lang/String; = "nameplace_to_be_added"

.field public static final EXTRAS_NAMEPLACE_TO_BE_REMOVED:Ljava/lang/String; = "nameplace_to_be_removed"

.field public static final EXTRAS_SCENETYPE_TO_BE_ADDED:Ljava/lang/String; = "OSC_SCENETYPE_TO_BE_ADDED"

.field public static final EXTRAS_SUBSCENETYPE_TO_BE_ADDED:Ljava/lang/String; = "OSC_SUBSCENETYPE_TO_BE_ADDED"

.field public static final FACETAGS_TO_BE_ADDED:Ljava/lang/String; = "facetags_to_be_added"

.field public static final FACETAGS_TO_BE_REMOVED:Ljava/lang/String; = "facetags_to_be_removed"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

.field private mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

.field private mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field private mService:Lcom/samsung/dcm/framework/FrameworkService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/FrameworkService;Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/broadcaster/Broadcaster;)V
    .locals 4
    .param p1, "service"    # Lcom/samsung/dcm/framework/FrameworkService;
    .param p2, "controller"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .param p3, "broadcaster"    # Lcom/samsung/dcm/framework/broadcaster/Broadcaster;

    .prologue
    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 430
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/samsung/dcm/framework/FrameworkService;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/FrameworkService;

    iput-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler;->mService:Lcom/samsung/dcm/framework/FrameworkService;

    .line 432
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iput-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 433
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is null!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    .line 436
    .local v0, "extCtrl":Lcom/samsung/dcm/framework/configuration/ExtractorsController;
    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-direct {v1, v0}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;-><init>(Lcom/samsung/dcm/framework/configuration/ExtractorsController;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    .line 438
    new-instance v1, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-direct {v1, p0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;-><init>(Lcom/samsung/dcm/framework/IntentHandler;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    .line 440
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/dcm/framework/IntentHandler;)Lcom/samsung/dcm/framework/FrameworkService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/IntentHandler;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler;->mService:Lcom/samsung/dcm/framework/FrameworkService;

    return-object v0
.end method

.method private attachDataAndExecute(Ljava/lang/Iterable;Landroid/os/Bundle;ZLcom/google/common/base/Optional;)V
    .locals 3
    .param p2, "data"    # Landroid/os/Bundle;
    .param p3, "bIsSdCardAction"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;",
            "Landroid/os/Bundle;",
            "Z",
            "Lcom/google/common/base/Optional",
            "<+",
            "Lcom/samsung/dcm/framework/extractormanager/CommandListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 916
    .local p1, "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .local p4, "listener":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<+Lcom/samsung/dcm/framework/extractormanager/CommandListener;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 919
    .local v0, "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, p2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->setData(Landroid/os/Bundle;)V

    .line 920
    const-string v2, "storageId"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->setStorageId(I)V

    .line 921
    invoke-virtual {v0, p3}, Lcom/samsung/dcm/framework/extractormanager/Command;->setIsSdCardAction(Z)V

    .line 922
    invoke-virtual {p4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 923
    invoke-virtual {p4}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/CommandListener;

    invoke-virtual {v0, v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->addCommandCallback(Lcom/samsung/dcm/framework/extractormanager/CommandListener;)V

    goto :goto_0

    .line 926
    .end local v0    # "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v2, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->appendCommands(Ljava/lang/Iterable;)V

    .line 927
    return-void
.end method

.method private isBulkOperation(Landroid/os/Bundle;)Z
    .locals 3
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 974
    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 976
    .local v0, "options":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Landroid/os/Bundle;>;"
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 977
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsBulk:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 978
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsBulk:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 982
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isOperationAllowedOnLowBattery(Landroid/content/Context;Lcom/samsung/dcm/framework/IntentHandler$OperationType;Z)Z
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "op_type"    # Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    .param p3, "isBulk"    # Z

    .prologue
    .line 999
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    if-eq p2, v0, :cond_0

    if-nez p3, :cond_0

    .line 1001
    const/4 v0, 0x1

    .line 1006
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatterOK(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private prepareData(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 897
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 899
    return-object p1
.end method

.method private processDeleteAction(Landroid/os/Bundle;Ljava/lang/String;I)V
    .locals 15
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "startId"    # I

    .prologue
    .line 750
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 751
    .local v7, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v7, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/samsung/dcm/framework/IntentHandler;->isBulkOperation(Landroid/os/Bundle;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 752
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error in processDeleteAction: Bundle data ids is null, aborting intent processing for origin:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 753
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v12, Ljava/lang/IllegalStateException;

    const-string v13, "Cannot handle intent: bundle getIds returns null"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v11, v0, v12}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 818
    :goto_0
    return-void

    .line 757
    :cond_0
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v11

    invoke-static {v11}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v9

    .line 759
    .local v9, "options":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Landroid/os/Bundle;>;"
    const/4 v11, 0x0

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Delete:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v11, v12}, Lcom/samsung/dcm/framework/IntentHandler;->resolveActionPriority(Landroid/os/Bundle;Ljava/lang/String;Lcom/samsung/dcm/framework/IntentHandler$OperationType;)Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-result-object v10

    .line 760
    .local v10, "priority":Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    const/4 v1, 0x0

    .line 762
    .local v1, "bIsSdCardAction":Z
    invoke-virtual {v9}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 765
    invoke-virtual {v9}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Bundle;

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-virtual {v9}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Bundle;

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    sget-object v11, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne v10, v11, :cond_1

    .line 768
    const/4 v1, 0x1

    .line 770
    :cond_1
    invoke-virtual {v9}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Bundle;

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v9}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Bundle;

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "sdcard"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 773
    const/4 v1, 0x1

    .line 776
    :cond_2
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->CalendarProvider2:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 780
    new-instance v8, Landroid/os/Bundle;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 781
    .local v8, "newData":Landroid/os/Bundle;
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CalendarUpdate:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 784
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v12, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    move/from16 v0, p3

    invoke-virtual {v11, v8, v0, v12}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    goto/16 :goto_0

    .line 789
    .end local v8    # "newData":Landroid/os/Bundle;
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/samsung/dcm/framework/IntentHandler;->isBulkOperation(Landroid/os/Bundle;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 791
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 792
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v12, "executed bulk delete from origin = "

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 793
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 794
    .local v2, "bundle":Landroid/os/Bundle;
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v11}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->StopOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    const/4 v12, -0x1

    const/4 v13, 0x0

    invoke-virtual {v11, v2, v12, v13}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    .line 797
    new-instance v5, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "bulk_delete/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "storageId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v11}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Ljava/lang/String;)V

    .line 798
    .local v5, "dcmId":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 799
    .local v4, "commands":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    sget-object v12, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v13}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v13

    invoke-virtual {v11, v5, v12, v13}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createDeleteCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateNotificationUri()Landroid/net/Uri;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v11, v0, v12}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->watch(ILandroid/net/Uri;)V

    .line 802
    const/4 v11, 0x1

    iget-object v12, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-static {v12}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-direct {p0, v4, v0, v11, v12}, Lcom/samsung/dcm/framework/IntentHandler;->attachDataAndExecute(Ljava/lang/Iterable;Landroid/os/Bundle;ZLcom/google/common/base/Optional;)V

    goto/16 :goto_0

    .line 805
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v4    # "commands":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v5    # "dcmId":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    :cond_4
    sget-object v11, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v12, "Skipped bulk delete from origin = "

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 806
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onFinished(I)V

    goto/16 :goto_0

    .line 810
    :cond_5
    const/4 v11, 0x0

    invoke-static {v7, v11}, Lcom/samsung/dcm/framework/IntentHandler;->translateUriPathList2DCM_IDList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 811
    .local v6, "dcm_ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v12}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v12

    invoke-virtual {v11, v6, v10, v12}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createDeleteCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v3

    .line 813
    .local v3, "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateNotificationUri()Landroid/net/Uri;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v11, v0, v12, v13}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->watch(IILandroid/net/Uri;)V

    .line 814
    invoke-direct/range {p0 .. p1}, Lcom/samsung/dcm/framework/IntentHandler;->prepareData(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-static {v12}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v12

    invoke-direct {p0, v3, v11, v1, v12}, Lcom/samsung/dcm/framework/IntentHandler;->attachDataAndExecute(Ljava/lang/Iterable;Landroid/os/Bundle;ZLcom/google/common/base/Optional;)V

    goto/16 :goto_0
.end method

.method private processInsertAction(Landroid/os/Bundle;Ljava/lang/String;I)V
    .locals 14
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "startId"    # I

    .prologue
    .line 590
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 591
    .local v4, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v4, :cond_0

    .line 592
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error in processInsertAction: Bundle data ids is null, aborting intent processing for origin:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 593
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "Cannot handle intent: bundle getIds returns null"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v9, v0, v10}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 664
    :goto_0
    return-void

    .line 597
    :cond_0
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v9}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v6

    .line 600
    .local v6, "options":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Landroid/os/Bundle;>;"
    const/4 v1, 0x0

    .line 601
    .local v1, "bIsSdCardAction":Z
    const/4 v9, 0x0

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Insert:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-direct {p0, p1, v9, v10}, Lcom/samsung/dcm/framework/IntentHandler;->resolveActionPriority(Landroid/os/Bundle;Ljava/lang/String;Lcom/samsung/dcm/framework/IntentHandler$OperationType;)Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-result-object v8

    .line 603
    .local v8, "priority":Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 604
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v10, "Options="

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-static {v9, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 607
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Paths:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    const-string v10, "[IH] processInsertAction(): paths are not in options bundle"

    invoke-static {v9, v10}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 609
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Paths:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 611
    .local v7, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v9, "[IH] processUpdateAction(): paths is null"

    invoke-static {v7, v9}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 616
    const/4 v1, 0x1

    .line 618
    :cond_1
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "sdcard"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 621
    const/4 v1, 0x1

    .line 624
    :cond_2
    invoke-static {v4, v7}, Lcom/samsung/dcm/framework/IntentHandler;->translateUriPathList2DCM_IDList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 628
    .local v3, "dcm_ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    invoke-virtual {v6}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Paths:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 635
    .end local v7    # "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v10, "isSdcCardAction="

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 637
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->CalendarProvider2:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 641
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 642
    .local v5, "newData":Landroid/os/Bundle;
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CalendarUpdate:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 645
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v10, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    move/from16 v0, p3

    invoke-virtual {v9, v5, v0, v10}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    goto/16 :goto_0

    .line 631
    .end local v3    # "dcm_ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    .end local v5    # "newData":Landroid/os/Bundle;
    :cond_3
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v10, "Options NOT present"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 632
    const/4 v9, 0x0

    invoke-static {v4, v9}, Lcom/samsung/dcm/framework/IntentHandler;->translateUriPathList2DCM_IDList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .restart local v3    # "dcm_ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    goto :goto_1

    .line 648
    :cond_4
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v10

    invoke-virtual {v9, v3, v8, v10}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createInsertCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v2

    .line 654
    .local v2, "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    iget-object v10, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateNotificationUri()Landroid/net/Uri;

    move-result-object v9

    :goto_2
    move/from16 v0, p3

    invoke-virtual {v10, v0, v11, v9}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->watch(IILandroid/net/Uri;)V

    .line 658
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/samsung/dcm/framework/utils/SystemUtils;->setHeavySharedPref(Landroid/content/Context;Z)V

    .line 660
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/IntentHandler;->prepareData(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-static {v10}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v10

    invoke-direct {p0, v2, v9, v1, v10}, Lcom/samsung/dcm/framework/IntentHandler;->attachDataAndExecute(Ljava/lang/Iterable;Landroid/os/Bundle;ZLcom/google/common/base/Optional;)V

    goto/16 :goto_0

    .line 654
    :cond_5
    const/4 v9, 0x0

    goto :goto_2
.end method

.method private processPerformAction(Landroid/os/Bundle;Ljava/lang/String;I)V
    .locals 4
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "startId"    # I

    .prologue
    .line 829
    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    move-result-object v0

    .line 830
    .local v0, "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    if-nez v0, :cond_0

    .line 831
    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in processPerformAction, aborting intent processing for origin:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 832
    iget-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot handle intent: processPerformAction"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3, v2}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 840
    :goto_0
    return-void

    .line 839
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v2, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-virtual {v1, p1, p3, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    goto :goto_0
.end method

.method private processUpdateAction(Landroid/os/Bundle;Ljava/lang/String;I)V
    .locals 14
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "startId"    # I

    .prologue
    .line 676
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v9}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v5

    .line 678
    .local v5, "fields":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Landroid/os/Bundle;>;"
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 679
    .local v6, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v6, :cond_0

    .line 680
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error in processUpdateAction , aborting intent processing for origin:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 681
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "Cannot handle intent: processUpdateAction"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v9, v0, v10}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 738
    :goto_0
    return-void

    .line 686
    :cond_0
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Update:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    move-object/from16 v0, p2

    invoke-direct {p0, p1, v0, v9}, Lcom/samsung/dcm/framework/IntentHandler;->resolveActionPriority(Landroid/os/Bundle;Ljava/lang/String;Lcom/samsung/dcm/framework/IntentHandler$OperationType;)Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-result-object v8

    .line 688
    .local v8, "priority":Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    invoke-virtual {v5}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 692
    const/4 v9, 0x0

    invoke-static {v6, v9}, Lcom/samsung/dcm/framework/IntentHandler;->translateUriPathList2DCM_IDList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 694
    .local v3, "dcm_ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->CalendarProvider2:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 698
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 699
    .local v7, "newData":Landroid/os/Bundle;
    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    const-string v10, "start_time"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v1, 0x2

    .line 701
    .local v1, "cal_op":I
    :goto_1
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 702
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CalendarUpdate:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v10, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    move/from16 v0, p3

    invoke-virtual {v9, v7, v0, v10}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V

    goto :goto_0

    .line 699
    .end local v1    # "cal_op":I
    :cond_1
    const/4 v1, 0x3

    goto :goto_1

    .line 707
    .end local v7    # "newData":Landroid/os/Bundle;
    :cond_2
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->FaceScanner:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    const-string v10, "OSC_SCENETYPE_TO_BE_ADDED"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 709
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 710
    .local v4, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v9

    const-class v10, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;

    invoke-virtual {v9, v10}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 712
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v10

    invoke-virtual {v9, v3, v4, v8, v10}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommands(Ljava/lang/Iterable;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v2

    .line 726
    .end local v4    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .local v2, "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    :goto_2
    sget-object v9, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v10, "update commands = "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 729
    iget-object v10, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne v8, v9, :cond_5

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateNotificationUri()Landroid/net/Uri;

    move-result-object v9

    :goto_3
    move/from16 v0, p3

    invoke-virtual {v10, v0, v11, v9}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->watch(IILandroid/net/Uri;)V

    .line 731
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/IntentHandler;->prepareData(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    invoke-static {v11}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v11

    invoke-direct {p0, v2, v9, v10, v11}, Lcom/samsung/dcm/framework/IntentHandler;->attachDataAndExecute(Ljava/lang/Iterable;Landroid/os/Bundle;ZLcom/google/common/base/Optional;)V

    goto/16 :goto_0

    .line 716
    .end local v2    # "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    :cond_3
    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    const-string v10, "latitude"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 717
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 718
    .restart local v4    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v9

    const-class v10, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    invoke-virtual {v9, v10}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 719
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v10

    invoke-virtual {v9, v3, v4, v8, v10}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommands(Ljava/lang/Iterable;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v2

    .line 721
    .restart local v2    # "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    goto :goto_2

    .line 722
    .end local v2    # "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v4    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    :cond_4
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdFactory:Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v10

    invoke-virtual {v9, v3, v8, v10}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v2

    .restart local v2    # "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    goto :goto_2

    .line 729
    :cond_5
    const/4 v9, 0x0

    goto :goto_3

    .line 735
    .end local v2    # "cmds":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v3    # "dcm_ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    :cond_6
    iget-object v9, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "Cannot handle update intent: fields and options are null"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v9, v0, v10}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private resolveActionPriority(Landroid/os/Bundle;Ljava/lang/String;Lcom/samsung/dcm/framework/IntentHandler$OperationType;)Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .locals 8
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "operation"    # Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 949
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    .line 950
    .local v1, "priority":Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    if-eqz p1, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Update:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {p3, v2}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 951
    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 952
    .local v0, "fieldsBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 953
    const-string v2, "file_path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 954
    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v3, "Intent has file path "

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "increased prioity"

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 955
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    .line 959
    .end local v0    # "fieldsBundle":Landroid/os/Bundle;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v3, "Operation.Priority = "

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 960
    return-object v1
.end method

.method private resolveOperation(Landroid/os/Bundle;)Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 571
    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 572
    .local v0, "strOp":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 573
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    move-result-object v1

    .line 576
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static translateUriPathList2DCM_IDList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "uris":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p1, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 854
    const/4 v1, 0x0

    .line 857
    .local v1, "ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    new-instance v5, Ljava/util/LinkedHashSet;

    invoke-direct {v5}, Ljava/util/LinkedHashSet;-><init>()V

    .line 858
    .local v5, "uniqueURISet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v5, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 859
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 861
    .restart local v1    # "ids":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v8

    if-eq v7, v8, :cond_0

    .line 862
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, " Intent does not contain unique URI initial size = "

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    const/4 v10, 0x1

    const-string v11, " reduced size = "

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 866
    :cond_0
    if-nez p1, :cond_1

    .line 867
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 868
    .local v2, "item":Ljava/lang/String;
    new-instance v7, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-direct {v7, v2}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 872
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 873
    .local v3, "pathHashSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 875
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v7

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v8

    if-ne v7, v8, :cond_2

    .line 876
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 877
    .local v6, "uriIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 878
    .local v4, "pathIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 879
    new-instance v9, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v9, v7, v8}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 883
    .end local v4    # "pathIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v6    # "uriIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, " command not processed path and uri size do not match"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 886
    .end local v3    # "pathHashSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    return-object v1
.end method


# virtual methods
.method public handleIntent(Landroid/content/Intent;I)V
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 449
    const-string v7, "[IH] handleIntent(): intent is null"

    invoke-static {p1, v7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "Intent action="

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, ", startId="

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 453
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    const/4 v8, 0x0

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->watch(ILandroid/net/Uri;)V

    .line 455
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 458
    .local v2, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 459
    :cond_0
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "Intent action/bundle is null, aborting intent processing"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 460
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot handle intent: getAction or getExtras returns null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 555
    :goto_0
    return-void

    .line 465
    :cond_1
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 466
    .local v3, "data":Landroid/os/Bundle;
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 468
    .local v6, "origin":Ljava/lang/String;
    if-eqz v3, :cond_2

    if-nez v6, :cond_3

    .line 469
    :cond_2
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bundle data or origin is null, aborting intent processing for intent action:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot handle intent: bundle getBundle data/origin returns null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 475
    :cond_3
    const-string v7, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 476
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->isShutDownStarted()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 477
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "[IH] handleIntent completed shutdown"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Lcom/samsung/dcm/framework/exceptions/IntentHandlingException;

    const-string v9, "Cannot handle intent: service is shutting down"

    invoke-direct {v8, v9}, Lcom/samsung/dcm/framework/exceptions/IntentHandlingException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 486
    :cond_4
    invoke-direct {p0, v2}, Lcom/samsung/dcm/framework/IntentHandler;->resolveOperation(Landroid/os/Bundle;)Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    move-result-object v5

    .line 487
    .local v5, "op_type":Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    if-nez v5, :cond_5

    .line 488
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bundle operation is null, aborting intent processing for action:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and origin:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot handle intent: bundle getOperation returns null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 493
    :cond_5
    invoke-direct {p0, v3}, Lcom/samsung/dcm/framework/IntentHandler;->isBulkOperation(Landroid/os/Bundle;)Z

    move-result v4

    .line 496
    .local v4, "isBulk":Z
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 497
    .local v1, "actionValue":Ljava/lang/String;
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "handleIntent(): opType="

    const/4 v9, 0x6

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    const/4 v10, 0x1

    const-string v11, ", isBulk="

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const-string v11, ", action="

    aput-object v11, v9, v10

    const/4 v10, 0x4

    aput-object v1, v9, v10

    const/4 v10, 0x5

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " origin="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 501
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isMemoryOK(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 502
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "Low Memory, Intent Not Processed"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 503
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Lcom/samsung/dcm/framework/exceptions/IntentHandlingException;

    const-string v9, "Cannot handle intent: low memory"

    invoke-direct {v8, v9}, Lcom/samsung/dcm/framework/exceptions/IntentHandlingException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 506
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto/16 :goto_0

    .line 509
    :cond_6
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0, v7, v5, v4}, Lcom/samsung/dcm/framework/IntentHandler;->isOperationAllowedOnLowBattery(Landroid/content/Context;Lcom/samsung/dcm/framework/IntentHandler$OperationType;Z)Z

    move-result v7

    if-nez v7, :cond_7

    .line 518
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Lcom/samsung/dcm/framework/exceptions/IntentHandlingException;

    const-string v9, "Cannot handle intent: low battery level"

    invoke-direct {v8, v9}, Lcom/samsung/dcm/framework/exceptions/IntentHandlingException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    .line 521
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "Low Battery, Intent Not Processed"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 522
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mService:Lcom/samsung/dcm/framework/FrameworkService;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/FrameworkService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto/16 :goto_0

    .line 527
    :cond_7
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler$1;->$SwitchMap$com$samsung$dcm$framework$IntentHandler$OperationType:[I

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto/16 :goto_0

    .line 529
    :pswitch_0
    invoke-direct {p0, v3, v6, p2}, Lcom/samsung/dcm/framework/IntentHandler;->processInsertAction(Landroid/os/Bundle;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 532
    :pswitch_1
    invoke-direct {p0, v3, v6, p2}, Lcom/samsung/dcm/framework/IntentHandler;->processUpdateAction(Landroid/os/Bundle;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 535
    :pswitch_2
    invoke-direct {p0, v3, v6, p2}, Lcom/samsung/dcm/framework/IntentHandler;->processDeleteAction(Landroid/os/Bundle;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 538
    :pswitch_3
    invoke-direct {p0, v3, v6, p2}, Lcom/samsung/dcm/framework/IntentHandler;->processPerformAction(Landroid/os/Bundle;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 551
    .end local v1    # "actionValue":Ljava/lang/String;
    .end local v4    # "isBulk":Z
    .end local v5    # "op_type":Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    :cond_8
    sget-object v7, Lcom/samsung/dcm/framework/IntentHandler;->TAG:Ljava/lang/String;

    const-string v8, "not a valid intent action : "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 552
    iget-object v7, p0, Lcom/samsung/dcm/framework/IntentHandler;->mCmdCallback:Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;

    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot handle intent: invalid action"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, v8}, Lcom/samsung/dcm/framework/IntentHandler$CommandCallback;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_0

    .line 527
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
