.class public final enum Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/IntentHandler$ValidOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

.field public static final enum IsBulk:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

.field public static final enum IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

.field public static final enum Paths:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

.field public static final enum Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

.field public static final enum RebuildLocation:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

.field public static final enum StorageId:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 286
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    const-string v1, "IsBulk"

    const-string v2, "bulk"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsBulk:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    .line 287
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    const-string v1, "Reason"

    const-string v2, "reason"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    .line 288
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    const-string v1, "StorageId"

    const-string v2, "storage_id"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->StorageId:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    .line 290
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    const-string v1, "IsSdCard"

    const-string v2, "is_sdcard"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    .line 291
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    const-string v1, "Paths"

    const-string v2, "paths"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Paths:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    .line 292
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    const-string v1, "RebuildLocation"

    const/4 v2, 0x5

    const-string v3, "locationRebuild"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->RebuildLocation:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    .line 285
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsBulk:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Reason:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->StorageId:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->IsSdCard:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->Paths:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->RebuildLocation:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 296
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 297
    iput-object p3, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->mName:Ljava/lang/String;

    .line 298
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 305
    const-string v4, "ValidOption fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->values()[Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    move-result-object v1

    .local v1, "arr$":[Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 307
    .local v0, "ao":Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    iget-object v4, v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 311
    .end local v0    # "ao":Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    :goto_1
    return-object v0

    .line 306
    .restart local v0    # "ao":Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 311
    .end local v0    # "ao":Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 285
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/IntentHandler$ValidOption;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->mName:Ljava/lang/String;

    return-object v0
.end method
