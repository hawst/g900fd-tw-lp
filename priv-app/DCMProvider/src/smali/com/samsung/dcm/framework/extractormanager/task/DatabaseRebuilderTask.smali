.class public Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "DatabaseRebuilderTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$1;,
        Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    }
.end annotation


# static fields
.field private static final COMMAND_BATCH_SIZE:I = 0x64

.field private static final DB_IMAGES_PROJECTION:[Ljava/lang/String;

.field private static final RESYNC_TIMEWAIT_SEC:J = 0x5L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private volatile mDBLatch:Ljava/util/concurrent/CountDownLatch;

.field private mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field private mMediaStoreClient:Landroid/content/ContentProviderClient;

.field private mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private mNRTSearcherId:J

.field mReBuiltCompleted:Z

.field private mRebuildCmds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation
.end field

.field private mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

.field mbFlag:Z

.field private volatile mbLocationRebuild:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 100
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    .line 118
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "storage_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->DB_IMAGES_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 129
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 101
    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDBLatch:Ljava/util/concurrent/CountDownLatch;

    .line 103
    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 106
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mbFlag:Z

    .line 107
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mbLocationRebuild:Z

    .line 108
    iput-boolean v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    .line 115
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    .line 130
    const-string v1, "DatabaseRebuilderTask needs context"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    .line 131
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 132
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "ContentResolver cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 134
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    const-string v1, "Config cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 136
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 137
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 138
    return-void
.end method

.method private createLuceneReader()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 227
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTSearcherId:J

    .line 228
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v1, "createLuceneReader done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    const/4 v0, 0x1

    return v0
.end method

.method private queryLucene(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Set;Landroid/util/SparseArray;Ljava/util/ArrayList;)Z
    .locals 19
    .param p1, "lastscoreDoc"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/BooleanQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/ScoreDoc;",
            "Lorg/apache/lucene/search/BooleanQuery;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 299
    .local p3, "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p4, "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    .local p5, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v17, 0x0

    .line 300
    .local v17, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/16 v5, 0x3e8

    .line 303
    .local v5, "nMax":I
    :try_start_0
    new-instance v16, Lorg/apache/lucene/search/SortField;

    const-string v1, "id"

    sget-object v2, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    move-object/from16 v0, v16

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 304
    .local v16, "sfield":Lorg/apache/lucene/search/SortField;
    new-instance v6, Lorg/apache/lucene/search/Sort;

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    .line 306
    .local v6, "sortby":Lorg/apache/lucene/search/Sort;
    const/4 v10, 0x0

    .line 307
    .local v10, "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v11, "fieldsToLoadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    const-string v1, "location__y"

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    const-string v1, "named_location"

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    const-string v1, "uri"

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    const-string v1, "path"

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    const-string v1, "title"

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    new-instance v10, Ljava/util/HashSet;

    .end local v10    # "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v10, v11}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 318
    .restart local v10    # "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 319
    const/4 v1, 0x0

    .line 360
    .end local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .end local v10    # "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v11    # "fieldsToLoadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "sfield":Lorg/apache/lucene/search/SortField;
    :goto_0
    return v1

    .line 321
    .restart local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .restart local v10    # "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v11    # "fieldsToLoadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v16    # "sfield":Lorg/apache/lucene/search/SortField;
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    const/4 v4, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v17

    .line 322
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v2, "topDocs hits ="

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    const/16 p1, 0x0

    .line 324
    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v7, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v14, v7

    .local v14, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_1
    if-ge v12, v14, :cond_6

    aget-object v15, v7, v12

    .line 325
    .local v15, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 326
    const/4 v1, 0x0

    goto :goto_0

    .line 328
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    iget v2, v15, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    invoke-virtual {v1, v2, v10}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v8

    .line 329
    .local v8, "doc":Lorg/apache/lucene/document/Document;
    const-string v1, "id"

    invoke-virtual {v8, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v13

    .line 330
    .local v13, "id":I
    if-eqz p3, :cond_3

    .line 331
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 334
    :cond_3
    if-eqz p5, :cond_4

    const-string v1, "location__y"

    invoke-virtual {v8, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v1, "named_location"

    invoke-virtual {v8, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v1

    if-nez v1, :cond_4

    .line 338
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    :cond_4
    if-eqz p4, :cond_5

    .line 342
    move-object/from16 v0, p4

    invoke-virtual {v0, v13, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_2

    .line 344
    :cond_5
    move-object/from16 p1, v15

    .line 324
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 346
    .end local v8    # "doc":Lorg/apache/lucene/document/Document;
    .end local v13    # "id":I
    .end local v15    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_6
    if-nez p1, :cond_0

    .line 358
    .end local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v10    # "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v11    # "fieldsToLoadList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "i$":I
    .end local v14    # "len$":I
    .end local v16    # "sfield":Lorg/apache/lucene/search/SortField;
    :goto_2
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v2, "resyncLuceneDB Lucene Query ended "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 348
    :catch_0
    move-exception v9

    .line 349
    .local v9, "e":Ljava/lang/OutOfMemoryError;
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v2, "OOM exception"

    invoke-static {v1, v2, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 350
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 351
    .end local v9    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v9

    .line 352
    .local v9, "e":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v2, "Exception"

    invoke-static {v1, v2, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 353
    .end local v9    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v9

    .line 355
    .local v9, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/exceptions/SearchingException;->printStackTrace()V

    goto :goto_2
.end method

.method private releaseLuceneReader()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 709
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 711
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 712
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTSearcherId:J

    .line 713
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v1, "releaseLuceneReader done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 716
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private resyncCalendarEvents()V
    .locals 32

    .prologue
    .line 638
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v3, "resyncCalendarEvents started "

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 639
    const/4 v15, 0x0

    .line 640
    .local v15, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "dtstart"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "dtend"

    aput-object v3, v4, v2

    .line 644
    .local v4, "CALENDAR_PROJECTION":[Ljava/lang/String;
    const-wide v24, 0x7fffffffffffffffL

    .line 645
    .local v24, "minStartTime":J
    const-wide/high16 v22, -0x8000000000000000L

    .line 648
    .local v22, "maxEndTime":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 650
    if-eqz v15, :cond_1

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 651
    :cond_0
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 652
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    .line 665
    :cond_1
    if-eqz v15, :cond_2

    invoke-interface {v15}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 666
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 669
    :cond_2
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v3, "minStartTime = ["

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    aput-object v31, v5, v30

    const/16 v30, 0x1

    const-string v31, "]"

    aput-object v31, v5, v30

    const/16 v30, 0x2

    const-string v31, "maxEndTime = ["

    aput-object v31, v5, v30

    const/16 v30, 0x3

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    aput-object v31, v5, v30

    const/16 v30, 0x4

    const-string v31, "]"

    aput-object v31, v5, v30

    invoke-static {v2, v3, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 670
    new-instance v7, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v7}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 671
    .local v7, "query":Lorg/apache/lucene/search/BooleanQuery;
    const-string v2, "deleted"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v30, 0x1

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-static {v2, v3, v5, v0, v1}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v11

    .line 673
    .local v11, "atomicQueryDeleted":Lorg/apache/lucene/search/Query;
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v7, v11, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 674
    const-string v2, "date"

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/16 v30, 0x1

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-static {v2, v3, v5, v0, v1}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v26

    .line 678
    .local v26, "numericQuery":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<Ljava/lang/Long;>;"
    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v26

    invoke-virtual {v7, v0, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 679
    const/4 v6, 0x0

    .line 680
    .local v6, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    new-instance v8, Ljava/util/LinkedHashSet;

    invoke-direct {v8}, Ljava/util/LinkedHashSet;-><init>()V

    .line 681
    .local v8, "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .line 682
    .local v9, "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    const/4 v10, 0x0

    .local v10, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v5, p0

    .line 683
    invoke-direct/range {v5 .. v10}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->queryLucene(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Set;Landroid/util/SparseArray;Ljava/util/ArrayList;)Z

    .line 684
    new-instance v14, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v2

    invoke-direct {v14, v2}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;-><init>(Lcom/samsung/dcm/framework/configuration/ExtractorsController;)V

    .line 685
    .local v14, "commandFactory":Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
    new-instance v20, Ljava/util/HashSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    .line 686
    .local v20, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v2

    const-class v3, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 687
    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 688
    .local v17, "external_uri":Landroid/net/Uri;
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .line 689
    .local v21, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 690
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 704
    :cond_3
    return-void

    .line 655
    .end local v6    # "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v7    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .end local v8    # "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v9    # "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    .end local v10    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v11    # "atomicQueryDeleted":Lorg/apache/lucene/search/Query;
    .end local v14    # "commandFactory":Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
    .end local v17    # "external_uri":Landroid/net/Uri;
    .end local v20    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .end local v21    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v26    # "numericQuery":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<Ljava/lang/Long;>;"
    :cond_4
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 656
    .local v28, "startTime":J
    const/4 v2, 0x1

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 657
    .local v18, "endTime":J
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v3, "startTime = ["

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    aput-object v31, v5, v30

    const/16 v30, 0x1

    const-string v31, "]"

    aput-object v31, v5, v30

    const/16 v30, 0x2

    const-string v31, "endTime = ["

    aput-object v31, v5, v30

    const/16 v30, 0x3

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    aput-object v31, v5, v30

    const/16 v30, 0x4

    const-string v31, "]"

    aput-object v31, v5, v30

    invoke-static {v2, v3, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 658
    cmp-long v2, v28, v24

    if-gez v2, :cond_5

    .line 659
    move-wide/from16 v24, v28

    .line 660
    :cond_5
    cmp-long v2, v18, v22

    if-lez v2, :cond_0

    .line 661
    move-wide/from16 v22, v18

    goto/16 :goto_0

    .line 665
    .end local v18    # "endTime":J
    .end local v28    # "startTime":J
    :catchall_0
    move-exception v2

    if-eqz v15, :cond_6

    invoke-interface {v15}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_6

    .line 666
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2

    .line 693
    .restart local v6    # "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v7    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v8    # "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v9    # "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    .restart local v10    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v11    # "atomicQueryDeleted":Lorg/apache/lucene/search/Query;
    .restart local v14    # "commandFactory":Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
    .restart local v17    # "external_uri":Landroid/net/Uri;
    .restart local v20    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .restart local v21    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v26    # "numericQuery":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<Ljava/lang/Long;>;"
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v27

    .line 694
    .local v27, "uri":Landroid/net/Uri;
    new-instance v2, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-object/from16 v0, v27

    invoke-direct {v2, v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;)V

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-object/from16 v0, v20

    invoke-virtual {v14, v2, v0, v3}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v13

    .line 696
    .local v13, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    new-instance v16, Landroid/os/Bundle;

    invoke-direct/range {v16 .. v16}, Landroid/os/Bundle;-><init>()V

    .line 697
    .local v16, "dataBundle":Landroid/os/Bundle;
    const-string v2, "CALENDAR_RESYNC"

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 698
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 699
    .local v12, "bundle":Landroid/os/Bundle;
    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v12, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 700
    invoke-virtual {v13, v12}, Lcom/samsung/dcm/framework/extractormanager/Command;->setData(Landroid/os/Bundle;)V

    .line 701
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v3, "Sending resync command for doc uri "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-virtual/range {v27 .. v27}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v31

    aput-object v31, v5, v30

    invoke-static {v2, v3, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 702
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method private resyncLocation()V
    .locals 22

    .prologue
    .line 236
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v18, "resyncLocation  "

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v4, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    new-instance v6, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v6}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 244
    .local v6, "query":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v18, "resyncLocation: querying only for documents with deleted = 0"

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v4, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 245
    const-string v4, "deleted"

    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const/16 v20, 0x1

    const/16 v21, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v4, v0, v1, v2, v3}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v10

    .line 247
    .local v10, "atomicQuery":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<Ljava/lang/Integer;>;"
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v6, v10, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 250
    const/4 v5, 0x0

    .line 253
    .local v5, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    const/4 v7, 0x0

    .line 256
    .local v7, "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v9, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    .local v8, "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    move-object/from16 v4, p0

    .line 262
    invoke-direct/range {v4 .. v9}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->queryLucene(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Set;Landroid/util/SparseArray;Ljava/util/ArrayList;)Z

    .line 266
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v18, "No of Location data to be added: "

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v4, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    const-string v4, "external"

    invoke-static {v4}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 269
    .local v14, "external_uri":Landroid/net/Uri;
    new-instance v13, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v4

    invoke-direct {v13, v4}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;-><init>(Lcom/samsung/dcm/framework/configuration/ExtractorsController;)V

    .line 271
    .local v13, "commandFactory":Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 273
    .local v15, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v4

    const-class v18, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v4

    invoke-interface {v15, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 275
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v16

    if-ge v0, v4, :cond_1

    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 289
    :goto_1
    return-void

    .line 279
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 280
    .local v17, "loc_uri":Landroid/net/Uri;
    new-instance v4, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;)V

    sget-object v18, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-object/from16 v0, v18

    invoke-virtual {v13, v4, v15, v0}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v12

    .line 282
    .local v12, "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 283
    .local v11, "bundle":Landroid/os/Bundle;
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v18, Landroid/os/Bundle;

    invoke-direct/range {v18 .. v18}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v11, v4, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 284
    invoke-virtual {v12, v11}, Lcom/samsung/dcm/framework/extractormanager/Command;->setData(Landroid/os/Bundle;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 288
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v12    # "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v17    # "loc_uri":Landroid/net/Uri;
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mbLocationRebuild:Z

    goto :goto_1
.end method

.method private resyncLuceneDB()V
    .locals 58
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 369
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "resyncLuceneDB started "

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    const-string v4, "external"

    invoke-static {v4}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 384
    .local v5, "external_uri":Landroid/net/Uri;
    const-string v7, "media_type = ? OR media_type = ?"

    .line 388
    .local v7, "selection":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v4

    const/4 v4, 0x1

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v4

    .line 394
    .local v8, "selectionArgs":[Ljava/lang/String;
    const/16 v27, 0x0

    .line 395
    .local v27, "cursor":Landroid/database/Cursor;
    new-instance v41, Ljava/util/LinkedHashSet;

    invoke-direct/range {v41 .. v41}, Ljava/util/LinkedHashSet;-><init>()V

    .line 397
    .local v41, "mediadbIdlist":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v40, Landroid/util/SparseArray;

    invoke-direct/range {v40 .. v40}, Landroid/util/SparseArray;-><init>()V

    .line 398
    .local v40, "mediaDBContainerList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;>;"
    new-instance v46, Landroid/util/ArrayMap;

    invoke-direct/range {v46 .. v46}, Landroid/util/ArrayMap;-><init>()V

    .line 399
    .local v46, "pathToIdMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v36, Landroid/util/ArrayMap;

    invoke-direct/range {v36 .. v36}, Landroid/util/ArrayMap;-><init>()V

    .line 407
    .local v36, "idToMimeMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->DB_IMAGES_PROJECTION:[Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v4 .. v10}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v27

    .line 410
    if-nez v27, :cond_6

    .line 411
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Media cursor returned null cannot proceed."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    if-eqz v27, :cond_0

    .line 463
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    :try_start_1
    new-instance v28, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-direct {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;-><init>(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$1;)V

    .line 425
    .local v28, "data":Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    const-string v4, "_id"

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 426
    .local v24, "columnid":I
    if-ltz v24, :cond_2

    .line 427
    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v48

    .line 428
    .local v48, "rowId":I
    move-object/from16 v0, v28

    move/from16 v1, v48

    # setter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mId:I
    invoke-static {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$102(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;I)I

    .line 429
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v41

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 431
    const-string v4, "mime_type"

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 432
    if-ltz v24, :cond_2

    .line 433
    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 434
    .local v42, "mime":Ljava/lang/String;
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v36

    move-object/from16 v1, v42

    invoke-virtual {v0, v4, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    .end local v42    # "mime":Ljava/lang/String;
    .end local v48    # "rowId":I
    :cond_2
    const-string v4, "_data"

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 439
    if-lez v24, :cond_3

    .line 440
    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    # setter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mPath:Ljava/lang/String;
    invoke-static {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$202(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;Ljava/lang/String;)Ljava/lang/String;

    .line 443
    :cond_3
    const-string v4, "title"

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 444
    if-lez v24, :cond_4

    .line 445
    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    # setter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mTitle:Ljava/lang/String;
    invoke-static {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$302(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;Ljava/lang/String;)Ljava/lang/String;

    .line 448
    :cond_4
    const-string v4, "storage_id"

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 449
    if-lez v24, :cond_5

    .line 450
    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v28

    # setter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mStorageId:I
    invoke-static {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$402(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;I)I

    .line 453
    :cond_5
    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mId:I
    invoke-static/range {v28 .. v28}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$100(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)I

    move-result v4

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 454
    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mPath:Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$200(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mId:I
    invoke-static/range {v28 .. v28}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$100(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v46

    invoke-virtual {v0, v4, v6}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    .end local v24    # "columnid":I
    .end local v28    # "data":Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    :cond_6
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 416
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 462
    if-eqz v27, :cond_0

    .line 463
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 462
    :cond_7
    if-eqz v27, :cond_8

    .line 463
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_8
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "resyncLuceneDB MediaDBQuery ended "

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 474
    new-instance v13, Landroid/util/SparseArray;

    invoke-direct {v13}, Landroid/util/SparseArray;-><init>()V

    .line 475
    .local v13, "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    new-instance v12, Ljava/util/LinkedHashSet;

    invoke-direct {v12}, Ljava/util/LinkedHashSet;-><init>()V

    .line 476
    .local v12, "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 485
    .local v14, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v11, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v11}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 486
    .local v11, "query":Lorg/apache/lucene/search/BooleanQuery;
    const-string v4, "deleted"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v15, 0x1

    const/16 v55, 0x1

    move/from16 v0, v55

    invoke-static {v4, v6, v9, v15, v0}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    sget-object v6, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v11, v4, v6}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 488
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/configuration/StorageController;->getMountedVolumeList()Ljava/util/List;

    move-result-object v50

    .line 489
    .local v50, "storageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v47, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct/range {v47 .. v47}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 490
    .local v47, "querySid":Lorg/apache/lucene/search/BooleanQuery;
    invoke-interface/range {v50 .. v50}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .local v34, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v49

    .line 491
    .local v49, "storageId":I
    const-string v4, "storage_id"

    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v15, 0x1

    const/16 v55, 0x1

    move/from16 v0, v55

    invoke-static {v4, v6, v9, v15, v0}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v51

    .line 493
    .local v51, "storageQuery":Lorg/apache/lucene/search/Query;
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v47

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_1

    .line 457
    .end local v11    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .end local v12    # "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v13    # "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    .end local v14    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v34    # "i$":Ljava/util/Iterator;
    .end local v47    # "querySid":Lorg/apache/lucene/search/BooleanQuery;
    .end local v49    # "storageId":I
    .end local v50    # "storageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v51    # "storageQuery":Lorg/apache/lucene/search/Query;
    :catch_0
    move-exception v31

    .line 458
    .local v31, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Unhandled exception"

    move-object/from16 v0, v31

    invoke-static {v4, v6, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 462
    if-eqz v27, :cond_0

    .line 463
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 462
    .end local v31    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v27, :cond_9

    .line 463
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v4

    .line 495
    .restart local v11    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v12    # "luceneIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v13    # "luceneDocsList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    .restart local v14    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v34    # "i$":Ljava/util/Iterator;
    .restart local v47    # "querySid":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v50    # "storageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_a
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v47

    invoke-virtual {v11, v0, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 498
    const/4 v10, 0x0

    .local v10, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    move-object/from16 v9, p0

    .line 503
    invoke-direct/range {v9 .. v14}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->queryLucene(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Set;Landroid/util/SparseArray;Ljava/util/ArrayList;)Z

    .line 514
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 516
    .local v26, "commonidlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Uris from media DB: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface/range {v41 .. v41}, Ljava/util/Set;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 517
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Uris from lucene DB: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 519
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    .line 520
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 521
    move-object/from16 v0, v26

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->retainAll(Ljava/util/Collection;)Z

    .line 523
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Uris that are common: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 525
    move-object/from16 v0, v26

    invoke-interface {v12, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 526
    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 528
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Uris to Add: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface/range {v41 .. v41}, Ljava/util/Set;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 530
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "SDCard status = "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 532
    new-instance v25, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;-><init>(Lcom/samsung/dcm/framework/configuration/ExtractorsController;)V

    .line 536
    .local v25, "commandFactory":Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
    new-instance v17, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct/range {v17 .. v17}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 537
    .local v17, "query2":Lorg/apache/lucene/search/BooleanQuery;
    const-string v4, "deleted"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v15, 0x1

    const/16 v55, 0x1

    move/from16 v0, v55

    invoke-static {v4, v6, v9, v15, v0}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v22

    .line 539
    .local v22, "atomicQuery2":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<Ljava/lang/Integer;>;"
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 540
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v17

    move-object/from16 v1, v47

    invoke-virtual {v0, v1, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 542
    const/16 v16, 0x0

    .line 543
    .local v16, "lastscoreDoc2":Lorg/apache/lucene/search/ScoreDoc;
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 544
    .local v18, "luceneIdList2":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v19, Landroid/util/SparseArray;

    invoke-direct/range {v19 .. v19}, Landroid/util/SparseArray;-><init>()V

    .line 545
    .local v19, "luceneDocsList2":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lorg/apache/lucene/document/Document;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .local v20, "locationList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v15, p0

    .line 546
    invoke-direct/range {v15 .. v20}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->queryLucene(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Set;Landroid/util/SparseArray;Ljava/util/ArrayList;)Z

    .line 547
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No of Uris with deleted flag as 1 =  "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->size()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 550
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :cond_b
    :goto_2
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/Integer;

    .line 551
    .local v35, "id":Ljava/lang/Integer;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lorg/apache/lucene/document/Document;

    .line 552
    .local v30, "doc":Lorg/apache/lucene/document/Document;
    if-eqz v30, :cond_b

    .line 553
    const-string v4, "path"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Ljava/lang/Integer;

    .line 555
    .local v44, "new_uriId":Ljava/lang/Integer;
    if-eqz v44, :cond_b

    .line 556
    invoke-virtual/range {v44 .. v44}, Ljava/lang/Integer;->intValue()I

    move-result v54

    .line 557
    .local v54, "uriid":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v54

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 558
    .local v21, "_uri":Landroid/net/Uri;
    invoke-static/range {v54 .. v54}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Ljava/lang/String;

    .line 559
    .local v43, "mimeString":Ljava/lang/String;
    if-eqz v43, :cond_b

    .line 560
    move-object/from16 v0, v21

    move-object/from16 v1, v43

    invoke-static {v0, v1}, Lcom/samsung/commons/Algorithms;->appendMimeToUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 561
    move-object/from16 v0, v40

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$200(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)Ljava/lang/String;

    move-result-object v45

    .line 562
    .local v45, "path":Ljava/lang/String;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Changing deleted flag for uri="

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v21, v9, v15

    const/4 v15, 0x1

    const-string v55, ", with path="

    aput-object v55, v9, v15

    const/4 v15, 0x2

    aput-object v45, v9, v15

    const/4 v15, 0x3

    const-string v55, "and  mime="

    aput-object v55, v9, v15

    const/4 v15, 0x4

    aput-object v43, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 564
    new-instance v29, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    move-object/from16 v2, v45

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 565
    .local v29, "dcmId":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v38

    .line 567
    .local v38, "insertUpdateCommand":Lcom/samsung/dcm/framework/extractormanager/Command;
    const/4 v4, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/Command;->setIsSdCardAction(Z)V

    .line 568
    move-object/from16 v0, v40

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mStorageId:I
    invoke-static {v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$400(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)I

    move-result v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/Command;->setStorageId(I)V

    .line 569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    move-object/from16 v0, v41

    move-object/from16 v1, v44

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 576
    .end local v21    # "_uri":Landroid/net/Uri;
    .end local v29    # "dcmId":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .end local v30    # "doc":Lorg/apache/lucene/document/Document;
    .end local v35    # "id":Ljava/lang/Integer;
    .end local v38    # "insertUpdateCommand":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v43    # "mimeString":Ljava/lang/String;
    .end local v44    # "new_uriId":Ljava/lang/Integer;
    .end local v45    # "path":Ljava/lang/String;
    .end local v54    # "uriid":I
    :cond_c
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Uris to add: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface/range {v41 .. v41}, Ljava/util/Set;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578
    invoke-interface/range {v41 .. v41}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :cond_d
    :goto_3
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v35

    .line 579
    .local v35, "id":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v4

    if-nez v4, :cond_0

    .line 582
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v53

    .line 583
    .local v53, "uri":Landroid/net/Uri;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Inserting with id="

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 584
    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Ljava/lang/String;

    .line 585
    .restart local v43    # "mimeString":Ljava/lang/String;
    if-eqz v43, :cond_d

    .line 586
    move-object/from16 v0, v53

    move-object/from16 v1, v43

    invoke-static {v0, v1}, Lcom/samsung/commons/Algorithms;->appendMimeToUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v53

    .line 587
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Inserting uri="

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v53, v9, v15

    const/4 v15, 0x1

    const-string v55, ", with mime="

    aput-object v55, v9, v15

    const/4 v15, 0x2

    aput-object v43, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 588
    new-instance v4, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-object/from16 v0, v53

    invoke-direct {v4, v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;)V

    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v6}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v37

    .line 589
    .local v37, "insertCommand":Lcom/samsung/dcm/framework/extractormanager/Command;
    move-object/from16 v0, v40

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 590
    move-object/from16 v0, v40

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mStorageId:I
    invoke-static {v4}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->access$400(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)I

    move-result v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Lcom/samsung/dcm/framework/extractormanager/Command;->setStorageId(I)V

    .line 592
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 596
    .end local v35    # "id":I
    .end local v37    # "insertCommand":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v43    # "mimeString":Ljava/lang/String;
    .end local v53    # "uri":Landroid/net/Uri;
    :cond_f
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Uris to remove: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :goto_4
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v35

    .line 598
    .restart local v35    # "id":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v4

    if-nez v4, :cond_0

    .line 601
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v53

    .line 602
    .restart local v53    # "uri":Landroid/net/Uri;
    new-instance v52, Lorg/apache/lucene/index/Term;

    const-string v4, "uri"

    invoke-virtual/range {v53 .. v53}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v52

    invoke-direct {v0, v4, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    .local v52, "term":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTSearcherId:J

    move-wide/from16 v56, v0

    invoke-static/range {v56 .. v57}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, v52

    invoke-virtual {v4, v6, v0, v9}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->deleteDocuments(Ljava/lang/Long;Lorg/apache/lucene/index/Term;Ljava/lang/Integer;)J

    goto :goto_4

    .line 606
    .end local v35    # "id":I
    .end local v52    # "term":Lorg/apache/lucene/index/Term;
    .end local v53    # "uri":Landroid/net/Uri;
    :cond_10
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "No of Location data to be added: "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v55

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    aput-object v55, v9, v15

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 607
    new-instance v32, Ljava/util/HashSet;

    invoke-direct/range {v32 .. v32}, Ljava/util/HashSet;-><init>()V

    .line 608
    .local v32, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v4

    const-class v6, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    invoke-virtual {v4, v6}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 609
    const/16 v33, 0x0

    .local v33, "i":I
    :goto_5
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v33

    if-ge v0, v4, :cond_11

    .line 610
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v4

    if-nez v4, :cond_0

    .line 613
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v33

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    .line 614
    .local v39, "loc_uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    new-instance v6, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-object/from16 v0, v39

    invoke-direct {v6, v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;)V

    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    invoke-virtual {v0, v6, v1, v9}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 609
    add-int/lit8 v33, v33, 0x1

    goto :goto_5

    .line 622
    .end local v39    # "loc_uri":Landroid/net/Uri;
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->isCalendarProcessingFinished(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_12

    const/16 v23, 0x1

    .line 623
    .local v23, "calendarResync":Z
    :goto_6
    if-eqz v23, :cond_13

    .line 624
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "calendar resync required"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 625
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->resyncCalendarEvents()V

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setCalendarProcessingStarted(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 622
    .end local v23    # "calendarResync":Z
    :cond_12
    const/16 v23, 0x0

    goto :goto_6

    .line 630
    .restart local v23    # "calendarResync":Z
    :cond_13
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "calendar resync not required"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v6, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected afterInternalRun()V
    .locals 0

    .prologue
    .line 146
    invoke-super {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->afterInternalRun()V

    .line 147
    return-void
.end method

.method public doStorageDBCleanup()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 787
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v5, "Cleaner called"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 788
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 789
    .local v2, "query":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v0, 0x0

    .line 790
    .local v0, "atomicQuery":Lorg/apache/lucene/search/Query;
    const-string v4, "deleted"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4, v5, v6, v7, v7}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    .line 792
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v0, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 794
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v3

    .line 795
    .local v3, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 798
    :try_start_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const-wide v6, 0x7fffffffffffffffL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->deleteDocuments(Ljava/lang/Long;Lorg/apache/lucene/search/Query;)J
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_0

    .line 803
    :goto_0
    return-void

    .line 799
    :catch_0
    move-exception v1

    .line 800
    .local v1, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v5, "doStorageDBCleanup"

    invoke-static {v4, v5, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 801
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    invoke-static {v4, v8}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto :goto_0
.end method

.method public finishNow()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDBLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDBLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 218
    :cond_0
    invoke-super {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->finishNow()V

    .line 219
    return-void
.end method

.method public declared-synchronized getLocationRebuild()Z
    .locals 1

    .prologue
    .line 820
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mbLocationRebuild:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNextBatch()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 725
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 726
    .local v0, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    const/16 v3, 0x64

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 728
    .local v1, "commandsToCopy":I
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v3, v7, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 729
    .local v2, "rebuildCmdsView":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 730
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 732
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "getNextBatch commands to process within this batch = "

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 733
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "getNextBatch commands still waiting to be processed = "

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 735
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 736
    iput-boolean v8, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    .line 737
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 738
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->finishNow()V

    .line 742
    :cond_0
    return-object v0
.end method

.method protected internalRun()V
    .locals 18

    .prologue
    .line 152
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 153
    .local v10, "start":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mRebuildCmds:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 154
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    .line 155
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v12, "external"

    invoke-static {v12}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 157
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v9, :cond_3

    .line 160
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->createLuceneReader()Z

    .line 161
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mbLocationRebuild:Z

    if-eqz v9, :cond_4

    .line 162
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->resyncLocation()V

    .line 167
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->releaseLuceneReader()Z

    .line 168
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 169
    .local v6, "end":J
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string/jumbo v12, "verifyDatabase Time taken ="

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    sub-long v16, v6, v10

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, " ms"

    aput-object v15, v13, v14

    invoke-static {v9, v12, v13}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v12, "Starting media manager do operation"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v9, v12, v13}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-static {v9, v12}, Lcom/samsung/dcm/framework/utils/SystemUtils;->setHeavySharedPref(Landroid/content/Context;Z)V

    .line 174
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    if-eqz v9, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->isFinishedNow()Z

    move-result v9

    if-nez v9, :cond_7

    .line 175
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->getNextBatch()Ljava/util/ArrayList;

    move-result-object v3

    .line 176
    .local v3, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 177
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 178
    .local v2, "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v9, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->execute(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 188
    .end local v2    # "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v3    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "end":J
    :catch_0
    move-exception v4

    .line 189
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v12, "Internal run "

    invoke-static {v9, v12, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 190
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 193
    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->releaseLuceneReader()Z

    .line 195
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    if-eqz v9, :cond_2

    .line 196
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v9}, Landroid/content/ContentProviderClient;->release()Z

    .line 197
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 203
    :cond_2
    :goto_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    invoke-static {v9, v12}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 211
    .end local v4    # "e":Ljava/io/IOException;
    .end local v10    # "start":J
    :cond_3
    :goto_4
    return-void

    .line 165
    .restart local v10    # "start":J
    :cond_4
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->resyncLuceneDB()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 192
    :catchall_0
    move-exception v9

    .line 193
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->releaseLuceneReader()Z

    .line 195
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    if-eqz v12, :cond_5

    .line 196
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v12}, Landroid/content/ContentProviderClient;->release()Z

    .line 197
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 203
    :cond_5
    :goto_5
    :try_start_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    invoke-static {v12, v13}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    throw v9
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 206
    .end local v10    # "start":J
    :catch_1
    move-exception v4

    .line 207
    .local v4, "e":Ljava/lang/Exception;
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v12, "InterruptedException"

    invoke-static {v9, v12, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-static {v9, v12}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto :goto_4

    .line 181
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "end":J
    .restart local v10    # "start":J
    :cond_6
    :try_start_8
    new-instance v9, Ljava/util/concurrent/CountDownLatch;

    const/4 v12, 0x1

    invoke-direct {v9, v12}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDBLatch:Ljava/util/concurrent/CountDownLatch;

    .line 182
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mDBLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v12, 0x5

    sget-object v14, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v12, v13, v14}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v8

    .line 183
    .local v8, "retVal":Z
    if-nez v8, :cond_0

    .line 184
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string/jumbo v12, "waiting time elapsed before the count reached zero"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v9, v12, v13}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 193
    .end local v3    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "retVal":Z
    :cond_7
    :try_start_9
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->releaseLuceneReader()Z

    .line 195
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    if-eqz v9, :cond_8

    .line 196
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v9}, Landroid/content/ContentProviderClient;->release()Z

    .line 197
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    .line 203
    :cond_8
    :goto_6
    :try_start_a
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mReBuiltCompleted:Z

    invoke-static {v9, v12}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto :goto_4

    .line 199
    :catch_2
    move-exception v4

    .line 200
    .local v4, "e":Ljava/io/IOException;
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v12, "internal run"

    invoke-static {v9, v12, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 199
    .end local v6    # "end":J
    :catch_3
    move-exception v4

    .line 200
    sget-object v9, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v12, "internal run"

    invoke-static {v9, v12, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 199
    .end local v4    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 200
    .restart local v4    # "e":Ljava/io/IOException;
    sget-object v12, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v13, "internal run"

    invoke-static {v12, v13, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto :goto_5
.end method

.method public postDBrebuildIntent(Z)Z
    .locals 10
    .param p1, "rebuildLocation"    # Z

    .prologue
    const/4 v9, 0x1

    .line 757
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->TAG:Ljava/lang/String;

    const-string v5, "postDBrebuildIntent rebuildLocation = "

    new-array v6, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 758
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 759
    .local v1, "dbRebuild":Landroid/content/Intent;
    const-string v4, "com.samsung.dcm"

    const-string v5, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 760
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 761
    .local v0, "actionData":Landroid/os/Bundle;
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->RebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 767
    .local v2, "extrasBundle":Landroid/os/Bundle;
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    if-ne v9, p1, :cond_0

    .line 775
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 776
    .local v3, "optionsBundle":Landroid/os/Bundle;
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->RebuildLocation:Lcom/samsung/dcm/framework/IntentHandler$ValidOption;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidOption;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 777
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 780
    .end local v3    # "optionsBundle":Landroid/os/Bundle;
    :cond_0
    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 781
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 782
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 783
    return v9
.end method

.method public declared-synchronized setLocationRebuild(Z)V
    .locals 1
    .param p1, "locationRebuild"    # Z

    .prologue
    .line 812
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->mbLocationRebuild:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 813
    monitor-exit p0

    return-void

    .line 812
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
