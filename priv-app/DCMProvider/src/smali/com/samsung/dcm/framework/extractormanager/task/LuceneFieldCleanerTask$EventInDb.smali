.class Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;
.super Ljava/lang/Object;
.source "LuceneFieldCleanerTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventInDb"
.end annotation


# instance fields
.field endTime:J

.field eventTitle:Ljava/lang/String;

.field startTime:J


# direct methods
.method public constructor <init>(JJLjava/lang/String;)V
    .locals 1
    .param p1, "sTime"    # J
    .param p3, "eTime"    # J
    .param p5, "title"    # Ljava/lang/String;

    .prologue
    .line 349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350
    iput-wide p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;->startTime:J

    .line 351
    iput-wide p3, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;->endTime:J

    .line 352
    iput-object p5, p0, Lcom/samsung/dcm/framework/extractormanager/task/LuceneFieldCleanerTask$EventInDb;->eventTitle:Ljava/lang/String;

    .line 353
    return-void
.end method
