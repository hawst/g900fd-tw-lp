.class Lcom/samsung/dcm/framework/configuration/StorageController$1;
.super Ljava/lang/Object;
.source "StorageController.java"

# interfaces
.implements Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/configuration/StorageController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/configuration/StorageController;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/configuration/StorageController;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIntentReceived(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;Landroid/os/Bundle;)V
    .locals 16
    .param p1, "id"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 84
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    # invokes: Lcom/samsung/dcm/framework/configuration/StorageController;->updateVolumeInformation()V
    invoke-static {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$000(Lcom/samsung/dcm/framework/configuration/StorageController;)V

    .line 86
    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$100()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Bundle = "

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object p2, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    const-string v12, "path"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 88
    .local v10, "path":Ljava/lang/String;
    const/4 v8, 0x0

    .line 89
    .local v8, "isOSRebuildSentForSDCard":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    iget-boolean v12, v12, Lcom/samsung/dcm/framework/configuration/StorageController;->mbStartSDCardRebuild:Z

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->getSDCardSubSystemEntry()Ljava/util/Map$Entry;

    move-result-object v11

    .line 92
    .local v11, "subSystemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->INTERNAL_SYSTEM_MEDIA:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$200(Lcom/samsung/dcm/framework/configuration/StorageController;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 93
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    const/4 v13, 0x1

    iput-boolean v13, v12, Lcom/samsung/dcm/framework/configuration/StorageController;->mbStartSDCardRebuild:Z

    .line 95
    :cond_0
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "emulated"

    invoke-virtual {v10, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 97
    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$100()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Sending intent for OS Upgrade for Phone contents"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    const/4 v12, 0x7

    invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "actualPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$300(Lcom/samsung/dcm/framework/configuration/StorageController;)Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-virtual {v13, v2}, Lcom/samsung/dcm/framework/configuration/StorageController;->getStorageId(Ljava/lang/String;)I

    move-result v13

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->startOSUpgradeRebuild(Landroid/content/Context;IZ)Z

    .line 107
    .end local v2    # "actualPath":Ljava/lang/String;
    :cond_1
    :goto_1
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    if-eqz v11, :cond_6

    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-object v12, v12, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mSubsystem:Ljava/lang/String;

    const-string v13, "sd"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-object v12, v12, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mSubsystem:Ljava/lang/String;

    const-string v13, "private"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    :cond_2
    if-nez v8, :cond_6

    .line 110
    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$100()Ljava/lang/String;

    move-result-object v12

    const-string v13, "send intent for SD card scan finished"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    new-instance v7, Landroid/content/Intent;

    const-string v12, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v7, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    .local v7, "intent":Landroid/content/Intent;
    const-string v12, "com.samsung.dcm"

    const-string v13, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v7, v12, v13}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 115
    .local v1, "actionData":Landroid/os/Bundle;
    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->SDScanFinished:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v13}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v13, "storage_id"

    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v1, v13, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 119
    .local v6, "extrasBundle":Landroid/os/Bundle;
    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v13}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {v13}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    sget-object v12, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v12}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {v7, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 127
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$300(Lcom/samsung/dcm/framework/configuration/StorageController;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 152
    .end local v1    # "actionData":Landroid/os/Bundle;
    .end local v6    # "extrasBundle":Landroid/os/Bundle;
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_3
    :goto_2
    return-void

    .line 89
    .end local v11    # "subSystemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-virtual {v12, v10}, Lcom/samsung/dcm/framework/configuration/StorageController;->getSubSystemEntry(Ljava/lang/String;)Ljava/util/Map$Entry;

    move-result-object v11

    goto/16 :goto_0

    .line 100
    .restart local v11    # "subSystemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :cond_5
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    const-string v12, "extSdCard"

    invoke-virtual {v10, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 102
    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$100()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Sending intent for OS Upgrade for External SD contents"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    const/4 v12, 0x7

    invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 104
    .restart local v2    # "actualPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$300(Lcom/samsung/dcm/framework/configuration/StorageController;)Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-virtual {v13, v2}, Lcom/samsung/dcm/framework/configuration/StorageController;->getStorageId(Ljava/lang/String;)I

    move-result v13

    const/4 v14, 0x1

    invoke-static {v12, v13, v14}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->startOSUpgradeRebuild(Landroid/content/Context;IZ)Z

    move-result v8

    goto/16 :goto_1

    .line 129
    .end local v2    # "actualPath":Ljava/lang/String;
    :cond_6
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    if-eqz v11, :cond_3

    .line 131
    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Sending BULK_DELETE for storage_id = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 133
    .local v9, "options":Landroid/os/Bundle;
    const-string v12, "bulk"

    const/4 v13, 0x1

    invoke-virtual {v9, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 134
    const-string v12, "reason"

    const-string v13, "sdcard"

    invoke-virtual {v9, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v12, "is_sdcard"

    const/4 v13, 0x1

    invoke-virtual {v9, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 136
    new-instance v3, Landroid/content/Intent;

    const-string v12, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v3, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 137
    .local v3, "bulkdeleteIntent":Landroid/content/Intent;
    const-string v12, "com.samsung.dcm"

    const-string v13, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 139
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v12, "operation"

    const-string v13, "delete"

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v12, "origin"

    sget-object v13, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v13}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 142
    .local v5, "dataBundle":Landroid/os/Bundle;
    const-string v13, "storageId"

    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v5, v13, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    if-eqz v9, :cond_7

    .line 144
    const-string v12, "options"

    invoke-virtual {v5, v12, v9}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 146
    :cond_7
    const-string v12, "data"

    invoke-virtual {v4, v12, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 148
    invoke-virtual {v3, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 150
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;->this$0:Lcom/samsung/dcm/framework/configuration/StorageController;

    # getter for: Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->access$300(Lcom/samsung/dcm/framework/configuration/StorageController;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2
.end method
