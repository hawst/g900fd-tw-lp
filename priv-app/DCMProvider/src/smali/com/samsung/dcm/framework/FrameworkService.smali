.class public Lcom/samsung/dcm/framework/FrameworkService;
.super Landroid/app/Service;
.source "FrameworkService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

.field private mIntentHandler:Lcom/samsung/dcm/framework/IntentHandler;

.field private mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/dcm/framework/FrameworkService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 44
    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 45
    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mIntentHandler:Lcom/samsung/dcm/framework/IntentHandler;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 116
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onBind"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 52
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate FrameworkService"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 55
    iget-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    if-nez v0, :cond_0

    .line 56
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate FrameworkService mConfig is null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    new-instance v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 58
    iget-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0, v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->bind(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0, v1}, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->createAndStart(Lcom/samsung/dcm/framework/configuration/DcmTransport;Lcom/samsung/dcm/framework/configuration/DCMConfig;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 63
    iget-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    const-string v1, "Transport cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler;

    iget-object v1, p0, Lcom/samsung/dcm/framework/FrameworkService;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    new-instance v2, Lcom/samsung/dcm/framework/broadcaster/Broadcaster;

    invoke-direct {v2, p0}, Lcom/samsung/dcm/framework/broadcaster/Broadcaster;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/samsung/dcm/framework/IntentHandler;-><init>(Lcom/samsung/dcm/framework/FrameworkService;Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/broadcaster/Broadcaster;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/FrameworkService;->mIntentHandler:Lcom/samsung/dcm/framework/IntentHandler;

    .line 69
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate FrameworkService end"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy Start"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 76
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy end"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v10, 0x0

    .line 81
    if-nez p1, :cond_0

    .line 82
    sget-object v5, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v6, "intent is null"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    :goto_0
    return v4

    .line 85
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 87
    iget-object v6, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->isSystemOnShutdown()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/samsung/dcm/framework/FrameworkService;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getSuccessState()Z

    move-result v6

    if-nez v6, :cond_2

    .line 88
    :cond_1
    sget-object v5, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v6, "DCM Initialization was not success, cannot proceed"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :cond_2
    sget-object v6, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand(intent="

    new-array v8, v5, [Ljava/lang/Object;

    aput-object p1, v8, v10

    const-string v9, ", startId="

    aput-object v9, v8, v11

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    const-wide/16 v2, 0x0

    .local v2, "mstartTime":J
    const-wide/16 v0, 0x0

    .line 95
    .local v0, "mendTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 96
    iget-object v6, p0, Lcom/samsung/dcm/framework/FrameworkService;->mIntentHandler:Lcom/samsung/dcm/framework/IntentHandler;

    invoke-virtual {v6, p1, p3}, Lcom/samsung/dcm/framework/IntentHandler;->handleIntent(Landroid/content/Intent;I)V

    .line 97
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long/2addr v6, v2

    const-wide/16 v8, 0x3e8

    div-long v0, v6, v8

    .line 98
    sget-object v6, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v7, "[DCM_Performance] onStartCommand completed , startId="

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    const-string v9, " in time "

    aput-object v9, v8, v11

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v4

    const-string v4, " mirosec. "

    aput-object v4, v8, v5

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v5

    .line 99
    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 5
    .param p1, "level"    # I

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Service;->onTrimMemory(I)V

    .line 123
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onTrimMemory Level:"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 104
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    .line 106
    sget-object v0, Lcom/samsung/dcm/framework/FrameworkService;->TAG:Ljava/lang/String;

    const-string v1, "onUnbind"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    return v3
.end method
