.class Lcom/samsung/dcm/framework/lucene/WriterFactory;
.super Ljava/lang/Object;
.source "WriterFactory.java"


# static fields
.field private static final RAM_BUFFER_SIZE:I = 0x10

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private mCodec:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Lorg/apache/lucene/codecs/Codec;",
            ">;"
        }
    .end annotation
.end field

.field private final mCodecVersion:Lorg/apache/lucene/util/Version;

.field private mContext:Landroid/content/Context;

.field private mDCMIndexDeletionPolicy:Lcom/samsung/dcm/framework/lucene/DCMIndexDeletionPolicy;

.field private mDCMMergeScheduler:Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;

.field private mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

.field private final mFieldAnalyzers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;"
        }
    .end annotation
.end field

.field private mIndexDir:Ljava/io/File;

.field private mIndexDirectory:Lorg/apache/lucene/store/Directory;

.field private mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

.field private mIsOpen:Z

.field private mOut:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/io/PrintStream;",
            ">;"
        }
    .end annotation
.end field

.field private mTaxoDir:Ljava/io/File;

.field private mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

.field private mTaxonomyDirectory:Lorg/apache/lucene/store/Directory;

.field private mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_43:Lorg/apache/lucene/util/Version;

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodecVersion:Lorg/apache/lucene/util/Version;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z

    .line 67
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodec:Lcom/google/common/base/Optional;

    .line 68
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 69
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 70
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    .line 71
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mOut:Lcom/google/common/base/Optional;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mFieldAnalyzers:Ljava/util/Map;

    .line 74
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 79
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    .line 80
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxonomyDirectory:Lorg/apache/lucene/store/Directory;

    .line 81
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    .line 96
    const-string v0, "Context cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    .line 109
    new-instance v0, Lorg/apache/lucene/analysis/core/WhitespaceAnalyzer;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodecVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/core/WhitespaceAnalyzer;-><init>(Lorg/apache/lucene/util/Version;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 110
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodecVersion:Lorg/apache/lucene/util/Version;

    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 111
    new-instance v0, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mDCMMergeScheduler:Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;

    .line 113
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mDCMMergeScheduler:Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergeScheduler(Lorg/apache/lucene/index/MergeScheduler;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 114
    return-void
.end method

.method private doTryDeleteFiles(Ljava/io/File;)V
    .locals 8
    .param p1, "dFile"    # Ljava/io/File;

    .prologue
    const/4 v7, 0x0

    .line 288
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 289
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rm -rf "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "deleteCmd":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    .line 292
    .local v2, "runtime":Ljava/lang/Runtime;
    sget-object v3, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v4, "doTryDeleteFiles command = "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    if-eqz v2, :cond_0

    .line 294
    invoke-virtual {v2, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    .end local v0    # "deleteCmd":Ljava/lang/String;
    .end local v2    # "runtime":Ljava/lang/Runtime;
    :cond_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 302
    :goto_0
    return-void

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v4, "doTryDeleteFiles exception"

    invoke-static {v3, v4, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    invoke-static {v4, v7}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    throw v3
.end method

.method private doTryUnlocking()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 306
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    invoke-static {v1}, Lorg/apache/lucene/index/IndexWriter;->isLocked(Lorg/apache/lucene/store/Directory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 307
    sget-object v1, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v2, "Unlocking index Directory"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    invoke-static {v1}, Lorg/apache/lucene/index/IndexWriter;->unlock(Lorg/apache/lucene/store/Directory;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e2":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v2, "Unlocking exception"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private initIndexWriter()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 166
    monitor-enter p0

    .line 167
    const/4 v0, 0x0

    .line 173
    .local v0, "commits":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexCommit;>;"
    :try_start_0
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriterConfig;->getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 175
    .local v4, "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :try_start_1
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    invoke-static {v7}, Lorg/apache/lucene/index/DirectoryReader;->listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/List;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 193
    :goto_0
    :try_start_2
    new-instance v7, Lorg/apache/lucene/index/IndexWriter;

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v9, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v7, v8, v9}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 213
    :goto_1
    :try_start_3
    sget-object v7, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-ne v4, v7, :cond_0

    if-eqz v0, :cond_0

    .line 229
    :try_start_4
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->verifyLuceneDB(Ljava/util/Collection;)V
    :try_end_4
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 266
    :cond_0
    :goto_2
    :try_start_5
    sget-object v7, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v8, "TAXO in mode"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->name()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    new-instance v7, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxonomyDirectory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v7, v8, v4}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)V

    iput-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 279
    :try_start_6
    sget-object v7, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v8, "Before facet"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    new-instance v7, Lorg/apache/lucene/facet/index/FacetFields;

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-direct {v7, v8}, Lorg/apache/lucene/facet/index/FacetFields;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;)V

    iput-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    .line 281
    sget-object v7, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v8, "After facet=!null ?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    if-eqz v11, :cond_1

    :goto_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 282
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z

    .line 283
    monitor-exit p0

    .line 284
    return-void

    .line 176
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Ljava/io/IOException;
    sget-object v7, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v8, "FreshIndex "

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    sget-object v7, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v8, "FreshIndex "

    invoke-static {v7, v8, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 180
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 181
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    goto :goto_0

    .line 283
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v5

    .line 194
    .restart local v4    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catch_1
    move-exception v1

    .line 195
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_7
    sget-object v4, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 196
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v7, v4}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 198
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 199
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 201
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryUnlocking()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 203
    :try_start_8
    new-instance v7, Lorg/apache/lucene/index/IndexWriter;

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v9, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v7, v8, v9}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 204
    :catch_2
    move-exception v2

    .line 205
    .local v2, "e1":Ljava/io/IOException;
    :try_start_9
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v1}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 206
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 207
    .local v3, "initError":Ljava/lang/ExceptionInInitializerError;
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v3}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 209
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    :catch_4
    move-exception v3

    .line 210
    .restart local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v3}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 230
    .end local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    :catch_5
    move-exception v1

    .line 231
    .local v1, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v4, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 234
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 235
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 237
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v7, v4}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 238
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryUnlocking()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 240
    :try_start_a
    new-instance v7, Lorg/apache/lucene/index/IndexWriter;

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v9, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v7, v8, v9}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_2

    .line 241
    :catch_6
    move-exception v2

    .line 242
    .restart local v2    # "e1":Ljava/io/IOException;
    :try_start_b
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v1}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 243
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_7
    move-exception v3

    .line 244
    .restart local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v3}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 246
    .end local v1    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    .end local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    :catch_8
    move-exception v1

    .line 247
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 249
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 250
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 251
    iget-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v7, v4}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 252
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryUnlocking()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 254
    :try_start_c
    new-instance v7, Lorg/apache/lucene/index/IndexWriter;

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v9, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v7, v8, v9}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v7, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_c .. :try_end_c} :catch_a
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_2

    .line 255
    :catch_9
    move-exception v2

    .line 256
    .restart local v2    # "e1":Ljava/io/IOException;
    :try_start_d
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v1}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 258
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_a
    move-exception v3

    .line 259
    .restart local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;

    invoke-direct {v5, v3}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 268
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "initError":Ljava/lang/ExceptionInInitializerError;
    :catch_b
    move-exception v1

    .line 269
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v6, "TAXO failed"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 271
    :try_start_e
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexWriter;->close(Z)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 275
    :goto_4
    const/4 v5, 0x0

    :try_start_f
    iput-object v5, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 276
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;

    invoke-direct {v5, v1}, Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 272
    :catch_c
    move-exception v2

    .line 273
    .restart local v2    # "e1":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v6, "Index failed"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_4

    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_1
    move v5, v6

    .line 281
    goto/16 :goto_3
.end method

.method private verifyLuceneDB(Ljava/util/Collection;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    .local p1, "commits":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexCommit;>;"
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB ++"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    const/4 v2, 0x0

    .line 330
    .local v2, "bLuceneDBVerified":Z
    const/4 v1, 0x0

    .line 333
    .local v1, "bDBrebuildNeeded":Z
    if-eqz p1, :cond_a

    .line 337
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 338
    .local v9, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/IndexCommit;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 340
    .local v5, "ic_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/index/IndexCommit;>;"
    if-eqz v9, :cond_0

    .line 341
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 342
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v12

    add-int/lit8 v6, v12, -0x2

    .line 349
    .local v6, "ic_point":I
    const/4 v8, 0x0

    .line 350
    .local v8, "ir":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    :goto_1
    if-nez v2, :cond_a

    .line 352
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v8

    .line 353
    new-instance v7, Lorg/apache/lucene/search/IndexSearcher;

    invoke-direct {v7, v8}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    .line 354
    .local v7, "indexSearcher":Lorg/apache/lucene/search/IndexSearcher;
    new-instance v10, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v10}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 355
    .local v10, "query":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v12, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v12}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    sget-object v13, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v10, v12, v13}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 357
    const/16 v12, 0x64

    invoke-virtual {v7, v10, v12}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v11

    .line 366
    .local v11, "topdocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v2, 0x1

    .line 367
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB OK breaking"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    if-eqz v8, :cond_2

    .line 402
    invoke-virtual {v8}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 405
    :cond_2
    if-eqz v1, :cond_1

    .line 406
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 407
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB: Auto Rebuild  enabled, context:"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 369
    .end local v7    # "indexSearcher":Lorg/apache/lucene/search/IndexSearcher;
    .end local v10    # "query":Lorg/apache/lucene/search/BooleanQuery;
    .end local v11    # "topdocs":Lorg/apache/lucene/search/TopDocs;
    :catch_0
    move-exception v4

    .line 370
    .local v4, "e":Ljava/lang/Exception;
    if-eqz v8, :cond_3

    .line 371
    :try_start_1
    invoke-virtual {v8}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 372
    const/4 v8, 0x0

    .line 374
    :cond_3
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB exception"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v12, :cond_4

    .line 376
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v12}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 377
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 380
    :cond_4
    if-eqz v5, :cond_6

    if-ltz v6, :cond_6

    .line 381
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexCommit;

    .line 382
    .local v3, "commit":Lorg/apache/lucene/index/IndexCommit;
    add-int/lit8 v6, v6, -0x1

    .line 383
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB loading IndexCommit ="

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 386
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v12, v3}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 387
    new-instance v12, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v12, v13, v14}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388
    const/4 v1, 0x1

    .line 401
    if-eqz v8, :cond_5

    .line 402
    invoke-virtual {v8}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 405
    :cond_5
    if-eqz v1, :cond_1

    .line 406
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 407
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB: Auto Rebuild  enabled, context:"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 391
    .end local v3    # "commit":Lorg/apache/lucene/index/IndexCommit;
    :cond_6
    :try_start_2
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB NOT OK throwing"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v12, :cond_7

    .line 393
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v12}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 394
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 396
    :cond_7
    new-instance v12, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    invoke-direct {v12, v4}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/Throwable;)V

    throw v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v12

    if-eqz v8, :cond_8

    .line 402
    invoke-virtual {v8}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 405
    :cond_8
    if-eqz v1, :cond_9

    .line 406
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 407
    sget-object v13, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v14, "verifyLuceneDB: Auto Rebuild  enabled, context:"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_9
    throw v12

    .line 412
    .end local v5    # "ic_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/index/IndexCommit;>;"
    .end local v6    # "ic_point":I
    .end local v8    # "ir":Lorg/apache/lucene/index/IndexReader;
    .end local v9    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/IndexCommit;>;"
    :cond_a
    sget-object v12, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "verifyLuceneDB OK --"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 414
    return-void
.end method


# virtual methods
.method public close(Z)V
    .locals 7
    .param p1, "waitForMerges"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 423
    sget-object v2, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v3, "Call to close index writer"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 424
    monitor-enter p0

    .line 425
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    const-string v3, "indexWriter is NULL, open() need to be invoked before close())"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    const-string v3, "indexDirectory is NULL, open() need to be invoked before close())"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    iget-boolean v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z

    const-string v3, " Indexer must be opened"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    const/4 v1, 0x0

    .line 438
    .local v1, "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    :try_start_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/IndexWriter;->close(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 443
    :goto_0
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 446
    :try_start_3
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 451
    :goto_1
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    .line 452
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z

    .line 453
    if-eqz v1, :cond_0

    .line 454
    throw v1

    .line 456
    .end local v1    # "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 439
    .restart local v1    # "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    :catch_0
    move-exception v0

    .line 440
    .local v0, "e1":Ljava/io/IOException;
    :try_start_5
    sget-object v2, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v3, "IndexWriter Exception:"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 441
    new-instance v1, Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;

    .end local v1    # "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    invoke-direct {v1, v0}, Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;-><init>(Ljava/lang/Throwable;)V

    .restart local v1    # "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    goto :goto_0

    .line 447
    .end local v0    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 448
    .restart local v0    # "e1":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v3, "TaxoWriter Exception:"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    new-instance v1, Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;

    .end local v1    # "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    invoke-direct {v1, v0}, Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;-><init>(Ljava/lang/Throwable;)V

    .restart local v1    # "exception":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    goto :goto_1

    .line 456
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_0
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 458
    return-void
.end method

.method public commit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 496
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 497
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->commit()V

    .line 498
    return-void
.end method

.method public getAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mFieldAnalyzers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public declared-synchronized getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;
    .locals 1

    .prologue
    .line 485
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getIndexWriter()Lorg/apache/lucene/index/IndexWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;,
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
        }
    .end annotation

    .prologue
    .line 466
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z

    if-nez v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->open()V

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTaxoReader()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;-><init>(Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTaxonomyWriter()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;,
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
        }
    .end annotation

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z

    if-nez v0, :cond_0

    .line 477
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->open()V

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isOpen()Z
    .locals 1

    .prologue
    .line 491
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIsOpen:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public open()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;,
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 506
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    if-nez v1, :cond_3

    new-instance v1, Lorg/apache/lucene/store/RAMDirectory;

    invoke-direct {v1}, Lorg/apache/lucene/store/RAMDirectory;-><init>()V

    :goto_0
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDirectory:Lorg/apache/lucene/store/Directory;

    .line 509
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    if-nez v1, :cond_4

    new-instance v1, Lorg/apache/lucene/store/RAMDirectory;

    invoke-direct {v1}, Lorg/apache/lucene/store/RAMDirectory;-><init>()V

    :goto_1
    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxonomyDirectory:Lorg/apache/lucene/store/Directory;

    .line 512
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodec:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 513
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodec:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/IndexWriterConfig;->setCodec(Lorg/apache/lucene/codecs/Codec;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 516
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mOut:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 517
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mOut:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/IndexWriterConfig;->setInfoStream(Ljava/io/PrintStream;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 520
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mDCMIndexDeletionPolicy:Lcom/samsung/dcm/framework/lucene/DCMIndexDeletionPolicy;

    if-eqz v1, :cond_2

    .line 521
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mDCMIndexDeletionPolicy:Lcom/samsung/dcm/framework/lucene/DCMIndexDeletionPolicy;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 524
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;->setMaxBufferedDocs(I)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 526
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    const-wide/high16 v2, 0x4030000000000000L    # 16.0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/IndexWriterConfig;->setRAMBufferSizeMB(D)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 527
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v2, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 529
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->initIndexWriter()V
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 540
    :goto_2
    return-void

    .line 506
    :cond_3
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    invoke-static {v1}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v1

    goto :goto_0

    .line 509
    :cond_4
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    invoke-static {v1}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v1

    goto :goto_1

    .line 530
    :catch_0
    move-exception v0

    .line 531
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
    sget-object v1, Lcom/samsung/dcm/framework/lucene/WriterFactory;->TAG:Ljava/lang/String;

    const-string v2, "Taxo writer in append mode failed"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 532
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v2, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 533
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 536
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 537
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->doTryDeleteFiles(Ljava/io/File;)V

    .line 538
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->initIndexWriter()V

    goto :goto_2
.end method

.method public rollback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 543
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->rollback()V

    .line 546
    :cond_0
    return-void
.end method

.method public declared-synchronized setCodec(Lorg/apache/lucene/codecs/Codec;)V
    .locals 1
    .param p1, "codec"    # Lorg/apache/lucene/codecs/Codec;

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    const-string v0, "Codec cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mCodec:Lcom/google/common/base/Optional;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setIndexDir(Ljava/io/File;)V
    .locals 1
    .param p1, "indexDir"    # Ljava/io/File;

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    const-string v0, "Index directory cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iput-object p1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mIndexDir:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTaxoDir(Ljava/io/File;)V
    .locals 1
    .param p1, "taxoDir"    # Ljava/io/File;

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    const-string v0, "Taxonomy directory cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iput-object p1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mTaxoDir:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    monitor-exit p0

    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setVerbose(Ljava/io/PrintStream;)V
    .locals 1
    .param p1, "os"    # Ljava/io/PrintStream;

    .prologue
    .line 143
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 144
    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mOut:Lcom/google/common/base/Optional;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :goto_0
    monitor-exit p0

    return-void

    .line 146
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mOut:Lcom/google/common/base/Optional;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setWriterConfig(Lorg/apache/lucene/index/IndexWriterConfig;)V
    .locals 1
    .param p1, "writerConfig"    # Lorg/apache/lucene/index/IndexWriterConfig;

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/dcm/framework/lucene/WriterFactory;->mWriterConfig:Lorg/apache/lucene/index/IndexWriterConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
