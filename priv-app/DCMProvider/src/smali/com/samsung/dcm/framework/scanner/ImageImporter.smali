.class public Lcom/samsung/dcm/framework/scanner/ImageImporter;
.super Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;
.source "ImageImporter.java"


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field private static final CONTENT_TYPE:Ljava/lang/String; = "external"

.field private static final IMAGES_PROJECTION:[Ljava/lang/String;

.field public static final MY_PROFILE:Ljava/lang/String; = "profile"

.field private static final NO_BUCKET_ID:I = 0x7

.field private static final NO_DATA:I = 0x1

.field private static final NO_DATE_TAKEN:I = 0x4

.field private static final NO_FACE_COUNT:I = 0x2

.field private static final NO_ID:I = 0x0

.field private static final NO_LATITUDE:I = 0x5

.field private static final NO_LONGITUDE:I = 0x6

.field private static final NO_MIME_TYPE:I = 0x3

.field private static final NO_STORAGE_ID:I = 0x8

.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->BASE_URI:Landroid/net/Uri;

    .line 67
    const-class v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->TAG:Ljava/lang/String;

    .line 69
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "face_count"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "storage_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->IMAGES_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;-><init>(Landroid/content/ContentResolver;)V

    .line 79
    return-void
.end method

.method private extractDataFromCurrentPos(Landroid/database/Cursor;)Lcom/samsung/dcm/documents/ImageMediaDoc;
    .locals 22
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 127
    new-instance v6, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-direct {v6}, Lcom/samsung/dcm/documents/ImageMediaDoc;-><init>()V

    .line 128
    .local v6, "doc":Lcom/samsung/dcm/documents/ImageMediaDoc;
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 129
    .local v9, "id":I
    sget-object v19, Lcom/samsung/dcm/framework/scanner/ImageImporter;->BASE_URI:Landroid/net/Uri;

    int-to-long v0, v9

    move-wide/from16 v20, v0

    invoke-static/range {v19 .. v21}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v18

    .line 130
    .local v18, "uri":Landroid/net/Uri;
    invoke-virtual {v6, v9}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setId(I)V

    .line 131
    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setUri(Landroid/net/Uri;)V

    .line 132
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_0

    .line 133
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 134
    .local v16, "path":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setPath(Ljava/lang/String;)V

    .line 135
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setTitle(Ljava/lang/String;)V

    .line 138
    .end local v8    # "file":Ljava/io/File;
    .end local v16    # "path":Ljava/lang/String;
    :cond_0
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_1

    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_1

    .line 140
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    .line 141
    .local v10, "latitude":D
    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    .line 142
    .local v14, "longitude":D
    new-instance v12, Landroid/location/Location;

    const-string v19, ""

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 143
    .local v12, "location":Landroid/location/Location;
    invoke-virtual {v12, v10, v11}, Landroid/location/Location;->setLatitude(D)V

    .line 144
    invoke-virtual {v12, v14, v15}, Landroid/location/Location;->setLongitude(D)V

    .line 145
    invoke-virtual {v6, v12}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setLocation(Landroid/location/Location;)V

    .line 147
    .end local v10    # "latitude":D
    .end local v12    # "location":Landroid/location/Location;
    .end local v14    # "longitude":D
    :cond_1
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_2

    .line 148
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 149
    .local v7, "faceCount":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v7, v0, :cond_2

    .line 150
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setFaceCount(Ljava/lang/Integer;)V

    .line 153
    .end local v7    # "faceCount":I
    :cond_2
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_3

    .line 154
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 155
    .local v13, "mimeType":Ljava/lang/String;
    invoke-virtual {v6, v13}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setMimeType(Ljava/lang/String;)V

    .line 158
    .end local v13    # "mimeType":Ljava/lang/String;
    :cond_3
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_4

    .line 159
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 161
    .local v4, "date":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setCreationDate(Ljava/lang/Long;)V

    .line 164
    .end local v4    # "date":J
    :cond_4
    const/16 v19, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_5

    .line 165
    const/16 v19, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 166
    .local v17, "sid":I
    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setStorageId(I)V

    .line 169
    .end local v17    # "sid":I
    :cond_5
    const/16 v19, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_6

    .line 170
    const/16 v19, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 171
    .local v2, "bid":J
    invoke-virtual {v6, v2, v3}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setBucketId(J)V

    .line 173
    .end local v2    # "bid":J
    :cond_6
    return-object v6
.end method


# virtual methods
.method public scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 9
    .param p1, "singleUri"    # Landroid/net/Uri;

    .prologue
    .line 83
    const-string v0, "Uri is null."

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->TAG:Ljava/lang/String;

    const-string v1, "Scanning image file - "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    const/4 v6, 0x0

    .line 86
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 89
    .local v7, "doc":Lcom/samsung/dcm/documents/ImageMediaDoc;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/dcm/framework/scanner/ImageImporter;->IMAGES_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 90
    if-eqz v6, :cond_1

    .line 91
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/scanner/ImageImporter;->extractDataFromCurrentPos(Landroid/database/Cursor;)Lcom/samsung/dcm/documents/ImageMediaDoc;

    move-result-object v7

    .line 94
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_1
    :goto_0
    return-object v7

    .line 96
    :catch_0
    move-exception v8

    .line 97
    .local v8, "e":Ljava/lang/Throwable;
    sget-object v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public scanForFaceCount(Landroid/net/Uri;)I
    .locals 9
    .param p1, "singleUri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 109
    const-string v0, "Uri is null."

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->TAG:Ljava/lang/String;

    const-string v1, "Scanning image file - "

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    const/4 v6, 0x0

    .line 112
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, -0x1

    .line 113
    .local v7, "faceCount":I
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/ImageImporter;->mContentResolver:Landroid/content/ContentResolver;

    new-array v2, v5, [Ljava/lang/String;

    const-string v1, "face_count"

    aput-object v1, v2, v8

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 116
    if-eqz v6, :cond_1

    .line 117
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-interface {v6, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 122
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 124
    :cond_1
    return v7
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
