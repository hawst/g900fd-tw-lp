.class public Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;
.super Lorg/apache/lucene/index/MergeScheduler;
.source "DCMMergeScheduler.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeScheduler;-><init>()V

    .line 46
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    return-void
.end method

.method public declared-synchronized merge(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 6
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;->TAG:Ljava/lang/String;

    const-string v4, "merge ++"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :goto_0
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 58
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v1, :cond_0

    .line 85
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :goto_1
    :try_start_2
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;->TAG:Ljava/lang/String;

    const-string v4, "merge --"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    monitor-exit p0

    return-void

    .line 60
    .restart local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_0
    :try_start_3
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexWriter;->merge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 70
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :catch_0
    move-exception v2

    .line 71
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_4
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;->TAG:Ljava/lang/String;

    const-string v4, "OutOfMemoryError ="

    invoke-static {v3, v4, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 54
    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 73
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMMergeScheduler;->TAG:Ljava/lang/String;

    const-string v4, "Exception ="

    invoke-static {v3, v4, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    instance-of v3, v0, Ljava/io/IOException;

    if-eqz v3, :cond_1

    .line 79
    check-cast v0, Ljava/io/IOException;

    .end local v0    # "e":Ljava/lang/Exception;
    throw v0

    .line 82
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
