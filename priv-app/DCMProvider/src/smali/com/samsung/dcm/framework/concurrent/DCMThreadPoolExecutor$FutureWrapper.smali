.class Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
.super Ljava/lang/Object;
.source "DCMThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FutureWrapper"
.end annotation


# instance fields
.field private final mCallback:Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;

.field private final mFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

.field private mbIsCancelled:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Future;Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)V
    .locals 1
    .param p2, "runnable"    # Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    .param p3, "callback"    # Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;",
            "Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;",
            "Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mbIsCancelled:Z

    .line 96
    iput-object p1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mFuture:Ljava/util/concurrent/Future;

    .line 97
    iput-object p2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    .line 98
    iput-object p3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mCallback:Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;

    .line 99
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Ljava/util/concurrent/Future;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    return-object v0
.end method


# virtual methods
.method cancel(Z)V
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mbIsCancelled:Z

    .line 105
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    invoke-interface {v0}, Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;->finishNow()V

    .line 107
    return-void
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mbIsCancelled:Z

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mFuture:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    return v0
.end method

.method public onCallback(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mCallback:Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mCallback:Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;

    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    invoke-interface {v0, v1, p1}, Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;->afterExecute(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Ljava/lang/Throwable;)V

    .line 121
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    invoke-interface {v0}, Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;->pause()V

    .line 129
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    invoke-interface {v0}, Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;->resume()V

    .line 125
    return-void
.end method
