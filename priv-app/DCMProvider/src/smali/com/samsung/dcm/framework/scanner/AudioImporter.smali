.class public Lcom/samsung/dcm/framework/scanner/AudioImporter;
.super Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;
.source "AudioImporter.java"


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field private static final CONTENT_TYPE:Ljava/lang/String; = "external"

.field private static final MUSIC_PROJECTION:[Ljava/lang/String;

.field private static NO_ALBUM:I = 0x0

.field private static NO_ARTIST:I = 0x0

.field private static NO_BUCKET_ID:I = 0x0

.field private static NO_CREATION_DATE:I = 0x0

.field private static NO_DATA:I = 0x0

.field private static NO_GENRE:I = 0x0

.field private static NO_ID:I = 0x0

.field private static NO_MIME:I = 0x0

.field private static final NO_STORAGE_ID:I = 0xa

.field private static NO_TITLE:I = 0x0

.field private static NO_YEAR:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final UNKNOWN:Ljava/lang/String; = "<unknown>"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    sput v2, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ID:I

    .line 54
    sput v3, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_DATA:I

    .line 55
    sput v4, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ARTIST:I

    .line 56
    sput v5, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ALBUM:I

    .line 57
    sput v6, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_YEAR:I

    .line 58
    const/4 v0, 0x5

    sput v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_TITLE:I

    .line 59
    const/4 v0, 0x6

    sput v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_MIME:I

    .line 60
    const/4 v0, 0x7

    sput v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_GENRE:I

    .line 61
    const/16 v0, 0x8

    sput v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_CREATION_DATE:I

    .line 62
    const/16 v0, 0x9

    sput v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_BUCKET_ID:I

    .line 66
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->BASE_URI:Landroid/net/Uri;

    .line 67
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "artist_id"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string/jumbo v1, "year"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "genre_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "storage_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->MUSIC_PROJECTION:[Ljava/lang/String;

    .line 73
    const-class v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;-><init>(Landroid/content/ContentResolver;)V

    .line 78
    return-void
.end method

.method private extractFromCurrentPosition(Landroid/database/Cursor;)Lcom/samsung/dcm/documents/AudioMediaDoc;
    .locals 20
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 99
    new-instance v5, Lcom/samsung/dcm/documents/AudioMediaDoc;

    invoke-direct {v5}, Lcom/samsung/dcm/documents/AudioMediaDoc;-><init>()V

    .line 102
    .local v5, "doc":Lcom/samsung/dcm/documents/AudioMediaDoc;
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ID:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 103
    .local v7, "id":I
    invoke-virtual {v5, v7}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setId(I)V

    .line 106
    sget-object v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->BASE_URI:Landroid/net/Uri;

    int-to-long v0, v7

    move-wide/from16 v16, v0

    invoke-static/range {v15 .. v17}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    .line 107
    .local v13, "uri":Landroid/net/Uri;
    invoke-virtual {v5, v13}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setUri(Landroid/net/Uri;)V

    .line 109
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_TITLE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_0

    .line 110
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_TITLE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 111
    .local v12, "title":Ljava/lang/String;
    invoke-virtual {v5, v12}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setTitle(Ljava/lang/String;)V

    .line 114
    .end local v12    # "title":Ljava/lang/String;
    :cond_0
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_DATA:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_1

    .line 115
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_DATA:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 116
    .local v9, "path":Ljava/lang/String;
    invoke-virtual {v5, v9}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setPath(Ljava/lang/String;)V

    .line 119
    .end local v9    # "path":Ljava/lang/String;
    :cond_1
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_MIME:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_2

    .line 120
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_MIME:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 121
    .local v8, "mime":Ljava/lang/String;
    invoke-virtual {v5, v8}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setMimeType(Ljava/lang/String;)V

    .line 124
    .end local v8    # "mime":Ljava/lang/String;
    :cond_2
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_CREATION_DATE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_3

    .line 125
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_CREATION_DATE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 127
    .local v4, "creationDate":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Lcom/samsung/commons/Algorithms;->secondsToMilliseconds(J)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 128
    invoke-virtual {v5, v4}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setCreationDate(Ljava/lang/Long;)V

    .line 131
    .end local v4    # "creationDate":Ljava/lang/Long;
    :cond_3
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ARTIST:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_4

    .line 132
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ARTIST:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 133
    .local v6, "iTmp":I
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/dcm/framework/scanner/AudioImporter;->retrieveArtist(I)Ljava/lang/String;

    move-result-object v10

    .line 134
    .local v10, "sTmp":Ljava/lang/String;
    if-eqz v10, :cond_4

    const-string v15, "<unknown>"

    invoke-virtual {v10, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 135
    sget-object v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->TAG:Ljava/lang/String;

    const-string v16, "Artist ID="

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " Artist="

    aput-object v19, v17, v18

    const/16 v18, 0x2

    aput-object v10, v17, v18

    invoke-static/range {v15 .. v17}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    invoke-virtual {v5, v10}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setArtist(Ljava/lang/String;)V

    .line 140
    .end local v6    # "iTmp":I
    .end local v10    # "sTmp":Ljava/lang/String;
    :cond_4
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ALBUM:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_5

    .line 141
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_ALBUM:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 142
    .restart local v6    # "iTmp":I
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/dcm/framework/scanner/AudioImporter;->retrieveAlbum(I)Ljava/lang/String;

    move-result-object v10

    .line 143
    .restart local v10    # "sTmp":Ljava/lang/String;
    if-eqz v10, :cond_5

    const-string v15, "<unknown>"

    invoke-virtual {v10, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_5

    .line 144
    sget-object v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->TAG:Ljava/lang/String;

    const-string v16, "Album ID="

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " Album="

    aput-object v19, v17, v18

    const/16 v18, 0x2

    aput-object v10, v17, v18

    invoke-static/range {v15 .. v17}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    invoke-virtual {v5, v10}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setAlbum(Ljava/lang/String;)V

    .line 149
    .end local v6    # "iTmp":I
    .end local v10    # "sTmp":Ljava/lang/String;
    :cond_5
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_YEAR:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_6

    .line 150
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_YEAR:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 151
    .local v14, "year":I
    invoke-virtual {v5, v14}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setYear(I)V

    .line 154
    .end local v14    # "year":I
    :cond_6
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_GENRE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_7

    .line 155
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_GENRE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 156
    .restart local v8    # "mime":Ljava/lang/String;
    invoke-virtual {v5, v8}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setGenre(Ljava/lang/String;)V

    .line 159
    .end local v8    # "mime":Ljava/lang/String;
    :cond_7
    const/16 v15, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_8

    .line 160
    const/16 v15, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 161
    .local v11, "sid":I
    invoke-virtual {v5, v11}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setStorageId(I)V

    .line 164
    .end local v11    # "sid":I
    :cond_8
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_BUCKET_ID:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_9

    .line 165
    sget v15, Lcom/samsung/dcm/framework/scanner/AudioImporter;->NO_BUCKET_ID:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 166
    .local v2, "bid":J
    invoke-virtual {v5, v2, v3}, Lcom/samsung/dcm/documents/AudioMediaDoc;->setBucketId(J)V

    .line 168
    .end local v2    # "bid":J
    :cond_9
    return-object v5
.end method

.method private retrieveAlbum(I)Ljava/lang/String;
    .locals 8
    .param p1, "albumId"    # I

    .prologue
    const/4 v3, 0x0

    .line 199
    const-string v6, "<unknown>"

    .line 200
    .local v6, "album":Ljava/lang/String;
    sget-object v0, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 202
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "album"

    aput-object v0, v2, v3

    .line 209
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 211
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 212
    if-eqz v7, :cond_0

    .line 213
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 218
    :cond_0
    if-eqz v7, :cond_1

    .line 219
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 222
    :cond_1
    return-object v6

    .line 218
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 219
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private retrieveArtist(I)Ljava/lang/String;
    .locals 8
    .param p1, "artistId"    # I

    .prologue
    const/4 v3, 0x0

    .line 172
    const-string v6, "<unknown>"

    .line 173
    .local v6, "artist":Ljava/lang/String;
    sget-object v0, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 175
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artist"

    aput-object v0, v2, v3

    .line 182
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 184
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 185
    if-eqz v7, :cond_0

    .line 186
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 191
    :cond_0
    if-eqz v7, :cond_1

    .line 192
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 195
    :cond_1
    return-object v6

    .line 191
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 192
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 8
    .param p1, "singleUri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 82
    const-string v0, "uri is null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->TAG:Ljava/lang/String;

    const-string v1, "Scanning audio file - "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    const/4 v6, 0x0

    .line 85
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 87
    .local v7, "doc":Lcom/samsung/dcm/documents/AudioMediaDoc;
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/AudioImporter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/dcm/framework/scanner/AudioImporter;->MUSIC_PROJECTION:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 88
    if-eqz v6, :cond_1

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/scanner/AudioImporter;->extractFromCurrentPosition(Landroid/database/Cursor;)Lcom/samsung/dcm/documents/AudioMediaDoc;

    move-result-object v7

    .line 92
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 95
    :cond_1
    return-object v7
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
