.class public final enum Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;
.super Ljava/lang/Enum;
.source "DCMLinkedHashSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LhsPriority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

.field public static final enum HIGH:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

.field public static final enum LOW:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

.field public static final enum UNKNOWN:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    .line 98
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->LOW:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    .line 99
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->UNKNOWN:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->LOW:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->UNKNOWN:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    return-object v0
.end method
