.class Lcom/samsung/dcm/framework/lucene/DCMIndexDeletionPolicy;
.super Lorg/apache/lucene/index/IndexDeletionPolicy;
.source "DCMIndexDeletionPolicy.java"


# static fields
.field private static final MAX_COMMIT_RETAINED:I = 0x2


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexDeletionPolicy;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommit(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 48
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 49
    .local v2, "size":I
    const-string v3, "DCMIndexDeletionPolicy"

    const-string v4, "onCommit"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .local v1, "point":I
    :goto_0
    if-ltz v1, :cond_0

    .line 52
    const-string v4, "DCMIndexDeletionPolicy"

    const-string v5, "SegmentsFileName"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 56
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    add-int/lit8 v3, v2, -0x2

    if-ge v0, v3, :cond_1

    .line 57
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexCommit;->delete()V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59
    :cond_1
    return-void
.end method

.method public onInit(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    const-string v0, "DCMIndexDeletionPolicy"

    const-string v1, "onInit"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/dcm/framework/lucene/DCMIndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 65
    return-void
.end method
