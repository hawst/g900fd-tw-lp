.class public final enum Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
.super Ljava/lang/Enum;
.source "DCMUtilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/utils/DCMUtilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LuceneDataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

.field public static final enum Default:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

.field public static final enum IntField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

.field public static final enum LongField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

.field public static final enum StoredField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

.field public static final enum StringField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

.field public static final enum TextField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118
    new-instance v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    const-string v1, "TextField"

    const-string v2, "TextField"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->TextField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    .line 119
    new-instance v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    const-string v1, "IntField"

    const-string v2, "IntField"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->IntField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    .line 120
    new-instance v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    const-string v1, "LongField"

    const-string v2, "LongField"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->LongField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    .line 121
    new-instance v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    const-string v1, "StoredField"

    const-string v2, "StoredField"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->StoredField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    .line 122
    new-instance v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    const-string v1, "StringField"

    const-string v2, "StringField"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->StringField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    .line 123
    new-instance v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    const-string v1, "Default"

    const/4 v2, 0x5

    const-string v3, "default"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->Default:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    .line 117
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    sget-object v1, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->TextField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->IntField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->LongField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->StoredField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->StringField:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->Default:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->$VALUES:[Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 128
    iput-object p3, p0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->mName:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public static getDataType(Ljava/lang/String;)Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 136
    const-string v4, "LuceneDataType getDataType"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->values()[Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 138
    .local v3, "ot":Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    iget-object v4, v3, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    .end local v3    # "ot":Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    :goto_1
    return-object v3

    .line 137
    .restart local v3    # "ot":Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    .end local v3    # "ot":Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    :cond_1
    sget-object v3, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->Default:Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 117
    const-class v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->$VALUES:[Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->mName:Ljava/lang/String;

    return-object v0
.end method
