.class Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
.super Ljava/lang/Object;
.source "DatabaseRebuilderTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaDBContainer"
.end annotation


# instance fields
.field private mId:I

.field private mPath:Ljava/lang/String;

.field private mStorageId:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 746
    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mId:I

    .line 747
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mPath:Ljava/lang/String;

    .line 748
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mTitle:Ljava/lang/String;

    .line 750
    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mStorageId:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$1;

    .prologue
    .line 745
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    .prologue
    .line 745
    iget v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mId:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    .param p1, "x1"    # I

    .prologue
    .line 745
    iput p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mId:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    .prologue
    .line 745
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 745
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 745
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;

    .prologue
    .line 745
    iget v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mStorageId:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;
    .param p1, "x1"    # I

    .prologue
    .line 745
    iput p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask$MediaDBContainer;->mStorageId:I

    return p1
.end method
