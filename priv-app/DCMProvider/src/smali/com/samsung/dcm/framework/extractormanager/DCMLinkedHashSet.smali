.class public Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;
.super Ljava/lang/Object;
.source "DCMLinkedHashSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;,
        Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/samsung/dcm/framework/extractormanager/Command;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    .line 23
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    .line 96
    return-void
.end method

.method private deleteFromIterator(Ljava/util/Iterator;I)V
    .locals 2
    .param p2, "storageId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 440
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    const/4 v0, 0x0

    .line 441
    .local v0, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 442
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 443
    .restart local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 444
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBeingProcessed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 445
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    .line 446
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->interrupt()V

    goto :goto_0

    .line 448
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    .line 449
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto :goto_0

    .line 453
    :cond_2
    return-void
.end method

.method static getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;
    .locals 1
    .param p0, "cmd"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 92
    const-string v0, "Command cannot be null"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 407
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->clear()V

    .line 408
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->clear()V

    .line 409
    return-void
.end method

.method public clearCommandsForId(I)V
    .locals 1
    .param p1, "storageId"    # I

    .prologue
    .line 434
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->deleteFromIterator(Ljava/util/Iterator;I)V

    .line 435
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->deleteFromIterator(Ljava/util/Iterator;I)V

    .line 436
    return-void
.end method

.method public contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 342
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    .local p1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 258
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 259
    .local v0, "oldcommand":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->get(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 260
    if-nez v0, :cond_0

    .line 261
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->get(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 263
    :cond_0
    return-object v0
.end method

.method public hasHighPriorityCommand()Z
    .locals 1

    .prologue
    .line 296
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHighPriorityCommand(Z)Z
    .locals 3
    .param p1, "busy"    # Z

    .prologue
    .line 305
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 306
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v2

    if-ne v2, p1, :cond_0

    .line 307
    const/4 v2, 0x1

    .line 310
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public hasLowPriorityCommand()Z
    .locals 1

    .prologue
    .line 317
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLowPriorityCommand(Z)Z
    .locals 3
    .param p1, "busy"    # Z

    .prologue
    .line 326
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 327
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v2

    if-ne v2, p1, :cond_0

    .line 328
    const/4 v2, 0x1

    .line 331
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 349
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public peekLhsPriorityCommand(ZLcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .param p1, "busy"    # Z
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    .prologue
    .line 412
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 413
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    const/4 v1, 0x0

    .line 414
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->LOW:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    invoke-virtual {p2, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 415
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 424
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 425
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 426
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v2

    if-ne v2, p1, :cond_0

    move-object v2, v0

    .line 430
    :goto_1
    return-object v2

    .line 417
    :cond_1
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    invoke-virtual {p2, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 418
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v2, v0

    .line 421
    goto :goto_1

    .line 430
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public put(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    .local p1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, "status":Z
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isHighPriority()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->add(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v0

    .line 116
    :goto_0
    if-nez v0, :cond_0

    .line 117
    const-string v1, "DCMLinkedHashSet"

    const-string v2, "failed to add commmand "

    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, " ID = "

    aput-object v4, v3, v5

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, " URI = "

    aput-object v4, v3, v7

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandCompleted()V

    .line 122
    :cond_0
    const-string v1, "DCMLinkedHashSet"

    const-string v2, " status = "

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    const-string v4, " ID = "

    aput-object v4, v3, v6

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v4

    aput-object v4, v3, v7

    const-string v4, " URI = "

    aput-object v4, v3, v8

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    return-void

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->add(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v0

    goto :goto_0
.end method

.method public remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 274
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    .local p1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    const/4 v0, 0x0

    .line 275
    .local v0, "removed":Z
    if-nez p1, :cond_0

    .line 276
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Class="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-class v3, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " method=remove(T) Null command argument"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->remove(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 281
    const/4 v0, 0x1

    .line 289
    :goto_0
    return v0

    .line 282
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 283
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->remove(Ljava/lang/Object;)Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 284
    const/4 v0, 0x1

    goto :goto_0

    .line 287
    :cond_2
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->TAG:Ljava/lang/String;

    const-string v2, "Falied to removed command for URI =  "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getKeyFromCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 356
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeHigh()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeLow()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public sizeHigh()I
    .locals 1

    .prologue
    .line 385
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public sizeHigh(Z)I
    .locals 4
    .param p1, "busy"    # Z

    .prologue
    .line 394
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v2, 0x0

    .line 395
    .local v2, "size":I
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 396
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v3

    if-ne v3, p1, :cond_0

    .line 397
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 400
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    return v2
.end method

.method public sizeLow()I
    .locals 1

    .prologue
    .line 363
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public sizeLow(Z)I
    .locals 4
    .param p1, "busy"    # Z

    .prologue
    .line 372
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v2, 0x0

    .line 373
    .local v2, "size":I
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 374
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v3

    if-ne v3, p1, :cond_0

    .line 375
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 378
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    return v2
.end method

.method public take()Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 133
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    const/4 v1, 0x0

    .line 135
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 136
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 141
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 143
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 145
    :cond_1
    return-object v0

    .line 137
    :cond_2
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method

.method public take(Z)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 4
    .param p1, "busy"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v2, 0x0

    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    const/4 v1, 0x0

    .line 158
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 159
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 165
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 167
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v3

    if-ne v3, p1, :cond_0

    .line 168
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    move-object v2, v0

    .line 172
    :cond_1
    return-object v2

    .line 160
    :cond_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 161
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method

.method public takeHigh()Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 183
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 184
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 185
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 187
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 189
    :cond_0
    return-object v0
.end method

.method public takeHigh(Z)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .param p1, "busy"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 202
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mHigh:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 203
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 204
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 205
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v2

    if-ne v2, p1, :cond_0

    .line 206
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    move-object v2, v0

    .line 211
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public takeLow()Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 221
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 222
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 223
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 225
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 227
    :cond_0
    return-object v0
.end method

.method public takeLow(Z)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 3
    .param p1, "busy"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "this":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<TT;>;"
    const/4 v0, 0x0

    .line 240
    .local v0, "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->mLow:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$CommandLinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 241
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 243
    .restart local v0    # "element":Lcom/samsung/dcm/framework/extractormanager/Command;, "TT;"
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v2

    if-ne v2, p1, :cond_0

    .line 244
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    move-object v2, v0

    .line 249
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
