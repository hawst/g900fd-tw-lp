.class Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;
.super Ljava/lang/Object;
.source "DCMController.java"

# interfaces
.implements Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeavyTaskStatusListener"
.end annotation


# instance fields
.field private mTask:Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

.field final synthetic this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;


# direct methods
.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;)V
    .locals 1
    .param p2, "task"    # Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;->mTask:Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    .line 226
    return-void
.end method


# virtual methods
.method public onAborted(ILjava/lang/Throwable;)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    const/4 v5, 0x0

    .line 236
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "Heavy worker task "

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string/jumbo v4, "was aborted by exception : "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;->mTask:Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    # invokes: Lcom/samsung/dcm/framework/extractormanager/DCMController;->markTaskAsFinished(Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V
    invoke-static {v0, v1, p2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$900(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V

    .line 238
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "STOPPED TASK !!! : "

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    return-void
.end method

.method public onFinished(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 230
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "Heavy worker task "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, " was finished"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$HeavyTaskStatusListener;->mTask:Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/dcm/framework/extractormanager/DCMController;->markTaskAsFinished(Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V
    invoke-static {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$900(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;Ljava/lang/Throwable;)V

    .line 232
    return-void
.end method
