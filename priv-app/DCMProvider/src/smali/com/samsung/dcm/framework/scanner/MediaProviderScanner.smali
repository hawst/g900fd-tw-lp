.class public Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;
.super Ljava/lang/Object;
.source "MediaProviderScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$1;,
        Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final URI_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAudio:Lcom/samsung/dcm/framework/scanner/AudioImporter;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCount:I

.field private mImage:Lcom/samsung/dcm/framework/scanner/ImageImporter;

.field private mUrimeasurement:J

.field private mVideo:Lcom/samsung/dcm/framework/scanner/VideoImporter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->URI_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mContentResolver:Landroid/content/ContentResolver;

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mUrimeasurement:J

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mCount:I

    .line 36
    return-void
.end method


# virtual methods
.method public scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 7
    .param p1, "singleUri"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "doc":Lcom/samsung/dcm/documents/MediaDoc;
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning uri="

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning AUDIO file from "

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 70
    .local v1, "sUri":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mAudio:Lcom/samsung/dcm/framework/scanner/AudioImporter;

    aget-object v3, v1, v5

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/scanner/AudioImporter;->scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 89
    :goto_0
    if-nez v0, :cond_0

    .line 90
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "File already deleted from SD CARD "

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Specialized scanning finished."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    return-object v0

    .line 72
    .end local v1    # "sUri":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 73
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning VIDEO file from "

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 75
    .restart local v1    # "sUri":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mVideo:Lcom/samsung/dcm/framework/scanner/VideoImporter;

    aget-object v3, v1, v5

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/scanner/VideoImporter;->scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 77
    goto :goto_0

    .end local v1    # "sUri":[Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning IMAGE file from "

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 82
    .restart local v1    # "sUri":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mImage:Lcom/samsung/dcm/framework/scanner/ImageImporter;

    aget-object v3, v1, v5

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/scanner/ImageImporter;->scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 84
    goto :goto_0

    .line 85
    .end local v1    # "sUri":[Ljava/lang/String;
    :cond_3
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Unknown media type or corrupted uri."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
.end method

.method public scan(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 8
    .param p1, "dcmId"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "doc":Lcom/samsung/dcm/documents/MediaDoc;
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning: "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v1

    .line 108
    .local v1, "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$1;->$SwitchMap$com$samsung$dcm$documents$MediaDoc$DocType:[I

    invoke-virtual {v1}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 125
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Unknown media type "

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown media type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", document not scanned: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 110
    :pswitch_0
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning IMAGE file from "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object v2, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mImage:Lcom/samsung/dcm/framework/scanner/ImageImporter;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/scanner/ImageImporter;->scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 115
    :pswitch_1
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning AUDIO file from "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v2, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mAudio:Lcom/samsung/dcm/framework/scanner/AudioImporter;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/scanner/AudioImporter;->scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 117
    goto :goto_0

    .line 120
    :pswitch_2
    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v3, "Scanning VIDEO file from "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    iget-object v2, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mVideo:Lcom/samsung/dcm/framework/scanner/VideoImporter;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/scanner/VideoImporter;->scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 122
    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public scanForFaceCount(Ljava/lang/String;Lcom/samsung/dcm/documents/MediaDoc$DocType;)I
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "docType"    # Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 140
    const/4 v0, -0x1

    .line 141
    .local v0, "faceCount":I
    sget-object v1, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v2, "Scanning for facecount : "

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    sget-object v1, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$1;->$SwitchMap$com$samsung$dcm$documents$MediaDoc$DocType:[I

    invoke-virtual {p2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 149
    sget-object v1, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v2, "Not supported  media type "

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not supported  media type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", document not scanned: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :pswitch_0
    sget-object v1, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v2, "Scanning for facecount IMAGE file from "

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    iget-object v1, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mImage:Lcom/samsung/dcm/framework/scanner/ImageImporter;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/scanner/ImageImporter;->scanForFaceCount(Landroid/net/Uri;)I

    move-result v0

    .line 153
    return v0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setContentResolver(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mContentResolver:Landroid/content/ContentResolver;

    .line 57
    new-instance v0, Lcom/samsung/dcm/framework/scanner/ImageImporter;

    invoke-direct {v0, p1}, Lcom/samsung/dcm/framework/scanner/ImageImporter;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mImage:Lcom/samsung/dcm/framework/scanner/ImageImporter;

    .line 58
    new-instance v0, Lcom/samsung/dcm/framework/scanner/AudioImporter;

    invoke-direct {v0, p1}, Lcom/samsung/dcm/framework/scanner/AudioImporter;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mAudio:Lcom/samsung/dcm/framework/scanner/AudioImporter;

    .line 59
    new-instance v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;

    invoke-direct {v0, p1}, Lcom/samsung/dcm/framework/scanner/VideoImporter;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mVideo:Lcom/samsung/dcm/framework/scanner/VideoImporter;

    .line 60
    return-void
.end method

.method public validuri(Landroid/net/Uri;)Z
    .locals 14
    .param p1, "singleuri"    # Landroid/net/Uri;

    .prologue
    .line 157
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 158
    .local v12, "startQuery":J
    iget v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mCount:I

    .line 159
    const/4 v10, 0x0

    .line 160
    .local v10, "result":Z
    const/4 v6, 0x0

    .line 162
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->URI_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 163
    if-eqz v6, :cond_0

    .line 164
    const/4 v10, 0x1

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    const/4 v6, 0x0

    .line 173
    :cond_0
    if-nez v10, :cond_1

    .line 174
    sget-object v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, " = "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v8, v0, v12

    .line 177
    .local v8, "endQuery":J
    iget-wide v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mUrimeasurement:J

    add-long/2addr v0, v8

    iput-wide v0, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mUrimeasurement:J

    .line 178
    sget-object v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    const-string v1, "mUrimeasurement  ="

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mUrimeasurement:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "ms  mCount = "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->mCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v11, v10

    .line 179
    .end local v8    # "endQuery":J
    .end local v10    # "result":Z
    .local v11, "result":Z
    :goto_0
    return v11

    .line 168
    .end local v11    # "result":Z
    .restart local v10    # "result":Z
    :catch_0
    move-exception v7

    .line 169
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v11, v10

    .line 170
    .end local v10    # "result":Z
    .restart local v11    # "result":Z
    goto :goto_0
.end method
