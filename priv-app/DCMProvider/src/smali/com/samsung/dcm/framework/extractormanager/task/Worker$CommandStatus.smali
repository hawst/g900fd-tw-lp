.class final enum Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;
.super Ljava/lang/Enum;
.source "Worker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/task/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CommandStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

.field public static final enum ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

.field public static final enum COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    const-string v1, "ABORTED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    .line 87
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    return-object v0
.end method
