.class public final enum Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidOrigins"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

.field public static final enum CalendarProvider2:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

.field public static final enum DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

.field public static final enum FaceScanner:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

.field public static final enum MediaProvider:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 228
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    const-string v1, "CalendarProvider2"

    const-string v2, "com.android.providers.calendar.CalendarProvider2"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->CalendarProvider2:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    .line 229
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    const-string v1, "MediaProvider"

    const-string v2, "com.android.providers.media.MediaProvider"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->MediaProvider:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    .line 230
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    const-string v1, "FaceScanner"

    const-string v2, "com.android.providers.media.FaceScanner"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->FaceScanner:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    .line 231
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    const-string v1, "DcmController"

    const-class v2, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    .line 227
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->CalendarProvider2:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->MediaProvider:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->FaceScanner:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 236
    iput-object p3, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->mName:Ljava/lang/String;

    .line 237
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 244
    const-string v4, "ValidOrigins fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->values()[Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    move-result-object v1

    .local v1, "arr$":[Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 246
    .local v0, "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    iget-object v4, v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 250
    .end local v0    # "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    :goto_1
    return-object v0

    .line 245
    .restart local v0    # "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 250
    .end local v0    # "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 227
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;
    .locals 1

    .prologue
    .line 227
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->mName:Ljava/lang/String;

    return-object v0
.end method
