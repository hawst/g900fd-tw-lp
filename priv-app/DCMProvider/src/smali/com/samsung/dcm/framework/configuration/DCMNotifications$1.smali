.class final Lcom/samsung/dcm/framework/configuration/DCMNotifications$1;
.super Ljava/lang/Object;
.source "DCMNotifications.java"

# interfaces
.implements Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/configuration/DCMNotifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 5

    .prologue
    .line 98
    # getter for: Lcom/samsung/dcm/framework/configuration/DCMNotifications;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->access$000()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    # getter for: Lcom/samsung/dcm/framework/configuration/DCMNotifications;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->access$000()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateNotificationUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 101
    const-string v0, "DCMNotifications"

    const-string v1, "Resolver notified : OP:"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "UPDATE"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    :cond_0
    return-void
.end method
