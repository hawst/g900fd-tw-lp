.class public Lcom/samsung/dcm/framework/configuration/DCMConfig;
.super Ljava/lang/Object;
.source "DCMConfig.java"


# static fields
.field private static final MAX_INIT_AWAIT:J = 0x5L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field protected mIndexDir:Ljava/io/File;

.field private mNoOfCPUCore:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

.field protected mTaxoDir:Ljava/io/File;

.field mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

.field private volatile mbConfigCreationSucess:Z

.field private mbSDCardMounted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    .line 63
    iput-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 67
    iput-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTaxoDir:Ljava/io/File;

    .line 69
    iput-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mIndexDir:Ljava/io/File;

    .line 70
    iput-boolean v2, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbSDCardMounted:Z

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    .line 89
    const-string v0, "Context must be defined"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    .line 90
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    const-string v1, "taxonomyDirectory"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTaxoDir:Ljava/io/File;

    .line 91
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    const-string v1, "indexDirectory"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mIndexDir:Ljava/io/File;

    .line 93
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->initializeTransport()V

    .line 94
    iput-boolean v2, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    .line 95
    return-void
.end method

.method private configure(Lcom/samsung/dcm/framework/configuration/DcmTransport;)V
    .locals 6
    .param p1, "transport"    # Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .prologue
    .line 212
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->getNoOfCPUCoresEx()I

    move-result v0

    .line 215
    .local v0, "no_of_cpucore":I
    if-lez v0, :cond_0

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    .line 216
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    .line 222
    :goto_0
    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v2, "No of CPU Core"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    invoke-virtual {v5}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    return-void

    .line 220
    :cond_0
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    goto :goto_0
.end method

.method private initializeTransport()V
    .locals 22

    .prologue
    .line 102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 103
    .local v14, "starttime":J
    new-instance v13, Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-direct {v13}, Lcom/samsung/dcm/framework/configuration/DcmTransport;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 105
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->initialize(Landroid/content/Context;)Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    move-result-object v12

    .line 106
    .local v12, "receiver":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v13, v12}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setIntentReceiver(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V

    .line 107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 108
    .local v4, "endtime":J
    sget-object v13, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v16, "initializeTransport.SystemBroadcastReceiver "

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    sub-long v20, v4, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " ms"

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-static {v13, v12}, Lcom/samsung/dcm/framework/configuration/StorageController;->initialize(Landroid/content/Context;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V

    .line 113
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v6

    .line 116
    .local v6, "extractorCtrl":Lcom/samsung/dcm/framework/configuration/ExtractorsController;
    new-instance v2, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;

    invoke-direct {v2}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;-><init>()V

    .line 117
    .local v2, "calendarExtractor":Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->setContextResover(Landroid/content/ContentResolver;)V

    .line 119
    new-instance v7, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    invoke-direct {v7}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;-><init>()V

    .line 120
    .local v7, "namedPlaceExtractor":Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v13}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->setContext(Landroid/content/Context;)V

    .line 122
    new-instance v10, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;

    invoke-direct {v10}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;-><init>()V

    .line 123
    .local v10, "personNameExtractor":Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-virtual {v10, v13}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->setContextResolver(Landroid/content/ContentResolver;)V

    .line 125
    const-class v13, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v6, v13, v7}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->registerExtractorForClass(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z

    .line 128
    const-class v13, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v6, v13, v2}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->registerExtractorForClass(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z

    .line 131
    const-class v13, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v6, v13, v10}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->registerExtractorForClass(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z

    .line 135
    new-instance v9, Lcom/samsung/dcm/framework/extractors/OscExtractor;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v13

    invoke-direct {v9, v13}, Lcom/samsung/dcm/framework/extractors/OscExtractor;-><init>(Landroid/content/res/AssetManager;)V

    .line 136
    .local v9, "oscExtractor":Lcom/samsung/dcm/framework/extractors/OscExtractor;
    const-class v13, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v6, v13, v9}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->registerLateExtractorForClass(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z

    .line 139
    new-instance v11, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-direct {v11, v13, v12}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;-><init>(Landroid/content/Context;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V

    .line 140
    .local v11, "policymanager":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v13, v11}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setDCMPolicyManager(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;)V

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 142
    sget-object v13, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v16, "initializeTransport.DCMPolicyManager "

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    sub-long v20, v4, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " ms"

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    new-instance v3, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    invoke-direct {v3, v13, v11, v12}, Lcom/samsung/dcm/framework/extractormanager/DCMController;-><init>(Landroid/content/Context;Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V

    .line 146
    .local v3, "controller":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v13, v3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setDCMController(Lcom/samsung/dcm/framework/extractormanager/DCMController;)V

    .line 147
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 148
    sget-object v13, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v16, "initializeTransport.MediaManager "

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    sub-long v20, v4, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " ms"

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    new-instance v8, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mIndexDir:Ljava/io/File;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTaxoDir:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v8, v13, v0, v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)V

    .line 152
    .local v8, "nrtmanager":Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v13, v8}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setNRTManager(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;)V

    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 154
    sget-object v13, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v16, "initializeTransport.DCMNRTManager "

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    sub-long v20, v4, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " ms"

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->configure(Lcom/samsung/dcm/framework/configuration/DcmTransport;)V

    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 159
    sget-object v13, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v16, "initializeTransport took "

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    sub-long v20, v4, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " ms"

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    return-void
.end method

.method public static isVAEnabled()Z
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x1

    return v0
.end method

.method private waitForSuccessLatch()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 284
    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "waitForSuccessLatch "

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v2, :cond_0

    .line 287
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    .line 288
    .local v1, "retVal":Z
    if-nez v1, :cond_0

    .line 289
    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "waiting time elapsed before the count reached zero"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    .end local v1    # "retVal":Z
    :cond_0
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getIndexDir()Ljava/io/File;
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mIndexDir:Ljava/io/File;

    const-string v1, "DCMConfig index dir is null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mIndexDir:Ljava/io/File;

    return-object v0
.end method

.method public getNoOfCPUCore()I
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 239
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public getSDCardMountStatus()Z
    .locals 1

    .prologue
    .line 253
    monitor-enter p0

    .line 254
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbSDCardMounted:Z

    monitor-exit p0

    return v0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSuccessState()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 268
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v1, "getSuccessState ="

    new-array v2, v5, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 270
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v1, "mSuccessLatch not null, waiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->waitForSuccessLatch()V

    .line 272
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    .line 280
    :goto_0
    return v0

    .line 275
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    if-nez v0, :cond_1

    .line 276
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    .line 277
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->waitForSuccessLatch()V

    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    .line 280
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    goto :goto_0
.end method

.method public getTaxoDir()Ljava/io/File;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTaxoDir:Ljava/io/File;

    const-string v1, "DCMConfig taxonomy dir is null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTaxoDir:Ljava/io/File;

    return-object v0
.end method

.method public getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    return-object v0
.end method

.method public onConfigDestroyed()V
    .locals 4

    .prologue
    .line 226
    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v2, "onConfigDestroyed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v0

    .line 228
    .local v0, "nrtManager":Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    if-eqz v0, :cond_0

    .line 229
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->close()V

    .line 231
    :cond_0
    return-void
.end method

.method public setIndexDir(Ljava/io/File;)V
    .locals 0
    .param p1, "indexDir"    # Ljava/io/File;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mIndexDir:Ljava/io/File;

    .line 178
    return-void
.end method

.method public setNoOfCPUCore(I)V
    .locals 1
    .param p1, "no_of_core"    # I

    .prologue
    .line 243
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mNoOfCPUCore:Lcom/google/common/base/Optional;

    .line 244
    return-void
.end method

.method public setSDCardMountStatus(Z)V
    .locals 1
    .param p1, "mountStatus"    # Z

    .prologue
    .line 247
    monitor-enter p0

    .line 248
    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbSDCardMounted:Z

    .line 249
    monitor-exit p0

    .line 250
    return-void

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSuccessState(Z)V
    .locals 5
    .param p1, "status"    # Z

    .prologue
    const/4 v4, 0x0

    .line 259
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v1, "setSuccessState = "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mbConfigCreationSucess:Z

    .line 261
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mSuccessLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 263
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->TAG:Ljava/lang/String;

    const-string v1, "setSuccessState latch is down"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    :cond_0
    return-void
.end method

.method public setTaxoDir(Ljava/io/File;)V
    .locals 0
    .param p1, "taxoDir"    # Ljava/io/File;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTaxoDir:Ljava/io/File;

    .line 183
    return-void
.end method

.method public setTransport(Lcom/samsung/dcm/framework/configuration/DcmTransport;)V
    .locals 0
    .param p1, "transport"    # Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/DCMConfig;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 188
    return-void
.end method
