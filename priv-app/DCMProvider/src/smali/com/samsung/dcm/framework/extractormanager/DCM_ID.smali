.class public Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
.super Ljava/lang/Object;
.source "DCM_ID.java"


# instance fields
.field private final mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field private final mPath:Ljava/lang/String;

.field private final mUri:Landroid/net/Uri;

.field private final mUriWithType:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "uriString"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uriString"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-string v1, "uri is null"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUriWithType:Landroid/net/Uri;

    .line 67
    const/16 v1, 0x3b

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 69
    .local v0, "semicolonPos":I
    const/4 v1, -0x1

    if-eq v1, v0, :cond_0

    .line 70
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUri:Landroid/net/Uri;

    .line 71
    invoke-static {p1}, Lcom/samsung/commons/Algorithms;->getDocTypeByUri(Ljava/lang/String;)Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 78
    :goto_0
    const-string v1, "path is null"

    invoke-static {p2, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mPath:Ljava/lang/String;

    .line 79
    return-void

    .line 74
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUri:Landroid/net/Uri;

    .line 75
    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 115
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    move-object v0, p1

    .line 116
    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .line 117
    .local v0, "o":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 119
    .end local v0    # "o":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getUriWithType()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUriWithType:Landroid/net/Uri;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v0, "s":Ljava/lang/StringBuilder;
    const-string v1, "DCMID ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    const-string v1, "uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 128
    const-string v1, ", path=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
