.class public Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;
.super Lcom/samsung/dcm/framework/extractors/Extractor;
.source "CalendarEventExtractor.java"


# static fields
.field public static final CALENDAR_PROJECTION:[Ljava/lang/String;

.field static final CALENDAR_SELECTION:Ljava/lang/String; = "dtstart < ?  AND dtend > ?  AND deleted==0"

.field private static final MAX_EVENTS_SUPPORTED:I = 0x19

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mContextResover:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const-class v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->CALENDAR_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractors/Extractor;-><init>()V

    return-void
.end method

.method private process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 10
    .param p1, "doc"    # Lcom/samsung/dcm/documents/ImageMediaDoc;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 71
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->mContextResover:Landroid/content/ContentResolver;

    const-string v3, "Context resolver cannot be null"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    sget-object v2, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v3, "Begin image processing..."

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    sget-object v3, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v4, "Creation date="

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/commons/Algorithms;->formatTime(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->retrieveEventsFromCalendar(J)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setEvents(Ljava/util/List;)V

    .line 76
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getEvents()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/documents/EventData;

    .line 77
    .local v0, "e":Lcom/samsung/dcm/documents/EventData;
    sget-object v2, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v3, "Adding category:"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/EventData;->getCategories()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/EventData;->getCategories()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/dcm/documents/ImageMediaDoc;->addCategory(Ljava/lang/String;)V

    goto :goto_0

    .line 80
    .end local v0    # "e":Lcom/samsung/dcm/documents/EventData;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v3, "End image processing"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object p1
.end method

.method private process_video(Lcom/samsung/dcm/documents/VideoMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 7
    .param p1, "doc"    # Lcom/samsung/dcm/documents/VideoMediaDoc;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 86
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->mContextResover:Landroid/content/ContentResolver;

    const-string v1, "Context resolver cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v1, "Begin video processing..."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    sget-object v1, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v2, "Creation date="

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/commons/Algorithms;->formatTime(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getCreationDate()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->retrieveEventsFromCalendar(J)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setEvents(Ljava/util/List;)V

    .line 91
    sget-object v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v1, "End video processing"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    :cond_0
    return-object p1
.end method

.method private retrieveEventsFromCalendar(J)Ljava/util/ArrayList;
    .locals 13
    .param p1, "dateTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 97
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v9, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/documents/EventData;>;"
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 99
    .local v6, "creationDate":Ljava/lang/String;
    new-array v4, v12, [Ljava/lang/String;

    aput-object v6, v4, v10

    aput-object v6, v4, v11

    .line 103
    .local v4, "args":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 105
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->mContextResover:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->CALENDAR_PROJECTION:[Ljava/lang/String;

    const-string v3, "dtstart < ?  AND dtend > ?  AND deleted==0"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 107
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 108
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    new-instance v8, Lcom/samsung/dcm/documents/EventData;

    invoke-direct {v8}, Lcom/samsung/dcm/documents/EventData;-><init>()V

    .line 110
    .local v8, "event":Lcom/samsung/dcm/documents/EventData;
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/samsung/dcm/documents/EventData;->setId(Ljava/lang/Long;)V

    .line 111
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Lcom/samsung/dcm/documents/EventData;->getId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/samsung/dcm/documents/EventData;->setUri(Landroid/net/Uri;)V

    .line 113
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/samsung/dcm/documents/EventData;->setTitle(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    .line 117
    sget-object v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v1, " Beaking out"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    .end local v8    # "event":Lcom/samsung/dcm/documents/EventData;
    :cond_1
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 124
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 127
    :cond_2
    sget-object v0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->TAG:Ljava/lang/String;

    const-string v1, "Number of found events = ["

    new-array v2, v12, [Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    const-string v3, "]"

    aput-object v3, v2, v11

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    return-object v9

    .line 123
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 124
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method public hashCode()I
    .locals 2

    .prologue
    .line 173
    const/16 v0, 0xd

    .line 174
    .local v0, "prime":I
    const/16 v1, 0xd

    return v1
.end method

.method public initialize()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method protected initializeSupportedClasses()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->mSupportedExtractors:Ljava/util/LinkedList;

    const-class v1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->mSupportedExtractors:Ljava/util/LinkedList;

    const-class v1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 133
    instance-of v12, p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    if-eqz v12, :cond_1

    .line 134
    check-cast p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    .line 158
    :cond_0
    :goto_0
    return-object v0

    .line 136
    .restart local p1    # "data":Ljava/lang/Object;
    :cond_1
    instance-of v12, p1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    if-eqz v12, :cond_2

    .line 137
    check-cast p1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->process_video(Lcom/samsung/dcm/documents/VideoMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v0

    goto :goto_0

    .line 138
    .restart local p1    # "data":Ljava/lang/Object;
    :cond_2
    instance-of v12, p1, Lorg/apache/lucene/document/Document;

    if-eqz v12, :cond_4

    move-object v9, p1

    .line 139
    check-cast v9, Lorg/apache/lucene/document/Document;

    .line 140
    .local v9, "lucenedoc":Lorg/apache/lucene/document/Document;
    const/4 v8, 0x0

    .line 141
    .local v8, "iF":Lorg/apache/lucene/index/IndexableField;
    const-string v12, "date"

    invoke-virtual {v9, v12}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v8

    .line 142
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 143
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v8, :cond_0

    .line 145
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    .line 146
    .local v2, "creationDate":J
    invoke-direct {p0, v2, v3}, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->retrieveEventsFromCalendar(J)Ljava/util/ArrayList;

    move-result-object v5

    .line 147
    .local v5, "eventDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/documents/EventData;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v6, "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/documents/EventData;

    .line 149
    .local v1, "e":Lcom/samsung/dcm/documents/EventData;
    invoke-virtual {v1}, Lcom/samsung/dcm/documents/EventData;->getTitle()Ljava/lang/String;

    move-result-object v10

    .line 150
    .local v10, "title":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/samsung/dcm/documents/EventData;->getUri()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 151
    .local v11, "uri":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "event":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 154
    .end local v1    # "e":Lcom/samsung/dcm/documents/EventData;
    .end local v4    # "event":Ljava/lang/String;
    .end local v10    # "title":Ljava/lang/String;
    .end local v11    # "uri":Ljava/lang/String;
    :cond_3
    const-string v12, "EVENT_LIST"

    invoke-virtual {v0, v12, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 158
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "creationDate":J
    .end local v5    # "eventDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/documents/EventData;>;"
    .end local v6    # "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "iF":Lorg/apache/lucene/index/IndexableField;
    .end local v9    # "lucenedoc":Lorg/apache/lucene/document/Document;
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public setContextResover(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "contextResover"    # Landroid/content/ContentResolver;

    .prologue
    .line 50
    const-string v0, "Context resolver cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;->mContextResover:Landroid/content/ContentResolver;

    .line 52
    return-void
.end method
