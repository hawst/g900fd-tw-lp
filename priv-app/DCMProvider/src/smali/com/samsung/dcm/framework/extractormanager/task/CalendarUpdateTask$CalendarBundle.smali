.class Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;
.super Ljava/lang/Object;
.source "CalendarUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CalendarBundle"
.end annotation


# instance fields
.field private bundle:Landroid/os/Bundle;

.field private final startId:I


# direct methods
.method public constructor <init>(Landroid/os/Bundle;I)V
    .locals 1
    .param p1, "mBundle"    # Landroid/os/Bundle;
    .param p2, "startId"    # I

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->bundle:Landroid/os/Bundle;

    .line 132
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->bundle:Landroid/os/Bundle;

    .line 133
    iput p2, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;

    .prologue
    .line 124
    iget v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I

    return v0
.end method


# virtual methods
.method getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->bundle:Landroid/os/Bundle;

    return-object v0
.end method
