.class Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;
.super Ljava/lang/Object;
.source "DCMThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DoubleKeyMap"
.end annotation


# instance fields
.field private final mFutureMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final mRunnableMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    .line 136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method add(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)V
    .locals 2
    .param p1, "futureWrapper"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mFuture:Ljava/util/concurrent/Future;
    invoke-static {p1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->access$100(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    invoke-static {p1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->access$200(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    return-void
.end method

.method clear()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 174
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 175
    return-void
.end method

.method public containsKey(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z
    .locals 1
    .param p1, "runnable"    # Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsKey(Ljava/util/concurrent/Future;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "ff":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method get(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    .locals 1
    .param p1, "r"    # Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    return-object v0
.end method

.method get(Ljava/util/concurrent/Future;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "ff":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method remove(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    .locals 3
    .param p1, "r"    # Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    .prologue
    .line 163
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .line 164
    .local v0, "key":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    if-eqz v0, :cond_0

    .line 165
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mFuture:Ljava/util/concurrent/Future;
    invoke-static {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->access$100(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Ljava/util/concurrent/Future;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .line 168
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method remove(Ljava/util/concurrent/Future;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "ff":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .line 154
    .local v0, "key":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    if-eqz v0, :cond_0

    .line 155
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mRunnableMap:Ljava/util/HashMap;

    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    invoke-static {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->access$200(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .line 158
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->mFutureMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
