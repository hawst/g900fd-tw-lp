.class public Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;
.super Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;
.source "ImageMediaDocConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V
    .locals 26
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->convertCommon2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V

    .line 144
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;

    move/from16 v19, v0

    if-nez v19, :cond_0

    .line 145
    sget-object v19, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->TAG:Ljava/lang/String;

    const-string v20, "mediadoc not instance of ImageMediaDoc, Cannot convert"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-static/range {v19 .. v21}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    .end local p1    # "mediaDoc":Lcom/samsung/dcm/documents/MediaDoc;
    :goto_0
    return-void

    .restart local p1    # "mediaDoc":Lcom/samsung/dcm/documents/MediaDoc;
    :cond_0
    move-object/from16 v10, p1

    .line 148
    check-cast v10, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .line 150
    .local v10, "imd":Lcom/samsung/dcm/documents/ImageMediaDoc;
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->storeDocType(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc$DocType;)V

    .line 153
    new-instance v20, Lorg/apache/lucene/document/IntField;

    const-string v21, "faceCount"

    move-object/from16 v19, p1

    check-cast v19, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getFaceCount()Lcom/google/common/base/Optional;

    move-result-object v19

    sget-object v22, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->DEFAULT_FACE_COUNT_VALUE:Ljava/lang/Integer;

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/base/Optional;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v19

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 157
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->mSpatialContext:Lcom/spatial4j/core/context/SpatialContext;

    move-object/from16 v20, v0

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/location/Location;

    invoke-virtual/range {v19 .. v19}, Landroid/location/Location;->getLongitude()D

    move-result-wide v22

    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getLocation()Lcom/google/common/base/Optional;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/location/Location;

    invoke-virtual/range {v19 .. v19}, Landroid/location/Location;->getLatitude()D

    move-result-wide v24

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v15

    .line 161
    .local v15, "point":Lcom/spatial4j/core/shape/Shape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->mSpatialStrategy:Lorg/apache/lucene/spatial/SpatialStrategy;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lorg/apache/lucene/spatial/SpatialStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;

    move-result-object v6

    .local v6, "arr$":[Lorg/apache/lucene/document/Field;
    array-length v12, v6

    .local v12, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v12, :cond_1

    aget-object v8, v6, v9

    .line 163
    .local v8, "f":Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 161
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 168
    .end local v6    # "arr$":[Lorg/apache/lucene/document/Field;
    .end local v8    # "f":Lorg/apache/lucene/index/IndexableField;
    .end local v9    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "point":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getNamedPlace()Lcom/google/common/base/Optional;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 169
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getNamedPlace()Lcom/google/common/base/Optional;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/dcm/documents/LocationData;

    .line 171
    .local v11, "lData":Lcom/samsung/dcm/documents/LocationData;
    new-instance v20, Lorg/apache/lucene/document/StringField;

    const-string v21, "named_location"

    invoke-virtual {v11}, Lcom/samsung/dcm/documents/LocationData;->getLocality()Lcom/google/common/base/Optional;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v19

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 189
    .end local v11    # "lData":Lcom/samsung/dcm/documents/LocationData;
    :cond_2
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getEvents()Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    if-lez v19, :cond_4

    .line 190
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getEvents()Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/dcm/documents/EventData;

    .line 191
    .local v7, "event":Lcom/samsung/dcm/documents/EventData;
    invoke-virtual {v7}, Lcom/samsung/dcm/documents/EventData;->getTitle()Ljava/lang/String;

    move-result-object v18

    .line 193
    .local v18, "temp":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 194
    new-instance v19, Lorg/apache/lucene/document/StringField;

    const-string v20, "calendar_event"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v19 .. v22}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 196
    :cond_3
    new-instance v19, Lorg/apache/lucene/document/TextField;

    const-string v20, "event_uri"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/dcm/documents/EventData;->getUri()Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v19 .. v22}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_2

    .line 201
    .end local v7    # "event":Lcom/samsung/dcm/documents/EventData;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v18    # "temp":Ljava/lang/String;
    :cond_4
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getPersons()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->size()I

    move-result v19

    if-lez v19, :cond_5

    .line 202
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getPersons()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/dcm/documents/Person;

    .line 203
    .local v14, "person":Lcom/samsung/dcm/documents/Person;
    invoke-virtual {v14}, Lcom/samsung/dcm/documents/Person;->getDisplayName()Ljava/lang/String;

    move-result-object v13

    .line 206
    .local v13, "name":Ljava/lang/String;
    new-instance v19, Lorg/apache/lucene/document/StringField;

    const-string v20, "person_names"

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v19 .. v22}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_3

    .line 210
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "person":Lcom/samsung/dcm/documents/Person;
    :cond_5
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getSceneTypes()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->size()I

    move-result v19

    if-lez v19, :cond_6

    .line 211
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getSceneTypes()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 212
    .local v16, "sceneType":Ljava/lang/String;
    new-instance v19, Lorg/apache/lucene/document/StringField;

    const-string v20, "scene_type"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v19 .. v22}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_4

    .line 215
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v16    # "sceneType":Ljava/lang/String;
    :cond_6
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getSubSceneTypes()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->size()I

    move-result v19

    if-lez v19, :cond_7

    .line 216
    invoke-virtual {v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getSubSceneTypes()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 217
    .local v17, "subSceneType":Ljava/lang/String;
    new-instance v19, Lorg/apache/lucene/document/StringField;

    const-string v20, "subscene_type"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v19 .. v22}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_5

    .line 221
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v17    # "subSceneType":Ljava/lang/String;
    :cond_7
    new-instance v19, Lorg/apache/lucene/document/IntField;

    const-string v20, "osc_status"

    check-cast p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .end local p1    # "mediaDoc":Lcom/samsung/dcm/documents/MediaDoc;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getOscStatus()I

    move-result v21

    sget-object v22, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v19 .. v22}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0
.end method

.method public convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V
    .locals 26
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;

    .prologue
    .line 55
    invoke-super/range {p0 .. p2}, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V

    .line 56
    move-object/from16 v0, p2

    instance-of v0, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;

    move/from16 v23, v0

    if-nez v23, :cond_0

    .line 57
    sget-object v23, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;->TAG:Ljava/lang/String;

    const-string v24, "mediadoc not instance of ImageMediaDoc, Cannot convert"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-static/range {v23 .. v25}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    :goto_0
    return-void

    :cond_0
    move-object/from16 v9, p2

    .line 61
    check-cast v9, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .line 62
    .local v9, "imd":Lcom/samsung/dcm/documents/ImageMediaDoc;
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 63
    .local v14, "persons":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 64
    .local v16, "sceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 65
    .local v18, "subSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v4, "events":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/documents/EventData;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v5

    .line 68
    .local v5, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexableField;>;"
    const/4 v3, 0x0

    .line 69
    .local v3, "event":Lcom/samsung/dcm/documents/EventData;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/IndexableField;

    .line 70
    .local v8, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "faceCount"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 71
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v19

    .line 72
    .local v19, "temp":I
    sget-object v23, Lcom/samsung/dcm/framework/indexer/converter/MediaDocConverter;->DEFAULT_FACE_COUNT_VALUE:Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move/from16 v0, v19

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 73
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setFaceCount(Ljava/lang/Integer;)V

    goto :goto_1

    .line 74
    .end local v19    # "temp":I
    :cond_2
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "location__x"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 76
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v12

    .line 78
    .local v12, "lon":Ljava/lang/String;
    const-string v23, "location__y"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 80
    .local v10, "lat":Ljava/lang/String;
    const/4 v6, 0x0

    .line 81
    .local v6, "geo":Landroid/location/Location;
    if-eqz v10, :cond_1

    if-eqz v12, :cond_1

    .line 82
    new-instance v6, Landroid/location/Location;

    .end local v6    # "geo":Landroid/location/Location;
    const-string v23, "DCM_Provider"

    move-object/from16 v0, v23

    invoke-direct {v6, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v6    # "geo":Landroid/location/Location;
    invoke-static {v12}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 84
    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 85
    invoke-virtual {v9, v6}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setLocation(Landroid/location/Location;)V

    goto :goto_1

    .line 87
    .end local v6    # "geo":Landroid/location/Location;
    .end local v10    # "lat":Ljava/lang/String;
    .end local v12    # "lon":Ljava/lang/String;
    :cond_3
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "named_location"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 92
    new-instance v11, Lcom/samsung/dcm/documents/LocationData;

    invoke-direct {v11}, Lcom/samsung/dcm/documents/LocationData;-><init>()V

    .line 93
    .local v11, "location":Lcom/samsung/dcm/documents/LocationData;
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/LocationData;->setLocality(Ljava/lang/String;)V

    .line 94
    invoke-virtual {v9, v11}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setNamedPlace(Lcom/samsung/dcm/documents/LocationData;)V

    goto/16 :goto_1

    .line 96
    .end local v11    # "location":Lcom/samsung/dcm/documents/LocationData;
    :cond_4
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "id"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 97
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setId(I)V

    goto/16 :goto_1

    .line 98
    :cond_5
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "person_names"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 99
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "displayName":Ljava/lang/String;
    new-instance v13, Lcom/samsung/dcm/documents/Person;

    invoke-direct {v13, v2, v2}, Lcom/samsung/dcm/documents/Person;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .local v13, "person":Lcom/samsung/dcm/documents/Person;
    invoke-interface {v14, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 102
    .end local v2    # "displayName":Ljava/lang/String;
    .end local v13    # "person":Lcom/samsung/dcm/documents/Person;
    :cond_6
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "calendar_event"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 103
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v20

    .line 104
    .local v20, "title":Ljava/lang/String;
    new-instance v3, Lcom/samsung/dcm/documents/EventData;

    .end local v3    # "event":Lcom/samsung/dcm/documents/EventData;
    invoke-direct {v3}, Lcom/samsung/dcm/documents/EventData;-><init>()V

    .line 105
    .restart local v3    # "event":Lcom/samsung/dcm/documents/EventData;
    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/documents/EventData;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 106
    .end local v20    # "title":Ljava/lang/String;
    :cond_7
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "event_uri"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 107
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v22

    .line 108
    .local v22, "value":Ljava/lang/String;
    if-eqz v22, :cond_8

    if-eqz v3, :cond_8

    .line 109
    const-string v23, "\\s"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 110
    .local v21, "uriSplit":[Ljava/lang/String;
    const/16 v23, 0x0

    aget-object v23, v21, v23

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/samsung/dcm/documents/EventData;->setUri(Landroid/net/Uri;)V

    .line 112
    .end local v21    # "uriSplit":[Ljava/lang/String;
    :cond_8
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 113
    .end local v22    # "value":Ljava/lang/String;
    :cond_9
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "scene_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 114
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v15

    .line 115
    .local v15, "sceneType":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 116
    .end local v15    # "sceneType":Ljava/lang/String;
    :cond_a
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "subscene_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 117
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v17

    .line 118
    .local v17, "subSceneType":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 119
    .end local v17    # "subSceneType":Ljava/lang/String;
    :cond_b
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    const-string v24, "osc_status"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 120
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setOscStatus(I)V

    goto/16 :goto_1

    .line 123
    .end local v8    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_c
    invoke-virtual {v9, v14}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setPersons(Ljava/util/Set;)V

    .line 124
    invoke-virtual {v9, v4}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setEvents(Ljava/util/List;)V

    .line 125
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setSceneTypes(Ljava/util/Set;)V

    .line 126
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setSubSceneTypes(Ljava/util/Set;)V

    goto/16 :goto_0
.end method
