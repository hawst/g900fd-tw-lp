.class public Lcom/samsung/dcm/framework/configuration/DcmTransport;
.super Ljava/lang/Object;
.source "DcmTransport.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static gmDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field private static gmDCMNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private static gmDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

.field private static gmSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;


# instance fields
.field private mbSystemOnShutdown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const-class v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->TAG:Ljava/lang/String;

    .line 21
    sput-object v1, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 22
    sput-object v1, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 23
    sput-object v1, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 24
    sput-object v1, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isSystemShutdown()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->mbSystemOnShutdown:Z

    return-void
.end method


# virtual methods
.method public getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    const-string v1, "MediaManager cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    return-object v0
.end method

.method public getDCMPolicyManager()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    const-string v1, "DCMPolicyManager cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    return-object v0
.end method

.method public getIntentReceiver()Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    const-string v1, "SystemBroadcastReceiver cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    return-object v0
.end method

.method public getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const-string v1, "DCMNRTManager cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    return-object v0
.end method

.method public isSystemOnShutdown()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->mbSystemOnShutdown:Z

    return v0
.end method

.method public setDCMController(Lcom/samsung/dcm/framework/extractormanager/DCMController;)V
    .locals 1
    .param p1, "controller"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .prologue
    .line 33
    const-string v0, "DCMController cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 35
    return-void
.end method

.method public setDCMPolicyManager(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;)V
    .locals 1
    .param p1, "policymanager"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .prologue
    .line 59
    const-string v0, "DCMPolicyManager cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 63
    return-void
.end method

.method public setIntentReceiver(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V
    .locals 1
    .param p1, "receiver"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .prologue
    .line 48
    const-string v0, "SystemBroadcastReceiver cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 51
    return-void
.end method

.method public setNRTManager(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;)V
    .locals 1
    .param p1, "dcmnrtManager"    # Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .prologue
    .line 38
    const-string v0, "DCMNRTManager cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->gmDCMNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 40
    return-void
.end method

.method public setSystemOnShutdown(Z)V
    .locals 0
    .param p1, "systemOnShutdown"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/configuration/DcmTransport;->mbSystemOnShutdown:Z

    .line 78
    return-void
.end method
