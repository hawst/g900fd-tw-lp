.class Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;
.super Landroid/app/IProcessObserver$Stub;
.source "ProcessObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/configuration/ProcessObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 6
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "bForeground"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 86
    sget-object v0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->TAG:Ljava/lang/String;

    const-string v1, "onForegroundActivitiesChanged = "

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, " uuid = "

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, " foreground = "

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    if-eqz p3, :cond_0

    .line 90
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$100(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Ljava/util/Timer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$200(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    move-result-object v0

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->APP:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v0, v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    # setter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;
    invoke-static {v0, v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$102(Lcom/samsung/dcm/framework/configuration/ProcessObserver;Ljava/util/Timer;)Ljava/util/Timer;

    .line 97
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$100(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Ljava/util/Timer;

    move-result-object v0

    new-instance v2, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;

    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-direct {v2, v3}, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;-><init>(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 103
    :goto_0
    monitor-exit v1

    .line 113
    :cond_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseRequested:Z
    invoke-static {v0, v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$002(Lcom/samsung/dcm/framework/configuration/ProcessObserver;Z)Z

    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onProcessDied(II)V
    .locals 5
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 73
    sget-object v0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->TAG:Ljava/lang/String;

    const-string v1, "onProcessDied = "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method public onProcessStateChanged(III)V
    .locals 5
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "procState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->TAG:Ljava/lang/String;

    const-string v1, "onImportanceChanged = "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    return-void
.end method
