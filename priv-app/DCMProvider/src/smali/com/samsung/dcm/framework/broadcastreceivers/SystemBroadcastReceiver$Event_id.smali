.class public final enum Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
.super Ljava/lang/Enum;
.source "SystemBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Event_id"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_MEDIA_EJECTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_MEDIA_MOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_MEDIA_SCAN_STARTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_NETWORK_CHANGED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_SETUP_WIZARD_COMPLETE:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_SETUP_WIZARD_COMPLETE2:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field public static final enum EVENT_SIOP:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_CAMERA_ON"

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 74
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_CAMERA_OFF"

    invoke-direct {v0, v1, v4}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 75
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_SCREEN_ON"

    invoke-direct {v0, v1, v5}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 76
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_SCREEN_OFF"

    invoke-direct {v0, v1, v6}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 77
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_SIOP"

    invoke-direct {v0, v1, v7}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SIOP:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 78
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_SHUTDOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 79
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_NETWORK_CHANGED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_NETWORK_CHANGED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 82
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_MEDIA_MOUNTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_MOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 83
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_MEDIA_UNMOUNTED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 84
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_MEDIA_EJECTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_EJECTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 85
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_MEDIA_SCAN_STARTED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_STARTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 86
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_MEDIA_SCAN_FINISHED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 87
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_SETUP_WIZARD_COMPLETE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 89
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    const-string v1, "EVENT_SETUP_WIZARD_COMPLETE2"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE2:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 72
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SIOP:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_NETWORK_CHANGED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_MOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_EJECTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_STARTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE2:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->$VALUES:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->$VALUES:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    return-object v0
.end method
