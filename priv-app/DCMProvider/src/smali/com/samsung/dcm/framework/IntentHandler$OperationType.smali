.class public final enum Lcom/samsung/dcm/framework/IntentHandler$OperationType;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OperationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/IntentHandler$OperationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/IntentHandler$OperationType;

.field public static final enum Delete:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

.field public static final enum Insert:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

.field public static final enum PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

.field public static final enum Update:Lcom/samsung/dcm/framework/IntentHandler$OperationType;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 372
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    const-string v1, "Insert"

    const-string v2, "insert"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Insert:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    .line 373
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    const-string v1, "Update"

    const-string v2, "update"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Update:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    .line 374
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    const-string v1, "Delete"

    const-string v2, "delete"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Delete:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    .line 375
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    const-string v1, "PerformAction"

    const-string v2, "perform_action"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    .line 371
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Insert:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Update:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->Delete:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 379
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 380
    iput-object p3, p0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->mName:Ljava/lang/String;

    .line 381
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 388
    const-string v4, "OperationType fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->values()[Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 390
    .local v3, "ot":Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    iget-object v4, v3, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 394
    .end local v3    # "ot":Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    :goto_1
    return-object v3

    .line 389
    .restart local v3    # "ot":Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 394
    .end local v3    # "ot":Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 371
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/IntentHandler$OperationType;
    .locals 1

    .prologue
    .line 371
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/IntentHandler$OperationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->mName:Ljava/lang/String;

    return-object v0
.end method
