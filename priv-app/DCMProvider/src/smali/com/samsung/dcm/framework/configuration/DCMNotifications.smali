.class public final Lcom/samsung/dcm/framework/configuration/DCMNotifications;
.super Ljava/lang/Object;
.source "DCMNotifications.java"


# static fields
.field private static final ARGUEMENT_START:Ljava/lang/String; = "?"

.field private static final IMAGE_DOCUMENT_URI:Ljava/lang/String; = "content://com.sec.dcm.provider.DCMProvider.data/imagedocument"

.field private static final NOTIFY_OPERATION_INSERT:Ljava/lang/String; = "INSERT"

.field private static final NOTIFY_OPERATION_NL_UPDATE:Ljava/lang/String; = "NAMED_LOCATION_UPDATE"

.field private static final NOTIFY_OPERATION_UPDATE:Ljava/lang/String; = "UPDATE"

.field private static final VIDEO_DOCUMENT_URI:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static nrtImageUpdatelistner:Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->mContext:Landroid/content/Context;

    .line 59
    sget-object v0, Lcom/sec/dcm/provider/dcmcolumns/DCMDocumentVideoColumns;->URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->VIDEO_DOCUMENT_URI:Ljava/lang/String;

    .line 94
    new-instance v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications$1;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/configuration/DCMNotifications$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->nrtImageUpdatelistner:Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static final getImageInsertNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 87
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/imagedocument"

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getInsertNotificationUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getImageUpdateListner()Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->nrtImageUpdatelistner:Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    return-object v0
.end method

.method public static final getImageUpdateNLNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 83
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/imagedocument"

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getUpdateNLNotificationUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final getImageUpdateNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 79
    const-string v0, "content://com.sec.dcm.provider.DCMProvider.data/imagedocument"

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getUpdateNotificationUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static final getInsertNotificationUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "baseUri"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "INSERT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static final getUpdateNLNotificationUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "baseUri"    # Ljava/lang/String;

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NAMED_LOCATION_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static final getUpdateNotificationUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "baseUri"    # Ljava/lang/String;

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final getVideoInsertNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->VIDEO_DOCUMENT_URI:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getInsertNotificationUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    sput-object p0, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method
