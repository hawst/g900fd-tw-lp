.class public Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;
.super Lcom/samsung/dcm/framework/extractors/Extractor;
.source "PersonNameExtractor.java"


# static fields
.field private static final FACES_PROJECTION:[Ljava/lang/String;

.field public static final MY_PROFILE:Ljava/lang/String; = "profile"

.field private static final PERSON_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static final UNKNOWN_FACE:I = 0x1


# instance fields
.field private mContextResolver:Landroid/content/ContentResolver;

.field private mRecommededId2Person:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/dcm/documents/Person;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    const-class v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->TAG:Ljava/lang/String;

    .line 62
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "image_id"

    aput-object v1, v0, v2

    const-string v1, "recommended_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->FACES_PROJECTION:[Ljava/lang/String;

    .line 65
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v2

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->PERSON_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractors/Extractor;-><init>()V

    .line 59
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mRecommededId2Person:Landroid/util/LruCache;

    return-void
.end method

.method private addPersonsCategories(Lcom/samsung/dcm/documents/ImageMediaDoc;)V
    .locals 6
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/ImageMediaDoc;

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getPersons()Ljava/util/Set;

    move-result-object v3

    .line 213
    .local v3, "persons":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/documents/Person;

    .line 214
    .local v2, "p":Lcom/samsung/dcm/documents/Person;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v4, "root_person"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    iget-object v4, v2, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    const-string v5, "profile"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 220
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "profile/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/dcm/documents/ImageMediaDoc;->addCategory(Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :cond_0
    iget-object v4, v2, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 226
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "p":Lcom/samsung/dcm/documents/Person;
    :cond_1
    return-void
.end method

.method private fillRecomendedPerson(Landroid/net/Uri;Z)Ljava/util/Set;
    .locals 19
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "checkCache"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Z)",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/documents/Person;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 142
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    .line 143
    .local v17, "personId2Check":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 145
    .local v9, "docPersons":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/documents/Person;>;"
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 146
    .local v10, "id":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mContextResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/arcsoft/provider/Columns;->FACES_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->FACES_PROJECTION:[Ljava/lang/String;

    const-string v5, "image_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v10}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 151
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 152
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 155
    :cond_0
    const-string v2, "recommended_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 157
    .local v16, "personId":I
    const/4 v2, 0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_2

    .line 159
    const/4 v15, 0x0

    .line 160
    .local v15, "p":Lcom/samsung/dcm/documents/Person;
    if-eqz p2, :cond_1

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mRecommededId2Person:Landroid/util/LruCache;

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "p":Lcom/samsung/dcm/documents/Person;
    check-cast v15, Lcom/samsung/dcm/documents/Person;

    .line 163
    .restart local v15    # "p":Lcom/samsung/dcm/documents/Person;
    :cond_1
    if-nez v15, :cond_9

    .line 165
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 171
    .end local v15    # "p":Lcom/samsung/dcm/documents/Person;
    :cond_2
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 173
    .end local v16    # "personId":I
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 178
    :cond_4
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 179
    const-string v2, ","

    invoke-static {v2}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v12

    .line 180
    .local v12, "joiner":Lcom/google/common/base/Joiner;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .local v11, "inClause":Ljava/lang/StringBuilder;
    const-string v2, " in ("

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string v2, ")"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mContextResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/arcsoft/provider/Columns;->PERSONS_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->PERSON_PROJECTION:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 188
    if-eqz v8, :cond_8

    .line 189
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 191
    :cond_5
    const-string v2, "name"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 192
    .local v14, "name":Ljava/lang/String;
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 193
    .local v16, "personId":Ljava/lang/Integer;
    if-eqz v14, :cond_6

    .line 194
    const-string v2, "/"

    invoke-virtual {v14, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 195
    .local v13, "lookupAndName":[Ljava/lang/String;
    new-instance v15, Lcom/samsung/dcm/documents/Person;

    const/4 v2, 0x0

    aget-object v2, v13, v2

    const/4 v3, 0x1

    aget-object v3, v13, v3

    invoke-direct {v15, v2, v3}, Lcom/samsung/dcm/documents/Person;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    .restart local v15    # "p":Lcom/samsung/dcm/documents/Person;
    invoke-virtual {v9, v15}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mRecommededId2Person:Landroid/util/LruCache;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v15}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    .end local v13    # "lookupAndName":[Ljava/lang/String;
    .end local v15    # "p":Lcom/samsung/dcm/documents/Person;
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 203
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "personId":Ljava/lang/Integer;
    :cond_7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 208
    .end local v11    # "inClause":Ljava/lang/StringBuilder;
    .end local v12    # "joiner":Lcom/google/common/base/Joiner;
    :cond_8
    return-object v9

    .line 168
    .restart local v15    # "p":Lcom/samsung/dcm/documents/Person;
    .local v16, "personId":I
    :cond_9
    invoke-virtual {v9, v15}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private process_doc(Lorg/apache/lucene/document/Document;)Landroid/os/Bundle;
    .locals 9
    .param p1, "lucenedoc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 95
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 96
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v4, "personNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "uri"

    invoke-virtual {p1, v6}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 99
    .local v5, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    :try_start_0
    invoke-direct {p0, v5, v6}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->fillRecomendedPerson(Landroid/net/Uri;Z)Ljava/util/Set;

    move-result-object v3

    .line 100
    .local v3, "person":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 101
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/documents/Person;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 102
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/dcm/documents/Person;

    invoke-virtual {v6}, Lcom/samsung/dcm/documents/Person;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/documents/Person;>;"
    .end local v3    # "person":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    :catch_0
    move-exception v1

    .line 106
    .local v1, "e":Ljava/lang/Throwable;
    sget-object v6, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception in: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_0
    const-string v6, "PERSON_NAME_LIST"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 109
    return-object v0
.end method

.method private process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 8
    .param p1, "doc"    # Lcom/samsung/dcm/documents/ImageMediaDoc;

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getFaceCount()Lcom/google/common/base/Optional;

    move-result-object v1

    .line 80
    .local v1, "faceCount":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    :try_start_0
    sget-object v3, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->TAG:Ljava/lang/String;

    const-string v4, "Face count present ="

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_0

    .line 82
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getUri()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->fillRecomendedPerson(Landroid/net/Uri;Z)Ljava/util/Set;

    move-result-object v2

    .line 83
    .local v2, "persons":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    invoke-virtual {p1, v2}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setPersons(Ljava/util/Set;)V

    .line 84
    sget-object v3, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->TAG:Ljava/lang/String;

    const-string v4, "Adding person categories"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->addPersonsCategories(Lcom/samsung/dcm/documents/ImageMediaDoc;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v2    # "persons":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    :cond_0
    :goto_0
    return-object p1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v3, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public initialize()V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method protected initializeSupportedClasses()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mSupportedExtractors:Ljava/util/LinkedList;

    const-class v1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 128
    return-void
.end method

.method public process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 114
    instance-of v1, p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    if-eqz v1, :cond_0

    .line 115
    check-cast p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v1

    .line 122
    :goto_0
    return-object v1

    .line 117
    .restart local p1    # "data":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/document/Document;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 119
    check-cast v0, Lorg/apache/lucene/document/Document;

    .line 120
    .local v0, "lucenedoc":Lorg/apache/lucene/document/Document;
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->process_doc(Lorg/apache/lucene/document/Document;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0

    .line 122
    .end local v0    # "lucenedoc":Lorg/apache/lucene/document/Document;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public setContextResolver(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "contextResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 73
    const-string v0, "Context resolver cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;->mContextResolver:Landroid/content/ContentResolver;

    .line 75
    return-void
.end method
