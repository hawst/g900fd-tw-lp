.class public abstract Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
.super Ljava/lang/Object;
.source "IMediaDocConverter.java"


# static fields
.field public static final DEFAULT_FACE_COUNT_VALUE:Ljava/lang/Integer;

.field public static final FIELD_ALBUM:Ljava/lang/String; = "album"

.field public static final FIELD_ARTIST:Ljava/lang/String; = "artist"

.field public static final FIELD_BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket"

.field public static final FIELD_BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final FIELD_CATEGORIES:Ljava/lang/String; = "categories"

.field public static final FIELD_CREATION_DATE:Ljava/lang/String; = "date"

.field public static final FIELD_DELETED:Ljava/lang/String; = "deleted"

.field public static final FIELD_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FIELD_DOCTYPE:Ljava/lang/String; = "doctype"

.field public static final FIELD_DURATION:Ljava/lang/String; = "duration"

.field public static final FIELD_EVENT_TITLE:Ljava/lang/String; = "calendar_event"

.field public static final FIELD_EVENT_URI:Ljava/lang/String; = "event_uri"

.field public static final FIELD_FACECOUNT:Ljava/lang/String; = "faceCount"

.field public static final FIELD_FATVOLUME_ID:Ljava/lang/String; = "fatvolume_id"

.field public static final FIELD_GENRE:Ljava/lang/String; = "genre"

.field public static final FIELD_ID:Ljava/lang/String; = "id"

.field public static final FIELD_LANGUAGE:Ljava/lang/String; = "language"

.field public static final FIELD_LOCATION:Ljava/lang/String; = "location"

.field public static final FIELD_MIME:Ljava/lang/String; = "mimetype"

.field public static final FIELD_NAMED_LOCATION:Ljava/lang/String; = "named_location"

.field public static final FIELD_OSC_STATUS:Ljava/lang/String; = "osc_status"

.field public static final FIELD_PATH:Ljava/lang/String; = "path"

.field public static final FIELD_PERSON:Ljava/lang/String; = "person_names"

.field public static final FIELD_PLACE:Ljava/lang/String; = "place"

.field public static final FIELD_RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final FIELD_SCENE_TYPE:Ljava/lang/String; = "scene_type"

.field public static final FIELD_STORAGE_ID:Ljava/lang/String; = "storage_id"

.field public static final FIELD_SUBSCENE_TYPE:Ljava/lang/String; = "subscene_type"

.field public static final FIELD_TITLE:Ljava/lang/String; = "title"

.field public static final FIELD_TITLE_LOWER:Ljava/lang/String; = "title_lower"

.field public static final FIELD_URI:Ljava/lang/String; = "uri"

.field public static final FIELD_USER_TAG:Ljava/lang/String; = "user_tags"

.field public static final FIELD_YEAR:Ljava/lang/String; = "year"

.field private static sConverters:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

.field protected mSpatialContext:Lcom/spatial4j/core/context/SpatialContext;

.field protected mSpatialStrategy:Lorg/apache/lucene/spatial/SpatialStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->sConverters:Landroid/util/SparseArray;

    .line 47
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->DEFAULT_FACE_COUNT_VALUE:Ljava/lang/Integer;

    .line 95
    sget-object v0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->sConverters:Landroid/util/SparseArray;

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    new-instance v2, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;

    invoke-direct {v2}, Lcom/samsung/dcm/framework/indexer/converter/AudioMediaDocConverter;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 96
    sget-object v0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->sConverters:Landroid/util/SparseArray;

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    new-instance v2, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;

    invoke-direct {v2}, Lcom/samsung/dcm/framework/indexer/converter/VideoMediaDocConverter;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 97
    sget-object v0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->sConverters:Landroid/util/SparseArray;

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    new-instance v2, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;

    invoke-direct {v2}, Lcom/samsung/dcm/framework/indexer/converter/ImageMediaDocConverter;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 98
    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    sget-object v0, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    iput-object v0, p0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->mSpatialContext:Lcom/spatial4j/core/context/SpatialContext;

    .line 106
    new-instance v0, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    iget-object v1, p0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->mSpatialContext:Lcom/spatial4j/core/context/SpatialContext;

    const-string v2, "location"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->mSpatialStrategy:Lorg/apache/lucene/spatial/SpatialStrategy;

    .line 107
    return-void
.end method

.method public static final getConverter(Lcom/samsung/dcm/documents/MediaDoc;)Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
    .locals 2
    .param p0, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;

    .prologue
    .line 126
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-virtual {p0}, Lcom/samsung/dcm/documents/MediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v0

    .line 129
    .local v0, "mediaDocType":I
    sget-object v1, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->sConverters:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;

    return-object v1
.end method

.method public static final getConverter(Lorg/apache/lucene/document/Document;)Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
    .locals 2
    .param p0, "doc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 134
    const-string v1, "uri"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    const-string v1, "doctype"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string v1, "doctype"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 138
    .local v0, "docType":I
    sget-object v1, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->sConverters:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;

    return-object v1
.end method


# virtual methods
.method public abstract convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract convert2MediaDoc(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V
.end method

.method public setFacetFields(Lorg/apache/lucene/facet/index/FacetFields;)V
    .locals 0
    .param p1, "facetFields"    # Lorg/apache/lucene/facet/index/FacetFields;

    .prologue
    .line 117
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iput-object p1, p0, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->mFacetFields:Lorg/apache/lucene/facet/index/FacetFields;

    .line 119
    return-void
.end method
