.class public Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;
.super Ljava/lang/Object;
.source "DocumentsUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanDocumentContent(Lorg/apache/lucene/document/Document;)V
    .locals 1
    .param p0, "doc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 19
    invoke-virtual {p0}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 20
    return-void
.end method
