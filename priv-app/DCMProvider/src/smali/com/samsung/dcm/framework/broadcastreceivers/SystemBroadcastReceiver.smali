.class public Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SystemBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;,
        Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$DCMBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_CAMERA_START:Ljava/lang/String; = "com.sec.android.app.camera.ACTION_CAMERA_START"

.field public static final ACTION_CAMERA_STOP:Ljava/lang/String; = "com.sec.android.app.camera.ACTION_CAMERA_STOP"

.field public static ACTION_NETWORK_CHANGE:Ljava/lang/String; = null

.field public static final ACTION_SETUP_WIZARD_COMPLETE:Ljava/lang/String; = "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

.field public static final ACTION_SETUP_WIZARD_COMPLETE2:Ljava/lang/String; = "com.sec.android.app.setupwizard.SETUPWIZARD_COMPLETE"

.field public static final ACTION_SIOP_LEVEL:Ljava/lang/String; = "android.intent.action.CHECK_SIOP_LEVEL"

.field private static MAX_NOTIFICATION_TIMEOUT:I = 0x0

.field public static final SIOP_LEVEL_KEYWORD:Ljava/lang/String; = "siop_level_broadcast"

.field private static final TAG:Ljava/lang/String;

.field public static mEventMap:Lcom/google/common/collect/HashBiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/HashBiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;",
            ">;"
        }
    .end annotation
.end field

.field private static mSelf:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private mNotificationQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mRegisteredListnerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;",
            ">;>;"
        }
    .end annotation
.end field

.field private mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private mbRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mSelf:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 70
    const-class v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 107
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->ACTION_NETWORK_CHANGE:Ljava/lang/String;

    .line 118
    invoke-static {}, Lcom/google/common/collect/HashBiMap;->create()Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    .line 123
    const/4 v0, 0x1

    sput v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->MAX_NOTIFICATION_TIMEOUT:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 93
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 120
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 126
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mRegisteredListnerMap:Ljava/util/HashMap;

    .line 128
    iput-object p1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 129
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mbRegistered:Z

    .line 132
    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getNotificationFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    move-result-object v0

    .line 133
    .local v0, "factory":Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    new-instance v1, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getMaxThreadCount()I

    move-result v2

    invoke-direct {v1, v2, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 138
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mNotificationQueue:Ljava/util/concurrent/BlockingQueue;

    .line 139
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    sget v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->MAX_NOTIFICATION_TIMEOUT:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    .line 140
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 141
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->notifyListeners(Landroid/content/Intent;)V

    return-void
.end method

.method public static declared-synchronized getInstController()Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    .locals 3

    .prologue
    .line 67
    const-class v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mSelf:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    const-string v2, "Not yet intialized"

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getRegisteredListnersForIntent(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 18
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    const-wide/16 v10, 0x0

    .local v10, "startTime":J
    const-wide/16 v8, 0x0

    .line 257
    .local v8, "mendTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 258
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v7, "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 261
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 263
    .local v2, "action":Ljava/lang/String;
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    invoke-virtual {v12, v2}, Lcom/google/common/collect/HashBiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 264
    .local v4, "event_id":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v13, "Event id = "

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mRegisteredListnerMap:Ljava/util/HashMap;

    invoke-virtual {v12, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 268
    .local v6, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    if-nez v6, :cond_0

    .line 269
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v13, "No one is registered with Event Id"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 278
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    sub-long/2addr v12, v10

    const-wide/16 v14, 0x3e8

    div-long v8, v12, v14

    .line 279
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v13, "[DCM_Performance] getRegisteredListnersForIntent completed in time "

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, " microsec. "

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    :goto_0
    return-object v7

    .line 273
    :cond_0
    :try_start_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    .line 274
    .local v3, "callback":Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 277
    .end local v2    # "action":Ljava/lang/String;
    .end local v3    # "callback":Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;
    .end local v4    # "event_id":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    :catchall_0
    move-exception v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v13}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 278
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    sub-long/2addr v14, v10

    const-wide/16 v16, 0x3e8

    div-long v8, v14, v16

    .line 279
    sget-object v13, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v14, "[DCM_Performance] getRegisteredListnersForIntent completed in time "

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, " microsec. "

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v12

    .line 277
    .restart local v2    # "action":Ljava/lang/String;
    .restart local v4    # "event_id":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v12}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 278
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    sub-long/2addr v12, v10

    const-wide/16 v14, 0x3e8

    div-long v8, v12, v14

    .line 279
    sget-object v12, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v13, "[DCM_Performance] getRegisteredListnersForIntent completed in time "

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, " microsec. "

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;)Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const-class v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mSelf:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mSelf:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 60
    :cond_0
    sget-object v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mSelf:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private notifyListeners(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->getRegisteredListnersForIntent(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v6

    .line 226
    .local v6, "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 229
    .local v1, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_0

    .line 230
    new-instance v1, Landroid/os/Bundle;

    .end local v1    # "bundle":Landroid/os/Bundle;
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 234
    .restart local v1    # "bundle":Landroid/os/Bundle;
    :cond_0
    const-string v7, "path"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 235
    const-string v7, "path"

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_1
    sget-object v7, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    invoke-virtual {v7, v0}, Lcom/google/common/collect/HashBiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 241
    .local v3, "event_id":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    :try_start_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    .line 242
    .local v5, "listner":Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;
    invoke-interface {v5, v3, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;->onIntentReceived(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 245
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "listner":Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;
    :catch_0
    move-exception v2

    .line 246
    .local v2, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v8, "Execption "

    invoke-static {v7, v8, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 248
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 209
    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Received broadcast"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    const-wide/16 v2, 0x0

    .local v2, "startTime":J
    const-wide/16 v0, 0x0

    .line 211
    .local v0, "mendTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 212
    invoke-virtual {p0, p2, v8}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->postNewIntent(Landroid/content/Intent;I)V

    .line 213
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    const-wide/16 v6, 0x3e8

    div-long v0, v4, v6

    .line 214
    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "[DCM_Performance] onReceive completed in time "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    const-string v8, " microsec. "

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    return-void
.end method

.method postNewIntent(Landroid/content/Intent;I)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "delayInMs"    # I

    .prologue
    .line 144
    new-instance v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$1;-><init>(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;Landroid/content/Intent;)V

    .line 153
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    int-to-long v2, p2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 154
    return-void
.end method

.method public registerIntents()V
    .locals 4

    .prologue
    .line 157
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mbRegistered:Z

    if-eqz v1, :cond_0

    .line 205
    :goto_0
    return-void

    .line 160
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 161
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 162
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    const-string v1, "com.sec.android.app.camera.ACTION_CAMERA_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    const-string v1, "com.sec.android.app.camera.ACTION_CAMERA_STOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    const-string v1, "android.intent.action.CHECK_SIOP_LEVEL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 168
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->ACTION_NETWORK_CHANGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 170
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.SCREEN_ON"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.SCREEN_OFF"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "com.sec.android.app.camera.ACTION_CAMERA_START"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "com.sec.android.app.camera.ACTION_CAMERA_STOP"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.CHECK_SIOP_LEVEL"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SIOP:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    sget-object v2, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->ACTION_NETWORK_CHANGE:Ljava/lang/String;

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_NETWORK_CHANGED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 190
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_MOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.MEDIA_EJECT"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_EJECTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_STARTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 200
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mEventMap:Lcom/google/common/collect/HashBiMap;

    const-string v2, "com.sec.android.app.setupwizard.SETUPWIZARD_COMPLETE"

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SETUP_WIZARD_COMPLETE2:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/collect/HashBiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.app.setupwizard.SETUPWIZARD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 204
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mbRegistered:Z

    goto/16 :goto_0
.end method

.method public varargs registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V
    .locals 14
    .param p1, "listner"    # Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;
    .param p2, "event_ids"    # [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .prologue
    .line 286
    const-wide/16 v8, 0x0

    .local v8, "startTime":J
    const-wide/16 v4, 0x0

    .line 287
    .local v4, "mendTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 289
    iget-boolean v7, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mbRegistered:Z

    if-nez v7, :cond_0

    .line 290
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerIntents()V

    .line 292
    :cond_0
    iget-object v7, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 294
    move-object/from16 v0, p2

    .local v0, "arr$":[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    :try_start_0
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 295
    .local v1, "event":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    iget-object v7, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mRegisteredListnerMap:Ljava/util/HashMap;

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 298
    .local v6, "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    if-nez v6, :cond_1

    .line 299
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 300
    .restart local v6    # "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    iget-object v7, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mRegisteredListnerMap:Ljava/util/HashMap;

    invoke-virtual {v7, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    :cond_1
    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 306
    .end local v1    # "event":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .end local v6    # "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    :cond_2
    iget-object v7, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 308
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    sub-long/2addr v10, v8

    const-wide/16 v12, 0x3e8

    div-long v4, v10, v12

    .line 309
    sget-object v7, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v10, "[DCM_Performance] registerListener completed in time "

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string v13, " microsec. "

    aput-object v13, v11, v12

    invoke-static {v7, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    return-void

    .line 306
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catchall_0
    move-exception v7

    iget-object v10, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v7
.end method

.method public unRegisterListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;)V
    .locals 13
    .param p1, "listner"    # Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    .prologue
    .line 313
    const-wide/16 v6, 0x0

    .local v6, "startTime":J
    const-wide/16 v4, 0x0

    .line 314
    .local v4, "mendTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 316
    iget-object v8, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 318
    :try_start_0
    iget-object v8, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mRegisteredListnerMap:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 319
    .local v0, "allkey":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 320
    .local v1, "event":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    iget-object v8, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mRegisteredListnerMap:Ljava/util/HashMap;

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 323
    .local v3, "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    if-eqz v3, :cond_0

    .line 324
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 328
    .end local v0    # "allkey":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;>;"
    .end local v1    # "event":Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "registeredListners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;>;"
    :catchall_0
    move-exception v8

    iget-object v9, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v9}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v8

    .restart local v0    # "allkey":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v8, p0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->mListenerMapLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 331
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long/2addr v8, v6

    const-wide/16 v10, 0x3e8

    div-long v4, v8, v10

    .line 332
    sget-object v8, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v9, "[DCM_Performance] unregisterListener completed in time "

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, " microsec. "

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    return-void
.end method
