.class Lcom/samsung/dcm/framework/extractormanager/DCMController$1;
.super Ljava/lang/Object;
.source "DCMController.java"

# interfaces
.implements Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskFinished(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 142
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "task finished = "

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDCMContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$000(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$100(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mDatabaseRebuilderTask:Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$200(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/task/DatabaseRebuilderTask;->postDBrebuildIntent(Z)Z

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$300(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$300(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$400(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mCalendarHeavyTask:Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
    invoke-static {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$300(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;

    .line 166
    :cond_1
    :goto_0
    return-void

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->postOscStartIntent(Z)V

    goto :goto_0

    .line 157
    :cond_3
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$500(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 158
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "Heavy Task Finished,Resuming again to process pending images"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->postOscStartIntent(Z)V

    goto :goto_0

    .line 162
    :cond_4
    instance-of v0, p1, Lcom/samsung/dcm/framework/extractormanager/task/Worker;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mPrimaryExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$600(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->getActiveCount()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 163
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "Worker Tasks Finished,Resuming Heavy task to process pending images"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->postOscStartIntent(Z)V

    goto :goto_0
.end method

.method public onTaskRejected(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 137
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "Task was rejected by executor"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    return-void
.end method
