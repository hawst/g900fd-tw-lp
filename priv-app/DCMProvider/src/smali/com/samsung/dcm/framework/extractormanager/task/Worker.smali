.class public Lcom/samsung/dcm/framework/extractormanager/task/Worker;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "Worker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/task/Worker$1;,
        Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;
    }
.end annotation


# static fields
.field private static final BULK_DELETE_BATCH_SIZE:I = 0x3e8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

.field private final mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

.field private final mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private final mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;)V
    .locals 3
    .param p1, "container"    # Lcom/samsung/dcm/framework/extractormanager/DCMContainer;
    .param p2, "scanner"    # Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 110
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    .line 111
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    .line 112
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 114
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    new-instance v2, Lorg/apache/lucene/document/Document;

    invoke-direct {v2}, Lorg/apache/lucene/document/Document;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;-><init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lorg/apache/lucene/document/Document;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    .line 115
    return-void
.end method

.method private addDataFromExtractors(Lcom/samsung/dcm/framework/extractormanager/Command;Lorg/apache/lucene/document/Document;Ljava/util/List;)V
    .locals 39
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .param p2, "document"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 498
    .local p3, "categories":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getExtractors()Ljava/util/Set;

    move-result-object v15

    .line 499
    .local v15, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Landroid/os/Bundle;

    sget-object v36, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual/range {v36 .. v36}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v13

    .line 502
    .local v13, "existingDataBundle":Landroid/os/Bundle;
    if-nez v13, :cond_1

    .line 503
    sget-object v35, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v36, "NO fields bundle Extractor will not run"

    const/16 v37, 0x0

    move/from16 v0, v37

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v37, v0

    invoke-static/range {v35 .. v37}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 628
    :cond_0
    return-void

    .line 507
    :cond_1
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_2
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 508
    .local v14, "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    instance-of v0, v14, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    move/from16 v35, v0

    if-eqz v35, :cond_6

    const-string v35, "latitude"

    move-object/from16 v0, v35

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_6

    .line 509
    const-string v35, "location__x"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 510
    const-string v35, "location__y"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 511
    sget-object v31, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    .line 513
    .local v31, "spatialContext":Lcom/spatial4j/core/context/SpatialContext;
    new-instance v32, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    const-string v35, "location"

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    .line 515
    .local v32, "spatialStrategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    const-string v35, "location"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v35

    if-eqz v35, :cond_3

    .line 516
    const-string v35, "location"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 518
    :cond_3
    const-string v35, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v12

    .line 520
    .local v12, "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_5

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 521
    .local v25, "name":Ljava/lang/String;
    const-string v35, "root_named_location"

    move-object/from16 v0, v25

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 522
    const-string v35, ";"

    move-object/from16 v0, v25

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v35

    const/16 v36, 0x1

    aget-object v28, v35, v36

    .line 523
    .local v28, "oldLocation":Ljava/lang/String;
    const-string v35, "nameplace_to_be_removed"

    move-object/from16 v0, v35

    move-object/from16 v1, v28

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    .end local v25    # "name":Ljava/lang/String;
    .end local v28    # "oldLocation":Ljava/lang/String;
    :cond_5
    const-string v35, "latitude"

    move-object/from16 v0, v35

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v22

    .line 530
    .local v22, "lat":D
    const-string v35, "longitude"

    move-object/from16 v0, v35

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v26

    .line 531
    .local v26, "lon":D
    move-object/from16 v0, v31

    move-wide/from16 v1, v26

    move-wide/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v30

    .line 532
    .local v30, "point":Lcom/spatial4j/core/shape/Shape;
    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/apache/lucene/spatial/SpatialStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;

    move-result-object v6

    .local v6, "arr$":[Lorg/apache/lucene/document/Field;
    array-length v0, v6

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v19, 0x0

    .local v19, "i$":I
    :goto_1
    move/from16 v0, v19

    move/from16 v1, v24

    if-ge v0, v1, :cond_6

    aget-object v16, v6, v19

    .line 533
    .local v16, "f":Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 532
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 536
    .end local v6    # "arr$":[Lorg/apache/lucene/document/Field;
    .end local v12    # "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v16    # "f":Lorg/apache/lucene/index/IndexableField;
    .end local v19    # "i$":I
    .end local v22    # "lat":D
    .end local v24    # "len$":I
    .end local v26    # "lon":D
    .end local v30    # "point":Lcom/spatial4j/core/shape/Shape;
    .end local v31    # "spatialContext":Lcom/spatial4j/core/context/SpatialContext;
    .end local v32    # "spatialStrategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    :cond_6
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 538
    .local v8, "data":Ljava/lang/Object;
    instance-of v0, v8, Landroid/os/Bundle;

    move/from16 v35, v0

    if-eqz v35, :cond_2

    move-object/from16 v20, v8

    .line 539
    check-cast v20, Landroid/os/Bundle;

    .line 542
    .local v20, "inBundle":Landroid/os/Bundle;
    const-string v35, "nameplace_to_be_added"

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 543
    const-string v35, "nameplace_to_be_added"

    const-string v36, "nameplace_to_be_added"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 548
    :cond_7
    const-string v35, "EVENT_LIST"

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_b

    .line 549
    const-string v35, "EVENT_LIST"

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 552
    .local v11, "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v35, "calendar_event"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 553
    const-string v35, "event_uri"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 555
    const-string v35, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v12

    .line 558
    .restart local v12    # "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v35

    if-lez v35, :cond_9

    .line 559
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .line 560
    .local v21, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_8
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_9

    .line 561
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 562
    .local v7, "category":Ljava/lang/String;
    const-string v35, "root_calendar_event"

    move-object/from16 v0, v35

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 564
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 571
    .end local v7    # "category":Ljava/lang/String;
    .end local v21    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_9
    if-eqz v11, :cond_a

    .line 572
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v35

    move/from16 v0, v17

    move/from16 v1, v35

    if-ge v0, v1, :cond_a

    .line 573
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 574
    .local v10, "event":Ljava/lang/String;
    const-string v35, " "

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 575
    .local v9, "delimPosition":I
    const/16 v35, 0x0

    move/from16 v0, v35

    invoke-virtual {v10, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v34

    .line 576
    .local v34, "uri":Ljava/lang/String;
    add-int/lit8 v35, v9, 0x1

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v33

    .line 577
    .local v33, "title":Ljava/lang/String;
    new-instance v35, Lorg/apache/lucene/document/StringField;

    const-string v36, "calendar_event"

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v37

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v35 .. v38}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 579
    new-instance v35, Lorg/apache/lucene/document/TextField;

    const-string v36, "event_uri"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v35 .. v38}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 581
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "root_calendar_event;"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-interface {v12, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 572
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 586
    .end local v9    # "delimPosition":I
    .end local v10    # "event":Ljava/lang/String;
    .end local v17    # "i":I
    .end local v33    # "title":Ljava/lang/String;
    .end local v34    # "uri":Ljava/lang/String;
    :cond_a
    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v35

    if-lez v35, :cond_2

    .line 587
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 588
    const-string v35, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 589
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 590
    .restart local v7    # "category":Ljava/lang/String;
    new-instance v35, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v36, ";"

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p3

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 591
    new-instance v35, Lorg/apache/lucene/document/StoredField;

    const-string v36, "categories"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_4

    .line 594
    .end local v7    # "category":Ljava/lang/String;
    .end local v11    # "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v19    # "i$":Ljava/util/Iterator;
    :cond_b
    const-string v35, "PERSON_NAME_LIST"

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_2

    .line 595
    const-string v35, "PERSON_NAME_LIST"

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v29

    .line 596
    .local v29, "personNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v35, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v12

    .line 598
    .restart local v12    # "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v35, "person_names"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 599
    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v35

    if-lez v35, :cond_d

    .line 600
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .line 601
    .restart local v21    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_c
    :goto_5
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_d

    .line 602
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 603
    .restart local v7    # "category":Ljava/lang/String;
    const-string v35, "root_person"

    move-object/from16 v0, v35

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 605
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .line 609
    .end local v7    # "category":Ljava/lang/String;
    .end local v21    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_d
    if-eqz v29, :cond_e

    .line 610
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .restart local v19    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_e

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 611
    .restart local v25    # "name":Ljava/lang/String;
    new-instance v35, Lorg/apache/lucene/document/StringField;

    const-string v36, "person_names"

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v37

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v35 .. v38}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 612
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "root_person;"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-interface {v12, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 617
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v25    # "name":Ljava/lang/String;
    :cond_e
    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v35

    if-lez v35, :cond_2

    .line 618
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 619
    const-string v35, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 620
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .restart local v19    # "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 621
    .restart local v7    # "category":Ljava/lang/String;
    new-instance v35, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v36, ";"

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p3

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 622
    new-instance v35, Lorg/apache/lucene/document/StoredField;

    const-string v36, "categories"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_7
.end method

.method private applyBulkDelete(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/document/Document;JLjava/util/ArrayList;)J
    .locals 7
    .param p1, "newDocument"    # Lorg/apache/lucene/document/Document;
    .param p2, "oldDocument"    # Lorg/apache/lucene/document/Document;
    .param p3, "searchId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Lorg/apache/lucene/document/Document;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .local p5, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    const/4 v4, 0x0

    .line 718
    invoke-static {p1}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 719
    invoke-virtual {p5}, Ljava/util/ArrayList;->clear()V

    .line 720
    new-array v0, v4, [Z

    invoke-static {p2, p5, p1, v0}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 722
    const-string v0, "deleted"

    invoke-virtual {p1, v0}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 723
    new-instance v0, Lorg/apache/lucene/document/IntField;

    const-string v1, "deleted"

    const/4 v2, 0x1

    sget-object v3, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 725
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v2, p1

    move-object v3, p5

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    move-result-wide v0

    return-wide v0
.end method

.method private buildQueryDocsOnSdCard(I)Lorg/apache/lucene/search/BooleanQuery;
    .locals 5
    .param p1, "storageid"    # I

    .prologue
    const/4 v4, 0x1

    .line 1049
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 1051
    .local v0, "query":Lorg/apache/lucene/search/BooleanQuery;
    const-string v1, "storage_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3, v4, v4}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 1054
    return-object v0
.end method

.method private buildQueryMatchingDocs(Landroid/net/Uri;Z)Lorg/apache/lucene/search/BooleanQuery;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "deleted"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1018
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 1020
    .local v1, "query":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v3, Lorg/apache/lucene/search/TermQuery;

    new-instance v4, Lorg/apache/lucene/index/Term;

    const-string v5, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 1021
    if-eqz p2, :cond_0

    move v0, v2

    .line 1022
    .local v0, "isDeleted":I
    :goto_0
    const-string v3, "deleted"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5, v2, v2}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 1025
    return-object v1

    .line 1021
    .end local v0    # "isDeleted":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private buildQueryMatchingDocs(Ljava/lang/String;IZ)Lorg/apache/lucene/search/BooleanQuery;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fatVolumeId"    # I
    .param p3, "deletedOnly"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1030
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 1031
    .local v1, "query":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz p3, :cond_0

    move v0, v2

    .line 1033
    .local v0, "deleted":I
    :goto_0
    new-instance v3, Lorg/apache/lucene/search/TermQuery;

    new-instance v4, Lorg/apache/lucene/index/Term;

    const-string v5, "path"

    invoke-direct {v4, v5, p1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 1037
    const-string v3, "fatvolume_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5, v2, v2}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 1040
    return-object v1

    .line 1031
    .end local v0    # "deleted":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private convertAndUpdate(Landroid/os/Bundle;Lcom/samsung/dcm/documents/MediaDoc;)V
    .locals 10
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 953
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "convertAndUpdate "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/samsung/dcm/documents/MediaDoc;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 954
    new-instance v2, Lorg/apache/lucene/document/Document;

    invoke-direct {v2}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 955
    .local v2, "document":Lorg/apache/lucene/document/Document;
    invoke-static {p2}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->getConverter(Lcom/samsung/dcm/documents/MediaDoc;)Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;

    move-result-object v0

    .line 957
    .local v0, "converter":Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
    if-eqz v0, :cond_0

    .line 959
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->setFacetFields(Lorg/apache/lucene/facet/index/FacetFields;)V

    .line 960
    invoke-virtual {v0, p2, v2}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V

    .line 961
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 963
    .local v3, "categoryPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-direct {p0, p1, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->updateLuceneDoc(Landroid/os/Bundle;Lorg/apache/lucene/document/Document;Ljava/util/List;)V

    .line 965
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 967
    const-string v1, "$facets"

    invoke-virtual {v2, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 968
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    invoke-virtual {v1, v2, p2}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->addDelayedPluginInfo(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V

    .line 969
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "cateogories = "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    invoke-static {v1, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 970
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getCurrentSearchId()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    .line 989
    .end local v3    # "categoryPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_0
    :goto_0
    return-void

    .line 974
    .restart local v3    # "categoryPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_1
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "No category present"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v9, "Facet search will fail"

    aput-object v9, v5, v6

    invoke-static {v1, v4, v5}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 975
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    invoke-virtual {v1, v2, p2}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->addDelayedPluginInfo(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V

    .line 976
    new-instance v8, Lorg/apache/lucene/index/Term;

    const-string v1, "uri"

    const-string v4, "uri"

    invoke-virtual {v2, v4}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v1, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    .local v8, "term":Lorg/apache/lucene/index/Term;
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getCurrentSearchId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p2}, Lcom/samsung/dcm/documents/MediaDoc;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v8, v2, v4, v5}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Ljava/lang/Long;Ljava/lang/Integer;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 985
    .end local v3    # "categoryPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v8    # "term":Lorg/apache/lucene/index/Term;
    :catch_0
    move-exception v7

    .line 986
    .local v7, "e":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "Exception caught! "

    invoke-static {v1, v4, v7}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;
    .locals 6
    .param p1, "indexableFields"    # [Lorg/apache/lucene/index/IndexableField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/lucene/index/IndexableField;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1078
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1079
    .local v1, "fields":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object v0, p1

    .local v0, "arr$":[Lorg/apache/lucene/index/IndexableField;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v3, v0, v2

    .line 1080
    .local v3, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1079
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1083
    .end local v3    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_0
    return-object v1
.end method

.method private fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V
    .locals 2
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .param p2, "status"    # Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    .prologue
    .line 998
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker$1;->$SwitchMap$com$samsung$dcm$framework$extractormanager$task$Worker$CommandStatus:[I

    invoke-virtual {p2}, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1008
    :goto_0
    return-void

    .line 1000
    :pswitch_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto :goto_0

    .line 1003
    :pswitch_1
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandCompleted()V

    goto :goto_0

    .line 998
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hasDataBundle(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 2
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 1065
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processBulkDelete(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 30
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 638
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 639
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/dcm/framework/configuration/StorageController;->getFatVolumeId(I)I

    move-result v23

    .line 640
    .local v23, "fatVolumeId":I
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v7, "processBulkDelete: storageid="

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, " fatVolume Id="

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v6, v7, v13}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 641
    const/4 v2, 0x0

    .line 645
    .local v2, "nrtSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v2

    .line 646
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v10

    .line 647
    .local v10, "searchId":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->buildQueryDocsOnSdCard(I)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v4

    .line 649
    .local v4, "sdcardquery":Lorg/apache/lucene/search/BooleanQuery;
    const-string v6, "deleted"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v6, v7, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeFilter;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeFilter;

    move-result-object v5

    .line 651
    .local v5, "sdcardFilter":Lorg/apache/lucene/search/Filter;
    const/4 v3, 0x0

    .line 652
    .local v3, "after":Lorg/apache/lucene/search/ScoreDoc;
    const/16 v29, 0x0

    .line 653
    .local v29, "topdocs":Lorg/apache/lucene/search/TopDocs;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 654
    .local v12, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v8, Lorg/apache/lucene/document/Document;

    invoke-direct {v8}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 655
    .local v8, "newDocument":Lorg/apache/lucene/document/Document;
    const-wide/16 v20, 0x0

    .line 658
    .local v20, "commitId":J
    :cond_0
    const/16 v6, 0x3e8

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v29

    .line 659
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v7, "Hits = "

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, v29

    iget v15, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, " processing = "

    aput-object v15, v13, v14

    const/4 v14, 0x2

    move-object/from16 v0, v29

    iget-object v15, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v15, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v6, v7, v13}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 660
    move-object/from16 v0, v29

    iget-object v0, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    move-object/from16 v19, v0

    .local v19, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v26, v0

    .local v26, "len$":I
    const/16 v24, 0x0

    .local v24, "i$":I
    :goto_0
    move/from16 v0, v24

    move/from16 v1, v26

    if-ge v0, v1, :cond_8

    aget-object v28, v19, v24

    .line 661
    .local v28, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    move-object/from16 v0, v28

    iget v6, v0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v9

    .local v9, "oldDocument":Lorg/apache/lucene/document/Document;
    move-object/from16 v7, p0

    .line 663
    invoke-direct/range {v7 .. v12}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->applyBulkDelete(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/document/Document;JLjava/util/ArrayList;)J

    move-result-wide v20

    .line 664
    cmp-long v6, v20, v10

    if-eqz v6, :cond_3

    .line 665
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v7, "Colision! during bulk delete must not occur "

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v6, v7, v13}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 666
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 667
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v27

    .line 668
    .local v27, "newseracher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 671
    .local v25, "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    :try_start_1
    const-string v6, "uri"

    invoke-virtual {v9, v6}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-static {v0, v6, v1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V

    .line 675
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 676
    const/4 v6, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/document/Document;

    const-wide v16, 0x7fffffffffffffffL

    move-object/from16 v13, p0

    move-object v14, v8

    move-object/from16 v18, v12

    invoke-direct/range {v13 .. v18}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->applyBulkDelete(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/document/Document;JLjava/util/ArrayList;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 680
    :cond_1
    if-eqz v27, :cond_2

    .line 681
    :try_start_2
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 683
    :cond_2
    const/16 v27, 0x0

    .line 688
    .end local v25    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v27    # "newseracher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :cond_3
    move-object/from16 v3, v28

    .line 690
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->isFinishedNow()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    if-eqz v6, :cond_7

    .line 708
    if-eqz v2, :cond_4

    .line 709
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 711
    :cond_4
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    .line 713
    .end local v3    # "after":Lorg/apache/lucene/search/ScoreDoc;
    .end local v4    # "sdcardquery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v5    # "sdcardFilter":Lorg/apache/lucene/search/Filter;
    .end local v8    # "newDocument":Lorg/apache/lucene/document/Document;
    .end local v9    # "oldDocument":Lorg/apache/lucene/document/Document;
    .end local v10    # "searchId":J
    .end local v12    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v19    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v20    # "commitId":J
    .end local v24    # "i$":I
    .end local v26    # "len$":I
    .end local v28    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v29    # "topdocs":Lorg/apache/lucene/search/TopDocs;
    :goto_1
    return-void

    .line 680
    .restart local v3    # "after":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v4    # "sdcardquery":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v5    # "sdcardFilter":Lorg/apache/lucene/search/Filter;
    .restart local v8    # "newDocument":Lorg/apache/lucene/document/Document;
    .restart local v9    # "oldDocument":Lorg/apache/lucene/document/Document;
    .restart local v10    # "searchId":J
    .restart local v12    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .restart local v19    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v20    # "commitId":J
    .restart local v24    # "i$":I
    .restart local v25    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v26    # "len$":I
    .restart local v27    # "newseracher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v28    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v29    # "topdocs":Lorg/apache/lucene/search/TopDocs;
    :catchall_0
    move-exception v6

    if-eqz v27, :cond_5

    .line 681
    :try_start_3
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 683
    :cond_5
    const/16 v27, 0x0

    throw v6
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 703
    .end local v3    # "after":Lorg/apache/lucene/search/ScoreDoc;
    .end local v4    # "sdcardquery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v5    # "sdcardFilter":Lorg/apache/lucene/search/Filter;
    .end local v8    # "newDocument":Lorg/apache/lucene/document/Document;
    .end local v9    # "oldDocument":Lorg/apache/lucene/document/Document;
    .end local v10    # "searchId":J
    .end local v12    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v19    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v20    # "commitId":J
    .end local v24    # "i$":I
    .end local v25    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v26    # "len$":I
    .end local v27    # "newseracher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v28    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v29    # "topdocs":Lorg/apache/lucene/search/TopDocs;
    :catch_0
    move-exception v22

    .line 704
    .local v22, "e":Ljava/io/IOException;
    :try_start_4
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v7, "IOException caught during bulk delete!: "

    move-object/from16 v0, v22

    invoke-static {v6, v7, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 708
    if-eqz v2, :cond_6

    .line 709
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 711
    :cond_6
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto :goto_1

    .line 660
    .end local v22    # "e":Ljava/io/IOException;
    .restart local v3    # "after":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v4    # "sdcardquery":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v5    # "sdcardFilter":Lorg/apache/lucene/search/Filter;
    .restart local v8    # "newDocument":Lorg/apache/lucene/document/Document;
    .restart local v9    # "oldDocument":Lorg/apache/lucene/document/Document;
    .restart local v10    # "searchId":J
    .restart local v12    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .restart local v19    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v20    # "commitId":J
    .restart local v24    # "i$":I
    .restart local v26    # "len$":I
    .restart local v28    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v29    # "topdocs":Lorg/apache/lucene/search/TopDocs;
    :cond_7
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_0

    .line 695
    .end local v9    # "oldDocument":Lorg/apache/lucene/document/Document;
    .end local v28    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_8
    :try_start_5
    move-object/from16 v0, v29

    iget-object v6, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v6, v6

    const/16 v7, 0x3e8

    if-ge v6, v7, :cond_9

    .line 696
    const/4 v3, 0x0

    .line 698
    :cond_9
    if-nez v3, :cond_0

    .line 701
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 708
    if-eqz v2, :cond_a

    .line 709
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 711
    :cond_a
    sget-object v6, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto :goto_1

    .line 708
    .end local v3    # "after":Lorg/apache/lucene/search/ScoreDoc;
    .end local v4    # "sdcardquery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v5    # "sdcardFilter":Lorg/apache/lucene/search/Filter;
    .end local v8    # "newDocument":Lorg/apache/lucene/document/Document;
    .end local v10    # "searchId":J
    .end local v12    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v19    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v20    # "commitId":J
    .end local v24    # "i$":I
    .end local v26    # "len$":I
    .end local v29    # "topdocs":Lorg/apache/lucene/search/TopDocs;
    :catchall_1
    move-exception v6

    if-eqz v2, :cond_b

    .line 709
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 711
    :cond_b
    sget-object v7, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    throw v6
.end method

.method private processBulkInsert(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 27
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 181
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 182
    .local v22, "start":J
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v8

    invoke-virtual {v3, v8}, Lcom/samsung/dcm/framework/configuration/StorageController;->getFatVolumeId(I)I

    move-result v18

    .line 183
    .local v18, "fatVolumeId":I
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "Processing BULK Insert command: "

    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v24, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x1

    const-string v25, " fat volume Id:"

    aput-object v25, v21, v24

    const/16 v24, 0x2

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v21, v24

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    const/16 v20, 0x0

    .line 186
    .local v20, "searcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    const/16 v19, 0x0

    .line 190
    .local v19, "processedBulk":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v20

    .line 191
    if-nez v20, :cond_1

    .line 192
    new-instance v3, Lcom/samsung/dcm/framework/exceptions/UpdatingException;

    const-string v8, "Searcher is null"

    invoke-direct {v3, v8}, Lcom/samsung/dcm/framework/exceptions/UpdatingException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :catch_0
    move-exception v14

    .line 260
    .local v14, "e":Lcom/samsung/dcm/framework/exceptions/UpdatingException;
    :try_start_1
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "UpdatingException caught during BULK indexing! "

    invoke-static {v3, v8, v14}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    if-eqz v20, :cond_0

    .line 265
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 269
    .end local v14    # "e":Lcom/samsung/dcm/framework/exceptions/UpdatingException;
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    sub-long v16, v24, v22

    .line 270
    .local v16, "end":J
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "[DCM_Performance] Real processBulkInsert : ["

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v24, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x1

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "]ms processedBulk = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v21, v24

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    return v19

    .line 195
    .end local v16    # "end":J
    :cond_1
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getPath()Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v3, v1, v8}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->buildQueryMatchingDocs(Ljava/lang/String;IZ)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v3

    const/4 v8, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v3, v8}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->runLuceneQuery(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lorg/apache/lucene/search/BooleanQuery;I)Ljava/util/List;

    move-result-object v13

    .line 199
    .local v13, "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v6

    .line 200
    .local v6, "searchId":J
    const-wide/16 v10, 0x0

    .line 203
    .local v10, "commitId":J
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "BULK insert found "

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v24, 0x0

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x1

    const-string v25, " documents"

    aput-object v25, v21, v24

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 207
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v5, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v4, Lorg/apache/lucene/document/Document;

    invoke-direct {v4}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 209
    .local v4, "document":Lorg/apache/lucene/document/Document;
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/document/Document;

    const/4 v8, 0x0

    new-array v8, v8, [Z

    invoke-static {v3, v5, v4, v8}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 211
    const/4 v9, 0x0

    .line 212
    .local v9, "data":Landroid/os/Bundle;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->hasDataBundle(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 213
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "Command has data bundle, it means that update has come"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    sget-object v8, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    .line 227
    :goto_1
    const-string v3, "deleted"

    const/4 v8, 0x0

    invoke-virtual {v9, v3, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 228
    const-string v3, "uri"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v3, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v3, "id"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v24

    move-wide/from16 v0, v24

    long-to-int v8, v0

    invoke-virtual {v9, v3, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 232
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->updateLuceneDoc(Landroid/os/Bundle;Lorg/apache/lucene/document/Document;Ljava/util/List;)V

    .line 235
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 236
    .local v15, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v3

    const-class v8, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;

    invoke-virtual {v3, v8}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 237
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v3

    const-class v8, Lcom/samsung/dcm/framework/extractors/PersonNameExtractor;

    invoke-virtual {v3, v8}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 238
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/dcm/framework/extractormanager/Command;->setExtractors(Ljava/util/Set;)V

    .line 240
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 241
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->addDataFromExtractors(Lcom/samsung/dcm/framework/extractormanager/Command;Lorg/apache/lucene/document/Document;Ljava/util/List;)V

    .line 242
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "processBulkInsert: "

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v24, 0x0

    aput-object v4, v21, v24

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    move-result-wide v10

    .line 248
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "Updating: commitId="

    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v24, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x1

    const-string v25, ", searchId="

    aput-object v25, v21, v24

    const/16 v24, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v21, v24

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    cmp-long v3, v10, v6

    if-eqz v3, :cond_2

    .line 250
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "Colision! Reupdating..."

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v3, v8, v0}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    .line 254
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 255
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V
    :try_end_2
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 256
    const/16 v19, 0x1

    .line 264
    .end local v4    # "document":Lorg/apache/lucene/document/Document;
    .end local v5    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v9    # "data":Landroid/os/Bundle;
    .end local v15    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    :cond_3
    if-eqz v20, :cond_0

    .line 265
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_0

    .line 217
    .restart local v4    # "document":Lorg/apache/lucene/document/Document;
    .restart local v5    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .restart local v9    # "data":Landroid/os/Bundle;
    :cond_4
    :try_start_3
    new-instance v9, Landroid/os/Bundle;

    .end local v9    # "data":Landroid/os/Bundle;
    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 220
    .restart local v9    # "data":Landroid/os/Bundle;
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 221
    .local v12, "dataBundle":Landroid/os/Bundle;
    const-string v3, "CALENDAR_RESYNC"

    const/4 v8, 0x1

    invoke-virtual {v12, v3, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 222
    const-string v3, "PERSON_NAME_RESYNC"

    const/4 v8, 0x1

    invoke-virtual {v12, v3, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 223
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 224
    .local v2, "bundle":Landroid/os/Bundle;
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v12}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 225
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->setData(Landroid/os/Bundle;)V
    :try_end_3
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 261
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v4    # "document":Lorg/apache/lucene/document/Document;
    .end local v5    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v6    # "searchId":J
    .end local v9    # "data":Landroid/os/Bundle;
    .end local v10    # "commitId":J
    .end local v12    # "dataBundle":Landroid/os/Bundle;
    .end local v13    # "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    :catch_1
    move-exception v14

    .line 262
    .local v14, "e":Ljava/io/IOException;
    :try_start_4
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v8, "IOException caught during BULK indexing! "

    invoke-static {v3, v8, v14}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 264
    if-eqz v20, :cond_0

    .line 265
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_0

    .line 264
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v20, :cond_5

    .line 265
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_5
    throw v3
.end method

.method private processDelete(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 14
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 735
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 737
    .local v8, "start":J
    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getCurrentSearchId()J

    move-result-wide v6

    .line 738
    .local v6, "searchId":J
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->deleteDoc(Landroid/net/Uri;Ljava/lang/Long;)J

    move-result-wide v0

    .line 739
    .local v0, "commitId":J
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v10, "Deleting: commitId="

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string v13, ", genId="

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v3, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 743
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    invoke-direct {p0, p1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    .line 745
    .end local v0    # "commitId":J
    .end local v6    # "searchId":J
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 746
    .local v4, "end":J
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v10, "[DCM_Performance] Real processDelete : ["

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string v13, "]ms"

    aput-object v13, v11, v12

    invoke-static {v3, v10, v11}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 747
    return-void

    .line 740
    .end local v4    # "end":J
    :catch_0
    move-exception v2

    .line 741
    .local v2, "e":Lcom/samsung/dcm/framework/exceptions/UpdatingException;
    :try_start_1
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v10, "Exception caught while deleting documents!: "

    invoke-static {v3, v10, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 743
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    invoke-direct {p0, p1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto :goto_0

    .end local v2    # "e":Lcom/samsung/dcm/framework/exceptions/UpdatingException;
    :catchall_0
    move-exception v3

    sget-object v10, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    invoke-direct {p0, p1, v10}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    throw v3
.end method

.method private processInsert(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 22
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 285
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 286
    .local v16, "start":J
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "Processing Insert command: "

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object p1, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->scan(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v12

    .line 290
    .local v12, "mediaDoc":Lcom/samsung/dcm/documents/MediaDoc;
    if-nez v12, :cond_0

    .line 291
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "Scanning failed, aborting command processing"

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    .line 391
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v13

    invoke-virtual {v12}, Lcom/samsung/dcm/documents/MediaDoc;->getStorageId()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/dcm/framework/configuration/StorageController;->getFatVolumeId(I)I

    move-result v10

    .line 299
    .local v10, "fatVolumeId":I
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "Storing sdcard Id in DB"

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, " for path:"

    aput-object v21, v19, v20

    const/16 v20, 0x2

    invoke-virtual {v12}, Lcom/samsung/dcm/documents/MediaDoc;->getPath()Lcom/google/common/base/Optional;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    invoke-virtual {v12, v10}, Lcom/samsung/dcm/documents/MediaDoc;->setFatVolumeId(I)V

    .line 302
    sget-object v18, Lcom/samsung/dcm/framework/extractormanager/task/Worker$1;->$SwitchMap$com$samsung$dcm$documents$MediaDoc$DocType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDocType()Lcom/google/common/base/Optional;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v13}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v13

    aget v13, v18, v13

    packed-switch v13, :pswitch_data_0

    goto :goto_0

    .line 304
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getExtractors()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 305
    .local v7, "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInterrupted()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 306
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto :goto_0

    .line 309
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->maybePause()V

    move-object v13, v12

    .line 310
    check-cast v13, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v7, v13}, Lcom/samsung/dcm/framework/extractors/Extractor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 315
    .end local v7    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v11    # "i$":Ljava/util/Iterator;
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getExtractors()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 316
    .restart local v7    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInterrupted()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 317
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto/16 :goto_0

    .line 320
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->maybePause()V

    move-object v13, v12

    .line 321
    check-cast v13, Lcom/samsung/dcm/documents/AudioMediaDoc;

    invoke-virtual {v7, v13}, Lcom/samsung/dcm/framework/extractors/Extractor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 326
    .end local v7    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v11    # "i$":Ljava/util/Iterator;
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getExtractors()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 327
    .restart local v7    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInterrupted()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 328
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto/16 :goto_0

    .line 331
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->maybePause()V

    move-object v13, v12

    .line 332
    check-cast v13, Lcom/samsung/dcm/documents/VideoMediaDoc;

    invoke-virtual {v7, v13}, Lcom/samsung/dcm/framework/extractors/Extractor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 345
    .end local v7    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    :cond_4
    const-string v5, "root_metadata;DUMMY"

    .line 347
    .local v5, "dummyCategory":Ljava/lang/String;
    invoke-virtual {v12, v5}, Lcom/samsung/dcm/documents/MediaDoc;->addCategory(Ljava/lang/String;)V

    .line 348
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "added dummy CATEGORY_ROOT_METADATA"

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 358
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->hasDataBundle(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 359
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "Converting and updating mediaDoc"

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/Bundle;

    sget-object v18, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 362
    .local v4, "data":Landroid/os/Bundle;
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v12}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertAndUpdate(Landroid/os/Bundle;Lcom/samsung/dcm/documents/MediaDoc;)V
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/IndexingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_1

    .line 383
    .end local v4    # "data":Landroid/os/Bundle;
    :cond_5
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInterrupted()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 384
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    .line 389
    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v8, v18, v16

    .line 390
    .local v8, "end":J
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "[DCM_Performance] Real processInsert : ["

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, "]ms"

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 365
    .end local v8    # "end":J
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v13}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getCurrentSearchId()J

    move-result-wide v14

    .line 366
    .local v14, "searchId":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v12, v0}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->indexDocument(Lcom/samsung/dcm/documents/MediaDoc;Ljava/lang/Long;)J

    move-result-wide v2

    .line 367
    .local v2, "commitId":J
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "Indexing: commitId="

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, ", genId="

    aput-object v21, v19, v20

    const/16 v20, 0x2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 369
    cmp-long v13, v2, v14

    if-eqz v13, :cond_5

    .line 370
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "Collision! Reindexing: commitId="

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, ", searchId="

    aput-object v21, v19, v20

    const/16 v20, 0x2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v13, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mIndexer:Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v12, v0}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->indexDocument(Lcom/samsung/dcm/documents/MediaDoc;Ljava/lang/Long;)J
    :try_end_1
    .catch Lcom/samsung/dcm/framework/exceptions/IndexingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_4

    .line 375
    .end local v2    # "commitId":J
    .end local v14    # "searchId":J
    :catch_0
    move-exception v6

    .line 376
    .local v6, "e":Lcom/samsung/dcm/framework/exceptions/IndexingException;
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "IndexingException caught! "

    move-object/from16 v0, v18

    invoke-static {v13, v0, v6}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 377
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->interrupt()V

    goto/16 :goto_4

    .line 378
    .end local v6    # "e":Lcom/samsung/dcm/framework/exceptions/IndexingException;
    :catch_1
    move-exception v6

    .line 379
    .local v6, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v18, "processInsert"

    move-object/from16 v0, v18

    invoke-static {v13, v0, v6}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 380
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->interrupt()V

    goto/16 :goto_4

    .line 386
    .end local v6    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :cond_7
    sget-object v13, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    goto/16 :goto_5

    .line 302
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private processUpdate(Lcom/samsung/dcm/framework/extractormanager/Command;J)V
    .locals 26
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .param p2, "searchId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 402
    .local v22, "start":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getExtractors()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    const/16 v24, 0x1

    .line 403
    .local v24, "useExtractors":Z
    :goto_0
    const/16 v20, 0x0

    .line 405
    .local v20, "searcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->hasDataBundle(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 406
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    sget-object v6, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 408
    .local v2, "data":Landroid/os/Bundle;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "Updating document with data - "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v20

    .line 411
    if-nez v20, :cond_2

    .line 412
    new-instance v3, Lcom/samsung/dcm/framework/exceptions/UpdatingException;

    const-string v6, "Searcher is null"

    invoke-direct {v3, v6}, Lcom/samsung/dcm/framework/exceptions/UpdatingException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    .end local v2    # "data":Landroid/os/Bundle;
    :catch_0
    move-exception v13

    .line 475
    .local v13, "e":Lcom/samsung/dcm/framework/exceptions/UpdatingException;
    :try_start_1
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "UpdatingException caught! "

    invoke-static {v3, v6, v13}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480
    if-eqz v20, :cond_0

    .line 481
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 483
    :cond_0
    const/16 v20, 0x0

    .line 485
    .end local v13    # "e":Lcom/samsung/dcm/framework/exceptions/UpdatingException;
    :goto_1
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->COMPLETED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    .line 487
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v14, v6, v22

    .line 488
    .local v14, "end":J
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real processUpdate : ["

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v7, v8

    const/4 v8, 0x1

    const-string v25, "]ms"

    aput-object v25, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    .end local v14    # "end":J
    :goto_2
    return-void

    .line 402
    .end local v20    # "searcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v24    # "useExtractors":Z
    :cond_1
    const/16 v24, 0x0

    goto :goto_0

    .line 414
    .restart local v2    # "data":Landroid/os/Bundle;
    .restart local v20    # "searcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v24    # "useExtractors":Z
    :cond_2
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v3

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->buildQueryMatchingDocs(Landroid/net/Uri;Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v3

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v3, v6}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->runLuceneQuery(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lorg/apache/lucene/search/BooleanQuery;I)Ljava/util/List;

    move-result-object v12

    .line 418
    .local v12, "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "Updating: documentsToUpdate ="

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v6

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    .line 421
    const-wide/16 v10, -0x1

    .line 423
    .local v10, "commitId":J
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/document/Document;

    .line 424
    .local v19, "iterDoc":Lorg/apache/lucene/document/Document;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 425
    .local v5, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v4, Lorg/apache/lucene/document/Document;

    invoke-direct {v4}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 426
    .local v4, "document":Lorg/apache/lucene/document/Document;
    const/4 v3, 0x0

    new-array v3, v3, [Z

    move-object/from16 v0, v19

    invoke-static {v0, v5, v4, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 427
    const-string v3, "doctype"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v9

    .line 428
    .local v9, "docType":I
    const-string v3, "OSC_SCENETYPE_TO_BE_ADDED"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v3}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v3

    if-ne v9, v3, :cond_4

    .line 431
    const/16 v16, -0x1

    .line 432
    .local v16, "faceCount":I
    const-string v3, "faceCount"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v17

    .line 433
    .local v17, "faceCountField":Lorg/apache/lucene/index/IndexableField;
    if-eqz v17, :cond_3

    .line 434
    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v16

    .line 436
    :cond_3
    const/4 v3, -0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_4

    .line 437
    const-string v3, "uri"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v21

    .line 438
    .local v21, "uriValue":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mScanner:Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;

    sget-object v6, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0, v6}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner;->scanForFaceCount(Ljava/lang/String;Lcom/samsung/dcm/documents/MediaDoc$DocType;)I

    move-result v16

    .line 439
    const-string v3, "faceCount"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 440
    new-instance v3, Lorg/apache/lucene/document/IntField;

    const-string v6, "faceCount"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move/from16 v0, v16

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 445
    .end local v16    # "faceCount":I
    .end local v17    # "faceCountField":Lorg/apache/lucene/index/IndexableField;
    .end local v21    # "uriValue":Ljava/lang/String;
    :cond_4
    if-eqz v24, :cond_5

    .line 446
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->addDataFromExtractors(Lcom/samsung/dcm/framework/extractormanager/Command;Lorg/apache/lucene/document/Document;Ljava/util/List;)V

    .line 448
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->updateLuceneDoc(Landroid/os/Bundle;Lorg/apache/lucene/document/Document;Ljava/util/List;)V

    .line 449
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "processUpdate: "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInterrupted()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 452
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;->ABORTED:Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->fireCommandCallbacks(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/task/Worker$CommandStatus;)V

    .line 453
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "processUpdate interrupted"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 480
    if-eqz v20, :cond_6

    .line 481
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 483
    :cond_6
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 457
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-wide/from16 v6, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    move-result-wide v10

    .line 459
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "Updating: commitId="

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v7, v8

    const/4 v8, 0x1

    const-string v25, ", searchId="

    aput-object v25, v7, v8

    const/4 v8, 0x2

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 476
    .end local v2    # "data":Landroid/os/Bundle;
    .end local v4    # "document":Lorg/apache/lucene/document/Document;
    .end local v5    # "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    .end local v9    # "docType":I
    .end local v10    # "commitId":J
    .end local v12    # "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v19    # "iterDoc":Lorg/apache/lucene/document/Document;
    :catch_1
    move-exception v13

    .line 477
    .local v13, "e":Ljava/io/IOException;
    :try_start_4
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "IOException caught! "

    invoke-static {v3, v6, v13}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 480
    if-eqz v20, :cond_8

    .line 481
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 483
    :cond_8
    const/16 v20, 0x0

    .line 484
    goto/16 :goto_1

    .line 465
    .end local v13    # "e":Ljava/io/IOException;
    .restart local v2    # "data":Landroid/os/Bundle;
    .restart local v10    # "commitId":J
    .restart local v12    # "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    .restart local v18    # "i$":Ljava/util/Iterator;
    :cond_9
    cmp-long v3, v10, p2

    if-eqz v3, :cond_b

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v3, p2, v6

    if-eqz v3, :cond_b

    .line 466
    :try_start_5
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v6, "Colision! Reupdating..."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 467
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 468
    const-wide v6, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->processUpdate(Lcom/samsung/dcm/framework/extractormanager/Command;J)V
    :try_end_5
    .catch Lcom/samsung/dcm/framework/exceptions/UpdatingException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 480
    if-eqz v20, :cond_a

    .line 481
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 483
    :cond_a
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 480
    .end local v2    # "data":Landroid/os/Bundle;
    .end local v10    # "commitId":J
    .end local v12    # "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    .end local v18    # "i$":Ljava/util/Iterator;
    :cond_b
    if-eqz v20, :cond_c

    .line 481
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 483
    :cond_c
    const/16 v20, 0x0

    .line 484
    goto/16 :goto_1

    .line 480
    :catchall_0
    move-exception v3

    if-eqz v20, :cond_d

    .line 481
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 483
    :cond_d
    const/16 v20, 0x0

    throw v3
.end method

.method private runLuceneQuery(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Lorg/apache/lucene/search/BooleanQuery;I)Ljava/util/List;
    .locals 14
    .param p1, "indexSearcher"    # Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .param p2, "query"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p3, "max_expected"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;",
            "Lorg/apache/lucene/search/BooleanQuery;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 751
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 753
    .local v8, "documents":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    const/4 v13, 0x0

    .line 755
    .local v13, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object/from16 v2, p2

    move/from16 v4, p3

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v13

    .line 756
    iget-object v6, v13, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v6, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v11, v6

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v11, :cond_0

    aget-object v12, v6, v10

    .line 757
    .local v12, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget v0, v12, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v7

    .line 758
    .local v7, "document":Lorg/apache/lucene/document/Document;
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 756
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 760
    .end local v6    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v7    # "document":Lorg/apache/lucene/document/Document;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v12    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catch_0
    move-exception v9

    .line 761
    .local v9, "e":Ljava/io/IOException;
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v1, "Exception caught! "

    invoke-static {v0, v1, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 766
    .end local v9    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    return-object v8

    .line 762
    :catch_1
    move-exception v9

    .line 763
    .local v9, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v1, "SearchingException caught! "

    invoke-static {v0, v1, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private updateLuceneDoc(Landroid/os/Bundle;Lorg/apache/lucene/document/Document;Ljava/util/List;)V
    .locals 40
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 778
    .local p3, "categories":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    const-string v36, "facetags_to_be_added"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 780
    .local v15, "facetagsToBeAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v36, "facetags_to_be_removed"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    .line 782
    .local v16, "facetagsToBeRemoved":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v36, "OSC_SCENETYPE_TO_BE_ADDED"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v30

    .line 784
    .local v30, "scenetypeToBeAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v36, "OSC_SUBSCENETYPE_TO_BE_ADDED"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v33

    .line 787
    .local v33, "subscenetypeToBeAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v36, "nameplace_to_be_added"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 788
    .local v27, "namePlace":Ljava/lang/String;
    const-string v36, "nameplace_to_be_removed"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 789
    .local v28, "oldNamePlace":Ljava/lang/String;
    const-string v36, "file_path"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 790
    .local v19, "filePath":Ljava/lang/String;
    const-string v36, "latitude"

    const-wide/16 v38, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    move-wide/from16 v2, v38

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v22

    .line 791
    .local v22, "latitude":D
    const-string v36, "longitude"

    const-wide/16 v38, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    move-wide/from16 v2, v38

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v24

    .line 792
    .local v24, "longitude":D
    const-string v36, "deleted"

    const/16 v37, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 793
    .local v8, "deleted":I
    const-string v36, "uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 794
    .local v35, "uri":Ljava/lang/String;
    const-string v36, "id"

    const/16 v37, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v17

    .line 796
    .local v17, "field_id":I
    const-string v36, "person_names"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v11

    .line 798
    .local v11, "existingPersonNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v36, "scene_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v12

    .line 800
    .local v12, "existingSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v36, "subscene_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v13

    .line 802
    .local v13, "existingSubsceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v36, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v10

    .line 805
    .local v10, "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v16, :cond_1

    .line 806
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_1

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 807
    .local v34, "tag":Ljava/lang/String;
    const/16 v26, 0x0

    .line 808
    .local v26, "name":Ljava/lang/String;
    const-string v36, "profile"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_0

    .line 809
    move-object/from16 v26, v34

    .line 813
    :goto_1
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_person;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 815
    const-string v36, "/"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    const/16 v37, 0x1

    aget-object v36, v36, v37

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v11, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 811
    :cond_0
    const-string v36, "/"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    const/16 v37, 0x1

    aget-object v26, v36, v37

    goto :goto_1

    .line 819
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v26    # "name":Ljava/lang/String;
    .end local v34    # "tag":Ljava/lang/String;
    :cond_1
    if-eqz v15, :cond_3

    .line 820
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .restart local v20    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_3

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 821
    .restart local v34    # "tag":Ljava/lang/String;
    const/16 v26, 0x0

    .line 822
    .restart local v26    # "name":Ljava/lang/String;
    const-string v36, "profile"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_2

    .line 823
    move-object/from16 v26, v34

    .line 827
    :goto_3
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_person;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 829
    const-string v36, "/"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    const/16 v37, 0x1

    aget-object v36, v36, v37

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v11, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 825
    :cond_2
    const-string v36, "/"

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    const/16 v37, 0x1

    aget-object v26, v36, v37

    goto :goto_3

    .line 833
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v26    # "name":Ljava/lang/String;
    .end local v34    # "tag":Ljava/lang/String;
    :cond_3
    if-eqz v30, :cond_5

    .line 834
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .restart local v20    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_5

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 835
    .restart local v34    # "tag":Ljava/lang/String;
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_scene_type;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v36

    if-nez v36, :cond_4

    .line 837
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_scene_type;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 840
    :cond_4
    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v12, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 844
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v34    # "tag":Ljava/lang/String;
    :cond_5
    if-eqz v33, :cond_7

    .line 845
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .restart local v20    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_7

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 846
    .restart local v34    # "tag":Ljava/lang/String;
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_subscene_type;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v36

    if-nez v36, :cond_6

    .line 848
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_subscene_type;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 851
    :cond_6
    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 854
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v34    # "tag":Ljava/lang/String;
    :cond_7
    if-eqz v28, :cond_8

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->isEmpty()Z

    move-result v36

    if-nez v36, :cond_8

    .line 855
    const-string v36, "named_location"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 856
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_named_location;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 860
    :cond_8
    if-eqz v27, :cond_9

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->isEmpty()Z

    move-result v36

    if-nez v36, :cond_9

    .line 861
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "root_named_location;"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 865
    :cond_9
    if-eqz v19, :cond_a

    .line 866
    const/16 v36, 0x2f

    move-object/from16 v0, v19

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    .line 867
    .local v9, "delimPosition":I
    add-int/lit8 v36, v9, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    .line 868
    .local v18, "fileName":Ljava/lang/String;
    const-string v36, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 869
    const-string v36, "title_lower"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 870
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "title"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v18

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 871
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "title_lower"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v38

    sget-object v39, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v36 .. v39}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 873
    const-string v36, "path"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 874
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "path"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v19

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 877
    .end local v9    # "delimPosition":I
    .end local v18    # "fileName":Ljava/lang/String;
    :cond_a
    const-wide/16 v36, 0x0

    cmpl-double v36, v22, v36

    if-eqz v36, :cond_d

    const-wide/16 v36, 0x0

    cmpl-double v36, v24, v36

    if-eqz v36, :cond_d

    .line 878
    const-string v36, "location__x"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 879
    const-string v36, "location__y"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 880
    sget-object v31, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    .line 882
    .local v31, "spatialContext":Lcom/spatial4j/core/context/SpatialContext;
    new-instance v32, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    const-string v36, "location"

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    .line 884
    .local v32, "spatialStrategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    const-string v36, "location"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v36

    if-eqz v36, :cond_b

    .line 885
    const-string v36, "location"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 887
    :cond_b
    move-object/from16 v0, v31

    move-wide/from16 v1, v24

    move-wide/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v29

    .line 888
    .local v29, "point":Lcom/spatial4j/core/shape/Shape;
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/apache/lucene/spatial/SpatialStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;

    move-result-object v6

    .local v6, "arr$":[Lorg/apache/lucene/document/Field;
    array-length v0, v6

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v20, 0x0

    .local v20, "i$":I
    :goto_6
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_c

    aget-object v14, v6, v20

    .line 889
    .local v14, "f":Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 888
    add-int/lit8 v20, v20, 0x1

    goto :goto_6

    .line 892
    .end local v14    # "f":Lorg/apache/lucene/index/IndexableField;
    :cond_c
    if-nez v27, :cond_d

    .line 893
    const-string v36, "named_location"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 897
    .end local v6    # "arr$":[Lorg/apache/lucene/document/Field;
    .end local v20    # "i$":I
    .end local v21    # "len$":I
    .end local v29    # "point":Lcom/spatial4j/core/shape/Shape;
    .end local v31    # "spatialContext":Lcom/spatial4j/core/context/SpatialContext;
    .end local v32    # "spatialStrategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    :cond_d
    if-eqz v27, :cond_e

    .line 898
    const-string v36, "named_location"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 899
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "named_location"

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v38

    sget-object v39, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v36 .. v39}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 903
    :cond_e
    const/16 v36, -0x1

    move/from16 v0, v36

    if-eq v0, v8, :cond_f

    .line 904
    const-string v36, "deleted"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 905
    new-instance v36, Lorg/apache/lucene/document/IntField;

    const-string v37, "deleted"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 908
    :cond_f
    const/16 v36, -0x1

    move/from16 v0, v36

    move/from16 v1, v17

    if-eq v0, v1, :cond_10

    .line 909
    const-string v36, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 910
    new-instance v36, Lorg/apache/lucene/document/IntField;

    const-string v37, "id"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move/from16 v2, v17

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 913
    :cond_10
    if-eqz v35, :cond_11

    .line 914
    const-string v36, "uri"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 915
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "uri"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v35

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 918
    :cond_11
    const-string v36, "person_names"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 919
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_12

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 920
    .restart local v34    # "tag":Ljava/lang/String;
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "person_names"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v34

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_7

    .line 923
    .end local v34    # "tag":Ljava/lang/String;
    :cond_12
    const-string v36, "scene_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 924
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_8
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_13

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 925
    .restart local v34    # "tag":Ljava/lang/String;
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "scene_type"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v34

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_8

    .line 928
    .end local v34    # "tag":Ljava/lang/String;
    :cond_13
    const-string v36, "subscene_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 929
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_9
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_14

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 930
    .restart local v34    # "tag":Ljava/lang/String;
    new-instance v36, Lorg/apache/lucene/document/StringField;

    const-string v37, "subscene_type"

    sget-object v38, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v34

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_9

    .line 933
    .end local v34    # "tag":Ljava/lang/String;
    :cond_14
    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v36

    if-lez v36, :cond_15

    .line 934
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 935
    const-string v36, "categories"

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 936
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_a
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_15

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 937
    .local v7, "category":Ljava/lang/String;
    new-instance v36, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v37, ";"

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v37

    invoke-direct/range {v36 .. v37}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p3

    move-object/from16 v1, v36

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 938
    new-instance v36, Lorg/apache/lucene/document/StoredField;

    const-string v37, "categories"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_a

    .line 941
    .end local v7    # "category":Ljava/lang/String;
    :cond_15
    return-void
.end method


# virtual methods
.method protected internalRun()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 122
    .local v0, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->isFinishedNow()Z

    move-result v3

    if-nez v3, :cond_1

    .line 123
    const/4 v0, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->maybePause()V

    .line 125
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mContainer:Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->take()Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 127
    if-nez v0, :cond_2

    .line 128
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "No command exiting"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    :cond_1
    :goto_1
    return-void

    .line 132
    :cond_2
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "Took command from container. URI = "

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, " for id = "

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker$1;->$SwitchMap$com$samsung$dcm$framework$extractormanager$Operation:[I

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getOperation()Lcom/samsung/dcm/framework/extractormanager/Operation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/extractormanager/Operation;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 137
    :pswitch_0
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v2

    .line 139
    .local v2, "sdCardAction":Z
    if-eqz v2, :cond_3

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->processBulkInsert(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 140
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->processInsert(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 173
    .end local v2    # "sdCardAction":Z
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "Thread got interrupted!"

    invoke-static {v3, v4, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 147
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :pswitch_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getCurrentSearchId()J

    move-result-wide v4

    invoke-direct {p0, v0, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->processUpdate(Lcom/samsung/dcm/framework/extractormanager/Command;J)V

    .line 150
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isHighPriority()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 151
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "high priority command, calling maybeRefreshBlocking"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    goto/16 :goto_0

    .line 155
    :cond_4
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->TAG:Ljava/lang/String;

    const-string v4, "low priority command, not calling maybeRefreshBlocking"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    :pswitch_2
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 161
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->processBulkDelete(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    goto/16 :goto_0

    .line 163
    :cond_5
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/task/Worker;->processDelete(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 134
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
