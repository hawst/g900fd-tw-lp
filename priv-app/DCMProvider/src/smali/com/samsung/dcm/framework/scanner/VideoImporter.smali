.class public Lcom/samsung/dcm/framework/scanner/VideoImporter;
.super Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;
.source "VideoImporter.java"


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field private static final CONTENT_TYPE:Ljava/lang/String; = "external"

.field private static NO_ALBUM:I = 0x0

.field private static NO_ARTIST:I = 0x0

.field private static NO_BUCKET_DISPLAY_NAME:I = 0x0

.field private static NO_BUCKET_ID:I = 0x0

.field private static NO_CREATION_DATE:I = 0x0

.field private static NO_DATA:I = 0x0

.field private static NO_DESCRIPTION:I = 0x0

.field private static NO_DURATION:I = 0x0

.field private static NO_ID:I = 0x0

.field private static NO_LANGUAGE:I = 0x0

.field private static NO_LATITUDE:I = 0x0

.field private static NO_LONGITUDE:I = 0x0

.field private static NO_MIME_TYPE:I = 0x0

.field private static NO_RESOLUTION:I = 0x0

.field private static final NO_STORAGE_ID:I = 0xf

.field private static NO_TITLE:I

.field private static final TAG:Ljava/lang/String;

.field private static final VIDEO_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    sput v2, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ID:I

    .line 64
    sput v3, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DATA:I

    .line 65
    sput v4, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_TITLE:I

    .line 66
    sput v5, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_MIME_TYPE:I

    .line 67
    sput v6, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_CREATION_DATE:I

    .line 68
    const/4 v0, 0x5

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ARTIST:I

    .line 69
    const/4 v0, 0x6

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ALBUM:I

    .line 70
    const/4 v0, 0x7

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DESCRIPTION:I

    .line 71
    const/16 v0, 0x8

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DURATION:I

    .line 72
    const/16 v0, 0x9

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LANGUAGE:I

    .line 73
    const/16 v0, 0xa

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_RESOLUTION:I

    .line 74
    const/16 v0, 0xb

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_BUCKET_DISPLAY_NAME:I

    .line 75
    const/16 v0, 0xc

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LATITUDE:I

    .line 76
    const/16 v0, 0xd

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LONGITUDE:I

    .line 77
    const/16 v0, 0xe

    sput v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_BUCKET_ID:I

    .line 81
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->BASE_URI:Landroid/net/Uri;

    .line 82
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    const-string v1, "datetaken"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "language"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "storage_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->VIDEO_PROJECTION:[Ljava/lang/String;

    .line 86
    const-class v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/scanner/MediaProviderScanner$Importer;-><init>(Landroid/content/ContentResolver;)V

    .line 90
    return-void
.end method

.method private extractFromCurrentPosition(Landroid/database/Cursor;)Lcom/samsung/dcm/documents/VideoMediaDoc;
    .locals 30
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 112
    new-instance v11, Lcom/samsung/dcm/documents/VideoMediaDoc;

    invoke-direct {v11}, Lcom/samsung/dcm/documents/VideoMediaDoc;-><init>()V

    .line 114
    .local v11, "doc":Lcom/samsung/dcm/documents/VideoMediaDoc;
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ID:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 115
    .local v13, "id":I
    invoke-virtual {v11, v13}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setId(I)V

    .line 116
    sget-object v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->BASE_URI:Landroid/net/Uri;

    int-to-long v0, v13

    move-wide/from16 v28, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v25

    .line 117
    .local v25, "uri":Landroid/net/Uri;
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setUri(Landroid/net/Uri;)V

    .line 119
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_TITLE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_0

    .line 120
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_TITLE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 121
    .local v24, "title":Ljava/lang/String;
    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setTitle(Ljava/lang/String;)V

    .line 124
    .end local v24    # "title":Ljava/lang/String;
    :cond_0
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_MIME_TYPE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_1

    .line 125
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_MIME_TYPE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 126
    .local v20, "mimeType":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setMimeType(Ljava/lang/String;)V

    .line 129
    .end local v20    # "mimeType":Ljava/lang/String;
    :cond_1
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_CREATION_DATE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_2

    .line 130
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_CREATION_DATE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 132
    .local v9, "creationDate":Ljava/lang/Long;
    invoke-virtual {v11, v9}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setCreationDate(Ljava/lang/Long;)V

    .line 135
    .end local v9    # "creationDate":Ljava/lang/Long;
    :cond_2
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ARTIST:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_3

    .line 136
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ARTIST:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 137
    .local v5, "artist":Ljava/lang/String;
    invoke-virtual {v11, v5}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setArtist(Ljava/lang/String;)V

    .line 140
    .end local v5    # "artist":Ljava/lang/String;
    :cond_3
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ALBUM:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_4

    .line 141
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_ALBUM:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, "album":Ljava/lang/String;
    invoke-virtual {v11, v4}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setAlbum(Ljava/lang/String;)V

    .line 145
    .end local v4    # "album":Ljava/lang/String;
    :cond_4
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DESCRIPTION:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_5

    .line 146
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DESCRIPTION:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 147
    .local v10, "description":Ljava/lang/String;
    invoke-virtual {v11, v10}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setDescription(Ljava/lang/String;)V

    .line 150
    .end local v10    # "description":Ljava/lang/String;
    :cond_5
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DURATION:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_6

    .line 151
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DURATION:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 152
    .local v12, "duration":Ljava/lang/Long;
    invoke-virtual {v11, v12}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setDuration(Ljava/lang/Long;)V

    .line 155
    .end local v12    # "duration":Ljava/lang/Long;
    :cond_6
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LANGUAGE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_7

    .line 156
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LANGUAGE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 157
    .local v14, "language":Ljava/lang/String;
    invoke-virtual {v11, v14}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setLanguage(Ljava/lang/String;)V

    .line 160
    .end local v14    # "language":Ljava/lang/String;
    :cond_7
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_RESOLUTION:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_8

    .line 161
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_RESOLUTION:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 162
    .local v22, "resolution":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setResolution(Ljava/lang/String;)V

    .line 165
    .end local v22    # "resolution":Ljava/lang/String;
    :cond_8
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_BUCKET_DISPLAY_NAME:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_9

    .line 166
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_BUCKET_DISPLAY_NAME:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 167
    .local v8, "bucketDisplayName":Ljava/lang/String;
    invoke-virtual {v11, v8}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setBucketDisplayName(Ljava/lang/String;)V

    .line 170
    .end local v8    # "bucketDisplayName":Ljava/lang/String;
    :cond_9
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DATA:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_a

    .line 171
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_DATA:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 172
    .local v21, "path":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setPath(Ljava/lang/String;)V

    .line 175
    .end local v21    # "path":Ljava/lang/String;
    :cond_a
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LATITUDE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_b

    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LONGITUDE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_b

    .line 177
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LATITUDE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v16

    .line 178
    .local v16, "latitude":D
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_LONGITUDE:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v18

    .line 179
    .local v18, "longitude":D
    new-instance v15, Landroid/location/Location;

    const-string v26, ""

    move-object/from16 v0, v26

    invoke-direct {v15, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 180
    .local v15, "location":Landroid/location/Location;
    invoke-virtual/range {v15 .. v17}, Landroid/location/Location;->setLatitude(D)V

    .line 181
    move-wide/from16 v0, v18

    invoke-virtual {v15, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 182
    invoke-virtual {v11, v15}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setLocation(Landroid/location/Location;)V

    .line 185
    .end local v15    # "location":Landroid/location/Location;
    .end local v16    # "latitude":D
    .end local v18    # "longitude":D
    :cond_b
    const/16 v26, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_c

    .line 186
    const/16 v26, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 187
    .local v23, "sid":I
    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setStorageId(I)V

    .line 190
    .end local v23    # "sid":I
    :cond_c
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_BUCKET_ID:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v26

    if-nez v26, :cond_d

    .line 191
    sget v26, Lcom/samsung/dcm/framework/scanner/VideoImporter;->NO_BUCKET_ID:I

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 192
    .local v6, "bid":J
    invoke-virtual {v11, v6, v7}, Lcom/samsung/dcm/documents/VideoMediaDoc;->setBucketId(J)V

    .line 194
    .end local v6    # "bid":J
    :cond_d
    return-object v11
.end method


# virtual methods
.method public scan(Landroid/net/Uri;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 8
    .param p1, "singleUri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 94
    const-string v0, "Uri is null."

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->TAG:Ljava/lang/String;

    const-string v1, "Scanning video file - "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    const-string v0, "uri is null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    const/4 v6, 0x0

    .line 98
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 100
    .local v7, "doc":Lcom/samsung/dcm/documents/VideoMediaDoc;
    iget-object v0, p0, Lcom/samsung/dcm/framework/scanner/VideoImporter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/dcm/framework/scanner/VideoImporter;->VIDEO_PROJECTION:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 101
    if-eqz v6, :cond_1

    .line 102
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/scanner/VideoImporter;->extractFromCurrentPosition(Landroid/database/Cursor;)Lcom/samsung/dcm/documents/VideoMediaDoc;

    move-result-object v7

    .line 105
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 108
    :cond_1
    return-object v7
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
