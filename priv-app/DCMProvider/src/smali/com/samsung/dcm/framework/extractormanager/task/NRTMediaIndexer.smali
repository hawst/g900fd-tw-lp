.class Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;
.super Ljava/lang/Object;
.source "NRTMediaIndexer.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lorg/apache/lucene/document/Document;)V
    .locals 1
    .param p1, "nrtManager"    # Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 45
    const-string v0, "DCMNRTManager must not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 47
    return-void
.end method


# virtual methods
.method public addDelayedPluginInfo(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V
    .locals 9
    .param p1, "lucenedoc"    # Lorg/apache/lucene/document/Document;
    .param p2, "doctype"    # Lcom/samsung/dcm/documents/MediaDoc;

    .prologue
    .line 97
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v1

    .line 100
    .local v1, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-eqz v1, :cond_0

    .line 101
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 102
    .local v0, "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    new-instance v2, Lorg/apache/lucene/document/StringField;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "false"

    sget-object v6, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v2, v4, v5, v6}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    .line 103
    .local v2, "field":Lorg/apache/lucene/document/StringField;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->TAG:Ljava/lang/String;

    const-string v5, "Plugin"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, " set as false"

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p1, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 107
    .end local v0    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v2    # "field":Lorg/apache/lucene/document/StringField;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method public deleteDoc(Landroid/net/Uri;Ljava/lang/Long;)J
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "id"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/UpdatingException;
        }
    .end annotation

    .prologue
    .line 57
    const-string v2, "List of Uris must not be null"

    invoke-static {p1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    new-instance v1, Lorg/apache/lucene/index/Term;

    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .local v1, "term":Lorg/apache/lucene/index/Term;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p2, v1, v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->deleteDocuments(Ljava/lang/Long;Lorg/apache/lucene/index/Term;Ljava/lang/Integer;)J
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    return-wide v2

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->TAG:Ljava/lang/String;

    const-string v3, "deleteDoc"

    invoke-static {v2, v3, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    new-instance v2, Lcom/samsung/dcm/framework/exceptions/UpdatingException;

    invoke-direct {v2, v0}, Lcom/samsung/dcm/framework/exceptions/UpdatingException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public indexDocument(Lcom/samsung/dcm/documents/MediaDoc;Ljava/lang/Long;)J
    .locals 6
    .param p1, "mediaDoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p2, "id"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/IndexingException;
        }
    .end annotation

    .prologue
    .line 76
    const-string v4, "MediaDoc must not be null"

    invoke-static {p1, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "Media doc should have Uri filled"

    invoke-static {v4, v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v1, Lorg/apache/lucene/document/Document;

    invoke-direct {v1}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 79
    .local v1, "mLuceneDoc":Lorg/apache/lucene/document/Document;
    invoke-static {v1}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 81
    :try_start_0
    invoke-static {p1}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->getConverter(Lcom/samsung/dcm/documents/MediaDoc;)Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;

    move-result-object v2

    .line 82
    .local v2, "mediaDocConverter":Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
    if-eqz v2, :cond_0

    .line 83
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->setFacetFields(Lorg/apache/lucene/facet/index/FacetFields;)V

    .line 84
    invoke-virtual {v2, p1, v1}, Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;->convert2Document(Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;)V

    .line 86
    :cond_0
    invoke-virtual {p0, v1, p1}, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->addDelayedPluginInfo(Lorg/apache/lucene/document/Document;Lcom/samsung/dcm/documents/MediaDoc;)V

    .line 87
    new-instance v3, Lorg/apache/lucene/index/Term;

    const-string v4, "uri"

    const-string v5, "uri"

    invoke-virtual {v1, v5}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .local v3, "term":Lorg/apache/lucene/index/Term;
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {p1}, Lcom/samsung/dcm/documents/MediaDoc;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v1, p2, v5}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Ljava/lang/Long;Ljava/lang/Integer;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    return-wide v4

    .line 90
    .end local v2    # "mediaDocConverter":Lcom/samsung/dcm/framework/indexer/IMediaDocConverter;
    .end local v3    # "term":Lorg/apache/lucene/index/Term;
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/NRTMediaIndexer;->TAG:Ljava/lang/String;

    const-string v5, "Indexing exception:"

    invoke-static {v4, v5, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    new-instance v4, Lcom/samsung/dcm/framework/exceptions/IndexingException;

    invoke-direct {v4, v0}, Lcom/samsung/dcm/framework/exceptions/IndexingException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method
