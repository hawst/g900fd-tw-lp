.class Lcom/samsung/dcm/framework/lucene/DCMNRTManager$1;
.super Ljava/lang/Object;
.source "DCMNRTManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/dcm/framework/lucene/DCMNRTManager;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager$1;->this$0:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 156
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v4

    const/16 v5, 0xa

    invoke-static {v4, v5}, Landroid/os/Process;->setThreadPriority(II)V

    .line 158
    const-class v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v4}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 159
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v3

    .line 160
    .local v3, "transport":Lcom/samsung/dcm/framework/configuration/DcmTransport;
    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    .line 161
    .local v1, "controller":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    invoke-static {}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isAlive()Z

    move-result v2

    .line 163
    .local v2, "sendPauseResume":Z
    if-eqz v2, :cond_0

    .line 164
    :try_start_0
    sget-object v4, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->LUCENE_COMMIT:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 166
    :cond_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager$1;->this$0:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->commitAndRefresh()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    if-eqz v2, :cond_1

    .line 169
    sget-object v4, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->LUCENE_COMMIT:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 172
    :cond_1
    return-void

    .line 168
    :catchall_0
    move-exception v4

    if-eqz v2, :cond_2

    .line 169
    sget-object v5, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->LUCENE_COMMIT:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    :cond_2
    throw v4
.end method
