.class public interface abstract Lcom/samsung/dcm/framework/extractormanager/task/IDCMPluginTask;
.super Ljava/lang/Object;
.source "IDCMPluginTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract executePlugins(Ljava/lang/Class;Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;Ljava/util/Set;Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/Set",
            "<TE;>;",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract updateBundle(Ljava/lang/Class;Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            "Lorg/apache/lucene/document/Document;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation
.end method

.method public abstract updateDocument(Ljava/lang/Object;Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            "Lorg/apache/lucene/document/Document;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation
.end method
