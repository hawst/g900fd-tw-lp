.class public final Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;
.super Ljava/lang/Object;
.source "DCMAffinityControl.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final msCPUAffinityList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->TAG:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->msCPUAffinityList:Ljava/util/Set;

    .line 39
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->msCPUAffinityList:Ljava/util/Set;

    const-string v1, "exynos5433"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->msCPUAffinityList:Ljava/util/Set;

    const-string v1, "exynos5422"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->msCPUAffinityList:Ljava/util/Set;

    const-string v1, "exynos5420"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs setAffinityForThread(I[I)V
    .locals 6
    .param p0, "tid"    # I
    .param p1, "list"    # [I

    .prologue
    const/4 v5, 0x0

    .line 48
    const-string v1, "ro.chipname"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "chipsetName":Ljava/lang/String;
    if-eqz v0, :cond_0

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->msCPUAffinityList:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->TAG:Ljava/lang/String;

    const-string v2, "Applying for"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, " cores"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->TAG:Ljava/lang/String;

    const-string v2, "No Affinity Set"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
