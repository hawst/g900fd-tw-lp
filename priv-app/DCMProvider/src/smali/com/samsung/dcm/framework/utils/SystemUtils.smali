.class public Lcom/samsung/dcm/framework/utils/SystemUtils;
.super Ljava/lang/Object;
.source "SystemUtils.java"


# static fields
.field private static final CPU_DEV_BASE_PATH:Ljava/lang/String; = "/sys/devices/system/cpu/cpu"

.field public static final LOW_BATTERY_THRESHOLD_PERC:I = 0x5

.field private static final MAX_CPU_NOS:I = 0x8

.field private static final PROC_STAT:Ljava/lang/String; = "/proc/stat"

.field private static final TAG:Ljava/lang/String;

.field private static isBatteryOK:Z

.field private static isMemoryOK:Z

.field private static isTemperatureOK:Z

.field public static kKeyHeavyStartFlag:Ljava/lang/String;

.field private static kKeyOSUpgradeStartFlagForExtSd:Ljava/lang/String;

.field private static kKeyOSUpgradeStartFlagForMedia:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    const-class v0, Lcom/samsung/dcm/framework/utils/SystemUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    .line 51
    sput-boolean v1, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatteryOK:Z

    .line 52
    sput-boolean v1, Lcom/samsung/dcm/framework/utils/SystemUtils;->isMemoryOK:Z

    .line 53
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/dcm/framework/utils/SystemUtils;->isTemperatureOK:Z

    .line 55
    const-string v0, "KeyHeavyStartFlag"

    sput-object v0, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyHeavyStartFlag:Ljava/lang/String;

    .line 56
    const-string v0, "KeyOSUpgradeStartFlagForMedia"

    sput-object v0, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyOSUpgradeStartFlagForMedia:Ljava/lang/String;

    .line 57
    const-string v0, "KeyOSUpgradeStartFlagForExtSd"

    sput-object v0, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyOSUpgradeStartFlagForExtSd:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBatteryPerc(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, -0x1

    const/4 v5, 0x0

    .line 62
    if-nez p0, :cond_0

    move v0, v5

    .line 79
    :goto_0
    return v0

    .line 66
    :cond_0
    new-instance v2, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 67
    .local v2, "intent":Landroid/content/IntentFilter;
    const/4 v6, 0x0

    invoke-virtual {p0, v6, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 69
    .local v1, "bStatus":Landroid/content/Intent;
    if-nez v1, :cond_1

    move v0, v5

    .line 70
    goto :goto_0

    .line 73
    :cond_1
    const-string v6, "level"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 74
    .local v3, "level":I
    const-string v6, "scale"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 76
    .local v4, "scale":I
    mul-int/lit8 v6, v3, 0x64

    div-int v0, v6, v4

    .line 77
    .local v0, "bPer":I
    sget-object v6, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v7, "Battery  bPer ="

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    const/4 v5, 0x1

    const-string v9, "%"

    aput-object v9, v8, v5

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static declared-synchronized getHeavySharedPref(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    const-class v3, Lcom/samsung/dcm/framework/utils/SystemUtils;

    monitor-enter v3

    const/4 v0, 0x1

    .line 140
    .local v0, "defaultPrefValue":Z
    :try_start_0
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string v2, "Pref"

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 142
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 143
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyHeavyStartFlag:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 144
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v4, "pref_value getHeavySharedPref is ="

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_0
    monitor-exit v3

    return v0

    .line 139
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getNoOfCPUCores()I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 282
    const/4 v5, 0x0

    .line 285
    .local v5, "reader":Ljava/io/RandomAccessFile;
    const/4 v3, -0x1

    .line 287
    .local v3, "online_cpus":I
    :try_start_0
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v7, "/proc/stat"

    const-string v8, "r"

    invoke-direct {v6, v7, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    .end local v5    # "reader":Ljava/io/RandomAccessFile;
    .local v6, "reader":Ljava/io/RandomAccessFile;
    const/4 v2, 0x0

    .line 289
    .local v2, "line":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 291
    const-string v7, "cpu"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 292
    add-int/lit8 v3, v3, 0x1

    .line 295
    :cond_1
    const/16 v7, 0x8

    if-lt v3, v7, :cond_0

    .line 300
    :cond_2
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 301
    const/4 v5, 0x0

    .line 307
    .end local v6    # "reader":Ljava/io/RandomAccessFile;
    .restart local v5    # "reader":Ljava/io/RandomAccessFile;
    sget-object v7, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v8, "getCPUCoresOnline Failed"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    if-eqz v5, :cond_3

    .line 310
    :try_start_2
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    :goto_0
    move v4, v3

    .line 318
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "online_cpus":I
    .local v4, "online_cpus":I
    :goto_1
    return v4

    .line 311
    .end local v4    # "online_cpus":I
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "online_cpus":I
    :catch_0
    move-exception v1

    .line 312
    .local v1, "e1":Ljava/io/IOException;
    sget-object v7, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v8, "reader close"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 303
    .end local v1    # "e1":Ljava/io/IOException;
    .end local v2    # "line":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v7, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v8, "getCPUCoresOnline Exception: online cpus so far is:"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 307
    sget-object v7, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v8, "getCPUCoresOnline Failed"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    if-eqz v5, :cond_4

    .line 310
    :try_start_4
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_4
    :goto_3
    move v4, v3

    .line 314
    .end local v3    # "online_cpus":I
    .restart local v4    # "online_cpus":I
    goto :goto_1

    .line 311
    .end local v4    # "online_cpus":I
    .restart local v3    # "online_cpus":I
    :catch_2
    move-exception v1

    .line 312
    .restart local v1    # "e1":Ljava/io/IOException;
    sget-object v7, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v8, "reader close"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 307
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_4
    sget-object v8, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v9, "getCPUCoresOnline Failed"

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    if-eqz v5, :cond_5

    .line 310
    :try_start_5
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 314
    :cond_5
    :goto_5
    throw v7

    .line 311
    :catch_3
    move-exception v1

    .line 312
    .restart local v1    # "e1":Ljava/io/IOException;
    sget-object v8, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v9, "reader close"

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 307
    .end local v1    # "e1":Ljava/io/IOException;
    .end local v5    # "reader":Ljava/io/RandomAccessFile;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v6    # "reader":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/RandomAccessFile;
    .restart local v5    # "reader":Ljava/io/RandomAccessFile;
    goto :goto_4

    .line 303
    .end local v5    # "reader":Ljava/io/RandomAccessFile;
    .restart local v6    # "reader":Ljava/io/RandomAccessFile;
    :catch_4
    move-exception v0

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/RandomAccessFile;
    .restart local v5    # "reader":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method public static getNoOfCPUCoresEx()I
    .locals 10

    .prologue
    .line 325
    const/4 v0, 0x0

    .line 326
    .local v0, "cpuDevPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 327
    .local v1, "cpu_number":I
    const/4 v4, 0x0

    .line 329
    .local v4, "no_of_cpus":I
    :goto_0
    const/16 v5, 0x8

    if-ge v1, v5, :cond_1

    .line 331
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/sys/devices/system/cpu/cpu"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 332
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 333
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    .line 334
    add-int/lit8 v4, v4, 0x1

    .line 336
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 337
    .end local v3    # "f":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 338
    .local v2, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v6, "getNoOfCPUCoresEx"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    const/4 v4, 0x4

    .line 344
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "no_of_cpus":I
    :cond_1
    return v4
.end method

.method public static getOSUpgradeSharedPrefForExtSd(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "defaultPrefValue":Z
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    const-string v2, "OSUpgradePrefForExtSd"

    invoke-virtual {p0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 201
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 202
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyOSUpgradeStartFlagForExtSd:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 203
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v3, "pref_value getOSUpgradeSharedPrefForExtSd is ="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    :cond_0
    return v0
.end method

.method public static getOSUpgradeSharedPrefForMedia(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "defaultPrefValue":Z
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-string v2, "OSUpgradePrefForMedia"

    invoke-virtual {p0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 167
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 168
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyOSUpgradeStartFlagForMedia:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 169
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v3, "pref_value getOSUpgradeSharedPrefForMedia is ="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    :cond_0
    return v0
.end method

.method public static declared-synchronized handlerOOM()V
    .locals 5

    .prologue
    .line 209
    const-class v4, Lcom/samsung/dcm/framework/utils/SystemUtils;

    monitor-enter v4

    :try_start_0
    const-class v3, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v3}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 210
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v2

    .line 212
    .local v2, "transport":Lcom/samsung/dcm/framework/configuration/DcmTransport;
    if-eqz v2, :cond_0

    .line 213
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v1

    .line 214
    .local v1, "controller":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onOOM()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    .end local v1    # "controller":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .end local v2    # "transport":Lcom/samsung/dcm/framework/configuration/DcmTransport;
    :cond_0
    monitor-exit v4

    return-void

    .line 209
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static isBatterOK(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 84
    invoke-static {p0}, Lcom/samsung/dcm/framework/utils/SystemUtils;->getBatteryPerc(Landroid/content/Context;)I

    move-result v0

    .line 85
    .local v0, "bPer":I
    sget-object v1, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v2, "isBatterOK  bPer ="

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const-string v4, "%"

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    const/4 v1, 0x5

    if-ge v1, v0, :cond_0

    .line 88
    sput-boolean v6, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatteryOK:Z

    .line 94
    :goto_0
    sget-boolean v1, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatteryOK:Z

    return v1

    .line 90
    :cond_0
    sget-object v1, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v2, "Low Battery Case: Level:"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const-string v4, "%"

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    sput-boolean v5, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatteryOK:Z

    goto :goto_0
.end method

.method public static isFileInSdCard(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "status":Z
    if-eqz p0, :cond_0

    const-string v1, "/storage/extSdCard"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    const/4 v0, 0x1

    .line 352
    :cond_0
    return v0
.end method

.method public static isMemoryOK(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 98
    if-nez p0, :cond_1

    .line 99
    sput-boolean v3, Lcom/samsung/dcm/framework/utils/SystemUtils;->isMemoryOK:Z

    .line 100
    sget-boolean v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->isMemoryOK:Z

    .line 111
    :cond_0
    :goto_0
    return v2

    .line 103
    :cond_1
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 104
    .local v1, "mi":Landroid/app/ActivityManager$MemoryInfo;
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 106
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 108
    sget-object v4, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v5, "isMemoryOK lowmemory ="

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    iget-boolean v7, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v3

    const-string v7, " availMem = "

    aput-object v7, v6, v2

    iget-wide v8, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    const-string v7, " totalMem = "

    aput-object v7, v6, v11

    const/4 v7, 0x4

    iget-wide v8, v1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, " threshold = "

    aput-object v8, v6, v7

    const/4 v7, 0x6

    iget-wide v8, v1, Landroid/app/ActivityManager$MemoryInfo;->threshold:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    sget-object v4, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v5, "LM:"

    new-array v6, v11, [Ljava/lang/Object;

    iget-boolean v7, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v3

    const-string v7, ",AM:"

    aput-object v7, v6, v2

    iget-wide v8, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-boolean v4, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method public static isOSCEnabled()Z
    .locals 4

    .prologue
    .line 125
    const-string v1, "persist.sys.dcm.enableosc"

    .line 127
    .local v1, "kKeySystemOSC":Ljava/lang/String;
    const/4 v0, 0x1

    .line 128
    .local v0, "enableosc":Z
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "prop":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 135
    :cond_0
    return v0
.end method

.method public static isSystemShutdown()Z
    .locals 6

    .prologue
    .line 374
    const/4 v0, 0x0

    .line 378
    .local v0, "bBeingShutdown":Z
    const-string v2, "sys.shutdown.requested"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "prop":Ljava/lang/String;
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v3, "isSystemShutdown "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    if-eqz v1, :cond_1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "1GlobalActions restart"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 382
    :cond_0
    const/4 v0, 0x1

    .line 384
    :cond_1
    return v0
.end method

.method public static isTemperatureOK()Z
    .locals 1

    .prologue
    .line 116
    sget-boolean v0, Lcom/samsung/dcm/framework/utils/SystemUtils;->isTemperatureOK:Z

    return v0
.end method

.method public static isValidPath(Ljava/lang/String;)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 241
    if-eqz p0, :cond_0

    .line 242
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, "ff":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    const/4 v0, 0x0

    .line 248
    const/4 v1, 0x1

    .line 254
    .end local v0    # "ff":Ljava/io/File;
    :goto_0
    return v1

    .line 253
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v3, "File does not exist "

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static isValidPathforOSC(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 356
    invoke-static {p0}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isValidPath(Ljava/lang/String;)Z

    move-result v1

    .line 358
    .local v1, "valid":Z
    if-eqz v1, :cond_0

    .line 359
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/samsung/dcm/framework/configuration/StorageController;->getStorageId(Ljava/lang/String;)I

    move-result v0

    .line 360
    .local v0, "storageid":I
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/dcm/framework/configuration/StorageController;->isVolumeMounted(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 361
    const/4 v1, 0x0

    .line 365
    .end local v0    # "storageid":I
    :cond_0
    return v1
.end method

.method public static killself()V
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 122
    return-void
.end method

.method public static declared-synchronized setHeavySharedPref(Landroid/content/Context;Z)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "status"    # Z

    .prologue
    const/4 v2, 0x1

    .line 150
    const-class v4, Lcom/samsung/dcm/framework/utils/SystemUtils;

    monitor-enter v4

    :try_start_0
    const-string v3, "Context cannot be null"

    invoke-static {p0, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->isVAEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, p1

    .line 153
    .local v2, "statusToSet":Z
    :cond_0
    const-string v3, "Pref"

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 154
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 155
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 156
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_1

    .line 157
    sget-object v3, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyHeavyStartFlag:Ljava/lang/String;

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 159
    sget-object v3, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v5, "setHeavySharedPref set as "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    monitor-exit v4

    return-void

    .line 150
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    .end local v2    # "statusToSet":Z
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static declared-synchronized setOSUpgradeSharedPrefForExtSd(Landroid/content/Context;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "status"    # Z

    .prologue
    .line 186
    const-class v3, Lcom/samsung/dcm/framework/utils/SystemUtils;

    monitor-enter v3

    :try_start_0
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    const-string v2, "OSUpgradePrefForExtSd"

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 188
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 189
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 190
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 191
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyOSUpgradeStartFlagForExtSd:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 192
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 193
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v4, "setOSUpgradeSharedPrefForExtSd set as "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    monitor-exit v3

    return-void

    .line 186
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized setOSUpgradeSharedPrefForMedia(Landroid/content/Context;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "status"    # Z

    .prologue
    .line 174
    const-class v3, Lcom/samsung/dcm/framework/utils/SystemUtils;

    monitor-enter v3

    :try_start_0
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const-string v2, "OSUpgradePrefForMedia"

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 176
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 177
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 178
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 179
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyOSUpgradeStartFlagForMedia:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 181
    sget-object v2, Lcom/samsung/dcm/framework/utils/SystemUtils;->TAG:Ljava/lang/String;

    const-string v4, "setOSUpgradeSharedPrefForMedia set as "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    monitor-exit v3

    return-void

    .line 174
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
