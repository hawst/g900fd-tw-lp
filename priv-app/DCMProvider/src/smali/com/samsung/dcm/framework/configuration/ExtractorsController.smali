.class public Lcom/samsung/dcm/framework/configuration/ExtractorsController;
.super Ljava/lang/Object;
.source "ExtractorsController.java"


# static fields
.field private static sExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;


# instance fields
.field mClassExtractorToInstanceMapper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation
.end field

.field mExtractorsForGivenClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;>;"
        }
    .end annotation
.end field

.field mLateExtractorsForGivenClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;>;"
        }
    .end annotation
.end field

.field mRegisteredExtractors:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mRegisteredExtractors:Ljava/util/Set;

    .line 204
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mExtractorsForGivenClass:Ljava/util/Map;

    .line 205
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    .line 207
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mLateExtractorsForGivenClass:Ljava/util/Map;

    .line 209
    return-void
.end method

.method private getExtractorToChangeState(Lcom/samsung/dcm/framework/extractors/Extractor;)Lcom/google/common/base/Optional;
    .locals 4
    .param p1, "extractorToFind"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ")",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    .line 185
    .local v2, "wantedExtractor":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mRegisteredExtractors:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 186
    .local v0, "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractors/Extractor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 187
    invoke-static {v0}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    .line 191
    .end local v0    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    :cond_1
    return-object v2
.end method

.method public static getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->sExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->sExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    .line 37
    :cond_0
    sget-object v0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->sExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    return-object v0
.end method

.method private isExtractorRegisteredWithCharacteristics(Ljava/util/Map;Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 2
    .param p3, "testedExtractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;>;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "keyToExtractorsMapper":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;>;"
    .local p2, "keyUsed4Registration":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 198
    .local v0, "registeredExtractorsSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method private registerExtractor(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 4
    .param p2, "extractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "extractorKey":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    const/4 v1, 0x1

    .line 167
    .local v1, "result":Z
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mExtractorsForGivenClass:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 168
    .local v0, "extractorsForGivenClass":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-nez v0, :cond_0

    .line 169
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "extractorsForGivenClass":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 170
    .restart local v0    # "extractorsForGivenClass":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mExtractorsForGivenClass:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 173
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mRegisteredExtractors:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 174
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mRegisteredExtractors:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 177
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_2
    return v1
.end method


# virtual methods
.method public disableRegisteredExtractor(Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 2
    .param p1, "registeredExtractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;

    .prologue
    .line 132
    const-string v1, "Argument [registeredExtractor] cannot be null!"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorToChangeState(Lcom/samsung/dcm/framework/extractors/Extractor;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 136
    .local v0, "extractorToDisable":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractors/Extractor;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractors/Extractor;->disableExtractor()V

    .line 138
    const/4 v1, 0x1

    .line 140
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableRegisteredExtractor(Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 2
    .param p1, "registeredExtractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;

    .prologue
    .line 113
    const-string v1, "Argument [registeredExtractor] cannot be null!"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorToChangeState(Lcom/samsung/dcm/framework/extractors/Extractor;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 117
    .local v0, "extractorToEnable":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractors/Extractor;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractors/Extractor;->enableExtractor()V

    .line 119
    const/4 v1, 0x1

    .line 121
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;)",
            "Lcom/samsung/dcm/framework/extractors/Extractor;"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    return-object v0
.end method

.method public getExtractorsForClass(Lcom/samsung/dcm/documents/MediaDoc;)Ljava/util/Set;
    .locals 1
    .param p1, "doc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    const-string v0, "Argument [doc] cannot be null!"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "extractorsClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mExtractorsForGivenClass:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 55
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    goto :goto_0
.end method

.method public getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "extractorsClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mLateExtractorsForGivenClass:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 230
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    goto :goto_0
.end method

.method public getRegisteredExtractors()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mRegisteredExtractors:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public isExtractorEnabled(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 76
    .local v0, "registeredExtractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->isEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method public isExtractorRegistered(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 1
    .param p2, "testedExtractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "keyUsed4Registration":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    const-string v0, "Argument [keyUsed4Registration] cannot be null!"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mExtractorsForGivenClass:Ljava/util/Map;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->isExtractorRegisteredWithCharacteristics(Ljava/util/Map;Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z

    move-result v0

    return v0
.end method

.method public onLostInternetConnection()V
    .locals 3

    .prologue
    .line 147
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    const-class v2, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 148
    .local v0, "dependentExtractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->disableExtractor()V

    .line 150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractors/Extractor;->setIsConnected(Z)V

    .line 152
    :cond_0
    return-void
.end method

.method public onReconnectToInternet()V
    .locals 3

    .prologue
    .line 158
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    const-class v2, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 159
    .local v0, "dependentExtractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->enableExtractor()V

    .line 161
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractors/Extractor;->setIsConnected(Z)V

    .line 163
    :cond_0
    return-void
.end method

.method public registerExtractorForClass(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 1
    .param p2, "extractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "registrationKey":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    const-string v0, "Extractor key argument cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v0, "Extractor argument cannot be null!"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->registerExtractor(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z

    move-result v0

    return v0
.end method

.method public registerLateExtractorForClass(Ljava/lang/Class;Lcom/samsung/dcm/framework/extractors/Extractor;)Z
    .locals 4
    .param p2, "extractor"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "registrationKey":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    const/4 v1, 0x1

    .line 214
    .local v1, "result":Z
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mLateExtractorsForGivenClass:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 215
    .local v0, "extractorsForGivenClass":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-nez v0, :cond_0

    .line 216
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "extractorsForGivenClass":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 217
    .restart local v0    # "extractorsForGivenClass":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mLateExtractorsForGivenClass:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 221
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 222
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->mClassExtractorToInstanceMapper:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    :cond_1
    return v1
.end method
