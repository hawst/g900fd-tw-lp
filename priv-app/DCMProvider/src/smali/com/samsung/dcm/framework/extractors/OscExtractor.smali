.class public Lcom/samsung/dcm/framework/extractors/OscExtractor;
.super Lcom/samsung/dcm/framework/extractors/Extractor;
.source "OscExtractor.java"


# static fields
.field public static final OBJECT_AND_SCENE:Ljava/lang/String; = "ObjectAndScene"

.field private static final TAG:Ljava/lang/String;

.field private static final UNCLASSIFIED:Ljava/lang/String; = "Unclassified"


# instance fields
.field mAssetManager:Landroid/content/res/AssetManager;

.field mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/AssetManager;)V
    .locals 3
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractors/Extractor;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    .line 57
    iput-object v0, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mAssetManager:Landroid/content/res/AssetManager;

    .line 69
    sget-object v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v1, "OscExtractor using AssetManager"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mAssetManager:Landroid/content/res/AssetManager;

    .line 72
    return-void
.end method

.method private process(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 20
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 197
    const-string v14, "path cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    const-string v15, "imageClassifier cannot be null"

    invoke-static {v14, v15}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, " [DCM] OscExtractor Processing...path = "

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p1, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 201
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 202
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v14, "OSC_STATUS"

    const/4 v15, 0x1

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 204
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 205
    .local v12, "time":J
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "[DCM] osc process() start, path: "

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p1, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    const/4 v6, 0x0

    .line 208
    .local v6, "mClasses":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->classify(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 213
    :goto_0
    if-nez v6, :cond_0

    .line 258
    :goto_1
    return-object v2

    .line 209
    :catch_0
    move-exception v3

    .line 210
    .local v3, "e":Ljava/lang/Exception;
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "classify got exception"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v3, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 218
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_0
    if-eqz v6, :cond_6

    .line 219
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v8, "sceneTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v11, "subSceneTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v14

    if-ge v4, v14, :cond_3

    .line 223
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual {v14}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v7

    .line 224
    .local v7, "sceneName":Ljava/lang/String;
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "Scene Type for path:"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p1, v16, v17

    const/16 v17, 0x1

    const-string v18, " sceneName:"

    aput-object v18, v16, v17

    const/16 v17, 0x2

    aput-object v7, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 225
    const-string v14, "Unclassified"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 226
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual {v14}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getSubTagEntities()Ljava/util/List;

    move-result-object v9

    .line 228
    .local v9, "subEntity":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    if-eqz v9, :cond_2

    .line 229
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_3
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v14

    if-ge v5, v14, :cond_2

    .line 230
    sget-object v15, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v16, "Sub Scene type: "

    const/4 v14, 0x1

    new-array v0, v14, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual {v14}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v17, v18

    invoke-static/range {v15 .. v17}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual {v14}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v10

    .line 232
    .local v10, "subName":Ljava/lang/String;
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "Sub Scene Type for path:"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p1, v16, v17

    const/16 v17, 0x1

    const-string v18, " subsceneName:"

    aput-object v18, v16, v17

    const/16 v17, 0x2

    aput-object v10, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 238
    .end local v5    # "j":I
    .end local v9    # "subEntity":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    .end local v10    # "subName":Ljava/lang/String;
    :cond_1
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "scene type unclassified"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 242
    .end local v7    # "sceneName":Ljava/lang/String;
    :cond_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_4

    .line 243
    const-string v14, "OSC_SCENETYPE_TO_BE_ADDED"

    invoke-virtual {v2, v14, v8}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 246
    :cond_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_5

    .line 247
    const-string v14, "OSC_SUBSCENETYPE_TO_BE_ADDED"

    invoke-virtual {v2, v14, v11}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 251
    :cond_5
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "[DCM] osc process() end, time : "

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v12

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    const-string v18, ", path: "

    aput-object v18, v16, v17

    const/16 v17, 0x2

    aput-object p1, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    .end local v4    # "i":I
    .end local v8    # "sceneTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11    # "subSceneTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "Time: "

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v12

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    sget-object v14, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v15, "[DCM] OscExtractor Processing... end"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method private process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;
    .locals 22
    .param p1, "doc"    # Lcom/samsung/dcm/documents/ImageMediaDoc;

    .prologue
    .line 87
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, " [DCM] OscExtractor Processing..."

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getPath()Lcom/google/common/base/Optional;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v16

    if-eqz v16, :cond_5

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 91
    .local v14, "time":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getPath()Lcom/google/common/base/Optional;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 93
    .local v8, "path":Ljava/lang/String;
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "[DCM] osc process() start, path: "

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v8, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    const/4 v7, 0x0

    .line 97
    .local v7, "mClasses":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->classify(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 102
    :goto_0
    if-nez v7, :cond_0

    .line 103
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "mClasses is null for file:"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getPath()Lcom/google/common/base/Optional;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    .end local v7    # "mClasses":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    .end local v8    # "path":Ljava/lang/String;
    .end local v14    # "time":J
    :goto_1
    return-object p1

    .line 98
    .restart local v7    # "mClasses":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    .restart local v8    # "path":Ljava/lang/String;
    .restart local v14    # "time":J
    :catch_0
    move-exception v4

    .line 99
    .local v4, "e":Ljava/lang/Exception;
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "classify got exception"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v4, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_0
    if-eqz v7, :cond_4

    .line 111
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 112
    .local v10, "sceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 113
    .local v13, "subSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .local v2, "builder":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    move/from16 v0, v16

    if-ge v5, v0, :cond_3

    .line 116
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v9

    .line 117
    .local v9, "sceneName":Ljava/lang/String;
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "Scene Type for path:"

    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v8, v18, v19

    const/16 v19, 0x1

    const-string v20, " sceneName:"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    aput-object v9, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    const-string v16, "Unclassified"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 119
    invoke-interface {v10, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 121
    const-string v16, "root_scene_type"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v16, ";"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "category":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/documents/ImageMediaDoc;->addCategory(Ljava/lang/String;)V

    .line 126
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getSubTagEntities()Ljava/util/List;

    move-result-object v11

    .line 128
    .local v11, "subEntity":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    if-eqz v11, :cond_2

    .line 129
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_3
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v16

    move/from16 v0, v16

    if-ge v6, v0, :cond_2

    .line 132
    sget-object v17, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v18, "Sub Scene type: "

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v19, v20

    invoke-static/range {v17 .. v19}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/vision/imageclassification/TagEntity;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v12

    .line 134
    .local v12, "subName":Ljava/lang/String;
    invoke-interface {v13, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "Sub Scene Type for path:"

    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v8, v18, v19

    const/16 v19, 0x1

    const-string v20, " subsceneName:"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    aput-object v12, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    .end local v2    # "builder":Ljava/lang/StringBuilder;
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    .restart local v2    # "builder":Ljava/lang/StringBuilder;
    const-string v16, "root_subscene_type"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string v16, ";"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 142
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/documents/ImageMediaDoc;->addCategory(Ljava/lang/String;)V

    .line 129
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 147
    .end local v3    # "category":Ljava/lang/String;
    .end local v6    # "j":I
    .end local v11    # "subEntity":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    .end local v12    # "subName":Ljava/lang/String;
    :cond_1
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "scene type unclassified"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 150
    .end local v9    # "sceneName":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setSubSceneTypes(Ljava/util/Set;)V

    .line 151
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setSceneTypes(Ljava/util/Set;)V

    .line 152
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "[DCM] osc process() end, time : "

    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, ", path: "

    aput-object v20, v18, v19

    const/16 v19, 0x2

    aput-object v8, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "i":I
    .end local v10    # "sceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "subSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_4
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "Time: "

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v14

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    .end local v7    # "mClasses":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    .end local v8    # "path":Ljava/lang/String;
    .end local v14    # "time":J
    :goto_4
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "[DCM] OscExtractor Processing... end"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 159
    :cond_5
    sget-object v16, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v17, "[DCM] doc doesn\'t have path"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v18}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4
.end method

.method private releaseImageClassifier()V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    invoke-virtual {v0}, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->release()V

    .line 185
    sget-object v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v1, "OscExtractor\'s ImageClassifier Released"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    .line 188
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 176
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 178
    sget-object v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v1, "finalize()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractors/OscExtractor;->releaseImageClassifier()V

    .line 180
    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 276
    const/16 v0, 0x13

    .line 277
    .local v0, "prime":I
    const/16 v1, 0x13

    return v1
.end method

.method public initialize()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    sget-object v1, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v2, "initialize()"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    :try_start_0
    const-string v1, "ObjectAndScene"

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "ObjectAndScene"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->createInstance(Ljava/lang/String;Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mImageClassifier:Lcom/samsung/android/vision/imageclassification/ImageClassifier;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v2, "createInstance got exception"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected initializeSupportedClasses()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->mSupportedExtractors:Ljava/util/LinkedList;

    const-class v1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method public process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 263
    instance-of v2, p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    if-eqz v2, :cond_0

    .line 264
    check-cast p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractors/OscExtractor;->process_image(Lcom/samsung/dcm/documents/ImageMediaDoc;)Lcom/samsung/dcm/documents/MediaDoc;

    move-result-object v2

    .line 273
    :goto_0
    return-object v2

    .line 266
    .restart local p1    # "data":Ljava/lang/Object;
    :cond_0
    instance-of v2, p1, Lorg/apache/lucene/document/Document;

    if-eqz v2, :cond_1

    move-object v0, p1

    .line 268
    check-cast v0, Lorg/apache/lucene/document/Document;

    .line 269
    .local v0, "lucenedoc":Lorg/apache/lucene/document/Document;
    const-string v2, "path"

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "path":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/dcm/framework/extractors/OscExtractor;->process(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0

    .line 273
    .end local v0    # "lucenedoc":Lorg/apache/lucene/document/Document;
    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 167
    sget-object v0, Lcom/samsung/dcm/framework/extractors/OscExtractor;->TAG:Ljava/lang/String;

    const-string v1, "release()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractors/OscExtractor;->releaseImageClassifier()V

    .line 169
    return-void
.end method
