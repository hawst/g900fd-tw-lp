.class Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask$1;
.super Ljava/lang/Object;
.source "CommandProcessorTask.java"

# interfaces
.implements Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIntentReceived(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "id"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 80
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;

    iget-object v3, v3, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->TAG:Ljava/lang/String;

    const-string v4, "Bundle = "

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p2, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    const-string v3, "path"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 83
    const-string v3, "file://"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "subpath":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 85
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v3

    aget-object v4, v2, v7

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/configuration/StorageController;->getStorageId(Ljava/lang/String;)I

    move-result v1

    .line 86
    .local v1, "storageId":I
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask$1;->this$0:Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->mUnmountedMedia:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CommandProcessorTask;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    .end local v1    # "storageId":I
    .end local v2    # "subpath":[Ljava/lang/String;
    :cond_0
    return-void
.end method
