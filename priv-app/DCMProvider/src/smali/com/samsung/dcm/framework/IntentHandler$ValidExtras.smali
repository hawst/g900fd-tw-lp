.class public final enum Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidExtras"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

.field public static final enum Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

.field public static final enum Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

.field public static final enum Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 346
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    const-string v1, "Operation"

    const-string v2, "operation"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    .line 347
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    const-string v1, "Origin"

    const-string v2, "origin"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    .line 348
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    const-string v1, "Data"

    const-string v2, "data"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    .line 345
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 353
    iput-object p3, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->mName:Ljava/lang/String;

    .line 354
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 361
    const-string v4, "ValidExtras fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->values()[Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 363
    .local v1, "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    iget-object v4, v1, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 367
    .end local v1    # "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    :goto_1
    return-object v1

    .line 362
    .restart local v1    # "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 367
    .end local v1    # "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 345
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->mName:Ljava/lang/String;

    return-object v0
.end method
