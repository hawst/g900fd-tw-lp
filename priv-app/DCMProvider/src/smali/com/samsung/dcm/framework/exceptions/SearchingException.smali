.class public Lcom/samsung/dcm/framework/exceptions/SearchingException;
.super Lcom/samsung/dcm/framework/exceptions/LuceneException;
.source "SearchingException.java"


# static fields
.field private static final serialVersionUID:J = 0x69a45722066eb72bL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/Throwable;)V

    .line 20
    return-void
.end method
