.class Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;
.super Landroid/util/LruCache;
.source "NamedPlaceExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AddressCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Lcom/samsung/commons/PointD;",
        "Lcom/samsung/dcm/documents/LocationData;",
        ">;"
    }
.end annotation


# instance fields
.field protected mGeocoder:Landroid/location/Geocoder;


# direct methods
.method public constructor <init>(ILandroid/location/Geocoder;)V
    .locals 0
    .param p1, "num"    # I
    .param p2, "geoCoder"    # Landroid/location/Geocoder;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    .line 53
    iput-object p2, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;->mGeocoder:Landroid/location/Geocoder;

    .line 54
    return-void
.end method


# virtual methods
.method protected create(Lcom/samsung/commons/PointD;)Lcom/samsung/dcm/documents/LocationData;
    .locals 11
    .param p1, "key"    # Lcom/samsung/commons/PointD;

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;->mGeocoder:Landroid/location/Geocoder;

    iget-wide v2, p1, Lcom/samsung/commons/PointD;->x:D

    iget-wide v4, p1, Lcom/samsung/commons/PointD;->y:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v8

    .line 61
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v8, :cond_0

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    .end local v8    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_0
    :goto_0
    return-object v7

    .line 64
    .restart local v8    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Address;

    # invokes: Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->getLocationDataFromAddress(Landroid/location/Address;)Lcom/samsung/dcm/documents/LocationData;
    invoke-static {v1}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->access$000(Landroid/location/Address;)Lcom/samsung/dcm/documents/LocationData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 65
    .local v7, "l":Lcom/samsung/dcm/documents/LocationData;
    goto :goto_0

    .line 66
    .end local v7    # "l":Lcom/samsung/dcm/documents/LocationData;
    .end local v8    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Location error: "

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v0, v3, v9

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic create(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 48
    check-cast p1, Lcom/samsung/commons/PointD;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/dcm/framework/extractors/NamedPlaceExtractor$AddressCache;->create(Lcom/samsung/commons/PointD;)Lcom/samsung/dcm/documents/LocationData;

    move-result-object v0

    return-object v0
.end method
