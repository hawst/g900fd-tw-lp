.class public Lcom/samsung/dcm/framework/extractormanager/DCMContainer;
.super Ljava/lang/Object;
.source "DCMContainer.java"

# interfaces
.implements Lcom/samsung/dcm/framework/extractormanager/CommandListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;
    }
.end annotation


# static fields
.field private static final NO_COUNT:I = 0x3

.field private static final NO_DELETES:I = 0x2

.field private static final NO_INSERTS:I = 0x0

.field private static final NO_UPDATES:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isBulkProcessing:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

.field private mContentIndex_ForHigh:I

.field private mContentIndex_ForLow:I

.field private mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation
.end field

.field private mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;

    .line 45
    iput v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForLow:I

    .line 46
    iput v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForHigh:I

    .line 49
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mHandler:Landroid/os/Handler;

    .line 55
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    .line 62
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    .line 98
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    .line 99
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    .line 100
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    .line 101
    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;Ljava/util/concurrent/locks/ReentrantLock;Ljava/util/concurrent/locks/Condition;)V
    .locals 2
    .param p4, "lock"    # Ljava/util/concurrent/locks/ReentrantLock;
    .param p5, "condition"    # Ljava/util/concurrent/locks/Condition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;",
            "Ljava/util/concurrent/locks/ReentrantLock;",
            "Ljava/util/concurrent/locks/Condition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .local p1, "insertHashSet":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .local p2, "updateHashSet":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .local p3, "deleteHashSet":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;

    .line 45
    iput v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForLow:I

    .line 46
    iput v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForHigh:I

    .line 49
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mHandler:Landroid/os/Handler;

    .line 55
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    .line 62
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    .line 112
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    .line 113
    iput-object p2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    .line 114
    iput-object p3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    .line 115
    iput-object p4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 116
    iput-object p5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/extractormanager/DCMContainer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method private static doTryConsolidateCommand(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;)Z
    .locals 5
    .param p0, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "commmandLinkedHashSet":Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;, "Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    const/4 v1, 0x0

    .line 81
    invoke-virtual {p1, p0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82
    invoke-virtual {p1, p0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 83
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBeingProcessed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 84
    invoke-virtual {v0, p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->consolidate(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {p1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 86
    invoke-virtual {p1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 88
    :cond_0
    const/4 v1, 0x1

    .line 94
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    :goto_0
    return v1

    .line 91
    .restart local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_2
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "command busy not being consolidated"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private hasCommandToTake()Z
    .locals 1

    .prologue
    .line 460
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->hasHighPriorityCommand()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->hasLowPriorityCommand()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasHighPriorityCommand()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 469
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasHighPriorityCommand(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasHighPriorityCommand(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasHighPriorityCommand()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private hasLowPriorityCommand()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 475
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasLowPriorityCommand(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasLowPriorityCommand(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasLowPriorityCommand()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private isBusySdCardCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Z
    .locals 1
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 628
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyOnProcessingFinished()V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$2;

    invoke-direct {v1, p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$2;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 506
    return-void
.end method

.method private notifyOnProcessingStarted()V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$1;

    invoke-direct {v1, p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer$1;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCMContainer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 492
    return-void
.end method

.method private processDelete(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 8
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 221
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 222
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v5, "Processing bulk delete: "

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 226
    .local v2, "oldvalue":Ljava/lang/Integer;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 227
    .local v0, "bulkdeleteActive":I
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 229
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    if-le v0, v7, :cond_1

    .line 234
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v5, "1. Aborting another Bulk delete for storage id "

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    .line 302
    .end local v0    # "bulkdeleteActive":I
    .end local v2    # "oldvalue":Ljava/lang/Integer;
    :goto_1
    return-void

    .restart local v2    # "oldvalue":Ljava/lang/Integer;
    :cond_0
    move v0, v3

    .line 226
    goto :goto_0

    .line 239
    .restart local v0    # "bulkdeleteActive":I
    :cond_1
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-virtual {v4, v5, v3}, Lcom/samsung/dcm/framework/configuration/StorageController;->setVolumeStatus(IZ)V

    .line 241
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clearCommandsForId(I)V

    .line 243
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clearCommandsForId(I)V

    .line 247
    .end local v0    # "bulkdeleteActive":I
    .end local v2    # "oldvalue":Ljava/lang/Integer;
    :cond_2
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 249
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 250
    .local v1, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isHighPriority()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isHighPriority()Z

    move-result v3

    if-nez v3, :cond_3

    .line 251
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->increasePriority()V

    .line 253
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 254
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 258
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto :goto_1

    .line 264
    .end local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_4
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 266
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 267
    .restart local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBeingProcessed()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 268
    invoke-virtual {p1, v7}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    .line 269
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->interrupt()V

    .line 281
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v5, "no. of inserts left: "

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    .end local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_5
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 287
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 288
    .restart local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isBusy()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 291
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->interrupt()V

    .line 300
    .end local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_6
    :goto_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 301
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signal()V

    goto/16 :goto_1

    .line 274
    .restart local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_7
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 275
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    .line 277
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandCompleted()V

    goto/16 :goto_1

    .line 294
    :cond_8
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 295
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto :goto_2
.end method

.method private processInsert(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 2
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 160
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-static {p1, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->doTryConsolidateCommand(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-static {p1, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->doTryConsolidateCommand(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 167
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    goto :goto_0

    .line 171
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 173
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 174
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 175
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    .line 176
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto :goto_0

    .line 181
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 182
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signal()V

    goto :goto_0
.end method

.method private processUpdate(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 1
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-static {p1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->doTryConsolidateCommand(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-static {p1, v0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->doTryConsolidateCommand(Lcom/samsung/dcm/framework/extractormanager/Command;Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->contains(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    goto :goto_0

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->put(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 211
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    goto :goto_0
.end method

.method private tryToTakeHighPriorityCommand()Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 355
    const/4 v2, 0x0

    .line 357
    .local v2, "i":I
    const/4 v1, 0x0

    .line 361
    .local v1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->hasHighPriorityCommand()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 365
    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForHigh:I

    rem-int/lit8 v2, v3, 0x3

    .line 366
    packed-switch v2, :pswitch_data_0

    .line 401
    :cond_1
    :goto_0
    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForHigh:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForHigh:I

    .line 402
    if-eqz v1, :cond_0

    .line 407
    :cond_2
    return-object v1

    .line 368
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasHighPriorityCommand(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 371
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    invoke-virtual {v3, v5, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->peekLhsPriorityCommand(ZLcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 372
    if-eqz v1, :cond_3

    .line 373
    invoke-virtual {v1, v6}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    .line 380
    :cond_3
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 381
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    if-eqz v0, :cond_1

    .line 382
    invoke-virtual {v0, v6}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    goto :goto_0

    .line 391
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->takeHigh(Z)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 392
    goto :goto_0

    .line 395
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasHighPriorityCommand()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 396
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->take()Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private tryToTakeLowPriorityCommand()Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 411
    const/4 v2, 0x0

    .line 412
    .local v2, "i":I
    const/4 v1, 0x0

    .line 413
    .local v1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->hasLowPriorityCommand()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 415
    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForLow:I

    rem-int/lit8 v2, v3, 0x3

    .line 416
    packed-switch v2, :pswitch_data_0

    .line 450
    :cond_1
    :goto_0
    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForLow:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContentIndex_ForLow:I

    .line 451
    if-eqz v1, :cond_0

    .line 456
    :cond_2
    return-object v1

    .line 418
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasLowPriorityCommand(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 420
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;->LOW:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;

    invoke-virtual {v3, v5, v4}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->peekLhsPriorityCommand(ZLcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet$LhsPriority;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 421
    if-eqz v1, :cond_3

    .line 422
    invoke-virtual {v1, v6}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    .line 429
    :cond_3
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 430
    .local v0, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    if-eqz v0, :cond_1

    .line 431
    invoke-virtual {v0, v6}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    goto :goto_0

    .line 440
    .end local v0    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->takeLow(Z)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 441
    goto :goto_0

    .line 444
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->hasLowPriorityCommand()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->take()Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    goto :goto_0

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addStatusListener(Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;

    .prologue
    .line 663
    const-string v0, "listener is null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 664
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 665
    return-void
.end method

.method public cleanUp()V
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 637
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clear()V

    .line 638
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clear()V

    .line 639
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 643
    return-void

    .line 641
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public onAborted(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 10
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 567
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v5, "Processing command onAborted...URI = "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v7

    aput-object v7, v6, v8

    const-string v7, " start id = "

    aput-object v7, v6, v9

    const/4 v7, 0x2

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 568
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 577
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInsertFamilyCommand()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 578
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 581
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 582
    const/4 v3, 0x0

    .line 584
    .local v3, "updatecmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v3

    .line 585
    if-eqz v3, :cond_1

    .line 586
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v5, "callback done and removed update for URI = "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 588
    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V

    .line 589
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 592
    :cond_1
    if-nez v3, :cond_0

    .line 594
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBusySdCardCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 611
    .end local v3    # "updatecmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 612
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->notifyOnProcessingFinished()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 615
    :cond_3
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 617
    return-void

    .line 597
    .restart local v3    # "updatecmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_4
    :try_start_1
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 598
    .local v1, "deletecmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    if-eqz v1, :cond_2

    .line 599
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v4, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 600
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Command;->fireCommandCallback_onCommandAborted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 615
    .end local v1    # "deletecmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v3    # "updatecmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4

    .line 603
    :cond_5
    :try_start_2
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isDeleteFamilyCommand()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 604
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v5, "Bulk delete aborted for storage id "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 605
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 606
    .local v2, "oldvalue":Ljava/lang/Integer;
    const-string v4, "must be set"

    invoke-static {v2, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 608
    .local v0, "bulkdeleteActive":I
    add-int/lit8 v0, v0, -0x1

    .line 609
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onCompleted(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 9
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 510
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v4, "Processing command onCompleted... URI = "

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v5, v7

    const-string v6, " start id = "

    aput-object v6, v5, v8

    const/4 v6, 0x2

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 521
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInsertFamilyCommand()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 522
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->remove(Lcom/samsung/dcm/framework/extractormanager/Command;)Z

    .line 524
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 525
    .local v1, "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    if-eqz v1, :cond_0

    .line 526
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    .line 527
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 531
    :cond_0
    const/4 v1, 0x0

    .line 534
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 557
    .end local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 558
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->notifyOnProcessingFinished()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561
    :cond_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 563
    return-void

    .line 537
    .restart local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->getCommand(Lcom/samsung/dcm/framework/extractormanager/Command;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v1

    .line 538
    if-eqz v1, :cond_1

    .line 539
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/samsung/dcm/framework/extractormanager/Command;->setBusy(Z)V

    .line 540
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mContainerNotEmptyCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 561
    .end local v1    # "c":Lcom/samsung/dcm/framework/extractormanager/Command;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v3

    .line 545
    :cond_4
    :try_start_2
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isDeleteFamilyCommand()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 546
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v4, "Bulk delete finished for storage id "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 548
    .local v2, "oldvalue":Ljava/lang/Integer;
    const-string v3, "must be set"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 550
    .local v0, "bulkdeleteActive":I
    add-int/lit8 v0, v0, -0x1

    .line 552
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->isBulkProcessing:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getStorageId()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/samsung/dcm/framework/configuration/StorageController;->setVolumeStatus(IZ)V

    .line 555
    const-class v3, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v3}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->onBulkDeleteCompleted()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public put(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 10
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 122
    .local v0, "starttime":J
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 124
    :try_start_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "PUT locked:"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, " isSdCardAction"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isSdCardAction()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->notifyOnProcessingStarted()V

    .line 130
    :cond_0
    invoke-virtual {p1, p0}, Lcom/samsung/dcm/framework/extractormanager/Command;->addCommandCallback(Lcom/samsung/dcm/framework/extractormanager/CommandListener;)V

    .line 133
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isInsertFamilyCommand()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 134
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->processInsert(Lcom/samsung/dcm/framework/extractormanager/Command;)V

    .line 144
    :cond_1
    :goto_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "PUT unlocked ("

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, ","

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, ","

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 148
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "[DCM_Performance] PUT time taken = "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, " ms"

    aput-object v5, v4, v9

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    return-void

    .line 137
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isUpdateFamilyCommand()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 138
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->processUpdate(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 148
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v4, "[DCM_Performance] PUT time taken = "

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    const-string v6, " ms"

    aput-object v6, v5, v9

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v2

    .line 141
    :cond_3
    :try_start_2
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/Command;->isDeleteFamilyCommand()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->processDelete(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public removeStatusListener(Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;

    .prologue
    .line 668
    const-string v0, "listener is null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 670
    return-void
.end method

.method public shutdownNow()V
    .locals 2

    .prologue
    .line 649
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 653
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clear()V

    .line 654
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clear()V

    .line 655
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 658
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 660
    return-void

    .line 658
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 308
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    .line 310
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public take()Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 315
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "TAKE locked"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 318
    const/4 v0, 0x0

    .line 320
    .local v0, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->hasCommandToTake()Z

    move-result v2

    if-nez v2, :cond_0

    .line 321
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "Queue is empty so exit"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 347
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "TAKE unlocked ("

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const-string v5, ","

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, ","

    aput-object v5, v4, v10

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x5

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    .end local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .local v1, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :goto_0
    return-object v1

    .line 332
    .end local v1    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->tryToTakeHighPriorityCommand()Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 333
    if-eqz v0, :cond_1

    .line 335
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "Taking high priority command("

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, ") "

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 347
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "TAKE unlocked ("

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const-string v5, ","

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, ","

    aput-object v5, v4, v10

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x5

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    .end local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v1    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    goto :goto_0

    .line 340
    .end local v1    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->tryToTakeLowPriorityCommand()Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    .line 341
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "Taking low priority command("

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->getCommandIds()Ljava/util/HashSet;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 346
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 347
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v3, "TAKE unlocked ("

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const-string v5, ","

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, ","

    aput-object v5, v4, v10

    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x5

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    .end local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v1    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    goto/16 :goto_0

    .line 346
    .end local v1    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v0    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 347
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->TAG:Ljava/lang/String;

    const-string v4, "TAKE unlocked ("

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const-string v6, ","

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    const-string v6, ","

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v11

    const/4 v6, 0x5

    const-string v7, ")"

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 674
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 678
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 679
    .local v0, "s":Ljava/lang/StringBuilder;
    const-string v1, "<I>[ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeHigh(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 681
    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeHigh(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 683
    const-string v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeLow(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 685
    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mInsertCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeLow(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 687
    const-string v1, " ] , <U>[ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeHigh(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 689
    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeHigh(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 691
    const-string v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeLow(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 693
    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mUpdateCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeLow(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 695
    const-string v1, " ] , <D>[ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeHigh()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 697
    const-string v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mDeleteCommands:Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/DCMLinkedHashSet;->sizeLow()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 699
    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 702
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v1

    .end local v0    # "s":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMContainer;->mLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method
