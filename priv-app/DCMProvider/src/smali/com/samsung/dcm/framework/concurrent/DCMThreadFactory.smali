.class public Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
.super Ljava/lang/Object;
.source "DCMThreadFactory.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# static fields
.field private static final MAX_BROADCAST_THREAD:I = 0x1

.field private static final MAX_HEAVY_THREAD:I = 0x2

.field private static final MAX_LUCENE_THREAD:I = 0x1

.field private static final MAX_REBUILD_THREAD:I = 0x1

.field private static final MAX_WORKER_THREAD:I = 0x3

.field private static mCommandProcessorFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

.field private static mCommandProcessorQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static mHeavyFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

.field private static mHeavyQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static mLuceneFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

.field private static mLuceneQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static mNotificationFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

.field private static mPrimaryFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

.field private static mPrimaryQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static mSyncFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

.field private static mSyncQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mMaxThreadCount:I

.field private mMinThreadCount:I

.field private final mThreadName:Ljava/lang/String;

.field private mThreadNo:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 38
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    const-string v1, "WORKER"

    invoke-direct {v0, v1, v4, v4}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mPrimaryFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    .line 40
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    const-string v1, "REBUILD"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mSyncFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    .line 42
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    const-string v1, "HEAVY"

    invoke-direct {v0, v1, v3, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mHeavyFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    .line 45
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    const-string v1, "LUCENE"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mLuceneFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    .line 48
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    const-string v1, "BROADCASET"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mNotificationFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    .line 51
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mPrimaryQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 53
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mSyncQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 55
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mHeavyQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 58
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mLuceneQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 61
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    const-string v1, "COMMAND_PROCESSOR"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mCommandProcessorFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    .line 62
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mCommandProcessorQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "max_thread"    # I
    .param p3, "min_thread"    # I

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mThreadNo:I

    .line 66
    iput v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mMaxThreadCount:I

    .line 68
    iput v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mMinThreadCount:I

    .line 71
    iput-object p1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mThreadName:Ljava/lang/String;

    .line 72
    iput p2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mMaxThreadCount:I

    .line 73
    iput p3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mMinThreadCount:I

    .line 74
    return-void
.end method

.method public static getCommandProcessorFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mCommandProcessorFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    return-object v0
.end method

.method public static getCommandProcessorQueue()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mCommandProcessorQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public static getHeavyFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mHeavyFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    return-object v0
.end method

.method public static getHeavyQueue()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mHeavyQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public static getLuceneFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mLuceneFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    return-object v0
.end method

.method public static getLuceneQueue()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mLuceneQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public static getNotificationFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mNotificationFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    return-object v0
.end method

.method public static getPrimaryFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mPrimaryFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    return-object v0
.end method

.method public static getPrimaryQueue()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mPrimaryQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public static getSyncFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mSyncFactory:Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    return-object v0
.end method

.method public static getSyncQueue()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mSyncQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method


# virtual methods
.method public getMaxThreadCount()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mMaxThreadCount:I

    return v0
.end method

.method public getMinThreadCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mMinThreadCount:I

    return v0
.end method

.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 4
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 86
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 87
    .local v0, "thread":Ljava/lang/Thread;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mThreadName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mThreadNo:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->mThreadNo:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 88
    new-instance v1, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory$1;

    invoke-direct {v1, p0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory$1;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;)V

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 96
    return-object v0
.end method
