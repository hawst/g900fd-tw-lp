.class Lcom/samsung/dcm/framework/extractormanager/DCMController$3;
.super Ljava/lang/Object;
.source "DCMController.java"

# interfaces
.implements Lcom/samsung/dcm/framework/extractormanager/CommandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMController;->indexAndGet(Landroid/net/Uri;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$result:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iput-object p2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->val$result:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAborted(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 2
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 547
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->val$result:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 548
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 549
    return-void
.end method

.method public onCompleted(Lcom/samsung/dcm/framework/extractormanager/Command;)V
    .locals 2
    .param p1, "command"    # Lcom/samsung/dcm/framework/extractormanager/Command;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->val$result:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 542
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$3;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 543
    return-void
.end method
