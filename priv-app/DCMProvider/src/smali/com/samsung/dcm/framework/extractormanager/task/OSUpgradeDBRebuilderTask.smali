.class public Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "OSUpgradeDBRebuilderTask.java"


# static fields
.field private static final DB_OSUPGRADE_PROJECTION:[Ljava/lang/String;

.field private static final FIELD_NAMED_LOCATION:Ljava/lang/String; = "namedlocation"

.field private static final FIELD_PERSON:Ljava/lang/String; = "person"

.field private static final FIELD_USER_TAG:Ljava/lang/String; = "user_tag"

.field private static final NO_BUCKET_ID:I = 0x2

.field private static final NO_STORAGE_ID:I = 0x1


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mIsExternalSDCard:Z

.field private mMediaStoreClient:Landroid/content/ContentProviderClient;

.field private mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private mNRTSearcherId:J

.field private mStorageId:I

.field private mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 99
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "storage_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->DB_OSUPGRADE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storageId"    # I
    .param p3, "isExternalSDCard"    # Z

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 89
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 112
    const-string v1, "OSUpgradeDBRebuilderTask needs context"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContext:Landroid/content/Context;

    .line 113
    iput p2, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mStorageId:I

    .line 114
    iput-boolean p3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mIsExternalSDCard:Z

    .line 115
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 116
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "ContentResolver cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 118
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    const-string v1, "Config cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 120
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 121
    return-void
.end method

.method private MakeStorageQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 181
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 183
    .local v2, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v0, 0x0

    .line 186
    .local v0, "rangeQuery1":Lorg/apache/lucene/search/Query;
    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mStorageId:I

    .line 188
    .local v3, "storageid1":I
    iget-boolean v5, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mIsExternalSDCard:Z

    if-eqz v5, :cond_0

    .line 189
    const/4 v4, 0x1

    .line 193
    .local v4, "storageid2":I
    :goto_0
    const-string v5, "storage_id"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v5, v6, v7, v8, v8}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    .line 195
    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v0, v5}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 197
    const/4 v1, 0x0

    .line 198
    .local v1, "rangeQuery2":Lorg/apache/lucene/search/Query;
    const-string v5, "storage_id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v5, v6, v7, v8, v8}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 201
    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v1, v5}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 204
    return-object v2

    .line 191
    .end local v1    # "rangeQuery2":Lorg/apache/lucene/search/Query;
    .end local v4    # "storageid2":I
    :cond_0
    const/4 v4, 0x0

    .restart local v4    # "storageid2":I
    goto :goto_0
.end method

.method private addDataFromCalendarExtractor(Ljava/util/Set;Lorg/apache/lucene/document/Document;)V
    .locals 15
    .param p2, "document"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;",
            "Lorg/apache/lucene/document/Document;",
            ")V"
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 417
    .local v5, "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 419
    .local v1, "data":Ljava/lang/Object;
    instance-of v11, v1, Landroid/os/Bundle;

    if-eqz v11, :cond_0

    move-object v8, v1

    .line 420
    check-cast v8, Landroid/os/Bundle;

    .line 422
    .local v8, "inBundle":Landroid/os/Bundle;
    const-string v11, "EVENT_LIST"

    invoke-virtual {v8, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 423
    const-string v11, "EVENT_LIST"

    invoke-virtual {v8, v11}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 426
    .local v4, "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "calendar_event"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 427
    const-string v11, "event_uri"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 429
    if-eqz v4, :cond_0

    .line 430
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v6, v11, :cond_0

    .line 431
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 432
    .local v3, "event":Ljava/lang/String;
    const-string v11, " "

    invoke-virtual {v3, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 433
    .local v2, "delimPosition":I
    const/4 v11, 0x0

    invoke-virtual {v3, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 434
    .local v10, "uri":Ljava/lang/String;
    add-int/lit8 v11, v2, 0x1

    invoke-virtual {v3, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 435
    .local v9, "title":Ljava/lang/String;
    new-instance v11, Lorg/apache/lucene/document/StringField;

    const-string v12, "calendar_event"

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v11, v12, v13, v14}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 437
    new-instance v11, Lorg/apache/lucene/document/TextField;

    const-string v12, "event_uri"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v11, v12, v13, v14}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 430
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 444
    .end local v1    # "data":Ljava/lang/Object;
    .end local v2    # "delimPosition":I
    .end local v3    # "event":Ljava/lang/String;
    .end local v4    # "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v6    # "i":I
    .end local v8    # "inBundle":Landroid/os/Bundle;
    .end local v9    # "title":Ljava/lang/String;
    .end local v10    # "uri":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;
    .locals 6
    .param p1, "indexableFields"    # [Lorg/apache/lucene/index/IndexableField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/lucene/index/IndexableField;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 402
    .local v1, "fields":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object v0, p1

    .local v0, "arr$":[Lorg/apache/lucene/index/IndexableField;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v3, v0, v2

    .line 403
    .local v3, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 402
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 406
    .end local v3    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_0
    return-object v1
.end method

.method private createLuceneReader()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 170
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 171
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTSearcherId:J

    .line 172
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v1, "createLuceneReader done "

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTSearcherId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    return v6
.end method

.method private release()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 378
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 380
    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 381
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTSearcherId:J

    .line 382
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v1, "releaseLuceneReader done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 386
    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 389
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private resyncDBForOSUpgrade()V
    .locals 20

    .prologue
    .line 211
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->MakeStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v3

    .line 212
    .local v3, "query":Lorg/apache/lucene/search/Query;
    const/16 v16, 0x0

    .line 213
    .local v16, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/16 v5, 0x1f4

    .line 214
    .local v5, "nMax":I
    const/4 v2, 0x0

    .line 216
    .local v2, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :try_start_0
    new-instance v15, Lorg/apache/lucene/search/SortField;

    const-string v1, "id"

    sget-object v4, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {v15, v1, v4}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 217
    .local v15, "sfield":Lorg/apache/lucene/search/SortField;
    new-instance v6, Lorg/apache/lucene/search/Sort;

    invoke-direct {v6, v15}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    .line 219
    .local v6, "sortby":Lorg/apache/lucene/search/Sort;
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    const/4 v4, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v16

    .line 220
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "Lucene DB update during OS upgrade started for "

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " docs"

    aput-object v19, v17, v18

    move-object/from16 v0, v17

    invoke-static {v1, v4, v0}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    const/4 v2, 0x0

    .line 222
    move-object/from16 v0, v16

    iget-object v7, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v7, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v12, v7

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_1

    aget-object v14, v7, v11

    .line 223
    .local v14, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTIndexSearcher:Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    iget v4, v14, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v1, v4, v0}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v8

    .line 224
    .local v8, "doc":Lorg/apache/lucene/document/Document;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->updateDoc(Lorg/apache/lucene/document/Document;)V

    .line 225
    move-object v2, v14

    .line 222
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 228
    .end local v8    # "doc":Lorg/apache/lucene/document/Document;
    .end local v14    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateListner()Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->addRefreshListener(Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_2

    .line 230
    if-nez v2, :cond_0

    .line 242
    .end local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "sfield":Lorg/apache/lucene/search/SortField;
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContext:Landroid/content/Context;

    const-string v4, "Pref"

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v1, v4, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 243
    .local v13, "pref":Landroid/content/SharedPreferences;
    if-eqz v13, :cond_2

    .line 244
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 245
    .local v10, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v10, :cond_2

    .line 246
    sget-object v1, Lcom/samsung/dcm/framework/utils/SystemUtils;->kKeyHeavyStartFlag:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v10, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 247
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 248
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "setHeavySharedPref set as "

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v17

    invoke-static {v1, v4, v0}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    .end local v10    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    return-void

    .line 232
    .end local v13    # "pref":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v9

    .line 233
    .local v9, "e":Ljava/lang/OutOfMemoryError;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "OOM exception"

    invoke-static {v1, v4, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 234
    .end local v9    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v9

    .line 235
    .local v9, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "Exception"

    invoke-static {v1, v4, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 236
    .end local v9    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v9

    .line 237
    .local v9, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "SearchingException "

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Lcom/samsung/dcm/framework/exceptions/SearchingException;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v1, v4, v0}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private updateDoc(Lorg/apache/lucene/document/Document;)V
    .locals 21
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 335
    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 336
    .local v16, "external_uri":Landroid/net/Uri;
    const-string v2, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    .line 337
    .local v18, "id":Ljava/lang/Integer;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 338
    .local v3, "uri":Landroid/net/Uri;
    const/16 v19, -0x1

    .line 339
    .local v19, "storageId":I
    const/16 v17, -0x1

    .line 340
    .local v17, "fatVolumeId":I
    const-wide/16 v12, -0x1

    .line 341
    .local v12, "bucketId":J
    const/4 v14, 0x0

    .line 343
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->DB_OSUPGRADE_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v14

    .line 346
    if-eqz v14, :cond_4

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_4

    move-wide v8, v12

    .end local v12    # "bucketId":J
    .local v8, "bucketId":J
    move/from16 v7, v17

    .end local v17    # "fatVolumeId":I
    .local v7, "fatVolumeId":I
    move/from16 v6, v19

    .line 347
    .end local v19    # "storageId":I
    .local v6, "storageId":I
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 348
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 349
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 350
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/samsung/dcm/framework/configuration/StorageController;->getFatVolumeId(I)I

    move-result v7

    .line 353
    :cond_1
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 354
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    goto :goto_0

    .line 357
    :cond_2
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTSearcherId:J

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v11}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->updateLuceneDB(Lorg/apache/lucene/document/Document;IIJJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 367
    :goto_1
    if-eqz v14, :cond_3

    .line 368
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 372
    :cond_3
    :goto_2
    return-void

    .line 359
    .end local v6    # "storageId":I
    .end local v7    # "fatVolumeId":I
    .end local v8    # "bucketId":J
    .restart local v12    # "bucketId":J
    .restart local v17    # "fatVolumeId":I
    .restart local v19    # "storageId":I
    :cond_4
    :try_start_2
    new-instance v20, Lorg/apache/lucene/index/Term;

    const-string v2, "uri"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    .local v20, "term":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTSearcherId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v2, v4, v0, v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->deleteDocuments(Ljava/lang/Long;Lorg/apache/lucene/index/Term;Ljava/lang/Integer;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide v8, v12

    .end local v12    # "bucketId":J
    .restart local v8    # "bucketId":J
    move/from16 v7, v17

    .end local v17    # "fatVolumeId":I
    .restart local v7    # "fatVolumeId":I
    move/from16 v6, v19

    .end local v19    # "storageId":I
    .restart local v6    # "storageId":I
    goto :goto_1

    .line 362
    .end local v6    # "storageId":I
    .end local v7    # "fatVolumeId":I
    .end local v8    # "bucketId":J
    .end local v20    # "term":Lorg/apache/lucene/index/Term;
    .restart local v12    # "bucketId":J
    .restart local v17    # "fatVolumeId":I
    .restart local v19    # "storageId":I
    :catch_0
    move-exception v15

    move-wide v8, v12

    .end local v12    # "bucketId":J
    .restart local v8    # "bucketId":J
    move/from16 v7, v17

    .end local v17    # "fatVolumeId":I
    .restart local v7    # "fatVolumeId":I
    move/from16 v6, v19

    .line 363
    .end local v19    # "storageId":I
    .restart local v6    # "storageId":I
    .local v15, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "Unhandled exception"

    invoke-static {v2, v4, v15}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 367
    if-eqz v14, :cond_3

    .line 368
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 367
    .end local v6    # "storageId":I
    .end local v7    # "fatVolumeId":I
    .end local v8    # "bucketId":J
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v12    # "bucketId":J
    .restart local v17    # "fatVolumeId":I
    .restart local v19    # "storageId":I
    :catchall_0
    move-exception v2

    move-wide v8, v12

    .end local v12    # "bucketId":J
    .restart local v8    # "bucketId":J
    move/from16 v7, v17

    .end local v17    # "fatVolumeId":I
    .restart local v7    # "fatVolumeId":I
    move/from16 v6, v19

    .end local v19    # "storageId":I
    .restart local v6    # "storageId":I
    :goto_4
    if-eqz v14, :cond_5

    .line 368
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    .line 367
    :catchall_1
    move-exception v2

    goto :goto_4

    .line 362
    :catch_1
    move-exception v15

    goto :goto_3
.end method

.method private updateLuceneDB(Lorg/apache/lucene/document/Document;IIJJ)V
    .locals 26
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "storageId"    # I
    .param p3, "fatVolumeId"    # I
    .param p4, "bucketId"    # J
    .param p6, "searchId"    # J

    .prologue
    .line 255
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v5, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v4, Lorg/apache/lucene/document/Document;

    invoke-direct {v4}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 258
    .local v4, "newDoc":Lorg/apache/lucene/document/Document;
    const-string v3, "person"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v17

    .line 260
    .local v17, "existingPersonNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v3, "user_tag"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v18

    .line 262
    .local v18, "existingUserTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v3, "namedlocation"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->convertToStringSet([Lorg/apache/lucene/index/IndexableField;)Ljava/util/Set;

    move-result-object v16

    .line 266
    .local v16, "existingNamedLocation":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x0

    new-array v3, v3, [Z

    move-object/from16 v0, p1

    invoke-static {v0, v5, v4, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 268
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v3

    const-class v6, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v3, v6}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v20

    .line 271
    .local v20, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    const-string v3, "storage_id"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 272
    new-instance v3, Lorg/apache/lucene/document/IntField;

    const-string v6, "storage_id"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move/from16 v0, p2

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 274
    const-string v3, "person"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 275
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 276
    .local v24, "tag":Ljava/lang/String;
    new-instance v3, Lorg/apache/lucene/document/StringField;

    const-string v6, "person_names"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v24

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 279
    .end local v24    # "tag":Ljava/lang/String;
    :cond_0
    const-string v3, "user_tag"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 280
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_1
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 281
    .restart local v24    # "tag":Ljava/lang/String;
    new-instance v3, Lorg/apache/lucene/document/StringField;

    const-string v6, "user_tags"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v24

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_1

    .line 284
    .end local v24    # "tag":Ljava/lang/String;
    :cond_1
    const-string v3, "namedlocation"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 285
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 286
    .restart local v24    # "tag":Ljava/lang/String;
    new-instance v3, Lorg/apache/lucene/document/StringField;

    const-string v6, "named_location"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v24

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_2

    .line 292
    .end local v24    # "tag":Ljava/lang/String;
    :cond_2
    const-string v3, "osc_status"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    if-nez v3, :cond_3

    .line 293
    if-eqz v20, :cond_4

    .line 294
    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_3
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 295
    .local v19, "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    new-instance v22, Lorg/apache/lucene/document/StringField;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "false"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v22

    invoke-direct {v0, v3, v6, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    .line 297
    .local v22, "field":Lorg/apache/lucene/document/StringField;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Plugin"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, " set as false"

    aput-object v9, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_3

    .line 302
    .end local v19    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v22    # "field":Lorg/apache/lucene/document/StringField;
    :cond_3
    if-eqz v20, :cond_4

    .line 303
    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 304
    .restart local v19    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    new-instance v22, Lorg/apache/lucene/document/StringField;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "true"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v22

    invoke-direct {v0, v3, v6, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    .line 306
    .restart local v22    # "field":Lorg/apache/lucene/document/StringField;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Plugin"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, " set as true"

    aput-object v9, v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_4

    .line 311
    .end local v19    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v22    # "field":Lorg/apache/lucene/document/StringField;
    :cond_4
    new-instance v3, Lorg/apache/lucene/document/LongField;

    const-string v6, "bucket_id"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-wide/from16 v0, p4

    invoke-direct {v3, v6, v0, v1, v7}, Lorg/apache/lucene/document/LongField;-><init>(Ljava/lang/String;JLorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 312
    new-instance v3, Lorg/apache/lucene/document/IntField;

    const-string v6, "fatvolume_id"

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move/from16 v0, p3

    invoke-direct {v3, v6, v0, v7}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 315
    new-instance v21, Ljava/util/HashSet;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashSet;-><init>()V

    .line 316
    .local v21, "extractorsList":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v3

    const-class v6, Lcom/samsung/dcm/framework/extractors/CalendarEventExtractor;

    invoke-virtual {v3, v6}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorInstance(Ljava/lang/Class;)Lcom/samsung/dcm/framework/extractors/Extractor;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 317
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->addDataFromCalendarExtractor(Ljava/util/Set;Lorg/apache/lucene/document/Document;)V

    .line 320
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-wide/from16 v6, p6

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    move-result-wide v14

    .line 322
    .local v14, "commitId":J
    cmp-long v3, v14, p6

    if-eqz v3, :cond_5

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v3, p6, v6

    if-eqz v3, :cond_5

    .line 323
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v6, "Colision! Reupdating..."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 325
    const-wide v12, 0x7fffffffffffffffL

    move-object/from16 v6, p0

    move-object v7, v4

    move/from16 v8, p2

    move/from16 v9, p3

    move-wide/from16 v10, p4

    invoke-direct/range {v6 .. v13}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->updateLuceneDB(Lorg/apache/lucene/document/Document;IIJJ)V
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    .end local v14    # "commitId":J
    :cond_5
    :goto_5
    return-void

    .line 328
    :catch_0
    move-exception v2

    .line 329
    .local v2, "e1":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/exceptions/LuceneException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5
.end method


# virtual methods
.method protected internalRun()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 125
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 127
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->createLuceneReader()Z

    .line 128
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v1, "resyncDBForOSUpgrade started for storage id"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mStorageId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->resyncDBForOSUpgrade()V

    .line 130
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v1, "resyncDBForOSUpgrade finished for storage id "

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mStorageId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->release()Z

    .line 134
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mIsExternalSDCard:Z

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->sendIntentForSDScanFinished()V

    .line 138
    :cond_0
    return-void
.end method

.method sendIntentForSDScanFinished()V
    .locals 8

    .prologue
    .line 145
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->TAG:Ljava/lang/String;

    const-string v4, "send intent for SD card scan finished for storage id"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mStorageId:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "com.samsung.dcm"

    const-string v4, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 150
    .local v0, "actionData":Landroid/os/Bundle;
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->SDScanFinished:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v3, "storage_id"

    iget v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mStorageId:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 153
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 154
    .local v1, "extrasBundle":Landroid/os/Bundle;
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 160
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 162
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/OSUpgradeDBRebuilderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 163
    return-void
.end method
