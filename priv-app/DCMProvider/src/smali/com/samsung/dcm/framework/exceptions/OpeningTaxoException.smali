.class public Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
.super Lcom/samsung/dcm/framework/exceptions/LuceneException;
.source "OpeningTaxoException.java"


# static fields
.field private static final serialVersionUID:J = 0x69a45722066eb72fL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>()V

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/Throwable;)V

    .line 17
    return-void
.end method
