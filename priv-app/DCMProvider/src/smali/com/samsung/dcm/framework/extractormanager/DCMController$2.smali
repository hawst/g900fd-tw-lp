.class Lcom/samsung/dcm/framework/extractormanager/DCMController$2;
.super Ljava/lang/Object;
.source "DCMController.java"

# interfaces
.implements Lcom/samsung/dcm/framework/extractormanager/DCMContainer$DCMContainerStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/DCMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;


# direct methods
.method constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProcessingFinished()V
    .locals 3

    .prologue
    .line 192
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "DCMContainer processing finished"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->postOscStartIntent(Z)V

    .line 194
    return-void
.end method

.method public onProcessingStarted()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 177
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$700(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # setter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mbManagerSetupDone:Z
    invoke-static {v0, v5}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$702(Lcom/samsung/dcm/framework/extractormanager/DCMController;Z)Z

    .line 179
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$800(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->unRegisterListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;)V

    .line 180
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mSystemBroadcastReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$800(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iget-object v2, v2, Lcom/samsung/dcm/framework/extractormanager/DCMController;->mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V

    .line 181
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "Setup done"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, " Normal events registered"

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    :cond_0
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;->TAG:Ljava/lang/String;

    const-string v1, "DCMContainer processing started"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExecutor:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$400(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    move-result-object v0

    new-array v1, v5, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;
    invoke-static {v2}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$500(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/DCMController$2;->this$0:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    # getter for: Lcom/samsung/dcm/framework/extractormanager/DCMController;->mHeavyExtractionTask:Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;
    invoke-static {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->access$500(Lcom/samsung/dcm/framework/extractormanager/DCMController;)Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->finishNow()V

    .line 188
    :cond_1
    return-void
.end method
