.class public Lcom/samsung/dcm/framework/utils/DCMUtilities;
.super Ljava/lang/Object;
.source "DCMUtilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/utils/DCMUtilities$1;,
        Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    }
.end annotation


# static fields
.field private static final MAP_FIELD_TO_TYPE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/document/Field;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final kKeyAutoRebuild:Ljava/lang/String; = "isAutoRebuildDone"

.field private static kKeyCalendarProcessing:Ljava/lang/String;

.field private static kKeyIntentProcessingFinished:Ljava/lang/String;

.field private static kKeyOnCommit:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 84
    const-class v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    .line 86
    const-string v0, "KeyOnCommit"

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyOnCommit:Ljava/lang/String;

    .line 87
    const-string v0, "IntentProcessingFinished"

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyIntentProcessingFinished:Ljava/lang/String;

    .line 88
    const-string v0, "CalendarProcessing"

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyCalendarProcessing:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    .line 96
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "date"

    const-class v2, Lorg/apache/lucene/document/LongField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "bucket_id"

    const-class v2, Lorg/apache/lucene/document/LongField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "mimetype"

    const-class v2, Lorg/apache/lucene/document/StoredField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "categories"

    const-class v2, Lorg/apache/lucene/document/StoredField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "faceCount"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "id"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "doctype"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "storage_id"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "deleted"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "osc_status"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "person_names"

    const-class v2, Lorg/apache/lucene/document/TextField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "event_uri"

    const-class v2, Lorg/apache/lucene/document/TextField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    const-string v1, "fatvolume_id"

    const-class v2, Lorg/apache/lucene/document/IntField;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    return-void
.end method

.method public static afterCommit(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 251
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    const-string v2, "Pref"

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 253
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 254
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 255
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 256
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyOnCommit:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 257
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 258
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "afterCommit; setting kKeyOnCommit to true"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public static beforeCommit(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 238
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    const-string v2, "Pref"

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 240
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 241
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 242
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 243
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyOnCommit:Ljava/lang/String;

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 244
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 245
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "beforCommit; setting kKeyOnCommit to false"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public static fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V
    .locals 13
    .param p0, "newnrtSearcher"    # Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 362
    .local p2, "updatedImageDocList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Document;>;"
    const-string v0, "Searcher cannot be null"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    const-string v0, "List cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    new-instance v2, Lorg/apache/lucene/search/TermQuery;

    new-instance v0, Lorg/apache/lucene/index/Term;

    const-string v1, "uri"

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 367
    .local v2, "uriQuery":Lorg/apache/lucene/search/Query;
    const/4 v12, 0x0

    .line 369
    .local v12, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x1388

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v12

    .line 371
    iget-object v6, v12, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v6, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v11, v6, v9

    .line 372
    .local v11, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget v0, v11, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v7

    .line 373
    .local v7, "doc":Lorg/apache/lucene/document/Document;
    invoke-interface {p2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 371
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 375
    .end local v6    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v7    # "doc":Lorg/apache/lucene/document/Document;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catch_0
    move-exception v8

    .line 376
    .local v8, "e":Ljava/io/IOException;
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v1, "IO Exception"

    invoke-static {v0, v1, v8}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 380
    .end local v8    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    return-void

    .line 377
    :catch_1
    move-exception v8

    .line 378
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v1, "Exception"

    invoke-static {v0, v1, v8}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static varargs getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V
    .locals 27
    .param p0, "currentDoc"    # Lorg/apache/lucene/document/Document;
    .param p2, "newDoc"    # Lorg/apache/lucene/document/Document;
    .param p3, "bs"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/ArrayList",
            "<TE;>;",
            "Lorg/apache/lucene/document/Document;",
            "[Z)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TE;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v17

    .line 161
    .local v17, "oldDocfields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexableField;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/index/IndexableField;

    .line 162
    .local v13, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v22

    const-string v23, "location__x"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 164
    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v16

    .line 165
    .local v16, "lon":Ljava/lang/String;
    const-string v22, "location__y"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 167
    .local v14, "lat":Ljava/lang/String;
    if-eqz v14, :cond_0

    if-eqz v16, :cond_0

    .line 168
    sget-object v19, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    .line 169
    .local v19, "spatialContext":Lcom/spatial4j/core/context/SpatialContext;
    new-instance v20, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;

    const-string v22, "location"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/spatial/vector/PointVectorStrategy;-><init>(Lcom/spatial4j/core/context/SpatialContext;Ljava/lang/String;)V

    .line 172
    .local v20, "spatialStrategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v18

    .line 174
    .local v18, "point":Lcom/spatial4j/core/shape/Shape;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/spatial/SpatialStrategy;->createIndexableFields(Lcom/spatial4j/core/shape/Shape;)[Lorg/apache/lucene/document/Field;

    move-result-object v6

    .local v6, "arr$":[Lorg/apache/lucene/document/Field;
    array-length v15, v6

    .local v15, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_1
    if-ge v12, v15, :cond_0

    aget-object v9, v6, v12

    .line 176
    .local v9, "f":Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 174
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 180
    .end local v6    # "arr$":[Lorg/apache/lucene/document/Field;
    .end local v9    # "f":Lorg/apache/lucene/index/IndexableField;
    .end local v12    # "i$":I
    .end local v14    # "lat":Ljava/lang/String;
    .end local v15    # "len$":I
    .end local v16    # "lon":Ljava/lang/String;
    .end local v18    # "point":Lcom/spatial4j/core/shape/Shape;
    .end local v19    # "spatialContext":Lcom/spatial4j/core/context/SpatialContext;
    .end local v20    # "spatialStrategy":Lorg/apache/lucene/spatial/SpatialStrategy;
    :cond_1
    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v22

    const-string v23, "location__y"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_0

    .line 185
    const/4 v10, 0x0

    .line 186
    .local v10, "fieldClassType":Ljava/lang/String;
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities;->MAP_FIELD_TO_TYPE:Ljava/util/Map;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    .line 187
    .local v7, "classType":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/document/Field;>;"
    if-nez v7, :cond_2

    .line 188
    const-class v7, Lorg/apache/lucene/document/StringField;

    .line 191
    :cond_2
    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    .line 192
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v23, "IF ="

    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, " clname "

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v10, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    invoke-static {v10}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->getDataType(Ljava/lang/String;)Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;

    move-result-object v8

    .line 195
    .local v8, "datatype":Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities$1;->$SwitchMap$com$samsung$dcm$framework$utils$DCMUtilities$LuceneDataType:[I

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_0

    .line 231
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v23, "Unsupported fieldClassType"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v10, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 197
    :pswitch_0
    new-instance v22, Lorg/apache/lucene/document/TextField;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v24

    sget-object v25, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v22 .. v25}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0

    .line 201
    :pswitch_1
    new-instance v22, Lorg/apache/lucene/document/IntField;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Number;->intValue()I

    move-result v24

    sget-object v25, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v22 .. v25}, Lorg/apache/lucene/document/IntField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0

    .line 205
    :pswitch_2
    new-instance v22, Lorg/apache/lucene/document/LongField;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Number;->longValue()J

    move-result-wide v24

    sget-object v26, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct/range {v22 .. v26}, Lorg/apache/lucene/document/LongField;-><init>(Ljava/lang/String;JLorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0

    .line 209
    :pswitch_3
    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v22

    const-string v23, "categories"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 210
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v22, v0

    if-nez v22, :cond_4

    .line 211
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v23, "Category"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    new-instance v22, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v23

    const-string v24, ";"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_3
    :goto_2
    new-instance v22, Lorg/apache/lucene/document/StoredField;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v22 .. v24}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0

    .line 214
    :cond_4
    const/16 v22, 0x0

    aget-boolean v22, p3, v22

    if-eqz v22, :cond_3

    .line 215
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v23, "String"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 222
    :pswitch_4
    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v21

    .line 223
    .local v21, "value":Ljava/lang/String;
    if-eqz v21, :cond_5

    .line 224
    new-instance v22, Lorg/apache/lucene/document/StringField;

    invoke-interface {v13}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v23

    sget-object v24, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto/16 :goto_0

    .line 227
    :cond_5
    sget-object v22, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v23, "StringField value is null"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 235
    .end local v7    # "classType":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/document/Field;>;"
    .end local v8    # "datatype":Lcom/samsung/dcm/framework/utils/DCMUtilities$LuceneDataType;
    .end local v10    # "fieldClassType":Ljava/lang/String;
    .end local v13    # "iF":Lorg/apache/lucene/index/IndexableField;
    .end local v21    # "value":Ljava/lang/String;
    :cond_6
    return-void

    .line 195
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getIsRebuildDoneStatus(Landroid/content/Context;Z)Z
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "defaulStatus"    # Z

    .prologue
    const/4 v6, 0x0

    .line 350
    const-string v2, "Pref"

    invoke-virtual {p0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 351
    .local v1, "pref":Landroid/content/SharedPreferences;
    move v0, p1

    .line 352
    .local v0, "isAutoRebuildDone":Z
    if-eqz v1, :cond_0

    .line 353
    const-string v2, "isAutoRebuildDone"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 354
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "getIsRebuildDoneStatus status = "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    :cond_0
    return v0
.end method

.method public static getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 460
    const/4 v4, 0x0

    .line 461
    .local v4, "rangeQuery":Lorg/apache/lucene/search/Query;
    new-instance v5, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v5}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 462
    .local v5, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/configuration/StorageController;->getMountedVolumeList()Ljava/util/List;

    move-result-object v3

    .line 463
    .local v3, "mountedStorage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 464
    .local v6, "storageid":I
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 465
    .local v0, "combineQuery":Lorg/apache/lucene/search/BooleanQuery;
    const-string v7, "storage_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v7, v8, v9, v12, v12}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 467
    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4, v7}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 468
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/dcm/framework/configuration/StorageController;->getFatVolumeId(I)I

    move-result v1

    .line 469
    .local v1, "fatVolumeId":I
    const-string v7, "fatvolume_id"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v7, v8, v9, v12, v12}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v4

    .line 471
    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4, v7}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 472
    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v5, v0, v7}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 473
    sget-object v7, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v8, "Storageid: "

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " and sdcardid:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    const/4 v10, 0x2

    const-string v11, " for query"

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 475
    .end local v0    # "combineQuery":Lorg/apache/lucene/search/BooleanQuery;
    .end local v1    # "fatVolumeId":I
    .end local v6    # "storageid":I
    :cond_0
    return-object v5
.end method

.method public static declared-synchronized isCalendarProcessingFinished(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 400
    const-class v5, Lcom/samsung/dcm/framework/utils/DCMUtilities;

    monitor-enter v5

    const/4 v1, 0x1

    .line 401
    .local v1, "result":Z
    :try_start_0
    const-string v4, "Context cannot be null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    const-string v4, "Pref"

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 403
    .local v0, "pref":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_1

    .line 404
    sget-object v4, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyCalendarProcessing:Ljava/lang/String;

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 405
    sget-object v6, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v7, "isCalendarProcessingFinished value"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    if-nez v1, :cond_0

    move v4, v2

    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    :goto_1
    if-nez v1, :cond_2

    :goto_2
    monitor-exit v5

    return v2

    :cond_0
    move v4, v3

    .line 405
    goto :goto_0

    .line 407
    :cond_1
    :try_start_1
    sget-object v4, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v6, "isIntentProcessingFinished: pref is null"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v6, v7}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 400
    .end local v0    # "pref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v5

    throw v2

    .restart local v0    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    move v2, v3

    .line 409
    goto :goto_2
.end method

.method public static isCommitOk(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 264
    const/4 v0, 0x1

    .line 265
    .local v0, "defaultPrefValue":Z
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    const-string v2, "Pref"

    invoke-virtual {p0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 267
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 268
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyOnCommit:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 269
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "isCommitOk; kKeyOnCommit ="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    :cond_0
    return v0
.end method

.method public static isIntentProcessingFinished(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 304
    const/4 v1, 0x1

    .line 305
    .local v1, "result":Z
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    const-string v2, "Pref"

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 307
    .local v0, "pref":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 308
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyIntentProcessingFinished:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 312
    :goto_0
    return v1

    .line 310
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "isIntentProcessingFinished: pref is null"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static mergeBundles(Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 13
    .param p0, "oldBundle"    # Landroid/os/Bundle;
    .param p1, "updateBundle"    # Landroid/os/Bundle;

    .prologue
    .line 420
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 421
    .local v8, "result":Landroid/os/Bundle;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 423
    .local v9, "tmp":Landroid/os/Bundle;
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 424
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 425
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 426
    .local v7, "obj":Ljava/lang/Object;
    instance-of v12, v7, Ljava/util/ArrayList;

    if-eqz v12, :cond_3

    .line 427
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .local v4, "listA":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v5, v7

    .line 428
    check-cast v5, Ljava/util/ArrayList;

    .line 429
    .local v5, "listB":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    .line 430
    invoke-static {v4}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v11

    .line 431
    .local v11, "valuesA":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 432
    .local v10, "valueB":Ljava/lang/String;
    invoke-interface {v11, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 433
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 436
    .end local v10    # "valueB":Ljava/lang/String;
    :cond_2
    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 438
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "listA":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "listB":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11    # "valuesA":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    instance-of v12, v7, Landroid/os/Bundle;

    if-eqz v12, :cond_0

    .line 443
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 444
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_4

    .line 445
    check-cast v7, Landroid/os/Bundle;

    .end local v7    # "obj":Ljava/lang/Object;
    invoke-static {v0, v7}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->mergeBundles(Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v6

    .line 451
    .local v6, "newBundle":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {v9, v3, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .end local v6    # "newBundle":Landroid/os/Bundle;
    .restart local v7    # "obj":Ljava/lang/Object;
    :cond_4
    move-object v6, v7

    .line 448
    check-cast v6, Landroid/os/Bundle;

    .restart local v6    # "newBundle":Landroid/os/Bundle;
    goto :goto_2

    .line 455
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "key":Ljava/lang/String;
    .end local v6    # "newBundle":Landroid/os/Bundle;
    .end local v7    # "obj":Ljava/lang/Object;
    :cond_5
    invoke-virtual {v8, v9}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 456
    return-object v8
.end method

.method public static declared-synchronized setCalendarProcessingStarted(Landroid/content/Context;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 383
    const-class v3, Lcom/samsung/dcm/framework/utils/DCMUtilities;

    monitor-enter v3

    :try_start_0
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    const-string v2, "Pref"

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 385
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 386
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 387
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 388
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyCalendarProcessing:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 389
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 390
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v4, "setCalendarProcessingStarted; setting value"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    monitor-exit v3

    return-void

    .line 392
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :try_start_1
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v4, "setCalendarProcessingStarted: editor is null"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 395
    .restart local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_1
    :try_start_2
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v4, "setCalendarProcessingStarted: pref is null"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static setIntentProcessingFinished(Landroid/content/Context;Z)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    const/4 v6, 0x0

    .line 281
    const-string v2, "Context cannot be null"

    invoke-static {p0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const-string v2, "Pref"

    invoke-virtual {p0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 283
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 284
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 285
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 286
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->kKeyIntentProcessingFinished:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 287
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 288
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "setIntentProcessingFinished; setting value"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 290
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "setIntentProcessingFinished: editor is null"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 293
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-object v2, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v3, "setIntentProcessingFinished: pref is null"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setIsRebuildDoneStatus(Landroid/content/Context;Z)Z
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "status"    # Z

    .prologue
    const/4 v11, 0x0

    .line 323
    const/4 v4, 0x0

    .line 325
    .local v4, "ret":Z
    :try_start_0
    const-string v6, "Pref"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 326
    .local v3, "pref":Landroid/content/SharedPreferences;
    if-eqz v3, :cond_0

    .line 327
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 328
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v1, :cond_0

    .line 329
    const-string v6, "isAutoRebuildDone"

    invoke-interface {v1, v6, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 330
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 331
    sget-object v6, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v7, "setAutoRebuildStatus:status set to "

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 332
    const/4 v4, 0x1

    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    move v5, v4

    .line 343
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    .end local v4    # "ret":Z
    .local v5, "ret":I
    :goto_0
    return v5

    .line 335
    .end local v5    # "ret":I
    .restart local v4    # "ret":Z
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v7, "Exception"

    invoke-static {v6, v7, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v5, v4

    .line 337
    .restart local v5    # "ret":I
    goto :goto_0

    .line 338
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v5    # "ret":I
    :catch_1
    move-exception v2

    .line 339
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    sget-object v6, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v7, "OOM"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v4

    .line 340
    .restart local v5    # "ret":I
    goto :goto_0
.end method

.method public static startOSUpgradeRebuild(Landroid/content/Context;IZ)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storageId"    # I
    .param p2, "isExternalSDCard"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 478
    sget-object v6, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v7, "Scan Completed:Check if lucene upgrade is required for OS upgrade"

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 479
    const/4 v2, 0x0

    .line 480
    .local v2, "isOSUpgradeDone":Z
    if-eqz p2, :cond_0

    .line 481
    invoke-static {p0}, Lcom/samsung/dcm/framework/utils/SystemUtils;->getOSUpgradeSharedPrefForExtSd(Landroid/content/Context;)Z

    move-result v2

    .line 482
    invoke-static {p0, v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->setOSUpgradeSharedPrefForExtSd(Landroid/content/Context;Z)V

    .line 487
    :goto_0
    if-nez v2, :cond_1

    .line 488
    sget-object v6, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v7, "postOSUpgradeRebuildIntent"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 490
    .local v3, "upgradeRebuild":Landroid/content/Intent;
    const-string v5, "com.samsung.dcm"

    const-string v6, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 491
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 492
    .local v0, "actionData":Landroid/os/Bundle;
    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->OSUpgradeRebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const-string v5, "storage_id"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 495
    const-string v5, "isExternalSDCard"

    invoke-virtual {v0, v5, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 496
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 497
    .local v1, "extrasBundle":Landroid/os/Bundle;
    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {v6}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    sget-object v5, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 502
    invoke-virtual {v3, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 503
    invoke-virtual {p0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 507
    .end local v0    # "actionData":Landroid/os/Bundle;
    .end local v1    # "extrasBundle":Landroid/os/Bundle;
    .end local v3    # "upgradeRebuild":Landroid/content/Intent;
    :goto_1
    return v4

    .line 484
    :cond_0
    invoke-static {p0}, Lcom/samsung/dcm/framework/utils/SystemUtils;->getOSUpgradeSharedPrefForMedia(Landroid/content/Context;)Z

    move-result v2

    .line 485
    invoke-static {p0, v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->setOSUpgradeSharedPrefForMedia(Landroid/content/Context;Z)V

    goto :goto_0

    .line 506
    :cond_1
    sget-object v4, Lcom/samsung/dcm/framework/utils/DCMUtilities;->TAG:Ljava/lang/String;

    const-string v6, "OSUpgrade not required"

    new-array v7, v5, [Ljava/lang/Object;

    invoke-static {v4, v6, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v5

    .line 507
    goto :goto_1
.end method
