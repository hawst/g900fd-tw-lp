.class public final Lcom/samsung/dcm/framework/configuration/DCMIntitialize;
.super Ljava/lang/Object;
.source "DCMIntitialize.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static mSelf:Lcom/samsung/dcm/framework/configuration/DCMIntitialize;


# instance fields
.field mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

.field mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mSelf:Lcom/samsung/dcm/framework/configuration/DCMIntitialize;

    return-void
.end method

.method private constructor <init>(Lcom/samsung/dcm/framework/configuration/DcmTransport;Lcom/samsung/dcm/framework/configuration/DCMConfig;)V
    .locals 1
    .param p1, "mTransport"    # Lcom/samsung/dcm/framework/configuration/DcmTransport;
    .param p2, "mConfig"    # Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 51
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 52
    iput-object p2, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 53
    return-void
.end method

.method public static declared-synchronized createAndStart(Lcom/samsung/dcm/framework/configuration/DcmTransport;Lcom/samsung/dcm/framework/configuration/DCMConfig;)V
    .locals 4
    .param p0, "mTransport"    # Lcom/samsung/dcm/framework/configuration/DcmTransport;
    .param p1, "mConfig"    # Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .prologue
    .line 40
    const-class v2, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mSelf:Lcom/samsung/dcm/framework/configuration/DCMIntitialize;

    if-nez v1, :cond_0

    .line 41
    new-instance v1, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;

    invoke-direct {v1, p0, p1}, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;-><init>(Lcom/samsung/dcm/framework/configuration/DcmTransport;Lcom/samsung/dcm/framework/configuration/DCMConfig;)V

    sput-object v1, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mSelf:Lcom/samsung/dcm/framework/configuration/DCMIntitialize;

    .line 42
    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mSelf:Lcom/samsung/dcm/framework/configuration/DCMIntitialize;

    const-string v3, "DCM_Initialization"

    invoke-direct {v0, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 43
    .local v0, "dcminitThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    .end local v0    # "dcminitThread":Ljava/lang/Thread;
    :cond_0
    monitor-exit v2

    return-void

    .line 40
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 58
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isSystemShutdown()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    const-string v3, "DCMIntitialize"

    const-string v4, "System on shutdown"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setSystemOnShutdown(Z)V

    .line 61
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v3, v6}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->setSuccessState(Z)V

    .line 83
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v2

    .line 67
    .local v2, "nrtmanager":Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
    :try_start_0
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->intialize()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/OpeningIndexException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException; {:try_start_0 .. :try_end_0} :catch_2

    .line 79
    :goto_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMController()Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-result-object v0

    .line 80
    .local v0, "dcmController":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->intialize()V

    .line 82
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMIntitialize;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v3, v7}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->setSuccessState(Z)V

    goto :goto_0

    .line 68
    .end local v0    # "dcmController":Lcom/samsung/dcm/framework/extractormanager/DCMController;
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 70
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    goto :goto_1

    .line 71
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 72
    .local v1, "e":Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;->printStackTrace()V

    .line 73
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    goto :goto_1

    .line 74
    .end local v1    # "e":Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;
    :catch_2
    move-exception v1

    .line 75
    .local v1, "e":Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;->printStackTrace()V

    .line 76
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    goto :goto_1
.end method
