.class public Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
.super Ljava/lang/Object;
.source "DCMPolicyManager.java"

# interfaces
.implements Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;
.implements Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;,
        Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;
    }
.end annotation


# static fields
.field public static final CPU_CHECK_TIME:I = 0x5

.field private static final SIOP_DEFAULT:I = 0x0

.field private static final SIOP_THRESHOLD:I = 0x2

.field private static final mCPUMonitorThreadSleepInMs:J = 0x3e8L


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mBlockinVariable:Landroid/os/ConditionVariable;

.field private mCPUIdle:I

.field mCPUThreshHold:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field mDCMRuleBook:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;",
            ">;"
        }
    .end annotation
.end field

.field private final mIntentReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

.field private final mPolicyObserverList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

.field private mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

.field mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

.field private mSIOPLevel:I

.field private final mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field mTimeLeft:I

.field private mbInterrupted:Z

.field prev_idle:J

.field prev_total:J

.field prev_user:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .prologue
    const/4 v9, 0x5

    const-wide/16 v10, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-class v3, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    .line 66
    const/4 v3, 0x6

    new-array v3, v3, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v3, v6

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    sget-object v5, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    sget-object v5, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v5, v3, v4

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SIOP:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v3, v9

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    .line 74
    iput-object v8, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    .line 78
    iput-object v8, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    .line 79
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUThreshHold:Ljava/util/ArrayList;

    .line 81
    iput-wide v10, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_total:J

    .line 82
    iput-wide v10, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_idle:J

    .line 83
    iput-wide v10, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_user:J

    .line 85
    const/16 v3, 0x64

    iput v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUIdle:I

    .line 86
    const-string v3, "sys.siop.level"

    invoke-static {v3, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mSIOPLevel:I

    .line 92
    iput-boolean v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mbInterrupted:Z

    .line 96
    new-instance v3, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v3}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    .line 100
    iput v9, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I

    .line 102
    iput-object v8, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    .line 128
    const-string v3, "Context cannot be null"

    invoke-static {p1, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    .line 129
    const-string v3, "SystemBroadcastReceiver cannot be null"

    invoke-static {p2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mIntentReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 132
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->checkCPUUsage()[I

    move-result-object v2

    .line 133
    .local v2, "first":[I
    aget v3, v2, v7

    iput v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUIdle:I

    .line 134
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 135
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->updateSystemState()V

    .line 136
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    .line 137
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    .line 138
    new-instance v3, Landroid/os/ConditionVariable;

    invoke-direct {v3, v7}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    .line 140
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->createRules()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mIntentReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mRegisteredEvents:[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {v3, p0, v4}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V

    .line 151
    new-instance v3, Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;-><init>(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    .line 153
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v4, "Exception in reading DCMRules.txt from assets"

    invoke-static {v3, v4, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 144
    new-instance v1, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    invoke-direct {v1, v6, v6, v7}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;-><init>(IIZ)V

    .line 145
    .local v1, "exitrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private checkCPUUsage()[I
    .locals 28

    .prologue
    .line 412
    const/4 v15, 0x2

    new-array v13, v15, [I

    fill-array-data v13, :array_0

    .line 415
    .local v13, "retVal":[I
    const/4 v11, 0x0

    .line 417
    .local v11, "reader":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v12, Ljava/io/RandomAccessFile;

    const-string v15, "/proc/stat"

    const-string v24, "r"

    move-object/from16 v0, v24

    invoke-direct {v12, v15, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    .end local v11    # "reader":Ljava/io/RandomAccessFile;
    .local v12, "reader":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v10

    .line 419
    .local v10, "load":Ljava/lang/String;
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 421
    .local v14, "toks":[Ljava/lang/String;
    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 422
    const/4 v11, 0x0

    .line 424
    .end local v12    # "reader":Ljava/io/RandomAccessFile;
    .restart local v11    # "reader":Ljava/io/RandomAccessFile;
    const/4 v15, 0x2

    :try_start_2
    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v22

    .line 425
    .local v22, "user_val":J
    const/4 v15, 0x5

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 426
    .local v8, "idle_val":J
    const/4 v15, 0x2

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    const/4 v15, 0x3

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    add-long v24, v24, v26

    const/4 v15, 0x4

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    add-long v24, v24, v26

    const/4 v15, 0x5

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    add-long v24, v24, v26

    const/4 v15, 0x6

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    add-long v24, v24, v26

    const/4 v15, 0x7

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    add-long v24, v24, v26

    const/16 v15, 0x8

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    add-long v18, v24, v26

    .line 434
    .local v18, "total_val":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_total:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    cmp-long v15, v24, v26

    if-ltz v15, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_idle:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    cmp-long v15, v24, v26

    if-ltz v15, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_user:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    cmp-long v15, v24, v26

    if-gez v15, :cond_2

    .line 435
    :cond_0
    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_total:J

    .line 436
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_idle:J

    .line 437
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_user:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 461
    :goto_0
    if-eqz v11, :cond_1

    .line 463
    :try_start_3
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 470
    .end local v8    # "idle_val":J
    .end local v10    # "load":Ljava/lang/String;
    .end local v14    # "toks":[Ljava/lang/String;
    .end local v18    # "total_val":J
    .end local v22    # "user_val":J
    :cond_1
    :goto_1
    return-object v13

    .line 440
    .restart local v8    # "idle_val":J
    .restart local v10    # "load":Ljava/lang/String;
    .restart local v14    # "toks":[Ljava/lang/String;
    .restart local v18    # "total_val":J
    .restart local v22    # "user_val":J
    :cond_2
    const-wide/16 v16, 0x0

    .line 441
    .local v16, "totalTime":J
    const-wide/16 v6, 0x0

    .line 442
    .local v6, "idleTime":J
    const-wide/16 v20, 0x0

    .line 444
    .local v20, "userTime":J
    :try_start_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_total:J

    move-wide/from16 v24, v0

    sub-long v16, v18, v24

    .line 445
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_idle:J

    move-wide/from16 v24, v0

    sub-long v6, v8, v24

    .line 446
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_user:J

    move-wide/from16 v24, v0

    sub-long v20, v22, v24

    .line 448
    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_total:J

    .line 449
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_idle:J

    .line 450
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->prev_user:J

    .line 452
    const/4 v15, 0x0

    const-wide/16 v24, 0x64

    mul-long v24, v24, v20

    div-long v24, v24, v16

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v24, v0

    aput v24, v13, v15

    .line 453
    const/4 v15, 0x1

    const-wide/16 v24, 0x64

    mul-long v24, v24, v6

    div-long v24, v24, v16

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v24, v0

    aput v24, v13, v15
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 458
    .end local v6    # "idleTime":J
    .end local v8    # "idle_val":J
    .end local v10    # "load":Ljava/lang/String;
    .end local v14    # "toks":[Ljava/lang/String;
    .end local v16    # "totalTime":J
    .end local v18    # "total_val":J
    .end local v20    # "userTime":J
    .end local v22    # "user_val":J
    :catch_0
    move-exception v4

    .line 459
    .local v4, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v24, "CPU Proc Info Read Failed"

    move-object/from16 v0, v24

    invoke-static {v15, v0, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 461
    if-eqz v11, :cond_1

    .line 463
    :try_start_6
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 464
    :catch_1
    move-exception v5

    .line 465
    .local v5, "e1":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v24, "reader close"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v15, v0, v1}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 464
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "e1":Ljava/io/IOException;
    .restart local v8    # "idle_val":J
    .restart local v10    # "load":Ljava/lang/String;
    .restart local v14    # "toks":[Ljava/lang/String;
    .restart local v18    # "total_val":J
    .restart local v22    # "user_val":J
    :catch_2
    move-exception v5

    .line 465
    .restart local v5    # "e1":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v24, "reader close"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v15, v0, v1}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 461
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v8    # "idle_val":J
    .end local v10    # "load":Ljava/lang/String;
    .end local v14    # "toks":[Ljava/lang/String;
    .end local v18    # "total_val":J
    .end local v22    # "user_val":J
    :catchall_0
    move-exception v15

    :goto_3
    if-eqz v11, :cond_3

    .line 463
    :try_start_7
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 466
    :cond_3
    :goto_4
    throw v15

    .line 464
    :catch_3
    move-exception v5

    .line 465
    .restart local v5    # "e1":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, "reader close"

    const/16 v26, 0x0

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 461
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v11    # "reader":Ljava/io/RandomAccessFile;
    .restart local v12    # "reader":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v15

    move-object v11, v12

    .end local v12    # "reader":Ljava/io/RandomAccessFile;
    .restart local v11    # "reader":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 458
    .end local v11    # "reader":Ljava/io/RandomAccessFile;
    .restart local v12    # "reader":Ljava/io/RandomAccessFile;
    :catch_4
    move-exception v4

    move-object v11, v12

    .end local v12    # "reader":Ljava/io/RandomAccessFile;
    .restart local v11    # "reader":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 412
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private createRules()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    const/4 v2, 0x0

    .line 214
    .local v2, "br":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 217
    .local v1, "am":Landroid/content/res/AssetManager;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    .end local v2    # "br":Ljava/io/BufferedReader;
    new-instance v12, Ljava/io/FileReader;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/DCMRules.txt"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :goto_0
    const/4 v4, 0x0

    .line 230
    .local v4, "cpuIdleMin":I
    const/4 v3, 0x0

    .line 232
    .local v3, "cpuIdleMax":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    .line 234
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .line 235
    .local v9, "readcurrentline":Ljava/lang/String;
    :goto_1
    if-eqz v9, :cond_1

    .line 236
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v13, " readcurrentline :"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v9, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    new-instance v8, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v8, v12, v13, v14}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;-><init>(IIZ)V

    .line 239
    .local v8, "newrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    const/4 v12, 0x4

    new-array v11, v12, [I

    .line 240
    .local v11, "val":[I
    const-string v12, " "

    invoke-virtual {v9, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 242
    .local v10, "tokens":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    const/4 v12, 0x4

    if-ge v7, v12, :cond_0

    .line 243
    aget-object v12, v10, v7

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    aput v12, v11, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 219
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "cpuIdleMax":I
    .end local v4    # "cpuIdleMin":I
    .end local v7    # "i":I
    .end local v8    # "newrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    .end local v9    # "readcurrentline":Ljava/lang/String;
    .end local v10    # "tokens":[Ljava/lang/String;
    .end local v11    # "val":[I
    :catch_0
    move-exception v5

    .line 221
    .local v5, "e1":Ljava/io/FileNotFoundException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 222
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v13, "opening file DCMRules.txt"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v12, Ljava/io/InputStreamReader;

    const-string v13, "DCMRules.txt"

    invoke-virtual {v1, v13}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v13

    const-string v14, "UTF-8"

    invoke-direct {v12, v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 245
    .end local v5    # "e1":Ljava/io/FileNotFoundException;
    .restart local v3    # "cpuIdleMax":I
    .restart local v4    # "cpuIdleMin":I
    .restart local v7    # "i":I
    .restart local v8    # "newrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    .restart local v9    # "readcurrentline":Ljava/lang/String;
    .restart local v10    # "tokens":[Ljava/lang/String;
    .restart local v11    # "val":[I
    :cond_0
    const/4 v12, 0x0

    :try_start_2
    aget v3, v11, v12

    .line 246
    const/4 v12, 0x1

    aget v4, v11, v12

    .line 247
    const/4 v12, 0x2

    aget v12, v11, v12

    iput v12, v8, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    .line 248
    const/4 v12, 0x3

    aget v12, v11, v12

    iput v12, v8, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    .line 250
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v13, "cpuIdleMax :"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v13, "cpuIdleMin :"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v13, "images to process :"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget v0, v8, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v13, "timeout :"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget v0, v8, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUThreshHold:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .line 257
    goto/16 :goto_1

    .line 258
    .end local v7    # "i":I
    .end local v8    # "newrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    .end local v10    # "tokens":[Ljava/lang/String;
    .end local v11    # "val":[I
    :cond_1
    new-instance v6, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-direct {v6, v12, v13, v14}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;-><init>(IIZ)V

    .line 259
    .local v6, "exitrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 262
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 264
    return-void

    .line 262
    .end local v6    # "exitrule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    .end local v9    # "readcurrentline":Ljava/lang/String;
    :catchall_0
    move-exception v12

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    throw v12
.end method

.method private getProcessState()Z
    .locals 5

    .prologue
    .line 400
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 401
    .local v2, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x1

    .line 402
    .local v3, "state":Z
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 403
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 404
    const/4 v3, 0x0

    .line 408
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return v3
.end method

.method private internalrun()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 499
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Monitor thread running"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 501
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->isFinishedNow()Z

    move-result v2

    if-nez v2, :cond_2

    .line 503
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->checkCPUUsage()[I

    move-result-object v0

    .line 504
    .local v0, "CPUUsages":[I
    const/4 v2, 0x1

    aget v2, v0, v2

    iput v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUIdle:I

    .line 505
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 507
    iget v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUIdle:I

    const/16 v3, 0xa

    if-ge v2, v3, :cond_1

    .line 508
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v3, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CPU:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    :goto_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->block()V

    .line 514
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 515
    iget v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I

    .line 516
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Nothing is registered exiting left count ="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 517
    iget v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I

    if-nez v2, :cond_0

    .line 518
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Exiting"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 530
    .end local v0    # "CPUUsages":[I
    :goto_2
    return-void

    .line 511
    .restart local v0    # "CPUUsages":[I
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v3, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CPU:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 526
    .end local v0    # "CPUUsages":[I
    :catch_0
    move-exception v1

    .line 527
    .local v1, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Thread stopped normally "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 529
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Monitor thread done"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 523
    .restart local v0    # "CPUUsages":[I
    :cond_3
    const/4 v2, 0x5

    :try_start_1
    iput v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private declared-synchronized notifyListeners(Z)V
    .locals 5
    .param p1, "bShutdown"    # Z

    .prologue
    .line 382
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 384
    if-eqz p1, :cond_0

    .line 385
    :try_start_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;

    .line 386
    .local v1, "observer":Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;
    invoke-interface {v1}, Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;->onShutDown()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "observer":Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;
    :catchall_0
    move-exception v3

    :try_start_2
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382
    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3

    .line 389
    :cond_0
    :try_start_3
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->getProcessState()Z

    move-result v2

    .line 390
    .local v2, "state":Z
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;

    .line 391
    .restart local v1    # "observer":Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;
    invoke-interface {v1, v2}, Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;->onCanProcess(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 395
    .end local v1    # "observer":Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;
    .end local v2    # "state":Z
    :cond_1
    :try_start_4
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 397
    monitor-exit p0

    return-void
.end method

.method private updateSystemState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->setCriticalStateFromSystem()V

    .line 180
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CPU:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->APP:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_QUERY:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CP_UPDATE:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v1, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->LUCENE_COMMIT:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    return-void
.end method


# virtual methods
.method public finishNow()V
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mbInterrupted:Z

    .line 535
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 536
    return-void
.end method

.method public getCurrentRule()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 267
    const/4 v2, 0x0

    .line 268
    .local v2, "rule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatterOK(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isMemoryOK(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 270
    :cond_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v5, " OSC will be stopped ProcessingRule 4"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "rule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    check-cast v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    .line 272
    .restart local v2    # "rule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->clone()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    move-result-object v4

    .line 296
    :goto_0
    return-object v4

    .line 283
    :cond_1
    const/4 v1, -0x1

    .line 284
    .local v1, "index":I
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUThreshHold:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 285
    .local v3, "threshhold":I
    add-int/lit8 v1, v1, 0x1

    .line 286
    iget v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUIdle:I

    if-gt v3, v4, :cond_2

    .line 291
    .end local v3    # "threshhold":I
    :cond_3
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_4

    move v4, v5

    :goto_1
    const-string v7, "DCM RUle Invalid index"

    invoke-static {v4, v7}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 294
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mDCMRuleBook:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "rule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    check-cast v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    .line 295
    .restart local v2    # "rule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v7, "mCPUIdle ="

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mCPUIdle:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    const-string v6, " index "

    aput-object v6, v8, v5

    const/4 v5, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v8, v5

    const/4 v5, 0x3

    const-string v6, " "

    aput-object v6, v8, v5

    const/4 v5, 0x4

    aput-object v2, v8, v5

    invoke-static {v4, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->clone()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    move-result-object v4

    goto :goto_0

    :cond_4
    move v4, v6

    .line 291
    goto :goto_1
.end method

.method public isFinishedNow()Z
    .locals 1

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mbInterrupted:Z

    return v0
.end method

.method public onIntentReceived(Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "action"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 340
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v2, "onReceive action ="

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342
    const/4 v0, 0x0

    .line 343
    .local v0, "bShutdown":Z
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 344
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->SCREEN:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 373
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 375
    return-void

    .line 347
    :cond_1
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SCREEN_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 348
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->SCREEN:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    goto :goto_0

    .line 351
    :cond_2
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SHUTDOWN:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 352
    const/4 v0, 0x1

    goto :goto_0

    .line 354
    :cond_3
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_ON:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 355
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CAMERA:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    goto :goto_0

    .line 358
    :cond_4
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_CAMERA_OFF:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 359
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CAMERA:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mBlockinVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    goto :goto_0

    .line 362
    :cond_5
    sget-object v1, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_SIOP:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    invoke-virtual {p1, v1}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    const-string v1, "siop_level_broadcast"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mSIOPLevel:I

    .line 364
    iget v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mSIOPLevel:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_6

    .line 365
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v2, "SIOP level is high ="

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mSIOPLevel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->SIOP:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 369
    :cond_6
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->SIOP:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 552
    return-void
.end method

.method public pauseHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V
    .locals 6
    .param p1, "policy"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 560
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v1, "pause Policy name = "

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 561
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    invoke-direct {p0, v4}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 563
    return-void
.end method

.method public registerDCMPolicyObserver(Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;)Z
    .locals 2
    .param p1, "observer"    # Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;

    .prologue
    .line 156
    const-string v0, "Observer cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->setCriticalStateFromSystem()V

    .line 158
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->getProcessState()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;->onCanProcess(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 165
    const/4 v0, 0x1

    return v0

    .line 163
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mTimeLeft:I

    .line 546
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mbInterrupted:Z

    .line 547
    return-void
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 557
    return-void
.end method

.method public resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V
    .locals 5
    .param p1, "policy"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    .prologue
    const/4 v4, 0x0

    .line 566
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v1, "resume Policy name = "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 567
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    invoke-direct {p0, v4}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 569
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 476
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    const/16 v3, 0xf

    invoke-static {v2, v3}, Landroid/os/Process;->setThreadPriority(II)V

    .line 479
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->registerProcessObserver()V

    .line 482
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-static {v2, v3}, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->setAffinityForThread(I[I)V

    .line 483
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->internalrun()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Policy manager has"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, " listeners "

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 493
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 494
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->unregisterProcessObserver()V

    .line 496
    :goto_0
    return-void

    .line 484
    :catch_0
    move-exception v1

    .line 485
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "OOM"

    invoke-static {v2, v3, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 486
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Policy manager has"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, " listeners "

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 493
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 494
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->unregisterProcessObserver()V

    goto :goto_0

    .line 487
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v0

    .line 488
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Uncatched"

    invoke-static {v2, v3, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 490
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v3, "Policy manager has"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, " listeners "

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 493
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 494
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->unregisterProcessObserver()V

    goto :goto_0

    .line 490
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v4, "Policy manager has"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const-string v6, " listeners "

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->notifyListeners(Z)V

    .line 493
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 494
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mProcessObserver:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->unregisterProcessObserver()V

    throw v2

    .line 482
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data
.end method

.method setCriticalStateFromSystem()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 189
    const-string v6, "sys.siop.level"

    invoke-static {v6, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mSIOPLevel:I

    .line 191
    iget-object v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mContext:Landroid/content/Context;

    const-string v7, "power"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 195
    .local v1, "pm":Landroid/os/PowerManager;
    const-string v6, "service.camera.running"

    invoke-static {v6, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-lez v6, :cond_0

    move v0, v5

    .line 197
    .local v0, "camera":Z
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    .line 198
    .local v2, "screen":Z
    :goto_1
    iget v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mSIOPLevel:I

    if-lt v6, v10, :cond_2

    move v3, v5

    .line 200
    .local v3, "siop":Z
    :goto_2
    iget-object v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->TAG:Ljava/lang/String;

    const-string v7, "setCriticalStateFromSystem screenOn = "

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v4

    const-string v9, " high siop = "

    aput-object v9, v8, v5

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v10

    const/4 v9, 0x3

    const-string v10, " camera running = "

    aput-object v10, v8, v9

    const/4 v9, 0x4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    iget-object v6, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v7, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->SCREEN:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v7

    if-nez v2, :cond_3

    :goto_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v5, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->CAMERA:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mStateMapList:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v5, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->SIOP:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    return-void

    .end local v0    # "camera":Z
    .end local v2    # "screen":Z
    .end local v3    # "siop":Z
    :cond_0
    move v0, v4

    .line 195
    goto :goto_0

    .restart local v0    # "camera":Z
    :cond_1
    move v2, v4

    .line 197
    goto :goto_1

    .restart local v2    # "screen":Z
    :cond_2
    move v3, v4

    .line 198
    goto :goto_2

    .restart local v3    # "siop":Z
    :cond_3
    move v5, v4

    .line 202
    goto :goto_3
.end method

.method public unregisterDCMPolicyObserver(Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;)Z
    .locals 2
    .param p1, "observer"    # Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 171
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 175
    const/4 v0, 0x1

    return v0

    .line 173
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->mPolicyObserverLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
