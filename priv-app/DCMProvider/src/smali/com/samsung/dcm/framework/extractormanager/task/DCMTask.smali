.class public abstract Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.super Ljava/lang/Object;
.source "DCMTask.java"

# interfaces
.implements Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;


# static fields
.field public static final HEAVY_TASK_PRIORITY:I = 0xf

.field public static final SYNCHRO_TASK_PRIORITY:I = 0xc

.field public static final TASK_PRIORITY:I = 0xa


# instance fields
.field protected TAG:Ljava/lang/String;

.field private volatile mFinishNow:Z

.field private volatile mIsDone:Z

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPauseVariable:Landroid/os/ConditionVariable;

.field private mStartId:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mStartId:I

    .line 34
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mPauseVariable:Landroid/os/ConditionVariable;

    .line 35
    iput-boolean v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mFinishNow:Z

    .line 39
    iput-boolean v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mIsDone:Z

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v1, "Spawning a TASK"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method private fireCallbacks(Ljava/lang/Throwable;)V
    .locals 7
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 133
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v3, "Firing callbacks with startId="

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mStartId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, " and exception="

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 136
    if-nez p1, :cond_1

    .line 137
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 138
    .local v1, "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    if-eqz v1, :cond_0

    .line 139
    iget v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mStartId:I

    invoke-interface {v1, v2}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    goto :goto_0

    .line 142
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 143
    .restart local v1    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    if-eqz v1, :cond_2

    .line 144
    iget v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mStartId:I

    invoke-interface {v1, v2, p1}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto :goto_1

    .line 148
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    :cond_3
    return-void
.end method


# virtual methods
.method public final addListener(Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method protected afterInternalRun()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method protected beforeInternalRun()V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 51
    return-void
.end method

.method public clearListeners()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 101
    :cond_0
    return-void
.end method

.method public finishNow()V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v1, "Request to finishNow"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mFinishNow:Z

    .line 129
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 130
    return-void
.end method

.method protected abstract internalRun()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mIsDone:Z

    return v0
.end method

.method public final isFinishedNow()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mFinishNow:Z

    return v0
.end method

.method protected maybePause()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 111
    return-void
.end method

.method public final pause()V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v1, "task paused"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 117
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mFinishNow:Z

    .line 161
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 162
    return-void
.end method

.method public final resume()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v1, "task resumed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 123
    return-void
.end method

.method public final run()V
    .locals 13

    .prologue
    const/4 v10, 0x2

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 61
    .local v4, "start":J
    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v7, "run starting "

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->beforeInternalRun()V

    .line 64
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->internalRun()V

    .line 65
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->afterInternalRun()V

    .line 66
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->fireCallbacks(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    iput-boolean v11, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mIsDone:Z

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v4

    .line 79
    .local v2, "end":J
    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v7, "[DCM_Performance] ran for : ["

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v12

    const-string v9, "]ms"

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    :goto_0
    return-void

    .line 67
    .end local v2    # "end":J
    :catch_0
    move-exception v1

    .line 68
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v7, "OOM"

    invoke-static {v6, v7, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    invoke-direct {p0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->fireCallbacks(Ljava/lang/Throwable;)V

    .line 70
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    iput-boolean v11, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mIsDone:Z

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v4

    .line 79
    .restart local v2    # "end":J
    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v7, "[DCM_Performance] ran for : ["

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v12

    const-string v9, "]ms"

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    .end local v2    # "end":J
    :catch_1
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v7, "Unhandled exception"

    invoke-static {v6, v7, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    invoke-direct {p0, v0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->fireCallbacks(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    iput-boolean v11, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mIsDone:Z

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v4

    .line 79
    .restart local v2    # "end":J
    iget-object v6, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v7, "[DCM_Performance] ran for : ["

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v12

    const-string v9, "]ms"

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "end":J
    :catchall_0
    move-exception v6

    iput-boolean v11, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mIsDone:Z

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v2, v8, v4

    .line 79
    .restart local v2    # "end":J
    iget-object v7, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->TAG:Ljava/lang/String;

    const-string v8, "[DCM_Performance] ran for : ["

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v12

    const-string v10, "]ms"

    aput-object v10, v9, v11

    invoke-static {v7, v8, v9}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    throw v6
.end method

.method public final setStartId(I)V
    .locals 0
    .param p1, "startId"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->mStartId:I

    .line 85
    return-void
.end method
