.class public Lcom/samsung/dcm/framework/lucene/DCMNRTManager;
.super Ljava/lang/Object;
.source "DCMNRTManager.java"

# interfaces
.implements Lorg/apache/lucene/search/ReferenceManager$RefreshListener;


# static fields
.field private static final MAX_LUCENE_TIMEOUT:J = 0xaL

.field private static final TAG:Ljava/lang/String;

.field public static final kRefreshAndCommitDelayms:J = 0x7d0L

.field private static mDBExceptionTime:J

.field private static mDBInitTime:J

.field private static mbNRTClosed:Z

.field private static mbNRTReady:Z


# instance fields
.field private mArrayRefreshMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCommandCount:I

.field protected mContext:Landroid/content/Context;

.field private mDCMIndexSearcherList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;",
            ">;"
        }
    .end annotation
.end field

.field private mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/lucene/DCMCacheManager",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

.field private final mNRTLock:Ljava/util/concurrent/locks/Lock;

.field private mNRTManager:Lorg/apache/lucene/search/NRTManager;

.field private mRunnableTask:Ljava/lang/Runnable;

.field private mScheduledFuture:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/dcm/framework/lucene/DCMCacheManager",
            "<",
            "Lorg/apache/lucene/search/IndexSearcher;",
            ">;"
        }
    .end annotation
.end field

.field private mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 87
    const-class v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    .line 118
    sput-boolean v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    .line 119
    sput-boolean v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTClosed:Z

    .line 120
    sput-wide v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDBExceptionTime:J

    .line 121
    sput-wide v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDBInitTime:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "indexdir"    # Ljava/io/File;
    .param p3, "taxodir"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mContext:Landroid/content/Context;

    .line 93
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    .line 94
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    .line 96
    new-instance v1, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-direct {v1}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    .line 97
    new-instance v1, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-direct {v1}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    .line 100
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    .line 104
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    .line 108
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mCommandCount:I

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDCMIndexSearcherList:Ljava/util/ArrayList;

    .line 114
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 116
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mRunnableTask:Ljava/lang/Runnable;

    .line 117
    iput-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 135
    const-string v1, "Context must not be null"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mContext:Landroid/content/Context;

    .line 136
    new-instance v1, Lcom/samsung/dcm/framework/lucene/WriterFactory;

    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/dcm/framework/lucene/WriterFactory;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    .line 137
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v1, p2}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->setIndexDir(Ljava/io/File;)V

    .line 138
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v1, p3}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->setTaxoDir(Ljava/io/File;)V

    .line 139
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    const-string v2, "DCMCodec"

    invoke-static {v2}, Lorg/apache/lucene/codecs/Codec;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/Codec;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->setCodec(Lorg/apache/lucene/codecs/Codec;)V

    .line 141
    invoke-static {}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getLuceneFactory()Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;

    move-result-object v0

    .line 143
    .local v0, "factory":Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    new-instance v1, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getMaxThreadCount()I

    move-result v2

    invoke-direct {v1, v2, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 149
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    .line 150
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 153
    new-instance v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager$1;-><init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mRunnableTask:Ljava/lang/Runnable;

    .line 174
    return-void
.end method

.method private commit()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/IndexCommitException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 396
    sget-boolean v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v1, :cond_0

    .line 397
    sget-object v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "commit"

    new-array v7, v10, [Ljava/lang/Object;

    const-string v8, "Already closed ?"

    aput-object v8, v7, v9

    invoke-static {v1, v6, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 412
    :goto_0
    return-void

    .line 400
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 401
    .local v4, "start":J
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->beforeCommit(Landroid/content/Context;)V

    .line 404
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->afterCommit(Landroid/content/Context;)V

    .line 410
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v4

    .line 411
    .local v2, "end":J
    sget-object v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real commit : ["

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    const-string v8, "]ms"

    aput-object v8, v7, v10

    invoke-static {v1, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 405
    .end local v2    # "end":J
    :catch_0
    move-exception v0

    .line 406
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/samsung/dcm/framework/exceptions/IndexCommitException;

    const-string v6, "Error in committing index"

    invoke-direct {v1, v6, v0}, Lcom/samsung/dcm/framework/exceptions/IndexCommitException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private declared-synchronized doScheduleCommit(Z)V
    .locals 6
    .param p1, "reset"    # Z

    .prologue
    .line 374
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 375
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 376
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->isSearcherCurrent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    :goto_0
    monitor-exit p0

    return-void

    .line 380
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_1

    .line 381
    sget-object v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v1, "doScheduleCommit OK"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 382
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledTPExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mRunnableTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledFuture:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 385
    :cond_1
    :try_start_2
    sget-object v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v1, "doScheduleCommit time left ="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mScheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private intializeLuceneDatabase()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;,
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
        }
    .end annotation

    .prologue
    .line 754
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->open()V

    .line 755
    return-void
.end method

.method protected static isNRTReady()Z
    .locals 1

    .prologue
    .line 180
    sget-boolean v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    return v0
.end method

.method private isSearcherCurrent()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 236
    const/4 v1, 0x0

    .line 237
    .local v1, "latest":Z
    sget-boolean v3, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v3, :cond_0

    .line 238
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v4, "isSearcherCurrent"

    new-array v5, v2, [Ljava/lang/Object;

    const-string v6, "Not ready"

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    :goto_0
    return v2

    .line 243
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v2}, Lorg/apache/lucene/search/NRTManager;->isSearcherCurrent()Z

    move-result v1

    .line 244
    sget-object v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v3, "isSearcherCurrent ="

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v2, v1

    .line 248
    goto :goto_0

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v3, "isSearcherCurrent"

    invoke-static {v2, v3, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private resetLuceneDatabase()V
    .locals 1

    .prologue
    .line 740
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->intialize()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/OpeningIndexException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException; {:try_start_0 .. :try_end_0} :catch_2

    .line 751
    :goto_0
    return-void

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 743
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    goto :goto_0

    .line 744
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 745
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;->printStackTrace()V

    .line 746
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    goto :goto_0

    .line 747
    .end local v0    # "e":Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;
    :catch_2
    move-exception v0

    .line 748
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;->printStackTrace()V

    .line 749
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V

    goto :goto_0
.end method


# virtual methods
.method public addRefreshListener(Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;)V
    .locals 10
    .param p1, "listener"    # Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    .prologue
    .line 525
    sget-boolean v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v5, :cond_0

    .line 526
    invoke-interface {p1}, Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;->onCompleted()V

    .line 559
    :goto_0
    return-void

    .line 529
    :cond_0
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 531
    :try_start_0
    const-string v5, "DCMNRTRefreshListener cannot be null"

    invoke-static {p1, v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->isSearcherCurrent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 533
    invoke-interface {p1}, Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;->onCompleted()V

    .line 534
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "addRefreshListner completed"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    :goto_1
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 537
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v5}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getGeneration()J

    move-result-wide v2

    .line 538
    .local v2, "id":J
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "addRefreshListner pending id ="

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    .line 541
    .local v1, "previousCallBacks":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    if-nez v1, :cond_2

    .line 542
    new-instance v1, Ljava/util/HashSet;

    .end local v1    # "previousCallBacks":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 543
    .restart local v1    # "previousCallBacks":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    :cond_2
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    .line 546
    .local v4, "success":Z
    if-eqz v4, :cond_3

    .line 547
    iget v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mCommandCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mCommandCount:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 554
    .end local v1    # "previousCallBacks":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    .end local v2    # "id":J
    .end local v4    # "success":Z
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "Exception"

    invoke-static {v5, v6, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 557
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 550
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "previousCallBacks":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    .restart local v2    # "id":J
    .restart local v4    # "success":Z
    :cond_3
    :try_start_3
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "addRefreshListener "

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    const-string v9, " not added"

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 557
    .end local v1    # "previousCallBacks":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    .end local v2    # "id":J
    .end local v4    # "success":Z
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v5
.end method

.method public afterRefresh(Z)V
    .locals 22
    .param p1, "arg0"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 563
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 565
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v15}, Lorg/apache/lucene/search/NRTManager;->getCurrentSearchingGen()J

    move-result-wide v12

    .line 566
    .local v12, "nowid":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v15}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getGeneration()J

    move-result-wide v4

    .line 567
    .local v4, "genid":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    invoke-virtual {v15}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 568
    .local v14, "registeredId":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 569
    .local v10, "keyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 570
    .local v8, "key":J
    cmp-long v15, v8, v4

    if-gez v15, :cond_0

    .line 571
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    .line 573
    .local v2, "completedList":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    .line 575
    .local v11, "listener":Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;
    invoke-interface {v11}, Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;->onCompleted()V

    .line 576
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mCommandCount:I

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mCommandCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 609
    .end local v2    # "completedList":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;>;"
    .end local v4    # "genid":J
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "key":J
    .end local v10    # "keyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v11    # "listener":Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;
    .end local v12    # "nowid":J
    .end local v14    # "registeredId":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catch_0
    move-exception v3

    .line 610
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v15, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v18, "Exception"

    move-object/from16 v0, v18

    invoke-static {v15, v0, v3}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 615
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 580
    .restart local v4    # "genid":J
    .restart local v10    # "keyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v12    # "nowid":J
    .restart local v14    # "registeredId":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_1
    :try_start_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 581
    .restart local v8    # "key":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 612
    .end local v4    # "genid":J
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "key":J
    .end local v10    # "keyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v12    # "nowid":J
    .end local v14    # "registeredId":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v15

    .line 583
    .restart local v4    # "genid":J
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v10    # "keyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v12    # "nowid":J
    .restart local v14    # "registeredId":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_2
    :try_start_3
    sget-object v15, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v18, "afterRefresh Pending = "

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mArrayRefreshMap:Ljava/util/HashMap;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v15, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 586
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->verifyIS()V

    .line 593
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    const-wide/16 v18, 0x1

    sub-long v18, v4, v18

    move-wide/from16 v0, v18

    invoke-virtual {v15, v0, v1}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->getoldestid(J)J

    move-result-wide v16

    .line 595
    .local v16, "removedid":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->size()I

    move-result v15

    if-nez v15, :cond_3

    .line 597
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    const-wide/16 v18, 0x1

    sub-long v18, v4, v18

    move-wide/from16 v0, v18

    invoke-virtual {v15, v0, v1}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->clearCache(J)V

    .line 603
    :goto_3
    sget-object v15, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v18, " afterRefresh stats mSearcherCache ="

    const/16 v19, 0x9

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, " removedid = "

    aput-object v21, v19, v20

    const/16 v20, 0x2

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x3

    const-string v21, " nowid = "

    aput-object v21, v19, v20

    const/16 v20, 0x4

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x5

    const-string v21, " genid = "

    aput-object v21, v19, v20

    const/16 v20, 0x6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x7

    const-string v21, " mDocumentCache size = "

    aput-object v21, v19, v20

    const/16 v20, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v15, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 608
    sget-object v15, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v18, "afterRefresh "

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "Pending RefreshMap ="

    aput-object v21, v19, v20

    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mCommandCount:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v15, v0, v1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 612
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_1

    .line 600
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual/range {v15 .. v17}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->clearCache(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3
.end method

.method public beforeRefresh()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 623
    return-void
.end method

.method public close()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 318
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 319
    sget-boolean v3, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v3, :cond_0

    .line 345
    :goto_0
    return-void

    .line 322
    :cond_0
    sput-boolean v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    .line 323
    sput-boolean v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTClosed:Z

    .line 326
    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v3}, Lorg/apache/lucene/search/NRTManager;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    :goto_1
    const/4 v3, 0x0

    :try_start_1
    iput-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332
    :try_start_2
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->close(Z)V
    :try_end_2
    .catch Lcom/samsung/dcm/framework/exceptions/ClosingIndexException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 336
    :goto_2
    const/4 v3, 0x0

    :try_start_3
    iput-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 342
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 327
    :catch_0
    move-exception v1

    .line 328
    .local v1, "e1":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 337
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 338
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_5
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v4, "OOM"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 342
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 333
    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v0

    .line 334
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    :try_start_6
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v4, "ClosingIndexException"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 342
    .end local v0    # "e":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
.end method

.method protected commitAndRefresh()V
    .locals 5

    .prologue
    .line 353
    sget-object v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v3, "commitAndRefresh started"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 357
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->commit()V

    .line 358
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/IndexCommitException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 368
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v1

    .line 361
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    sget-object v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v3, "OOM"

    invoke-static {v2, v3, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 362
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V

    goto :goto_0

    .line 363
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v0

    .line 364
    .local v0, "e":Lcom/samsung/dcm/framework/exceptions/IndexCommitException;
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 365
    .end local v0    # "e":Lcom/samsung/dcm/framework/exceptions/IndexCommitException;
    :catch_2
    move-exception v0

    .line 366
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public deleteDocuments(Ljava/lang/Long;Lorg/apache/lucene/index/Term;Ljava/lang/Integer;)J
    .locals 8
    .param p1, "id"    # Ljava/lang/Long;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "reference"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 447
    sget-boolean v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v5, :cond_0

    .line 448
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    const-string v6, "deleteDocumentsDoc not deleted"

    invoke-direct {v5, v6}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 451
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 453
    .local v2, "ll":J
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v5, p2}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->deleteDocuments(Lorg/apache/lucene/index/Term;)J

    move-result-wide v2

    .line 454
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual {v5, p3, v2, v3}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->add(Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    .line 465
    :goto_1
    return-wide v2

    .line 451
    .end local v2    # "ll":J
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0

    .line 455
    .restart local v2    # "ll":J
    :catch_0
    move-exception v4

    .line 456
    .local v4, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "OOM"

    invoke-static {v5, v6, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 457
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_1

    .line 458
    .end local v4    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v0

    .line 459
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "deleteDocuments Exception"

    invoke-static {v5, v6, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 463
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_1

    .line 460
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 461
    .local v1, "general":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {p0, v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 463
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_1

    .end local v1    # "general":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    throw v5
.end method

.method public deleteDocuments(Ljava/lang/Long;Lorg/apache/lucene/search/Query;)J
    .locals 8
    .param p1, "id"    # Ljava/lang/Long;
    .param p2, "q"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 420
    sget-boolean v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v5, :cond_0

    .line 421
    new-instance v5, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    const-string v6, "deleteDocumentsby query doc not deleted"

    invoke-direct {v5, v6}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 424
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 427
    .local v2, "ll":J
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v5, p2}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->deleteDocuments(Lorg/apache/lucene/search/Query;)J
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 436
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    .line 438
    :goto_1
    return-wide v2

    .line 424
    .end local v2    # "ll":J
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0

    .line 428
    .restart local v2    # "ll":J
    :catch_0
    move-exception v4

    .line 429
    .local v4, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "OOM"

    invoke-static {v5, v6, v4}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 430
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 436
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_1

    .line 431
    .end local v4    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v0

    .line 432
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v5, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "deleteDocuments Exception"

    invoke-static {v5, v6, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 436
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_1

    .line 433
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 434
    .local v1, "general":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {p0, v1}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 436
    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_1

    .end local v1    # "general":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-direct {p0, v7}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    throw v5
.end method

.method public getCurrentSearchId()J
    .locals 2

    .prologue
    .line 781
    sget-boolean v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v0, :cond_0

    .line 782
    const-wide/16 v0, -0x1

    .line 785
    :goto_0
    return-wide v0

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "NRT must be intialized"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 785
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v0}, Lorg/apache/lucene/search/NRTManager;->getCurrentSearchingGen()J

    move-result-wide v0

    goto :goto_0

    .line 784
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 193
    sget-boolean v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v11, :cond_0

    .line 194
    new-instance v11, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    const-string v14, "getDCMIndexSearcher Lucene is not ready"

    invoke-direct {v11, v14}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 197
    :cond_0
    const-wide/16 v4, 0x0

    .line 198
    .local v4, "generationid":J
    const/4 v6, 0x0

    .line 200
    .local v6, "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    const/4 v10, 0x0

    .line 201
    .local v10, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 202
    .local v12, "start":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v11}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 205
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v11}, Lorg/apache/lucene/search/NRTManager;->acquire()Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    move-object v10, v0

    .line 206
    invoke-virtual {v10}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/lucene/index/IndexReader;->getRefCount()I

    move-result v9

    .line 207
    .local v9, "refcount":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v11}, Lorg/apache/lucene/search/NRTManager;->getCurrentSearchingGen()J

    move-result-wide v4

    .line 208
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v14, "getIndexSearcher refcount ="

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, " id = "

    aput-object v17, v15, v16

    const/16 v16, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v11, v14, v15}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mSearcherCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual {v11, v10, v4, v5}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->add(Ljava/lang/Object;J)V

    .line 218
    new-instance v7, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v10, v4, v5}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;-><init>(Lcom/samsung/dcm/framework/lucene/DCMNRTManager;Lorg/apache/lucene/search/IndexSearcher;J)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    .end local v6    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .local v7, "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDCMIndexSearcherList:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 224
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v11}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long v2, v14, v12

    .line 226
    .local v2, "end":J
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v14, "[DCM_Performance] Real getDCMIndexSearcher : ["

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "]ms"

    aput-object v17, v15, v16

    invoke-static {v11, v14, v15}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v6, v7

    .line 228
    .end local v7    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v9    # "refcount":I
    .restart local v6    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :goto_0
    return-object v6

    .line 220
    .end local v2    # "end":J
    :catch_0
    move-exception v8

    .line 221
    .local v8, "oom":Ljava/lang/OutOfMemoryError;
    :goto_1
    :try_start_2
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v14, "OOM"

    invoke-static {v11, v14, v8}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 222
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 224
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v11}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long v2, v14, v12

    .line 226
    .restart local v2    # "end":J
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v14, "[DCM_Performance] Real getDCMIndexSearcher : ["

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "]ms"

    aput-object v17, v15, v16

    invoke-static {v11, v14, v15}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 224
    .end local v2    # "end":J
    .end local v8    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v11

    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long v2, v14, v12

    .line 226
    .restart local v2    # "end":J
    sget-object v14, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v15, "[DCM_Performance] Real getDCMIndexSearcher : ["

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    const-string v18, "]ms"

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    throw v11

    .line 224
    .end local v2    # "end":J
    .end local v6    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v7    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v9    # "refcount":I
    :catchall_1
    move-exception v11

    move-object v6, v7

    .end local v7    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v6    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    goto :goto_2

    .line 220
    .end local v6    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v7    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :catch_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v6    # "object":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    goto :goto_1
.end method

.method public getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 758
    sget-boolean v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v0, :cond_0

    .line 759
    new-instance v0, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    const-string v1, "getFacetFieldsLucene not ready"

    invoke-direct {v0, v1}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getTaxoReader()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 664
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 665
    const/4 v0, 0x0

    .line 667
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->getTaxoReader()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected handleException(Ljava/lang/Exception;)V
    .locals 10
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 671
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 672
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "handleException"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 673
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "handleException "

    invoke-static {v4, v5, p1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 674
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sput-wide v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDBExceptionTime:J

    .line 675
    sget-boolean v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTClosed:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    if-nez v4, :cond_1

    .line 676
    :cond_0
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "Already is closed state "

    new-array v6, v9, [Ljava/lang/Object;

    const-string v7, " will simply return"

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 736
    :goto_0
    return-void

    .line 679
    :cond_1
    sput-boolean v8, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    .line 681
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isSystemShutdown()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 682
    const-class v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v4}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->setSystemOnShutdown(Z)V

    .line 683
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "Shutdown started "

    new-array v6, v9, [Ljava/lang/Object;

    const-string v7, " will simply return"

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 690
    :cond_2
    :try_start_0
    sget-boolean v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-eqz v4, :cond_3

    .line 691
    sget-wide v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDBInitTime:J

    sget-wide v6, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDBExceptionTime:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 692
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "DB was loaded after exception "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, " Returning"

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 733
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 697
    :cond_3
    :try_start_1
    const-class v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v4}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->setSuccessState(Z)V

    .line 699
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->setIsRebuildDoneStatus(Landroid/content/Context;Z)Z

    .line 700
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDCMIndexSearcherList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .line 701
    .local v3, "searcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->invalidateIndexSearcher()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 733
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "searcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4

    .line 705
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_2
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->rollback()V

    .line 706
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->close(Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/ClosingIndexException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 716
    :goto_2
    :try_start_3
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v4}, Lorg/apache/lucene/search/NRTManager;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 720
    :goto_3
    const/4 v4, 0x0

    :try_start_4
    iput-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    .line 721
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->resetLuceneDatabase()V

    .line 724
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    if-eqz v4, :cond_5

    .line 725
    const-class v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v4}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->setSuccessState(Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 733
    :goto_4
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 707
    :catch_0
    move-exception v1

    .line 709
    .local v1, "e2":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 710
    .end local v1    # "e2":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 712
    .local v0, "e1":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;->printStackTrace()V

    goto :goto_2

    .line 717
    .end local v0    # "e1":Lcom/samsung/dcm/framework/exceptions/ClosingIndexException;
    :catch_2
    move-exception v0

    .line 718
    .local v0, "e1":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 728
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_5
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "Unable to restart Lucene DB "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "Exit process"

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 729
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->killself()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4
.end method

.method public declared-synchronized intialize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningIndexException;,
            Lcom/samsung/dcm/framework/exceptions/OpeningTaxoException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 770
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v3, "intialize"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 771
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    if-nez v2, :cond_0

    :goto_0
    const-string v1, "NRT must be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 772
    invoke-direct {p0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->intializeLuceneDatabase()V

    .line 773
    new-instance v0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->getIndexWriter()Lorg/apache/lucene/index/IndexWriter;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;-><init>(Lorg/apache/lucene/index/IndexWriter;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    .line 774
    new-instance v0, Lorg/apache/lucene/search/NRTManager;

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/NRTManager;-><init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    .line 775
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/search/NRTManager;->addListener(Lorg/apache/lucene/search/ReferenceManager$RefreshListener;)V

    .line 776
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    .line 777
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDBInitTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 778
    monitor-exit p0

    return-void

    :cond_0
    move v0, v1

    .line 771
    goto :goto_0

    .line 770
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public maybeRefreshBlocking()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 257
    sget-boolean v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v1, :cond_0

    .line 273
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 261
    .local v4, "start":J
    sget-object v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "maybeRefreshBlocking "

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v1}, Lorg/apache/lucene/search/NRTManager;->isSearcherCurrent()Z

    move-result v1

    if-nez v1, :cond_1

    .line 264
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v1}, Lorg/apache/lucene/search/NRTManager;->maybeRefreshBlocking()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 271
    :cond_1
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v4

    .line 272
    .local v2, "end":J
    sget-object v1, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v6, "[DCM_Performance] Real maybeRefresh : ["

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    const/4 v8, 0x1

    const-string v9, "]ms"

    aput-object v9, v7, v8

    invoke-static {v1, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 266
    .end local v2    # "end":J
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e1":Ljava/io/IOException;
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    goto :goto_1

    .line 268
    .end local v0    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 269
    .local v0, "e1":Ljava/lang/Exception;
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method protected releaseIndexSearcher(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;)V
    .locals 10
    .param p1, "searcher"    # Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    .prologue
    const/4 v6, 0x0

    .line 290
    sget-boolean v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v4, :cond_0

    .line 291
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "releaseIndexSearcher called when not ready "

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 296
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->getRefCount()I

    move-result v3

    .line 297
    .local v3, "refcount":I
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "releaseIndexSearcher id ="

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, " ref = "

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDCMIndexSearcherList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 299
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTManager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {p1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getIndexSearcher()Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/NRTManager;->release(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 300
    .end local v3    # "refcount":I
    :catch_0
    move-exception v2

    .line 301
    .local v2, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "OOM"

    invoke-static {v4, v5, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 302
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 303
    .end local v2    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v0

    .line 304
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "releaseIndexSearcher"

    invoke-static {v4, v5, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 308
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 305
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 306
    .local v1, "e1":Ljava/lang/Exception;
    :try_start_3
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v5, "Exception"

    invoke-static {v4, v5, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 308
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .end local v1    # "e1":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mNRTLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4
.end method

.method public update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J
    .locals 7
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p3, "generationid"    # J
    .param p5, "bUsePath"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 638
    .local p2, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    sget-boolean v3, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v3, :cond_0

    .line 639
    new-instance v3, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    const-string v4, "updateDoc not updated"

    invoke-direct {v3, v4}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 644
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mLocalWriterfactory:Lcom/samsung/dcm/framework/lucene/WriterFactory;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/WriterFactory;->getFacetFields()Lorg/apache/lucene/facet/index/FacetFields;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/facet/index/FacetFields;->addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 649
    :goto_0
    const/4 v2, 0x0

    .line 650
    .local v2, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 651
    new-instance v2, Lorg/apache/lucene/index/Term;

    .end local v2    # "term":Lorg/apache/lucene/index/Term;
    const-string v3, "path"

    const-string v4, "path"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    .restart local v2    # "term":Lorg/apache/lucene/index/Term;
    :goto_1
    const-string v3, "id"

    invoke-virtual {p1, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 659
    .local v1, "reference":I
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v2, p1, v3, v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Ljava/lang/Long;Ljava/lang/Integer;)J

    move-result-wide v4

    return-wide v4

    .line 645
    .end local v1    # "reference":I
    .end local v2    # "term":Lorg/apache/lucene/index/Term;
    :catch_0
    move-exception v0

    .line 647
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 655
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "term":Lorg/apache/lucene/index/Term;
    :cond_1
    new-instance v2, Lorg/apache/lucene/index/Term;

    .end local v2    # "term":Lorg/apache/lucene/index/Term;
    const-string v3, "uri"

    const-string v4, "uri"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v2    # "term":Lorg/apache/lucene/index/Term;
    goto :goto_1
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;Ljava/lang/Long;Ljava/lang/Integer;)J
    .locals 16
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p3, "id"    # Ljava/lang/Long;
    .param p4, "reference"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 477
    .local p2, "d":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    sget-boolean v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mbNRTReady:Z

    if-nez v11, :cond_0

    .line 478
    new-instance v11, Lcom/samsung/dcm/framework/exceptions/LuceneException;

    const-string v12, "updateDocumentDoc not updated"

    invoke-direct {v11, v12}, Lcom/samsung/dcm/framework/exceptions/LuceneException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 481
    :cond_0
    const-string v11, "Searcher id cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    const-string v11, "reference cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 485
    .local v6, "ll":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    move-object/from16 v0, p4

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->checkCache(Ljava/lang/Object;)J

    move-result-wide v4

    .line 490
    .local v4, "latestid":J
    cmp-long v11, v4, v6

    if-gtz v11, :cond_2

    .line 491
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mTrackingIndexWriter:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v11, v0, v1}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Ljava/lang/Iterable;)J

    move-result-wide v8

    .line 492
    .local v8, "newid":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    move-object/from16 v0, p4

    invoke-virtual {v11, v0, v8, v9}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->add(Ljava/lang/Object;J)V

    .line 493
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v12, "mDocumentCache size  ="

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->mDocumentCache:Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->size()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 507
    .end local v8    # "newid":J
    :goto_1
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    .line 509
    :goto_2
    return-wide v6

    .line 483
    .end local v4    # "latestid":J
    .end local v6    # "ll":J
    :cond_1
    const-wide/16 v6, 0x0

    goto :goto_0

    .line 496
    .restart local v4    # "latestid":J
    .restart local v6    # "ll":J
    :cond_2
    move-wide v6, v4

    .line 497
    :try_start_1
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v12, "Document is modified update on generation id ="

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 499
    :catch_0
    move-exception v10

    .line 500
    .local v10, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_2
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v12, "OOM"

    invoke-static {v11, v12, v10}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 501
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->handlerOOM()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 507
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_2

    .line 502
    .end local v10    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v2

    .line 503
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v11, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->TAG:Ljava/lang/String;

    const-string v12, "updateDocument Exception"

    invoke-static {v11, v12, v2}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 507
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_2

    .line 504
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 505
    .local v3, "general":Ljava/lang/Exception;
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->handleException(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 507
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    goto :goto_2

    .end local v3    # "general":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->doScheduleCommit(Z)V

    throw v11
.end method
