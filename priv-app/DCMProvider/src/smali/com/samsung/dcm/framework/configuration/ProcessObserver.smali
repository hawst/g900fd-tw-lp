.class public Lcom/samsung/dcm/framework/configuration/ProcessObserver;
.super Ljava/lang/Object;
.source "ProcessObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;
    }
.end annotation


# static fields
.field protected static final MAX_THREAD_SLEEP_MS:J = 0xbb8L

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

.field private mIActivityManager:Landroid/app/IActivityManager;

.field private mPauseRequested:Z

.field private mPauseTimer:Ljava/util/Timer;

.field private mProcessObserver:Landroid/app/IProcessObserver;

.field private mbObserverRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;Landroid/content/Context;)V
    .locals 3
    .param p1, "dcmPolicyManager"    # Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 42
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mIActivityManager:Landroid/app/IActivityManager;

    .line 45
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;

    .line 46
    iput-boolean v2, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseRequested:Z

    .line 69
    new-instance v0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver$1;-><init>(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 119
    const-string v0, "DCMPolicyManager cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 121
    const-string v0, "Context cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mContext:Landroid/content/Context;

    .line 122
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mIActivityManager:Landroid/app/IActivityManager;

    .line 123
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mIActivityManager:Landroid/app/IActivityManager;

    const-string v1, "IAM cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iput-boolean v2, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mbObserverRegistered:Z

    .line 125
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseRequested:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/dcm/framework/configuration/ProcessObserver;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/ProcessObserver;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseRequested:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/dcm/framework/configuration/ProcessObserver;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/ProcessObserver;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized registerProcessObserver()V
    .locals 3

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mbObserverRegistered:Z

    if-nez v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mIActivityManager:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    .line 131
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mbObserverRegistered:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    sget-object v1, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->TAG:Ljava/lang/String;

    const-string v2, "exception"

    invoke-static {v1, v2, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 129
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unregisterProcessObserver()V
    .locals 3

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mbObserverRegistered:Z

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mIActivityManager:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V

    .line 142
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mbObserverRegistered:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    sget-object v1, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->TAG:Ljava/lang/String;

    const-string v2, "exception"

    invoke-static {v1, v2, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 140
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
