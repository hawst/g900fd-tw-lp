.class public final enum Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/IntentHandler$ValidAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum CalendarUpdate:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum CleanUserData:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum DCMPolicy:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum OSUpgradeRebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum RebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum SDScanFinished:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum StartOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

.field public static final enum StopOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 255
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "StartOsc"

    const-string v2, "start_osc"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->StartOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 256
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "StopOsc"

    const-string v2, "stop_osc"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->StopOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 257
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "CleanUserData"

    const-string v2, "clean_user_data"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CleanUserData:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 258
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "RebuildDB"

    const-string v2, "db_rebuild"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->RebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 259
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "CalendarUpdate"

    const-string v2, "calendar_change"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CalendarUpdate:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 260
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "DCMPolicy"

    const/4 v2, 0x5

    const-string v3, "dcm_policy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->DCMPolicy:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 261
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "SDScanFinished"

    const/4 v2, 0x6

    const-string v3, "sdcard_scanned"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->SDScanFinished:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 262
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    const-string v1, "OSUpgradeRebuildDB"

    const/4 v2, 0x7

    const-string v3, "osupgrade_db_rebuild"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->OSUpgradeRebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    .line 254
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->StartOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->StopOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CleanUserData:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->RebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->CalendarUpdate:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->DCMPolicy:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->SDScanFinished:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->OSUpgradeRebuildDB:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 266
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 267
    iput-object p3, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->mName:Ljava/lang/String;

    .line 268
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 275
    const-string v4, "ValidAction fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->values()[Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    move-result-object v1

    .local v1, "arr$":[Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 277
    .local v0, "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    iget-object v4, v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 281
    .end local v0    # "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    :goto_1
    return-object v0

    .line 276
    .restart local v0    # "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 281
    .end local v0    # "aa":Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 254
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/IntentHandler$ValidAction;
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->mName:Ljava/lang/String;

    return-object v0
.end method
