.class Lcom/samsung/dcm/framework/lucene/DCMCacheManager;
.super Ljava/lang/Object;
.source "DCMCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<G:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGenerationIdMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap",
            "<TG;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mObjectLock:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    .line 51
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    .line 55
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;J)V
    .locals 2
    .param p2, "currentSearchingGen"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;J)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    .local p1, "reference":Ljava/lang/Object;, "TG;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 84
    return-void

    .line 82
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public checkCache(Ljava/lang/Object;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)J"
        }
    .end annotation

    .prologue
    .line 138
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    .local p1, "reference":Ljava/lang/Object;, "TG;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 139
    const/4 v0, 0x0

    .line 141
    .local v0, "ll":Ljava/lang/Long;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ll":Ljava/lang/Long;
    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    .restart local v0    # "ll":Ljava/lang/Long;
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 145
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :goto_0
    return-wide v2

    .line 143
    .end local v0    # "ll":Ljava/lang/Long;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1

    .line 145
    .restart local v0    # "ll":Ljava/lang/Long;
    :cond_0
    const-wide/high16 v2, -0x8000000000000000L

    goto :goto_0
.end method

.method public clearAll()V
    .locals 5

    .prologue
    .line 171
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 173
    :try_start_0
    sget-object v0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->TAG:Ljava/lang/String;

    const-string v1, "clearAll items ="

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    iget-object v0, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 178
    return-void

    .line 176
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public clearCache(J)V
    .locals 9
    .param p1, "removedid"    # J

    .prologue
    .line 153
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 155
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v1, "itemsFound":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TG;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 157
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 158
    .local v2, "key":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-gtz v3, :cond_0

    .line 159
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    .end local v2    # "key":Ljava/lang/Long;
    :cond_1
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->removeAll(Ljava/util/Collection;)Z

    .line 164
    sget-object v3, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->TAG:Ljava/lang/String;

    const-string v4, "clearCache items ="

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 168
    return-void

    .line 166
    .end local v0    # "i":I
    .end local v1    # "itemsFound":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TG;>;"
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
.end method

.method public get(Ljava/lang/Object;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)J"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    .local p1, "reference":Ljava/lang/Object;, "TG;"
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 92
    const/4 v2, 0x0

    .line 94
    .local v2, "ll":Ljava/lang/Long;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/Long;

    move-object v2, v0

    .line 96
    const-string v3, " Map missed something ?"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 101
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    return-wide v4

    .line 98
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
.end method

.method getAll()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TG;>;"
        }
    .end annotation

    .prologue
    .line 211
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 213
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 216
    .local v0, "set":Ljava/util/Set;, "Ljava/util/Set<TG;>;"
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    .end local v0    # "set":Ljava/util/Set;, "Ljava/util/Set<TG;>;"
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method

.method getoldestid(J)J
    .locals 7
    .param p1, "currentid"    # J

    .prologue
    .line 109
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 110
    move-wide v2, p1

    .line 112
    .local v2, "oldid":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 113
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 114
    .local v1, "key":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 112
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    .end local v1    # "key":Ljava/lang/Long;
    :cond_1
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 119
    return-wide v2

    .line 117
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4
.end method

.method public remove(Ljava/lang/Object;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)J"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    .local p1, "reference":Ljava/lang/Object;, "TG;"
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 64
    const-wide/16 v0, 0x0

    .line 66
    .local v0, "value":J
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 68
    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 70
    return-wide v0

    .line 68
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2
.end method

.method public size()I
    .locals 3

    .prologue
    .line 123
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    const/4 v0, 0x0

    .line 124
    .local v0, "size":I
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 128
    iget-object v1, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 130
    return v0

    .line 128
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method

.method public verifyIS()V
    .locals 9

    .prologue
    .line 186
    .local p0, "this":Lcom/samsung/dcm/framework/lucene/DCMCacheManager;, "Lcom/samsung/dcm/framework/lucene/DCMCacheManager<TG;>;"
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 188
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v1, "itemsFound":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TG;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 190
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "verifyIS Key id = "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v8, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/lucene/search/IndexSearcher;

    if-eqz v4, :cond_0

    .line 192
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/IndexSearcher;

    .line 193
    .local v3, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v3}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->getRefCount()I

    move-result v2

    .line 194
    .local v2, "refcount":I
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->TAG:Ljava/lang/String;

    const-string v5, "v refcount id ="

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    if-nez v2, :cond_0

    .line 197
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    .end local v2    # "refcount":I
    .end local v3    # "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 202
    sget-object v4, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "verifyIS Releasing refcount is zero for  = "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mGenerationIdMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v1}, Landroid/util/ArrayMap;->removeAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :cond_2
    iget-object v4, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 208
    return-void

    .line 206
    .end local v0    # "i":I
    .end local v1    # "itemsFound":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TG;>;"
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/samsung/dcm/framework/lucene/DCMCacheManager;->mObjectLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4
.end method
