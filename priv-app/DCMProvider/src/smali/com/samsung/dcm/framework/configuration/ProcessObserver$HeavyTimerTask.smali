.class public Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;
.super Ljava/util/TimerTask;
.source "ProcessObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/configuration/ProcessObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HeavyTimerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 53
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    monitor-enter v1

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseRequested:Z
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$000(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 56
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseRequested:Z
    invoke-static {v0, v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$002(Lcom/samsung/dcm/framework/configuration/ProcessObserver;Z)Z

    .line 57
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$100(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Ljava/util/Timer;

    move-result-object v0

    new-instance v2, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;

    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    invoke-direct {v2, v3}, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;-><init>(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 64
    :goto_0
    monitor-exit v1

    .line 65
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$200(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    move-result-object v0

    sget-object v2, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;->APP:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;

    invoke-virtual {v0, v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->resumeHeavyTask(Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$CPUIntensiveTaskPolicy;)V

    .line 61
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    # getter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$100(Lcom/samsung/dcm/framework/configuration/ProcessObserver;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 62
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/ProcessObserver$HeavyTimerTask;->this$0:Lcom/samsung/dcm/framework/configuration/ProcessObserver;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/dcm/framework/configuration/ProcessObserver;->mPauseTimer:Ljava/util/Timer;
    invoke-static {v0, v2}, Lcom/samsung/dcm/framework/configuration/ProcessObserver;->access$102(Lcom/samsung/dcm/framework/configuration/ProcessObserver;Ljava/util/Timer;)Ljava/util/Timer;

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
