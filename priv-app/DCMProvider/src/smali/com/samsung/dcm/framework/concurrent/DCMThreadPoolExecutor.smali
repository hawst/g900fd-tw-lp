.class public Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "DCMThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$1;,
        Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;,
        Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;,
        Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;,
        Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final mKeepAliveTimeInSeconds:I = 0x5


# instance fields
.field private isPaused:Z

.field private final mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

.field public mListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

.field private final mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

.field private final pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final unpaused:Ljava/util/concurrent/locks/Condition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;Ljava/util/concurrent/LinkedBlockingQueue;)V
    .locals 9
    .param p1, "listener"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;
    .param p2, "threadfactory"    # Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;",
            "Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p3, "linkedBlockingQueue":Ljava/util/concurrent/LinkedBlockingQueue;, "Ljava/util/concurrent/LinkedBlockingQueue<Ljava/lang/Runnable;>;"
    invoke-virtual {p2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getMinThreadCount()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadFactory;->getMaxThreadCount()I

    move-result v3

    const-wide/16 v4, 0x5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    move-object v7, p3

    move-object v8, p2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 43
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    .line 44
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isPaused:Z

    .line 48
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 49
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->unpaused:Ljava/util/concurrent/locks/Condition;

    .line 205
    new-instance v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DCMRejectedExecutionHandler;-><init>(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$1;)V

    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V

    .line 206
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 207
    iput-object p1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    .line 209
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private doPauseAllTask()V
    .locals 4

    .prologue
    .line 257
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 259
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 260
    .local v0, "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 261
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->pause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 265
    .end local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .end local v1    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .restart local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .restart local v1    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :cond_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 267
    return-void
.end method

.method private doResumeAllTask()V
    .locals 4

    .prologue
    .line 281
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 283
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 284
    .local v0, "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 285
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->resume()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 289
    .end local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .end local v1    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .restart local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .restart local v1    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :cond_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 291
    return-void
.end method


# virtual methods
.method protected afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 7
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 301
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 302
    sget-object v2, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v3, "afterExecute"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    const/4 v1, 0x0

    .line 304
    .local v1, "future":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 307
    :try_start_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    check-cast p1, Ljava/util/concurrent/Future;

    .end local p1    # "r":Ljava/lang/Runnable;
    invoke-virtual {v2, p1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->remove(Ljava/util/concurrent/Future;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    move-result-object v1

    .line 308
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->purge()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 316
    :goto_0
    if-eqz v1, :cond_1

    .line 317
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    if-eqz v2, :cond_0

    .line 318
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mListener:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;

    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mRunnable:Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    invoke-static {v1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->access$200(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;->onTaskFinished(Ljava/lang/Runnable;)V

    .line 320
    :cond_0
    invoke-virtual {v1, p2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->onCallback(Ljava/lang/Throwable;)V

    .line 323
    :cond_1
    return-void

    .line 309
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v3, "afterExecute"

    invoke-static {v2, v3, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 311
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
.end method

.method protected beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Thread;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 235
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isPaused:Z

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->unpaused:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 243
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :goto_1
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V

    .line 244
    return-void

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public varargs cancelTasks([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)V
    .locals 9
    .param p1, "runnables"    # [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    .prologue
    .line 352
    sget-object v7, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v8, "cancelTasks runnables"

    invoke-static {v7, v8, p1}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    iget-object v7, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 355
    if-nez p1, :cond_2

    .line 356
    :try_start_0
    iget-object v7, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 357
    .local v0, "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 358
    .local v4, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 359
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->cancel(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    .end local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .end local v4    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v7

    .line 361
    .restart local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .restart local v4    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :cond_0
    :try_start_1
    iget-object v7, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v7}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->clear()V

    .line 371
    .end local v0    # "allrunnables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    .end local v4    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;>;"
    :cond_1
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->purge()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373
    iget-object v7, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 375
    return-void

    .line 364
    :cond_2
    move-object v1, p1

    .local v1, "arr$":[Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    :try_start_2
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v6, v1, v3

    .line 365
    .local v6, "runnable":Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    iget-object v7, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v7, v6}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->remove(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    move-result-object v2

    .line 366
    .local v2, "future":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    if-eqz v2, :cond_3

    .line 367
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->cancel(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 364
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public varargs isTasksRunning([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z
    .locals 7
    .param p1, "runnables"    # [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    .prologue
    .line 378
    sget-object v5, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v6, "isTasksRunning"

    invoke-static {v5, v6, p1}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    iget-object v5, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 380
    const/4 v4, 0x0

    .line 382
    .local v4, "status":Z
    if-eqz p1, :cond_2

    .line 383
    move-object v0, p1

    .local v0, "arr$":[Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    :try_start_0
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 384
    .local v3, "runnable":Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    iget-object v5, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v5, v3}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->containsKey(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 385
    const/4 v4, 0x1

    .line 394
    .end local v0    # "arr$":[Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "runnable":Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 396
    return v4

    .line 383
    .restart local v0    # "arr$":[Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "runnable":Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 391
    .end local v0    # "arr$":[Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "runnable":Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v5}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-nez v5, :cond_3

    const/4 v4, 0x1

    :goto_2
    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    .line 394
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 249
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isPaused:Z

    .line 250
    invoke-direct {p0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->doPauseAllTask()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 254
    return-void

    .line 252
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 272
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->isPaused:Z

    .line 273
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->unpaused:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 274
    invoke-direct {p0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->doResumeAllTask()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    iget-object v0, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 278
    return-void

    .line 276
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;

    invoke-virtual {p0, v0}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->cancelTasks([Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)V

    .line 218
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1, "task"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 228
    invoke-super {p0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public submitNewTask(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)Ljava/util/concurrent/Future;
    .locals 8
    .param p1, "runnable"    # Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;
    .param p2, "callback"    # Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;",
            "Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 326
    sget-object v3, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v4, "Task being added"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 329
    :try_start_0
    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v3, p1}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->get(Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;)Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    move-result-object v2

    .line 331
    .local v2, "futureWrapper":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->isDone()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 332
    :cond_0
    invoke-interface {p1}, Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;->reset()V

    .line 333
    invoke-super {p0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    .line 334
    .local v1, "ff":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    new-instance v2, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;

    .end local v2    # "futureWrapper":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    invoke-direct {v2, v1, p1, p2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;-><init>(Ljava/util/concurrent/Future;Lcom/samsung/dcm/framework/concurrent/IDCMRunnable;Lcom/samsung/dcm/framework/concurrent/IDCMTaskHelper;)V

    .line 335
    .restart local v2    # "futureWrapper":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mTaskFutureMap:Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;

    invoke-virtual {v3, v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$DoubleKeyMap;->add(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)V

    .line 336
    sget-object v3, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v4, "Task"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    const-string v7, " is submitted"

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342
    .end local v1    # "ff":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    :goto_0
    # getter for: Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->mFuture:Ljava/util/concurrent/Future;
    invoke-static {v2}, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;->access$100(Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 346
    iget-object v4, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 348
    .end local v2    # "futureWrapper":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    :goto_1
    return-object v3

    .line 339
    .restart local v2    # "futureWrapper":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v4, "Task"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, " is already active not submitted"

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 343
    .end local v2    # "futureWrapper":Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$FutureWrapper;
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->TAG:Ljava/lang/String;

    const-string v4, "submitNewTask"

    invoke-static {v3, v4, v0}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 346
    iget-object v3, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 348
    const/4 v3, 0x0

    goto :goto_1

    .line 346
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;->mFutureProtect:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v3
.end method
