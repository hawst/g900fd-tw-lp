.class public final enum Lcom/samsung/dcm/framework/extractormanager/Operation$Family;
.super Ljava/lang/Enum;
.source "Operation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/extractormanager/Operation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Family"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/extractormanager/Operation$Family;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

.field public static final enum DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

.field public static final enum INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

.field public static final enum UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    const-string v1, "INSERT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    .line 25
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    .line 26
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    .line 23
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/extractormanager/Operation$Family;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/extractormanager/Operation$Family;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    return-object v0
.end method
