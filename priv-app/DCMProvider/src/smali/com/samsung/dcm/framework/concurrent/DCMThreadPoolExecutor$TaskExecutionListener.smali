.class public interface abstract Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor$TaskExecutionListener;
.super Ljava/lang/Object;
.source "DCMThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/concurrent/DCMThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TaskExecutionListener"
.end annotation


# virtual methods
.method public abstract onTaskFinished(Ljava/lang/Runnable;)V
.end method

.method public abstract onTaskRejected(Ljava/lang/Runnable;)V
.end method
