.class public Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "StorageProcessorTask.java"


# static fields
.field private static final DB_IMAGES_PROJECTION:[Ljava/lang/String;

.field private static final SELECTION_ARGS:[Ljava/lang/String;


# instance fields
.field private mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

.field private mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

.field private mMediaStoreClient:Landroid/content/ContentProviderClient;

.field private mStartId:I

.field private mStorageId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 58
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v2

    const-string v1, "storage_id"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->DB_IMAGES_PROJECTION:[Ljava/lang/String;

    .line 65
    new-array v0, v4, [Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->SELECTION_ARGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/extractormanager/DCMController;Lcom/samsung/dcm/framework/extractormanager/DCMContainer;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;I)V
    .locals 2
    .param p1, "dcmController"    # Lcom/samsung/dcm/framework/extractormanager/DCMController;
    .param p2, "container"    # Lcom/samsung/dcm/framework/extractormanager/DCMContainer;
    .param p3, "storageId"    # I
    .param p4, "taskStatusListener"    # Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    .param p5, "startid"    # I

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 52
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    .line 54
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStartId:I

    .line 74
    const-string v0, "Controller cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/DCMController;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    .line 75
    iput p3, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    .line 76
    iput-object p4, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 77
    iput p5, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStartId:I

    .line 78
    return-void
.end method

.method private getDataFromMediaProvider(Landroid/content/ContentProviderClient;[Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "mMediaStoreClient2"    # Landroid/content/ContentProviderClient;
    .param p2, "dbImagesProjection"    # [Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 210
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 211
    .local v1, "external_uri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "media_type = ? AND storage_id = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 216
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object v2, p2

    move-object v4, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 218
    .local v7, "cursor":Landroid/database/Cursor;
    return-object v7
.end method

.method private sendSelfIntentForInsertOrUpdate(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 6
    .param p3, "storageID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "mediaUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "mediaPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 185
    .local v2, "insertIntent":Landroid/content/Intent;
    const-string v4, "com.samsung.dcm"

    const-string v5, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 187
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "operation"

    const-string v5, "insert"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v4, "origin"

    const-string v5, "com.android.providers.media.MediaProvider"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 191
    .local v1, "data":Landroid/os/Bundle;
    const-string v4, "ids"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 192
    const-string v4, "storageId"

    invoke-virtual {v1, v4, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 195
    .local v3, "options":Landroid/os/Bundle;
    const-string v4, "bulk"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 196
    const-string v4, "reason"

    const-string v5, "sdcard"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v4, "paths"

    invoke-virtual {v3, v4, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 198
    const-string v4, "options"

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 200
    const-string v4, "data"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 201
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 203
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 204
    return-void
.end method


# virtual methods
.method protected internalRun()V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 87
    .local v5, "contentresolver":Landroid/content/ContentResolver;
    const-string v19, "external"

    invoke-static/range {v19 .. v19}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 90
    const/4 v6, 0x0

    .line 92
    .local v6, "datacursor":Landroid/database/Cursor;
    const-string v19, "external"

    invoke-static/range {v19 .. v19}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 94
    .local v8, "external_uri":Landroid/net/Uri;
    new-instance v11, Ljava/util/ArrayList;

    const/16 v19, 0x64

    move/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 95
    .local v11, "mediaUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v13, Ljava/util/ArrayList;

    const/16 v19, 0x64

    move/from16 v0, v19

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 98
    .local v13, "mediapaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->SELECTION_ARGS:[Ljava/lang/String;

    .local v4, "arr$":[Ljava/lang/String;
    array-length v10, v4

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_e

    aget-object v17, v4, v9

    .line 99
    .local v17, "selectarg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    sget-object v20, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->DB_IMAGES_PROJECTION:[Ljava/lang/String;

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v17, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->getDataFromMediaProvider(Landroid/content/ContentProviderClient;[Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 100
    if-eqz v6, :cond_d

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->TAG:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "Cursor entries =  "

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v19 .. v21}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    const-string v19, "_id"

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 103
    .local v12, "mediaid_idx":I
    const-string v19, "_data"

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 105
    .local v16, "path_idx":I
    const-string v19, "mime_type"

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 107
    .local v14, "mime_idx":I
    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-eqz v19, :cond_b

    .line 108
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/StorageController;->getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/dcm/framework/configuration/StorageController;->isVolumeMounted(I)Z

    move-result v19

    if-nez v19, :cond_4

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->TAG:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, " Media unmounted id = "

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v19 .. v21}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    if-eqz v6, :cond_1

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->startWorkerThreads()V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    if-eqz v19, :cond_2

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentProviderClient;->release()Z

    .line 173
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStartId:I

    move/from16 v20, v0

    invoke-interface/range {v19 .. v20}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    .line 178
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "datacursor":Landroid/database/Cursor;
    .end local v8    # "external_uri":Landroid/net/Uri;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "mediaUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "mediaid_idx":I
    .end local v13    # "mediapaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14    # "mime_idx":I
    .end local v16    # "path_idx":I
    .end local v17    # "selectarg":Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 113
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v6    # "datacursor":Landroid/database/Cursor;
    .restart local v8    # "external_uri":Landroid/net/Uri;
    .restart local v9    # "i$":I
    .restart local v10    # "len$":I
    .restart local v11    # "mediaUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "mediaid_idx":I
    .restart local v13    # "mediapaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "mime_idx":I
    .restart local v16    # "path_idx":I
    .restart local v17    # "selectarg":Ljava/lang/String;
    :cond_4
    :try_start_1
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    invoke-interface {v6, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/commons/Algorithms;->appendMimeToUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 116
    .local v18, "uri":Landroid/net/Uri;
    move/from16 v0, v16

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 119
    .local v15, "path":Ljava/lang/String;
    invoke-static {v15}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isValidPath(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 120
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x64

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_0

    .line 123
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v11, v13, v1}, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->sendSelfIntentForInsertOrUpdate(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 124
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 125
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 160
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v12    # "mediaid_idx":I
    .end local v14    # "mime_idx":I
    .end local v15    # "path":Ljava/lang/String;
    .end local v16    # "path_idx":I
    .end local v17    # "selectarg":Ljava/lang/String;
    .end local v18    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 161
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->TAG:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "Exceptions "

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v7}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 164
    if-eqz v6, :cond_5

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->startWorkerThreads()V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentProviderClient;->release()Z

    .line 173
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStartId:I

    move/from16 v20, v0

    invoke-interface/range {v19 .. v20}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    goto/16 :goto_2

    .line 140
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v9    # "i$":I
    .restart local v10    # "len$":I
    .restart local v12    # "mediaid_idx":I
    .restart local v14    # "mime_idx":I
    .restart local v15    # "path":Ljava/lang/String;
    .restart local v16    # "path_idx":I
    .restart local v17    # "selectarg":Ljava/lang/String;
    .restart local v18    # "uri":Landroid/net/Uri;
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->TAG:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "DELETE command for path = "

    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v15, v21, v22

    const/16 v22, 0x1

    const-string v23, "\nUri = "

    aput-object v23, v21, v22

    const/16 v22, 0x2

    aput-object v18, v21, v22

    invoke-static/range {v19 .. v21}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 164
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v12    # "mediaid_idx":I
    .end local v14    # "mime_idx":I
    .end local v15    # "path":Ljava/lang/String;
    .end local v16    # "path_idx":I
    .end local v17    # "selectarg":Ljava/lang/String;
    .end local v18    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v19

    if-eqz v6, :cond_8

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->startWorkerThreads()V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v20, v0

    if-eqz v20, :cond_9

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderClient;->release()Z

    .line 173
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStartId:I

    move/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    :cond_a
    throw v19

    .line 146
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v9    # "i$":I
    .restart local v10    # "len$":I
    .restart local v12    # "mediaid_idx":I
    .restart local v14    # "mime_idx":I
    .restart local v16    # "path_idx":I
    .restart local v17    # "selectarg":Ljava/lang/String;
    :cond_b
    :try_start_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_c

    .line 147
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStorageId:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v11, v13, v1}, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->sendSelfIntentForInsertOrUpdate(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 148
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 149
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 152
    :cond_c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 153
    const/4 v6, 0x0

    .line 98
    .end local v12    # "mediaid_idx":I
    .end local v14    # "mime_idx":I
    .end local v16    # "path_idx":I
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 156
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->TAG:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "cursor is null"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-static/range {v19 .. v21}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 164
    .end local v17    # "selectarg":Ljava/lang/String;
    :cond_e
    if-eqz v6, :cond_f

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mDCMController:Lcom/samsung/dcm/framework/extractormanager/DCMController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/dcm/framework/extractormanager/DCMController;->startWorkerThreads()V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    if-eqz v19, :cond_10

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mMediaStoreClient:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentProviderClient;->release()Z

    .line 173
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mITaskStatusListener:Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/StorageProcessorTask;->mStartId:I

    move/from16 v20, v0

    invoke-interface/range {v19 .. v20}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    goto/16 :goto_2
.end method
