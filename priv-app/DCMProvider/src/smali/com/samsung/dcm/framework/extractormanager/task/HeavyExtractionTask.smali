.class public Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "HeavyExtractionTask.java"

# interfaces
.implements Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;


# static fields
.field private static final kMAX_OSC_COUNT:I = 0x1f4

.field private static localDoc:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field

.field private static localbundle:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private static mContext:Landroid/content/Context;

.field private static mIsAlive:Z

.field private static mOSCProcessed:I

.field private static m_Current_MAX_OSC_COUNT:I


# instance fields
.field private mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

.field mLastProcessedImageID:Ljava/lang/Integer;

.field private mLuceneDoc:Lorg/apache/lucene/document/Document;

.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field private final mPolicyPauseVariable:Landroid/os/ConditionVariable;

.field private final mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

.field private mUpdateCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    .line 105
    const/16 v0, 0x1f4

    sput v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->m_Current_MAX_OSC_COUNT:I

    .line 106
    const/4 v0, 0x0

    sput v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    .line 112
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask$1;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->localDoc:Ljava/lang/ThreadLocal;

    .line 119
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask$2;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask$2;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->localbundle:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 96
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLastProcessedImageID:Ljava/lang/Integer;

    .line 98
    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 101
    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 109
    new-instance v1, Landroid/os/ConditionVariable;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mPolicyPauseVariable:Landroid/os/ConditionVariable;

    .line 129
    const-class v1, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v1}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 130
    .local v0, "config":Lcom/samsung/dcm/framework/configuration/DCMConfig;
    const-string v1, "DCMConfig cannot be null"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    .line 132
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    const-string v2, "Context cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    .line 134
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    const-string v2, "DcmTransport cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getDCMPolicyManager()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    .line 136
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    const-string v2, "DCMPolicyManager cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mTransport:Lcom/samsung/dcm/framework/configuration/DcmTransport;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 138
    const/4 v1, 0x0

    sput v1, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    .line 139
    return-void
.end method

.method private executeOneBatch(I)I
    .locals 26
    .param p1, "imagesToProcess"    # I

    .prologue
    .line 185
    sget-object v2, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    const-class v4, Lcom/samsung/dcm/documents/ImageMediaDoc;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->getLuceneQuery(Lcom/samsung/dcm/documents/MediaDoc$DocType;Ljava/lang/Class;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v3

    .line 186
    .local v3, "query":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .line 189
    .local v1, "indexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v1

    .line 191
    const-string v2, "IndexSearcher cannot be null"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    new-instance v20, Lorg/apache/lucene/search/SortField;

    const-string v2, "id"

    sget-object v4, Lorg/apache/lucene/search/SortField$Type;->INT:Lorg/apache/lucene/search/SortField$Type;

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v4}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;)V

    .line 194
    .local v20, "sfield":Lorg/apache/lucene/search/SortField;
    new-instance v6, Lorg/apache/lucene/search/Sort;

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    .line 196
    .local v6, "sortby":Lorg/apache/lucene/search/Sort;
    const/4 v2, 0x0

    const/4 v4, 0x0

    move/from16 v5, p1

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v21

    .line 197
    .local v21, "topDocs":Lorg/apache/lucene/search/TopDocs;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v4, "executeOneBatch:VA Pending Images ="

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v24, 0x0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v5, v24

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    move-object/from16 v0, v21

    iget v2, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    if-nez v2, :cond_0

    .line 199
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->setHeavySharedPref(Landroid/content/Context;Z)V

    .line 200
    const/16 v2, 0x1f4

    sput v2, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->m_Current_MAX_OSC_COUNT:I

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->finishNow()V

    .line 204
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v9, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    move-object/from16 v0, v21

    iget-object v7, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v7, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v0, v7

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    move v15, v14

    .end local v14    # "i$":I
    .local v15, "i$":I
    :goto_0
    move/from16 v0, v16

    if-ge v15, v0, :cond_1

    aget-object v19, v7, v15

    .line 207
    .local v19, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->maybePause()V

    .line 208
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isFinishedNow()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 252
    .end local v15    # "i$":I
    .end local v19    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_1
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isSystemShutdown()Z

    move-result v2

    if-nez v2, :cond_3

    .line 253
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 254
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->updateDocuments(Ljava/util/ArrayList;)V

    .line 255
    sget v2, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v2, v4

    sput v2, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    .line 257
    :cond_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :cond_3
    if-eqz v1, :cond_4

    .line 270
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 273
    .end local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v9    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v16    # "len$":I
    .end local v20    # "sfield":Lorg/apache/lucene/search/SortField;
    .end local v21    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_4
    :goto_1
    return p1

    .line 212
    .restart local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .restart local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v9    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .restart local v15    # "i$":I
    .restart local v16    # "len$":I
    .restart local v19    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v20    # "sfield":Lorg/apache/lucene/search/SortField;
    .restart local v21    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_5
    :try_start_1
    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v10

    .line 213
    .local v10, "doc":Lorg/apache/lucene/document/Document;
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v2

    const-class v4, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v2, v4}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v13

    .line 216
    .local v13, "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    const-string v2, "path"

    invoke-virtual {v10, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v18

    .line 218
    .local v18, "path":Ljava/lang/String;
    if-eqz v13, :cond_6

    .line 219
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .end local v15    # "i$":I
    .local v14, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 220
    .local v12, "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-static/range {v18 .. v18}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isValidPathforOSC(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 222
    const-string v2, "id"

    invoke-virtual {v10, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLastProcessedImageID:Ljava/lang/Integer;

    .line 244
    .end local v12    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v14    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isFinishedNow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 206
    add-int/lit8 v14, v15, 0x1

    .local v14, "i$":I
    move v15, v14

    .end local v14    # "i$":I
    .restart local v15    # "i$":I
    goto/16 :goto_0

    .line 226
    .end local v15    # "i$":I
    .restart local v12    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .local v14, "i$":Ljava/util/Iterator;
    :cond_7
    invoke-virtual {v12, v10}, Lcom/samsung/dcm/framework/extractors/Extractor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/Bundle;

    .line 227
    .local v17, "outdata":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v4, "outdata :"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v24, 0x0

    aput-object v17, v5, v24

    invoke-static {v2, v4, v5}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    const-string v2, "uri"

    invoke-virtual {v10, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v23

    .line 230
    .local v23, "uri_field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface/range {v23 .. v23}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v22

    .line 231
    .local v22, "uri":Ljava/lang/String;
    new-instance v8, Lcom/samsung/dcm/framework/extractormanager/Command;

    new-instance v2, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;-><init>(Landroid/net/Uri;)V

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-direct {v8, v2, v4}, Lcom/samsung/dcm/framework/extractormanager/Command;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;)V

    .line 232
    .local v8, "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    if-eqz v17, :cond_8

    .line 233
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/dcm/framework/extractormanager/Command;->setData(Landroid/os/Bundle;)V

    .line 236
    :cond_8
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/samsung/dcm/framework/extractors/Extractor;

    const/4 v4, 0x0

    aput-object v12, v2, v4

    invoke-virtual {v8, v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->setExtractors([Lcom/samsung/dcm/framework/extractors/Extractor;)V

    .line 241
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->maybePause()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/dcm/framework/exceptions/SearchingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 262
    .end local v6    # "sortby":Lorg/apache/lucene/search/Sort;
    .end local v7    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v8    # "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v9    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    .end local v10    # "doc":Lorg/apache/lucene/document/Document;
    .end local v12    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v13    # "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v16    # "len$":I
    .end local v17    # "outdata":Landroid/os/Bundle;
    .end local v18    # "path":Ljava/lang/String;
    .end local v19    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v20    # "sfield":Lorg/apache/lucene/search/SortField;
    .end local v21    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    .end local v22    # "uri":Ljava/lang/String;
    .end local v23    # "uri_field":Lorg/apache/lucene/index/IndexableField;
    :catch_0
    move-exception v11

    .line 263
    .local v11, "e":Ljava/io/IOException;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v4, "Exception"

    invoke-static {v2, v4, v11}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 269
    if-eqz v1, :cond_4

    .line 270
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_1

    .line 264
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 265
    .local v11, "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v4, "SearchingException"

    invoke-static {v2, v4, v11}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 269
    if-eqz v1, :cond_4

    .line 270
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_1

    .line 266
    .end local v11    # "e":Lcom/samsung/dcm/framework/exceptions/SearchingException;
    :catch_2
    move-exception v11

    .line 267
    .local v11, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v4, "LuceneException"

    invoke-static {v2, v4, v11}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 269
    if-eqz v1, :cond_4

    .line 270
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_1

    .line 269
    .end local v11    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_9

    .line 270
    invoke-virtual {v1}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_9
    throw v2
.end method

.method private getLuceneQuery(Lcom/samsung/dcm/documents/MediaDoc$DocType;Ljava/lang/Class;)Lorg/apache/lucene/search/BooleanQuery;
    .locals 16
    .param p1, "type"    # Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;)",
            "Lorg/apache/lucene/search/BooleanQuery;"
        }
    .end annotation

    .prologue
    .line 323
    .local p2, "classtype":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    new-instance v8, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v8}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 327
    .local v8, "query":Lorg/apache/lucene/search/BooleanQuery;
    const-string v11, "id"

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLastProcessedImageID:Ljava/lang/Integer;

    const v13, 0x7fffffff

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v11, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 329
    .local v1, "atomicQuery":Lorg/apache/lucene/search/Query;
    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v1, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 332
    const-string v11, "deleted"

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v11, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v2

    .line 334
    .local v2, "deleteQuery":Lorg/apache/lucene/search/Query;
    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v2, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 336
    invoke-static {}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getStorageQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v9

    .line 337
    .local v9, "storageQuery":Lorg/apache/lucene/search/BooleanQuery;
    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v9, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 341
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v4

    .line 344
    .local v4, "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    if-eqz v4, :cond_0

    .line 346
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 347
    .local v3, "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    .line 348
    .local v6, "name":Ljava/lang/String;
    new-instance v7, Lorg/apache/lucene/search/TermQuery;

    new-instance v11, Lorg/apache/lucene/index/Term;

    const-string v12, "false"

    invoke-direct {v11, v6, v12}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v11}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 350
    .local v7, "pluginquery":Lorg/apache/lucene/search/Query;
    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v7, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v12, "Lucene being queried for"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v6, v13, v14

    invoke-static {v11, v12, v13}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 355
    .end local v3    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "pluginquery":Lorg/apache/lucene/search/Query;
    :cond_0
    const-string v11, "doctype"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static {v11, v12, v13, v14, v15}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v10

    .line 357
    .local v10, "typeQuery":Lorg/apache/lucene/search/Query;
    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 364
    return-object v8
.end method

.method public static isAlive()Z
    .locals 1

    .prologue
    .line 578
    sget-boolean v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mIsAlive:Z

    return v0
.end method

.method private updateDocument(Lcom/samsung/dcm/framework/extractors/Extractor;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;J)V
    .locals 18
    .param p1, "plugin"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .param p2, "lucenedoc"    # Lorg/apache/lucene/document/Document;
    .param p3, "outdata"    # Landroid/os/Bundle;
    .param p4, "useGenerationid"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 436
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLuceneDoc:Lorg/apache/lucene/document/Document;

    invoke-static {v4}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mUpdateCategories:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 438
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v15

    .line 439
    .local v15, "name":Ljava/lang/String;
    new-instance v13, Lorg/apache/lucene/document/StringField;

    const-string v4, "true"

    sget-object v5, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v13, v15, v4, v5}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    .line 440
    .local v13, "field":Lorg/apache/lucene/document/StringField;
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 441
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 442
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "Plugin"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v15, v6, v7

    const/4 v7, 0x1

    const-string v8, " set as 1"

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mUpdateCategories:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLuceneDoc:Lorg/apache/lucene/document/Document;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->updateFieldsInDoc(Landroid/os/Bundle;Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V

    .line 466
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLuceneDoc:Lorg/apache/lucene/document/Document;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mUpdateCategories:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    move-wide/from16 v8, p4

    invoke-virtual/range {v5 .. v10}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J

    move-result-wide v16

    .line 468
    .local v16, "newgenid":J
    cmp-long v4, v16, p4

    if-eqz v4, :cond_1

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "race generationid ="

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, " newgenid = "

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 471
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 472
    const/4 v14, 0x0

    .line 473
    .local v14, "indexsearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 475
    .local v11, "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v14

    .line 476
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLuceneDoc:Lorg/apache/lucene/document/Document;

    const-string v5, "uri"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v14, v4, v11}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    if-eqz v14, :cond_0

    .line 483
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 486
    :cond_0
    :goto_0
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 487
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/document/Document;

    const-wide v8, 0x7fffffffffffffffL

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p3

    invoke-direct/range {v4 .. v9}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->updateDocument(Lcom/samsung/dcm/framework/extractors/Extractor;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;J)V

    .line 490
    .end local v11    # "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v14    # "indexsearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :cond_1
    return-void

    .line 479
    .restart local v11    # "doclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v14    # "indexsearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :catch_0
    move-exception v12

    .line 480
    .local v12, "e":Ljava/io/IOException;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "IOException "

    invoke-static {v4, v5, v12}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    if-eqz v14, :cond_0

    .line 483
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto :goto_0

    .line 482
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    if-eqz v14, :cond_2

    .line 483
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_2
    throw v4
.end method

.method private updateDocuments(Ljava/util/ArrayList;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 278
    .local p1, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    const/4 v15, 0x0

    .line 280
    .local v15, "nrtSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 281
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v15

    .line 283
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/Command;

    .line 285
    .local v2, "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isSystemShutdown()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 314
    if-eqz v15, :cond_1

    .line 315
    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 320
    .end local v2    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_1
    :goto_1
    return-void

    .line 288
    .restart local v2    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->getData()Lcom/google/common/base/Optional;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    .line 290
    .local v7, "outdata":Landroid/os/Bundle;
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->getDCMID()Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getUri()Landroid/net/Uri;

    move-result-object v17

    .line 291
    .local v17, "uri":Landroid/net/Uri;
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    .line 292
    .local v16, "selection":Ljava/lang/String;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 293
    .local v14, "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    move-object/from16 v0, v16

    invoke-static {v15, v0, v14}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V

    .line 296
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 298
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "No Document found for the uri"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3, v5, v8}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311
    .end local v2    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v7    # "outdata":Landroid/os/Bundle;
    .end local v14    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v16    # "selection":Ljava/lang/String;
    .end local v17    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v10

    .line 312
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "Exception:"

    invoke-static {v3, v5, v10}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 314
    if-eqz v15, :cond_1

    .line 315
    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto :goto_1

    .line 300
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v2    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .restart local v7    # "outdata":Landroid/os/Bundle;
    .restart local v14    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .restart local v16    # "selection":Ljava/lang/String;
    .restart local v17    # "uri":Landroid/net/Uri;
    :cond_4
    const/4 v3, 0x0

    :try_start_3
    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/document/Document;

    .line 302
    .local v6, "doc":Lorg/apache/lucene/document/Document;
    invoke-virtual {v2}, Lcom/samsung/dcm/framework/extractormanager/Command;->getExtractors()Ljava/util/Set;

    move-result-object v11

    .line 303
    .local v11, "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 304
    .local v4, "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    const/4 v5, 0x0

    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v8

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->updateDocument(Lcom/samsung/dcm/framework/extractors/Extractor;Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;J)V

    .line 305
    const-string v3, "id"

    invoke-virtual {v6, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLastProcessedImageID:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 314
    .end local v2    # "command":Lcom/samsung/dcm/framework/extractormanager/Command;
    .end local v4    # "extractor":Lcom/samsung/dcm/framework/extractors/Extractor;
    .end local v6    # "doc":Lorg/apache/lucene/document/Document;
    .end local v7    # "outdata":Landroid/os/Bundle;
    .end local v11    # "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "imageDocList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/document/Document;>;"
    .end local v16    # "selection":Ljava/lang/String;
    .end local v17    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v3

    if-eqz v15, :cond_5

    .line 315
    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_5
    throw v3

    .line 314
    :cond_6
    if-eqz v15, :cond_1

    .line 315
    invoke-virtual {v15}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_1
.end method

.method private updateFieldsInDoc(Landroid/os/Bundle;Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V
    .locals 20
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .param p4, "newDoc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;",
            "Lorg/apache/lucene/document/Document;",
            ")V"
        }
    .end annotation

    .prologue
    .line 495
    .local p3, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    const-string v17, "OSC_SCENETYPE_TO_BE_ADDED"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 497
    .local v13, "scenetypeToBeAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v17, "OSC_SUBSCENETYPE_TO_BE_ADDED"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 500
    .local v15, "subscenetypeToBeAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 501
    .local v7, "existingSceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 502
    .local v8, "existingSubsceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 504
    .local v6, "existingCategories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Z

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 506
    const-string v17, "scene_type"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v12

    .line 507
    .local v12, "sceneTypeFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    array-length v0, v12

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    .line 508
    aget-object v11, v12, v9

    .line 509
    .local v11, "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v11}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 507
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 512
    .end local v11    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_0
    const-string v17, "subscene_type"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v14

    .line 514
    .local v14, "subsceneTypeFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v9, 0x0

    :goto_1
    array-length v0, v14

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_1

    .line 515
    aget-object v11, v14, v9

    .line 516
    .restart local v11    # "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v11}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 514
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 519
    .end local v11    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_1
    const-string v17, "categories"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v5

    .line 520
    .local v5, "categoryFields":[Lorg/apache/lucene/index/IndexableField;
    const/4 v9, 0x0

    :goto_2
    array-length v0, v5

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_2

    .line 521
    aget-object v11, v5, v9

    .line 522
    .restart local v11    # "iF":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v11}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 520
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 526
    .end local v11    # "iF":Lorg/apache/lucene/index/IndexableField;
    :cond_2
    if-eqz v13, :cond_4

    .line 527
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 528
    .local v16, "tag":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "root_scene_type;"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 530
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "root_scene_type;"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 533
    :cond_3
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 538
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v16    # "tag":Ljava/lang/String;
    :cond_4
    if-eqz v15, :cond_6

    .line 539
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 540
    .restart local v16    # "tag":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "root_subscene_type;"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 542
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "root_subscene_type;"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 545
    :cond_5
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 550
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v16    # "tag":Ljava/lang/String;
    :cond_6
    const-string v17, "scene_type"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 551
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 552
    .restart local v16    # "tag":Ljava/lang/String;
    new-instance v17, Lorg/apache/lucene/document/StringField;

    const-string v18, "scene_type"

    sget-object v19, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_5

    .line 556
    .end local v16    # "tag":Ljava/lang/String;
    :cond_7
    const-string v17, "subscene_type"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 557
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 558
    .restart local v16    # "tag":Ljava/lang/String;
    new-instance v17, Lorg/apache/lucene/document/StringField;

    const-string v18, "subscene_type"

    sget-object v19, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_6

    .line 562
    .end local v16    # "tag":Ljava/lang/String;
    :cond_8
    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v17

    if-lez v17, :cond_9

    .line 563
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->clear()V

    .line 564
    const-string v17, "categories"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 565
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 566
    .local v4, "category":Ljava/lang/String;
    new-instance v17, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v18, ";"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    new-instance v17, Lorg/apache/lucene/document/StoredField;

    const-string v18, "categories"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_7

    .line 575
    .end local v4    # "category":Ljava/lang/String;
    :cond_9
    return-void
.end method


# virtual methods
.method protected afterInternalRun()V
    .locals 5

    .prologue
    .line 665
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->localbundle:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->remove()V

    .line 666
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->localDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->remove()V

    .line 667
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    invoke-virtual {v3, p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->unregisterDCMPolicyObserver(Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;)Z

    .line 668
    const/4 v3, 0x0

    sput-boolean v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mIsAlive:Z

    .line 670
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v3

    const-class v4, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v1

    .line 673
    .local v1, "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 674
    .local v0, "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->release()V

    goto :goto_0

    .line 677
    .end local v0    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    :cond_0
    return-void
.end method

.method protected beforeInternalRun()V
    .locals 5

    .prologue
    .line 640
    const/16 v3, 0xf

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    .line 642
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mIsAlive:Z

    .line 643
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->localbundle:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    .line 644
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->localDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/document/Document;

    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLuceneDoc:Lorg/apache/lucene/document/Document;

    .line 645
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mUpdateCategories:Ljava/util/ArrayList;

    .line 647
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    invoke-virtual {v3, p0}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->registerDCMPolicyObserver(Lcom/samsung/dcm/framework/configuration/IDCMPolicyObserver;)Z

    .line 648
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-static {v3, v4}, Lcom/samsung/dcm/framework/configuration/DCMAffinityControl;->setAffinityForThread(I[I)V

    .line 651
    iget-object v3, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 653
    invoke-static {}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getInstance()Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    move-result-object v3

    const-class v4, Lcom/samsung/dcm/documents/ImageMediaDoc;

    invoke-virtual {v3, v4}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getLateExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v1

    .line 655
    .local v1, "extractorSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractors/Extractor;

    .line 656
    .local v0, "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractors/Extractor;->initialize()V

    goto :goto_0

    .line 658
    .end local v0    # "ext":Lcom/samsung/dcm/framework/extractors/Extractor;
    :cond_0
    return-void

    .line 648
    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public finishNow()V
    .locals 1

    .prologue
    .line 407
    invoke-super {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->finishNow()V

    .line 408
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mPolicyPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 409
    return-void
.end method

.method public internalRun()V
    .locals 7

    .prologue
    .line 143
    const/4 v2, -0x1

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLastProcessedImageID:Ljava/lang/Integer;

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mDCMPolicyManager:Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager;->getCurrentRule()Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;

    move-result-object v0

    .line 146
    .local v0, "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    if-eqz v0, :cond_6

    .line 148
    iget-boolean v2, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mbStopProcessing:Z

    if-eqz v2, :cond_1

    .line 149
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v3, "processing must be stopped"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    .end local v0    # "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    :goto_0
    return-void

    .line 153
    .restart local v0    # "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    :cond_1
    iget v2, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    if-lez v2, :cond_2

    .line 154
    iget v2, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mImagesToProcess:I

    invoke-direct {p0, v2}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->executeOneBatch(I)I

    .line 157
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isFinishedNow()Z

    move-result v2

    if-nez v2, :cond_3

    sget v2, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    sget v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->m_Current_MAX_OSC_COUNT:I

    if-lt v2, v3, :cond_4

    .line 158
    :cond_3
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v3, "Exit thread is requested"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 174
    .end local v0    # "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    :catch_0
    move-exception v1

    .line 175
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_1
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v3, "Thread stopped normally "

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 178
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    throw v2

    .line 162
    .restart local v0    # "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    :cond_4
    :try_start_2
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-static {}, Lcom/samsung/dcm/framework/configuration/DCMNotifications;->getImageUpdateListner()Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->addRefreshListener(Lcom/samsung/dcm/framework/lucene/IDCMNRTRefreshListener;)V

    .line 165
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->maybePause()V

    .line 166
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isFinishedNow()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 167
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v3, "Exit thread is requested"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 176
    .end local v0    # "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    :catch_1
    move-exception v1

    .line 177
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v3, "Exception in OSC thread"

    invoke-static {v2, v3, v1}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 170
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "currentRule":Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;
    :cond_5
    :try_start_4
    iget-object v2, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v3, "Going to sleep for"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    iget v2, v0, Lcom/samsung/dcm/framework/configuration/DCMPolicyManager$ProcessingRule;->mMinTimeoutInMs:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 173
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->isFinishedNow()Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method protected maybePause()V
    .locals 1

    .prologue
    .line 399
    invoke-super {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->maybePause()V

    .line 402
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mPolicyPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 403
    return-void
.end method

.method public onCanProcess(Z)V
    .locals 5
    .param p1, "state"    # Z

    .prologue
    .line 369
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v1, "onCanProcess "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 370
    if-eqz p1, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->policyResume()V

    .line 376
    :goto_0
    return-void

    .line 374
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->policyPause()V

    goto :goto_0
.end method

.method public onShutDown()V
    .locals 0

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->policyResume()V

    .line 414
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->resume()V

    .line 415
    invoke-virtual {p0}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->finishNow()V

    .line 416
    return-void
.end method

.method public policyPause()V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v1, "Heavy task pausing due to policy request"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mPolicyPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 385
    return-void
.end method

.method public policyResume()V
    .locals 3

    .prologue
    .line 392
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v1, "Heavy task resuming due to policy request"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 393
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mPolicyPauseVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 394
    return-void
.end method

.method public postHeavyStartIntent(Z)Z
    .locals 8
    .param p1, "reset"    # Z

    .prologue
    const/4 v3, 0x0

    .line 589
    if-eqz p1, :cond_0

    .line 590
    sput v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    .line 593
    :cond_0
    sget-boolean v4, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mIsAlive:Z

    if-nez v4, :cond_1

    sget v4, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mOSCProcessed:I

    sget v5, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->m_Current_MAX_OSC_COUNT:I

    if-lt v4, v5, :cond_2

    .line 594
    :cond_1
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "postHeavyStartIntent skipped"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move p1, v3

    .line 624
    .end local p1    # "reset":Z
    :goto_0
    return p1

    .line 598
    .restart local p1    # "reset":Z
    :cond_2
    invoke-static {}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isOSCEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isBatterOK(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->isMemoryOK(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/dcm/framework/utils/SystemUtils;->getHeavySharedPref(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 603
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "send intent to start OSC"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 604
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.dcm.action.DCM_EXECUTE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 605
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "com.samsung.dcm"

    const-string v4, "com.samsung.dcm.framework.FrameworkService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 607
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 608
    .local v0, "actionData":Landroid/os/Bundle;
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->StartOsc:Lcom/samsung/dcm/framework/IntentHandler$ValidAction;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidAction;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 612
    .local v1, "extrasBundle":Landroid/os/Bundle;
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Origin:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->DcmController:Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$ValidOrigins;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Operation:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->PerformAction:Lcom/samsung/dcm/framework/IntentHandler$OperationType;

    invoke-virtual {v4}, Lcom/samsung/dcm/framework/IntentHandler$OperationType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    sget-object v3, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->Data:Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidExtras;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 618
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 620
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 622
    .end local v0    # "actionData":Landroid/os/Bundle;
    .end local v1    # "extrasBundle":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v4, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->TAG:Ljava/lang/String;

    const-string v5, "postOscStartIntent(), reset : "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x1

    const-string v7, " NOT SENT"

    aput-object v7, v6, v3

    invoke-static {v4, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 631
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->mLastProcessedImageID:Ljava/lang/Integer;

    .line 632
    invoke-super {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->reset()V

    .line 633
    return-void
.end method

.method public setHeavyLimit(I)V
    .locals 0
    .param p1, "maxValue"    # I

    .prologue
    .line 680
    sput p1, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->m_Current_MAX_OSC_COUNT:I

    .line 681
    return-void
.end method

.method public updateBundle(Ljava/lang/Class;Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "mediadoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p3, "lucenedoc"    # Lorg/apache/lucene/document/Document;
    .param p4, "indata"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            "Lorg/apache/lucene/document/Document;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 421
    .local p1, "importer":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    return-void
.end method

.method public updateDocument(Lcom/samsung/dcm/framework/extractors/Extractor;Lcom/samsung/dcm/documents/MediaDoc;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;J)V
    .locals 7
    .param p1, "plugin"    # Lcom/samsung/dcm/framework/extractors/Extractor;
    .param p2, "mediadoc"    # Lcom/samsung/dcm/documents/MediaDoc;
    .param p3, "lucenedoc"    # Lorg/apache/lucene/document/Document;
    .param p4, "outdata"    # Landroid/os/Bundle;
    .param p5, "useGenerationid"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/dcm/framework/exceptions/LuceneException;
        }
    .end annotation

    .prologue
    .line 426
    const-string v0, "Document cannot be null "

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    if-eqz p4, :cond_0

    .line 428
    if-eqz p3, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-wide v4, p5

    .line 429
    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/task/HeavyExtractionTask;->updateDocument(Lcom/samsung/dcm/framework/extractors/Extractor;Lorg/apache/lucene/document/Document;Landroid/os/Bundle;J)V

    .line 432
    :cond_0
    return-void
.end method
