.class public Lcom/samsung/dcm/framework/extractormanager/CommandFactory;
.super Ljava/lang/Object;
.source "CommandFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/dcm/framework/configuration/ExtractorsController;)V
    .locals 1
    .param p1, "extractors"    # Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "ExtractorsController must not be null!"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iput-object p1, p0, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->mExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    .line 44
    return-void
.end method

.method private createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 6
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "op"    # Lcom/samsung/dcm/framework/extractormanager/Operation;
    .param p5, "docType"    # Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
            ")",
            "Lcom/samsung/dcm/framework/extractormanager/Command;"
        }
    .end annotation

    .prologue
    .line 65
    .local p3, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .local p4, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    invoke-virtual {p4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    invoke-virtual {p4}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/dcm/framework/extractormanager/Command;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;I)V

    .line 68
    .local v0, "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :goto_0
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->TAG:Ljava/lang/String;

    const-string v2, "Creating command ("

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    const-string v5, ") for id ("

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    const/4 v4, 0x3

    const-string v5, ")"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    if-eqz p5, :cond_0

    .line 71
    invoke-virtual {v0, p5}, Lcom/samsung/dcm/framework/extractormanager/Command;->setDocType(Lcom/samsung/dcm/documents/MediaDoc$DocType;)V

    .line 73
    :cond_0
    if-eqz p3, :cond_1

    .line 74
    invoke-virtual {v0, p3}, Lcom/samsung/dcm/framework/extractormanager/Command;->setExtractors(Ljava/util/Set;)V

    .line 77
    :cond_1
    return-object v0

    .line 65
    .end local v0    # "cmd":Lcom/samsung/dcm/framework/extractormanager/Command;
    :cond_2
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Command;

    invoke-direct {v0, p1, p2}, Lcom/samsung/dcm/framework/extractormanager/Command;-><init>(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;)V

    goto :goto_0
.end method


# virtual methods
.method public createDeleteCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 6
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/samsung/dcm/framework/extractormanager/Command;"
        }
    .end annotation

    .prologue
    .local p3, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 225
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p2, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, v3

    .line 228
    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 225
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createDeleteCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Ljava/lang/Iterable;
    .locals 1
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createDeleteCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public createDeleteCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;
    .locals 4
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 302
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    .local p3, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 304
    .local v0, "commandList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .line 305
    .local v2, "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    invoke-virtual {p0, v2, p2, p3}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createDeleteCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 308
    .end local v2    # "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    :cond_0
    return-object v0
.end method

.method public createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 7
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    .prologue
    .line 94
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p2, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 97
    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v5

    .line 99
    .local v5, "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    invoke-static {v5}, Lcom/samsung/commons/Algorithms;->getDocClassByDocType(Lcom/samsung/dcm/documents/MediaDoc$DocType;)Ljava/lang/Class;

    move-result-object v6

    .line 100
    .local v6, "doc":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->mExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    invoke-virtual {v0, v6}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v3

    .line 102
    .local v3, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 94
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    .end local v3    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .end local v5    # "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .end local v6    # "doc":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;I)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 7
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .param p3, "startId"    # I

    .prologue
    .line 119
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p2, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 122
    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v5

    .line 124
    .local v5, "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    invoke-static {v5}, Lcom/samsung/commons/Algorithms;->getDocClassByDocType(Lcom/samsung/dcm/documents/MediaDoc$DocType;)Ljava/lang/Class;

    move-result-object v6

    .line 125
    .local v6, "doc":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->mExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    invoke-virtual {v0, v6}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v3

    .line 127
    .local v3, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 119
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    .end local v3    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .end local v5    # "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .end local v6    # "doc":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 7
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/samsung/dcm/framework/extractormanager/Command;"
        }
    .end annotation

    .prologue
    .line 145
    .local p3, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p2, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 148
    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v5

    .line 150
    .local v5, "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    invoke-static {v5}, Lcom/samsung/commons/Algorithms;->getDocClassByDocType(Lcom/samsung/dcm/documents/MediaDoc$DocType;)Ljava/lang/Class;

    move-result-object v6

    .line 151
    .local v6, "doc":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->mExtractorsController:Lcom/samsung/dcm/framework/configuration/ExtractorsController;

    invoke-virtual {v0, v6}, Lcom/samsung/dcm/framework/configuration/ExtractorsController;->getExtractorsForClass(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v3

    .local v3, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    .line 153
    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 145
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    .end local v3    # "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .end local v5    # "docType":Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .end local v6    # "doc":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/dcm/documents/MediaDoc;>;"
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createInsertCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Ljava/lang/Iterable;
    .locals 1
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createInsertCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public createInsertCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;
    .locals 4
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    .local p3, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .local v0, "commandList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .line 265
    .local v2, "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    invoke-virtual {p0, v2, p2, p3}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createInsertCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 268
    .end local v2    # "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    :cond_0
    return-object v0
.end method

.method public createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 6
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/samsung/dcm/framework/extractormanager/Command;"
        }
    .end annotation

    .prologue
    .local p3, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 202
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p2, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, v3

    .line 205
    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 202
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Ljava/util/Set;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 6
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/samsung/dcm/framework/extractormanager/Command;"
        }
    .end annotation

    .prologue
    .line 173
    .local p3, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .local p4, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p2, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 176
    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 173
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Lcom/samsung/dcm/framework/extractormanager/Command;
    .locals 6
    .param p1, "id"    # Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    .param p3, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            ")",
            "Lcom/samsung/dcm/framework/extractormanager/Command;"
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne p3, v0, :cond_0

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 186
    .local v2, "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :goto_0
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation;Ljava/util/Set;Lcom/google/common/base/Optional;Lcom/samsung/dcm/documents/MediaDoc$DocType;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v0

    return-object v0

    .line 183
    .end local v2    # "op":Lcom/samsung/dcm/framework/extractormanager/Operation;
    :cond_0
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    goto :goto_0
.end method

.method public createUpdateCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;)Ljava/lang/Iterable;
    .locals 1
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public createUpdateCommands(Ljava/lang/Iterable;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;
    .locals 4
    .param p2, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    .local p3, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v0, "commandList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .line 344
    .local v2, "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    invoke-virtual {p0, v2, p2, p3}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347
    .end local v2    # "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    :cond_0
    return-object v0
.end method

.method public createUpdateCommands(Ljava/lang/Iterable;Ljava/util/Set;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/google/common/base/Optional;)Ljava/lang/Iterable;
    .locals 4
    .param p3, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/DCM_ID;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/framework/extractors/Extractor;",
            ">;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    .local p1, "ids":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/samsung/dcm/framework/extractormanager/DCM_ID;>;"
    .local p2, "extractors":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/framework/extractors/Extractor;>;"
    .local p4, "startId":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 368
    .local v0, "commandList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/dcm/framework/extractormanager/Command;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/framework/extractormanager/DCM_ID;

    .line 369
    .local v2, "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    invoke-virtual {p0, v2, p3, p2, p4}, Lcom/samsung/dcm/framework/extractormanager/CommandFactory;->createUpdateCommand(Lcom/samsung/dcm/framework/extractormanager/DCM_ID;Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;Ljava/util/Set;Lcom/google/common/base/Optional;)Lcom/samsung/dcm/framework/extractormanager/Command;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 372
    .end local v2    # "id":Lcom/samsung/dcm/framework/extractormanager/DCM_ID;
    :cond_0
    return-object v0
.end method
