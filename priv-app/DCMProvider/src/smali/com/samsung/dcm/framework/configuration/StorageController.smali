.class public Lcom/samsung/dcm/framework/configuration/StorageController;
.super Ljava/lang/Object;
.source "StorageController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mStorageController:Lcom/samsung/dcm/framework/configuration/StorageController;


# instance fields
.field private final INTERNAL_SYSTEM_MEDIA:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mGlobalReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

.field private final mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;",
            ">;"
        }
    .end annotation
.end field

.field protected mbStartSDCardRebuild:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 74
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mGlobalReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 76
    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/media"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->INTERNAL_SYSTEM_MEDIA:Ljava/lang/String;

    .line 79
    iput-boolean v4, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mbStartSDCardRebuild:Z

    .line 80
    new-instance v0, Lcom/samsung/dcm/framework/configuration/StorageController$1;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/framework/configuration/StorageController$1;-><init>(Lcom/samsung/dcm/framework/configuration/StorageController;)V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    .line 156
    const-string v0, "content cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;

    .line 157
    const-string v0, "SystemBroadcastReceiver cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mGlobalReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .line 159
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;

    const-string v1, "storage"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 160
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 162
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/StorageController;->updateVolumeInformation()V

    .line 165
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mGlobalReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mReceiver:Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    sget-object v3, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_MOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_UNMOUNTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_STARTED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;->EVENT_MEDIA_SCAN_FINISHED:Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;->registerListener(Lcom/samsung/dcm/framework/broadcastreceivers/IBroadcastListner;[Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver$Event_id;)V

    .line 170
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/dcm/framework/configuration/StorageController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/StorageController;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/dcm/framework/configuration/StorageController;->updateVolumeInformation()V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/dcm/framework/configuration/StorageController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/StorageController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->INTERNAL_SYSTEM_MEDIA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/dcm/framework/configuration/StorageController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/dcm/framework/configuration/StorageController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized getInstController()Lcom/samsung/dcm/framework/configuration/StorageController;
    .locals 3

    .prologue
    .line 222
    const-class v1, Lcom/samsung/dcm/framework/configuration/StorageController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageController:Lcom/samsung/dcm/framework/configuration/StorageController;

    const-string v2, "Not yet intialized"

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/StorageController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "receiver"    # Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;

    .prologue
    .line 213
    const-class v1, Lcom/samsung/dcm/framework/configuration/StorageController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageController:Lcom/samsung/dcm/framework/configuration/StorageController;

    if-nez v0, :cond_0

    .line 214
    new-instance v0, Lcom/samsung/dcm/framework/configuration/StorageController;

    invoke-direct {v0, p0, p1}, Lcom/samsung/dcm/framework/configuration/StorageController;-><init>(Landroid/content/Context;Lcom/samsung/dcm/framework/broadcastreceivers/SystemBroadcastReceiver;)V

    sput-object v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageController:Lcom/samsung/dcm/framework/configuration/StorageController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    :cond_0
    monitor-exit v1

    return-void

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized updateVolumeInformation()V
    .locals 20

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageManager:Landroid/os/storage/StorageManager;

    if-eqz v15, :cond_2

    .line 174
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v15}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v13

    .line 175
    .local v13, "storageVolumes":[Landroid/os/storage/StorageVolume;
    if-eqz v13, :cond_2

    .line 176
    move-object v1, v13

    .local v1, "arr$":[Landroid/os/storage/StorageVolume;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_2

    aget-object v12, v1, v5

    .line 177
    .local v12, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getStorageId()I

    move-result v11

    .line 178
    .local v11, "storageId":I
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getFatVolumeId()I

    move-result v4

    .line 179
    .local v4, "fatVolumeId":I
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    move-result v8

    .line 181
    .local v8, "removable":Z
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 182
    .local v7, "path":Ljava/lang/String;
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getState()Ljava/lang/String;

    move-result-object v10

    .line 183
    .local v10, "state1":Ljava/lang/String;
    const-string v15, "StorageController"

    const-string v16, " state through storage volume = "

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v10, v17, v18

    invoke-static/range {v15 .. v17}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v14

    .line 186
    .local v14, "subsystem":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v15, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 187
    .local v9, "state":Ljava/lang/String;
    const-string v15, "mounted"

    invoke-virtual {v9, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v2, 0x1

    .line 189
    .local v2, "bMounted":Z
    :goto_1
    const-string v15, "StorageController"

    const-string v16, " storageId = "

    const/16 v17, 0x9

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, " removable = "

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x3

    const-string v19, " path = "

    aput-object v19, v17, v18

    const/16 v18, 0x4

    aput-object v7, v17, v18

    const/16 v18, 0x5

    const-string v19, " state = "

    aput-object v19, v17, v18

    const/16 v18, 0x6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageManager:Landroid/os/storage/StorageManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x7

    const-string v19, " subsystem = "

    aput-object v19, v17, v18

    const/16 v18, 0x8

    aput-object v14, v17, v18

    invoke-static/range {v15 .. v17}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    .line 196
    .local v3, "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    if-nez v3, :cond_1

    .line 197
    new-instance v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    .end local v3    # "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    invoke-direct {v3, v7, v2, v14, v4}, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;-><init>(Ljava/lang/String;ZLjava/lang/String;I)V

    .line 203
    .restart local v3    # "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 187
    .end local v2    # "bMounted":Z
    .end local v3    # "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 200
    .restart local v2    # "bMounted":Z
    .restart local v3    # "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    :cond_1
    iput-boolean v2, v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbMounted:Z

    .line 201
    iput v4, v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mFatVolumeId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 173
    .end local v1    # "arr$":[Landroid/os/storage/StorageVolume;
    .end local v2    # "bMounted":Z
    .end local v3    # "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    .end local v4    # "fatVolumeId":I
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "removable":Z
    .end local v9    # "state":Ljava/lang/String;
    .end local v10    # "state1":Ljava/lang/String;
    .end local v11    # "storageId":I
    .end local v12    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v13    # "storageVolumes":[Landroid/os/storage/StorageVolume;
    .end local v14    # "subsystem":Ljava/lang/String;
    :catchall_0
    move-exception v15

    monitor-exit p0

    throw v15

    .line 208
    :cond_2
    :try_start_1
    const-string v15, "StorageController"

    const-string v16, " mounted = "

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/configuration/StorageController;->getMountedVolumeList()Ljava/util/List;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v15 .. v17}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public getFatVolumeId(I)I
    .locals 5
    .param p1, "storageId"    # I

    .prologue
    .line 359
    const/4 v0, -0x1

    .line 360
    .local v0, "fatVolumeId":I
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    .line 361
    .local v3, "infos":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 362
    .local v2, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 363
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget v0, v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mFatVolumeId:I

    .line 367
    .end local v2    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :cond_1
    return v0
.end method

.method public getMountedVolumeList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v3, "status":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 279
    .local v2, "infos":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 280
    .local v1, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-boolean v4, v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbMounted:Z

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-boolean v4, v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbIsReady:Z

    if-eqz v4, :cond_0

    .line 281
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 284
    .end local v1    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    return-object v4
.end method

.method protected getSDCardSubSystemEntry()Ljava/util/Map$Entry;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 346
    sget-object v3, Lcom/samsung/dcm/framework/configuration/StorageController;->TAG:Ljava/lang/String;

    const-string v4, "getSDCardSubSystemEntry"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "Rebooted ?"

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    iput-boolean v7, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mbStartSDCardRebuild:Z

    .line 349
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 350
    .local v2, "infos":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 351
    .local v1, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-object v3, v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mSubsystem:Ljava/lang/String;

    const-string v4, "sd"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-boolean v3, v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbMounted:Z

    if-eqz v3, :cond_0

    .line 355
    .end local v1    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStorageId(Ljava/lang/String;)I
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 322
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 323
    .local v2, "infos":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 324
    .local v1, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-object v3, v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mPath:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 325
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 328
    .end local v1    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :goto_0
    return v3

    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public getSubSystemEntry(Ljava/lang/String;)Ljava/util/Map$Entry;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    iget-object v3, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 333
    .local v2, "infos":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 334
    .local v1, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-object v3, v3, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mPath:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 338
    .end local v1    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUnmountedVolumeList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v3, "status":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 293
    .local v2, "infos":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 294
    .local v1, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-boolean v4, v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbMounted:Z

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    iget-boolean v4, v4, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbIsReady:Z

    if-nez v4, :cond_0

    .line 295
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    .end local v1    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;>;"
    :cond_2
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    return-object v4
.end method

.method public getVolumeStatus(I)Z
    .locals 8
    .param p1, "storageId"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 258
    const/4 v1, 0x0

    .line 259
    .local v1, "status":Z
    iget-object v2, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    .line 260
    .local v0, "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    if-nez v0, :cond_0

    .line 261
    const-string v2, "StorageController"

    const-string v3, "No volume with that storageid "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    const/4 v1, 0x1

    .line 270
    :goto_0
    return v1

    .line 267
    :cond_0
    iget-boolean v1, v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbIsReady:Z

    .line 268
    const-string v2, "StorageController"

    const-string v3, "getVolumeStatus storageId "

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, "status "

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getVolumeType(I)Ljava/lang/String;
    .locals 3
    .param p1, "storageId"    # I

    .prologue
    .line 310
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    .line 311
    .local v0, "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mSubsystem:Ljava/lang/String;

    goto :goto_0
.end method

.method public isVolumeMounted(I)Z
    .locals 3
    .param p1, "storageId"    # I

    .prologue
    .line 232
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    .line 233
    .local v0, "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-boolean v1, v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbMounted:Z

    goto :goto_0
.end method

.method public setVolumeStatus(IZ)V
    .locals 6
    .param p1, "storageId"    # I
    .param p2, "status"    # Z

    .prologue
    .line 244
    iget-object v1, p0, Lcom/samsung/dcm/framework/configuration/StorageController;->mStorageMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;

    .line 245
    .local v0, "data":Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;
    if-eqz v0, :cond_0

    .line 246
    iput-boolean p2, v0, Lcom/samsung/dcm/framework/configuration/StorageController$StorageInformation;->mbIsReady:Z

    .line 247
    const-string v1, "StorageController"

    const-string v2, "setVolumeStatus storageId "

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "status "

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    :cond_0
    return-void
.end method
