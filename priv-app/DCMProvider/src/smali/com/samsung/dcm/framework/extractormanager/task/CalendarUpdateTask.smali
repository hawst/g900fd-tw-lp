.class public Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;
.super Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;
.source "CalendarUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;,
        Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;
    }
.end annotation


# static fields
.field private static final BASE_CALLBACK_STRING:Ljava/lang/String; = "intentcallback"

.field private static final CALENDAR_DELIMIER:Ljava/lang/String; = ";"

.field public static final CALENDAR_EVENT_DELETE:I = 0x0

.field public static final CALENDAR_EVENT_INSERT:I = 0x1

.field public static final CALENDAR_EVENT_NAME_CHANGED:I = 0x3

.field public static final CALENDAR_EVENT_TIMING_CHANGED:I = 0x2

.field private static final INSERT_PENDING:Ljava/lang/String; = "INSERT_PENDING"

.field private static final MAX_EVENTS_SUPPORTED:I = 0x19

.field private static final TAG:Ljava/lang/String;

.field private static auxDoc:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;",
            ">;"
        }
    .end annotation
.end field

.field private mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mDocumentList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field mEventHashList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mExistingcategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mFacetCateogries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;"
        }
    .end annotation
.end field

.field mLuceneDocList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

.field mNewEvents:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNewIntentsReceived:I

.field mUniqueFinalCatogories:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$1;

    invoke-direct {v0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->auxDoc:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;-><init>()V

    .line 76
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 79
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 99
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    .line 101
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;

    .line 105
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    .line 114
    iput-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewEvents:Ljava/util/Set;

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mExistingcategories:Ljava/util/ArrayList;

    .line 116
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    .line 117
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mUniqueFinalCatogories:Ljava/util/HashSet;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mFacetCateogries:Ljava/util/ArrayList;

    .line 193
    const-class v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-static {v0}, Lcom/samsung/dcm/framework/configuration/DCMServiceLocator;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/configuration/DCMConfig;

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    .line 194
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    const-string v1, "CalendarHeavyTask needs DCMConfig"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mAppContext:Landroid/content/Context;

    .line 196
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 197
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConfig:Lcom/samsung/dcm/framework/configuration/DCMConfig;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DCMConfig;->getTransport()Lcom/samsung/dcm/framework/configuration/DcmTransport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/configuration/DcmTransport;->getNRTManager()Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    .line 198
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    .line 199
    return-void
.end method

.method private addToPendingQueue(Lorg/apache/lucene/document/Document;JLjava/lang/String;ILcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;)V
    .locals 6
    .param p1, "currentDoc"    # Lorg/apache/lucene/document/Document;
    .param p2, "searchid"    # J
    .param p4, "uri_id"    # Ljava/lang/String;
    .param p5, "operation"    # I
    .param p6, "calendarconext"    # Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;

    .prologue
    const/4 v3, 0x1

    .line 727
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashMap;

    .line 729
    .local v0, "olddata":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 730
    new-instance v0, Ljava/util/LinkedHashMap;

    .end local v0    # "olddata":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 734
    .restart local v0    # "olddata":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    packed-switch p5, :pswitch_data_0

    .line 745
    :goto_0
    iget v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p4}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 746
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v2, "Only one intent received"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, " direct update"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 747
    const-string v1, "INSERT_PENDING"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 748
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->applyPendingEventsToDoc(Lorg/apache/lucene/document/Document;Ljava/util/LinkedHashMap;J)Z

    .line 753
    :goto_1
    return-void

    .line 736
    :pswitch_0
    invoke-direct {p0, p4, p6, v0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->updateBundleForInsert(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;Ljava/util/LinkedHashMap;)Z

    goto :goto_0

    .line 739
    :pswitch_1
    invoke-direct {p0, p4, p6, v0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->updateBundleForDelete(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;Ljava/util/LinkedHashMap;)Z

    goto :goto_0

    .line 751
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 734
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private applyDeleteToExistingQueue(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;)V
    .locals 9
    .param p1, "calendarconext"    # Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;

    .prologue
    .line 681
    iget-object v5, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 684
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    const/4 v1, 0x0

    .line 685
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v3, 0x0

    .line 686
    .local v3, "old_value":Ljava/lang/String;
    const/4 v0, 0x0

    .line 687
    .local v0, "doc_data":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 688
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    check-cast v1, Ljava/util/Map$Entry;

    .line 689
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v1, :cond_0

    .line 691
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "doc_data":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    check-cast v0, Ljava/util/LinkedHashMap;

    .line 692
    .restart local v0    # "doc_data":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 693
    .local v4, "oldcount":I
    const-string v5, "INSERT_PENDING"

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 694
    const-string v5, "INSERT_PENDING"

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 698
    :cond_1
    iget-object v5, p1, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "old_value":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 700
    .restart local v3    # "old_value":Ljava/lang/String;
    if-eqz v3, :cond_2

    const-string v5, "I;"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 701
    sget-object v5, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v6, "applyDeleteToExistingQueue removed = "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-static {v5, v6, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 704
    const-string v5, "INSERT_PENDING"

    add-int/lit8 v6, v4, -0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    :cond_2
    const/4 v1, 0x0

    .line 707
    const/4 v0, 0x0

    .line 708
    const/4 v3, 0x0

    .line 709
    goto :goto_0

    .line 711
    .end local v4    # "oldcount":I
    :cond_3
    return-void
.end method

.method private applyPendingEventsToDoc(Lorg/apache/lucene/document/Document;Ljava/util/LinkedHashMap;J)Z
    .locals 27
    .param p1, "currentDoc"    # Lorg/apache/lucene/document/Document;
    .param p3, "searchid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)Z"
        }
    .end annotation

    .prologue
    .line 360
    .local p2, "pendingevents":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v25, 0x0

    .line 362
    .local v25, "reloadIndexSearcher":Z
    invoke-virtual/range {p2 .. p2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewEvents:Ljava/util/Set;

    .line 364
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewEvents:Ljava/util/Set;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewEvents:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 365
    :cond_0
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "applyPendingEventsToDoc "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "no newEvents"

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move/from16 v26, v25

    .line 482
    .end local v25    # "reloadIndexSearcher":Z
    .local v26, "reloadIndexSearcher":I
    :goto_0
    return v26

    .line 369
    .end local v26    # "reloadIndexSearcher":I
    .restart local v25    # "reloadIndexSearcher":Z
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->cleanall()V

    .line 371
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/document/Document;

    .line 372
    .local v4, "newdoc":Lorg/apache/lucene/document/Document;
    invoke-static {v4}, Lcom/samsung/dcm/framework/indexer/utils/DocumentsUtils;->cleanDocumentContent(Lorg/apache/lucene/document/Document;)V

    .line 374
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mExistingcategories:Ljava/util/ArrayList;

    const/4 v5, 0x1

    new-array v5, v5, [Z

    const/4 v6, 0x0

    const/4 v7, 0x1

    aput-boolean v7, v5, v6

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4, v5}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->getCopyOfCurrentDoc(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;[Z)V

    .line 377
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "applyPendingEventsToDoc URI = "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "uri"

    invoke-virtual {v4, v8}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 380
    const-string v3, "event_uri"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v24

    .line 382
    .local v24, "oldevnets":[Lorg/apache/lucene/index/IndexableField;
    const-string v3, "calendar_event"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 383
    const-string v3, "event_uri"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 385
    move-object/from16 v2, v24

    .local v2, "arr$":[Lorg/apache/lucene/index/IndexableField;
    array-length v0, v2

    move/from16 v20, v0

    .local v20, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    aget-object v11, v2, v16

    .line 386
    .local v11, "event":Lorg/apache/lucene/index/IndexableField;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    invoke-interface {v11}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 385
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 389
    .end local v11    # "event":Lorg/apache/lucene/index/IndexableField;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewEvents:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .end local v16    # "i$":I
    :cond_3
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 390
    .local v11, "event":Ljava/lang/String;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "newEvents Uri = "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    const/4 v7, 0x1

    const-string v8, " data = "

    aput-object v8, v6, v7

    const/4 v7, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v5, ";"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 392
    .local v13, "event_data":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v13, v3

    const-string v5, "D"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 394
    .local v21, "oldEvent":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 400
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v21    # "oldEvent":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x0

    aget-object v3, v13, v3

    const-string v5, "I"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 401
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v5, 0x1

    aget-object v5, v13, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 402
    .local v14, "event_uri":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .restart local v17    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 403
    .restart local v21    # "oldEvent":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 404
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 408
    .end local v21    # "oldEvent":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    invoke-virtual {v3, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 412
    .end local v11    # "event":Ljava/lang/String;
    .end local v13    # "event_data":[Ljava/lang/String;
    .end local v14    # "event_uri":Ljava/lang/String;
    .end local v17    # "i$":Ljava/util/Iterator;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mExistingcategories:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 414
    .local v19, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_9
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 415
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v5, "root_calendar_event"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 416
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 420
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mUniqueFinalCatogories:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mExistingcategories:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 422
    const/4 v12, 0x0

    .line 424
    .local v12, "event_count":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 425
    .local v15, "fulluri":Ljava/lang/String;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "applyPendingEventsToDoc URI = "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v15, v6, v7

    invoke-static {v3, v5, v6}, Lcom/samsung/commons/Log;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    const-string v3, " "

    invoke-virtual {v15, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    .line 427
    .local v18, "indexof":I
    if-lez v18, :cond_c

    .line 429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mUniqueFinalCatogories:Ljava/util/HashSet;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "root_calendar_event;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v18, 0x1

    invoke-virtual {v15, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 433
    new-instance v3, Lorg/apache/lucene/document/StringField;

    const-string v5, "calendar_event"

    add-int/lit8 v6, v18, 0x1

    invoke-virtual {v15, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v3, v5, v6, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 436
    new-instance v3, Lorg/apache/lucene/document/TextField;

    const-string v5, "event_uri"

    sget-object v6, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v3, v5, v15, v6}, Lorg/apache/lucene/document/TextField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 439
    add-int/lit8 v12, v12, 0x1

    .line 442
    :cond_c
    const/16 v3, 0x19

    if-ne v12, v3, :cond_b

    .line 443
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "Events supported = "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/16 v8, 0x19

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 448
    .end local v15    # "fulluri":Ljava/lang/String;
    .end local v18    # "indexof":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mUniqueFinalCatogories:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mFacetCateogries:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v4}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->updateCategorgyForFacet(Ljava/util/HashSet;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V

    .line 451
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mFacetCateogries:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->update(Lorg/apache/lucene/document/Document;Ljava/util/ArrayList;JLjava/lang/Boolean;)J
    :try_end_0
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v22

    .line 452
    .local v22, "newid":J
    cmp-long v3, v22, p3

    if-eqz v3, :cond_f

    .line 453
    const/4 v9, 0x0

    .line 454
    .local v9, "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    const/16 v25, 0x1

    .line 457
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 458
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v3}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v9

    .line 459
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 460
    const-string v3, "uri"

    invoke-virtual {v4, v3}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    invoke-static {v9, v3, v5}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V

    .line 464
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    .line 465
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/document/Document;

    const-wide v6, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1, v6, v7}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->applyPendingEventsToDoc(Lorg/apache/lucene/document/Document;Ljava/util/LinkedHashMap;J)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 473
    :cond_e
    if-eqz v9, :cond_f

    .line 474
    :try_start_2
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V
    :try_end_2
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_2 .. :try_end_2} :catch_1

    .end local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v22    # "newid":J
    :cond_f
    :goto_4
    move/from16 v26, v25

    .line 482
    .restart local v26    # "reloadIndexSearcher":I
    goto/16 :goto_0

    .line 470
    .end local v26    # "reloadIndexSearcher":I
    .restart local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v22    # "newid":J
    :catch_0
    move-exception v10

    .line 471
    .local v10, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "Calendar race update "

    invoke-static {v3, v5, v10}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 473
    if-eqz v9, :cond_f

    .line 474
    :try_start_4
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V
    :try_end_4
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    .line 478
    .end local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v22    # "newid":J
    :catch_1
    move-exception v10

    .line 479
    .local v10, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v5, "Calendar update "

    invoke-static {v3, v5, v10}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 473
    .end local v10    # "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    .restart local v9    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v22    # "newid":J
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_10

    .line 474
    :try_start_5
    invoke-virtual {v9}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_10
    throw v3
    :try_end_5
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_5 .. :try_end_5} :catch_1
.end method

.method private static buildQueryForCalendarInsert(IJJZLandroid/net/Uri;)Lorg/apache/lucene/search/Query;
    .locals 9
    .param p0, "currentId"    # I
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "checkEventUri"    # Z
    .param p6, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 850
    new-instance v3, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 853
    .local v3, "query":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz p5, :cond_0

    .line 854
    new-instance v2, Lorg/apache/lucene/search/TermQuery;

    new-instance v4, Lorg/apache/lucene/index/Term;

    const-string v5, "event_uri"

    invoke-virtual {p6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 856
    .local v2, "atomicQueryUri":Lorg/apache/lucene/search/Query;
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 859
    .end local v2    # "atomicQueryUri":Lorg/apache/lucene/search/Query;
    :cond_0
    const-string v4, "date"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v4, v5, v6, v7, v7}, Lorg/apache/lucene/search/NumericRangeQuery;->newLongRange(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v0

    .line 861
    .local v0, "atomicQueryDate":Lorg/apache/lucene/search/NumericRangeQuery;, "Lorg/apache/lucene/search/NumericRangeQuery<Ljava/lang/Long;>;"
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v0, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 863
    const-string v4, "deleted"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4, v5, v6, v7, v7}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    .line 865
    .local v1, "atomicQueryDeleted":Lorg/apache/lucene/search/Query;
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v3, v1, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 872
    return-object v3
.end method

.method private static buildQueryForCalendarUpdate(ILandroid/net/Uri;)Lorg/apache/lucene/search/BooleanQuery;
    .locals 7
    .param p0, "currentId"    # I
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 817
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 822
    .local v0, "query":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    new-instance v2, Lorg/apache/lucene/index/Term;

    const-string v3, "event_uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 826
    const-string v1, "deleted"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3, v6, v6}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 835
    return-object v0
.end method

.method private cleanall()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mExistingcategories:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 487
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mEventHashList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 488
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mUniqueFinalCatogories:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 489
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mFacetCateogries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 490
    return-void
.end method

.method private updateBundleForDelete(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;Ljava/util/LinkedHashMap;)Z
    .locals 3
    .param p1, "uri_id"    # Ljava/lang/String;
    .param p2, "calendarconext"    # Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 759
    .local p3, "olddata":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "D;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    const/4 v0, 0x1

    return v0
.end method

.method private updateBundleForInsert(Ljava/lang/String;Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;Ljava/util/LinkedHashMap;)Z
    .locals 6
    .param p1, "uri_id"    # Ljava/lang/String;
    .param p2, "calendarconext"    # Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "olddata":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 767
    const/4 v0, 0x0

    .line 768
    .local v0, "oldcount":I
    const-string v1, "INSERT_PENDING"

    invoke-virtual {p3, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 769
    const-string v1, "INSERT_PENDING"

    invoke-virtual {p3, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 772
    :cond_0
    const/16 v1, 0x19

    if-lt v0, v1, :cond_1

    .line 773
    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v4, "updateBundleForInsert limit for insert =  "

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-static {v1, v4, v3}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 781
    :goto_0
    return v1

    .line 777
    :cond_1
    iget-object v1, p2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "I;"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->title:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    const-string v1, "INSERT_PENDING"

    add-int/lit8 v2, v0, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v3

    .line 781
    goto :goto_0
.end method

.method private updateCategorgyForFacet(Ljava/util/HashSet;Ljava/util/ArrayList;Lorg/apache/lucene/document/Document;)V
    .locals 4
    .param p3, "newDoc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;",
            "Lorg/apache/lucene/document/Document;",
            ")V"
        }
    .end annotation

    .prologue
    .line 501
    .local p1, "existingCategories":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .local p2, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 502
    const-string v2, "categories"

    invoke-virtual {p3, v2}, Lorg/apache/lucene/document/Document;->removeFields(Ljava/lang/String;)V

    .line 503
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 504
    .local v0, "category":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 506
    new-instance v2, Lorg/apache/lucene/document/StoredField;

    const-string v3, "categories"

    invoke-direct {v2, v3, v0}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 510
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method private updateDocumentList(Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 46
    .param p1, "eventUri"    # Landroid/net/Uri;
    .param p2, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 514
    const-string v13, ""

    .line 515
    .local v13, "title":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 516
    .local v8, "startTime":J
    const-wide/16 v10, 0x0

    .line 519
    .local v10, "endTime":J
    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v35

    .line 520
    .local v35, "operation":I
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v3, "Processing event: "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p1, v7, v12

    const/4 v12, 0x1

    const-string v17, ", operation="

    aput-object v17, v7, v12

    const/4 v12, 0x2

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v7, v12

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 523
    if-eqz v35, :cond_1

    .line 524
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v30

    .line 526
    .local v30, "id":J
    const/16 v27, 0x0

    .line 528
    .local v27, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "title"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "dtstart"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "dtend"

    aput-object v3, v4, v2

    .line 533
    .local v4, "PROJECTION":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v30

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 536
    .local v5, "SELECTION":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 538
    if-eqz v27, :cond_5

    .line 539
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v3, "Processing Event; Cursor size: "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->getCount()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v7, v12

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 540
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 541
    const/4 v2, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    .line 542
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 543
    const/4 v2, 0x2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v10

    .line 550
    :cond_0
    if-eqz v27, :cond_1

    .line 551
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 556
    .end local v4    # "PROJECTION":[Ljava/lang/String;
    .end local v5    # "SELECTION":Ljava/lang/String;
    .end local v27    # "cursor":Landroid/database/Cursor;
    .end local v30    # "id":J
    :cond_1
    new-instance v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;

    const/4 v7, 0x0

    move-object/from16 v12, p1

    invoke-direct/range {v6 .. v13}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;-><init>(IJJLandroid/net/Uri;Ljava/lang/String;)V

    .line 563
    .local v6, "calendarconext":Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v41

    .line 564
    .local v41, "time":Ljava/util/Calendar;
    invoke-virtual/range {v41 .. v41}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual/range {v41 .. v41}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v44

    move-wide/from16 v0, v44

    invoke-virtual {v2, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v36, v0

    .line 565
    .local v36, "offset":J
    iget-wide v2, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    sub-long v2, v2, v36

    iput-wide v2, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    .line 566
    iget-wide v2, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    sub-long v2, v2, v36

    iput-wide v2, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    .line 568
    new-instance v34, Ljava/util/LinkedHashMap;

    invoke-direct/range {v34 .. v34}, Ljava/util/LinkedHashMap;-><init>()V

    .line 570
    .local v34, "luceneQueryList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    packed-switch v35, :pswitch_data_0

    .line 615
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unhandled calendar event "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    :cond_2
    :goto_0
    const/16 v42, 0x0

    .line 620
    .local v42, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/4 v15, 0x0

    .line 621
    .local v15, "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    const/16 v19, 0x0

    .line 623
    .local v19, "currentDoc":Lorg/apache/lucene/document/Document;
    const/4 v14, 0x0

    .line 624
    .local v14, "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    const-wide/16 v20, -0x1

    .line 626
    .local v20, "searchid":J
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    invoke-virtual {v2}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v14

    .line 627
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v20

    .line 628
    invoke-virtual/range {v34 .. v34}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .line 630
    .local v39, "queryiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;>;"
    :goto_1
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 631
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/util/Map$Entry;

    .line 633
    .local v38, "query_operation":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    invoke-interface/range {v38 .. v38}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/search/Query;

    .line 634
    .local v16, "query":Lorg/apache/lucene/search/Query;
    invoke-interface/range {v38 .. v38}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 636
    .local v23, "op":I
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v3, "operation ="

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v7, v12

    const/4 v12, 0x1

    const-string v17, " query = "

    aput-object v17, v7, v12

    const/4 v12, 0x2

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v7, v12

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 639
    if-nez v23, :cond_3

    .line 640
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->applyDeleteToExistingQueue(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    move-object/from16 v26, v19

    .line 644
    .end local v19    # "currentDoc":Lorg/apache/lucene/document/Document;
    .local v26, "currentDoc":Lorg/apache/lucene/document/Document;
    const/16 v17, 0x0

    const/16 v18, 0x3e8

    const/16 v19, 0x0

    :try_start_2
    invoke-virtual/range {v14 .. v19}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopDocs;

    move-result-object v42

    .line 647
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v3, "topDocs hits= "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, v42

    iget v0, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v7, v12

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 649
    const/4 v15, 0x0

    .line 650
    move-object/from16 v0, v42

    iget-object v0, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    move-object/from16 v25, v0

    .local v25, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v33, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .local v33, "len$":I
    const/16 v29, 0x0

    .local v29, "i$":I
    move-object/from16 v19, v26

    .end local v26    # "currentDoc":Lorg/apache/lucene/document/Document;
    .restart local v19    # "currentDoc":Lorg/apache/lucene/document/Document;
    :goto_2
    move/from16 v0, v29

    move/from16 v1, v33

    if-ge v0, v1, :cond_9

    :try_start_3
    aget-object v40, v25, v29

    .line 651
    .local v40, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    move-object/from16 v0, v40

    iget v2, v0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    const/4 v3, 0x0

    invoke-virtual {v14, v2, v3}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getDoc(ILjava/util/Set;)Lorg/apache/lucene/document/Document;

    move-result-object v19

    .line 652
    const-string v2, "uri"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/index/IndexableField;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v18, p0

    move-object/from16 v24, v6

    invoke-direct/range {v18 .. v24}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->addToPendingQueue(Lorg/apache/lucene/document/Document;JLjava/lang/String;ILcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;)V

    .line 658
    move-object/from16 v15, v40

    .line 660
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->isFinishedNow()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 661
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v3, "Exit is requested"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v17, " updateDocumentList"

    aput-object v17, v7, v12

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 674
    if-eqz v14, :cond_4

    .line 675
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 678
    .end local v16    # "query":Lorg/apache/lucene/search/Query;
    .end local v23    # "op":I
    .end local v25    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v29    # "i$":I
    .end local v33    # "len$":I
    .end local v38    # "query_operation":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    .end local v40    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_4
    :goto_3
    return-void

    .line 546
    .end local v6    # "calendarconext":Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;
    .end local v14    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .end local v15    # "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .end local v19    # "currentDoc":Lorg/apache/lucene/document/Document;
    .end local v20    # "searchid":J
    .end local v34    # "luceneQueryList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    .end local v36    # "offset":J
    .end local v39    # "queryiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;>;"
    .end local v41    # "time":Ljava/util/Calendar;
    .end local v42    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    .restart local v4    # "PROJECTION":[Ljava/lang/String;
    .restart local v5    # "SELECTION":Ljava/lang/String;
    .restart local v27    # "cursor":Landroid/database/Cursor;
    .restart local v30    # "id":J
    :cond_5
    :try_start_4
    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v3, "Query returned null; exiting."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v7}, Lcom/samsung/commons/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "No data in cursor"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 550
    :catchall_0
    move-exception v2

    if-eqz v27, :cond_6

    .line 551
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2

    .line 574
    .end local v4    # "PROJECTION":[Ljava/lang/String;
    .end local v5    # "SELECTION":Ljava/lang/String;
    .end local v27    # "cursor":Landroid/database/Cursor;
    .end local v30    # "id":J
    .restart local v6    # "calendarconext":Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;
    .restart local v34    # "luceneQueryList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    .restart local v36    # "offset":J
    .restart local v41    # "time":Ljava/util/Calendar;
    :pswitch_0
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->buildQueryForCalendarUpdate(ILandroid/net/Uri;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v28

    .line 575
    .local v28, "deleteQuery":Lorg/apache/lucene/search/Query;
    if-eqz v28, :cond_2

    .line 576
    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v34

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 582
    .end local v28    # "deleteQuery":Lorg/apache/lucene/search/Query;
    :pswitch_1
    iget v15, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->currentId:I

    iget-wide v0, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    move-wide/from16 v16, v0

    iget-wide v0, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    move-wide/from16 v18, v0

    const/16 v20, 0x0

    iget-object v0, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    move-object/from16 v21, v0

    invoke-static/range {v15 .. v21}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->buildQueryForCalendarInsert(IJJZLandroid/net/Uri;)Lorg/apache/lucene/search/Query;

    move-result-object v32

    .line 586
    .local v32, "insertQuery":Lorg/apache/lucene/search/Query;
    if-eqz v32, :cond_2

    .line 587
    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 596
    .end local v32    # "insertQuery":Lorg/apache/lucene/search/Query;
    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->buildQueryForCalendarUpdate(ILandroid/net/Uri;)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v28

    .line 597
    .local v28, "deleteQuery":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz v28, :cond_7

    .line 598
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v34

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    .line 605
    .end local v28    # "deleteQuery":Lorg/apache/lucene/search/BooleanQuery;
    :cond_7
    :pswitch_3
    iget v15, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->currentId:I

    iget-wide v0, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->startTime:J

    move-wide/from16 v16, v0

    iget-wide v0, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->endTime:J

    move-wide/from16 v18, v0

    const/16 v20, 0x0

    iget-object v0, v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$ProcessingContext;->uri:Landroid/net/Uri;

    move-object/from16 v21, v0

    invoke-static/range {v15 .. v21}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->buildQueryForCalendarInsert(IJJZLandroid/net/Uri;)Lorg/apache/lucene/search/Query;

    move-result-object v32

    .line 609
    .restart local v32    # "insertQuery":Lorg/apache/lucene/search/Query;
    if-eqz v32, :cond_2

    .line 610
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 650
    .end local v32    # "insertQuery":Lorg/apache/lucene/search/Query;
    .restart local v14    # "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    .restart local v15    # "lastscoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v16    # "query":Lorg/apache/lucene/search/Query;
    .restart local v19    # "currentDoc":Lorg/apache/lucene/document/Document;
    .restart local v20    # "searchid":J
    .restart local v23    # "op":I
    .restart local v25    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v29    # "i$":I
    .restart local v33    # "len$":I
    .restart local v38    # "query_operation":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    .restart local v39    # "queryiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;>;"
    .restart local v40    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v42    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_8
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_2

    .line 666
    .end local v40    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_9
    :try_start_5
    move-object/from16 v0, v42

    iget-object v2, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v2, v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_a

    .line 667
    const/4 v15, 0x0

    .line 670
    :cond_a
    if-nez v15, :cond_3

    goto/16 :goto_1

    .line 674
    .end local v16    # "query":Lorg/apache/lucene/search/Query;
    .end local v23    # "op":I
    .end local v25    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v29    # "i$":I
    .end local v33    # "len$":I
    .end local v38    # "query_operation":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    :cond_b
    if-eqz v14, :cond_4

    .line 675
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    goto/16 :goto_3

    .line 674
    .end local v39    # "queryiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;>;"
    :catchall_1
    move-exception v2

    :goto_4
    if-eqz v14, :cond_c

    .line 675
    invoke-virtual {v14}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    :cond_c
    throw v2

    .line 674
    .end local v19    # "currentDoc":Lorg/apache/lucene/document/Document;
    .restart local v16    # "query":Lorg/apache/lucene/search/Query;
    .restart local v23    # "op":I
    .restart local v26    # "currentDoc":Lorg/apache/lucene/document/Document;
    .restart local v38    # "query_operation":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    .restart local v39    # "queryiterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;>;"
    :catchall_2
    move-exception v2

    move-object/from16 v19, v26

    .end local v26    # "currentDoc":Lorg/apache/lucene/document/Document;
    .restart local v19    # "currentDoc":Lorg/apache/lucene/document/Document;
    goto :goto_4

    .line 570
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public addNewBundle(Landroid/os/Bundle;ILcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;)Z
    .locals 2
    .param p1, "newBundle"    # Landroid/os/Bundle;
    .param p2, "startId"    # I
    .param p3, "listener"    # Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;

    invoke-direct {v1, p1, p2}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;-><init>(Landroid/os/Bundle;I)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 215
    if-eqz p3, :cond_0

    .line 216
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    :cond_0
    iget v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    .line 218
    const/4 v0, 0x1

    return v0
.end method

.method protected afterInternalRun()V
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNewIntentsReceived:I

    .line 805
    invoke-direct {p0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->cleanall()V

    .line 806
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 807
    invoke-super {p0}, Lcom/samsung/dcm/framework/extractormanager/task/DCMTask;->afterInternalRun()V

    .line 808
    return-void
.end method

.method protected beforeInternalRun()V
    .locals 1

    .prologue
    .line 792
    const/16 v0, 0xf

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 793
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->auxDoc:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    .line 794
    return-void
.end method

.method protected internalRun()V
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 223
    sget-object v24, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->toString()Ljava/lang/String;

    move-result-object v23

    .line 226
    .local v23, "uriKey":Ljava/lang/String;
    :cond_0
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Number of Event URIs to process = ["

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    const/16 v27, 0x1

    const-string v28, "]"

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->isFinishedNow()Z

    move-result v24

    if-eqz v24, :cond_1

    .line 230
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Thread interrupted, exiting"

    const/16 v26, 0x0

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 356
    :goto_1
    return-void

    .line 234
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;

    .line 236
    .local v6, "calendarBundle":Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;
    if-eqz v6, :cond_9

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 238
    invoke-virtual {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->getBundle()Landroid/os/Bundle;

    move-result-object v19

    .line 239
    .local v19, "nextBundle":Landroid/os/Bundle;
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 246
    .local v13, "eventUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 248
    .local v11, "error":Ljava/lang/Exception;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->maybeRefreshBlocking()V

    .line 250
    if-eqz v13, :cond_4

    .line 251
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 252
    .local v12, "eventUri":Ljava/lang/String;
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->updateDocumentList(Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 255
    .end local v12    # "eventUri":Ljava/lang/String;
    .end local v15    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v14

    .line 256
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Exception "

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v14}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    move-object v11, v14

    .line 260
    if-nez v11, :cond_6

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "intentcallback;"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/util/LinkedHashMap;

    const/16 v27, 0x0

    invoke-direct/range {v26 .. v27}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-virtual/range {v24 .. v26}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Added Intent = "

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    .end local v14    # "ex":Ljava/lang/Exception;
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_9

    .line 352
    .end local v11    # "error":Ljava/lang/Exception;
    .end local v13    # "eventUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "nextBundle":Landroid/os/Bundle;
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_0

    goto/16 :goto_0

    .line 260
    .restart local v11    # "error":Ljava/lang/Exception;
    .restart local v13    # "eventUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "nextBundle":Landroid/os/Bundle;
    :cond_4
    if-nez v11, :cond_5

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "intentcallback;"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/util/LinkedHashMap;

    const/16 v27, 0x0

    invoke-direct/range {v26 .. v27}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-virtual/range {v24 .. v26}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Added Intent = "

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 269
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 271
    .local v18, "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    if-eqz v18, :cond_2

    .line 272
    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v24

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1, v11}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_3

    .line 269
    .end local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    .restart local v14    # "ex":Ljava/lang/Exception;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 271
    .restart local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    if-eqz v18, :cond_2

    .line 272
    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v24

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1, v11}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto/16 :goto_3

    .line 260
    .end local v14    # "ex":Ljava/lang/Exception;
    .end local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    :catchall_0
    move-exception v24

    if-nez v11, :cond_8

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v25, v0

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "intentcallback;"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    new-instance v27, Ljava/util/LinkedHashMap;

    const/16 v28, 0x0

    invoke-direct/range {v27 .. v28}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-virtual/range {v25 .. v27}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v25, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v26, "Added Intent = "

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v25 .. v27}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    :cond_7
    :goto_5
    throw v24

    .line 269
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 271
    .restart local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    if-eqz v18, :cond_7

    .line 272
    # getter for: Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->startId:I
    invoke-static {v6}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;->access$000(Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask$CalendarBundle;)I

    move-result v25

    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-interface {v0, v1, v11}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onAborted(ILjava/lang/Throwable;)V

    goto :goto_5

    .line 282
    .end local v11    # "error":Ljava/lang/Exception;
    .end local v13    # "eventUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    .end local v19    # "nextBundle":Landroid/os/Bundle;
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->isFinishedNow()Z

    move-result v24

    if-eqz v24, :cond_a

    .line 283
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Exit is requested"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const-string v28, " internal run"

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 288
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_3

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 292
    .local v17, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/LinkedHashMap;->size()I

    move-result v16

    .line 294
    .local v16, "initial_size":I
    const/4 v8, 0x0

    .line 295
    .local v8, "dcmIndexSearcher":Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;
    const/4 v10, 0x0

    .line 296
    .local v10, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/16 v20, 0x0

    .line 298
    .local v20, "pendingEvents":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mNRTManager:Lcom/samsung/dcm/framework/lucene/DCMNRTManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/dcm/framework/lucene/DCMNRTManager;->getDCMIndexSearcher()Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;

    move-result-object v8

    .line 299
    :cond_b
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_12

    .line 300
    const/4 v10, 0x0

    .line 301
    const/16 v20, 0x0

    .line 302
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    move-object/from16 v0, v24

    check-cast v0, Ljava/util/Map$Entry;

    move-object v10, v0

    .line 303
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    .line 304
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v24

    move-object/from16 v0, v24

    check-cast v0, Ljava/util/LinkedHashMap;

    move-object/from16 v20, v0

    .line 305
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "intentcallback"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 306
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, ";"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 307
    .local v7, "callback":[Ljava/lang/String;
    array-length v0, v7

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 308
    const/16 v24, 0x1

    aget-object v24, v7, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 309
    .local v22, "startid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentCallback:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;

    .line 310
    .restart local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    if-eqz v18, :cond_c

    .line 311
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;->onFinished(I)V

    .line 313
    :cond_c
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Processed Intent = "

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    .end local v7    # "callback":[Ljava/lang/String;
    .end local v18    # "listener":Lcom/samsung/dcm/framework/extractormanager/task/ITaskStatusListener;
    .end local v22    # "startid":I
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->isFinishedNow()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 334
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "Exit is requested"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const-string v28, " processing doc"

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 345
    if-eqz v8, :cond_e

    .line 346
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 348
    :cond_e
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, " initial_size = "

    const/16 v26, 0x3

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    const/16 v27, 0x1

    const-string v28, " pending = "

    aput-object v28, v26, v27

    const/16 v27, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedHashMap;->size()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 316
    :cond_f
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->clear()V

    .line 317
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v8, v0, v1}, Lcom/samsung/dcm/framework/utils/DCMUtilities;->fetchLuceneDocsForUri(Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;Ljava/lang/String;Ljava/util/List;)V

    .line 320
    const/16 v21, 0x0

    .line 321
    .local v21, "reload":Z
    if-eqz v20, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_10

    .line 322
    const-string v24, "INSERT_PENDING"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mLuceneDocList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/lucene/document/Document;

    invoke-virtual {v8}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->getGenerationId()J

    move-result-wide v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v20

    move-wide/from16 v3, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->applyPendingEventsToDoc(Lorg/apache/lucene/document/Document;Ljava/util/LinkedHashMap;J)Z

    move-result v21

    .line 328
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_11

    if-eqz v21, :cond_d

    .line 329
    :cond_11
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "In between break reload = "

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/dcm/framework/exceptions/LuceneException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 345
    .end local v21    # "reload":Z
    :cond_12
    if-eqz v8, :cond_13

    .line 346
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 348
    :cond_13
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, " initial_size = "

    const/16 v26, 0x3

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    const/16 v27, 0x1

    const-string v28, " pending = "

    aput-object v28, v26, v27

    const/16 v27, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedHashMap;->size()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 338
    :catch_1
    move-exception v9

    .line 339
    .local v9, "e":Ljava/io/IOException;
    :try_start_4
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "IOException "

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 340
    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 345
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v24

    if-eqz v8, :cond_14

    .line 346
    invoke-virtual {v8}, Lcom/samsung/dcm/framework/lucene/DCMIndexSearcher;->release()V

    .line 348
    :cond_14
    sget-object v25, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v26, " initial_size = "

    const/16 v27, 0x3

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    const/16 v28, 0x1

    const-string v29, " pending = "

    aput-object v29, v27, v28

    const/16 v28, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mDocumentList:Ljava/util/LinkedHashMap;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/util/LinkedHashMap;->size()I

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v25 .. v27}, Lcom/samsung/commons/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v24

    .line 341
    :catch_2
    move-exception v9

    .line 342
    .local v9, "e":Lcom/samsung/dcm/framework/exceptions/LuceneException;
    :try_start_5
    sget-object v24, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->TAG:Ljava/lang/String;

    const-string v25, "LuceneException "

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v9}, Lcom/samsung/commons/Log;->throwable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 343
    throw v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/task/CalendarUpdateTask;->mConcurrentEventBundles:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    return v0
.end method
