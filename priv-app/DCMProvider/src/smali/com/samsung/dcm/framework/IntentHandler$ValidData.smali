.class public final enum Lcom/samsung/dcm/framework/IntentHandler$ValidData;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/framework/IntentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/IntentHandler$ValidData;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidData;

.field public static final enum Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

.field public static final enum CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

.field public static final enum Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

.field public static final enum Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

.field public static final enum Package:Lcom/samsung/dcm/framework/IntentHandler$ValidData;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 316
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    const-string v1, "Ids"

    const-string v2, "ids"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    .line 317
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    const-string v1, "Action"

    const-string v2, "action_name"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    .line 318
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    const-string v1, "Fields"

    const-string v2, "fields"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    .line 319
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    const-string v1, "Options"

    const-string v2, "options"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    .line 320
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    const-string v1, "Package"

    const-string v2, "package_name"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Package:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    .line 321
    new-instance v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    const-string v1, "CalendarOp"

    const/4 v2, 0x5

    const-string v3, "calendar_operation"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    .line 315
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Ids:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Action:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Fields:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Options:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->Package:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->CalendarOp:Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 327
    iput-object p3, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->mName:Ljava/lang/String;

    .line 328
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 335
    const-string v4, "ValidData fromString param is null"

    invoke-static {p0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    invoke-static {}, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->values()[Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 337
    .local v1, "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    iget-object v4, v1, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 341
    .end local v1    # "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    :goto_1
    return-object v1

    .line 336
    .restart local v1    # "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 341
    .end local v1    # "dt":Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 315
    const-class v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/IntentHandler$ValidData;
    .locals 1

    .prologue
    .line 315
    sget-object v0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->$VALUES:[Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/IntentHandler$ValidData;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/IntentHandler$ValidData;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/samsung/dcm/framework/IntentHandler$ValidData;->mName:Ljava/lang/String;

    return-object v0
.end method
