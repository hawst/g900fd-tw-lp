.class public final enum Lcom/samsung/dcm/framework/extractormanager/Operation;
.super Ljava/lang/Enum;
.source "Operation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/framework/extractormanager/Operation$Family;,
        Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/framework/extractormanager/Operation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/framework/extractormanager/Operation;

.field public static final enum DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

.field public static final enum INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

.field public static final enum QUICK_DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

.field public static final enum QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

.field public static final enum QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

.field public static final enum UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;


# instance fields
.field private mFamily:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

.field private mPriority:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 11
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    const-string v1, "QUICK_INSERT"

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/Operation;-><init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 12
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    const-string v1, "QUICK_UPDATE"

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/Operation;-><init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 13
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    const-string v1, "QUICK_DELETE"

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/Operation;-><init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 14
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    const-string v1, "INSERT"

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/Operation;-><init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 15
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    const-string v1, "UPDATE"

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/samsung/dcm/framework/extractormanager/Operation;-><init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 16
    new-instance v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    const-string v1, "DELETE"

    const/4 v2, 0x5

    sget-object v3, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v4, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/dcm/framework/extractormanager/Operation;-><init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    .line 10
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/dcm/framework/extractormanager/Operation;

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/dcm/framework/extractormanager/Operation;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/Operation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/samsung/dcm/framework/extractormanager/Operation$Priority;Lcom/samsung/dcm/framework/extractormanager/Operation$Family;)V
    .locals 0
    .param p3, "priority"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .param p4, "family"    # Lcom/samsung/dcm/framework/extractormanager/Operation$Family;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;",
            "Lcom/samsung/dcm/framework/extractormanager/Operation$Family;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mPriority:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    .line 34
    iput-object p4, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mFamily:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/framework/extractormanager/Operation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/framework/extractormanager/Operation;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/framework/extractormanager/Operation;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->$VALUES:[Lcom/samsung/dcm/framework/extractormanager/Operation;

    invoke-virtual {v0}, [Lcom/samsung/dcm/framework/extractormanager/Operation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/framework/extractormanager/Operation;

    return-object v0
.end method


# virtual methods
.method public getFafmilyDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mFamily:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-virtual {v0}, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPriority()Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mPriority:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    return-object v0
.end method

.method public isDeleteFamilyOperation()Z
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_DELETE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHighPriority()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mPriority:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->HIGH:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInsertFamilyOperation()Z
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_INSERT:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLowPriority()Z
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mPriority:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    sget-object v1, Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;->LOW:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdateFamilyOperation()Z
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/samsung/dcm/framework/extractormanager/Operation;->QUICK_UPDATE:Lcom/samsung/dcm/framework/extractormanager/Operation;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", family = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mFamily:Lcom/samsung/dcm/framework/extractormanager/Operation$Family;

    invoke-virtual {v1}, Lcom/samsung/dcm/framework/extractormanager/Operation$Family;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", priority = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/dcm/framework/extractormanager/Operation;->mPriority:Lcom/samsung/dcm/framework/extractormanager/Operation$Priority;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
