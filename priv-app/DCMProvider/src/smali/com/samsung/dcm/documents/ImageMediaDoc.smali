.class public Lcom/samsung/dcm/documents/ImageMediaDoc;
.super Lcom/samsung/dcm/documents/MediaDoc;
.source "ImageMediaDoc.java"

# interfaces
.implements Lcom/samsung/dcm/documents/IContainingEventData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/ImageMediaDoc;",
            ">;"
        }
    .end annotation
.end field

.field private static final MEMEBR_COUNT:I = 0x4


# instance fields
.field private mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field private mEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation
.end field

.field private mFaceCount:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLocation:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mMeta:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/MetaData;",
            ">;"
        }
    .end annotation
.end field

.field private mNamedPlace:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/LocationData;",
            ">;"
        }
    .end annotation
.end field

.field private mOscStatus:I

.field private mPersons:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/documents/Person;",
            ">;"
        }
    .end annotation
.end field

.field private mSceneTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSubSceneTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/samsung/dcm/documents/ImageMediaDoc$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/ImageMediaDoc$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/dcm/documents/MediaDoc;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    .line 42
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 46
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mOscStatus:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    .line 63
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSceneTypes:Ljava/util/Set;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSubSceneTypes:Ljava/util/Set;

    .line 82
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;-><init>(Landroid/net/Uri;)V

    .line 38
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    .line 42
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 46
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mOscStatus:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    .line 63
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSceneTypes:Ljava/util/Set;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSubSceneTypes:Ljava/util/Set;

    .line 82
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    .line 93
    invoke-direct {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;-><init>(Landroid/os/Parcel;)V

    .line 38
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    .line 42
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 46
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    .line 51
    iput v5, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mOscStatus:I

    .line 57
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    .line 63
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 68
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    .line 73
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSceneTypes:Ljava/util/Set;

    .line 78
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSubSceneTypes:Ljava/util/Set;

    .line 82
    sget-object v3, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 96
    iget-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    const-class v4, Lcom/samsung/dcm/documents/EventData;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v0, "personList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/documents/Person;>;"
    const-class v3, Lcom/samsung/dcm/documents/Person;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 100
    iget-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 102
    const/4 v3, 0x4

    new-array v1, v3, [Z

    .line 103
    .local v1, "presenceArray":[Z
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 105
    aget-boolean v3, v1, v5

    if-eqz v3, :cond_0

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    .line 108
    :cond_0
    const/4 v3, 0x1

    aget-boolean v3, v1, v3

    if-eqz v3, :cond_1

    .line 109
    new-instance v2, Landroid/location/Location;

    const-string v3, ""

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 110
    .local v2, "tempLocation":Landroid/location/Location;
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 112
    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 114
    .end local v2    # "tempLocation":Landroid/location/Location;
    :cond_1
    const/4 v3, 0x2

    aget-boolean v3, v1, v3

    if-eqz v3, :cond_2

    .line 117
    const-class v3, Lcom/samsung/dcm/documents/MetaData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/documents/MetaData;

    invoke-static {v3}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    .line 120
    :cond_2
    const/4 v3, 0x3

    aget-boolean v3, v1, v3

    if-eqz v3, :cond_3

    .line 121
    const-class v3, Lcom/samsung/dcm/documents/LocationData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/samsung/dcm/documents/LocationData;

    invoke-static {v3}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 126
    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 426
    instance-of v2, p1, Lcom/samsung/dcm/documents/ImageMediaDoc;

    if-eqz v2, :cond_4

    move-object v0, p1

    .line 427
    check-cast v0, Lcom/samsung/dcm/documents/ImageMediaDoc;

    .line 430
    .local v0, "imd":Lcom/samsung/dcm/documents/ImageMediaDoc;
    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 431
    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    iget-object v2, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    cmpl-double v2, v6, v8

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    iget-object v2, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    cmpl-double v2, v6, v8

    if-nez v2, :cond_0

    move v1, v3

    .line 440
    .local v1, "locationEquality":Z
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iget-object v5, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v2, v5}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    iget-object v5, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    iget-object v5, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    iget-object v5, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    .line 447
    .end local v0    # "imd":Lcom/samsung/dcm/documents/ImageMediaDoc;
    .end local v1    # "locationEquality":Z
    :goto_1
    return v3

    .restart local v0    # "imd":Lcom/samsung/dcm/documents/ImageMediaDoc;
    :cond_0
    move v1, v4

    .line 431
    goto :goto_0

    .line 434
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-nez v2, :cond_2

    .line 435
    const/4 v1, 0x1

    .restart local v1    # "locationEquality":Z
    goto :goto_0

    .line 437
    .end local v1    # "locationEquality":Z
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "locationEquality":Z
    goto :goto_0

    :cond_3
    move v3, v4

    .line 440
    goto :goto_1

    .end local v0    # "imd":Lcom/samsung/dcm/documents/ImageMediaDoc;
    .end local v1    # "locationEquality":Z
    :cond_4
    move v3, v4

    .line 447
    goto :goto_1
.end method

.method public getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method

.method public getEvents()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFaceCount()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getInputStream()Lcom/google/common/base/Optional;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<+",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 281
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    .line 282
    .local v0, "in":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<+Ljava/io/InputStream;>;"
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 286
    :cond_0
    return-object v0
.end method

.method public getLocation()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getMeta()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/MetaData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getNamedPlace()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/LocationData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getOscStatus()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mOscStatus:I

    return v0
.end method

.method public getPersons()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/documents/Person;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSceneTypes()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSceneTypes:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSubSceneTypes()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSubSceneTypes:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 452
    const/4 v0, 0x1

    .line 454
    .local v0, "multipNeutralElem":I
    invoke-super {p0}, Lcom/samsung/dcm/documents/MediaDoc;->hashCode()I

    move-result v3

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_0
    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v3}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v3

    add-int v1, v2, v3

    .line 458
    .local v1, "result":I
    return v1

    .line 454
    .end local v1    # "result":I
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setEvents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/documents/EventData;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    .line 330
    return-void
.end method

.method public setExifMeta(Ljava/util/HashMap;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 319
    .local p1, "exif":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/samsung/dcm/documents/MetaData;->fromMap(Ljava/util/HashMap;)Lcom/samsung/dcm/documents/MetaData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/dcm/documents/ImageMediaDoc;->setMeta(Lcom/samsung/dcm/documents/MetaData;)V

    .line 320
    iget-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    invoke-virtual {v0}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/documents/MetaData;

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/MetaData;->getMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public setFaceCount(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "faceCount"    # Ljava/lang/Integer;

    .prologue
    .line 237
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    .line 238
    return-void
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 255
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 256
    return-void
.end method

.method public setMeta(Lcom/samsung/dcm/documents/MetaData;)V
    .locals 1
    .param p1, "meta"    # Lcom/samsung/dcm/documents/MetaData;

    .prologue
    .line 356
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    .line 357
    return-void
.end method

.method public setNamedPlace(Lcom/samsung/dcm/documents/LocationData;)V
    .locals 1
    .param p1, "namedPlace"    # Lcom/samsung/dcm/documents/LocationData;

    .prologue
    .line 365
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 366
    return-void
.end method

.method public setOscStatus(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mOscStatus:I

    .line 229
    return-void
.end method

.method public setPersons(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/dcm/documents/Person;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "persons":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/dcm/documents/Person;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    .line 296
    return-void
.end method

.method public setSceneTypes(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p1, "sceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSceneTypes:Ljava/util/Set;

    .line 339
    return-void
.end method

.method public setSubSceneTypes(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    .local p1, "sceneTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mSubSceneTypes:Ljava/util/Set;

    .line 348
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 370
    const-string v0, "ImageMediaDoc"

    .line 371
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145
    invoke-super {p0, p1, v2}, Lcom/samsung/dcm/documents/MediaDoc;->writeToParcel(Landroid/os/Parcel;I)V

    .line 148
    iget-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mEvents:Ljava/util/List;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 150
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mPersons:Ljava/util/Set;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 152
    new-array v0, v5, [Z

    iget-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v2

    iget-object v3, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v1

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    .line 159
    .local v0, "presenceArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 160
    array-length v3, v0

    if-ne v3, v5, :cond_4

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 162
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mFaceCount:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 167
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 171
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mMeta:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 173
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 174
    iget-object v1, p0, Lcom/samsung/dcm/documents/ImageMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 175
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 160
    goto :goto_0
.end method
