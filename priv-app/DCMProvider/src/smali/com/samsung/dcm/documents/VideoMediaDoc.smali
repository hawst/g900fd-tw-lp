.class public Lcom/samsung/dcm/documents/VideoMediaDoc;
.super Lcom/samsung/dcm/documents/MediaDoc;
.source "VideoMediaDoc.java"

# interfaces
.implements Lcom/samsung/dcm/documents/IContainingEventData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/VideoMediaDoc;",
            ">;"
        }
    .end annotation
.end field

.field private static final OPTIONAL_NUM:I = 0x9


# instance fields
.field private mAlbum:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mArtist:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBucketDisplayName:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDescription:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field private mDuration:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation
.end field

.field private mLanguage:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLocation:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mNamedPlace:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/LocationData;",
            ">;"
        }
    .end annotation
.end field

.field private mResolution:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/samsung/dcm/documents/VideoMediaDoc$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/VideoMediaDoc$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/VideoMediaDoc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/dcm/documents/MediaDoc;-><init>()V

    .line 44
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 46
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 48
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 50
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    .line 52
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    .line 54
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    .line 56
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    .line 58
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    .line 60
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    .line 67
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;-><init>(Landroid/net/Uri;)V

    .line 44
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 46
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 48
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 50
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    .line 52
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    .line 54
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    .line 56
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    .line 58
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    .line 60
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    .line 67
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;-><init>(Landroid/os/Parcel;)V

    .line 44
    sget-object v2, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 46
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 48
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 50
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    .line 52
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    .line 54
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    .line 56
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    .line 58
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    .line 60
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 62
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    .line 67
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 79
    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    const-class v3, Lcom/samsung/dcm/documents/EventData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 81
    const/16 v2, 0x9

    new-array v0, v2, [Z

    .line 82
    .local v0, "presenceArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 84
    const/4 v2, 0x0

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_0

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 87
    :cond_0
    const/4 v2, 0x1

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_1

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 90
    :cond_1
    const/4 v2, 0x2

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_2

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    .line 93
    :cond_2
    const/4 v2, 0x3

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_3

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    .line 96
    :cond_3
    const/4 v2, 0x4

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_4

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    .line 99
    :cond_4
    const/4 v2, 0x5

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_5

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    .line 102
    :cond_5
    const/4 v2, 0x6

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_6

    .line 103
    new-instance v1, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 104
    .local v1, "tempLocation":Landroid/location/Location;
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 106
    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 108
    .end local v1    # "tempLocation":Landroid/location/Location;
    :cond_6
    const/4 v2, 0x7

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_7

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    .line 111
    :cond_7
    const/16 v2, 0x8

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_8

    .line 112
    const-class v2, Lcom/samsung/dcm/documents/LocationData;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/dcm/documents/LocationData;

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 115
    :cond_8
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/samsung/dcm/documents/VideoMediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 413
    if-eqz p1, :cond_4

    instance-of v2, p1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    if-eqz v2, :cond_4

    move-object v1, p1

    .line 414
    check-cast v1, Lcom/samsung/dcm/documents/VideoMediaDoc;

    .line 417
    .local v1, "vmd":Lcom/samsung/dcm/documents/VideoMediaDoc;
    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 418
    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    iget-object v2, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    cmpl-double v2, v6, v8

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    iget-object v2, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    cmpl-double v2, v6, v8

    if-nez v2, :cond_0

    move v0, v3

    .line 429
    .local v0, "locationEquality":Z
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v2, v5}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    iget-object v5, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v5}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 438
    .end local v0    # "locationEquality":Z
    .end local v1    # "vmd":Lcom/samsung/dcm/documents/VideoMediaDoc;
    :goto_1
    return v3

    .restart local v1    # "vmd":Lcom/samsung/dcm/documents/VideoMediaDoc;
    :cond_0
    move v0, v4

    .line 418
    goto :goto_0

    .line 423
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-nez v2, :cond_2

    .line 424
    const/4 v0, 0x1

    .restart local v0    # "locationEquality":Z
    goto :goto_0

    .line 426
    .end local v0    # "locationEquality":Z
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "locationEquality":Z
    goto :goto_0

    :cond_3
    move v3, v4

    .line 429
    goto :goto_1

    .end local v0    # "locationEquality":Z
    .end local v1    # "vmd":Lcom/samsung/dcm/documents/VideoMediaDoc;
    :cond_4
    move v3, v4

    .line 438
    goto :goto_1
.end method

.method public getAlbum()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getArtist()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getBucketDisplayName()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getDescription()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method

.method public getDuration()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getInputStream()Lcom/google/common/base/Optional;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<+",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 171
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    .line 172
    .local v0, "in":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<+Ljava/io/InputStream;>;"
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 176
    :cond_0
    return-object v0
.end method

.method public getLanguage()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getLocation()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getNamedPlace()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/samsung/dcm/documents/LocationData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getResolution()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 443
    const/4 v0, 0x1

    .line 445
    .local v0, "multipNeutralElem":I
    invoke-super {p0}, Lcom/samsung/dcm/documents/MediaDoc;->hashCode()I

    move-result v2

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v4}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v4

    mul-int/2addr v4, v2

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_0
    add-int/2addr v4, v2

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_1
    add-int/2addr v4, v2

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    :cond_0
    add-int v1, v4, v3

    .line 452
    .local v1, "result":I
    return v1

    .end local v1    # "result":I
    :cond_1
    move v2, v3

    .line 445
    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 1
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 231
    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "artist"    # Ljava/lang/String;

    .prologue
    .line 212
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 213
    return-void
.end method

.method public setBucketDisplayName(Ljava/lang/String;)V
    .locals 1
    .param p1, "bucketDisplayName"    # Ljava/lang/String;

    .prologue
    .line 338
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    .line 339
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 266
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    .line 267
    return-void
.end method

.method public setDuration(Ljava/lang/Long;)V
    .locals 1
    .param p1, "duration"    # Ljava/lang/Long;

    .prologue
    .line 284
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    .line 285
    return-void
.end method

.method public setEvents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/dcm/documents/EventData;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    .line 195
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    .line 303
    return-void
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 356
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    .line 357
    return-void
.end method

.method public setNamedPlace(Lcom/samsung/dcm/documents/LocationData;)V
    .locals 1
    .param p1, "namedPlace"    # Lcom/samsung/dcm/documents/LocationData;

    .prologue
    .line 248
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    .line 249
    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 1
    .param p1, "resolution"    # Ljava/lang/String;

    .prologue
    .line 320
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    .line 321
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 361
    const-string v0, ""

    .line 362
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 365
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/16 v5, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    invoke-super {p0, p1, p2}, Lcom/samsung/dcm/documents/MediaDoc;->writeToParcel(Landroid/os/Parcel;I)V

    .line 120
    iget-object v3, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mEvents:Ljava/util/List;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 122
    new-array v0, v5, [Z

    iget-object v3, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v2

    iget-object v3, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v1

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    .line 127
    .local v0, "presenceArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 128
    array-length v3, v0

    if-ne v3, v5, :cond_9

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 130
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 137
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mBucketDisplayName:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 140
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDescription:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 143
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mDuration:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 145
    :cond_4
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 146
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLanguage:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    :cond_5
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 149
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 150
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mLocation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 153
    :cond_6
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 154
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mResolution:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 156
    :cond_7
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 157
    iget-object v1, p0, Lcom/samsung/dcm/documents/VideoMediaDoc;->mNamedPlace:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 158
    :cond_8
    return-void

    :cond_9
    move v1, v2

    .line 128
    goto/16 :goto_0
.end method
