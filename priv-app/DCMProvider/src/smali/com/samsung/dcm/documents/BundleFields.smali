.class public final Lcom/samsung/dcm/documents/BundleFields;
.super Ljava/lang/Object;
.source "BundleFields.java"


# static fields
.field public static final SFIELD_CREATION_DATE:Ljava/lang/String; = "date"

.field public static final SFIELD_DUMMY:Ljava/lang/String; = "dummy"

.field public static final SFIELD_EVENT_TITLE:Ljava/lang/String; = "event_title"

.field public static final SFIELD_EVENT_URI:Ljava/lang/String; = "event_uri"

.field public static final SFIELD_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final SFIELD_LOCATION:Ljava/lang/String; = "location"

.field public static final SFIELD_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final SFIELD_PATH:Ljava/lang/String; = "path"

.field public static final SFIELD_PERSON:Ljava/lang/String; = "person"

.field public static final SFIELD_REQUEST_TYPE:Ljava/lang/String; = "request_type"

.field public static final SFIELD_SCENE_TYPE:Ljava/lang/String; = "scene_type"

.field public static final SFIELD_SCORELIST:Ljava/lang/String; = "scorelist"

.field public static final SFIELD_SEARCH_BUNDLE:Ljava/lang/String; = "SEARCH"

.field public static final SFIELD_SLIST:Ljava/lang/String; = "slist"

.field public static final SFIELD_SUBSCENE_TYPE:Ljava/lang/String; = "subscene_type"

.field public static final SFIELD_URI:Ljava/lang/String; = "uri"

.field public static final SFIELD_USER_TAG:Ljava/lang/String; = "user_tag"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
