.class public Lcom/samsung/dcm/documents/PackageFieldMapping;
.super Ljava/lang/Object;
.source "PackageFieldMapping.java"


# instance fields
.field private mHmap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/PackageFieldMapping;->mHmap:Ljava/util/HashMap;

    .line 8
    iget-object v0, p0, Lcom/samsung/dcm/documents/PackageFieldMapping;->mHmap:Ljava/util/HashMap;

    const-string v1, "package:com.android.providers.calendar"

    const-string v2, "root_calendar_event#event_uri#calendar_event"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iget-object v0, p0, Lcom/samsung/dcm/documents/PackageFieldMapping;->mHmap:Ljava/util/HashMap;

    const-string v1, "package:com.android.providers.contacts"

    const-string v2, "root_person#person_names"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    return-void
.end method


# virtual methods
.method public getMapping()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lcom/samsung/dcm/documents/PackageFieldMapping;->mHmap:Ljava/util/HashMap;

    return-object v0
.end method
