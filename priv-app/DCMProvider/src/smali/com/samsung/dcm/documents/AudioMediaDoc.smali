.class public Lcom/samsung/dcm/documents/AudioMediaDoc;
.super Lcom/samsung/dcm/documents/MediaDoc;
.source "AudioMediaDoc.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/AudioMediaDoc;",
            ">;"
        }
    .end annotation
.end field

.field private static final OPTIONAL_NUM:I = 0x4


# instance fields
.field private mAlbum:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mArtist:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field private mGenre:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mYear:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/samsung/dcm/documents/AudioMediaDoc$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/AudioMediaDoc$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/AudioMediaDoc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/dcm/documents/MediaDoc;-><init>()V

    .line 70
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 72
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 73
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 74
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    .line 75
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;-><init>(Landroid/net/Uri;)V

    .line 70
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 72
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 73
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 74
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    .line 75
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    .line 45
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;-><init>(Landroid/os/Parcel;)V

    .line 70
    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 72
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 73
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 74
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    .line 75
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    .line 49
    const/4 v1, 0x4

    new-array v0, v1, [Z

    .line 50
    .local v0, "presenceArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 52
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 55
    :cond_0
    const/4 v1, 0x1

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_1

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 58
    :cond_1
    const/4 v1, 0x2

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_2

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    .line 61
    :cond_2
    const/4 v1, 0x3

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_3

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    .line 63
    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/samsung/dcm/documents/AudioMediaDoc;->getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 224
    instance-of v2, p1, Lcom/samsung/dcm/documents/AudioMediaDoc;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 225
    check-cast v0, Lcom/samsung/dcm/documents/AudioMediaDoc;

    .line 226
    .local v0, "amd":Lcom/samsung/dcm/documents/AudioMediaDoc;
    invoke-super {p0, p1}, Lcom/samsung/dcm/documents/MediaDoc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iget-object v3, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 231
    .end local v0    # "amd":Lcom/samsung/dcm/documents/AudioMediaDoc;
    :cond_0
    return v1
.end method

.method public getAlbum()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getArtist()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method

.method public getGenre()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getInputStream()Lcom/google/common/base/Optional;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<+",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    .line 81
    .local v0, "in":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<+Ljava/io/InputStream;>;"
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    .line 85
    :cond_0
    return-object v0
.end method

.method public getYear()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 236
    const/4 v0, 0x1

    .line 238
    .local v0, "multipNeutralElem":I
    invoke-super {p0}, Lcom/samsung/dcm/documents/MediaDoc;->hashCode()I

    move-result v4

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_0
    add-int/2addr v4, v2

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_1
    add-int/2addr v4, v2

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_2
    add-int/2addr v4, v2

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_0
    add-int v1, v4, v3

    .line 244
    .local v1, "result":I
    return v1

    .end local v1    # "result":I
    :cond_1
    move v2, v3

    .line 238
    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 1
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    .line 122
    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "artist"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    .line 104
    return-void
.end method

.method public setGenre(Ljava/lang/String;)V
    .locals 1
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 157
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    .line 158
    return-void
.end method

.method public setYear(I)V
    .locals 1
    .param p1, "year"    # I

    .prologue
    .line 139
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    .line 140
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 162
    const-string v0, "AudioMediaDoc"

    .line 163
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 200
    invoke-super {p0, p1, p2}, Lcom/samsung/dcm/documents/MediaDoc;->writeToParcel(Landroid/os/Parcel;I)V

    .line 202
    new-array v0, v5, [Z

    iget-object v3, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v2

    iget-object v3, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v1

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    .line 205
    .local v0, "presenceArray":[Z
    array-length v3, v0

    if-ne v3, v5, :cond_4

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 206
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 208
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mArtist:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 212
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mAlbum:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 215
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mYear:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 217
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    iget-object v1, p0, Lcom/samsung/dcm/documents/AudioMediaDoc;->mGenre:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 220
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 205
    goto :goto_0
.end method
