.class public Lcom/samsung/dcm/documents/EventData;
.super Ljava/lang/Object;
.source "EventData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final COLUMN_ALL_DAY:I = 0x5

.field private static final COLUMN_DESCRIPTION:I = 0x6

.field private static final COLUMN_END_DATE:I = 0x3

.field private static final COLUMN_ID:I = 0x0

.field private static final COLUMN_START_DATE:I = 0x2

.field private static final COLUMN_TITLE:I = 0x1

.field private static final COLUMN_URI:I = 0x4

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAllDay:Z

.field private mDescription:Ljava/lang/String;

.field private mEndDate:Ljava/lang/Long;

.field private mId:Ljava/lang/Long;

.field private mStartDate:Ljava/lang/Long;

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/samsung/dcm/documents/EventData$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/EventData$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/EventData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/EventData;->mUri:Landroid/net/Uri;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/EventData;->mId:Ljava/lang/Long;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/EventData;->mStartDate:Ljava/lang/Long;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/EventData;->mEndDate:Ljava/lang/Long;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/dcm/documents/EventData;->mAllDay:Z

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mDescription:Ljava/lang/String;

    .line 43
    return-void

    .line 41
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 204
    if-nez p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v1

    .line 206
    :cond_1
    if-ne p1, p0, :cond_2

    .line 207
    const/4 v1, 0x1

    goto :goto_0

    .line 208
    :cond_2
    instance-of v2, p1, Lcom/samsung/dcm/documents/EventData;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 210
    check-cast v0, Lcom/samsung/dcm/documents/EventData;

    .line 211
    .local v0, "o":Lcom/samsung/dcm/documents/EventData;
    invoke-virtual {v0}, Lcom/samsung/dcm/documents/EventData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/dcm/documents/EventData;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "fieldIndex"    # I

    .prologue
    .line 246
    packed-switch p1, :pswitch_data_0

    .line 269
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No field with index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mId:Ljava/lang/Long;

    .line 266
    :goto_0
    return-object v0

    .line 251
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    goto :goto_0

    .line 254
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mUri:Landroid/net/Uri;

    goto :goto_0

    .line 257
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mStartDate:Ljava/lang/Long;

    goto :goto_0

    .line 260
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mEndDate:Ljava/lang/Long;

    goto :goto_0

    .line 263
    :pswitch_5
    iget-boolean v0, p0, Lcom/samsung/dcm/documents/EventData;->mAllDay:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 266
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mDescription:Ljava/lang/String;

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getCategories()Ljava/lang/String;
    .locals 2

    .prologue
    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 317
    .local v0, "s":Ljava/lang/StringBuilder;
    const-string v1, "root_calendar_event"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getEndDate()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mEndDate:Ljava/lang/Long;

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mId:Ljava/lang/Long;

    return-object v0
.end method

.method public getStartDate()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mStartDate:Ljava/lang/Long;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/samsung/dcm/documents/EventData;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAllDay()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/samsung/dcm/documents/EventData;->mAllDay:Z

    return v0
.end method

.method public set(ILjava/lang/Object;)V
    .locals 3
    .param p1, "fieldIndex"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 275
    packed-switch p1, :pswitch_data_0

    .line 305
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No field with index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :pswitch_0
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lcom/samsung/dcm/documents/EventData;->mId:Ljava/lang/Long;

    .line 308
    :goto_0
    return-void

    .line 281
    .restart local p2    # "value":Ljava/lang/Object;
    :pswitch_1
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    goto :goto_0

    .line 285
    .restart local p2    # "value":Ljava/lang/Object;
    :pswitch_2
    check-cast p2, Landroid/net/Uri;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lcom/samsung/dcm/documents/EventData;->mUri:Landroid/net/Uri;

    goto :goto_0

    .line 289
    .restart local p2    # "value":Ljava/lang/Object;
    :pswitch_3
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lcom/samsung/dcm/documents/EventData;->mStartDate:Ljava/lang/Long;

    goto :goto_0

    .line 293
    .restart local p2    # "value":Ljava/lang/Object;
    :pswitch_4
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lcom/samsung/dcm/documents/EventData;->mEndDate:Ljava/lang/Long;

    goto :goto_0

    .line 297
    .restart local p2    # "value":Ljava/lang/Object;
    :pswitch_5
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/dcm/documents/EventData;->mAllDay:Z

    goto :goto_0

    .line 301
    .restart local p2    # "value":Ljava/lang/Object;
    :pswitch_6
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lcom/samsung/dcm/documents/EventData;->mDescription:Ljava/lang/String;

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setAllDay(Z)V
    .locals 0
    .param p1, "allDay"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/samsung/dcm/documents/EventData;->mAllDay:Z

    .line 169
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/samsung/dcm/documents/EventData;->mDescription:Ljava/lang/String;

    .line 227
    return-void
.end method

.method public setEndDate(Ljava/lang/Long;)V
    .locals 0
    .param p1, "endDate"    # Ljava/lang/Long;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/dcm/documents/EventData;->mEndDate:Ljava/lang/Long;

    .line 151
    return-void
.end method

.method public setId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/dcm/documents/EventData;->mId:Ljava/lang/Long;

    .line 88
    return-void
.end method

.method public setStartDate(Ljava/lang/Long;)V
    .locals 0
    .param p1, "startDate"    # Ljava/lang/Long;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/dcm/documents/EventData;->mStartDate:Ljava/lang/Long;

    .line 133
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/dcm/documents/EventData;->mUri:Landroid/net/Uri;

    .line 106
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x1

    .line 231
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 232
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 233
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mStartDate:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 235
    iget-object v1, p0, Lcom/samsung/dcm/documents/EventData;->mEndDate:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 236
    iget-boolean v1, p0, Lcom/samsung/dcm/documents/EventData;->mAllDay:Z

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 237
    iget-object v0, p0, Lcom/samsung/dcm/documents/EventData;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 238
    return-void

    .line 236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
