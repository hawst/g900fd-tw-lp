.class public Lcom/samsung/dcm/documents/MediaDoc;
.super Ljava/lang/Object;
.source "MediaDoc.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/dcm/documents/MediaDoc$DocType;
    }
.end annotation


# static fields
.field public static final CATEGORY_DELIMITER:Ljava/lang/String; = ";"

.field public static final CATEGORY_ROOT_CALENDAREVENT:Ljava/lang/String; = "root_calendar_event"

.field public static final CATEGORY_ROOT_METADATA:Ljava/lang/String; = "root_metadata"

.field public static final CATEGORY_ROOT_NAMEDLOCATION:Ljava/lang/String; = "root_named_location"

.field public static final CATEGORY_ROOT_PERSON_NAME:Ljava/lang/String; = "root_person"

.field public static final CATEGORY_ROOT_SCENETYPE:Ljava/lang/String; = "root_scene_type"

.field public static final CATEGORY_ROOT_SUBSCENETYPE:Ljava/lang/String; = "root_subscene_type"

.field public static final CATEGORY_ROOT_USER_TAG:Ljava/lang/String; = "root_user_tag"

.field public static CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/MediaDoc;",
            ">;"
        }
    .end annotation
.end field

.field private static final OPTIONAL_NUM:I = 0x4

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBucketId:J

.field protected mCategories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mCreationDate:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleted:I

.field protected mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field private mFatVolumeId:I

.field protected mId:I

.field protected mMimeType:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mPath:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStorageId:I

.field protected mTitle:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mUri:Landroid/net/Uri;

.field private mUserTags:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/samsung/dcm/documents/MediaDoc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc;->TAG:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/samsung/dcm/documents/MediaDoc$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/MediaDoc$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mId:I

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    .line 78
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    .line 83
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    .line 87
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    .line 92
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    .line 101
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUserTags:Ljava/util/Set;

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDeleted:I

    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mId:I

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    .line 78
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    .line 83
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    .line 87
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    .line 92
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    .line 101
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUserTags:Ljava/util/Set;

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDeleted:I

    .line 168
    iput-object p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    .line 169
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x0

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    sget-object v2, Lcom/samsung/dcm/documents/MediaDoc$DocType;->MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 69
    const/4 v2, -0x1

    iput v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mId:I

    .line 74
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    .line 78
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    .line 83
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    .line 87
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    .line 92
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    .line 96
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    .line 101
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUserTags:Ljava/util/Set;

    .line 116
    iput v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDeleted:I

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "string":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    .line 176
    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 179
    const/4 v2, 0x4

    new-array v0, v2, [Z

    .line 180
    .local v0, "presenceArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 182
    aget-boolean v2, v0, v3

    if-eqz v2, :cond_0

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    .line 185
    :cond_0
    const/4 v2, 0x1

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_1

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    .line 188
    :cond_1
    const/4 v2, 0x2

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_2

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    .line 191
    :cond_2
    const/4 v2, 0x3

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_3

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    .line 193
    :cond_3
    return-void
.end method


# virtual methods
.method public addCategory(Ljava/lang/String;)V
    .locals 4
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 228
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc;->TAG:Ljava/lang/String;

    const-string v1, "Adding category :"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 487
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/samsung/dcm/documents/MediaDoc;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 488
    check-cast v0, Lcom/samsung/dcm/documents/MediaDoc;

    .line 489
    .local v0, "md":Lcom/samsung/dcm/documents/MediaDoc;
    if-eqz v0, :cond_0

    .line 490
    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v2, v3}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 497
    .end local v0    # "md":Lcom/samsung/dcm/documents/MediaDoc;
    :cond_0
    return v1
.end method

.method public getBucketId()J
    .locals 2

    .prologue
    .line 478
    iget-wide v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mBucketId:J

    return-wide v0
.end method

.method public getCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCategoriesCount()I
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCreationDate()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getDeleted()I
    .locals 1

    .prologue
    .line 470
    iget v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDeleted:I

    return v0
.end method

.method public getDocType()Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method

.method public getFatVolumeId()I
    .locals 1

    .prologue
    .line 462
    iget v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mFatVolumeId:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 361
    iget v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mId:I

    return v0
.end method

.method public getInputStream()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<+",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 375
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    return-object v0
.end method

.method public getMimeType()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getPath()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getStorageId()I
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mStorageId:I

    return v0
.end method

.method public getTitle()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getUserTags()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUserTags:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 502
    const/16 v1, 0x1f

    .line 503
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 505
    .local v0, "multipNeutralElem":I
    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDocType:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v3}, Lcom/samsung/dcm/documents/MediaDoc$DocType;->ordinal()I

    move-result v3

    mul-int/lit8 v5, v3, 0x1f

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    :goto_0
    mul-int/2addr v5, v3

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    :goto_1
    add-int/2addr v5, v3

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    :cond_0
    add-int v2, v5, v4

    .line 511
    .local v2, "result":I
    return v2

    .end local v2    # "result":I
    :cond_1
    move v3, v4

    .line 505
    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1
.end method

.method public removeCategory(Ljava/lang/String;)V
    .locals 1
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 243
    :cond_0
    return-void
.end method

.method public setBucketId(J)V
    .locals 1
    .param p1, "bucketId"    # J

    .prologue
    .line 482
    iput-wide p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mBucketId:J

    .line 483
    return-void
.end method

.method public setCategories(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    .line 270
    return-void
.end method

.method public setCreationDate(Ljava/lang/Long;)V
    .locals 1
    .param p1, "creationDate"    # Ljava/lang/Long;

    .prologue
    .line 287
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    .line 288
    return-void
.end method

.method public setDeleted(I)V
    .locals 0
    .param p1, "deleted"    # I

    .prologue
    .line 474
    iput p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mDeleted:I

    .line 475
    return-void
.end method

.method public setFatVolumeId(I)V
    .locals 0
    .param p1, "fatVolumeId"    # I

    .prologue
    .line 466
    iput p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mFatVolumeId:I

    .line 467
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 351
    iput p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mId:I

    .line 352
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 403
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    .line 404
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    .line 306
    return-void
.end method

.method public setStorageId(I)V
    .locals 0
    .param p1, "storageId"    # I

    .prologue
    .line 459
    iput p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mStorageId:I

    .line 460
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    .line 324
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    .line 343
    return-void
.end method

.method public setUserTags(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422
    .local p1, "tags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUserTags:Ljava/util/Set;

    .line 423
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    const-string v0, "MediaDoc"

    .line 428
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 429
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 431
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 199
    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCategories:Ljava/util/List;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 201
    new-array v0, v5, [Z

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v2

    iget-object v3, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    aput-boolean v3, v0, v1

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v3

    .line 205
    .local v0, "presenceArray":[Z
    array-length v3, v0

    if-ne v3, v5, :cond_4

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 206
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 209
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mTitle:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mPath:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 216
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mMimeType:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 218
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 219
    iget-object v1, p0, Lcom/samsung/dcm/documents/MediaDoc;->mCreationDate:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 220
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 205
    goto :goto_0
.end method
