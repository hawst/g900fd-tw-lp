.class public Lcom/samsung/dcm/documents/MetaData;
.super Ljava/lang/Object;
.source "MetaData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BOOLEAN_FALSE:I = 0x0

.field public static final BOOLEAN_TRUE:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/MetaData;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXIF_ORIENTATION:I = 0x4

.field public static final EXIF_TAG_FLASH:I = 0x2

.field public static final EXIF_TAG_ISO:I = 0x3

.field public static final EXIF_TAG_MODEL:I = 0x1

.field public static final FLASH_STATE_DISABLED:Ljava/lang/String; = "disabled"

.field public static final FLASH_STATE_ENABLED:Ljava/lang/String; = "enabled"

.field private static final OPTIONAL_NUM:I = 0x4

.field public static final ORIENTATION_LANDSCAPE:Ljava/lang/String; = "landscape"

.field public static final ORIENTATION_PORTRAIT:Ljava/lang/String; = "portrait"

.field public static final UNKNOWN:Ljava/lang/String; = "unknown"


# instance fields
.field private mDeviceModel:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFlash:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mIso:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOrientation:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/dcm/documents/MetaData;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/dcm/documents/MetaData;->$assertionsDisabled:Z

    .line 46
    new-instance v0, Lcom/samsung/dcm/documents/MetaData$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/MetaData$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/MetaData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    .line 28
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    .line 29
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    .line 30
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    .line 28
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    .line 29
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    .line 30
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    .line 63
    const/4 v2, 0x4

    new-array v0, v2, [Z

    .line 64
    .local v0, "presenceArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 66
    aget-boolean v2, v0, v4

    if-eqz v2, :cond_0

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    .line 69
    :cond_0
    aget-boolean v2, v0, v3

    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 71
    .local v1, "tempInt":I
    if-ne v1, v3, :cond_4

    .line 72
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    .line 77
    .end local v1    # "tempInt":I
    :cond_1
    :goto_0
    const/4 v2, 0x2

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_2

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    .line 80
    :cond_2
    const/4 v2, 0x3

    aget-boolean v2, v0, v2

    if-eqz v2, :cond_3

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    .line 82
    :cond_3
    return-void

    .line 74
    .restart local v1    # "tempInt":I
    :cond_4
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    goto :goto_0
.end method

.method public static fromMap(Ljava/util/HashMap;)Lcom/samsung/dcm/documents/MetaData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/dcm/documents/MetaData;"
        }
    .end annotation

    .prologue
    .line 264
    .local p0, "exif":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-nez p0, :cond_1

    .line 265
    const/4 v3, 0x0

    .line 299
    :cond_0
    return-object v3

    .line 268
    :cond_1
    new-instance v3, Lcom/samsung/dcm/documents/MetaData;

    invoke-direct {v3}, Lcom/samsung/dcm/documents/MetaData;-><init>()V

    .line 269
    .local v3, "meta":Lcom/samsung/dcm/documents/MetaData;
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 270
    .local v5, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 271
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 272
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 273
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 274
    .local v2, "key":Ljava/lang/Integer;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 275
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 294
    :pswitch_0
    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 277
    :pswitch_1
    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 281
    :pswitch_2
    const-string v6, "enabled"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 282
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 283
    :cond_2
    const-string v6, "disabled"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 284
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 286
    :cond_3
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 290
    :pswitch_3
    invoke-static {v4}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 319
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/samsung/dcm/documents/MetaData;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 320
    check-cast v0, Lcom/samsung/dcm/documents/MetaData;

    .line 321
    .local v0, "md":Lcom/samsung/dcm/documents/MetaData;
    if-eqz v0, :cond_0

    .line 322
    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    iget-object v3, v0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    invoke-virtual {v2, v3}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 327
    .end local v0    # "md":Lcom/samsung/dcm/documents/MetaData;
    :cond_0
    return v1
.end method

.method public get(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Integer;

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/samsung/dcm/documents/MetaData;->getMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCategories()Ljava/lang/String;
    .locals 2

    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    .local v0, "s":Ljava/lang/StringBuilder;
    const-string v1, "root_metadata"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getDeviceModel()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getIso()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getMap()Ljava/util/HashMap;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 190
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 191
    .local v0, "exif":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v3, :cond_4

    const-string v1, "enabled"

    :goto_0
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 205
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 210
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :cond_3
    return-object v0

    .line 192
    :cond_4
    const-string v1, "disabled"

    goto :goto_0
.end method

.method public getOrientation()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 313
    sget-boolean v0, Lcom/samsung/dcm/documents/MetaData;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 314
    :cond_0
    const/16 v0, 0x2a

    return v0
.end method

.method public isFlash()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public setDeviceModel(Lcom/google/common/base/Optional;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "deviceModel":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    .line 163
    return-void
.end method

.method public setFlash(Lcom/google/common/base/Optional;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "flash":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    .line 127
    return-void
.end method

.method public setIso(Lcom/google/common/base/Optional;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "iso":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    .line 145
    return-void
.end method

.method public setOrientation(Lcom/google/common/base/Optional;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "orientation":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    .line 181
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    const-string v0, ""

    .line 239
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    new-array v0, v5, [Z

    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    aput-boolean v1, v0, v3

    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    aput-boolean v1, v0, v2

    const/4 v1, 0x2

    iget-object v4, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v1

    const/4 v1, 0x3

    iget-object v4, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    invoke-virtual {v4}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v4

    aput-boolean v4, v0, v1

    .line 91
    .local v0, "presenceArray":[Z
    array-length v1, v0

    if-ne v1, v5, :cond_4

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 92
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 94
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mIso:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mFlash:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 99
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mOrientation:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    :cond_2
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 108
    iget-object v1, p0, Lcom/samsung/dcm/documents/MetaData;->mDeviceModel:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    :cond_3
    return-void

    :cond_4
    move v1, v3

    .line 91
    goto :goto_0

    .line 101
    :cond_5
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
