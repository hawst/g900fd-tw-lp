.class public interface abstract Lcom/samsung/dcm/documents/IContainingEventData;
.super Ljava/lang/Object;
.source "IContainingEventData.java"


# virtual methods
.method public abstract getEvents()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setEvents(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/dcm/documents/EventData;",
            ">;)V"
        }
    .end annotation
.end method
