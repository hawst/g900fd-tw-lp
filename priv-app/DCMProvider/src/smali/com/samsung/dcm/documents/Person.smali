.class public Lcom/samsung/dcm/documents/Person;
.super Ljava/lang/Object;
.source "Person.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/Person;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mDisplayName:Ljava/lang/String;

.field public final mLookupUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/samsung/dcm/documents/Person$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/Person$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/Person;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "lookupUri"    # Ljava/lang/String;
    .param p2, "displayName"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 89
    if-eqz p1, :cond_0

    move v1, v2

    :goto_0
    instance-of v4, p1, Lcom/samsung/dcm/documents/Person;

    and-int/2addr v1, v4

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 90
    check-cast v0, Lcom/samsung/dcm/documents/Person;

    .line 92
    .local v0, "p":Lcom/samsung/dcm/documents/Person;
    iget-object v1, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    .end local v0    # "p":Lcom/samsung/dcm/documents/Person;
    :goto_1
    return v2

    :cond_0
    move v1, v3

    .line 89
    goto :goto_0

    .restart local v0    # "p":Lcom/samsung/dcm/documents/Person;
    :cond_1
    move v2, v3

    .line 92
    goto :goto_1

    .end local v0    # "p":Lcom/samsung/dcm/documents/Person;
    :cond_2
    move v2, v3

    .line 96
    goto :goto_1
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getLookupUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 101
    const/16 v1, 0x1c4b

    .line 102
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 104
    .local v0, "multipNeutralElem":I
    iget-object v2, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_0
    mul-int/lit16 v2, v2, 0x1c4b

    iget-object v4, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    :cond_0
    add-int/2addr v2, v3

    return v2

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/dcm/documents/Person;->mLookupUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/dcm/documents/Person;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    return-void
.end method
