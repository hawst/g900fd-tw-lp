.class public Lcom/samsung/dcm/documents/LocationData;
.super Ljava/lang/Object;
.source "LocationData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/dcm/documents/LocationData;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_COUNT:I = 0x6

.field private static final INDEX_ADMIN_AREA:I = 0x1

.field private static final INDEX_COUNTRY_NAME:I = 0x0

.field private static final INDEX_LOCALITY:I = 0x2

.field private static final INDEX_POSTAL_CODE:I = 0x4

.field private static final INDEX_STREET_NAME:I = 0x3

.field private static final INDEX_STREET_NUMBER:I = 0x5

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdminArea:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCountryName:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLocality:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPostalCode:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStreetName:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStreetNumber:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/dcm/documents/LocationData$1;

    invoke-direct {v0}, Lcom/samsung/dcm/documents/LocationData$1;-><init>()V

    sput-object v0, Lcom/samsung/dcm/documents/LocationData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 49
    const-class v0, Lcom/samsung/dcm/documents/LocationData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/dcm/documents/LocationData;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    .line 35
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    .line 36
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    .line 37
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    .line 38
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    .line 39
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x6

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    .line 35
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    .line 36
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    .line 37
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    .line 38
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    .line 39
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    .line 56
    new-array v1, v4, [Z

    .line 58
    .local v1, "presenceArray":[Z
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 60
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 61
    aget-boolean v2, v1, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/samsung/dcm/documents/LocationData;->set(ILjava/lang/String;)V

    .line 60
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_1
    return-void
.end method

.method public static fromCategories(Ljava/lang/String;)Lcom/samsung/dcm/documents/LocationData;
    .locals 7
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 391
    const/4 v0, 0x0

    .line 393
    .local v0, "index":I
    new-instance v2, Lcom/samsung/dcm/documents/LocationData;

    invoke-direct {v2}, Lcom/samsung/dcm/documents/LocationData;-><init>()V

    .line 395
    .local v2, "location":Lcom/samsung/dcm/documents/LocationData;
    const-string v5, "root_named_location"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 398
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, ";"

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 399
    .local v4, "tokenizer":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 400
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 401
    .local v3, "s":Ljava/lang/String;
    const-string v5, "root_named_location"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 403
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .local v1, "index":I
    invoke-virtual {v2, v0, v3}, Lcom/samsung/dcm/documents/LocationData;->set(ILjava/lang/String;)V

    move v0, v1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    goto :goto_0

    .line 407
    .end local v3    # "s":Ljava/lang/String;
    .end local v4    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_1
    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 324
    if-ne p0, p1, :cond_1

    .line 361
    :cond_0
    :goto_0
    return v1

    .line 326
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 327
    goto :goto_0

    .line 328
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 329
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 330
    check-cast v0, Lcom/samsung/dcm/documents/LocationData;

    .line 331
    .local v0, "other":Lcom/samsung/dcm/documents/LocationData;
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    if-nez v3, :cond_4

    .line 332
    iget-object v3, v0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    if-eqz v3, :cond_5

    move v1, v2

    .line 333
    goto :goto_0

    .line 334
    :cond_4
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    iget-object v4, v0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    invoke-virtual {v3, v4}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 335
    goto :goto_0

    .line 336
    :cond_5
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    if-nez v3, :cond_6

    .line 337
    iget-object v3, v0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    if-eqz v3, :cond_7

    move v1, v2

    .line 338
    goto :goto_0

    .line 339
    :cond_6
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    iget-object v4, v0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    invoke-virtual {v3, v4}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 340
    goto :goto_0

    .line 341
    :cond_7
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    if-nez v3, :cond_8

    .line 342
    iget-object v3, v0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    if-eqz v3, :cond_9

    move v1, v2

    .line 343
    goto :goto_0

    .line 344
    :cond_8
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    iget-object v4, v0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    invoke-virtual {v3, v4}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 345
    goto :goto_0

    .line 346
    :cond_9
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    if-nez v3, :cond_a

    .line 347
    iget-object v3, v0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    if-eqz v3, :cond_b

    move v1, v2

    .line 348
    goto :goto_0

    .line 349
    :cond_a
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    iget-object v4, v0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    invoke-virtual {v3, v4}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 350
    goto :goto_0

    .line 351
    :cond_b
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    if-nez v3, :cond_c

    .line 352
    iget-object v3, v0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    if-eqz v3, :cond_d

    move v1, v2

    .line 353
    goto :goto_0

    .line 354
    :cond_c
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    iget-object v4, v0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    invoke-virtual {v3, v4}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 355
    goto/16 :goto_0

    .line 356
    :cond_d
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    if-nez v3, :cond_e

    .line 357
    iget-object v3, v0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    if-eqz v3, :cond_0

    move v1, v2

    .line 358
    goto/16 :goto_0

    .line 359
    :cond_e
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    iget-object v4, v0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    invoke-virtual {v3, v4}, Lcom/google/common/base/Optional;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 360
    goto/16 :goto_0
.end method

.method public get(I)Lcom/google/common/base/Optional;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    packed-switch p1, :pswitch_data_0

    .line 292
    sget-object v0, Lcom/samsung/dcm/documents/LocationData;->TAG:Ljava/lang/String;

    const-string v1, "Should never reach this line"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/commons/Log;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    :goto_0
    return-object v0

    .line 274
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 277
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 280
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 283
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 286
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 289
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public getAdminArea()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getCategories()Ljava/lang/String;
    .locals 4

    .prologue
    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    .local v1, "s":Ljava/lang/StringBuilder;
    const-string v3, "root_named_location"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x6

    if-ge v0, v3, :cond_0

    .line 376
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/documents/LocationData;->get(I)Lcom/google/common/base/Optional;

    move-result-object v2

    .line 377
    .local v2, "v":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 378
    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 386
    .end local v2    # "v":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getCountryName()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getLocality()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getPostalCode()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getStreetName()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public getStreetNumber()Lcom/google/common/base/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 300
    const/16 v0, 0x1f

    .line 301
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 302
    .local v1, "result":I
    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 303
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 304
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 305
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 306
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 307
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    if-nez v4, :cond_5

    :goto_5
    add-int v1, v2, v3

    .line 308
    return v1

    .line 302
    :cond_0
    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->hashCode()I

    move-result v2

    goto :goto_0

    .line 303
    :cond_1
    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->hashCode()I

    move-result v2

    goto :goto_1

    .line 304
    :cond_2
    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->hashCode()I

    move-result v2

    goto :goto_2

    .line 305
    :cond_3
    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->hashCode()I

    move-result v2

    goto :goto_3

    .line 306
    :cond_4
    iget-object v2, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->hashCode()I

    move-result v2

    goto :goto_4

    .line 307
    :cond_5
    iget-object v3, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    invoke-virtual {v3}, Lcom/google/common/base/Optional;->hashCode()I

    move-result v3

    goto :goto_5
.end method

.method public set(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 237
    packed-switch p1, :pswitch_data_0

    .line 263
    :goto_0
    return-void

    .line 239
    :pswitch_0
    invoke-static {p2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 243
    :pswitch_1
    invoke-static {p2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 247
    :pswitch_2
    invoke-static {p2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 251
    :pswitch_3
    invoke-static {p2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 255
    :pswitch_4
    invoke-static {p2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 259
    :pswitch_5
    invoke-static {p2}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public setAdminArea(Ljava/lang/String;)V
    .locals 1
    .param p1, "adminArea"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mAdminArea:Lcom/google/common/base/Optional;

    .line 123
    return-void
.end method

.method public setCountryName(Ljava/lang/String;)V
    .locals 1
    .param p1, "countryName"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mCountryName:Lcom/google/common/base/Optional;

    .line 105
    return-void
.end method

.method public setLocality(Ljava/lang/String;)V
    .locals 1
    .param p1, "locality"    # Ljava/lang/String;

    .prologue
    .line 140
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    .line 141
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 1
    .param p1, "postalCode"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mPostalCode:Lcom/google/common/base/Optional;

    .line 175
    return-void
.end method

.method public setStreetName(Ljava/lang/String;)V
    .locals 1
    .param p1, "streetName"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetName:Lcom/google/common/base/Optional;

    .line 157
    return-void
.end method

.method public setStreetNumber(Ljava/lang/String;)V
    .locals 1
    .param p1, "mStreetNumber"    # Ljava/lang/String;

    .prologue
    .line 192
    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/documents/LocationData;->mStreetNumber:Lcom/google/common/base/Optional;

    .line 193
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 197
    const-string v0, "LocationData"

    .line 198
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/samsung/dcm/documents/LocationData;->mLocality:Lcom/google/common/base/Optional;

    invoke-virtual {v1}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 201
    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v3, 0x6

    .line 69
    new-array v1, v3, [Z

    .line 71
    .local v1, "presenceArray":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 72
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/documents/LocationData;->get(I)Lcom/google/common/base/Optional;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->isPresent()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 77
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    .line 78
    aget-boolean v2, v1, v0

    if-eqz v2, :cond_1

    .line 79
    invoke-virtual {p0, v0}, Lcom/samsung/dcm/documents/LocationData;->get(I)Lcom/google/common/base/Optional;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/base/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 82
    :cond_2
    return-void
.end method
