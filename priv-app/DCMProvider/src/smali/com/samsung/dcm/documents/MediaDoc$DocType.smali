.class public final enum Lcom/samsung/dcm/documents/MediaDoc$DocType;
.super Ljava/lang/Enum;
.source "MediaDoc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/dcm/documents/MediaDoc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DocType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/dcm/documents/MediaDoc$DocType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field public static final enum AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field public static final enum IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field public static final enum MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

.field public static final enum VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49
    new-instance v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    const-string v1, "MEDIA_DOC"

    const-string v2, "media_doc"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 50
    new-instance v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    const-string v1, "IMAGE"

    const-string v2, "image"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 51
    new-instance v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    const-string v1, "AUDIO"

    const-string v2, "audio"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 52
    new-instance v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    const-string v1, "VIDEO"

    const-string/jumbo v2, "video"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/dcm/documents/MediaDoc$DocType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/dcm/documents/MediaDoc$DocType;

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->MEDIA_DOC:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->IMAGE:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->AUDIO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/dcm/documents/MediaDoc$DocType;->VIDEO:Lcom/samsung/dcm/documents/MediaDoc$DocType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->$VALUES:[Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput-object p3, p0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->mName:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/dcm/documents/MediaDoc$DocType;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->$VALUES:[Lcom/samsung/dcm/documents/MediaDoc$DocType;

    invoke-virtual {v0}, [Lcom/samsung/dcm/documents/MediaDoc$DocType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/dcm/documents/MediaDoc$DocType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/dcm/documents/MediaDoc$DocType;->mName:Ljava/lang/String;

    return-object v0
.end method
