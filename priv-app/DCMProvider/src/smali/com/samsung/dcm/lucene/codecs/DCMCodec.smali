.class public Lcom/samsung/dcm/lucene/codecs/DCMCodec;
.super Lorg/apache/lucene/codecs/Codec;
.source "DCMCodec.java"


# instance fields
.field private final defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

.field private fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

.field private final infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

.field private final liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

.field private final normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

.field private final postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    const-string v0, "DCMCodec"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 40
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42TermVectorsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42TermVectorsFormat;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    .line 41
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    .line 42
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    .line 43
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    .line 45
    new-instance v0, Lcom/samsung/dcm/lucene/codecs/DCMCodec$1;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/lucene/codecs/DCMCodec$1;-><init>(Lcom/samsung/dcm/lucene/codecs/DCMCodec;)V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 52
    new-instance v0, Lcom/samsung/dcm/lucene/codecs/DCMCodec$2;

    invoke-direct {v0, p0}, Lcom/samsung/dcm/lucene/codecs/DCMCodec$2;-><init>(Lcom/samsung/dcm/lucene/codecs/DCMCodec;)V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 59
    const-string v0, "Lucene41"

    invoke-static {v0}, Lorg/apache/lucene/codecs/PostingsFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 60
    const-string v0, "Lucene42"

    invoke-static {v0}, Lorg/apache/lucene/codecs/DocValuesFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 62
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42NormsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42NormsFormat;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    .line 75
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsFormat;-><init>()V

    iput-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 93
    return-void
.end method


# virtual methods
.method public final docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public final fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    return-object v0
.end method

.method public getDocValuesFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public getPostingsFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    return-object v0
.end method

.method public final normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    return-object v0
.end method

.method public final postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    return-object v0
.end method

.method public final storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    return-object v0
.end method

.method public final termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/dcm/lucene/codecs/DCMCodec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    return-object v0
.end method
