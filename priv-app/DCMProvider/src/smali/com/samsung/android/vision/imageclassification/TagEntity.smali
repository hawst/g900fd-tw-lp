.class public Lcom/samsung/android/vision/imageclassification/TagEntity;
.super Ljava/lang/Object;
.source "TagEntity.java"


# instance fields
.field private label:Ljava/lang/String;

.field private score:F

.field private subTagEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;FLjava/util/List;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "score"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p3, "subTagEntities":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/android/vision/imageclassification/TagEntity;->label:Ljava/lang/String;

    .line 28
    iput p2, p0, Lcom/samsung/android/vision/imageclassification/TagEntity;->score:F

    .line 29
    iput-object p3, p0, Lcom/samsung/android/vision/imageclassification/TagEntity;->subTagEntities:Ljava/util/List;

    .line 30
    return-void
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/vision/imageclassification/TagEntity;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()F
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/vision/imageclassification/TagEntity;->score:F

    return v0
.end method

.method public getSubTagEntities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/vision/imageclassification/TagEntity;->subTagEntities:Ljava/util/List;

    return-object v0
.end method
