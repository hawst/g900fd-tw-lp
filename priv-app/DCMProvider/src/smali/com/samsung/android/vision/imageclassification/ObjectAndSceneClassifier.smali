.class public Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;
.super Ljava/lang/Object;
.source "ObjectAndSceneClassifier.java"

# interfaces
.implements Lcom/samsung/android/vision/imageclassification/IImageClassifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    }
.end annotation


# static fields
.field private static final CATEGORIES_TABLE:[[Ljava/lang/String;

.field private static final CLASS_MAX_NUM:I = 0x1a

.field private static lock:Ljava/lang/Object;

.field private static properEnginePath:Ljava/lang/String;

.field private static volatile refCount:I

.field private static volatile uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    :try_start_0
    const-string v0, "ObjectAndSceneClassification"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    const/16 v0, 0x1a

    new-array v0, v0, [[Ljava/lang/String;

    .line 31
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "bike"

    aput-object v2, v1, v4

    const-string v2, "Vehicles"

    aput-object v2, v1, v5

    const-string v2, "Bikes"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    .line 32
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "car"

    aput-object v2, v1, v4

    const-string v2, "Vehicles"

    aput-object v2, v1, v5

    const-string v2, "Cars"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    .line 33
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "aeroplane"

    aput-object v2, v1, v4

    const-string v2, "Vehicles"

    aput-object v2, v1, v5

    const-string v2, "Airplanes"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    .line 34
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "train"

    aput-object v2, v1, v4

    const-string v2, "Vehicles"

    aput-object v2, v1, v5

    const-string v2, "Trains"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const/4 v1, 0x4

    .line 35
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "boat"

    aput-object v3, v2, v4

    const-string v3, "Vehicles"

    aput-object v3, v2, v5

    const-string v3, "Boats"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 36
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "document"

    aput-object v3, v2, v4

    const-string v3, "Documents"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 37
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "flower"

    aput-object v3, v2, v4

    const-string v3, "Flower"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 38
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "food"

    aput-object v3, v2, v4

    const-string v3, "Food"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 39
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "cat"

    aput-object v3, v2, v4

    const-string v3, "Pets"

    aput-object v3, v2, v5

    const-string v3, "Cats"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 40
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "dog"

    aput-object v3, v2, v4

    const-string v3, "Pets"

    aput-object v3, v2, v5

    const-string v3, "Dogs"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 41
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "person"

    aput-object v3, v2, v4

    const-string v3, "People"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 42
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "coast"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Coast"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 43
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "forest"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Forest"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 44
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "mountain"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Mountain"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 45
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "nightscene"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Night scene"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 46
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "opencountry"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Open country"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 47
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "building"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Building"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 48
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "snow"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Snow"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 49
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "street"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Street"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 50
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "sunset"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 51
    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "waterfall"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "Waterfall"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 52
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "cityoverlook"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, "City overlook"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 53
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "desert"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 54
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "scenery"

    aput-object v3, v2, v4

    const-string v3, "Scenery"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 55
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "pet"

    aput-object v3, v2, v4

    const-string v3, "Pets"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 56
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "vehicle"

    aput-object v3, v2, v4

    const-string v3, "Vehicles"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    .line 29
    sput-object v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    .line 63
    sput-object v8, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    .line 68
    sput-object v8, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->properEnginePath:Ljava/lang/String;

    .line 69
    sput v4, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->lock:Ljava/lang/Object;

    return-void

    .line 17
    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method

.method private native SCExit()I
.end method

.method private native SCGetModelVersion()Ljava/lang/String;
.end method

.method private static native SCInit(Landroid/content/res/AssetManager;Ljava/lang/String;)I
.end method

.method private native SCProcess([BIII[Ljava/lang/String;)I
.end method

.method private native SCProcessJpeg([Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native SCconvertARGBToGray([B[III)I
.end method

.method private convertTagEntityArrayFromCandidatedTagEntityArray(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449
    .local p1, "internalTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 450
    .local v2, "returnTagArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    const/4 v1, 0x0

    .line 452
    .local v1, "mainTagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 459
    return-object v2

    .line 454
    :cond_0
    new-instance v1, Lcom/samsung/android/vision/imageclassification/TagEntity;

    .end local v1    # "mainTagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    invoke-virtual {v3}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    invoke-virtual {v3}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getScore()F

    move-result v5

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    invoke-virtual {v3}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getSubTagEntityArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, v4, v5, v3}, Lcom/samsung/android/vision/imageclassification/TagEntity;-><init>(Ljava/lang/String;FLjava/util/List;)V

    .line 455
    .restart local v1    # "mainTagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/vision/imageclassification/TagEntity;->getLabel()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unclassified"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 452
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 457
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private getCandidatedMainTagEntityFromCandidatedMainTagArray(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    .locals 3
    .param p2, "mainCategoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;"
        }
    .end annotation

    .prologue
    .line 508
    .local p1, "candidatedReturningTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;>;"
    const/4 v1, 0x0

    .line 509
    .local v1, "returnMainTagEntity":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 517
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 511
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "returnMainTagEntity":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    check-cast v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    .line 512
    .restart local v1    # "returnMainTagEntity":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    invoke-virtual {v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    move-object v2, v1

    .line 514
    goto :goto_1

    .line 509
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;
    .locals 5
    .param p0, "assetManager"    # Landroid/content/res/AssetManager;
    .param p1, "modelDataDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 85
    sget-object v2, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 87
    :try_start_0
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-nez v1, :cond_3

    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-nez v1, :cond_3

    .line 90
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-nez v1, :cond_2

    .line 92
    const/4 v0, 0x0

    .line 93
    .local v0, "resultId":I
    invoke-static {p0, p1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCInit(Landroid/content/res/AssetManager;Ljava/lang/String;)I

    move-result v0

    .line 94
    if-ne v0, v3, :cond_0

    .line 96
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "ObjectAndSceneClassifier initialization is faild for illegal arguments"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    .end local v0    # "resultId":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 97
    .restart local v0    # "resultId":I
    :cond_0
    if-ne v0, v4, :cond_1

    .line 99
    :try_start_1
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;

    const-string v3, "ObjectAndSceneClassifier initialization is faild for internal operatations"

    invoke-direct {v1, v3}, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :cond_1
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    invoke-direct {v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;-><init>()V

    sput-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    .line 102
    sput-object p1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->properEnginePath:Ljava/lang/String;

    .line 103
    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    .line 85
    .end local v0    # "resultId":I
    :cond_2
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    return-object v1

    .line 106
    :cond_3
    :try_start_2
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-eqz v1, :cond_6

    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-nez v1, :cond_6

    .line 113
    const/4 v0, 0x0

    .line 114
    .restart local v0    # "resultId":I
    invoke-static {p0, p1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCInit(Landroid/content/res/AssetManager;Ljava/lang/String;)I

    move-result v0

    .line 115
    if-ne v0, v3, :cond_4

    .line 117
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "ObjectAndSceneClassifier initialization is faild for illegal arguments"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 118
    :cond_4
    if-ne v0, v4, :cond_5

    .line 120
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;

    const-string v3, "ObjectAndSceneClassifier initialization is faild for internal operatations"

    invoke-direct {v1, v3}, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 122
    :cond_5
    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    goto :goto_0

    .line 124
    .end local v0    # "resultId":I
    :cond_6
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-eqz v1, :cond_8

    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-eqz v1, :cond_8

    .line 126
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->properEnginePath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 128
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;

    const-string v3, "ObjectAndSceneClassifier initialization is faild for a previous instance, please check the previous used model data directory"

    invoke-direct {v1, v3}, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 132
    :cond_7
    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    goto :goto_0

    .line 135
    :cond_8
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-nez v1, :cond_9

    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-eqz v1, :cond_9

    .line 138
    const/4 v1, 0x0

    sput v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    .line 139
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-nez v1, :cond_2

    .line 141
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    invoke-direct {v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;-><init>()V

    sput-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    .line 142
    sput-object p1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->properEnginePath:Ljava/lang/String;

    .line 143
    sget v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    goto :goto_0

    .line 149
    :cond_9
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;

    const-string v3, "ObjectAndSceneClassifier initialization is faild for a previous instance, please check the previous used model data directory"

    invoke-direct {v1, v3}, Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private imageReadclassify(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 22
    .param p1, "imagePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;
        }
    .end annotation

    .prologue
    .line 198
    new-instance v18, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 199
    .local v18, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    move-object/from16 v0, v18

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 200
    const/4 v2, 0x0

    .line 201
    .local v2, "bmp":Landroid/graphics/Bitmap;
    const/16 v19, 0x0

    .line 203
    .local v19, "ratio":I
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 206
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lez v4, :cond_0

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gtz v4, :cond_1

    .line 208
    :cond_0
    new-instance v4, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;

    const-string v6, "A given image file format is invalid"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 211
    :cond_1
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/2addr v4, v6

    const/4 v6, 0x4

    if-gt v4, v6, :cond_2

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/2addr v4, v6

    const/4 v6, 0x4

    if-gt v4, v6, :cond_2

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/16 v6, 0x64

    if-lt v4, v6, :cond_2

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v6, 0x64

    if-ge v4, v6, :cond_5

    .line 213
    :cond_2
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v4, :cond_3

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v4, :cond_4

    .line 216
    :cond_3
    new-instance v4, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;

    const-string v6, "A given image format is not supported."

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 223
    :cond_4
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v21, "unClassifiedTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    new-instance v20, Lcom/samsung/android/vision/imageclassification/TagEntity;

    const-string v4, "Unclassified"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v6, v7}, Lcom/samsung/android/vision/imageclassification/TagEntity;-><init>(Ljava/lang/String;FLjava/util/List;)V

    .line 225
    .local v20, "tagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    .end local v20    # "tagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    .end local v21    # "unClassifiedTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :goto_0
    return-object v21

    .line 230
    :cond_5
    move-object/from16 v0, v18

    iget-object v4, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v6, "image/jpeg"

    invoke-virtual {v4, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_a

    .line 232
    const/16 v4, 0x72

    new-array v15, v4, [Ljava/lang/String;

    .line 233
    .local v15, "resultStr":[Ljava/lang/String;
    const/16 v17, 0x0

    .line 234
    .local v17, "errorCode":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCProcessJpeg([Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 235
    if-eqz v17, :cond_9

    .line 237
    const/4 v4, 0x1

    move/from16 v0, v17

    if-ne v0, v4, :cond_6

    .line 238
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v6, "Image classification processing is failed with illegal argument"

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 239
    :cond_6
    const/4 v4, 0x2

    move/from16 v0, v17

    if-ne v0, v4, :cond_7

    .line 240
    new-instance v4, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;

    const-string v6, "Image classification processing is failed with internal operation error"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 241
    :cond_7
    const/4 v4, 0x3

    move/from16 v0, v17

    if-ne v0, v4, :cond_8

    .line 242
    new-instance v4, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;

    const-string v6, "Image classification processing is failed with insufficient memory"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 243
    :cond_8
    const/4 v4, 0x4

    move/from16 v0, v17

    if-ne v0, v4, :cond_9

    .line 245
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .restart local v21    # "unClassifiedTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    new-instance v20, Lcom/samsung/android/vision/imageclassification/TagEntity;

    const-string v4, "Unclassified"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v6, v7}, Lcom/samsung/android/vision/imageclassification/TagEntity;-><init>(Ljava/lang/String;FLjava/util/List;)V

    .line 247
    .restart local v20    # "tagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    .end local v20    # "tagEntity":Lcom/samsung/android/vision/imageclassification/TagEntity;
    .end local v21    # "unClassifiedTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->makeTagArray([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v21

    goto :goto_0

    .line 256
    .end local v15    # "resultStr":[Ljava/lang/String;
    .end local v17    # "errorCode":I
    :cond_a
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v6, 0x3c0

    if-lt v4, v6, :cond_b

    .line 258
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/lit16 v0, v4, 0x168

    move/from16 v19, v0

    .line 259
    const/4 v4, 0x2

    move/from16 v0, v19

    if-le v0, v4, :cond_c

    move/from16 v4, v19

    :goto_1
    move-object/from16 v0, v18

    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 261
    :cond_b
    const/4 v4, 0x0

    move-object/from16 v0, v18

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 265
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 271
    if-nez v2, :cond_d

    .line 273
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 259
    :cond_c
    const/4 v4, 0x2

    goto :goto_1

    .line 266
    :catch_0
    move-exception v16

    .line 268
    .local v16, "e":Ljava/lang/OutOfMemoryError;
    new-instance v4, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;

    const-string v6, "Image loading is failed with insufficient memory"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 276
    .end local v16    # "e":Ljava/lang/OutOfMemoryError;
    :cond_d
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/16 v6, 0x3c0

    if-lt v4, v6, :cond_e

    .line 280
    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int v4, v4, v19

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int v6, v6, v19

    const/4 v7, 0x0

    invoke-static {v2, v4, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 287
    :cond_e
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 288
    .local v5, "srcWidth":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 289
    .local v9, "srcHeight":I
    const/4 v11, 0x0

    .line 290
    .local v11, "grayImg":[B
    const/4 v3, 0x0

    .line 291
    .local v3, "argbImg":[I
    const/4 v15, 0x0

    .line 292
    .restart local v15    # "resultStr":[Ljava/lang/String;
    const/16 v17, 0x0

    .line 296
    .restart local v17    # "errorCode":I
    const/16 v4, 0x72

    :try_start_2
    new-array v15, v4, [Ljava/lang/String;

    .line 297
    mul-int v4, v5, v9

    new-array v11, v4, [B

    .line 298
    mul-int v4, v5, v9

    new-array v3, v4, [I

    .line 300
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, v5

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 301
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v5, v9}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCconvertARGBToGray([B[III)I
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 307
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    const/4 v14, 0x1

    move-object/from16 v10, p0

    invoke-direct/range {v10 .. v15}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCProcess([BIII[Ljava/lang/String;)I

    move-result v17

    .line 309
    if-eqz v17, :cond_11

    .line 311
    const/4 v4, 0x1

    move/from16 v0, v17

    if-ne v0, v4, :cond_f

    .line 312
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v6, "Image classification processing is failed with illegal argument"

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 281
    .end local v3    # "argbImg":[I
    .end local v5    # "srcWidth":I
    .end local v9    # "srcHeight":I
    .end local v11    # "grayImg":[B
    .end local v15    # "resultStr":[Ljava/lang/String;
    .end local v17    # "errorCode":I
    :catch_1
    move-exception v16

    .line 283
    .restart local v16    # "e":Ljava/lang/OutOfMemoryError;
    new-instance v4, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;

    const-string v6, "Image scaled loading is failed with insufficient memory"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 302
    .end local v16    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v3    # "argbImg":[I
    .restart local v5    # "srcWidth":I
    .restart local v9    # "srcHeight":I
    .restart local v11    # "grayImg":[B
    .restart local v15    # "resultStr":[Ljava/lang/String;
    .restart local v17    # "errorCode":I
    :catch_2
    move-exception v16

    .line 304
    .restart local v16    # "e":Ljava/lang/OutOfMemoryError;
    new-instance v4, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;

    const-string v6, "Allocating memory is failed with insufficient memory"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 313
    .end local v16    # "e":Ljava/lang/OutOfMemoryError;
    :cond_f
    const/4 v4, 0x2

    move/from16 v0, v17

    if-ne v0, v4, :cond_10

    .line 314
    new-instance v4, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;

    const-string v6, "Image classification processing is failed with internal operation error"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 315
    :cond_10
    const/4 v4, 0x3

    move/from16 v0, v17

    if-ne v0, v4, :cond_11

    .line 316
    new-instance v4, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;

    const-string v6, "Image classification processing is failed with insufficient memory"

    invoke-direct {v4, v6}, Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 319
    :cond_11
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->makeTagArray([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v21

    goto/16 :goto_0
.end method

.method private makeTagArray([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "rawTagString"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 327
    .local v0, "candidatedReturningTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;>;"
    const/4 v1, 0x0

    .line 328
    .local v1, "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    const/4 v5, 0x0

    .line 330
    .local v5, "subClass":Lcom/samsung/android/vision/imageclassification/TagEntity;
    const/4 v4, 0x0

    .local v4, "nClassIdx":I
    :goto_0
    const/16 v7, 0x1a

    if-lt v4, v7, :cond_1

    .line 444
    :cond_0
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->convertTagEntityArrayFromCandidatedTagEntityArray(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v7

    return-object v7

    .line 332
    :cond_1
    if-nez v4, :cond_2

    .line 334
    add-int/lit8 v7, v4, 0x1a

    aget-object v7, p1, v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    cmpg-float v7, v7, v11

    if-gtz v7, :cond_3

    .line 336
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    .end local v1    # "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    invoke-direct {v1, p0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;-><init>(Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;)V

    .line 337
    .restart local v1    # "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    const-string v7, "Unclassified"

    invoke-virtual {v1, v7}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setLabel(Ljava/lang/String;)V

    .line 338
    invoke-virtual {v1, v11}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setScore(F)V

    .line 339
    invoke-virtual {v1, v10}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setSubTagEntityArray(Ljava/util/ArrayList;)V

    .line 340
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 347
    :cond_2
    add-int/lit8 v7, v4, 0x1a

    aget-object v7, p1, v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    cmpg-float v7, v7, v11

    if-lez v7, :cond_0

    .line 352
    :cond_3
    const/4 v2, 0x0

    .line 353
    .local v2, "fScore":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    array-length v7, v7

    if-lt v3, v7, :cond_4

    .line 330
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 354
    :cond_4
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    const/4 v8, 0x0

    aget-object v7, v7, v8

    aget-object v8, p1, v4

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 355
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v9

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 359
    add-int/lit8 v7, v4, 0x1a

    aget-object v7, p1, v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 366
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v12

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 373
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v9

    invoke-direct {p0, v0, v7}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->getCandidatedMainTagEntityFromCandidatedMainTagArray(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    move-result-object v1

    .line 374
    if-nez v1, :cond_6

    .line 377
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 378
    .local v6, "subTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    new-instance v5, Lcom/samsung/android/vision/imageclassification/TagEntity;

    .end local v5    # "subClass":Lcom/samsung/android/vision/imageclassification/TagEntity;
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v12

    invoke-direct {v5, v7, v2, v10}, Lcom/samsung/android/vision/imageclassification/TagEntity;-><init>(Ljava/lang/String;FLjava/util/List;)V

    .line 379
    .restart local v5    # "subClass":Lcom/samsung/android/vision/imageclassification/TagEntity;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    .end local v1    # "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    invoke-direct {v1, p0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;-><init>(Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;)V

    .line 381
    .restart local v1    # "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v9

    invoke-virtual {v1, v7}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setLabel(Ljava/lang/String;)V

    .line 382
    invoke-virtual {v1, v2}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setScore(F)V

    .line 383
    invoke-virtual {v1, v6}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setSubTagEntityArray(Ljava/util/ArrayList;)V

    .line 384
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    .end local v6    # "subTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :cond_5
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 389
    :cond_6
    new-instance v5, Lcom/samsung/android/vision/imageclassification/TagEntity;

    .end local v5    # "subClass":Lcom/samsung/android/vision/imageclassification/TagEntity;
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v12

    invoke-direct {v5, v7, v2, v10}, Lcom/samsung/android/vision/imageclassification/TagEntity;-><init>(Ljava/lang/String;FLjava/util/List;)V

    .line 390
    .restart local v5    # "subClass":Lcom/samsung/android/vision/imageclassification/TagEntity;
    invoke-virtual {v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getSubTagEntityArray()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 394
    invoke-virtual {v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getSubTagEntityArray()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 399
    :cond_7
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 400
    .restart local v6    # "subTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    invoke-virtual {v1, v6}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setSubTagEntityArray(Ljava/util/ArrayList;)V

    goto :goto_3

    .line 415
    .end local v6    # "subTagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :cond_8
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v9

    invoke-direct {p0, v0, v7}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->getCandidatedMainTagEntityFromCandidatedMainTagArray(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    move-result-object v1

    .line 416
    if-nez v1, :cond_9

    .line 419
    new-instance v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;

    .end local v1    # "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    invoke-direct {v1, p0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;-><init>(Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;)V

    .line 420
    .restart local v1    # "existedMainClass":Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
    sget-object v7, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->CATEGORIES_TABLE:[[Ljava/lang/String;

    aget-object v7, v7, v3

    aget-object v7, v7, v9

    invoke-virtual {v1, v7}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setLabel(Ljava/lang/String;)V

    .line 421
    invoke-virtual {v1, v2}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setScore(F)V

    .line 422
    invoke-virtual {v1, v10}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setSubTagEntityArray(Ljava/util/ArrayList;)V

    .line 423
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 428
    :cond_9
    invoke-virtual {v1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->getScore()F

    move-result v7

    cmpg-float v7, v7, v2

    if-gez v7, :cond_5

    .line 430
    invoke-virtual {v1, v2}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->setScore(F)V

    goto :goto_3
.end method


# virtual methods
.method public classify(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "imagePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 159
    sget-object v3, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 161
    :try_start_0
    sget v2, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-nez v2, :cond_0

    .line 162
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "The instance is in an invalid state"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 159
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    const/4 v1, 0x0

    .line 181
    .local v1, "tagArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :try_start_2
    invoke-direct {p0, p1}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->imageReadclassify(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_2
    .catch Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/vision/imageclassification/OutOfMemoryException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 182
    return-object v1

    .line 184
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;
    throw v0

    .line 187
    .end local v0    # "e":Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    throw v0

    .line 190
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 192
    .local v0, "e":Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;
    throw v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 550
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 553
    :try_start_0
    sget-object v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->uniqueObjectAndSceneClassifierInstance:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    if-eqz v0, :cond_0

    .line 555
    invoke-virtual {p0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->release()V

    .line 550
    :cond_0
    monitor-exit v1

    .line 558
    return-void

    .line 550
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getModelVersion()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    sget v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "The instance is in an invalid state"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 79
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCGetModelVersion()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 522
    sget-object v1, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 524
    :try_start_0
    sget v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-lez v0, :cond_1

    .line 526
    sget v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    .line 527
    sget v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-nez v0, :cond_0

    .line 529
    invoke-direct {p0}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->SCExit()I

    .line 522
    :cond_0
    :goto_0
    monitor-exit v1

    .line 546
    return-void

    .line 539
    :cond_1
    sget v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    if-gtz v0, :cond_0

    .line 541
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->refCount:I

    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
