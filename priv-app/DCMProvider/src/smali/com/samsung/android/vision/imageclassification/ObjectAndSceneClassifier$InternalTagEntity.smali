.class Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;
.super Ljava/lang/Object;
.source "ObjectAndSceneClassifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalTagEntity"
.end annotation


# instance fields
.field label:Ljava/lang/String;

.field score:F

.field subTagEntityArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;


# direct methods
.method constructor <init>(Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 469
    iput-object p1, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->this$0:Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470
    iput-object v1, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->label:Ljava/lang/String;

    .line 471
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->score:F

    .line 472
    iput-object v1, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->subTagEntityArray:Ljava/util/ArrayList;

    .line 473
    return-void
.end method


# virtual methods
.method getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->label:Ljava/lang/String;

    return-object v0
.end method

.method getScore()F
    .locals 1

    .prologue
    .line 497
    iget v0, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->score:F

    return v0
.end method

.method getSubTagEntityArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    iget-object v0, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->subTagEntityArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 477
    iput-object p1, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->label:Ljava/lang/String;

    .line 478
    return-void
.end method

.method setScore(F)V
    .locals 0
    .param p1, "score"    # F

    .prologue
    .line 482
    iput p1, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->score:F

    .line 483
    return-void
.end method

.method setSubTagEntityArray(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 487
    .local p1, "tagEntityArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    iput-object p1, p0, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier$InternalTagEntity;->subTagEntityArray:Ljava/util/ArrayList;

    .line 488
    return-void
.end method
