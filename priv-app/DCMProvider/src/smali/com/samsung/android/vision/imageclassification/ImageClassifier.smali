.class public Lcom/samsung/android/vision/imageclassification/ImageClassifier;
.super Ljava/lang/Object;
.source "ImageClassifier.java"


# static fields
.field public static final MODEL_OBJECT_AND_SCENE:Ljava/lang/String; = "ObjectAndScene"

.field public static final TAG:Ljava/lang/String; = "ImageClassification"


# instance fields
.field private iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;


# direct methods
.method private constructor <init>(Lcom/samsung/android/vision/imageclassification/IImageClassifier;)V
    .locals 0
    .param p1, "iImageClassifier"    # Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    .line 47
    return-void
.end method

.method public static createInstance(Ljava/lang/String;Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ImageClassifier;
    .locals 4
    .param p0, "modelName"    # Ljava/lang/String;
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "modelDataDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;,
            Lcom/samsung/android/vision/imageclassification/ModelNotFoundException;
        }
    .end annotation

    .prologue
    .line 72
    const-string v2, "ImageClassification"

    const-string v3, "createInstance enter"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    if-nez p1, :cond_0

    .line 74
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "An aseet manager is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 75
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "A string of a given model name is null or empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 77
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 78
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "A string of a given model directory is null or empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 87
    :cond_4
    const-string v2, "ObjectAndScene"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 88
    new-instance v2, Lcom/samsung/android/vision/imageclassification/ModelNotFoundException;

    const-string v3, "A given model is not found."

    invoke-direct {v2, v3}, Lcom/samsung/android/vision/imageclassification/ModelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 92
    :cond_5
    const/4 v1, 0x0

    .line 93
    .local v1, "iImageClassifier":Lcom/samsung/android/vision/imageclassification/IImageClassifier;
    :try_start_0
    invoke-static {p1, p2}, Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;->getInstance(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/samsung/android/vision/imageclassification/ObjectAndSceneClassifier;

    move-result-object v1

    .line 94
    const-string v2, "ImageClassification"

    const-string v3, "createInstance exit"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v2, Lcom/samsung/android/vision/imageclassification/ImageClassifier;

    invoke-direct {v2, v1}, Lcom/samsung/android/vision/imageclassification/ImageClassifier;-><init>(Lcom/samsung/android/vision/imageclassification/IImageClassifier;)V
    :try_end_0
    .catch Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v2

    .line 97
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;
    throw v0

    .line 100
    .end local v0    # "e":Lcom/samsung/android/vision/imageclassification/ModelLoadingFailedException;
    :catch_1
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    throw v0
.end method


# virtual methods
.method public classify(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "imagePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/FileNotFoundException;,
            Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;,
            Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 132
    const-string v3, "ImageClassification"

    const-string v4, "classify enter"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v3, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    if-nez v3, :cond_0

    .line 134
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The instance is in an invalid state"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 135
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 136
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "A string of a given image file is null or empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 138
    :cond_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 139
    .local v0, "checkFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v3

    if-nez v3, :cond_4

    .line 141
    :cond_3
    new-instance v3, Ljava/io/FileNotFoundException;

    const-string v4, "imagePath is not existed or imagePath is an invalid file"

    invoke-direct {v3, v4}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 146
    :cond_4
    const/4 v2, 0x0

    .line 147
    .local v2, "tagEntitiesArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/vision/imageclassification/TagEntity;>;"
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    invoke-interface {v3, p1}, Lcom/samsung/android/vision/imageclassification/IImageClassifier;->classify(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 148
    const-string v3, "ImageClassification"

    const-string v4, "classify exit"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/vision/imageclassification/OutOfMemoryException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 149
    return-object v2

    .line 151
    :catch_0
    move-exception v1

    .line 153
    .local v1, "e":Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;
    throw v1

    .line 154
    .end local v1    # "e":Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;
    :catch_1
    move-exception v1

    .line 156
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    throw v1

    .line 157
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 159
    .local v1, "e":Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;
    throw v1

    .line 160
    .end local v1    # "e":Lcom/samsung/android/vision/imageclassification/OutOfMemoryException;
    :catch_3
    move-exception v1

    .line 162
    .local v1, "e":Ljava/lang/IllegalStateException;
    throw v1
.end method

.method public getModelVersion()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 177
    const-string v1, "ImageClassification"

    const-string v2, "getModelVersion enter"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v1, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    if-nez v1, :cond_0

    .line 179
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The instance is in an invalid state"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :cond_0
    :try_start_0
    const-string v1, "ImageClassification"

    const-string v2, "getModelVersion exit"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v1, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    invoke-interface {v1}, Lcom/samsung/android/vision/imageclassification/IImageClassifier;->getModelVersion()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 183
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/IllegalStateException;
    throw v0
.end method

.method public release()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    if-eqz v0, :cond_0

    .line 201
    const-string v0, "ImageClassification"

    const-string v1, "release enter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v0, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    invoke-interface {v0}, Lcom/samsung/android/vision/imageclassification/IImageClassifier;->release()V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/vision/imageclassification/ImageClassifier;->iImageClassifier:Lcom/samsung/android/vision/imageclassification/IImageClassifier;

    .line 204
    const-string v0, "ImageClassification"

    const-string v1, "release exit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_0
    return-void
.end method
