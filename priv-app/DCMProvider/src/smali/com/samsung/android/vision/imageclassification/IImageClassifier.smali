.class public interface abstract Lcom/samsung/android/vision/imageclassification/IImageClassifier;
.super Ljava/lang/Object;
.source "IImageClassifier.java"


# virtual methods
.method public abstract classify(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/vision/imageclassification/TagEntity;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/vision/imageclassification/UnsupportedFormatException;,
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getModelVersion()Ljava/lang/String;
.end method

.method public abstract release()V
.end method
