.class public Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
.super Lcom/spatial4j/core/shape/impl/Range;
.source "Range.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/spatial4j/core/shape/impl/Range;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LongitudeRange"
.end annotation


# static fields
.field public static final WORLD_180E180W:Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 109
    new-instance v0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    const-wide v2, -0x3f99800000000000L    # -180.0

    const-wide v4, 0x4066800000000000L    # 180.0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;-><init>(DD)V

    sput-object v0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->WORLD_180E180W:Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "min"    # D
    .param p3, "max"    # D

    .prologue
    .line 112
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/spatial4j/core/shape/impl/Range;-><init>(DD)V

    .line 113
    return-void
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Rectangle;)V
    .locals 4
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 116
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/spatial4j/core/shape/impl/Range;-><init>(DD)V

    .line 117
    return-void
.end method

.method private static diff(DD)D
    .locals 6
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    const-wide v4, 0x4076800000000000L    # 360.0

    .line 151
    sub-double v0, p0, p2

    .line 152
    .local v0, "diff":D
    const-wide v2, 0x4066800000000000L    # 180.0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_1

    .line 153
    const-wide v2, -0x3f99800000000000L    # -180.0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    .line 157
    .end local v0    # "diff":D
    :goto_0
    return-wide v0

    .line 155
    .restart local v0    # "diff":D
    :cond_0
    add-double/2addr v0, v4

    goto :goto_0

    .line 157
    :cond_1
    sub-double/2addr v0, v4

    goto :goto_0
.end method


# virtual methods
.method public compareTo(Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;)D
    .locals 4
    .param p1, "b"    # Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->getCenter()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->getCenter()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->diff(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public contains(D)Z
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->crossesDateline()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    invoke-super {p0, p1, p2}, Lcom/spatial4j/core/shape/impl/Range;->contains(D)Z

    move-result v0

    .line 131
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->min:D

    cmpl-double v0, p1, v0

    if-gez v0, :cond_1

    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->max:D

    cmpg-double v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public crossesDateline()Z
    .locals 4

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->min:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->max:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public expandTo(Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;)Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    .locals 10
    .param p1, "other"    # Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->compareTo(Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_0

    .line 169
    move-object v0, p0

    .line 170
    .local v0, "a":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    move-object v1, p1

    .line 175
    .local v1, "b":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :goto_0
    iget-wide v4, v0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->min:D

    invoke-virtual {v1, v4, v5}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->contains(D)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v3, v1

    .line 176
    .local v3, "newMin":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :goto_1
    iget-wide v4, v1, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->max:D

    invoke-virtual {v0, v4, v5}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->contains(D)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v2, v0

    .line 177
    .local v2, "newMax":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :goto_2
    if-ne v3, v2, :cond_3

    .line 181
    .end local v3    # "newMin":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :goto_3
    return-object v3

    .line 172
    .end local v0    # "a":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    .end local v1    # "b":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    .end local v2    # "newMax":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :cond_0
    move-object v0, p1

    .line 173
    .restart local v0    # "a":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    move-object v1, p0

    .restart local v1    # "b":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    goto :goto_0

    :cond_1
    move-object v3, v0

    .line 175
    goto :goto_1

    .restart local v3    # "newMin":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :cond_2
    move-object v2, v1

    .line 176
    goto :goto_2

    .line 179
    .restart local v2    # "newMax":Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    :cond_3
    if-ne v3, v1, :cond_4

    if-ne v2, v0, :cond_4

    .line 180
    sget-object v3, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->WORLD_180E180W:Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    goto :goto_3

    .line 181
    :cond_4
    new-instance v4, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    iget-wide v6, v3, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->min:D

    iget-wide v8, v2, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->max:D

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;-><init>(DD)V

    move-object v3, v4

    goto :goto_3
.end method

.method public expandTo(Lcom/spatial4j/core/shape/impl/Range;)Lcom/spatial4j/core/shape/impl/Range;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/impl/Range;

    .prologue
    .line 163
    check-cast p1, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    .end local p1    # "other":Lcom/spatial4j/core/shape/impl/Range;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;->expandTo(Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;)Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    move-result-object v0

    return-object v0
.end method

.method public getCenter()D
    .locals 4

    .prologue
    .line 139
    invoke-super {p0}, Lcom/spatial4j/core/shape/impl/Range;->getCenter()D

    move-result-wide v0

    .line 140
    .local v0, "ctr":D
    const-wide v2, 0x4066800000000000L    # 180.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 141
    const-wide v2, 0x4076800000000000L    # 360.0

    sub-double/2addr v0, v2

    .line 142
    :cond_0
    return-wide v0
.end method

.method public getWidth()D
    .locals 4

    .prologue
    .line 121
    invoke-super {p0}, Lcom/spatial4j/core/shape/impl/Range;->getWidth()D

    move-result-wide v0

    .line 122
    .local v0, "w":D
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 123
    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    .line 124
    :cond_0
    return-wide v0
.end method
