.class public Lcom/spatial4j/core/shape/impl/GeoCircle;
.super Lcom/spatial4j/core/shape/impl/CircleImpl;
.source "GeoCircle.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private horizAxisY:D

.field private inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/spatial4j/core/shape/impl/GeoCircle;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V
    .locals 2
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "radiusDEG"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/spatial4j/core/shape/impl/CircleImpl;-><init>(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V

    .line 38
    sget-boolean v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p4}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_0
    invoke-direct {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->init()V

    .line 40
    return-void
.end method

.method private init()V
    .locals 15

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    const-wide v12, 0x4056800000000000L    # 90.0

    cmpl-double v0, v0, v12

    if-lez v0, :cond_3

    .line 51
    sget-boolean v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v0

    const-wide v12, 0x4076800000000000L    # 360.0

    cmpl-double v0, v0, v12

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_0
    const-wide v0, 0x4066800000000000L    # 180.0

    iget-wide v12, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    sub-double v10, v0, v12

    .line 53
    .local v10, "backDistDEG":D
    const-wide/16 v0, 0x0

    cmpl-double v0, v10, v0

    if-lez v0, :cond_2

    .line 54
    const-wide v0, 0x4066800000000000L    # 180.0

    iget-wide v12, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    sub-double v6, v0, v12

    .line 55
    .local v6, "backRadius":D
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    const-wide v12, 0x4066800000000000L    # 180.0

    add-double/2addr v0, v12

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v2

    .line 56
    .local v2, "backX":D
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    const-wide v12, 0x4066800000000000L    # 180.0

    add-double/2addr v0, v12

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->normLatDEG(D)D

    move-result-wide v4

    .line 60
    .local v4, "backY":D
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    add-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->ulp(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    add-double/2addr v12, v6

    invoke-static {v12, v13}, Ljava/lang/Math;->ulp(D)D

    move-result-wide v12

    invoke-static {v0, v1, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    sub-double/2addr v6, v0

    .line 61
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    if-eqz v0, :cond_1

    .line 62
    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    invoke-virtual/range {v1 .. v7}, Lcom/spatial4j/core/shape/impl/GeoCircle;->reset(DDD)V

    .line 69
    .end local v2    # "backX":D
    .end local v4    # "backY":D
    .end local v6    # "backRadius":D
    :goto_0
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->horizAxisY:D

    .line 83
    .end local v10    # "backDistDEG":D
    :goto_1
    return-void

    .line 64
    .restart local v2    # "backX":D
    .restart local v4    # "backY":D
    .restart local v6    # "backRadius":D
    .restart local v10    # "backDistDEG":D
    :cond_1
    new-instance v0, Lcom/spatial4j/core/shape/impl/GeoCircle;

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    iget-object v12, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-direct {v0, v1, v6, v7, v12}, Lcom/spatial4j/core/shape/impl/GeoCircle;-><init>(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V

    iput-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    goto :goto_0

    .line 67
    .end local v2    # "backX":D
    .end local v4    # "backY":D
    .end local v6    # "backRadius":D
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    goto :goto_0

    .line 71
    .end local v10    # "backDistDEG":D
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    .line 72
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    iget-wide v12, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    iget-object v14, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-interface {v0, v1, v12, v13, v14}, Lcom/spatial4j/core/distance/DistanceCalculator;->calcBoxByDistFromPt_yHorizAxisDEG(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v8

    .line 74
    .local v8, "_horizAxisY":D
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v0

    cmpl-double v0, v8, v0

    if-lez v0, :cond_4

    .line 75
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->horizAxisY:D

    goto :goto_1

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v0

    cmpg-double v0, v8, v0

    if-gez v0, :cond_5

    .line 77
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->horizAxisY:D

    goto :goto_1

    .line 79
    :cond_5
    iput-wide v8, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->horizAxisY:D

    goto :goto_1
.end method

.method private numCornersIntersect(Lcom/spatial4j/core/shape/Rectangle;)I
    .locals 6
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    const/4 v1, 0x1

    .line 209
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/GeoCircle;->contains(DD)Z

    move-result v0

    .line 210
    .local v0, "bool":Z
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/GeoCircle;->contains(DD)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 211
    if-nez v0, :cond_2

    .line 231
    :cond_0
    :goto_0
    return v1

    .line 214
    :cond_1
    if-nez v0, :cond_0

    .line 217
    :cond_2
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/GeoCircle;->contains(DD)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 218
    if-eqz v0, :cond_0

    .line 224
    :cond_3
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/GeoCircle;->contains(DD)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 225
    if-eqz v0, :cond_0

    .line 231
    :cond_4
    if-eqz v0, :cond_7

    const/4 v1, 0x4

    goto :goto_0

    .line 221
    :cond_5
    if-eqz v0, :cond_3

    goto :goto_0

    .line 228
    :cond_6
    if-eqz v0, :cond_4

    goto :goto_0

    .line 231
    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private relateRectangleCircleWrapsPole(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 20
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 156
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    move-wide/from16 v16, v0

    const-wide v18, 0x4066800000000000L    # 180.0

    cmpl-double v5, v16, v18

    if-nez v5, :cond_0

    .line 157
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 201
    :goto_0
    return-object v5

    .line 160
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v5

    invoke-interface {v5}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    move-wide/from16 v18, v0

    add-double v12, v16, v18

    .line 161
    .local v12, "yTop":D
    const-wide v16, 0x4056800000000000L    # 90.0

    cmpl-double v5, v12, v16

    if-lez v5, :cond_2

    .line 162
    const-wide v16, 0x4056800000000000L    # 90.0

    sub-double v14, v12, v16

    .line 163
    .local v14, "yTopOverlap":D
    sget-boolean v5, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    const-wide v16, 0x4056800000000000L    # 90.0

    cmpg-double v5, v14, v16

    if-lez v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 164
    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v16

    const-wide v18, 0x4056800000000000L    # 90.0

    sub-double v18, v18, v14

    cmpl-double v5, v16, v18

    if-ltz v5, :cond_5

    .line 165
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 167
    .end local v14    # "yTopOverlap":D
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->point:Lcom/spatial4j/core/shape/Point;

    invoke-interface {v5}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    move-wide/from16 v18, v0

    sub-double v8, v16, v18

    .line 168
    .local v8, "yBot":D
    const-wide v16, -0x3fa9800000000000L    # -90.0

    cmpg-double v5, v8, v16

    if-gez v5, :cond_4

    .line 169
    const-wide v16, -0x3fa9800000000000L    # -90.0

    sub-double v10, v16, v8

    .line 170
    .local v10, "yBotOverlap":D
    sget-boolean v5, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    const-wide v16, 0x4056800000000000L    # 90.0

    cmpg-double v5, v10, v16

    if-lez v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 171
    :cond_3
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v16

    const-wide v18, -0x3fa9800000000000L    # -90.0

    add-double v18, v18, v10

    cmpg-double v5, v16, v18

    if-gtz v5, :cond_5

    .line 172
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 175
    .end local v10    # "yBotOverlap":D
    :cond_4
    sget-boolean v5, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    const-wide v16, 0x4056800000000000L    # 90.0

    cmpl-double v5, v12, v16

    if-eqz v5, :cond_5

    const-wide v16, -0x3fa9800000000000L    # -90.0

    cmpl-double v5, v8, v16

    if-eqz v5, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 181
    .end local v8    # "yBot":D
    :cond_5
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v16

    const-wide v18, 0x4076800000000000L    # 360.0

    cmpl-double v5, v16, v18

    if-nez v5, :cond_6

    .line 182
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 185
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/spatial4j/core/shape/impl/GeoCircle;->numCornersIntersect(Lcom/spatial4j/core/shape/Rectangle;)I

    move-result v4

    .line 188
    .local v4, "cornersIntersect":I
    invoke-virtual/range {p0 .. p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v5

    invoke-interface {v5}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    .line 189
    .local v6, "frontX":D
    const/4 v5, 0x4

    if-ne v4, v5, :cond_9

    .line 190
    const-wide/16 v16, 0x0

    cmpg-double v5, v6, v16

    if-gtz v5, :cond_7

    const-wide v16, 0x4066800000000000L    # 180.0

    add-double v2, v6, v16

    .line 191
    .local v2, "backX":D
    :goto_1
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v2, v3}, Lcom/spatial4j/core/shape/Rectangle;->relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 192
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 190
    .end local v2    # "backX":D
    :cond_7
    const-wide v16, 0x4066800000000000L    # 180.0

    sub-double v2, v6, v16

    goto :goto_1

    .line 194
    .restart local v2    # "backX":D
    :cond_8
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 195
    .end local v2    # "backX":D
    :cond_9
    if-nez v4, :cond_b

    .line 196
    move-object/from16 v0, p1

    invoke-interface {v0, v6, v7, v6, v7}, Lcom/spatial4j/core/shape/Rectangle;->relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 197
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 199
    :cond_a
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 201
    :cond_b
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0
.end method


# virtual methods
.method protected getYAxis()D
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->horizAxisY:D

    return-wide v0
.end method

.method protected relateRectanglePhase2(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/SpatialRelation;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 10
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p2, "bboxSect"    # Lcom/spatial4j/core/shape/SpatialRelation;

    .prologue
    const-wide v8, 0x4076800000000000L    # 360.0

    .line 99
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v6

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    .line 100
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 149
    :goto_0
    return-object v6

    .line 103
    :cond_0
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    if-eqz v6, :cond_1

    .line 104
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->inverseCircle:Lcom/spatial4j/core/shape/impl/GeoCircle;

    invoke-virtual {v6, p1}, Lcom/spatial4j/core/shape/impl/GeoCircle;->relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/spatial4j/core/shape/SpatialRelation;->inverse()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    goto :goto_0

    .line 108
    :cond_1
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v6

    cmpl-double v6, v6, v8

    if-nez v6, :cond_2

    .line 109
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-direct {p0, p1, v6}, Lcom/spatial4j/core/shape/impl/GeoCircle;->relateRectangleCircleWrapsPole(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    goto :goto_0

    .line 113
    :cond_2
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getCrossesDateLine()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getCrossesDateLine()Z

    move-result v6

    if-nez v6, :cond_3

    .line 114
    invoke-super {p0, p1, p2}, Lcom/spatial4j/core/shape/impl/CircleImpl;->relateRectanglePhase2(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/SpatialRelation;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    goto :goto_0

    .line 118
    :cond_3
    invoke-direct {p0, p1}, Lcom/spatial4j/core/shape/impl/GeoCircle;->numCornersIntersect(Lcom/spatial4j/core/shape/Rectangle;)I

    move-result v0

    .line 119
    .local v0, "cornersIntersect":I
    const/4 v6, 0x4

    if-ne v0, v6, :cond_5

    .line 121
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    iget-object v8, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v8}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v8

    invoke-interface {p1, v6, v7, v8, v9}, Lcom/spatial4j/core/shape/Rectangle;->relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v1

    .line 122
    .local v1, "xIntersect":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v1, v6, :cond_4

    .line 123
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 124
    :cond_4
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 128
    .end local v1    # "xIntersect":Lcom/spatial4j/core/shape/SpatialRelation;
    :cond_5
    if-lez v0, :cond_6

    .line 129
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 135
    :cond_6
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getYAxis()D

    move-result-wide v6

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getYAxis()D

    move-result-wide v8

    invoke-interface {p1, v6, v7, v8, v9}, Lcom/spatial4j/core/shape/Rectangle;->relateYRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 136
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    iget-object v8, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v8}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v8

    invoke-interface {p1, v6, v7, v8, v9}, Lcom/spatial4j/core/shape/Rectangle;->relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 137
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 140
    :cond_7
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getXAxis()D

    move-result-wide v6

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getXAxis()D

    move-result-wide v8

    invoke-interface {p1, v6, v7, v8, v9}, Lcom/spatial4j/core/shape/Rectangle;->relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 141
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v6

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    iget-wide v8, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    add-double v4, v6, v8

    .line 142
    .local v4, "yTop":D
    sget-boolean v6, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v6, :cond_8

    const-wide v6, 0x4056800000000000L    # 90.0

    cmpg-double v6, v4, v6

    if-lez v6, :cond_8

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 143
    :cond_8
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v6

    invoke-interface {v6}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    iget-wide v8, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    sub-double v2, v6, v8

    .line 144
    .local v2, "yBot":D
    sget-boolean v6, Lcom/spatial4j/core/shape/impl/GeoCircle;->$assertionsDisabled:Z

    if-nez v6, :cond_9

    const-wide v6, -0x3fa9800000000000L    # -90.0

    cmpl-double v6, v2, v6

    if-gez v6, :cond_9

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 145
    :cond_9
    invoke-interface {p1, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/Rectangle;->relateYRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 146
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0

    .line 149
    .end local v2    # "yBot":D
    .end local v4    # "yTop":D
    :cond_a
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_0
.end method

.method public reset(DDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "radiusDEG"    # D

    .prologue
    .line 44
    invoke-super/range {p0 .. p6}, Lcom/spatial4j/core/shape/impl/CircleImpl;->reset(DDD)V

    .line 45
    invoke-direct {p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;->init()V

    .line 46
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 237
    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    const-wide v6, 0x40b8e3023ed7ac24L    # 6371.0087714

    invoke-static {v4, v5, v6, v7}, Lcom/spatial4j/core/distance/DistanceUtils;->degrees2Dist(DD)D

    move-result-wide v2

    .line 239
    .local v2, "distKm":D
    new-instance v1, Ljava/util/Formatter;

    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v4}, Ljava/util/Formatter;-><init>(Ljava/util/Locale;)V

    const-string v4, "%.1f\u00b0 %.2fkm"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->radiusDEG:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "dStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Circle("

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/spatial4j/core/shape/impl/GeoCircle;->point:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", d="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x29

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
