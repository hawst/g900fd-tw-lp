.class public Lcom/spatial4j/core/shape/ShapeCollection;
.super Ljava/util/AbstractList;
.source "ShapeCollection.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Shape;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Lcom/spatial4j/core/shape/Shape;",
        ">",
        "Ljava/util/AbstractList",
        "<TS;>;",
        "Lcom/spatial4j/core/shape/Shape;"
    }
.end annotation


# instance fields
.field protected final bbox:Lcom/spatial4j/core/shape/Rectangle;

.field protected final shapes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 3
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TS;>;",
            "Lcom/spatial4j/core/context/SpatialContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    .local p1, "shapes":Ljava/util/List;, "Ljava/util/List<TS;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 61
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must be given at least 1 shape"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    instance-of v0, p1, Ljava/util/RandomAccess;

    if-nez v0, :cond_1

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shapes arg must implement RandomAccess: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    iput-object p1, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    .line 66
    invoke-virtual {p0, p1, p2}, Lcom/spatial4j/core/shape/ShapeCollection;->computeBoundingBox(Ljava/util/Collection;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    iput-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    .line 67
    return-void
.end method

.method protected static computeMutualDisjoint(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/spatial4j/core/shape/Shape;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 178
    .local p0, "shapes":Ljava/util/List;, "Ljava/util/List<+Lcom/spatial4j/core/shape/Shape;>;"
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_0

    .line 186
    const/4 v4, 0x1

    :goto_1
    return v4

    .line 179
    :cond_0
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/spatial4j/core/shape/Shape;

    .line 180
    .local v2, "shapeI":Lcom/spatial4j/core/shape/Shape;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-lt v1, v0, :cond_1

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/spatial4j/core/shape/Shape;

    .line 182
    .local v3, "shapeJ":Lcom/spatial4j/core/shape/Shape;
    invoke-interface {v3, v2}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/spatial4j/core/shape/SpatialRelation;->intersects()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 183
    const/4 v4, 0x0

    goto :goto_1

    .line 180
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method


# virtual methods
.method protected computeBoundingBox(Ljava/util/Collection;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Rectangle;
    .locals 13
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/spatial4j/core/shape/Shape;",
            ">;",
            "Lcom/spatial4j/core/context/SpatialContext;",
            ")",
            "Lcom/spatial4j/core/shape/Rectangle;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    .local p1, "shapes":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/spatial4j/core/shape/Shape;>;"
    const/4 v11, 0x0

    .line 71
    .local v11, "xRange":Lcom/spatial4j/core/shape/impl/Range;
    const-wide/high16 v6, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .line 72
    .local v6, "minY":D
    const-wide/high16 v8, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 73
    .local v8, "maxY":D
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 85
    invoke-virtual {v11}, Lcom/spatial4j/core/shape/impl/Range;->getMin()D

    move-result-wide v2

    invoke-virtual {v11}, Lcom/spatial4j/core/shape/impl/Range;->getMax()D

    move-result-wide v4

    move-object v1, p2

    invoke-virtual/range {v1 .. v9}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v1

    return-object v1

    .line 73
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/spatial4j/core/shape/Shape;

    .line 74
    .local v0, "geom":Lcom/spatial4j/core/shape/Shape;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Shape;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v10

    .line 76
    .local v10, "r":Lcom/spatial4j/core/shape/Rectangle;
    invoke-static {v10, p2}, Lcom/spatial4j/core/shape/impl/Range;->xRange(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/impl/Range;

    move-result-object v12

    .line 77
    .local v12, "xRange2":Lcom/spatial4j/core/shape/impl/Range;
    if-nez v11, :cond_1

    .line 78
    move-object v11, v12

    .line 82
    :goto_1
    invoke-interface {v10}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    .line 83
    invoke-interface {v10}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {v11, v12}, Lcom/spatial4j/core/shape/impl/Range;->expandTo(Lcom/spatial4j/core/shape/impl/Range;)Lcom/spatial4j/core/shape/impl/Range;

    move-result-object v11

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222
    if-ne p0, p1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v1

    .line 223
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 225
    check-cast v0, Lcom/spatial4j/core/shape/ShapeCollection;

    .line 227
    .local v0, "that":Lcom/spatial4j/core/shape/ShapeCollection;
    iget-object v3, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    iget-object v4, v0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public get(I)Lcom/spatial4j/core/shape/Shape;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TS;"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/spatial4j/core/shape/Shape;

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/ShapeCollection;->get(I)Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    return-object v0
.end method

.method public getArea(Lcom/spatial4j/core/context/SpatialContext;)D
    .locals 8
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 191
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v3, p0, Lcom/spatial4j/core/shape/ShapeCollection;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v3, p1}, Lcom/spatial4j/core/shape/Rectangle;->getArea(Lcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v0

    .line 192
    .local v0, "MAX_AREA":D
    const-wide/16 v4, 0x0

    .line 193
    .local v4, "sum":D
    iget-object v3, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    move-wide v0, v4

    .line 199
    .end local v0    # "MAX_AREA":D
    :goto_0
    return-wide v0

    .line 193
    .restart local v0    # "MAX_AREA":D
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/spatial4j/core/shape/Shape;

    .line 194
    .local v2, "geom":Lcom/spatial4j/core/shape/Shape;
    invoke-interface {v2, p1}, Lcom/spatial4j/core/shape/Shape;->getArea(Lcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v6

    add-double/2addr v4, v6

    .line 195
    cmpl-double v6, v4, v0

    if-ltz v6, :cond_0

    goto :goto_0
.end method

.method public getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
    .locals 1

    .prologue
    .line 104
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    return-object v0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 109
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method

.method public getShapes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    return-object v0
.end method

.method public hasArea()Z
    .locals 3

    .prologue
    .line 114
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v1, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 119
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 114
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/spatial4j/core/shape/Shape;

    .line 115
    .local v0, "geom":Lcom/spatial4j/core/shape/Shape;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Shape;->hasArea()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 234
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 7
    .param p1, "other"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 124
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v5, p0, Lcom/spatial4j/core/shape/ShapeCollection;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v5, p1}, Lcom/spatial4j/core/shape/Rectangle;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    .line 125
    .local v0, "bboxSect":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq v0, v5, :cond_0

    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v0, v5, :cond_2

    :cond_0
    move-object v3, v0

    .line 146
    :cond_1
    :goto_0
    return-object v3

    .line 128
    :cond_2
    instance-of v5, p1, Lcom/spatial4j/core/shape/Point;

    if-nez v5, :cond_4

    .line 129
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/ShapeCollection;->relateContainsShortCircuits()Z

    move-result v5

    if-nez v5, :cond_4

    .line 128
    const/4 v1, 0x0

    .line 130
    .local v1, "containsWillShortCircuit":Z
    :goto_1
    const/4 v3, 0x0

    .line 131
    .local v3, "sect":Lcom/spatial4j/core/shape/SpatialRelation;
    iget-object v5, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/spatial4j/core/shape/Shape;

    .line 132
    .local v4, "shape":Lcom/spatial4j/core/shape/Shape;
    invoke-interface {v4, p1}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v2

    .line 134
    .local v2, "nextSect":Lcom/spatial4j/core/shape/SpatialRelation;
    if-nez v3, :cond_5

    .line 135
    move-object v3, v2

    .line 140
    :goto_2
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v3, v6, :cond_6

    .line 141
    sget-object v3, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 128
    .end local v1    # "containsWillShortCircuit":Z
    .end local v2    # "nextSect":Lcom/spatial4j/core/shape/SpatialRelation;
    .end local v3    # "sect":Lcom/spatial4j/core/shape/SpatialRelation;
    .end local v4    # "shape":Lcom/spatial4j/core/shape/Shape;
    :cond_4
    const/4 v1, 0x1

    goto :goto_1

    .line 137
    .restart local v1    # "containsWillShortCircuit":Z
    .restart local v2    # "nextSect":Lcom/spatial4j/core/shape/SpatialRelation;
    .restart local v3    # "sect":Lcom/spatial4j/core/shape/SpatialRelation;
    .restart local v4    # "shape":Lcom/spatial4j/core/shape/Shape;
    :cond_5
    invoke-virtual {v3, v2}, Lcom/spatial4j/core/shape/SpatialRelation;->combine(Lcom/spatial4j/core/shape/SpatialRelation;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v3

    goto :goto_2

    .line 143
    :cond_6
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v3, v6, :cond_3

    if-eqz v1, :cond_3

    .line 144
    sget-object v3, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method protected relateContainsShortCircuits()Z
    .locals 1

    .prologue
    .line 165
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 99
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    iget-object v0, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 204
    .local p0, "this":Lcom/spatial4j/core/shape/ShapeCollection;, "Lcom/spatial4j/core/shape/ShapeCollection<TS;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x64

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 205
    .local v0, "buf":Ljava/lang/StringBuilder;
    const-string v4, "ShapeCollection("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const/4 v1, 0x0

    .line 207
    .local v1, "i":I
    iget-object v4, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 216
    :goto_1
    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 207
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/spatial4j/core/shape/Shape;

    .line 208
    .local v3, "shape":Lcom/spatial4j/core/shape/Shape;
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    if-lez v1, :cond_1

    .line 209
    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const/16 v6, 0x96

    if-le v5, v6, :cond_2

    .line 212
    const-string v4, " ..."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/spatial4j/core/shape/ShapeCollection;->shapes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v1, v2

    .line 213
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    :cond_2
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0
.end method
