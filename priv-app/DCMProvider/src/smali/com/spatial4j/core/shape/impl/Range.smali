.class public Lcom/spatial4j/core/shape/impl/Range;
.super Ljava/lang/Object;
.source "Range.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final max:D

.field protected final min:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/spatial4j/core/shape/impl/Range;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/Range;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "min"    # D
    .param p3, "max"    # D

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-wide p1, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    .line 44
    iput-wide p3, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    .line 45
    return-void
.end method

.method public static xRange(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/impl/Range;
    .locals 6
    .param p0, "rect"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/Range$LongitudeRange;-><init>(DD)V

    .line 35
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/spatial4j/core/shape/impl/Range;

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/Range;-><init>(DD)V

    goto :goto_0
.end method

.method public static yRange(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/impl/Range;
    .locals 6
    .param p0, "rect"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 39
    new-instance v0, Lcom/spatial4j/core/shape/impl/Range;

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v2

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/Range;-><init>(DD)V

    return-object v0
.end method


# virtual methods
.method public contains(D)Z
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deltaLen(Lcom/spatial4j/core/shape/impl/Range;)D
    .locals 8
    .param p1, "other"    # Lcom/spatial4j/core/shape/impl/Range;

    .prologue
    .line 102
    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    iget-wide v6, p1, Lcom/spatial4j/core/shape/impl/Range;->min:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 103
    .local v2, "min3":D
    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    iget-wide v6, p1, Lcom/spatial4j/core/shape/impl/Range;->max:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 104
    .local v0, "max3":D
    sub-double v4, v0, v2

    return-wide v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p0, p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 60
    check-cast v0, Lcom/spatial4j/core/shape/impl/Range;

    .line 62
    .local v0, "range":Lcom/spatial4j/core/shape/impl/Range;
    iget-wide v4, v0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 63
    :cond_4
    iget-wide v4, v0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public expandTo(Lcom/spatial4j/core/shape/impl/Range;)Lcom/spatial4j/core/shape/impl/Range;
    .locals 8
    .param p1, "other"    # Lcom/spatial4j/core/shape/impl/Range;

    .prologue
    .line 97
    sget-boolean v0, Lcom/spatial4j/core/shape/impl/Range;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 98
    :cond_0
    new-instance v0, Lcom/spatial4j/core/shape/impl/Range;

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    iget-wide v4, p1, Lcom/spatial4j/core/shape/impl/Range;->min:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    iget-wide v6, p1, Lcom/spatial4j/core/shape/impl/Range;->max:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/Range;-><init>(DD)V

    return-object v0
.end method

.method public getCenter()D
    .locals 6

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/Range;->getWidth()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getMax()D
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    return-wide v0
.end method

.method public getMin()D
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    return-wide v0
.end method

.method public getWidth()D
    .locals 4

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/16 v10, 0x20

    const-wide/16 v8, 0x0

    .line 72
    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_0

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 73
    .local v2, "temp":J
    :goto_0
    ushr-long v6, v2, v10

    xor-long/2addr v6, v2

    long-to-int v0, v6

    .line 74
    .local v0, "result":I
    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_1

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 75
    :goto_1
    mul-int/lit8 v1, v0, 0x1f

    ushr-long v4, v2, v10

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 76
    return v0

    .end local v0    # "result":I
    .end local v2    # "temp":J
    :cond_0
    move-wide v2, v4

    .line 72
    goto :goto_0

    .restart local v0    # "result":I
    .restart local v2    # "temp":J
    :cond_1
    move-wide v2, v4

    .line 74
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Range{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/Range;->min:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TO "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/Range;->max:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
