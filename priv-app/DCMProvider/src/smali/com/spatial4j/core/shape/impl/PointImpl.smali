.class public Lcom/spatial4j/core/shape/impl/PointImpl;
.super Ljava/lang/Object;
.source "PointImpl.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Point;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final ctx:Lcom/spatial4j/core/context/SpatialContext;

.field private x:D

.field private y:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/spatial4j/core/shape/impl/PointImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/PointImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(DDLcom/spatial4j/core/context/SpatialContext;)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p5, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 36
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/spatial4j/core/shape/impl/PointImpl;->reset(DD)V

    .line 37
    return-void
.end method

.method public static equals(Lcom/spatial4j/core/shape/Point;Ljava/lang/Object;)Z
    .locals 8
    .param p0, "thiz"    # Lcom/spatial4j/core/shape/Point;
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    sget-boolean v3, Lcom/spatial4j/core/shape/impl/PointImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 97
    :cond_0
    if-ne p0, p1, :cond_2

    .line 105
    :cond_1
    :goto_0
    return v1

    .line 98
    :cond_2
    instance-of v3, p1, Lcom/spatial4j/core/shape/Point;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 100
    check-cast v0, Lcom/spatial4j/core/shape/Point;

    .line 102
    .local v0, "point":Lcom/spatial4j/core/shape/Point;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 103
    :cond_4
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method public static hashCode(Lcom/spatial4j/core/shape/Point;)I
    .locals 11
    .param p0, "thiz"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    const-wide/16 v4, 0x0

    const/16 v10, 0x20

    const-wide/16 v8, 0x0

    .line 119
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_0

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 120
    .local v2, "temp":J
    :goto_0
    ushr-long v6, v2, v10

    xor-long/2addr v6, v2

    long-to-int v0, v6

    .line 121
    .local v0, "result":I
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_1

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 122
    :goto_1
    mul-int/lit8 v1, v0, 0x1f

    ushr-long v4, v2, v10

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 123
    return v0

    .end local v0    # "result":I
    .end local v2    # "temp":J
    :cond_0
    move-wide v2, v4

    .line 119
    goto :goto_0

    .restart local v0    # "result":I
    .restart local v2    # "temp":J
    :cond_1
    move-wide v2, v4

    .line 121
    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 89
    invoke-static {p0, p1}, Lcom/spatial4j/core/shape/impl/PointImpl;->equals(Lcom/spatial4j/core/shape/Point;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getArea(Lcom/spatial4j/core/context/SpatialContext;)D
    .locals 2
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 79
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0, p0, p0}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/PointImpl;->getCenter()Lcom/spatial4j/core/shape/impl/PointImpl;

    move-result-object v0

    return-object v0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/impl/PointImpl;
    .locals 0

    .prologue
    .line 62
    return-object p0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->y:D

    return-wide v0
.end method

.method public hasArea()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Lcom/spatial4j/core/shape/impl/PointImpl;->hashCode(Lcom/spatial4j/core/shape/Point;)I

    move-result v0

    return v0
.end method

.method public relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 67
    instance-of v0, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_1

    .line 68
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/PointImpl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 69
    :goto_0
    return-object v0

    .line 68
    :cond_0
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 69
    :cond_1
    invoke-interface {p1, p0}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/SpatialRelation;->transpose()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0
.end method

.method public reset(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->x:D

    .line 42
    iput-wide p3, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->y:D

    .line 43
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pt(x="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/PointImpl;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
