.class public Lcom/spatial4j/core/shape/impl/BufferedLine;
.super Ljava/lang/Object;
.source "BufferedLine.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Shape;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bbox:Lcom/spatial4j/core/shape/Rectangle;

.field private final buf:D

.field private final linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

.field private final linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

.field private final pA:Lcom/spatial4j/core/shape/Point;

.field private final pB:Lcom/spatial4j/core/shape/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/spatial4j/core/shape/impl/BufferedLine;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V
    .locals 37
    .param p1, "pA"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "pB"    # Lcom/spatial4j/core/shape/Point;
    .param p3, "buf"    # D
    .param p5, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-boolean v6, Lcom/spatial4j/core/shape/impl/BufferedLine;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    const-wide/16 v6, 0x0

    cmpl-double v6, p3, v6

    if-gez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 65
    :cond_0
    const/16 v18, 0x1

    .line 68
    .local v18, "bufExtend":Z
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/spatial4j/core/shape/impl/BufferedLine;->pA:Lcom/spatial4j/core/shape/Point;

    .line 69
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/spatial4j/core/shape/impl/BufferedLine;->pB:Lcom/spatial4j/core/shape/Point;

    .line 70
    move-wide/from16 v0, p3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    .line 72
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    sub-double v22, v6, v8

    .line 73
    .local v22, "deltaY":D
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v8

    sub-double v20, v6, v8

    .line 75
    .local v20, "deltaX":D
    new-instance v5, Lcom/spatial4j/core/shape/impl/PointImpl;

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v8, v20, v8

    add-double/2addr v6, v8

    .line 76
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double v10, v22, v10

    add-double/2addr v8, v10

    const/4 v10, 0x0

    .line 75
    invoke-direct/range {v5 .. v10}, Lcom/spatial4j/core/shape/impl/PointImpl;-><init>(DDLcom/spatial4j/core/context/SpatialContext;)V

    .line 78
    .local v5, "center":Lcom/spatial4j/core/shape/impl/PointImpl;
    move-wide/from16 v34, p3

    .line 80
    .local v34, "perpExtent":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v20, v6

    if-nez v6, :cond_1

    const-wide/16 v6, 0x0

    cmpl-double v6, v22, v6

    if-nez v6, :cond_1

    .line 81
    new-instance v6, Lcom/spatial4j/core/shape/impl/InfBufLine;

    const-wide/16 v7, 0x0

    move-object v9, v5

    move-wide/from16 v10, p3

    invoke-direct/range {v6 .. v11}, Lcom/spatial4j/core/shape/impl/InfBufLine;-><init>(DLcom/spatial4j/core/shape/Point;D)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    .line 82
    new-instance v6, Lcom/spatial4j/core/shape/impl/InfBufLine;

    const-wide/high16 v7, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    move-object v9, v5

    move-wide/from16 v10, p3

    invoke-direct/range {v6 .. v11}, Lcom/spatial4j/core/shape/impl/InfBufLine;-><init>(DLcom/spatial4j/core/shape/Point;D)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

    .line 92
    :goto_0
    const-wide/16 v6, 0x0

    cmpl-double v6, v20, v6

    if-nez v6, :cond_3

    .line 93
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_2

    .line 94
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v32

    .line 95
    .local v32, "minY":D
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v28

    .line 100
    .local v28, "maxY":D
    :goto_1
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    sub-double v30, v6, p3

    .line 101
    .local v30, "minX":D
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    add-double v26, v6, p3

    .line 102
    .local v26, "maxX":D
    sub-double v32, v32, v34

    .line 103
    add-double v28, v28, v34

    .line 133
    :goto_2
    invoke-virtual/range {p5 .. p5}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v4

    .line 136
    .local v4, "bounds":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    move-wide/from16 v0, v30

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    .line 137
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v6

    move-wide/from16 v0, v26

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    .line 138
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v6

    move-wide/from16 v0, v32

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 139
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v6

    move-wide/from16 v0, v28

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v14

    move-object/from16 v7, p5

    .line 135
    invoke-virtual/range {v7 .. v15}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    .line 140
    return-void

    .line 84
    .end local v4    # "bounds":Lcom/spatial4j/core/shape/Rectangle;
    .end local v26    # "maxX":D
    .end local v28    # "maxY":D
    .end local v30    # "minX":D
    .end local v32    # "minY":D
    :cond_1
    new-instance v6, Lcom/spatial4j/core/shape/impl/InfBufLine;

    div-double v7, v22, v20

    move-object v9, v5

    move-wide/from16 v10, p3

    invoke-direct/range {v6 .. v11}, Lcom/spatial4j/core/shape/impl/InfBufLine;-><init>(DLcom/spatial4j/core/shape/Point;D)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    .line 85
    mul-double v6, v20, v20

    mul-double v8, v22, v22

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    .line 86
    .local v24, "length":D
    new-instance v6, Lcom/spatial4j/core/shape/impl/InfBufLine;

    move-wide/from16 v0, v20

    neg-double v8, v0

    div-double v7, v8, v22

    .line 87
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double v10, v24, v10

    add-double v10, v10, v34

    move-object v9, v5

    invoke-direct/range {v6 .. v11}, Lcom/spatial4j/core/shape/impl/InfBufLine;-><init>(DLcom/spatial4j/core/shape/Point;D)V

    .line 86
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

    goto/16 :goto_0

    .line 97
    .end local v24    # "length":D
    :cond_2
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v32

    .line 98
    .restart local v32    # "minY":D
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v28

    .restart local v28    # "maxY":D
    goto :goto_1

    .line 113
    .end local v28    # "maxY":D
    .end local v32    # "minY":D
    :cond_3
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v8}, Lcom/spatial4j/core/shape/impl/InfBufLine;->getSlope()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    add-double/2addr v6, v8

    mul-double v6, v6, p3

    .line 114
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v8}, Lcom/spatial4j/core/shape/impl/InfBufLine;->getDistDenomInv()D

    move-result-wide v8

    .line 113
    mul-double v16, v6, v8

    .line 115
    .local v16, "bboxBuf":D
    sget-boolean v6, Lcom/spatial4j/core/shape/impl/BufferedLine;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    cmpl-double v6, v16, p3

    if-ltz v6, :cond_4

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double v6, v6, p3

    cmpg-double v6, v16, v6

    if-lez v6, :cond_5

    :cond_4
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 117
    :cond_5
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v8

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_6

    .line 118
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    sub-double v30, v6, v16

    .line 119
    .restart local v30    # "minX":D
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    add-double v26, v6, v16

    .line 124
    .restart local v26    # "maxX":D
    :goto_3
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_7

    .line 125
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    sub-double v32, v6, v16

    .line 126
    .restart local v32    # "minY":D
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    add-double v28, v6, v16

    .line 127
    .restart local v28    # "maxY":D
    goto/16 :goto_2

    .line 121
    .end local v26    # "maxX":D
    .end local v28    # "maxY":D
    .end local v30    # "minX":D
    .end local v32    # "minY":D
    :cond_6
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    sub-double v30, v6, v16

    .line 122
    .restart local v30    # "minX":D
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    add-double v26, v6, v16

    .restart local v26    # "maxX":D
    goto :goto_3

    .line 128
    :cond_7
    invoke-interface/range {p2 .. p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    sub-double v32, v6, v16

    .line 129
    .restart local v32    # "minY":D
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    add-double v28, v6, v16

    .restart local v28    # "maxY":D
    goto/16 :goto_2
.end method

.method public static expandBufForLongitudeSkew(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;D)D
    .locals 10
    .param p0, "pA"    # Lcom/spatial4j/core/shape/Point;
    .param p1, "pB"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "buf"    # D

    .prologue
    .line 150
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 151
    .local v0, "absA":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 152
    .local v2, "absB":D
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 153
    .local v4, "maxLat":D
    invoke-static {v4, v5, p2, p3}, Lcom/spatial4j/core/distance/DistanceUtils;->calcLonDegreesAtLat(DD)D

    move-result-wide v6

    .line 157
    .local v6, "newBuf":D
    sget-boolean v8, Lcom/spatial4j/core/shape/impl/BufferedLine;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    cmpl-double v8, v6, p2

    if-gez v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 158
    :cond_0
    return-wide v6
.end method


# virtual methods
.method public contains(Lcom/spatial4j/core/shape/Point;)Z
    .locals 1
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v0, p1}, Lcom/spatial4j/core/shape/impl/InfBufLine;->contains(Lcom/spatial4j/core/shape/Point;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v0, p1}, Lcom/spatial4j/core/shape/impl/InfBufLine;->contains(Lcom/spatial4j/core/shape/Point;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 247
    if-ne p0, p1, :cond_1

    .line 256
    :cond_0
    :goto_0
    return v1

    .line 248
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 250
    check-cast v0, Lcom/spatial4j/core/shape/impl/BufferedLine;

    .line 252
    .local v0, "that":Lcom/spatial4j/core/shape/impl/BufferedLine;
    iget-wide v4, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 253
    :cond_4
    iget-object v3, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pA:Lcom/spatial4j/core/shape/Point;

    iget-object v4, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pA:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 254
    :cond_5
    iget-object v3, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pB:Lcom/spatial4j/core/shape/Point;

    iget-object v4, v0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pB:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getA()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pA:Lcom/spatial4j/core/shape/Point;

    return-object v0
.end method

.method public getArea(Lcom/spatial4j/core/context/SpatialContext;)D
    .locals 4
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/InfBufLine;->getBuf()D

    move-result-wide v0

    iget-object v2, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v2}, Lcom/spatial4j/core/shape/impl/InfBufLine;->getBuf()D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public getB()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pB:Lcom/spatial4j/core/shape/Point;

    return-object v0
.end method

.method public getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    return-object v0
.end method

.method public getBuf()D
    .locals 2

    .prologue
    .line 223
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    return-wide v0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/BufferedLine;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method

.method public getLinePerp()Lcom/spatial4j/core/shape/impl/InfBufLine;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

    return-object v0
.end method

.method public getLinePrimary()Lcom/spatial4j/core/shape/impl/InfBufLine;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    return-object v0
.end method

.method public hasArea()Z
    .locals 4

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 263
    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pA:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 264
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pB:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    add-int v0, v1, v4

    .line 265
    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    const-wide/16 v6, 0x0

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_0

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 266
    .local v2, "temp":J
    :goto_0
    mul-int/lit8 v1, v0, 0x1f

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 267
    return v0

    .line 265
    .end local v2    # "temp":J
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 10
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    const-wide/16 v2, 0x0

    .line 172
    iget-object v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->bbox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v4, p1}, Lcom/spatial4j/core/shape/Rectangle;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    .line 173
    .local v0, "bboxR":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v4, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq v0, v4, :cond_0

    sget-object v4, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v0, v4, :cond_2

    :cond_0
    move-object v8, v0

    .line 187
    :cond_1
    :goto_0
    return-object v8

    .line 177
    :cond_2
    new-instance v1, Lcom/spatial4j/core/shape/impl/PointImpl;

    const/4 v6, 0x0

    move-wide v4, v2

    invoke-direct/range {v1 .. v6}, Lcom/spatial4j/core/shape/impl/PointImpl;-><init>(DDLcom/spatial4j/core/context/SpatialContext;)V

    .line 178
    .local v1, "scratch":Lcom/spatial4j/core/shape/Point;
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v7

    .line 179
    .local v7, "prC":Lcom/spatial4j/core/shape/Point;
    iget-object v2, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePrimary:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v2, p1, v7, v1}, Lcom/spatial4j/core/shape/impl/InfBufLine;->relate(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v8

    .line 180
    .local v8, "result":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v2, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v8, v2, :cond_3

    .line 181
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 182
    :cond_3
    iget-object v2, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->linePerp:Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v2, p1, v7, v1}, Lcom/spatial4j/core/shape/impl/InfBufLine;->relate(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v9

    .line 183
    .local v9, "resultOpp":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v2, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v9, v2, :cond_4

    .line 184
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 185
    :cond_4
    if-eq v8, v9, :cond_1

    .line 187
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 163
    instance-of v0, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_1

    .line 164
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/BufferedLine;->contains(Lcom/spatial4j/core/shape/Point;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 166
    :goto_0
    return-object v0

    .line 164
    :cond_0
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 165
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    instance-of v0, p1, Lcom/spatial4j/core/shape/Rectangle;

    if-eqz v0, :cond_2

    .line 166
    check-cast p1, Lcom/spatial4j/core/shape/Rectangle;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/BufferedLine;->relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0

    .line 167
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BufferedLine("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pA:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->pB:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/BufferedLine;->buf:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
