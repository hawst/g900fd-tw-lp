.class public Lcom/spatial4j/core/shape/impl/CircleImpl;
.super Ljava/lang/Object;
.source "CircleImpl.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Circle;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final ctx:Lcom/spatial4j/core/context/SpatialContext;

.field protected enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

.field protected final point:Lcom/spatial4j/core/shape/Point;

.field protected radiusDEG:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/spatial4j/core/shape/impl/CircleImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/CircleImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V
    .locals 6
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "radiusDEG"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p4, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 47
    iput-object p1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    .line 48
    iput-wide p2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    .line 49
    invoke-virtual {p4}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    const/4 v5, 0x0

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceCalculator;->calcBoxByDistFromPt(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    iput-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    .line 50
    return-void
.end method

.method public static equals(Lcom/spatial4j/core/shape/Circle;Ljava/lang/Object;)Z
    .locals 8
    .param p0, "thiz"    # Lcom/spatial4j/core/shape/Circle;
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238
    sget-boolean v3, Lcom/spatial4j/core/shape/impl/CircleImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 239
    :cond_0
    if-ne p0, p1, :cond_2

    .line 247
    :cond_1
    :goto_0
    return v1

    .line 240
    :cond_2
    instance-of v3, p1, Lcom/spatial4j/core/shape/Circle;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 242
    check-cast v0, Lcom/spatial4j/core/shape/Circle;

    .line 244
    .local v0, "circle":Lcom/spatial4j/core/shape/Circle;
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v3

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 245
    :cond_4
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method public static hashCode(Lcom/spatial4j/core/shape/Circle;)I
    .locals 8
    .param p0, "thiz"    # Lcom/spatial4j/core/shape/Circle;

    .prologue
    .line 261
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 262
    .local v0, "result":I
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_0

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 263
    .local v2, "temp":J
    :goto_0
    mul-int/lit8 v1, v0, 0x1f

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 264
    return v0

    .line 262
    .end local v2    # "temp":J
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public contains(DD)Z
    .locals 7
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 79
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    move-wide v2, p1

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;DD)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 231
    invoke-static {p0, p1}, Lcom/spatial4j/core/shape/impl/CircleImpl;->equals(Lcom/spatial4j/core/shape/Circle;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getArea(Lcom/spatial4j/core/context/SpatialContext;)D
    .locals 4
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 71
    if-nez p1, :cond_0

    .line 72
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    mul-double/2addr v0, v2

    .line 74
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p1}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/spatial4j/core/distance/DistanceCalculator;->area(Lcom/spatial4j/core/shape/Circle;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    return-object v0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    return-object v0
.end method

.method public getRadius()D
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    return-wide v0
.end method

.method protected getXAxis()D
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    return-wide v0
.end method

.method protected getYAxis()D
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    return-wide v0
.end method

.method public hasArea()Z
    .locals 4

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 252
    invoke-static {p0}, Lcom/spatial4j/core/shape/impl/CircleImpl;->hashCode(Lcom/spatial4j/core/shape/Circle;)I

    move-result v0

    return v0
.end method

.method public relate(Lcom/spatial4j/core/shape/Circle;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 9
    .param p1, "circle"    # Lcom/spatial4j/core/shape/Circle;

    .prologue
    .line 212
    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v6}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v6

    iget-object v7, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/spatial4j/core/distance/DistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)D

    move-result-wide v4

    .line 213
    .local v4, "crossDist":D
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    .local v0, "aDist":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v2

    .line 214
    .local v2, "bDist":D
    add-double v6, v0, v2

    cmpl-double v6, v4, v6

    if-lez v6, :cond_0

    .line 215
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 221
    :goto_0
    return-object v6

    .line 216
    :cond_0
    cmpg-double v6, v4, v0

    if-gez v6, :cond_1

    add-double v6, v4, v2

    cmpg-double v6, v6, v0

    if-gtz v6, :cond_1

    .line 217
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 218
    :cond_1
    cmpg-double v6, v4, v2

    if-gez v6, :cond_2

    add-double v6, v4, v0

    cmpg-double v6, v6, v2

    if-gtz v6, :cond_2

    .line 219
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 221
    :cond_2
    sget-object v6, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 4
    .param p1, "point"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 115
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/spatial4j/core/shape/impl/CircleImpl;->contains(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 2
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 122
    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface {v1, p1}, Lcom/spatial4j/core/shape/Rectangle;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    .line 123
    .local v0, "bboxSect":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v0, v1, :cond_1

    .line 130
    .end local v0    # "bboxSect":Lcom/spatial4j/core/shape/SpatialRelation;
    :cond_0
    :goto_0
    return-object v0

    .line 125
    .restart local v0    # "bboxSect":Lcom/spatial4j/core/shape/SpatialRelation;
    :cond_1
    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/spatial4j/core/shape/impl/CircleImpl;->relateRectanglePhase2(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/SpatialRelation;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 102
    instance-of v0, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_0

    .line 103
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/CircleImpl;->relate(Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 105
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_0
    instance-of v0, p1, Lcom/spatial4j/core/shape/Rectangle;

    if-eqz v0, :cond_1

    .line 106
    check-cast p1, Lcom/spatial4j/core/shape/Rectangle;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/CircleImpl;->relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0

    .line 108
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    instance-of v0, p1, Lcom/spatial4j/core/shape/Circle;

    if-eqz v0, :cond_2

    .line 109
    check-cast p1, Lcom/spatial4j/core/shape/Circle;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/CircleImpl;->relate(Lcom/spatial4j/core/shape/Circle;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0

    .line 111
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_2
    invoke-interface {p1, p0}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/SpatialRelation;->transpose()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0
.end method

.method protected relateRectanglePhase2(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/SpatialRelation;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 32
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p2, "bboxSect"    # Lcom/spatial4j/core/shape/SpatialRelation;

    .prologue
    .line 145
    invoke-virtual/range {p0 .. p0}, Lcom/spatial4j/core/shape/impl/CircleImpl;->getXAxis()D

    move-result-wide v10

    .line 146
    .local v10, "ctr_x":D
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v28

    cmpg-double v19, v10, v28

    if-gez v19, :cond_0

    .line 147
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    .line 154
    .local v6, "closestX":D
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/spatial4j/core/shape/impl/CircleImpl;->getYAxis()D

    move-result-wide v12

    .line 155
    .local v12, "ctr_y":D
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v28

    cmpg-double v19, v12, v28

    if-gez v19, :cond_2

    .line 156
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v8

    .line 163
    .local v8, "closestY":D
    :goto_1
    const/16 v18, 0x0

    .line 164
    .local v18, "didContainOnClosestXY":Z
    cmpl-double v19, v10, v6

    if-nez v19, :cond_5

    .line 165
    sub-double v28, v12, v8

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    .line 166
    .local v16, "deltaY":D
    cmpg-double v19, v12, v8

    if-gez v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v28

    sub-double v22, v28, v12

    .line 167
    .local v22, "distYCirc":D
    :goto_2
    cmpl-double v19, v16, v22

    if-lez v19, :cond_8

    .line 168
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 194
    .end local v16    # "deltaY":D
    .end local v22    # "distYCirc":D
    :goto_3
    return-object v19

    .line 148
    .end local v6    # "closestX":D
    .end local v8    # "closestY":D
    .end local v12    # "ctr_y":D
    .end local v18    # "didContainOnClosestXY":Z
    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v28

    cmpl-double v19, v10, v28

    if-lez v19, :cond_1

    .line 149
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v6

    .restart local v6    # "closestX":D
    goto :goto_0

    .line 151
    .end local v6    # "closestX":D
    :cond_1
    move-wide v6, v10

    .restart local v6    # "closestX":D
    goto :goto_0

    .line 157
    .restart local v12    # "ctr_y":D
    :cond_2
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v28

    cmpl-double v19, v12, v28

    if-lez v19, :cond_3

    .line 158
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v8

    .restart local v8    # "closestY":D
    goto :goto_1

    .line 160
    .end local v8    # "closestY":D
    :cond_3
    move-wide v8, v12

    .restart local v8    # "closestY":D
    goto :goto_1

    .line 166
    .restart local v16    # "deltaY":D
    .restart local v18    # "didContainOnClosestXY":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v28

    sub-double v22, v12, v28

    goto :goto_2

    .line 169
    .end local v16    # "deltaY":D
    :cond_5
    cmpl-double v19, v12, v8

    if-nez v19, :cond_7

    .line 170
    sub-double v28, v10, v6

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    .line 171
    .local v14, "deltaX":D
    cmpg-double v19, v10, v6

    if-gez v19, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v28

    sub-double v20, v28, v10

    .line 172
    .local v20, "distXCirc":D
    :goto_4
    cmpl-double v19, v14, v20

    if-lez v19, :cond_8

    .line 173
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_3

    .line 171
    .end local v20    # "distXCirc":D
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v28

    sub-double v20, v10, v28

    goto :goto_4

    .line 176
    .end local v14    # "deltaX":D
    :cond_7
    const/16 v18, 0x1

    .line 177
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/spatial4j/core/shape/impl/CircleImpl;->contains(DD)Z

    move-result v19

    if-nez v19, :cond_8

    .line 178
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_3

    .line 185
    :cond_8
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_9

    .line 186
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_3

    .line 190
    :cond_9
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v28

    sub-double v28, v28, v10

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v30

    sub-double v30, v10, v30

    cmpl-double v19, v28, v30

    if-lez v19, :cond_a

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v24

    .line 191
    .local v24, "farthestX":D
    :goto_5
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v28

    sub-double v28, v28, v12

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v30

    sub-double v30, v12, v30

    cmpl-double v19, v28, v30

    if-lez v19, :cond_b

    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v26

    .line 192
    .local v26, "farthestY":D
    :goto_6
    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    move-wide/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/spatial4j/core/shape/impl/CircleImpl;->contains(DD)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 193
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_3

    .line 190
    .end local v24    # "farthestX":D
    .end local v26    # "farthestY":D
    :cond_a
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v24

    goto :goto_5

    .line 191
    .restart local v24    # "farthestX":D
    :cond_b
    invoke-interface/range {p1 .. p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v26

    goto :goto_6

    .line 194
    .restart local v26    # "farthestY":D
    :cond_c
    sget-object v19, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto/16 :goto_3
.end method

.method public reset(DDD)V
    .locals 7
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "radiusDEG"    # D

    .prologue
    .line 54
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    .line 55
    iput-wide p5, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    .line 56
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    iget-object v4, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    iget-object v5, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    invoke-interface/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceCalculator;->calcBoxByDistFromPt(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    iput-object v0, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->enclosingBox:Lcom/spatial4j/core/shape/Rectangle;

    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Circle("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->point:Lcom/spatial4j/core/shape/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", d="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/CircleImpl;->radiusDEG:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\u00b0)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
