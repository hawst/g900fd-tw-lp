.class Lcom/spatial4j/core/shape/impl/BufferedLineString$1;
.super Ljava/util/AbstractList;
.source "BufferedLineString.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/spatial4j/core/shape/impl/BufferedLineString;->getPoints()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Lcom/spatial4j/core/shape/Point;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/spatial4j/core/shape/impl/BufferedLineString;

.field private final synthetic val$lines:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/spatial4j/core/shape/impl/BufferedLineString;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;->this$0:Lcom/spatial4j/core/shape/impl/BufferedLineString;

    iput-object p2, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;->val$lines:Ljava/util/List;

    .line 138
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Lcom/spatial4j/core/shape/Point;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 141
    if-nez p1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;->val$lines:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/spatial4j/core/shape/impl/BufferedLine;

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/BufferedLine;->getA()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;->val$lines:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/spatial4j/core/shape/impl/BufferedLine;

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/BufferedLine;->getB()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;->get(I)Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;->val$lines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
