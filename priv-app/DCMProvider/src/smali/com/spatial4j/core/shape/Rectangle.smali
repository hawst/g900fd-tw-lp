.class public interface abstract Lcom/spatial4j/core/shape/Rectangle;
.super Ljava/lang/Object;
.source "Rectangle.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Shape;


# virtual methods
.method public abstract getCrossesDateLine()Z
.end method

.method public abstract getHeight()D
.end method

.method public abstract getMaxX()D
.end method

.method public abstract getMaxY()D
.end method

.method public abstract getMinX()D
.end method

.method public abstract getMinY()D
.end method

.method public abstract getWidth()D
.end method

.method public abstract relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;
.end method

.method public abstract relateYRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;
.end method

.method public abstract reset(DDDD)V
.end method
