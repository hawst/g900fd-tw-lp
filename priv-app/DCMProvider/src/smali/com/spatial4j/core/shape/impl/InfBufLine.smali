.class public Lcom/spatial4j/core/shape/impl/InfBufLine;
.super Ljava/lang/Object;
.source "InfBufLine.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final oppositeQuad:[I


# instance fields
.field private final buf:D

.field private final distDenomInv:D

.field private final intercept:D

.field private final slope:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/spatial4j/core/shape/impl/InfBufLine;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/InfBufLine;->$assertionsDisabled:Z

    .line 132
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/spatial4j/core/shape/impl/InfBufLine;->oppositeQuad:[I

    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :array_0
    .array-data 4
        -0x1
        0x3
        0x4
        0x1
        0x2
    .end array-data
.end method

.method constructor <init>(DLcom/spatial4j/core/shape/Point;D)V
    .locals 7
    .param p1, "slope"    # D
    .param p3, "point"    # Lcom/spatial4j/core/shape/Point;
    .param p4, "buf"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    sget-boolean v0, Lcom/spatial4j/core/shape/impl/InfBufLine;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_0
    iput-wide p1, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    .line 49
    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    invoke-interface {p3}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    .line 51
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->distDenomInv:D

    .line 56
    :goto_0
    iput-wide p4, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->buf:D

    .line 57
    return-void

    .line 53
    :cond_1
    invoke-interface {p3}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    invoke-interface {p3}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    mul-double/2addr v2, p1

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    .line 54
    mul-double v0, p1, p1

    add-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double v0, v4, v0

    iput-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->distDenomInv:D

    goto :goto_0
.end method

.method public static cornerByQuadrant(Lcom/spatial4j/core/shape/Rectangle;ILcom/spatial4j/core/shape/Point;)V
    .locals 6
    .param p0, "r"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p1, "cornerQuad"    # I
    .param p2, "out"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    const/4 v5, 0x1

    .line 135
    if-eq p1, v5, :cond_0

    const/4 v4, 0x4

    if-ne p1, v4, :cond_2

    :cond_0
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v0

    .line 136
    .local v0, "x":D
    :goto_0
    if-eq p1, v5, :cond_1

    const/4 v4, 0x2

    if-ne p1, v4, :cond_3

    :cond_1
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v2

    .line 137
    .local v2, "y":D
    :goto_1
    invoke-interface {p2, v0, v1, v2, v3}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    .line 138
    return-void

    .line 135
    .end local v0    # "x":D
    .end local v2    # "y":D
    :cond_2
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v0

    goto :goto_0

    .line 136
    .restart local v0    # "x":D
    :cond_3
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v2

    goto :goto_1
.end method


# virtual methods
.method contains(Lcom/spatial4j/core/shape/Point;)Z
    .locals 4
    .param p1, "p"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/InfBufLine;->distanceUnbuffered(Lcom/spatial4j/core/shape/Point;)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->buf:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public distanceUnbuffered(Lcom/spatial4j/core/shape/Point;)D
    .locals 8
    .param p1, "c"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 89
    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 93
    :goto_0
    return-wide v2

    .line 92
    :cond_0
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 93
    .local v0, "num":D
    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->distDenomInv:D

    mul-double/2addr v2, v0

    goto :goto_0
.end method

.method public getBuf()D
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->buf:D

    return-wide v0
.end method

.method public getDistDenomInv()D
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->distDenomInv:D

    return-wide v0
.end method

.method public getIntercept()D
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    return-wide v0
.end method

.method public getSlope()D
    .locals 2

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    return-wide v0
.end method

.method public quadrant(Lcom/spatial4j/core/shape/Point;)I
    .locals 10
    .param p1, "c"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 112
    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    invoke-static {v6, v7}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 114
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v6

    iget-wide v8, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    cmpl-double v5, v6, v8

    if-lez v5, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v4

    .line 114
    goto :goto_0

    .line 118
    :cond_2
    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v8

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    add-double v2, v6, v8

    .line 119
    .local v2, "yAtCinLine":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    cmpl-double v5, v6, v2

    if-ltz v5, :cond_3

    move v0, v1

    .line 120
    .local v0, "above":Z
    :goto_1
    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-lez v5, :cond_5

    .line 122
    if-eqz v0, :cond_4

    :goto_2
    move v1, v4

    goto :goto_0

    .line 119
    .end local v0    # "above":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 122
    .restart local v0    # "above":Z
    :cond_4
    const/4 v4, 0x4

    goto :goto_2

    .line 125
    :cond_5
    if-nez v0, :cond_0

    const/4 v1, 0x3

    goto :goto_0
.end method

.method relate(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 6
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p2, "prC"    # Lcom/spatial4j/core/shape/Point;
    .param p3, "scratch"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 60
    sget-boolean v5, Lcom/spatial4j/core/shape/impl/InfBufLine;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 62
    :cond_0
    invoke-virtual {p0, p2}, Lcom/spatial4j/core/shape/impl/InfBufLine;->quadrant(Lcom/spatial4j/core/shape/Point;)I

    move-result v0

    .line 64
    .local v0, "cQuad":I
    move-object v4, p3

    .line 65
    .local v4, "nearestP":Lcom/spatial4j/core/shape/Point;
    sget-object v5, Lcom/spatial4j/core/shape/impl/InfBufLine;->oppositeQuad:[I

    aget v5, v5, v0

    invoke-static {p1, v5, v4}, Lcom/spatial4j/core/shape/impl/InfBufLine;->cornerByQuadrant(Lcom/spatial4j/core/shape/Rectangle;ILcom/spatial4j/core/shape/Point;)V

    .line 66
    invoke-virtual {p0, v4}, Lcom/spatial4j/core/shape/impl/InfBufLine;->contains(Lcom/spatial4j/core/shape/Point;)Z

    move-result v3

    .line 68
    .local v3, "nearestContains":Z
    if-eqz v3, :cond_2

    .line 69
    move-object v2, p3

    .line 70
    .local v2, "farthestP":Lcom/spatial4j/core/shape/Point;
    const/4 v4, 0x0

    .line 71
    invoke-static {p1, v0, v2}, Lcom/spatial4j/core/shape/impl/InfBufLine;->cornerByQuadrant(Lcom/spatial4j/core/shape/Rectangle;ILcom/spatial4j/core/shape/Point;)V

    .line 72
    invoke-virtual {p0, v2}, Lcom/spatial4j/core/shape/impl/InfBufLine;->contains(Lcom/spatial4j/core/shape/Point;)Z

    move-result v1

    .line 73
    .local v1, "farthestContains":Z
    if-eqz v1, :cond_1

    .line 74
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 79
    .end local v1    # "farthestContains":Z
    .end local v2    # "farthestP":Lcom/spatial4j/core/shape/Point;
    :goto_0
    return-object v5

    .line 75
    .restart local v1    # "farthestContains":Z
    .restart local v2    # "farthestP":Lcom/spatial4j/core/shape/Point;
    :cond_1
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 77
    .end local v1    # "farthestContains":Z
    .end local v2    # "farthestP":Lcom/spatial4j/core/shape/Point;
    :cond_2
    invoke-virtual {p0, v4}, Lcom/spatial4j/core/shape/impl/InfBufLine;->quadrant(Lcom/spatial4j/core/shape/Point;)I

    move-result v5

    if-ne v5, v0, :cond_3

    .line 78
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 79
    :cond_3
    sget-object v5, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "InfBufLine{buf="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 160
    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->buf:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 161
    const-string v1, ", intercept="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->intercept:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 162
    const-string v1, ", slope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/InfBufLine;->slope:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 163
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
