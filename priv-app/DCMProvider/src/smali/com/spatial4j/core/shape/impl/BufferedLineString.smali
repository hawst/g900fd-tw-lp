.class public Lcom/spatial4j/core/shape/impl/BufferedLineString;
.super Ljava/lang/Object;
.source "BufferedLineString.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Shape;


# instance fields
.field private final buf:D

.field private final segments:Lcom/spatial4j/core/shape/ShapeCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/spatial4j/core/shape/ShapeCollection",
            "<",
            "Lcom/spatial4j/core/shape/impl/BufferedLine;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;DLcom/spatial4j/core/context/SpatialContext;)V
    .locals 6
    .param p2, "buf"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;D",
            "Lcom/spatial4j/core/context/SpatialContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/Point;>;"
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/spatial4j/core/shape/impl/BufferedLineString;-><init>(Ljava/util/List;DZLcom/spatial4j/core/context/SpatialContext;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/util/List;DZLcom/spatial4j/core/context/SpatialContext;)V
    .locals 18
    .param p2, "buf"    # D
    .param p4, "expandBufForLongitudeSkew"    # Z
    .param p5, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;DZ",
            "Lcom/spatial4j/core/context/SpatialContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/Point;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 65
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v10, "Need at least 1 point."

    invoke-direct {v5, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 66
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 68
    .local v4, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/impl/BufferedLine;>;"
    const/4 v6, 0x0

    .line 69
    .local v6, "prevPoint":Lcom/spatial4j/core/shape/Point;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 79
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 80
    new-instance v11, Lcom/spatial4j/core/shape/impl/BufferedLine;

    move-object v12, v6

    move-object v13, v6

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-direct/range {v11 .. v16}, Lcom/spatial4j/core/shape/impl/BufferedLine;-><init>(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_1
    new-instance v5, Lcom/spatial4j/core/shape/ShapeCollection;

    move-object/from16 v0, p5

    invoke-direct {v5, v4, v0}, Lcom/spatial4j/core/shape/ShapeCollection;-><init>(Ljava/util/List;Lcom/spatial4j/core/context/SpatialContext;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    .line 83
    move-wide/from16 v0, p2

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    .line 84
    return-void

    .line 69
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/spatial4j/core/shape/Point;

    .line 70
    .local v7, "point":Lcom/spatial4j/core/shape/Point;
    if-eqz v6, :cond_4

    .line 71
    move-wide/from16 v8, p2

    .line 72
    .local v8, "segBuf":D
    if-eqz p4, :cond_3

    .line 73
    move-wide/from16 v0, p2

    invoke-static {v6, v7, v0, v1}, Lcom/spatial4j/core/shape/impl/BufferedLine;->expandBufForLongitudeSkew(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;D)D

    move-result-wide v8

    .line 75
    :cond_3
    new-instance v5, Lcom/spatial4j/core/shape/impl/BufferedLine;

    move-object/from16 v10, p5

    invoke-direct/range {v5 .. v10}, Lcom/spatial4j/core/shape/impl/BufferedLine;-><init>(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    .end local v8    # "segBuf":D
    :cond_4
    move-object v6, v7

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    if-ne p0, p1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v1

    .line 156
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 158
    check-cast v0, Lcom/spatial4j/core/shape/impl/BufferedLineString;

    .line 160
    .local v0, "that":Lcom/spatial4j/core/shape/impl/BufferedLineString;
    iget-wide v4, v0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 161
    :cond_4
    iget-object v3, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    iget-object v4, v0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v3, v4}, Lcom/spatial4j/core/shape/ShapeCollection;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getArea(Lcom/spatial4j/core/context/SpatialContext;)D
    .locals 2
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v0, p1}, Lcom/spatial4j/core/shape/ShapeCollection;->getArea(Lcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/ShapeCollection;->getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public getBuf()D
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    return-wide v0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/ShapeCollection;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    return-object v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/ShapeCollection;->getShapes()Ljava/util/List;

    move-result-object v0

    .line 138
    .local v0, "lines":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/impl/BufferedLine;>;"
    new-instance v1, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;

    invoke-direct {v1, p0, v0}, Lcom/spatial4j/core/shape/impl/BufferedLineString$1;-><init>(Lcom/spatial4j/core/shape/impl/BufferedLineString;Ljava/util/List;)V

    return-object v1
.end method

.method public getSegments()Lcom/spatial4j/core/shape/ShapeCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/spatial4j/core/shape/ShapeCollection",
            "<",
            "Lcom/spatial4j/core/shape/impl/BufferedLine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    return-object v0
.end method

.method public hasArea()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/ShapeCollection;->hasArea()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 170
    iget-object v1, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/ShapeCollection;->hashCode()I

    move-result v0

    .line 171
    .local v0, "result":I
    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    const-wide/16 v6, 0x0

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_0

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 172
    .local v2, "temp":J
    :goto_0
    mul-int/lit8 v1, v0, 0x1f

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 173
    return v0

    .line 171
    .end local v2    # "temp":J
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->segments:Lcom/spatial4j/core/shape/ShapeCollection;

    invoke-virtual {v0, p1}, Lcom/spatial4j/core/shape/ShapeCollection;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 121
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 122
    .local v2, "str":Ljava/lang/StringBuilder;
    const-string v3, "BufferedLineString(buf="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/BufferedLineString;->buf:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pts="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    const/4 v0, 0x1

    .line 124
    .local v0, "first":Z
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/BufferedLineString;->getPoints()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 132
    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 124
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/spatial4j/core/shape/Point;

    .line 125
    .local v1, "point":Lcom/spatial4j/core/shape/Point;
    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x0

    .line 130
    :goto_1
    invoke-interface {v1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 128
    :cond_1
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
