.class public final enum Lcom/spatial4j/core/shape/SpatialRelation;
.super Ljava/lang/Enum;
.source "SpatialRelation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/spatial4j/core/shape/SpatialRelation;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$spatial4j$core$shape$SpatialRelation:[I

.field public static final enum CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

.field public static final enum DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

.field private static final synthetic ENUM$VALUES:[Lcom/spatial4j/core/shape/SpatialRelation;

.field public static final enum INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

.field public static final enum WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;


# direct methods
.method static synthetic $SWITCH_TABLE$com$spatial4j$core$shape$SpatialRelation()[I
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->$SWITCH_TABLE$com$spatial4j$core$shape$SpatialRelation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/spatial4j/core/shape/SpatialRelation;->values()[Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/SpatialRelation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/SpatialRelation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/SpatialRelation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-virtual {v1}, Lcom/spatial4j/core/shape/SpatialRelation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->$SWITCH_TABLE$com$spatial4j$core$shape$SpatialRelation:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/spatial4j/core/shape/SpatialRelation;

    const-string v1, "WITHIN"

    invoke-direct {v0, v1, v2}, Lcom/spatial4j/core/shape/SpatialRelation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 30
    new-instance v0, Lcom/spatial4j/core/shape/SpatialRelation;

    const-string v1, "CONTAINS"

    invoke-direct {v0, v1, v3}, Lcom/spatial4j/core/shape/SpatialRelation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 31
    new-instance v0, Lcom/spatial4j/core/shape/SpatialRelation;

    const-string v1, "DISJOINT"

    invoke-direct {v0, v1, v4}, Lcom/spatial4j/core/shape/SpatialRelation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 32
    new-instance v0, Lcom/spatial4j/core/shape/SpatialRelation;

    const-string v1, "INTERSECTS"

    invoke-direct {v0, v1, v5}, Lcom/spatial4j/core/shape/SpatialRelation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/spatial4j/core/shape/SpatialRelation;

    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->ENUM$VALUES:[Lcom/spatial4j/core/shape/SpatialRelation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/spatial4j/core/shape/SpatialRelation;

    return-object v0
.end method

.method public static values()[Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->ENUM$VALUES:[Lcom/spatial4j/core/shape/SpatialRelation;

    array-length v1, v0

    new-array v2, v1, [Lcom/spatial4j/core/shape/SpatialRelation;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public combine(Lcom/spatial4j/core/shape/SpatialRelation;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/SpatialRelation;

    .prologue
    .line 64
    if-ne p1, p0, :cond_0

    .line 69
    .end local p0    # "this":Lcom/spatial4j/core/shape/SpatialRelation;
    :goto_0
    return-object p0

    .line 66
    .restart local p0    # "this":Lcom/spatial4j/core/shape/SpatialRelation;
    :cond_0
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq p1, v0, :cond_2

    .line 67
    :cond_1
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne p0, v0, :cond_3

    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne p1, v0, :cond_3

    .line 68
    :cond_2
    sget-object p0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 69
    :cond_3
    sget-object p0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public intersects()Z
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public inverse()Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lcom/spatial4j/core/shape/SpatialRelation;->$SWITCH_TABLE$com$spatial4j$core$shape$SpatialRelation()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/SpatialRelation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    :goto_0
    return-object v0

    .line 88
    :pswitch_0
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 89
    :pswitch_1
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 90
    :pswitch_2
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public transpose()Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/spatial4j/core/shape/SpatialRelation;->$SWITCH_TABLE$com$spatial4j$core$shape$SpatialRelation()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/SpatialRelation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    .end local p0    # "this":Lcom/spatial4j/core/shape/SpatialRelation;
    :goto_0
    return-object p0

    .line 44
    .restart local p0    # "this":Lcom/spatial4j/core/shape/SpatialRelation;
    :pswitch_0
    sget-object p0, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 45
    :pswitch_1
    sget-object p0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
