.class public Lcom/spatial4j/core/shape/impl/RectangleImpl;
.super Ljava/lang/Object;
.source "RectangleImpl.java"

# interfaces
.implements Lcom/spatial4j/core/shape/Rectangle;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final ctx:Lcom/spatial4j/core/context/SpatialContext;

.field private maxX:D

.field private maxY:D

.field private minX:D

.field private minY:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V
    .locals 1
    .param p1, "minX"    # D
    .param p3, "maxX"    # D
    .param p5, "minY"    # D
    .param p7, "maxY"    # D
    .param p9, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p9, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 44
    invoke-virtual/range {p0 .. p8}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->reset(DDDD)V

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 11
    .param p1, "lowerLeft"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "upperRight"    # Lcom/spatial4j/core/shape/Point;
    .param p3, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 49
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    invoke-interface {p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    .line 50
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-interface {p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    move-object v1, p0

    move-object v10, p3

    invoke-direct/range {v1 .. v10}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 11
    .param p1, "r"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p2, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 55
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v6

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v8

    move-object v1, p0

    move-object v10, p2

    invoke-direct/range {v1 .. v10}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V

    .line 56
    return-void
.end method

.method public static equals(Lcom/spatial4j/core/shape/Rectangle;Ljava/lang/Object;)Z
    .locals 8
    .param p0, "thiz"    # Lcom/spatial4j/core/shape/Rectangle;
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 261
    sget-boolean v3, Lcom/spatial4j/core/shape/impl/RectangleImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 262
    :cond_0
    if-ne p0, p1, :cond_2

    .line 272
    :cond_1
    :goto_0
    return v1

    .line 263
    :cond_2
    instance-of v3, p1, Lcom/spatial4j/core/shape/Rectangle;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 265
    check-cast v0, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    .line 267
    .local v0, "rectangle":Lcom/spatial4j/core/shape/impl/RectangleImpl;
    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMaxX()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 268
    :cond_4
    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMaxY()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 269
    :cond_5
    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMinX()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_0

    .line 270
    :cond_6
    invoke-virtual {v0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMinY()D

    move-result-wide v4

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method public static hashCode(Lcom/spatial4j/core/shape/Rectangle;)I
    .locals 11
    .param p0, "thiz"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    const-wide/16 v4, 0x0

    const/16 v10, 0x20

    const-wide/16 v8, 0x0

    .line 286
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_0

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 287
    .local v2, "temp":J
    :goto_0
    ushr-long v6, v2, v10

    xor-long/2addr v6, v2

    long-to-int v0, v6

    .line 288
    .local v0, "result":I
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_1

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 289
    :goto_1
    mul-int/lit8 v1, v0, 0x1f

    ushr-long v6, v2, v10

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v0, v1, v6

    .line 290
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_2

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 291
    :goto_2
    mul-int/lit8 v1, v0, 0x1f

    ushr-long v6, v2, v10

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v0, v1, v6

    .line 292
    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_3

    invoke-interface {p0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 293
    :goto_3
    mul-int/lit8 v1, v0, 0x1f

    ushr-long v4, v2, v10

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 294
    return v0

    .end local v0    # "result":I
    .end local v2    # "temp":J
    :cond_0
    move-wide v2, v4

    .line 286
    goto :goto_0

    .restart local v0    # "result":I
    .restart local v2    # "temp":J
    :cond_1
    move-wide v2, v4

    .line 288
    goto :goto_1

    :cond_2
    move-wide v2, v4

    .line 290
    goto :goto_2

    :cond_3
    move-wide v2, v4

    .line 292
    goto :goto_3
.end method

.method private static relate_range(DDDD)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 2
    .param p0, "int_min"    # D
    .param p2, "int_max"    # D
    .param p4, "ext_min"    # D
    .param p6, "ext_max"    # D

    .prologue
    .line 187
    cmpl-double v0, p4, p2

    if-gtz v0, :cond_0

    cmpg-double v0, p6, p0

    if-gez v0, :cond_1

    .line 188
    :cond_0
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 198
    :goto_0
    return-object v0

    .line 191
    :cond_1
    cmpl-double v0, p4, p0

    if-ltz v0, :cond_2

    cmpg-double v0, p6, p2

    if-gtz v0, :cond_2

    .line 192
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 195
    :cond_2
    cmpg-double v0, p4, p0

    if-gtz v0, :cond_3

    cmpl-double v0, p6, p2

    if-ltz v0, :cond_3

    .line 196
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 198
    :cond_3
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 254
    invoke-static {p0, p1}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->equals(Lcom/spatial4j/core/shape/Rectangle;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getArea(Lcom/spatial4j/core/context/SpatialContext;)D
    .locals 4
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getWidth()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getHeight()D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 77
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p1}, Lcom/spatial4j/core/context/SpatialContext;->getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/spatial4j/core/distance/DistanceCalculator;->area(Lcom/spatial4j/core/shape/Rectangle;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
    .locals 0

    .prologue
    .line 123
    return-object p0
.end method

.method public getCenter()Lcom/spatial4j/core/shape/Point;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 245
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getHeight()D

    move-result-wide v0

    div-double/2addr v0, v8

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    add-double v4, v0, v6

    .line 246
    .local v4, "y":D
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getWidth()D

    move-result-wide v0

    div-double/2addr v0, v8

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    add-double v2, v0, v6

    .line 247
    .local v2, "x":D
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    iget-wide v6, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    cmpl-double v0, v0, v6

    if-lez v0, :cond_0

    .line 248
    invoke-static {v2, v3}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v2

    .line 249
    :cond_0
    new-instance v1, Lcom/spatial4j/core/shape/impl/PointImpl;

    iget-object v6, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-direct/range {v1 .. v6}, Lcom/spatial4j/core/shape/impl/PointImpl;-><init>(DDLcom/spatial4j/core/context/SpatialContext;)V

    return-object v1
.end method

.method public getCrossesDateLine()Z
    .locals 4

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeight()D
    .locals 4

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxY:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxX()D
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    return-wide v0
.end method

.method public getMaxY()D
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxY:D

    return-wide v0
.end method

.method public getMinX()D
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    return-wide v0
.end method

.method public getMinY()D
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    return-wide v0
.end method

.method public getWidth()D
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 93
    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    iget-wide v4, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    sub-double v0, v2, v4

    .line 94
    .local v0, "w":D
    cmpg-double v2, v0, v6

    if-gez v2, :cond_0

    .line 95
    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    .line 96
    sget-boolean v2, Lcom/spatial4j/core/shape/impl/RectangleImpl;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    cmpl-double v2, v0, v6

    if-gez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 98
    :cond_0
    return-wide v0
.end method

.method public hasArea()Z
    .locals 4

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxY:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 277
    invoke-static {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->hashCode(Lcom/spatial4j/core/shape/Rectangle;)I

    move-result v0

    return v0
.end method

.method public relate(Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 14
    .param p1, "point"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    const-wide v12, 0x4076800000000000L    # 360.0

    .line 138
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMaxY()D

    move-result-wide v10

    cmpl-double v8, v8, v10

    if-gtz v8, :cond_0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMinY()D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_1

    .line 139
    :cond_0
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 161
    :goto_0
    return-object v8

    .line 141
    :cond_1
    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    .line 142
    .local v2, "minX":D
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    .line 143
    .local v0, "maxX":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    .line 144
    .local v4, "pX":D
    iget-object v8, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v8}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 146
    sub-double v6, v0, v2

    .line 147
    .local v6, "rawWidth":D
    const-wide/16 v8, 0x0

    cmpg-double v8, v6, v8

    if-gez v8, :cond_2

    .line 148
    add-double v8, v6, v12

    add-double v0, v2, v8

    .line 151
    :cond_2
    cmpg-double v8, v4, v2

    if-gez v8, :cond_5

    .line 152
    add-double/2addr v4, v12

    .line 159
    .end local v6    # "rawWidth":D
    :cond_3
    :goto_1
    cmpg-double v8, v4, v2

    if-ltz v8, :cond_4

    cmpl-double v8, v4, v0

    if-lez v8, :cond_7

    .line 160
    :cond_4
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 153
    .restart local v6    # "rawWidth":D
    :cond_5
    cmpl-double v8, v4, v0

    if-lez v8, :cond_6

    .line 154
    sub-double/2addr v4, v12

    .line 155
    goto :goto_1

    .line 156
    :cond_6
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 161
    .end local v6    # "rawWidth":D
    :cond_7
    sget-object v8, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 6
    .param p1, "rect"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 165
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->relateYRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v1

    .line 166
    .local v1, "yIntersect":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v2, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v1, v2, :cond_1

    .line 167
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 182
    :cond_0
    :goto_0
    return-object v0

    .line 169
    :cond_1
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    .line 170
    .local v0, "xIntersect":Lcom/spatial4j/core/shape/SpatialRelation;
    sget-object v2, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    if-ne v0, v2, :cond_2

    .line 171
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->DISJOINT:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 173
    :cond_2
    if-eq v0, v1, :cond_0

    .line 177
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMinX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMaxX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3

    move-object v0, v1

    .line 178
    goto :goto_0

    .line 179
    :cond_3
    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMinY()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->getMaxY()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 182
    :cond_4
    sget-object v0, Lcom/spatial4j/core/shape/SpatialRelation;->INTERSECTS:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0
.end method

.method public relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 1
    .param p1, "other"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 128
    instance-of v0, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v0, :cond_0

    .line 129
    check-cast p1, Lcom/spatial4j/core/shape/Point;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->relate(Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    .line 131
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_0
    instance-of v0, p1, Lcom/spatial4j/core/shape/Rectangle;

    if-eqz v0, :cond_1

    .line 132
    check-cast p1, Lcom/spatial4j/core/shape/Rectangle;

    .end local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->relate(Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0

    .line 134
    .restart local p1    # "other":Lcom/spatial4j/core/shape/Shape;
    :cond_1
    invoke-interface {p1, p0}, Lcom/spatial4j/core/shape/Shape;->relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/spatial4j/core/shape/SpatialRelation;->transpose()Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    goto :goto_0
.end method

.method public relateXRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 13
    .param p1, "ext_minX"    # D
    .param p3, "ext_maxX"    # D

    .prologue
    .line 209
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    .line 210
    .local v0, "minX":D
    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    .line 211
    .local v2, "maxX":D
    iget-object v4, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v4}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 213
    sub-double v10, v2, v0

    .line 214
    .local v10, "rawWidth":D
    const-wide v4, 0x4076800000000000L    # 360.0

    cmpl-double v4, v10, v4

    if-nez v4, :cond_0

    .line 215
    sget-object v4, Lcom/spatial4j/core/shape/SpatialRelation;->CONTAINS:Lcom/spatial4j/core/shape/SpatialRelation;

    .line 235
    .end local v10    # "rawWidth":D
    :goto_0
    return-object v4

    .line 216
    .restart local v10    # "rawWidth":D
    :cond_0
    const-wide/16 v4, 0x0

    cmpg-double v4, v10, v4

    if-gez v4, :cond_1

    .line 217
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr v4, v10

    add-double v2, v0, v4

    .line 219
    :cond_1
    sub-double v8, p3, p1

    .line 220
    .local v8, "ext_rawWidth":D
    const-wide v4, 0x4076800000000000L    # 360.0

    cmpl-double v4, v8, v4

    if-nez v4, :cond_2

    .line 221
    sget-object v4, Lcom/spatial4j/core/shape/SpatialRelation;->WITHIN:Lcom/spatial4j/core/shape/SpatialRelation;

    goto :goto_0

    .line 222
    :cond_2
    const-wide/16 v4, 0x0

    cmpg-double v4, v8, v4

    if-gez v4, :cond_3

    .line 223
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr v4, v8

    add-double p3, p1, v4

    .line 226
    :cond_3
    cmpg-double v4, v2, p1

    if-gez v4, :cond_5

    .line 227
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v4

    .line 228
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr v2, v4

    .end local v8    # "ext_rawWidth":D
    .end local v10    # "rawWidth":D
    :cond_4
    :goto_1
    move-wide v4, p1

    move-wide/from16 v6, p3

    .line 235
    invoke-static/range {v0 .. v7}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->relate_range(DDDD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v4

    goto :goto_0

    .line 229
    .restart local v8    # "ext_rawWidth":D
    .restart local v10    # "rawWidth":D
    :cond_5
    cmpg-double v4, p3, v0

    if-gez v4, :cond_4

    .line 230
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr p1, v4

    .line 231
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double p3, p3, v4

    goto :goto_1
.end method

.method public relateYRange(DD)Lcom/spatial4j/core/shape/SpatialRelation;
    .locals 9
    .param p1, "ext_minY"    # D
    .param p3, "ext_maxY"    # D

    .prologue
    .line 203
    iget-wide v0, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxY:D

    move-wide v4, p1

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lcom/spatial4j/core/shape/impl/RectangleImpl;->relate_range(DDDD)Lcom/spatial4j/core/shape/SpatialRelation;

    move-result-object v0

    return-object v0
.end method

.method public reset(DDDD)V
    .locals 1
    .param p1, "minX"    # D
    .param p3, "maxX"    # D
    .param p5, "minY"    # D
    .param p7, "maxY"    # D

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    .line 61
    iput-wide p3, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    .line 62
    iput-wide p5, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    .line 63
    iput-wide p7, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxY:D

    .line 64
    sget-boolean v0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    cmpg-double v0, p5, p7

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Rect(minX="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minX:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",maxX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxX:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",minY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->minY:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",maxY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/spatial4j/core/shape/impl/RectangleImpl;->maxY:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
