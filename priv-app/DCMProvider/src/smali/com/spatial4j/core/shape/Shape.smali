.class public interface abstract Lcom/spatial4j/core/shape/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# virtual methods
.method public abstract getArea(Lcom/spatial4j/core/context/SpatialContext;)D
.end method

.method public abstract getBoundingBox()Lcom/spatial4j/core/shape/Rectangle;
.end method

.method public abstract getCenter()Lcom/spatial4j/core/shape/Point;
.end method

.method public abstract hasArea()Z
.end method

.method public abstract relate(Lcom/spatial4j/core/shape/Shape;)Lcom/spatial4j/core/shape/SpatialRelation;
.end method
