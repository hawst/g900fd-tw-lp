.class public Lcom/spatial4j/core/context/SpatialContext;
.super Ljava/lang/Object;
.source "SpatialContext.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final GEO:Lcom/spatial4j/core/context/SpatialContext;


# instance fields
.field private final calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

.field private final geo:Z

.field private final shapeReadWriter:Lcom/spatial4j/core/io/ShapeReadWriter;

.field private final worldBounds:Lcom/spatial4j/core/shape/Rectangle;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45
    const-class v0, Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/spatial4j/core/context/SpatialContext;->$assertionsDisabled:Z

    .line 48
    new-instance v0, Lcom/spatial4j/core/context/SpatialContext;

    invoke-direct {v0, v1}, Lcom/spatial4j/core/context/SpatialContext;-><init>(Z)V

    sput-object v0, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "geo"    # Z

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-direct {p0, p1, v0, v0}, Lcom/spatial4j/core/context/SpatialContext;-><init>(ZLcom/spatial4j/core/distance/DistanceCalculator;Lcom/spatial4j/core/shape/Rectangle;)V

    .line 91
    return-void
.end method

.method public constructor <init>(ZLcom/spatial4j/core/distance/DistanceCalculator;Lcom/spatial4j/core/shape/Rectangle;)V
    .locals 12
    .param p1, "geo"    # Z
    .param p2, "calculator"    # Lcom/spatial4j/core/distance/DistanceCalculator;
    .param p3, "worldBounds"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    const-wide v4, 0x4066800000000000L    # 180.0

    const-wide v8, 0x4056800000000000L    # 90.0

    const-wide v10, -0x10000000000001L

    const-wide v2, -0x3f99800000000000L    # -180.0

    const-wide v6, -0x3fa9800000000000L    # -90.0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-boolean p1, p0, Lcom/spatial4j/core/context/SpatialContext;->geo:Z

    .line 65
    if-nez p2, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    new-instance p2, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Haversine;

    .end local p2    # "calculator":Lcom/spatial4j/core/distance/DistanceCalculator;
    invoke-direct {p2}, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Haversine;-><init>()V

    .line 70
    .restart local p2    # "calculator":Lcom/spatial4j/core/distance/DistanceCalculator;
    :cond_0
    :goto_0
    iput-object p2, p0, Lcom/spatial4j/core/context/SpatialContext;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    .line 72
    if-nez p3, :cond_4

    .line 73
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    new-instance v1, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    move-object v10, p0

    invoke-direct/range {v1 .. v10}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V

    move-object p3, v1

    .line 84
    :cond_1
    :goto_1
    new-instance v0, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    invoke-direct {v0, p3, p0}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(Lcom/spatial4j/core/shape/Rectangle;Lcom/spatial4j/core/context/SpatialContext;)V

    iput-object v0, p0, Lcom/spatial4j/core/context/SpatialContext;->worldBounds:Lcom/spatial4j/core/shape/Rectangle;

    .line 86
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->makeShapeReadWriter()Lcom/spatial4j/core/io/ShapeReadWriter;

    move-result-object v0

    iput-object v0, p0, Lcom/spatial4j/core/context/SpatialContext;->shapeReadWriter:Lcom/spatial4j/core/io/ShapeReadWriter;

    .line 87
    return-void

    .line 68
    :cond_2
    new-instance p2, Lcom/spatial4j/core/distance/CartesianDistCalc;

    .end local p2    # "calculator":Lcom/spatial4j/core/distance/DistanceCalculator;
    invoke-direct {p2}, Lcom/spatial4j/core/distance/CartesianDistCalc;-><init>()V

    goto :goto_0

    .line 75
    .restart local p2    # "calculator":Lcom/spatial4j/core/distance/DistanceCalculator;
    :cond_3
    new-instance v1, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 76
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    move-wide v2, v10

    move-wide v6, v10

    move-object v10, p0

    .line 75
    invoke-direct/range {v1 .. v10}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V

    move-object p3, v1

    goto :goto_1

    .line 78
    :cond_4
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 79
    sget-boolean v0, Lcom/spatial4j/core/context/SpatialContext;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    new-instance v1, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    move-object v10, p0

    invoke-direct/range {v1 .. v10}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V

    invoke-virtual {p3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 80
    :cond_5
    invoke-interface {p3}, Lcom/spatial4j/core/shape/Rectangle;->getCrossesDateLine()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "worldBounds shouldn\'t cross dateline: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getDistCalc()Lcom/spatial4j/core/distance/DistanceCalculator;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/spatial4j/core/context/SpatialContext;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    return-object v0
.end method

.method public getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/spatial4j/core/context/SpatialContext;->worldBounds:Lcom/spatial4j/core/shape/Rectangle;

    return-object v0
.end method

.method public isGeo()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/spatial4j/core/context/SpatialContext;->geo:Z

    return v0
.end method

.method public makeCircle(DDD)Lcom/spatial4j/core/shape/Circle;
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "distance"    # D

    .prologue
    .line 177
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    invoke-virtual {p0, v0, p5, p6}, Lcom/spatial4j/core/context/SpatialContext;->makeCircle(Lcom/spatial4j/core/shape/Point;D)Lcom/spatial4j/core/shape/Circle;

    move-result-object v0

    return-object v0
.end method

.method public makeCircle(Lcom/spatial4j/core/shape/Point;D)Lcom/spatial4j/core/shape/Circle;
    .locals 4
    .param p1, "point"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distance"    # D

    .prologue
    .line 182
    const-wide/16 v0, 0x0

    cmpg-double v0, p2, v0

    if-gez v0, :cond_0

    .line 183
    new-instance v0, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "distance must be >= 0; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    const-wide v0, 0x4066800000000000L    # 180.0

    cmpl-double v0, p2, v0

    if-lez v0, :cond_1

    .line 186
    new-instance v0, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "distance must be <= 180; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_1
    new-instance v0, Lcom/spatial4j/core/shape/impl/GeoCircle;

    invoke-direct {v0, p1, p2, p3, p0}, Lcom/spatial4j/core/shape/impl/GeoCircle;-><init>(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V

    .line 189
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lcom/spatial4j/core/shape/impl/CircleImpl;

    invoke-direct {v0, p1, p2, p3, p0}, Lcom/spatial4j/core/shape/impl/CircleImpl;-><init>(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)V

    goto :goto_0
.end method

.method public makePoint(DD)Lcom/spatial4j/core/shape/Point;
    .locals 7
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 130
    invoke-virtual {p0, p1, p2}, Lcom/spatial4j/core/context/SpatialContext;->verifyX(D)V

    .line 131
    invoke-virtual {p0, p3, p4}, Lcom/spatial4j/core/context/SpatialContext;->verifyY(D)V

    .line 132
    new-instance v1, Lcom/spatial4j/core/shape/impl/PointImpl;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/spatial4j/core/shape/impl/PointImpl;-><init>(DDLcom/spatial4j/core/context/SpatialContext;)V

    return-object v1
.end method

.method public makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;
    .locals 13
    .param p1, "minX"    # D
    .param p3, "maxX"    # D
    .param p5, "minY"    # D
    .param p7, "maxY"    # D

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v2

    .line 149
    .local v2, "bounds":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    cmpl-double v3, p5, v4

    if-ltz v3, :cond_0

    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    cmpg-double v3, p7, v4

    if-lez v3, :cond_1

    .line 150
    :cond_0
    new-instance v3, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Y values ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p7

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] not in boundary "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 151
    :cond_1
    cmpl-double v3, p5, p7

    if-lez v3, :cond_2

    .line 152
    new-instance v3, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "maxY must be >= minY: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p7

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 154
    :cond_2
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->isGeo()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 155
    invoke-virtual {p0, p1, p2}, Lcom/spatial4j/core/context/SpatialContext;->verifyX(D)V

    .line 156
    move-wide/from16 v0, p3

    invoke-virtual {p0, v0, v1}, Lcom/spatial4j/core/context/SpatialContext;->verifyX(D)V

    .line 160
    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v3, p1, v4

    if-nez v3, :cond_4

    cmpl-double v3, p1, p3

    if-eqz v3, :cond_4

    .line 161
    const-wide p1, -0x3f99800000000000L    # -180.0

    .line 172
    :cond_3
    :goto_0
    new-instance v3, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-object v12, p0

    invoke-direct/range {v3 .. v12}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(DDDDLcom/spatial4j/core/context/SpatialContext;)V

    return-object v3

    .line 162
    :cond_4
    const-wide v4, -0x3f99800000000000L    # -180.0

    cmpl-double v3, p3, v4

    if-nez v3, :cond_3

    cmpl-double v3, p1, p3

    if-eqz v3, :cond_3

    .line 163
    const-wide p3, 0x4066800000000000L    # 180.0

    .line 166
    goto :goto_0

    .line 167
    :cond_5
    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v4

    cmpl-double v3, p1, v4

    if-ltz v3, :cond_6

    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    cmpg-double v3, p3, v4

    if-lez v3, :cond_7

    .line 168
    :cond_6
    new-instance v3, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "X values ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] not in boundary "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 169
    :cond_7
    cmpl-double v3, p1, p3

    if-lez v3, :cond_3

    .line 170
    new-instance v3, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "maxX must be >= minX: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public makeRectangle(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Rectangle;
    .locals 10
    .param p1, "lowerLeft"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "upperRight"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 137
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    invoke-interface {p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    .line 138
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v6

    invoke-interface {p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    move-object v1, p0

    .line 137
    invoke-virtual/range {v1 .. v9}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method protected makeShapeReadWriter()Lcom/spatial4j/core/io/ShapeReadWriter;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/spatial4j/core/io/ShapeReadWriter;

    invoke-direct {v0, p0}, Lcom/spatial4j/core/io/ShapeReadWriter;-><init>(Lcom/spatial4j/core/context/SpatialContext;)V

    return-object v0
.end method

.method public readShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/spatial4j/core/context/SpatialContext;->shapeReadWriter:Lcom/spatial4j/core/io/ShapeReadWriter;

    invoke-virtual {v0, p1}, Lcom/spatial4j/core/io/ShapeReadWriter;->readShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    sget-object v0, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/spatial4j/core/context/SpatialContext;->GEO:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".GEO"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 209
    const-string v1, "geo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/spatial4j/core/context/SpatialContext;->geo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 210
    const-string v1, ", calculator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/spatial4j/core/context/SpatialContext;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 211
    const-string v1, ", worldBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/spatial4j/core/context/SpatialContext;->worldBounds:Lcom/spatial4j/core/shape/Rectangle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 212
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString(Lcom/spatial4j/core/shape/Shape;)Ljava/lang/String;
    .locals 1
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/spatial4j/core/context/SpatialContext;->shapeReadWriter:Lcom/spatial4j/core/io/ShapeReadWriter;

    invoke-virtual {v0, p1}, Lcom/spatial4j/core/io/ShapeReadWriter;->writeShape(Lcom/spatial4j/core/shape/Shape;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public verifyX(D)V
    .locals 5
    .param p1, "x"    # D

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    .line 117
    .local v0, "bounds":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v2

    cmpl-double v1, p1, v2

    if-ltz v1, :cond_0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v2

    cmpg-double v1, p1, v2

    if-lez v1, :cond_1

    .line 118
    :cond_0
    new-instance v1, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad X value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not in boundary "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 119
    :cond_1
    return-void
.end method

.method public verifyY(D)V
    .locals 5
    .param p1, "y"    # D

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContext;->getWorldBounds()Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    .line 124
    .local v0, "bounds":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v2

    cmpl-double v1, p1, v2

    if-ltz v1, :cond_0

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v2

    cmpg-double v1, p1, v2

    if-lez v1, :cond_1

    .line 125
    :cond_0
    new-instance v1, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad Y value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not in boundary "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 126
    :cond_1
    return-void
.end method
