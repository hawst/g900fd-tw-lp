.class public Lcom/spatial4j/core/context/SpatialContextFactory;
.super Ljava/lang/Object;
.source "SpatialContextFactory.java"


# instance fields
.field protected args:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

.field protected classLoader:Ljava/lang/ClassLoader;

.field protected geo:Z

.field protected worldBounds:Lcom/spatial4j/core/shape/Rectangle;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->geo:Z

    .line 87
    return-void
.end method

.method public static makeSpatialContext(Ljava/util/Map;Ljava/lang/ClassLoader;)Lcom/spatial4j/core/context/SpatialContext;
    .locals 5
    .param p1, "classLoader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/ClassLoader;",
            ")",
            "Lcom/spatial4j/core/context/SpatialContext;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v4, "spatialContextFactory"

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 70
    .local v1, "cname":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 71
    const-string v4, "SpatialContextFactory"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    :cond_0
    if-nez v1, :cond_1

    .line 73
    new-instance v3, Lcom/spatial4j/core/context/SpatialContextFactory;

    invoke-direct {v3}, Lcom/spatial4j/core/context/SpatialContextFactory;-><init>()V

    .line 82
    .local v3, "instance":Lcom/spatial4j/core/context/SpatialContextFactory;
    :goto_0
    invoke-virtual {v3, p0, p1}, Lcom/spatial4j/core/context/SpatialContextFactory;->init(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 83
    invoke-virtual {v3}, Lcom/spatial4j/core/context/SpatialContextFactory;->newSpatialContext()Lcom/spatial4j/core/context/SpatialContext;

    move-result-object v4

    return-object v4

    .line 76
    .end local v3    # "instance":Lcom/spatial4j/core/context/SpatialContextFactory;
    :cond_1
    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 77
    .local v0, "c":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/spatial4j/core/context/SpatialContextFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v3    # "instance":Lcom/spatial4j/core/context/SpatialContextFactory;
    goto :goto_0

    .line 78
    .end local v0    # "c":Ljava/lang/Class;
    .end local v3    # "instance":Lcom/spatial4j/core/context/SpatialContextFactory;
    :catch_0
    move-exception v2

    .line 79
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method protected init(Ljava/util/Map;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p2, "classLoader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/ClassLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->args:Ljava/util/Map;

    .line 91
    iput-object p2, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->classLoader:Ljava/lang/ClassLoader;

    .line 92
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContextFactory;->initUnits()V

    .line 93
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContextFactory;->initCalculator()V

    .line 94
    invoke-virtual {p0}, Lcom/spatial4j/core/context/SpatialContextFactory;->initWorldBounds()V

    .line 95
    return-void
.end method

.method protected initCalculator()V
    .locals 4

    .prologue
    .line 104
    iget-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->args:Ljava/util/Map;

    const-string v2, "distCalculator"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 105
    .local v0, "calcStr":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 107
    :cond_0
    const-string v1, "haversine"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    new-instance v1, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Haversine;

    invoke-direct {v1}, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Haversine;-><init>()V

    iput-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    goto :goto_0

    .line 109
    :cond_1
    const-string v1, "lawOfCosines"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 110
    new-instance v1, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$LawOfCosines;

    invoke-direct {v1}, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$LawOfCosines;-><init>()V

    iput-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    goto :goto_0

    .line 111
    :cond_2
    const-string/jumbo v1, "vincentySphere"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 112
    new-instance v1, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Vincenty;

    invoke-direct {v1}, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Vincenty;-><init>()V

    iput-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    goto :goto_0

    .line 113
    :cond_3
    const-string v1, "cartesian"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 114
    new-instance v1, Lcom/spatial4j/core/distance/CartesianDistCalc;

    invoke-direct {v1}, Lcom/spatial4j/core/distance/CartesianDistCalc;-><init>()V

    iput-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    goto :goto_0

    .line 115
    :cond_4
    const-string v1, "cartesian^2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 116
    new-instance v1, Lcom/spatial4j/core/distance/CartesianDistCalc;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/spatial4j/core/distance/CartesianDistCalc;-><init>(Z)V

    iput-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    goto :goto_0

    .line 118
    :cond_5
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown calculator: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected initUnits()V
    .locals 3

    .prologue
    .line 98
    iget-object v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->args:Ljava/util/Map;

    const-string v2, "geo"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    .local v0, "geoStr":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 100
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->geo:Z

    .line 101
    :cond_0
    return-void
.end method

.method protected initWorldBounds()V
    .locals 5

    .prologue
    .line 123
    iget-object v2, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->args:Ljava/util/Map;

    const-string/jumbo v3, "worldBounds"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 124
    .local v1, "worldBoundsStr":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    new-instance v0, Lcom/spatial4j/core/context/SpatialContext;

    iget-boolean v2, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->geo:Z

    iget-object v3, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/spatial4j/core/context/SpatialContext;-><init>(ZLcom/spatial4j/core/distance/DistanceCalculator;Lcom/spatial4j/core/shape/Rectangle;)V

    .line 129
    .local v0, "simpleCtx":Lcom/spatial4j/core/context/SpatialContext;
    invoke-virtual {v0, v1}, Lcom/spatial4j/core/context/SpatialContext;->readShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v2

    check-cast v2, Lcom/spatial4j/core/shape/Rectangle;

    iput-object v2, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->worldBounds:Lcom/spatial4j/core/shape/Rectangle;

    goto :goto_0
.end method

.method protected newSpatialContext()Lcom/spatial4j/core/context/SpatialContext;
    .locals 4

    .prologue
    .line 134
    new-instance v0, Lcom/spatial4j/core/context/SpatialContext;

    iget-boolean v1, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->geo:Z

    iget-object v2, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->calculator:Lcom/spatial4j/core/distance/DistanceCalculator;

    iget-object v3, p0, Lcom/spatial4j/core/context/SpatialContextFactory;->worldBounds:Lcom/spatial4j/core/shape/Rectangle;

    invoke-direct {v0, v1, v2, v3}, Lcom/spatial4j/core/context/SpatialContext;-><init>(ZLcom/spatial4j/core/distance/DistanceCalculator;Lcom/spatial4j/core/shape/Rectangle;)V

    return-object v0
.end method
