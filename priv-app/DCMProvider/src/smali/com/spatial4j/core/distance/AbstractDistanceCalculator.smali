.class public abstract Lcom/spatial4j/core/distance/AbstractDistanceCalculator;
.super Ljava/lang/Object;
.source "AbstractDistanceCalculator.java"

# interfaces
.implements Lcom/spatial4j/core/distance/DistanceCalculator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public distance(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)D
    .locals 6
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "to"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 28
    invoke-interface {p2}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    invoke-interface {p2}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/spatial4j/core/distance/AbstractDistanceCalculator;->distance(Lcom/spatial4j/core/shape/Point;DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
