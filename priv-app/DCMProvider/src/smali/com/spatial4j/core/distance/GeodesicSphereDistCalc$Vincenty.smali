.class public Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Vincenty;
.super Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;
.source "GeodesicSphereDistCalc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Vincenty"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;-><init>()V

    return-void
.end method


# virtual methods
.method protected distanceLatLonRAD(DDDD)D
    .locals 3
    .param p1, "lat1"    # D
    .param p3, "lon1"    # D
    .param p5, "lat2"    # D
    .param p7, "lon2"    # D

    .prologue
    .line 119
    invoke-static/range {p1 .. p8}, Lcom/spatial4j/core/distance/DistanceUtils;->distVincentyRAD(DDDD)D

    move-result-wide v0

    return-wide v0
.end method
