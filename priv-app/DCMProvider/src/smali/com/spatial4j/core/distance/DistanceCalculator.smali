.class public interface abstract Lcom/spatial4j/core/distance/DistanceCalculator;
.super Ljava/lang/Object;
.source "DistanceCalculator.java"


# virtual methods
.method public abstract area(Lcom/spatial4j/core/shape/Circle;)D
.end method

.method public abstract area(Lcom/spatial4j/core/shape/Rectangle;)D
.end method

.method public abstract calcBoxByDistFromPt(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;
.end method

.method public abstract calcBoxByDistFromPt_yHorizAxisDEG(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)D
.end method

.method public abstract distance(Lcom/spatial4j/core/shape/Point;DD)D
.end method

.method public abstract distance(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;)D
.end method

.method public abstract pointOnBearing(Lcom/spatial4j/core/shape/Point;DDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Point;
.end method
