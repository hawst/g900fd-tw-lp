.class public Lcom/spatial4j/core/distance/CartesianDistCalc;
.super Lcom/spatial4j/core/distance/AbstractDistanceCalculator;
.source "CartesianDistCalc.java"


# instance fields
.field private final squared:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/spatial4j/core/distance/AbstractDistanceCalculator;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/spatial4j/core/distance/CartesianDistCalc;->squared:Z

    .line 34
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1, "squared"    # Z

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/spatial4j/core/distance/AbstractDistanceCalculator;-><init>()V

    .line 44
    iput-boolean p1, p0, Lcom/spatial4j/core/distance/CartesianDistCalc;->squared:Z

    .line 45
    return-void
.end method


# virtual methods
.method public area(Lcom/spatial4j/core/shape/Circle;)D
    .locals 2
    .param p1, "circle"    # Lcom/spatial4j/core/shape/Circle;

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/spatial4j/core/shape/Circle;->getArea(Lcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v0

    return-wide v0
.end method

.method public area(Lcom/spatial4j/core/shape/Rectangle;)D
    .locals 2
    .param p1, "rect"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/spatial4j/core/shape/Rectangle;->getArea(Lcom/spatial4j/core/context/SpatialContext;)D

    move-result-wide v0

    return-wide v0
.end method

.method public calcBoxByDistFromPt(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;
    .locals 10
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distDEG"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p5, "reuse"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 84
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    sub-double v2, v0, p2

    .line 85
    .local v2, "minX":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    add-double v4, v0, p2

    .line 86
    .local v4, "maxX":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    sub-double v6, v0, p2

    .line 87
    .local v6, "minY":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    add-double v8, v0, p2

    .line 88
    .local v8, "maxY":D
    if-nez p5, :cond_0

    move-object v1, p4

    .line 89
    invoke-virtual/range {v1 .. v9}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object p5

    .line 92
    .end local p5    # "reuse":Lcom/spatial4j/core/shape/Rectangle;
    :goto_0
    return-object p5

    .restart local p5    # "reuse":Lcom/spatial4j/core/shape/Rectangle;
    :cond_0
    move-object v1, p5

    .line 91
    invoke-interface/range {v1 .. v9}, Lcom/spatial4j/core/shape/Rectangle;->reset(DDDD)V

    goto :goto_0
.end method

.method public calcBoxByDistFromPt_yHorizAxisDEG(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)D
    .locals 2
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distDEG"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 98
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    return-wide v0
.end method

.method public distance(Lcom/spatial4j/core/shape/Point;DD)D
    .locals 6
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "toX"    # D
    .param p4, "toY"    # D

    .prologue
    .line 49
    const-wide/16 v0, 0x0

    .line 51
    .local v0, "result":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    sub-double v2, v4, p2

    .line 52
    .local v2, "v":D
    mul-double v4, v2, v2

    add-double/2addr v0, v4

    .line 54
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    sub-double v2, v4, p4

    .line 55
    mul-double v4, v2, v2

    add-double/2addr v0, v4

    .line 57
    iget-boolean v4, p0, Lcom/spatial4j/core/distance/CartesianDistCalc;->squared:Z

    if-eqz v4, :cond_0

    .line 60
    .end local v0    # "result":D
    :goto_0
    return-wide v0

    .restart local v0    # "result":D
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 114
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 116
    check-cast v0, Lcom/spatial4j/core/distance/CartesianDistCalc;

    .line 118
    .local v0, "that":Lcom/spatial4j/core/distance/CartesianDistCalc;
    iget-boolean v3, p0, Lcom/spatial4j/core/distance/CartesianDistCalc;->squared:Z

    iget-boolean v4, v0, Lcom/spatial4j/core/distance/CartesianDistCalc;->squared:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/spatial4j/core/distance/CartesianDistCalc;->squared:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pointOnBearing(Lcom/spatial4j/core/shape/Point;DDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Point;
    .locals 12
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distDEG"    # D
    .param p4, "bearingDEG"    # D
    .param p6, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p7, "reuse"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 65
    const-wide/16 v8, 0x0

    cmpl-double v8, p2, v8

    if-nez v8, :cond_1

    .line 66
    if-nez p7, :cond_0

    move-object/from16 p7, p1

    .line 78
    .end local p7    # "reuse":Lcom/spatial4j/core/shape/Point;
    :goto_0
    return-object p7

    .line 68
    .restart local p7    # "reuse":Lcom/spatial4j/core/shape/Point;
    :cond_0
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v8

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v10

    move-object/from16 v0, p7

    invoke-interface {v0, v8, v9, v10, v11}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    goto :goto_0

    .line 71
    :cond_1
    invoke-static/range {p4 .. p5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v2

    .line 72
    .local v2, "bearingRAD":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v10, p2

    add-double v4, v8, v10

    .line 73
    .local v4, "x":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v10, p2

    add-double v6, v8, v10

    .line 74
    .local v6, "y":D
    if-nez p7, :cond_2

    .line 75
    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object p7

    goto :goto_0

    .line 77
    :cond_2
    move-object/from16 v0, p7

    invoke-interface {v0, v4, v5, v6, v7}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    goto :goto_0
.end method
