.class public Lcom/spatial4j/core/distance/DistanceUtils;
.super Ljava/lang/Object;
.source "DistanceUtils.java"


# static fields
.field public static final DEGREES_TO_RADIANS:D = 0.017453292519943295

.field public static final DEG_180_AS_RADS:D = 3.141592653589793

.field public static final DEG_225_AS_RADS:D = 3.9269908169872414

.field public static final DEG_270_AS_RADS:D = 4.71238898038469

.field public static final DEG_45_AS_RADS:D = 0.7853981633974483

.field public static final DEG_90_AS_RADS:D = 1.5707963267948966

.field public static final DEG_TO_KM:D = 111.19507973436875

.field public static final EARTH_EQUATORIAL_RADIUS_KM:D = 6378.137

.field public static final EARTH_EQUATORIAL_RADIUS_MI:D = 3963.190590429304

.field public static final EARTH_MEAN_RADIUS_KM:D = 6371.0087714

.field public static final EARTH_MEAN_RADIUS_MI:D = 3958.7613145272735

.field public static final KM_TO_DEG:D = 0.008993203677616635

.field public static final KM_TO_MILES:D = 0.621371192

.field public static final MILES_TO_KM:D = 1.6093440006146922

.field public static final RADIANS_TO_DEGREES:D = 57.29577951308232

.field public static final SIN_45_AS_RADS:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-wide v0, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    sput-wide v0, Lcom/spatial4j/core/distance/DistanceUtils;->SIN_45_AS_RADS:D

    .line 58
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calcBoxByDistFromPtDEG(DDDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;
    .locals 12
    .param p0, "lat"    # D
    .param p2, "lon"    # D
    .param p4, "distDEG"    # D
    .param p6, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p7, "reuse"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 231
    const-wide/16 v0, 0x0

    cmpl-double v0, p4, v0

    if-nez v0, :cond_1

    .line 232
    move-wide v2, p2

    .local v2, "minX":D
    move-wide v4, p2

    .local v4, "maxX":D
    move-wide v6, p0

    .local v6, "minY":D
    move-wide v8, p0

    .line 260
    .local v8, "maxY":D
    :cond_0
    :goto_0
    if-nez p7, :cond_7

    move-object/from16 v1, p6

    .line 261
    invoke-virtual/range {v1 .. v9}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object p7

    .line 264
    .end local p7    # "reuse":Lcom/spatial4j/core/shape/Rectangle;
    :goto_1
    return-object p7

    .line 233
    .end local v2    # "minX":D
    .end local v4    # "maxX":D
    .end local v6    # "minY":D
    .end local v8    # "maxY":D
    .restart local p7    # "reuse":Lcom/spatial4j/core/shape/Rectangle;
    :cond_1
    const-wide v0, 0x4066800000000000L    # 180.0

    cmpl-double v0, p4, v0

    if-ltz v0, :cond_2

    .line 234
    const-wide v2, -0x3f99800000000000L    # -180.0

    .restart local v2    # "minX":D
    const-wide v4, 0x4066800000000000L    # 180.0

    .restart local v4    # "maxX":D
    const-wide v6, -0x3fa9800000000000L    # -90.0

    .restart local v6    # "minY":D
    const-wide v8, 0x4056800000000000L    # 90.0

    .line 235
    .restart local v8    # "maxY":D
    goto :goto_0

    .line 238
    .end local v2    # "minX":D
    .end local v4    # "maxX":D
    .end local v6    # "minY":D
    .end local v8    # "maxY":D
    :cond_2
    add-double v8, p0, p4

    .line 239
    .restart local v8    # "maxY":D
    sub-double v6, p0, p4

    .line 241
    .restart local v6    # "minY":D
    const-wide v0, 0x4056800000000000L    # 90.0

    cmpl-double v0, v8, v0

    if-gez v0, :cond_3

    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpg-double v0, v6, v0

    if-gtz v0, :cond_6

    .line 243
    :cond_3
    const-wide v2, -0x3f99800000000000L    # -180.0

    .restart local v2    # "minX":D
    const-wide v4, 0x4066800000000000L    # 180.0

    .line 244
    .restart local v4    # "maxX":D
    const-wide v0, 0x4056800000000000L    # 90.0

    cmpg-double v0, v8, v0

    if-gtz v0, :cond_4

    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpl-double v0, v6, v0

    if-ltz v0, :cond_4

    .line 245
    const-wide v0, 0x4056800000000000L    # 90.0

    sub-double v0, p2, v0

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v2

    .line 246
    const-wide v0, 0x4056800000000000L    # 90.0

    add-double/2addr v0, p2

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v4

    .line 248
    :cond_4
    const-wide v0, 0x4056800000000000L    # 90.0

    cmpl-double v0, v8, v0

    if-lez v0, :cond_5

    .line 249
    const-wide v8, 0x4056800000000000L    # 90.0

    .line 250
    :cond_5
    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpg-double v0, v6, v0

    if-gez v0, :cond_0

    .line 251
    const-wide v6, -0x3fa9800000000000L    # -90.0

    .line 252
    goto/16 :goto_0

    .line 254
    .end local v2    # "minX":D
    .end local v4    # "maxX":D
    :cond_6
    invoke-static/range {p0 .. p5}, Lcom/spatial4j/core/distance/DistanceUtils;->calcBoxByDistFromPt_deltaLonDEG(DDD)D

    move-result-wide v10

    .line 256
    .local v10, "lon_delta_deg":D
    sub-double v0, p2, v10

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v2

    .line 257
    .restart local v2    # "minX":D
    add-double v0, p2, v10

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->normLonDEG(D)D

    move-result-wide v4

    .restart local v4    # "maxX":D
    goto/16 :goto_0

    .end local v10    # "lon_delta_deg":D
    :cond_7
    move-object/from16 v1, p7

    .line 263
    invoke-interface/range {v1 .. v9}, Lcom/spatial4j/core/shape/Rectangle;->reset(DDDD)V

    goto/16 :goto_1
.end method

.method public static calcBoxByDistFromPt_deltaLonDEG(DDD)D
    .locals 10
    .param p0, "lat"    # D
    .param p2, "lon"    # D
    .param p4, "distDEG"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 274
    cmpl-double v8, p4, v6

    if-nez v8, :cond_0

    .line 282
    :goto_0
    return-wide v6

    .line 276
    :cond_0
    invoke-static {p0, p1}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v2

    .line 277
    .local v2, "lat_rad":D
    invoke-static {p4, p5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v0

    .line 278
    .local v0, "dist_rad":D
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    .line 280
    .local v4, "result_rad":D
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-nez v6, :cond_1

    .line 281
    invoke-static {v4, v5}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v6

    goto :goto_0

    .line 282
    :cond_1
    const-wide v6, 0x4056800000000000L    # 90.0

    goto :goto_0
.end method

.method public static calcBoxByDistFromPt_latHorizAxisDEG(DDD)D
    .locals 10
    .param p0, "lat"    # D
    .param p2, "lon"    # D
    .param p4, "distDEG"    # D

    .prologue
    .line 294
    const-wide/16 v6, 0x0

    cmpl-double v6, p4, v6

    if-nez v6, :cond_1

    .line 305
    .end local p0    # "lat":D
    :cond_0
    :goto_0
    return-wide p0

    .line 296
    .restart local p0    # "lat":D
    :cond_1
    invoke-static {p0, p1}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v2

    .line 297
    .local v2, "lat_rad":D
    invoke-static {p4, p5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v0

    .line 298
    .local v0, "dist_rad":D
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    .line 299
    .local v4, "result_rad":D
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-nez v6, :cond_2

    .line 300
    invoke-static {v4, v5}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide p0

    goto :goto_0

    .line 301
    :cond_2
    const-wide/16 v6, 0x0

    cmpl-double v6, p0, v6

    if-lez v6, :cond_3

    .line 302
    const-wide p0, 0x4056800000000000L    # 90.0

    goto :goto_0

    .line 303
    :cond_3
    const-wide/16 v6, 0x0

    cmpg-double v6, p0, v6

    if-gez v6, :cond_0

    .line 304
    const-wide p0, -0x3fa9800000000000L    # -90.0

    goto :goto_0
.end method

.method public static calcLonDegreesAtLat(DD)D
    .locals 20
    .param p0, "lat"    # D
    .param p2, "dist"    # D

    .prologue
    .line 334
    invoke-static/range {p2 .. p3}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v4

    .line 335
    .local v4, "distanceRAD":D
    invoke-static/range {p0 .. p1}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v12

    .line 337
    .local v12, "startLat":D
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 338
    .local v0, "cosAngDist":D
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 339
    .local v2, "cosStartLat":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    .line 340
    .local v8, "sinAngDist":D
    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 342
    .local v10, "sinStartLat":D
    mul-double v14, v8, v2

    .line 343
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    mul-double v18, v10, v10

    sub-double v16, v16, v18

    mul-double v16, v16, v0

    .line 342
    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    .line 345
    .local v6, "lonDelta":D
    invoke-static {v6, v7}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v14

    return-wide v14
.end method

.method public static degrees2Dist(DD)D
    .locals 2
    .param p0, "degrees"    # D
    .param p2, "radius"    # D

    .prologue
    .line 466
    invoke-static {p0, p1}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1, p2, p3}, Lcom/spatial4j/core/distance/DistanceUtils;->radians2Dist(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static dist2Degrees(DD)D
    .locals 2
    .param p0, "dist"    # D
    .param p2, "radius"    # D

    .prologue
    .line 457
    invoke-static {p0, p1, p2, p3}, Lcom/spatial4j/core/distance/DistanceUtils;->dist2Radians(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static dist2Radians(DD)D
    .locals 2
    .param p0, "dist"    # D
    .param p2, "radius"    # D

    .prologue
    .line 474
    div-double v0, p0, p2

    return-wide v0
.end method

.method public static distHaversineRAD(DDDD)D
    .locals 12
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D

    .prologue
    .line 377
    cmpl-double v6, p0, p4

    if-nez v6, :cond_0

    cmpl-double v6, p2, p6

    if-nez v6, :cond_0

    .line 378
    const-wide/16 v6, 0x0

    .line 383
    :goto_0
    return-wide v6

    .line 379
    :cond_0
    sub-double v6, p2, p6

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 380
    .local v2, "hsinX":D
    sub-double v6, p0, p4

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 381
    .local v4, "hsinY":D
    mul-double v6, v4, v4

    .line 382
    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    mul-double/2addr v8, v2

    mul-double/2addr v8, v2

    .line 381
    add-double v0, v6, v8

    .line 383
    .local v0, "h":D
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v10, v0

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    goto :goto_0
.end method

.method public static distLawOfCosinesRAD(DDDD)D
    .locals 14
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D

    .prologue
    .line 404
    cmpl-double v8, p0, p4

    if-nez v8, :cond_0

    cmpl-double v8, p2, p6

    if-nez v8, :cond_0

    .line 405
    const-wide/16 v8, 0x0

    .line 422
    :goto_0
    return-wide v8

    .line 409
    :cond_0
    sub-double v6, p6, p2

    .line 411
    .local v6, "dLon":D
    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v0, v8, p0

    .line 412
    .local v0, "a":D
    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v2, v8, p4

    .line 413
    .local v2, "c":D
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    .line 414
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    .line 413
    add-double v4, v8, v10

    .line 417
    .local v4, "cosB":D
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpg-double v8, v4, v8

    if-gez v8, :cond_1

    .line 418
    const-wide v8, 0x400921fb54442d18L    # Math.PI

    goto :goto_0

    .line 419
    :cond_1
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v8, v4, v8

    if-ltz v8, :cond_2

    .line 420
    const-wide/16 v8, 0x0

    goto :goto_0

    .line 422
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v8

    goto :goto_0
.end method

.method public static distSquaredCartesian([D[D)D
    .locals 10
    .param p0, "vec1"    # [D
    .param p1, "vec2"    # [D

    .prologue
    .line 357
    const-wide/16 v2, 0x0

    .line 358
    .local v2, "result":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_0

    .line 362
    return-wide v2

    .line 359
    :cond_0
    aget-wide v6, p0, v0

    aget-wide v8, p1, v0

    sub-double v4, v6, v8

    .line 360
    .local v4, "v":D
    mul-double v6, v4, v4

    add-double/2addr v2, v6

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static distVincentyRAD(DDDD)D
    .locals 26
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D

    .prologue
    .line 434
    cmpl-double v22, p0, p4

    if-nez v22, :cond_0

    cmpl-double v22, p2, p6

    if-nez v22, :cond_0

    .line 435
    const-wide/16 v22, 0x0

    .line 449
    :goto_0
    return-wide v22

    .line 437
    :cond_0
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 438
    .local v10, "cosLat1":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    .line 439
    .local v12, "cosLat2":D
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    .line 440
    .local v18, "sinLat1":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    .line 441
    .local v20, "sinLat2":D
    sub-double v14, p6, p2

    .line 442
    .local v14, "dLon":D
    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    .line 443
    .local v8, "cosDLon":D
    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    .line 445
    .local v16, "sinDLon":D
    mul-double v2, v12, v16

    .line 446
    .local v2, "a":D
    mul-double v22, v10, v20

    mul-double v24, v18, v12

    mul-double v24, v24, v8

    sub-double v4, v22, v24

    .line 447
    .local v4, "b":D
    mul-double v22, v18, v20

    mul-double v24, v10, v12

    mul-double v24, v24, v8

    add-double v6, v22, v24

    .line 449
    .local v6, "c":D
    mul-double v22, v2, v2

    mul-double v24, v4, v4

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v22

    goto :goto_0
.end method

.method public static normLatDEG(D)D
    .locals 8
    .param p0, "lat_deg"    # D

    .prologue
    const-wide v6, 0x4076800000000000L    # 360.0

    const-wide v4, 0x4056800000000000L    # 90.0

    .line 217
    const-wide v2, -0x3fa9800000000000L    # -90.0

    cmpl-double v2, p0, v2

    if-ltz v2, :cond_0

    cmpg-double v2, p0, v4

    if-gtz v2, :cond_0

    .line 220
    .end local p0    # "lat_deg":D
    :goto_0
    return-wide p0

    .line 219
    .restart local p0    # "lat_deg":D
    :cond_0
    add-double v2, p0, v4

    rem-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 220
    .local v0, "off":D
    const-wide v2, 0x4066800000000000L    # 180.0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_1

    .end local v0    # "off":D
    :goto_1
    sub-double p0, v0, v4

    goto :goto_0

    .restart local v0    # "off":D
    :cond_1
    sub-double v0, v6, v0

    goto :goto_1
.end method

.method public static normLonDEG(D)D
    .locals 12
    .param p0, "lon_deg"    # D

    .prologue
    const-wide v10, -0x3f99800000000000L    # -180.0

    const-wide/16 v8, 0x0

    const-wide v2, 0x4066800000000000L    # 180.0

    .line 202
    cmpl-double v4, p0, v10

    if-ltz v4, :cond_0

    cmpg-double v4, p0, v2

    if-gtz v4, :cond_0

    .line 210
    .end local p0    # "lon_deg":D
    .local v0, "off":D
    :goto_0
    return-wide p0

    .line 204
    .end local v0    # "off":D
    .restart local p0    # "lon_deg":D
    :cond_0
    add-double v4, p0, v2

    const-wide v6, 0x4076800000000000L    # 360.0

    rem-double v0, v4, v6

    .line 205
    .restart local v0    # "off":D
    cmpg-double v4, v0, v8

    if-gez v4, :cond_1

    .line 206
    add-double p0, v2, v0

    goto :goto_0

    .line 207
    :cond_1
    cmpl-double v4, v0, v8

    if-nez v4, :cond_2

    cmpl-double v4, p0, v8

    if-lez v4, :cond_2

    move-wide p0, v2

    .line 208
    goto :goto_0

    .line 210
    :cond_2
    add-double p0, v10, v0

    goto :goto_0
.end method

.method public static pointOnBearingRAD(DDDDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Point;
    .locals 22
    .param p0, "startLat"    # D
    .param p2, "startLon"    # D
    .param p4, "distanceRAD"    # D
    .param p6, "bearingRAD"    # D
    .param p8, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p9, "reuse"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 156
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 157
    .local v2, "cosAngDist":D
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 158
    .local v4, "cosStartLat":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 159
    .local v10, "sinAngDist":D
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 160
    .local v14, "sinStartLat":D
    mul-double v16, v14, v2

    .line 161
    mul-double v18, v4, v10

    invoke-static/range {p6 .. p7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    .line 160
    add-double v12, v16, v18

    .line 162
    .local v12, "sinLat2":D
    invoke-static {v12, v13}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    .line 163
    .local v6, "lat2":D
    invoke-static/range {p6 .. p7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v16, v16, v10

    mul-double v16, v16, v4

    .line 164
    mul-double v18, v14, v12

    sub-double v18, v2, v18

    .line 163
    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v16

    add-double v8, p2, v16

    .line 167
    .local v8, "lon2":D
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    cmpl-double v16, v8, v16

    if-lez v16, :cond_2

    .line 168
    const-wide/high16 v16, -0x4010000000000000L    # -1.0

    const-wide v18, 0x400921fb54442d18L    # Math.PI

    const-wide v20, 0x400921fb54442d18L    # Math.PI

    sub-double v20, v8, v20

    sub-double v18, v18, v20

    mul-double v8, v16, v18

    .line 174
    :cond_0
    :goto_0
    const-wide v16, 0x3ff921fb54442d18L    # 1.5707963267948966

    cmpl-double v16, v6, v16

    if-lez v16, :cond_4

    .line 175
    const-wide v16, 0x3ff921fb54442d18L    # 1.5707963267948966

    const-wide v18, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v6, v18

    sub-double v6, v16, v18

    .line 176
    const-wide/16 v16, 0x0

    cmpg-double v16, v8, v16

    if-gez v16, :cond_3

    .line 177
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    add-double v8, v8, v16

    .line 190
    :cond_1
    :goto_1
    if-nez p9, :cond_6

    .line 191
    move-object/from16 v0, p8

    invoke-virtual {v0, v8, v9, v6, v7}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object p9

    .line 194
    .end local p9    # "reuse":Lcom/spatial4j/core/shape/Point;
    :goto_2
    return-object p9

    .line 169
    .restart local p9    # "reuse":Lcom/spatial4j/core/shape/Point;
    :cond_2
    const-wide v16, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v16, v8, v16

    if-gez v16, :cond_0

    .line 170
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    add-double v16, v16, v8

    const-wide v18, 0x400921fb54442d18L    # Math.PI

    add-double v8, v16, v18

    goto :goto_0

    .line 179
    :cond_3
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    sub-double v8, v8, v16

    .line 181
    goto :goto_1

    :cond_4
    const-wide v16, -0x4006de04abbbd2e8L    # -1.5707963267948966

    cmpg-double v16, v6, v16

    if-gez v16, :cond_1

    .line 182
    const-wide v16, -0x4006de04abbbd2e8L    # -1.5707963267948966

    const-wide v18, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double v18, v18, v6

    sub-double v6, v16, v18

    .line 183
    const-wide/16 v16, 0x0

    cmpg-double v16, v8, v16

    if-gez v16, :cond_5

    .line 184
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    add-double v8, v8, v16

    .line 185
    goto :goto_1

    .line 186
    :cond_5
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    sub-double v8, v8, v16

    goto :goto_1

    .line 193
    :cond_6
    move-object/from16 v0, p9

    invoke-interface {v0, v8, v9, v6, v7}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    goto :goto_2
.end method

.method public static radians2Dist(DD)D
    .locals 2
    .param p0, "radians"    # D
    .param p2, "radius"    # D

    .prologue
    .line 482
    mul-double v0, p0, p2

    return-wide v0
.end method

.method public static toDegrees(D)D
    .locals 2
    .param p0, "radians"    # D

    .prologue
    .line 498
    const-wide v0, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, p0

    return-wide v0
.end method

.method public static toRadians(D)D
    .locals 2
    .param p0, "degrees"    # D

    .prologue
    .line 490
    const-wide v0, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v0, p0

    return-wide v0
.end method

.method public static vectorBoxCorner([D[DDZ)[D
    .locals 4
    .param p0, "center"    # [D
    .param p1, "result"    # [D
    .param p2, "distance"    # D
    .param p4, "upperRight"    # Z

    .prologue
    .line 124
    if-eqz p1, :cond_0

    array-length v1, p1

    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 125
    :cond_0
    array-length v1, p0

    new-array p1, v1, [D

    .line 127
    :cond_1
    if-nez p4, :cond_2

    .line 128
    neg-double p2, p2

    .line 133
    :cond_2
    sget-wide v2, Lcom/spatial4j/core/distance/DistanceUtils;->SIN_45_AS_RADS:D

    mul-double/2addr p2, v2

    .line 134
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_3

    .line 137
    return-object p1

    .line 135
    :cond_3
    aget-wide v2, p0, v0

    add-double/2addr v2, p2

    aput-wide v2, p1, v0

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static vectorDistance([D[DD)D
    .locals 6
    .param p0, "vec1"    # [D
    .param p1, "vec2"    # [D
    .param p2, "power"    # D

    .prologue
    .line 74
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    div-double v4, v0, p2

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceUtils;->vectorDistance([D[DDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static vectorDistance([D[DDD)D
    .locals 10
    .param p0, "vec1"    # [D
    .param p1, "vec2"    # [D
    .param p2, "power"    # D
    .param p4, "oneOverPower"    # D

    .prologue
    const-wide/16 v8, 0x0

    .line 87
    const-wide/16 v2, 0x0

    .line 89
    .local v2, "result":D
    cmpl-double v1, p2, v8

    if-nez v1, :cond_3

    .line 90
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_1

    .line 110
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return-wide v2

    .line 91
    .restart local v0    # "i":I
    :cond_1
    aget-wide v4, p0, v0

    aget-wide v6, p1, v0

    sub-double/2addr v4, v6

    cmpl-double v1, v4, v8

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    int-to-double v4, v1

    add-double/2addr v2, v4

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_2
    const/4 v1, 0x1

    goto :goto_2

    .line 94
    .end local v0    # "i":I
    :cond_3
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, p2, v4

    if-nez v1, :cond_4

    .line 95
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 96
    aget-wide v4, p0, v0

    aget-wide v6, p1, v0

    sub-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 98
    .end local v0    # "i":I
    :cond_4
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    cmpl-double v1, p2, v4

    if-nez v1, :cond_5

    .line 99
    invoke-static {p0, p1}, Lcom/spatial4j/core/distance/DistanceUtils;->distSquaredCartesian([D[D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 100
    goto :goto_1

    :cond_5
    const-wide v4, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v1, p2, v4

    if-eqz v1, :cond_6

    invoke-static {p2, p3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 101
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 102
    aget-wide v4, p0, v0

    aget-wide v6, p1, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 105
    .end local v0    # "i":I
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    array-length v1, p0

    if-lt v0, v1, :cond_8

    .line 108
    invoke-static {v2, v3, p4, p5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    goto :goto_1

    .line 106
    :cond_8
    aget-wide v4, p0, v0

    aget-wide v6, p1, v0

    sub-double/2addr v4, v6

    invoke-static {v4, v5, p2, p3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method
