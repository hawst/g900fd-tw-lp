.class public abstract Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;
.super Lcom/spatial4j/core/distance/AbstractDistanceCalculator;
.source "GeodesicSphereDistCalc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Haversine;,
        Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$LawOfCosines;,
        Lcom/spatial4j/core/distance/GeodesicSphereDistCalc$Vincenty;
    }
.end annotation


# static fields
.field private static final radiusDEG:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v0

    sput-wide v0, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;->radiusDEG:D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/spatial4j/core/distance/AbstractDistanceCalculator;-><init>()V

    return-void
.end method


# virtual methods
.method public area(Lcom/spatial4j/core/shape/Circle;)D
    .locals 8
    .param p1, "circle"    # Lcom/spatial4j/core/shape/Circle;

    .prologue
    .line 74
    const-wide v2, 0x4056800000000000L    # 90.0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v0

    .line 75
    .local v0, "lat":D
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    sget-wide v4, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;->radiusDEG:D

    mul-double/2addr v2, v4

    sget-wide v4, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;->radiusDEG:D

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    return-wide v2
.end method

.method public area(Lcom/spatial4j/core/shape/Rectangle;)D
    .locals 10
    .param p1, "rect"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 64
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v0

    .line 65
    .local v0, "lat1":D
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v2

    .line 66
    .local v2, "lat2":D
    const-wide v4, 0x3f91df46a2529d39L    # 0.017453292519943295

    sget-wide v6, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;->radiusDEG:D

    mul-double/2addr v4, v6

    sget-wide v6, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;->radiusDEG:D

    mul-double/2addr v4, v6

    .line 67
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    .line 66
    mul-double/2addr v4, v6

    .line 68
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Rectangle;->getWidth()D

    move-result-wide v6

    .line 66
    mul-double/2addr v4, v6

    return-wide v4
.end method

.method public calcBoxByDistFromPt(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;
    .locals 8
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distDEG"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p5, "reuse"    # Lcom/spatial4j/core/shape/Rectangle;

    .prologue
    .line 53
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/spatial4j/core/distance/DistanceUtils;->calcBoxByDistFromPtDEG(DDDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Rectangle;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public calcBoxByDistFromPt_yHorizAxisDEG(Lcom/spatial4j/core/shape/Point;DLcom/spatial4j/core/context/SpatialContext;)D
    .locals 6
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distDEG"    # D
    .param p4, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 58
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/spatial4j/core/distance/DistanceUtils;->calcBoxByDistFromPt_latHorizAxisDEG(DDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final distance(Lcom/spatial4j/core/shape/Point;DD)D
    .locals 10
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "toX"    # D
    .param p4, "toY"    # D

    .prologue
    .line 92
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v4

    invoke-static {p4, p5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v6

    invoke-static {p2, p3}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lcom/spatial4j/core/distance/GeodesicSphereDistCalc;->distanceLatLonRAD(DDDD)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v0

    return-wide v0
.end method

.method protected abstract distanceLatLonRAD(DDDD)D
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 80
    if-nez p1, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public pointOnBearing(Lcom/spatial4j/core/shape/Point;DDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Point;
    .locals 14
    .param p1, "from"    # Lcom/spatial4j/core/shape/Point;
    .param p2, "distDEG"    # D
    .param p4, "bearingDEG"    # D
    .param p6, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;
    .param p7, "reuse"    # Lcom/spatial4j/core/shape/Point;

    .prologue
    .line 37
    const-wide/16 v2, 0x0

    cmpl-double v2, p2, v2

    if-nez v2, :cond_1

    .line 38
    if-nez p7, :cond_0

    .line 48
    .end local p1    # "from":Lcom/spatial4j/core/shape/Point;
    :goto_0
    return-object p1

    .line 40
    .restart local p1    # "from":Lcom/spatial4j/core/shape/Point;
    :cond_0
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    move-object/from16 p1, p7

    .line 41
    goto :goto_0

    .line 44
    :cond_1
    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v2

    invoke-interface {p1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v4

    .line 45
    invoke-static/range {p2 .. p3}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v6

    .line 46
    invoke-static/range {p4 .. p5}, Lcom/spatial4j/core/distance/DistanceUtils;->toRadians(D)D

    move-result-wide v8

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    .line 43
    invoke-static/range {v2 .. v11}, Lcom/spatial4j/core/distance/DistanceUtils;->pointOnBearingRAD(DDDDLcom/spatial4j/core/context/SpatialContext;Lcom/spatial4j/core/shape/Point;)Lcom/spatial4j/core/shape/Point;

    move-result-object v12

    .line 47
    .local v12, "result":Lcom/spatial4j/core/shape/Point;
    invoke-interface {v12}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v2

    invoke-interface {v12}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/spatial4j/core/distance/DistanceUtils;->toDegrees(D)D

    move-result-wide v4

    invoke-interface {v12, v2, v3, v4, v5}, Lcom/spatial4j/core/shape/Point;->reset(DD)V

    move-object p1, v12

    .line 48
    goto :goto_0
.end method
