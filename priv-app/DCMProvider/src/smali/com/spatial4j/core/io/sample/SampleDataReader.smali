.class public Lcom/spatial4j/core/io/sample/SampleDataReader;
.super Lcom/spatial4j/core/io/LineReader;
.source "SampleDataReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/spatial4j/core/io/LineReader",
        "<",
        "Lcom/spatial4j/core/io/sample/SampleData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0
    .param p1, "f"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/spatial4j/core/io/LineReader;-><init>(Ljava/io/File;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "r"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/spatial4j/core/io/LineReader;-><init>(Ljava/io/InputStream;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "r"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/spatial4j/core/io/LineReader;-><init>(Ljava/io/Reader;)V

    .line 35
    return-void
.end method


# virtual methods
.method public parseLine(Ljava/lang/String;)Lcom/spatial4j/core/io/sample/SampleData;
    .locals 1
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Lcom/spatial4j/core/io/sample/SampleData;

    invoke-direct {v0, p1}, Lcom/spatial4j/core/io/sample/SampleData;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic parseLine(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/io/sample/SampleDataReader;->parseLine(Ljava/lang/String;)Lcom/spatial4j/core/io/sample/SampleData;

    move-result-object v0

    return-object v0
.end method
