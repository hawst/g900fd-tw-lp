.class public abstract Lcom/spatial4j/core/io/LineReader;
.super Ljava/lang/Object;
.source "LineReader.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private count:I

.field private lineNumber:I

.field private nextLine:Ljava/lang/String;

.field private reader:Ljava/io/BufferedReader;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 4
    .param p1, "f"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/spatial4j/core/io/LineReader;->count:I

    .line 26
    iput v0, p0, Lcom/spatial4j/core/io/LineReader;->lineNumber:I

    .line 52
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    .line 53
    invoke-virtual {p0}, Lcom/spatial4j/core/io/LineReader;->next()Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/spatial4j/core/io/LineReader;->count:I

    .line 26
    iput v0, p0, Lcom/spatial4j/core/io/LineReader;->lineNumber:I

    .line 37
    new-instance v0, Ljava/io/BufferedReader;

    .line 38
    new-instance v1, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    invoke-direct {v1, p1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 37
    iput-object v0, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    .line 39
    invoke-virtual {p0}, Lcom/spatial4j/core/io/LineReader;->next()Ljava/lang/Object;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "r"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/spatial4j/core/io/LineReader;->count:I

    .line 26
    iput v0, p0, Lcom/spatial4j/core/io/LineReader;->lineNumber:I

    .line 43
    instance-of v0, p1, Ljava/io/BufferedReader;

    if-eqz v0, :cond_0

    .line 44
    check-cast p1, Ljava/io/BufferedReader;

    .end local p1    # "r":Ljava/io/Reader;
    iput-object p1, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    .line 48
    :goto_0
    invoke-virtual {p0}, Lcom/spatial4j/core/io/LineReader;->next()Ljava/lang/Object;

    .line 49
    return-void

    .line 46
    .restart local p1    # "r":Ljava/io/Reader;
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 105
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    iget v0, p0, Lcom/spatial4j/core/io/LineReader;->count:I

    return v0
.end method

.method public getLineNumber()I
    .locals 1

    .prologue
    .line 101
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    iget v0, p0, Lcom/spatial4j/core/io/LineReader;->lineNumber:I

    return v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 58
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    iget-object v0, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    const/4 v1, 0x0

    .line 64
    .local v1, "val":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 65
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/spatial4j/core/io/LineReader;->parseLine(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 66
    iget v2, p0, Lcom/spatial4j/core/io/LineReader;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/spatial4j/core/io/LineReader;->count:I

    .line 69
    .end local v1    # "val":Ljava/lang/Object;, "TT;"
    :cond_0
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    if-eqz v2, :cond_2

    .line 71
    :cond_1
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    if-nez v2, :cond_3

    .line 92
    :cond_2
    :goto_1
    return-object v1

    .line 72
    :cond_3
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    .line 73
    iget v2, p0, Lcom/spatial4j/core/io/LineReader;->lineNumber:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/spatial4j/core/io/LineReader;->lineNumber:I

    .line 74
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 75
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 76
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/spatial4j/core/io/LineReader;->reader:Ljava/io/BufferedReader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "IOException thrown while reading/closing reader"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 78
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 79
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/spatial4j/core/io/LineReader;->readComment(Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :cond_5
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    .line 83
    iget-object v2, p0, Lcom/spatial4j/core/io/LineReader;->nextLine:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    if-lez v2, :cond_1

    goto :goto_1
.end method

.method public abstract parseLine(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method protected readComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 34
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    return-void
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 97
    .local p0, "this":Lcom/spatial4j/core/io/LineReader;, "Lcom/spatial4j/core/io/LineReader<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
