.class public Lcom/spatial4j/core/io/GeohashUtils;
.super Ljava/lang/Object;
.source "GeohashUtils.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BASE_32:[C

.field private static final BASE_32_IDX:[I

.field private static final BITS:[I

.field public static final MAX_PRECISION:I = 0x18

.field private static final hashLenToLatHeight:[D

.field private static final hashLenToLonWidth:[D


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/16 v9, 0x19

    const/16 v5, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 30
    const-class v2, Lcom/spatial4j/core/io/GeohashUtils;

    invoke-virtual {v2}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    sput-boolean v2, Lcom/spatial4j/core/io/GeohashUtils;->$assertionsDisabled:Z

    .line 32
    const/16 v2, 0x20

    new-array v2, v2, [C

    fill-array-data v2, :array_0

    sput-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    .line 39
    const/4 v2, 0x5

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    sput-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BITS:[I

    .line 42
    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    sget-object v7, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    aget-char v2, v2, v7

    sget-object v7, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    aget-char v7, v7, v4

    sub-int/2addr v2, v7

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    sput-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32_IDX:[I

    .line 43
    sget-boolean v2, Lcom/spatial4j/core/io/GeohashUtils;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32_IDX:[I

    array-length v2, v2

    const/16 v7, 0x64

    if-lt v2, v7, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    move v2, v4

    .line 30
    goto :goto_0

    .line 44
    :cond_1
    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32_IDX:[I

    const/16 v7, -0x1f4

    invoke-static {v2, v7}, Ljava/util/Arrays;->fill([II)V

    .line 45
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    array-length v2, v2

    if-lt v1, v2, :cond_2

    .line 184
    new-array v2, v9, [D

    sput-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLatHeight:[D

    .line 185
    new-array v2, v9, [D

    sput-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLonWidth:[D

    .line 186
    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLatHeight:[D

    const-wide v8, 0x4066800000000000L    # 180.0

    aput-wide v8, v2, v4

    .line 187
    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLonWidth:[D

    const-wide v8, 0x4076800000000000L    # 360.0

    aput-wide v8, v2, v4

    .line 188
    const/4 v0, 0x0

    .line 189
    .local v0, "even":Z
    const/4 v1, 0x1

    :goto_2
    const/16 v2, 0x18

    if-le v1, v2, :cond_3

    .line 194
    return-void

    .line 46
    .end local v0    # "even":Z
    :cond_2
    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32_IDX:[I

    sget-object v7, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    aget-char v7, v7, v1

    sget-object v8, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    aget-char v8, v8, v4

    sub-int/2addr v7, v8

    aput v1, v2, v7

    .line 45
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 190
    .restart local v0    # "even":Z
    :cond_3
    sget-object v7, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLatHeight:[D

    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLatHeight:[D

    add-int/lit8 v8, v1, -0x1

    aget-wide v8, v2, v8

    if-eqz v0, :cond_4

    move v2, v5

    :goto_3
    int-to-double v10, v2

    div-double/2addr v8, v10

    aput-wide v8, v7, v1

    .line 191
    sget-object v7, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLonWidth:[D

    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLonWidth:[D

    add-int/lit8 v8, v1, -0x1

    aget-wide v8, v2, v8

    if-eqz v0, :cond_5

    move v2, v6

    :goto_4
    int-to-double v10, v2

    div-double/2addr v8, v10

    aput-wide v8, v7, v1

    .line 192
    if-eqz v0, :cond_6

    move v0, v4

    .line 189
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move v2, v6

    .line 190
    goto :goto_3

    :cond_5
    move v2, v5

    .line 191
    goto :goto_4

    :cond_6
    move v0, v3

    .line 192
    goto :goto_5

    .line 32
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
        0x67s
        0x68s
        0x6as
        0x6bs
        0x6ds
        0x6es
        0x70s
        0x71s
        0x72s
        0x73s
        0x74s
        0x75s
        0x76s
        0x77s
        0x78s
        0x79s
        0x7as
    .end array-data

    .line 39
    :array_1
    .array-data 4
        0x10
        0x8
        0x4
        0x2
        0x1
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static decode(Ljava/lang/String;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Point;
    .locals 12
    .param p0, "geohash"    # Ljava/lang/String;
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 115
    invoke-static {p0, p1}, Lcom/spatial4j/core/io/GeohashUtils;->decodeBoundary(Ljava/lang/String;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v4

    .line 116
    .local v4, "rect":Lcom/spatial4j/core/shape/Rectangle;
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v6

    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v8

    add-double/2addr v6, v8

    div-double v0, v6, v10

    .line 117
    .local v0, "latitude":D
    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v6

    invoke-interface {v4}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v8

    add-double/2addr v6, v8

    div-double v2, v6, v10

    .line 118
    .local v2, "longitude":D
    invoke-virtual {p1, v2, v3, v0, v1}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v5

    return-object v5
.end method

.method public static decodeBoundary(Ljava/lang/String;Lcom/spatial4j/core/context/SpatialContext;)Lcom/spatial4j/core/shape/Rectangle;
    .locals 22
    .param p0, "geohash"    # Ljava/lang/String;
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 123
    const-wide v8, -0x3fa9800000000000L    # -90.0

    .local v8, "minY":D
    const-wide v10, 0x4056800000000000L    # 90.0

    .local v10, "maxY":D
    const-wide v4, -0x3f99800000000000L    # -180.0

    .local v4, "minX":D
    const-wide v6, 0x4066800000000000L    # 180.0

    .line 124
    .local v6, "maxX":D
    const/4 v14, 0x1

    .line 126
    .local v14, "isEven":Z
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v13, v3, :cond_0

    move-object/from16 v3, p1

    .line 150
    invoke-virtual/range {v3 .. v11}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v3

    return-object v3

    .line 127
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 128
    .local v2, "c":C
    const/16 v3, 0x41

    if-lt v2, v3, :cond_1

    const/16 v3, 0x5a

    if-gt v2, v3, :cond_1

    .line 129
    add-int/lit8 v3, v2, 0x20

    int-to-char v2, v3

    .line 130
    :cond_1
    sget-object v3, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32_IDX:[I

    sget-object v16, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    const/16 v17, 0x0

    aget-char v16, v16, v17

    sub-int v16, v2, v16

    aget v12, v3, v16

    .line 132
    .local v12, "cd":I
    sget-object v16, Lcom/spatial4j/core/io/GeohashUtils;->BITS:[I

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v3, 0x0

    :goto_1
    move/from16 v0, v17

    if-lt v3, v0, :cond_2

    .line 126
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 132
    :cond_2
    aget v15, v16, v3

    .line 133
    .local v15, "mask":I
    if-eqz v14, :cond_4

    .line 134
    and-int v18, v12, v15

    if-eqz v18, :cond_3

    .line 135
    add-double v18, v4, v6

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v4, v18, v20

    .line 146
    :goto_2
    if-eqz v14, :cond_6

    const/4 v14, 0x0

    .line 132
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 137
    :cond_3
    add-double v18, v4, v6

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v6, v18, v20

    .line 139
    goto :goto_2

    .line 140
    :cond_4
    and-int v18, v12, v15

    if-eqz v18, :cond_5

    .line 141
    add-double v18, v8, v10

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v8, v18, v20

    .line 142
    goto :goto_2

    .line 143
    :cond_5
    add-double v18, v8, v10

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v10, v18, v20

    goto :goto_2

    .line 146
    :cond_6
    const/4 v14, 0x1

    goto :goto_3
.end method

.method public static encodeLatLon(DD)Ljava/lang/String;
    .locals 2
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    .line 61
    const/16 v0, 0xc

    invoke-static {p0, p1, p2, p3, v0}, Lcom/spatial4j/core/io/GeohashUtils;->encodeLatLon(DDI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static encodeLatLon(DDI)Ljava/lang/String;
    .locals 14
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D
    .param p4, "precision"    # I

    .prologue
    .line 65
    const/4 v10, 0x2

    new-array v6, v10, [D

    fill-array-data v6, :array_0

    .line 66
    .local v6, "latInterval":[D
    const/4 v10, 0x2

    new-array v7, v10, [D

    fill-array-data v7, :array_1

    .line 68
    .local v7, "lngInterval":[D
    new-instance v4, Ljava/lang/StringBuilder;

    move/from16 v0, p4

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 69
    .local v4, "geohash":Ljava/lang/StringBuilder;
    const/4 v5, 0x1

    .line 71
    .local v5, "isEven":Z
    const/4 v2, 0x0

    .line 72
    .local v2, "bit":I
    const/4 v3, 0x0

    .line 74
    .local v3, "ch":I
    :goto_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    move/from16 v0, p4

    if-lt v10, v0, :cond_0

    .line 105
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .line 75
    :cond_0
    const-wide/16 v8, 0x0

    .line 76
    .local v8, "mid":D
    if-eqz v5, :cond_2

    .line 77
    const/4 v10, 0x0

    aget-wide v10, v7, v10

    const/4 v12, 0x1

    aget-wide v12, v7, v12

    add-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v8, v10, v12

    .line 78
    cmpl-double v10, p2, v8

    if-lez v10, :cond_1

    .line 79
    sget-object v10, Lcom/spatial4j/core/io/GeohashUtils;->BITS:[I

    aget v10, v10, v2

    or-int/2addr v3, v10

    .line 80
    const/4 v10, 0x0

    aput-wide v8, v7, v10

    .line 94
    :goto_1
    if-eqz v5, :cond_4

    const/4 v5, 0x0

    .line 96
    :goto_2
    const/4 v10, 0x4

    if-ge v2, v10, :cond_5

    .line 97
    add-int/lit8 v2, v2, 0x1

    .line 98
    goto :goto_0

    .line 82
    :cond_1
    const/4 v10, 0x1

    aput-wide v8, v7, v10

    goto :goto_1

    .line 85
    :cond_2
    const/4 v10, 0x0

    aget-wide v10, v6, v10

    const/4 v12, 0x1

    aget-wide v12, v6, v12

    add-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v8, v10, v12

    .line 86
    cmpl-double v10, p0, v8

    if-lez v10, :cond_3

    .line 87
    sget-object v10, Lcom/spatial4j/core/io/GeohashUtils;->BITS:[I

    aget v10, v10, v2

    or-int/2addr v3, v10

    .line 88
    const/4 v10, 0x0

    aput-wide v8, v6, v10

    goto :goto_1

    .line 90
    :cond_3
    const/4 v10, 0x1

    aput-wide v8, v6, v10

    goto :goto_1

    .line 94
    :cond_4
    const/4 v5, 0x1

    goto :goto_2

    .line 99
    :cond_5
    sget-object v10, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    aget-char v10, v10, v3

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 100
    const/4 v2, 0x0

    .line 101
    const/4 v3, 0x0

    goto :goto_0

    .line 65
    nop

    :array_0
    .array-data 8
        -0x3fa9800000000000L    # -90.0
        0x4056800000000000L    # 90.0
    .end array-data

    .line 66
    :array_1
    .array-data 8
        -0x3f99800000000000L    # -180.0
        0x4066800000000000L    # 180.0
    .end array-data
.end method

.method public static getSubGeohashes(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p0, "baseGeohash"    # Ljava/lang/String;

    .prologue
    .line 155
    sget-object v3, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    array-length v3, v3

    new-array v1, v3, [Ljava/lang/String;

    .line 156
    .local v1, "hashes":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 160
    return-object v1

    .line 157
    :cond_0
    sget-object v3, Lcom/spatial4j/core/io/GeohashUtils;->BASE_32:[C

    aget-char v0, v3, v2

    .line 158
    .local v0, "c":C
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 156
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static lookupDegreesSizeForHashLen(I)[D
    .locals 4
    .param p0, "hashLen"    # I

    .prologue
    .line 164
    const/4 v0, 0x2

    new-array v0, v0, [D

    const/4 v1, 0x0

    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLatHeight:[D

    aget-wide v2, v2, p0

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLonWidth:[D

    aget-wide v2, v2, p0

    aput-wide v2, v0, v1

    return-object v0
.end method

.method public static lookupHashLenForWidthHeight(DD)I
    .locals 8
    .param p0, "lonErr"    # D
    .param p2, "latErr"    # D

    .prologue
    const/16 v3, 0x18

    .line 172
    const/4 v2, 0x1

    .local v2, "len":I
    :goto_0
    if-lt v2, v3, :cond_1

    move v2, v3

    .line 178
    .end local v2    # "len":I
    :cond_0
    return v2

    .line 173
    .restart local v2    # "len":I
    :cond_1
    sget-object v6, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLatHeight:[D

    aget-wide v0, v6, v2

    .line 174
    .local v0, "latHeight":D
    sget-object v6, Lcom/spatial4j/core/io/GeohashUtils;->hashLenToLonWidth:[D

    aget-wide v4, v6, v2

    .line 175
    .local v4, "lonWidth":D
    cmpg-double v6, v0, p2

    if-gez v6, :cond_2

    cmpg-double v6, v4, p0

    if-ltz v6, :cond_0

    .line 172
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
