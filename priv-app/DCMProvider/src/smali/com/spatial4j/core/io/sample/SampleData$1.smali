.class Lcom/spatial4j/core/io/sample/SampleData$1;
.super Ljava/lang/Object;
.source "SampleData.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/spatial4j/core/io/sample/SampleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/spatial4j/core/io/sample/SampleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public compare(Lcom/spatial4j/core/io/sample/SampleData;Lcom/spatial4j/core/io/sample/SampleData;)I
    .locals 2
    .param p1, "o1"    # Lcom/spatial4j/core/io/sample/SampleData;
    .param p2, "o2"    # Lcom/spatial4j/core/io/sample/SampleData;

    .prologue
    .line 38
    iget-object v0, p1, Lcom/spatial4j/core/io/sample/SampleData;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/spatial4j/core/io/sample/SampleData;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/spatial4j/core/io/sample/SampleData;

    check-cast p2, Lcom/spatial4j/core/io/sample/SampleData;

    invoke-virtual {p0, p1, p2}, Lcom/spatial4j/core/io/sample/SampleData$1;->compare(Lcom/spatial4j/core/io/sample/SampleData;Lcom/spatial4j/core/io/sample/SampleData;)I

    move-result v0

    return v0
.end method
