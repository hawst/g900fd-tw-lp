.class public Lcom/spatial4j/core/io/geonames/Geoname;
.super Ljava/lang/Object;
.source "Geoname.java"


# instance fields
.field public adminCode1:Ljava/lang/String;

.field public adminCode2:Ljava/lang/String;

.field public adminCode3:Ljava/lang/String;

.field public adminCode4:Ljava/lang/String;

.field public alternateNames:[Ljava/lang/String;

.field public countryCode:Ljava/lang/String;

.field public countryCode2:[Ljava/lang/String;

.field public elevation:Ljava/lang/Integer;

.field public featureClass:C

.field public featureCode:Ljava/lang/String;

.field public gtopo30:Ljava/lang/Integer;

.field public id:I

.field public latitude:D

.field public longitude:D

.field public modified:Ljava/sql/Date;

.field public name:Ljava/lang/String;

.field public nameASCII:Ljava/lang/String;

.field public population:Ljava/lang/Long;

.field public timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 9
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x10

    const/16 v7, 0xf

    const/16 v6, 0xe

    const/4 v5, 0x6

    const/4 v4, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v1, "\t"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "vals":[Ljava/lang/String;
    aget-object v1, v0, v4

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->id:I

    .line 46
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->name:Ljava/lang/String;

    .line 47
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->nameASCII:Ljava/lang/String;

    .line 48
    const/4 v1, 0x3

    aget-object v1, v0, v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->alternateNames:[Ljava/lang/String;

    .line 49
    const/4 v1, 0x4

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/spatial4j/core/io/geonames/Geoname;->latitude:D

    .line 50
    const/4 v1, 0x5

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/spatial4j/core/io/geonames/Geoname;->longitude:D

    .line 51
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    aget-object v1, v0, v5

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    :goto_0
    iput-char v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->featureClass:C

    .line 52
    const/4 v1, 0x7

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->featureCode:Ljava/lang/String;

    .line 53
    const/16 v1, 0x8

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->countryCode:Ljava/lang/String;

    .line 54
    const/16 v1, 0x9

    aget-object v1, v0, v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->countryCode2:[Ljava/lang/String;

    .line 55
    const/16 v1, 0xa

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->adminCode1:Ljava/lang/String;

    .line 56
    const/16 v1, 0xb

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->adminCode2:Ljava/lang/String;

    .line 57
    const/16 v1, 0xc

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->adminCode3:Ljava/lang/String;

    .line 58
    const/16 v1, 0xd

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->adminCode4:Ljava/lang/String;

    .line 59
    aget-object v1, v0, v6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 60
    aget-object v1, v0, v6

    invoke-static {v1}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->population:Ljava/lang/Long;

    .line 62
    :cond_0
    aget-object v1, v0, v7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 63
    aget-object v1, v0, v7

    invoke-static {v1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->elevation:Ljava/lang/Integer;

    .line 65
    :cond_1
    aget-object v1, v0, v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 66
    aget-object v1, v0, v8

    invoke-static {v1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->gtopo30:Ljava/lang/Integer;

    .line 68
    :cond_2
    const/16 v1, 0x11

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->timezone:Ljava/lang/String;

    .line 69
    const/16 v1, 0x12

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 70
    const/16 v1, 0x12

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/sql/Date;->valueOf(Ljava/lang/String;)Ljava/sql/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/geonames/Geoname;->modified:Ljava/sql/Date;

    .line 72
    :cond_3
    return-void

    .line 51
    :cond_4
    const/16 v1, 0x53

    goto :goto_0
.end method
