.class public Lcom/spatial4j/core/io/WKTShapeParser;
.super Ljava/lang/Object;
.source "WKTShapeParser.java"


# instance fields
.field protected ctx:Lcom/spatial4j/core/context/SpatialContext;

.field protected offset:I

.field protected rawString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 0
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 48
    return-void
.end method

.method private parsePoint()Lcom/spatial4j/core/shape/Shape;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 118
    const/16 v1, 0x28

    invoke-virtual {p0, v1}, Lcom/spatial4j/core/io/WKTShapeParser;->expect(C)V

    .line 119
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->point()Lcom/spatial4j/core/shape/Point;

    move-result-object v0

    .line 120
    .local v0, "coordinate":Lcom/spatial4j/core/shape/Point;
    const/16 v1, 0x29

    invoke-virtual {p0, v1}, Lcom/spatial4j/core/io/WKTShapeParser;->expect(C)V

    .line 121
    return-object v0
.end method


# virtual methods
.method protected expect(C)V
    .locals 4
    .param p1, "expected"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->nextCharNoWS()C

    move-result v0

    .line 230
    .local v0, "c":C
    if-eq v0, p1, :cond_0

    .line 231
    new-instance v1, Ljava/text/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] found ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-direct {v1, v2, v3}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 233
    :cond_0
    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 234
    return-void
.end method

.method public getCtx()Lcom/spatial4j/core/context/SpatialContext;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    return-object v0
.end method

.method protected nextCharNoWS()C
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 246
    :goto_0
    iget v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    iget-object v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 253
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "EOF reached while expecting a non-whitespace character"

    iget v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0

    .line 250
    :cond_1
    iget v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    goto :goto_0
.end method

.method protected nextSubShapeString()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 277
    iget v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 278
    .local v2, "startOffset":I
    const/4 v1, 0x0

    .line 279
    .local v1, "parenStack":I
    :goto_0
    iget v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    iget-object v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 292
    :cond_0
    if-eqz v1, :cond_5

    .line 293
    new-instance v3, Ljava/text/ParseException;

    const-string v4, "Unbalanced parenthesis"

    invoke-direct {v3, v4, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3

    .line 280
    :cond_1
    iget-object v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 281
    .local v0, "c":C
    const/16 v3, 0x2c

    if-ne v0, v3, :cond_3

    .line 282
    if-eqz v1, :cond_0

    .line 279
    :cond_2
    :goto_1
    iget v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    goto :goto_0

    .line 284
    :cond_3
    const/16 v3, 0x29

    if-ne v0, v3, :cond_4

    .line 285
    if-eqz v1, :cond_0

    .line 287
    add-int/lit8 v1, v1, -0x1

    .line 288
    goto :goto_1

    :cond_4
    const/16 v3, 0x28

    if-ne v0, v3, :cond_2

    .line 289
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 294
    .end local v0    # "c":C
    :cond_5
    iget-object v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected nextWord()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 187
    iget v0, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 188
    .local v0, "startOffset":I
    :goto_0
    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    iget-object v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    .line 189
    iget v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 188
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    .line 189
    if-nez v1, :cond_1

    .line 192
    :cond_0
    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    if-ne v0, v1, :cond_2

    .line 193
    new-instance v1, Ljava/text/ParseException;

    const-string v2, "Word expected"

    invoke-direct {v1, v2, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 190
    :cond_1
    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    goto :goto_0

    .line 194
    :cond_2
    iget-object v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public parse(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;
    .locals 4
    .param p1, "wktString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/io/WKTShapeParser;->parseIfSupported(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    .line 62
    .local v0, "shape":Lcom/spatial4j/core/shape/Shape;
    if-eqz v0, :cond_0

    .line 63
    return-object v0

    .line 64
    :cond_0
    new-instance v1, Ljava/text/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown Shape definition ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-direct {v1, v2, v3}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1
.end method

.method protected parseDouble()D
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 206
    iget v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 208
    .local v2, "startOffset":I
    :try_start_0
    iget-object v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .local v0, "c":C
    :goto_0
    iget v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    iget-object v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-lt v3, v4, :cond_0

    .line 217
    new-instance v3, Ljava/text/ParseException;

    const-string v4, "EOF reached before delimiter for the number was found"

    iget v5, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-direct {v3, v4, v5}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3

    .line 209
    :cond_0
    :try_start_1
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_1

    const/16 v3, 0x2e

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2d

    if-eq v0, v3, :cond_1

    .line 210
    iget-object v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    return-wide v4

    .line 208
    :cond_1
    iget-object v3, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    iget v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    .line 213
    .end local v0    # "c":C
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/text/ParseException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    invoke-direct {v3, v4, v5}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3
.end method

.method protected parseEnvelope()Lcom/spatial4j/core/shape/Shape;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->pointList()Ljava/util/List;

    move-result-object v0

    .line 134
    .local v0, "pointList":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/Point;>;"
    new-instance v3, Lcom/spatial4j/core/shape/impl/RectangleImpl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/spatial4j/core/shape/Point;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/spatial4j/core/shape/Point;

    iget-object v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-direct {v3, v1, v2, v4}, Lcom/spatial4j/core/shape/impl/RectangleImpl;-><init>(Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/shape/Point;Lcom/spatial4j/core/context/SpatialContext;)V

    return-object v3
.end method

.method public parseIfSupported(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;
    .locals 3
    .param p1, "wktString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 78
    :cond_0
    const/4 v1, 0x0

    .line 82
    :goto_0
    return-object v1

    .line 79
    :cond_1
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->rawString:Ljava/lang/String;

    .line 80
    iput v2, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 81
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->nextWord()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "shapeType":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/spatial4j/core/io/WKTShapeParser;->parseShapeByType(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v1

    goto :goto_0
.end method

.method protected parseShapeByType(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;
    .locals 1
    .param p1, "shapeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 100
    const-string v0, "point"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->parsePoint()Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    .line 103
    :cond_0
    const-string v0, "envelope"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->parseEnvelope()Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected point()Lcom/spatial4j/core/shape/Point;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->nextCharNoWS()C

    .line 171
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->parseDouble()D

    move-result-wide v0

    .line 173
    .local v0, "x":D
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->nextCharNoWS()C

    .line 174
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->parseDouble()D

    move-result-wide v2

    .line 176
    .local v2, "y":D
    iget-object v4, p0, Lcom/spatial4j/core/io/WKTShapeParser;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v4

    return-object v4
.end method

.method protected pointList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/spatial4j/core/shape/Point;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v0, "sequence":Ljava/util/List;, "Ljava/util/List<Lcom/spatial4j/core/shape/Point;>;"
    const/16 v1, 0x28

    invoke-virtual {p0, v1}, Lcom/spatial4j/core/io/WKTShapeParser;->expect(C)V

    .line 149
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->point()Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    :goto_0
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->nextCharNoWS()C

    move-result v1

    const/16 v2, 0x2c

    if-eq v1, v2, :cond_0

    .line 156
    const/16 v1, 0x29

    invoke-virtual {p0, v1}, Lcom/spatial4j/core/io/WKTShapeParser;->expect(C)V

    .line 157
    return-object v0

    .line 152
    :cond_0
    iget v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/spatial4j/core/io/WKTShapeParser;->offset:I

    .line 153
    invoke-virtual {p0}, Lcom/spatial4j/core/io/WKTShapeParser;->point()Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
