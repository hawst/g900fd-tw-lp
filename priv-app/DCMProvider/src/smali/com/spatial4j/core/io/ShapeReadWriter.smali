.class public Lcom/spatial4j/core/io/ShapeReadWriter;
.super Ljava/lang/Object;
.source "ShapeReadWriter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CTX:",
        "Lcom/spatial4j/core/context/SpatialContext;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected ctx:Lcom/spatial4j/core/context/SpatialContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TCTX;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/spatial4j/core/context/SpatialContext;)V
    .locals 0
    .param p1, "ctx"    # Lcom/spatial4j/core/context/SpatialContext;

    .prologue
    .line 21
    .local p0, "this":Lcom/spatial4j/core/io/ShapeReadWriter;, "Lcom/spatial4j/core/io/ShapeReadWriter<TCTX;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/spatial4j/core/io/ShapeReadWriter;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    .line 23
    return-void
.end method

.method public static makeNumberFormat(I)Ljava/text/NumberFormat;
    .locals 2
    .param p0, "fractionDigits"    # I

    .prologue
    .line 91
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 92
    .local v0, "nf":Ljava/text/NumberFormat;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    .line 93
    invoke-virtual {v0, p0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 94
    invoke-virtual {v0, p0}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 95
    return-object v0
.end method

.method private readLatCommaLonPoint(Ljava/lang/String;)Lcom/spatial4j/core/shape/Point;
    .locals 6
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    .line 162
    .local p0, "this":Lcom/spatial4j/core/io/ShapeReadWriter;, "Lcom/spatial4j/core/io/ShapeReadWriter<TCTX;>;"
    invoke-static {p1}, Lcom/spatial4j/core/io/ParseUtils;->parseLatitudeLongitude(Ljava/lang/String;)[D

    move-result-object v0

    .line 163
    .local v0, "latLon":[D
    iget-object v1, p0, Lcom/spatial4j/core/io/ShapeReadWriter;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    const/4 v2, 0x1

    aget-wide v2, v0, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public readShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;
    .locals 4
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/spatial4j/core/io/ShapeReadWriter;, "Lcom/spatial4j/core/io/ShapeReadWriter<TCTX;>;"
    invoke-virtual {p0, p1}, Lcom/spatial4j/core/io/ShapeReadWriter;->readStandardShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;

    move-result-object v0

    .line 47
    .local v0, "s":Lcom/spatial4j/core/shape/Shape;
    if-nez v0, :cond_0

    .line 48
    new-instance v1, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to read: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_0
    return-object v0
.end method

.method protected readStandardShape(Ljava/lang/String;)Lcom/spatial4j/core/shape/Shape;
    .locals 28
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 99
    .local p0, "this":Lcom/spatial4j/core/io/ShapeReadWriter;, "Lcom/spatial4j/core/io/ShapeReadWriter<TCTX;>;"
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 100
    :cond_0
    new-instance v5, Lcom/spatial4j/core/exception/InvalidShapeException;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 103
    :cond_1
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 104
    const-string v5, "Circle("

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "CIRCLE("

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 105
    :cond_2
    const/16 v5, 0x29

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v16

    .line 106
    .local v16, "idx":I
    if-lez v16, :cond_9

    .line 107
    const-string v5, "Circle("

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 108
    .local v14, "body":Ljava/lang/String;
    new-instance v19, Ljava/util/StringTokenizer;

    const-string v5, " "

    move-object/from16 v0, v19

    invoke-direct {v0, v14, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .local v19, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v20

    .line 111
    .local v20, "token":Ljava/lang/String;
    const/16 v5, 0x2c

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v5, v0, :cond_4

    .line 112
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/spatial4j/core/io/ShapeReadWriter;->readLatCommaLonPoint(Ljava/lang/String;)Lcom/spatial4j/core/shape/Point;

    move-result-object v18

    .line 118
    .local v18, "pt":Lcom/spatial4j/core/shape/Point;
    :goto_0
    const/4 v15, 0x0

    .line 120
    .local v15, "d":Ljava/lang/Double;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 121
    .local v4, "arg":Ljava/lang/String;
    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v16

    .line 122
    if-lez v16, :cond_6

    .line 123
    const/4 v5, 0x0

    move/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 124
    .local v17, "k":Ljava/lang/String;
    const-string v5, "d"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "distance"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 125
    :cond_3
    add-int/lit8 v5, v16, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    .line 132
    .end local v17    # "k":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 133
    new-instance v5, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v26, "Extra arguments: "

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v26, " :: "

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 114
    .end local v4    # "arg":Ljava/lang/String;
    .end local v15    # "d":Ljava/lang/Double;
    .end local v18    # "pt":Lcom/spatial4j/core/shape/Point;
    :cond_4
    invoke-static/range {v20 .. v20}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    .line 115
    .local v22, "x":D
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    .line 116
    .local v24, "y":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/spatial4j/core/io/ShapeReadWriter;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    move-wide/from16 v0, v22

    move-wide/from16 v2, v24

    invoke-virtual {v5, v0, v1, v2, v3}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v18

    .restart local v18    # "pt":Lcom/spatial4j/core/shape/Point;
    goto/16 :goto_0

    .line 127
    .end local v22    # "x":D
    .end local v24    # "y":D
    .restart local v4    # "arg":Ljava/lang/String;
    .restart local v15    # "d":Ljava/lang/Double;
    .restart local v17    # "k":Ljava/lang/String;
    :cond_5
    new-instance v5, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v26, "unknown arg: "

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v26, " :: "

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 130
    .end local v17    # "k":Ljava/lang/String;
    :cond_6
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    goto/16 :goto_1

    .line 135
    :cond_7
    if-nez v15, :cond_8

    .line 136
    new-instance v5, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v26, "Missing Distance: "

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 139
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/spatial4j/core/io/ShapeReadWriter;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v15}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v26

    invoke-virtual {v5, v0, v1, v2}, Lcom/spatial4j/core/context/SpatialContext;->makeCircle(Lcom/spatial4j/core/shape/Point;D)Lcom/spatial4j/core/shape/Circle;

    move-result-object v5

    .line 157
    .end local v4    # "arg":Ljava/lang/String;
    .end local v14    # "body":Ljava/lang/String;
    .end local v15    # "d":Ljava/lang/Double;
    .end local v16    # "idx":I
    .end local v18    # "pt":Lcom/spatial4j/core/shape/Point;
    .end local v19    # "st":Ljava/util/StringTokenizer;
    .end local v20    # "token":Ljava/lang/String;
    :goto_2
    return-object v5

    .line 142
    :cond_9
    const/4 v5, 0x0

    goto :goto_2

    .line 145
    :cond_a
    const/16 v5, 0x2c

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v5, v0, :cond_b

    .line 146
    invoke-direct/range {p0 .. p1}, Lcom/spatial4j/core/io/ShapeReadWriter;->readLatCommaLonPoint(Ljava/lang/String;)Lcom/spatial4j/core/shape/Point;

    move-result-object v5

    goto :goto_2

    .line 147
    :cond_b
    new-instance v19, Ljava/util/StringTokenizer;

    const-string v5, " "

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    .restart local v19    # "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 149
    .local v6, "p0":D
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 150
    .local v10, "p1":D
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 151
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 152
    .local v8, "p2":D
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 153
    .local v12, "p3":D
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 154
    new-instance v5, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v26, "Only 4 numbers supported (rect) but found more: "

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 155
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/spatial4j/core/io/ShapeReadWriter;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual/range {v5 .. v13}, Lcom/spatial4j/core/context/SpatialContext;->makeRectangle(DDDD)Lcom/spatial4j/core/shape/Rectangle;

    move-result-object v5

    goto :goto_2

    .line 157
    .end local v8    # "p2":D
    .end local v12    # "p3":D
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/spatial4j/core/io/ShapeReadWriter;->ctx:Lcom/spatial4j/core/context/SpatialContext;

    invoke-virtual {v5, v6, v7, v10, v11}, Lcom/spatial4j/core/context/SpatialContext;->makePoint(DD)Lcom/spatial4j/core/shape/Point;

    move-result-object v5

    goto :goto_2
.end method

.method public writeShape(Lcom/spatial4j/core/shape/Shape;)Ljava/lang/String;
    .locals 1
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;

    .prologue
    .line 59
    .local p0, "this":Lcom/spatial4j/core/io/ShapeReadWriter;, "Lcom/spatial4j/core/io/ShapeReadWriter<TCTX;>;"
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/spatial4j/core/io/ShapeReadWriter;->makeNumberFormat(I)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/spatial4j/core/io/ShapeReadWriter;->writeShape(Lcom/spatial4j/core/shape/Shape;Ljava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeShape(Lcom/spatial4j/core/shape/Shape;Ljava/text/NumberFormat;)Ljava/lang/String;
    .locals 6
    .param p1, "shape"    # Lcom/spatial4j/core/shape/Shape;
    .param p2, "nf"    # Ljava/text/NumberFormat;

    .prologue
    .line 64
    .local p0, "this":Lcom/spatial4j/core/io/ShapeReadWriter;, "Lcom/spatial4j/core/io/ShapeReadWriter<TCTX;>;"
    instance-of v3, p1, Lcom/spatial4j/core/shape/Point;

    if-eqz v3, :cond_0

    move-object v1, p1

    .line 65
    check-cast v1, Lcom/spatial4j/core/shape/Point;

    .line 66
    .local v1, "point":Lcom/spatial4j/core/shape/Point;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {v1}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    .end local v1    # "point":Lcom/spatial4j/core/shape/Point;
    :goto_0
    return-object v3

    .line 68
    :cond_0
    instance-of v3, p1, Lcom/spatial4j/core/shape/Rectangle;

    if-eqz v3, :cond_1

    move-object v2, p1

    .line 69
    check-cast v2, Lcom/spatial4j/core/shape/Rectangle;

    .line 71
    .local v2, "rect":Lcom/spatial4j/core/shape/Rectangle;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMinX()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 72
    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMinY()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 73
    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMaxX()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 74
    invoke-interface {v2}, Lcom/spatial4j/core/shape/Rectangle;->getMaxY()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 71
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 76
    .end local v2    # "rect":Lcom/spatial4j/core/shape/Rectangle;
    :cond_1
    instance-of v3, p1, Lcom/spatial4j/core/shape/Circle;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 77
    check-cast v0, Lcom/spatial4j/core/shape/Circle;

    .line 78
    .local v0, "c":Lcom/spatial4j/core/shape/Circle;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Circle("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 79
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v4

    invoke-interface {v4}, Lcom/spatial4j/core/shape/Point;->getX()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 80
    invoke-interface {v0}, Lcom/spatial4j/core/shape/Circle;->getCenter()Lcom/spatial4j/core/shape/Point;

    move-result-object v4

    invoke-interface {v4}, Lcom/spatial4j/core/shape/Point;->getY()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 81
    const-string v4, "d="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/spatial4j/core/shape/Circle;->getRadius()D

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 82
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 78
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 84
    .end local v0    # "c":Lcom/spatial4j/core/shape/Circle;
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method
