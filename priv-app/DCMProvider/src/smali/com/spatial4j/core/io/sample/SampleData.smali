.class public Lcom/spatial4j/core/io/sample/SampleData;
.super Ljava/lang/Object;
.source "SampleData.java"


# static fields
.field public static NAME_ORDER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/spatial4j/core/io/sample/SampleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public shape:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/spatial4j/core/io/sample/SampleData$1;

    invoke-direct {v0}, Lcom/spatial4j/core/io/sample/SampleData$1;-><init>()V

    sput-object v0, Lcom/spatial4j/core/io/sample/SampleData;->NAME_ORDER:Ljava/util/Comparator;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v1, "\t"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "vals":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/sample/SampleData;->id:Ljava/lang/String;

    .line 31
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/sample/SampleData;->name:Ljava/lang/String;

    .line 32
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/spatial4j/core/io/sample/SampleData;->shape:Ljava/lang/String;

    .line 33
    return-void
.end method
