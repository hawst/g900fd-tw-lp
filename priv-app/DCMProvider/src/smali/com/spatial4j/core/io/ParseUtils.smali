.class public Lcom/spatial4j/core/io/ParseUtils;
.super Ljava/lang/Object;
.source "ParseUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static final parseLatitudeLongitude(Ljava/lang/String;)[D
    .locals 1
    .param p0, "latLonStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/spatial4j/core/io/ParseUtils;->parseLatitudeLongitude([DLjava/lang/String;)[D

    move-result-object v0

    return-object v0
.end method

.method public static final parseLatitudeLongitude([DLjava/lang/String;)[D
    .locals 8
    .param p0, "latLon"    # [D
    .param p1, "latLonStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 133
    if-nez p0, :cond_0

    .line 134
    new-array p0, v2, [D

    .line 136
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, p1, v2}, Lcom/spatial4j/core/io/ParseUtils;->parsePointDouble([DLjava/lang/String;I)[D

    move-result-object v0

    .line 138
    .local v0, "toks":[D
    aget-wide v2, v0, v6

    const-wide v4, -0x3fa9800000000000L    # -90.0

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_1

    aget-wide v2, v0, v6

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 139
    :cond_1
    new-instance v1, Lcom/spatial4j/core/exception/InvalidShapeException;

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid latitude: latitudes are range -90 to 90: provided lat: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 141
    aget-wide v4, v0, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 140
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-direct {v1, v2}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 143
    :cond_2
    aget-wide v2, v0, v6

    aput-wide v2, p0, v6

    .line 146
    aget-wide v2, v0, v7

    const-wide v4, -0x3f99800000000000L    # -180.0

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_3

    aget-wide v2, v0, v7

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_4

    .line 148
    :cond_3
    new-instance v1, Lcom/spatial4j/core/exception/InvalidShapeException;

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid longitude: longitudes are range -180 to 180: provided lon: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 150
    aget-wide v4, v0, v7

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-direct {v1, v2}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 152
    :cond_4
    aget-wide v2, v0, v7

    aput-wide v2, p0, v7

    .line 154
    return-object p0
.end method

.method public static parsePoint([Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;
    .locals 8
    .param p0, "out"    # [Ljava/lang/String;
    .param p1, "externalVal"    # Ljava/lang/String;
    .param p2, "dimension"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x2c

    const/16 v6, 0x20

    const/4 v5, -0x1

    .line 41
    if-eqz p0, :cond_0

    array-length v4, p0

    if-eq v4, p2, :cond_1

    :cond_0
    new-array p0, p2, [Ljava/lang/String;

    .line 42
    :cond_1
    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 43
    .local v2, "idx":I
    move v0, v2

    .line 44
    .local v0, "end":I
    const/4 v3, 0x0

    .line 45
    .local v3, "start":I
    const/4 v1, 0x0

    .line 46
    .local v1, "i":I
    if-ne v2, v5, :cond_3

    const/4 v4, 0x1

    if-ne p2, v4, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 47
    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p0, v4

    .line 48
    const/4 v1, 0x1

    .line 66
    :cond_2
    if-eq v1, p2, :cond_9

    .line 67
    new-instance v4, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "incompatible dimension ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 68
    const-string v6, ") and values ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ").  Only "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " values specified"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 67
    invoke-direct {v4, v5}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 49
    :cond_3
    if-lez v2, :cond_2

    .line 51
    :goto_0
    if-ge v1, p2, :cond_2

    .line 52
    :goto_1
    if-ge v3, v0, :cond_4

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_7

    .line 53
    :cond_4
    :goto_2
    if-le v0, v3, :cond_5

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_8

    .line 54
    :cond_5
    if-eq v3, v0, :cond_2

    .line 57
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, p0, v1

    .line 58
    add-int/lit8 v3, v2, 0x1

    .line 59
    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 60
    move v2, v0

    .line 61
    if-ne v0, v5, :cond_6

    .line 62
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 51
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 53
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 70
    :cond_9
    return-object p0
.end method

.method public static parsePointDouble([DLjava/lang/String;I)[D
    .locals 9
    .param p0, "out"    # [D
    .param p1, "externalVal"    # Ljava/lang/String;
    .param p2, "dimension"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/spatial4j/core/exception/InvalidShapeException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x2c

    const/16 v7, 0x20

    const/4 v6, -0x1

    .line 84
    if-eqz p0, :cond_0

    array-length v4, p0

    if-eq v4, p2, :cond_1

    :cond_0
    new-array p0, p2, [D

    .line 85
    :cond_1
    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 86
    .local v2, "idx":I
    move v0, v2

    .line 87
    .local v0, "end":I
    const/4 v3, 0x0

    .line 88
    .local v3, "start":I
    const/4 v1, 0x0

    .line 89
    .local v1, "i":I
    if-ne v2, v6, :cond_3

    const/4 v4, 0x1

    if-ne p2, v4, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 90
    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    aput-wide v6, p0, v4

    .line 91
    const/4 v1, 0x1

    .line 110
    :cond_2
    if-eq v1, p2, :cond_9

    .line 111
    new-instance v4, Lcom/spatial4j/core/exception/InvalidShapeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "incompatible dimension ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 112
    const-string v6, ") and values ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ").  Only "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " values specified"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 111
    invoke-direct {v4, v5}, Lcom/spatial4j/core/exception/InvalidShapeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 92
    :cond_3
    if-lez v2, :cond_2

    .line 94
    :goto_0
    if-ge v1, p2, :cond_2

    .line 96
    :goto_1
    if-ge v3, v0, :cond_4

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v7, :cond_7

    .line 97
    :cond_4
    :goto_2
    if-le v0, v3, :cond_5

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v7, :cond_8

    .line 98
    :cond_5
    if-eq v3, v0, :cond_2

    .line 101
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    aput-wide v4, p0, v1

    .line 102
    add-int/lit8 v3, v2, 0x1

    .line 103
    invoke-virtual {p1, v8, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 104
    move v2, v0

    .line 105
    if-ne v0, v6, :cond_6

    .line 106
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 94
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 97
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 114
    :cond_9
    return-object p0
.end method
