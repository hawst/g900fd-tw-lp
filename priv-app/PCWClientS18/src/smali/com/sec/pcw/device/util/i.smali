.class public final Lcom/sec/pcw/device/util/i;
.super Ljava/lang/Object;
.source "SppAPI.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static a:Lcom/sec/a/a/a;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/os/ConditionVariable;

.field private d:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    .line 169
    new-instance v0, Lcom/sec/pcw/device/util/i$1;

    invoke-direct {v0, p0}, Lcom/sec/pcw/device/util/i$1;-><init>(Lcom/sec/pcw/device/util/i;)V

    iput-object v0, p0, Lcom/sec/pcw/device/util/i;->d:Landroid/content/ServiceConnection;

    .line 62
    if-nez p1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/ExceptionInInitializerError;

    new-instance v1, Ljava/lang/String;

    const-string v2, "context is null"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    .line 68
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/sec/pcw/device/util/i;->c:Landroid/os/ConditionVariable;

    .line 73
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/device/util/i$2;

    invoke-direct {v1, p0}, Lcom/sec/pcw/device/util/i$2;-><init>(Lcom/sec/pcw/device/util/i;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 97
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 98
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/device/util/i;)Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->c:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic a()Lcom/sec/a/a/a;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a;)V
    .locals 0

    .prologue
    .line 51
    sput-object p0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    return-void
.end method

.method static synthetic b(Lcom/sec/pcw/device/util/i;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/pcw/device/util/i;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->d:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/pcw/device/util/i;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 320
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[deregistration]"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "appId : ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    sget-object v0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    .line 331
    :try_start_0
    sget-object v0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    invoke-interface {v0, p1}, Lcom/sec/a/a/a;->a(Ljava/lang/String;)V

    .line 333
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[Method1] mService.deregistration requested"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :goto_0
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 337
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 339
    const-string v1, "PCWCLIENTTRACE_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.deregistration() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 348
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 349
    const-string v1, "reqType"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 350
    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    invoke-static {v0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Intent;)Landroid/content/Intent;

    .line 353
    iget-object v1, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 355
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[Method2] broadcast intent"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_1
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "mService.deregistration() failed"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 261
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[registration]"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "appId : ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "userData : ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    sget-object v0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 274
    sget-object v1, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    invoke-interface {v1, p1, v0}, Lcom/sec/a/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[Method1] mService.registration requested"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    return-void

    .line 278
    :catch_0
    move-exception v0

    .line 280
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 282
    const-string v1, "PCWCLIENTTRACE_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.registration() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 291
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 292
    const-string v1, "reqType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 293
    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    const-string v1, "userdata"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    invoke-static {v0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Intent;)Landroid/content/Intent;

    .line 297
    iget-object v1, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 299
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[Method2] broadcast intent"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 307
    :cond_1
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "mService.registration() failed"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 132
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[close]"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    sget-object v0, Lcom/sec/pcw/device/util/i;->a:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/pcw/device/util/i;->b:Landroid/content/Context;

    .line 141
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/pcw/device/util/i$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/pcw/device/util/i$3;-><init>(Lcom/sec/pcw/device/util/i;Landroid/content/Context;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 162
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 164
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 4

    .prologue
    .line 113
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[finalize]"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    invoke-virtual {p0}, Lcom/sec/pcw/device/util/i;->close()V

    .line 125
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 121
    const-string v1, "PCWCLIENTTRACE_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "finalize() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
