.class final Lcom/sec/pcw/device/c/d$1;
.super Ljava/lang/Object;
.source "PCWHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/c/d;->b(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Landroid/content/Context;

.field private final synthetic b:[Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/pcw/device/c/d$1;->c:Ljava/lang/String;

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 161
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "[ensureRegistratison] - Token validation check!!"

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    new-instance v0, Lcom/sec/pcw/device/osp/a;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/pcw/device/osp/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/a;->a()Lcom/sec/pcw/device/osp/d;

    move-result-object v5

    .line 165
    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-virtual {v5, v0}, Lcom/sec/pcw/device/osp/d;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.pcw.device.TOKEN_RETRY"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 168
    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 169
    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/pcw/device/util/g;->i(Landroid/content/Context;)I

    move-result v3

    .line 170
    iget-object v4, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-static {v4, v0, v3}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    move-result v0

    .line 172
    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;I)Z

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    array-length v6, v0

    move v4, v2

    :goto_0
    if-lt v4, v6, :cond_2

    .line 255
    return-void

    .line 181
    :cond_2
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "[ensureRegistration] - PCW Client Version : 4.80"

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-static {v0}, Lcom/sec/pcw/device/util/h;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "PCWCLIENTTRACE_PushUtil"

    const-string v9, "[ensureRegistration] - DMVersion changing by : %s (%s -> %s)"

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v3, v10, v2

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v1

    aput-object v7, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 183
    :goto_1
    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v3}, Lcom/sec/pcw/device/util/h;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "PCWCLIENTTRACE_PushUtil"

    const-string v10, "[ensureRegistration] - PCWClientVersion changing by : %s (%s -> %s)"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v7, v11, v2

    invoke-static {v3}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v11, v1

    aput-object v8, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v1

    .line 182
    :goto_2
    or-int/2addr v3, v0

    .line 184
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v0}, Lcom/sec/pcw/device/util/h;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "PCWCLIENTTRACE_PushUtil"

    const-string v10, "[ensureRegistration] - SupportReactivationLock changing by : %s (%s -> %s)"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v7, v11, v2

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v1

    aput-object v8, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 182
    :goto_3
    or-int/2addr v0, v3

    .line 186
    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v3, v7}, Lcom/sec/pcw/device/util/g;->i(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 192
    const-string v3, "PCWCLIENTTRACE_PCWHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[ensureRegistration] - MG registered : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v8, v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v3, v7}, Lcom/sec/pcw/device/util/g;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 197
    const-string v7, ""

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 200
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "[ensureRegistration] - Re-registration ("

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ")"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-static {v0, v3}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 178
    :cond_3
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 182
    goto/16 :goto_1

    :cond_5
    move v3, v2

    .line 183
    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 184
    goto :goto_3

    .line 213
    :cond_7
    if-eqz v0, :cond_8

    .line 214
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v0, v3, v1, v7}, Lcom/sec/pcw/device/c/f;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_4

    .line 216
    :cond_8
    if-eqz v5, :cond_3

    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    invoke-virtual {v5, v0}, Lcom/sec/pcw/device/osp/d;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v5}, Lcom/sec/pcw/device/osp/d;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "======================================================================================================="

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Registered deviceID is different from the Samsung Account : Re-registration ("

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ")"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "======================================================================================================="

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v0, v3, v1, v7}, Lcom/sec/pcw/device/c/f;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_4

    .line 234
    :cond_9
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "[ensureRegistration] - MG unregistered : "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/g;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 241
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "[ensureRegistration] - request registration : "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-static {v0, v3}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 247
    :cond_a
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "[ensureRegistration] - dispatchMGRegistration"

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/sec/pcw/device/c/d$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/pcw/device/c/d$1;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/pcw/device/c/d$1;->b:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-static {v0, v3, v1, v7}, Lcom/sec/pcw/device/c/f;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V

    goto/16 :goto_4
.end method
