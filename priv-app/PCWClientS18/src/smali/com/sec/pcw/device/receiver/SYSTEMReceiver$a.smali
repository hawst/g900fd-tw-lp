.class public final Lcom/sec/pcw/device/receiver/SYSTEMReceiver$a;
.super Ljava/lang/Object;
.source "SYSTEMReceiver.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/receiver/SYSTEMReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 51
    invoke-static {}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->c()V

    .line 53
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "called uncaughtException - don\'t support the reactivationlock"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "set alarm of ACTION_CHECK_REACTIVATION_PKG_REPLACED"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;Z)V

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.pcw.device.CHECK_REACTIVATION_PKG_REPLACED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a()Landroid/content/Context;

    move-result-object v1

    .line 58
    const/high16 v2, 0x8000000

    .line 57
    invoke-static {v1, v7, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 60
    invoke-static {}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 61
    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 65
    :cond_0
    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    .line 66
    return-void
.end method
