.class final Lcom/sec/pcw/device/util/i$1;
.super Ljava/lang/Object;
.source "SppAPI.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/util/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/util/i;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/util/i;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/util/i$1;->a:Lcom/sec/pcw/device/util/i;

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 173
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[ServiceConnection] onServiceConnected"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-static {p2}, Lcom/sec/a/a/a$a;->a(Landroid/os/IBinder;)Lcom/sec/a/a/a;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/a/a/a;)V

    .line 177
    invoke-static {}, Lcom/sec/pcw/device/util/i;->a()Lcom/sec/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 179
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[ServiceConnection] ServiceConnection - success"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$1;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/pcw/device/util/i;)Landroid/os/ConditionVariable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$1;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/pcw/device/util/i;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[ServiceConnection] ServiceConnection - failed"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 194
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "[ServiceConnection] onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/a/a/a;)V

    .line 198
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$1;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/pcw/device/util/i;)Landroid/os/ConditionVariable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$1;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/pcw/device/util/i;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 203
    :cond_0
    return-void
.end method
