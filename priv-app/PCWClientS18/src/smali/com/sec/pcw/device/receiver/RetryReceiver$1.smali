.class final Lcom/sec/pcw/device/receiver/RetryReceiver$1;
.super Ljava/lang/Object;
.source "RetryReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/receiver/RetryReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/receiver/RetryReceiver;

.field private final synthetic b:Landroid/content/Context;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;

.field private final synthetic e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/receiver/RetryReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->a:Lcom/sec/pcw/device/receiver/RetryReceiver;

    iput-object p2, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->e:Ljava/lang/String;

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, -0x1

    .line 101
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    const-string v2, "ACTION_HTTP_REQUEST_RETRY Thread"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/pcw/device/util/h;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 105
    new-instance v1, Lcom/sec/pcw/device/b/a;

    iget-object v2, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/pcw/device/b/a;-><init>(Landroid/content/Context;)V

    .line 107
    invoke-static {}, Lcom/sec/pcw/device/b/a;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 108
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 111
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUSH_DELIVERY_REPORT DB Content length: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v0

    move v1, v0

    .line 113
    :goto_0
    if-lt v3, v5, :cond_1

    .line 132
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUSH_DELIVERY_REPORT(isfailed): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    if-eqz v1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->k(Landroid/content/Context;)I

    move-result v0

    .line 138
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUSH_DELIVERY_REPORT - backoffTimeMs: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v1, Landroid/content/Intent;

    .line 142
    const-string v2, "com.sec.pcw.device.HTTP_REQUEST_RETRY"

    .line 141
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 143
    const-string v2, "body"

    iget-object v3, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string v2, "uri"

    iget-object v3, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string v2, "pushType"

    iget-object v3, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    iget-object v2, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    invoke-static {v2, v1, v0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    move-result v0

    .line 151
    iget-object v1, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;I)V

    .line 176
    :cond_0
    :goto_1
    return-void

    .line 114
    :cond_1
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 116
    const/4 v2, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "SPP"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v8, v6, :cond_3

    const-string v2, "SPP"

    .line 118
    :cond_2
    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 120
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v2, "unrecognized push type!! delivery repory ignored!!"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 113
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto/16 :goto_0

    .line 116
    :cond_3
    const-string v6, "GCM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v8, v6, :cond_4

    const-string v2, "GCM"

    goto :goto_2

    :cond_4
    const-string v6, "C2DM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v8, v6, :cond_2

    const-string v2, "C2DM"

    goto :goto_2

    .line 124
    :cond_5
    iget-object v6, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    .line 125
    iget-object v7, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->c:Ljava/lang/String;

    .line 124
    invoke-static {v6, v0, v7, v2}, Lcom/sec/pcw/device/c/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    const-string v2, "BIZ-0030"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 127
    const-string v2, "BIZ-0031"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 128
    :cond_6
    const/4 v0, 0x1

    goto :goto_3

    .line 154
    :cond_7
    iget-object v0, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/device/util/h;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/sec/pcw/device/c/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UnsupportedEncodingException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 161
    :catch_1
    move-exception v0

    .line 162
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MalformedURLException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 164
    :catch_2
    move-exception v0

    .line 165
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ProtocolException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/ProtocolException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 167
    :catch_3
    move-exception v0

    .line 168
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UnknownHostException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 170
    :catch_4
    move-exception v0

    .line 171
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 173
    :catch_5
    move-exception v0

    .line 174
    const-string v1, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto/16 :goto_3
.end method
