.class public final Lcom/sec/pcw/device/osp/b;
.super Lcom/sec/pcw/device/osp/AccountAppFacade2_0;
.source "AccountAppFacadeServiceIF.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/osp/b$a;
    }
.end annotation


# instance fields
.field g:Lcom/sec/pcw/device/osp/b$a;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;-><init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V

    .line 26
    iput-object v1, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    .line 27
    iput-object v1, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    .line 28
    const-string v0, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b;->j:Ljava/lang/String;

    .line 124
    iput-object v1, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a(JLjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 38
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v3, "request AuthInfo Update"

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "expiredToken : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    const-string v3, "1.5.0200"

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v0, :cond_b

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x10000

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_0
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "doesSupportAIDLInterface :: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_1

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    const-string v3, "2.0.0001"

    invoke-static {v0, v3}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 45
    :cond_1
    iput-boolean v2, p0, Lcom/sec/pcw/device/osp/b;->d:Z

    .line 46
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    if-eqz v0, :cond_2

    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v2, "bindService:: invalid bindingServiceLock status detected. release all lock"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_2
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    if-eqz v0, :cond_3

    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v2, "bindService:: invalid tokenRequestLock status detected. release all lock"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    new-instance v0, Lcom/sec/pcw/device/osp/b$a;

    invoke-direct {v0, p0}, Lcom/sec/pcw/device/osp/b$a;-><init>(Lcom/sec/pcw/device/osp/b;)V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "bindService:: successful "

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :goto_1
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wait service binding.. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 51
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :try_start_1
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V

    .line 51
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 59
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    invoke-virtual {v0, p3}, Lcom/sec/pcw/device/osp/b$a;->a(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/b$a;->b()V

    .line 62
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wait update auth info.. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 65
    :try_start_2
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 66
    :try_start_3
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V

    .line 65
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 75
    :cond_5
    :goto_3
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "unbindService::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/sec/pcw/device/osp/b;->h:Ljava/lang/Object;

    iput-object v6, p0, Lcom/sec/pcw/device/osp/b;->i:Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/b$a;->c()Z

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v6, p0, Lcom/sec/pcw/device/osp/b;->g:Lcom/sec/pcw/device/osp/b$a;

    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "unbindService:: successful "

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :goto_4
    iget-boolean v0, p0, Lcom/sec/pcw/device/osp/b;->d:Z

    :goto_5
    return v0

    :cond_6
    move v0, v2

    .line 41
    goto/16 :goto_0

    .line 46
    :cond_7
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "bindService:: failed!! "

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 51
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    goto :goto_2

    .line 65
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1

    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1

    .line 71
    :catch_1
    move-exception v0

    goto :goto_3

    .line 72
    :cond_8
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "fail to connect service"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 75
    :cond_9
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "unbindService:: invalid status - mServiceConnection is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 77
    :cond_a
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "AIDL Interface does not support"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-super {p0, p1, p2, p3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(JLjava/lang/String;)Z

    move-result v0

    goto :goto_5

    :cond_b
    move v0, v2

    goto/16 :goto_0
.end method
