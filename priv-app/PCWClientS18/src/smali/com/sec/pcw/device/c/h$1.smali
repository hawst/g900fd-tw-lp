.class final Lcom/sec/pcw/device/c/h$1;
.super Ljava/lang/Object;
.source "SecureHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/c/h;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/c/h;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/c/h;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 73
    const-string v1, "PCWCLIENTTRACE_SecureHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]handleMessage()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v1, "PCWCLIENTTRACE_SecureHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]message ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", message : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->c(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    new-instance v2, Lcom/sec/pcw/device/b/d;

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/pcw/device/b/d;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;Lcom/sec/pcw/device/b/d;)V

    .line 79
    iget-object v1, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    iget-object v2, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v2}, Lcom/sec/pcw/device/c/h;->e(Lcom/sec/pcw/device/c/h;)Lcom/sec/pcw/device/b/d;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v4}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/pcw/device/b/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;Z)V

    .line 80
    const-string v1, "PCWCLIENTTRACE_SecureHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]MessageID is SAME? : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->f(Lcom/sec/pcw/device/c/h;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v1, "RCV-0000"

    .line 86
    iget-object v2, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v2}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0xa

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 88
    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->g(Lcom/sec/pcw/device/c/h;)Lcom/sec/pcw/device/osp/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/pcw/device/osp/a;->b()Lcom/sec/pcw/device/osp/d;

    move-result-object v3

    .line 90
    const-string v4, "PCWCLIENTTRACE_SecureHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "recvGUID : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", auth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/pcw/device/osp/d;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v3}, Lcom/sec/pcw/device/osp/d;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/sec/pcw/device/osp/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;Z)V

    .line 107
    :goto_0
    iget-object v2, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v2}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v4}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v1, v4}, Lcom/sec/pcw/device/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 108
    iget-object v1, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v1}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/device/util/h;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 109
    const/4 v1, 0x0

    move v7, v0

    move-object v0, v1

    move v1, v7

    .line 110
    :goto_1
    const/4 v4, 0x4

    if-lt v1, v4, :cond_3

    .line 142
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v1}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v4}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v2, v3, v4}, Lcom/sec/pcw/device/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :goto_3
    return-void

    .line 100
    :cond_2
    const-string v1, "RCV-0205"

    .line 101
    const-string v2, "PCWCLIENTTRACE_SecureHandler"

    const-string v3, "Error.RCV_0205 - The value of GUID is not suitable"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    const-string v1, "PCWCLIENTTRACE_SecureHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v3}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]Error occured in handleMessage - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 112
    :cond_3
    :try_start_1
    const-string v0, "PCWCLIENTTRACE_SecureHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v5}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]delivery report trying ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v0}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v4}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/pcw/device/util/h;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v5}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v0, v2, v4, v5, v6}, Lcom/sec/pcw/device/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 116
    const-string v4, "PCWCLIENTTRACE_SecureHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v6}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]PUSH_DELIVERY_REPORT - ErrorCode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v4, "BIZ-0030"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 121
    iget-object v1, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/sec/pcw/device/c/h;->b(Lcom/sec/pcw/device/c/h;Z)V

    goto/16 :goto_2

    .line 123
    :cond_4
    iget-object v4, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    const-string v4, "unknown-response"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 124
    mul-int/lit16 v4, v1, 0x3e8

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 125
    const/4 v4, 0x3

    if-ne v1, v4, :cond_5

    .line 126
    new-instance v4, Lcom/sec/pcw/device/b/a;

    iget-object v5, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v5}, Lcom/sec/pcw/device/c/h;->d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/pcw/device/b/a;-><init>(Landroid/content/Context;)V

    .line 127
    invoke-static {v2}, Lcom/sec/pcw/device/b/a;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 129
    const-string v4, "PCWCLIENTTRACE_SecureHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v6}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]PUSH_DELIVERY_REPORT [Added to Database]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_5
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 133
    :cond_6
    const-string v4, "PCWCLIENTTRACE_SecureHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/pcw/device/c/h$1;->a:Lcom/sec/pcw/device/c/h;

    invoke-static {v6}, Lcom/sec/pcw/device/c/h;->a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]PUSH_DELIVERY_REPORT [Adding to Database was failed]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method
