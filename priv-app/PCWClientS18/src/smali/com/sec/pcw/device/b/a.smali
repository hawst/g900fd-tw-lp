.class public final Lcom/sec/pcw/device/b/a;
.super Ljava/lang/Object;
.source "DBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/b/a$a;
    }
.end annotation


# static fields
.field private static a:Lcom/sec/pcw/device/b/a$a;

.field private static b:Ljava/util/concurrent/Semaphore;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    sget-object v0, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/sec/pcw/device/b/a$a;

    const-string v1, "pcw.db"

    invoke-direct {v0, p1, v1}, Lcom/sec/pcw/device/b/a$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    .line 111
    :cond_0
    sget-object v0, Lcom/sec/pcw/device/b/a;->b:Ljava/util/concurrent/Semaphore;

    if-nez v0, :cond_1

    .line 113
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v2, v2}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    sput-object v0, Lcom/sec/pcw/device/b/a;->b:Ljava/util/concurrent/Semaphore;

    .line 115
    :cond_1
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 321
    .line 322
    const/4 v11, -0x1

    .line 324
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "delivery_report"

    .line 325
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v3, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 326
    const-string v8, "_id ASC"

    const-string v9, "1"

    move-object v0, p0

    .line 324
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 328
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 334
    :goto_0
    if-eqz v1, :cond_0

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 339
    :cond_0
    return v0

    .line 333
    :catchall_0
    move-exception v0

    move-object v1, v10

    .line 334
    :goto_1
    if-eqz v1, :cond_1

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 337
    :cond_1
    throw v0

    .line 333
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v11

    goto :goto_0
.end method

.method private static a(Z)Landroid/database/sqlite/SQLiteDatabase;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 126
    sget-object v1, Lcom/sec/pcw/device/b/a;->b:Ljava/util/concurrent/Semaphore;

    if-eqz v1, :cond_0

    .line 130
    :try_start_0
    const-string v1, "PCWCLIENTTRACE_DBAdapter"

    const-string v2, "dblock acquire - before"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v1, Lcom/sec/pcw/device/b/a;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 132
    const-string v1, "PCWCLIENTTRACE_DBAdapter"

    const-string v2, "dblock acquire - after"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    if-eqz v1, :cond_1

    .line 143
    if-eqz p0, :cond_2

    .line 145
    sget-object v0, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    invoke-virtual {v0}, Lcom/sec/pcw/device/b/a$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 153
    :cond_1
    :goto_1
    return-object v0

    .line 136
    :catch_0
    move-exception v1

    const-string v1, "PCWCLIENTTRACE_DBAdapter"

    const-string v2, " InterruptedException is thrown and the current thread\'s interrupted status is cleared"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_2
    sget-object v0, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    invoke-virtual {v0}, Lcom/sec/pcw/device/b/a$a;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto :goto_1
.end method

.method public static a()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 280
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 283
    invoke-static {v0}, Lcom/sec/pcw/device/b/a;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 285
    if-nez v0, :cond_0

    .line 286
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Database is not open."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "delivery_report"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 291
    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "created_date"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "report"

    aput-object v5, v3, v4

    .line 292
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 290
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 294
    if-eqz v2, :cond_1

    .line 295
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_4

    .line 301
    :cond_1
    if-eqz v2, :cond_2

    .line 302
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_2
    if-eqz v0, :cond_3

    .line 306
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    .line 307
    :cond_3
    return-object v11

    .line 297
    :cond_4
    :try_start_2
    const-string v1, "report"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 296
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v1

    .line 301
    :goto_1
    if-eqz v2, :cond_5

    .line 302
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_5
    if-eqz v0, :cond_6

    .line 306
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    .line 307
    :cond_6
    throw v1

    .line 300
    :catchall_1
    move-exception v1

    move-object v2, v10

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 184
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v0

    .line 187
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 188
    const-string v3, "created_date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 189
    const-string v3, "report"

    invoke-virtual {v2, v3, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-static {v1}, Lcom/sec/pcw/device/b/a;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 194
    if-nez v3, :cond_2

    .line 195
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Database is not open."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 200
    :try_start_0
    invoke-static {v3}, Lcom/sec/pcw/device/b/a;->b(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v4

    .line 202
    const/16 v5, 0x13

    if-ge v5, v4, :cond_3

    .line 203
    invoke-static {v3}, Lcom/sec/pcw/device/b/a;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v4

    const-string v5, "delivery_report"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-virtual {v3, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 206
    :cond_3
    const-string v4, "delivery_report"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 208
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 214
    if-eqz v3, :cond_4

    .line 215
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    .line 216
    :cond_4
    const-wide/16 v2, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 223
    goto :goto_0

    :catch_0
    move-exception v1

    .line 212
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 214
    if-eqz v3, :cond_0

    .line 215
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    goto :goto_0

    .line 211
    :catchall_0
    move-exception v0

    .line 212
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 214
    if-eqz v3, :cond_5

    .line 215
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    .line 216
    :cond_5
    throw v0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 349
    .line 353
    :try_start_0
    const-string v1, "delivery_report"

    const/4 v0, 0x0

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 354
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "_id ASC"

    move-object v0, p0

    .line 353
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 356
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 358
    if-eqz v1, :cond_0

    .line 359
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 363
    :cond_0
    return v0

    .line 357
    :catchall_0
    move-exception v0

    move-object v1, v8

    .line 358
    :goto_0
    if-eqz v1, :cond_1

    .line 359
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 361
    :cond_1
    throw v0

    .line 357
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static b()V
    .locals 2

    .prologue
    .line 161
    sget-object v0, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    if-eqz v0, :cond_0

    .line 162
    sget-object v0, Lcom/sec/pcw/device/b/a;->a:Lcom/sec/pcw/device/b/a$a;

    invoke-virtual {v0}, Lcom/sec/pcw/device/b/a$a;->close()V

    .line 165
    :cond_0
    sget-object v0, Lcom/sec/pcw/device/b/a;->b:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_1

    .line 167
    const-string v0, "PCWCLIENTTRACE_DBAdapter"

    const-string v1, "dblock release - before"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sget-object v0, Lcom/sec/pcw/device/b/a;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 169
    const-string v0, "PCWCLIENTTRACE_DBAdapter"

    const-string v1, "dblock release - after"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_1
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 239
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    :goto_0
    return v9

    .line 245
    :cond_0
    invoke-static {v9}, Lcom/sec/pcw/device/b/a;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 247
    if-nez v0, :cond_1

    .line 248
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Database is not open."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_1
    :try_start_0
    const-string v1, "delivery_report"

    .line 253
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "report"

    aput-object v4, v2, v3

    const-string v3, "report=?"

    .line 254
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 252
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 256
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v1, :cond_4

    move v1, v8

    .line 258
    :goto_1
    if-eqz v2, :cond_2

    .line 259
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 260
    :cond_2
    if-eqz v0, :cond_3

    .line 264
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    :cond_3
    move v9, v1

    .line 265
    goto :goto_0

    :cond_4
    move v1, v9

    .line 256
    goto :goto_1

    .line 257
    :catchall_0
    move-exception v1

    move-object v2, v10

    .line 258
    :goto_2
    if-eqz v2, :cond_5

    .line 259
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 260
    :cond_5
    if-eqz v0, :cond_6

    .line 264
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    .line 265
    :cond_6
    throw v1

    .line 257
    :catchall_1
    move-exception v1

    goto :goto_2
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 376
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 394
    :goto_0
    return v1

    .line 382
    :cond_0
    invoke-static {v0}, Lcom/sec/pcw/device/b/a;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 384
    if-nez v2, :cond_1

    .line 385
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Database is not open."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389
    :cond_1
    :try_start_0
    const-string v3, "delivery_report"

    .line 390
    const-string v4, "report=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    .line 389
    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lez v3, :cond_3

    .line 392
    :goto_1
    if-eqz v2, :cond_2

    .line 393
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    :cond_2
    move v1, v0

    .line 394
    goto :goto_0

    :cond_3
    move v0, v1

    .line 389
    goto :goto_1

    .line 391
    :catchall_0
    move-exception v0

    .line 392
    if-eqz v2, :cond_4

    .line 393
    invoke-static {}, Lcom/sec/pcw/device/b/a;->b()V

    .line 394
    :cond_4
    throw v0
.end method
