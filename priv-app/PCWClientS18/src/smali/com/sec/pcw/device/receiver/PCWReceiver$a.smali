.class public final Lcom/sec/pcw/device/receiver/PCWReceiver$a;
.super Ljava/lang/Object;
.source "PCWReceiver.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/receiver/PCWReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 79
    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->c()V

    .line 81
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "called uncaughtException - don\'t support the reactivationlock"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "set alarm of ACTION_CHECK_REACTIVATION_SA_LOGIN"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;Z)V

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.pcw.device.CHECK_REACTIVATION_SA_LOGIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    const-string v1, "login_id"

    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->a()Landroid/content/Context;

    move-result-object v1

    .line 87
    const/high16 v2, 0x8000000

    .line 86
    invoke-static {v1, v7, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 89
    invoke-static {}, Lcom/sec/pcw/device/receiver/PCWReceiver;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 90
    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 94
    :cond_0
    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    .line 95
    return-void
.end method
