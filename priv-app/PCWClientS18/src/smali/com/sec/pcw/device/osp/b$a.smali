.class public final Lcom/sec/pcw/device/osp/b$a;
.super Ljava/lang/Object;
.source "AccountAppFacadeServiceIF.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/osp/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/osp/b$a$a;
    }
.end annotation


# instance fields
.field a:Lcom/sec/pcw/device/osp/b$a$a;

.field b:Ljava/lang/String;

.field c:Lcom/a/a/a/b;

.field final synthetic d:Lcom/sec/pcw/device/osp/b;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/pcw/device/osp/b;)V
    .locals 1

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/pcw/device/osp/b$a;->d:Lcom/sec/pcw/device/osp/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->d:Lcom/sec/pcw/device/osp/b;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 187
    const-string v2, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v3, "requestToken:: "

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    if-eqz v2, :cond_4

    .line 190
    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    monitor-enter v2

    .line 191
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/b$a;->a()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 192
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "requestToken :: failed (reason : isServiceBiding() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/b$a;->a()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", this.mRegistrationCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    :cond_0
    :try_start_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 197
    const/16 v4, 0x9

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 198
    const-string v6, "user_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 199
    const-string v6, "birthday"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    .line 200
    const-string v6, "mcc"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 201
    const-string v6, "server_url"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    .line 202
    const-string v6, "api_server_url"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    .line 203
    const-string v6, "auth_server_url"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    .line 204
    const-string v6, "cc"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    .line 205
    const-string v6, "device_physical_address_text"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    .line 206
    const-string v6, "refresh_token"

    aput-object v6, v4, v5

    .line 208
    const-string v5, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onServiceConnected :: isTokenExpired : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    if-eqz v7, :cond_1

    :goto_0
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 210
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "[expired_access_token] Reqeust New token : "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v0, "expired_access_token"

    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :goto_1
    const-string v0, "additional"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    const/4 v1, 0x1

    iget-object v4, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v4, v3}, Lcom/a/a/a/b;->a(ILjava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    .line 219
    if-eqz v0, :cond_3

    .line 220
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onServiceConnected :: requestAccessToken  isRequestSuccess : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    :goto_2
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 233
    :goto_3
    return-void

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0

    .line 213
    :cond_2
    :try_start_3
    const-string v0, "expired_access_token"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 225
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 222
    :cond_3
    :try_start_5
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onServiceConnected :: requestAccessToken  isRequestSuccess : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 226
    :catch_1
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 231
    :cond_4
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "SAServiceConnection::mISaService instance is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final c()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 236
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "SAServiceConnection::close::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v0, 0x0

    .line 239
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 240
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/a/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    .line 241
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SAServiceConnection::close::unregisterCallback:: result - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :goto_0
    iput-object v4, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    .line 250
    iput-object v4, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    .line 251
    iput-object v4, p0, Lcom/sec/pcw/device/osp/b$a;->e:Ljava/lang/String;

    .line 253
    return v0

    .line 243
    :cond_0
    :try_start_1
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v2, "SAServiceConnection::close::unregisterCallback is ignored due to (null == this.mISaService or null == this.mRegistrationCode)"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    .line 148
    invoke-static {p2}, Lcom/a/a/a/b$a;->a(Landroid/os/IBinder;)Lcom/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    .line 149
    new-instance v0, Lcom/sec/pcw/device/osp/b$a$a;

    invoke-direct {v0, p0}, Lcom/sec/pcw/device/osp/b$a$a;-><init>(Lcom/sec/pcw/device/osp/b$a;)V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->a:Lcom/sec/pcw/device/osp/b$a$a;

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->c:Lcom/a/a/a/b;

    .line 152
    const-string v1, "dlw5n54c92"

    .line 153
    const-string v2, "72F2B3FEBD1B1C59A2255784AD9AFF9D"

    .line 154
    iget-object v3, p0, Lcom/sec/pcw/device/osp/b$a;->d:Lcom/sec/pcw/device/osp/b;

    iget-object v3, v3, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 155
    iget-object v4, p0, Lcom/sec/pcw/device/osp/b$a;->a:Lcom/sec/pcw/device/osp/b$a$a;

    .line 151
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/a/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/a/a/a/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    .line 157
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "onServiceConnected :: registerCallback"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceConnected ::mRegistrationCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 161
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "onServiceConnected :: mRegistrationCode is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->d:Lcom/sec/pcw/device/osp/b;

    invoke-static {v0}, Lcom/sec/pcw/device/osp/b;->b(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 170
    :try_start_1
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->d:Lcom/sec/pcw/device/osp/b;

    invoke-static {v0}, Lcom/sec/pcw/device/osp/b;->b(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 171
    :try_start_2
    iget-object v0, p0, Lcom/sec/pcw/device/osp/b$a;->d:Lcom/sec/pcw/device/osp/b;

    invoke-static {v0}, Lcom/sec/pcw/device/osp/b;->b(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 170
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 173
    :try_start_3
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "onServiceConnected :: bindingServiceLock.notify() is successful"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 178
    :cond_1
    :goto_1
    return-void

    .line 164
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 182
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "onServiceDisconnected :: SA Service is disconnected"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method
