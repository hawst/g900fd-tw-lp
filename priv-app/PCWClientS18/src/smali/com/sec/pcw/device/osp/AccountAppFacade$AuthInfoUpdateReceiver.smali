.class public Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccountAppFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/osp/AccountAppFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AuthInfoUpdateReceiver"
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/osp/AccountAppFacade;


# direct methods
.method protected constructor <init>(Lcom/sec/pcw/device/osp/AccountAppFacade;)V
    .locals 0

    .prologue
    .line 461
    iput-object p1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 464
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v1, "onReceive() "

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 467
    :cond_0
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v1, "intent or intent.getAction() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_1
    :goto_0
    return-void

    .line 471
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 472
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->e:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 474
    sget-object v2, Lcom/sec/pcw/device/util/a/a;->c:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 475
    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v2, v2, Lcom/sec/pcw/device/osp/AccountAppFacade;->b:Lcom/sec/pcw/device/osp/c;

    invoke-virtual {v2}, Lcom/sec/pcw/device/osp/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 476
    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v2, v2, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_3

    .line 477
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    .line 478
    const-string v1, "Success response to auth info update, from samsung account app."

    .line 477
    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->d:Z

    .line 482
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 483
    :try_start_1
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 482
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 488
    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/sec/pcw/device/util/a/a;->d:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->b:Lcom/sec/pcw/device/osp/c;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 491
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    .line 492
    const-string v1, "Fail response to auth info update, from samsung account app."

    .line 491
    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->d:Z

    .line 496
    :try_start_3
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    monitor-enter v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 497
    :try_start_4
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 496
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method
