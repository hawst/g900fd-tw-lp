.class public Lcom/sec/pcw/device/service/C2DMRegistrationService;
.super Landroid/app/Service;
.source "C2DMRegistrationService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 67
    const-string v0, "PCWCLIENTTRACE_C2DMRegistrationService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 23
    const-string v0, "PCWCLIENTTRACE_C2DMRegistrationService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 30
    const-string v0, "PCWCLIENTTRACE_C2DMRegistrationService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 32
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 38
    const-string v0, "userId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    const-string v1, "isFirstRegist"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 40
    const-string v2, "PCWCLIENTTRACE_C2DMRegistrationService"

    const-string v3, "onStartCommand"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/pcw/device/service/C2DMRegistrationService$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/pcw/device/service/C2DMRegistrationService$1;-><init>(Lcom/sec/pcw/device/service/C2DMRegistrationService;ZLjava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 59
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 61
    const/4 v0, 0x3

    return v0
.end method
