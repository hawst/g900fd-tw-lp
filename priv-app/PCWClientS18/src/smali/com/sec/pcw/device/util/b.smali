.class public final Lcom/sec/pcw/device/util/b;
.super Ljava/lang/Object;
.source "AccountUtil.java"


# direct methods
.method public static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 34
    const-string v0, "com.google"

    invoke-static {p0, v0}, Lcom/sec/pcw/device/util/b;->c(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 38
    const-string v1, "PCWCLIENTTRACE_AccountUtil"

    const-string v2, "[hasGoogleAccount] - Google Account exist"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v1, "PCWCLIENTTRACE_AccountUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "account length: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x1

    .line 43
    :goto_0
    return v0

    .line 42
    :cond_0
    const-string v0, "PCWCLIENTTRACE_AccountUtil"

    const-string v1, "[hasGoogleAccount] - No Google Account"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 90
    const-string v0, "com.google"

    invoke-static {p0, v0}, Lcom/sec/pcw/device/util/b;->c(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 92
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 93
    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_1

    .line 101
    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 94
    :cond_1
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    const-string v0, "PCWCLIENTTRACE_AccountUtil"

    const-string v2, "Google Account exist"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 93
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 60
    const-string v0, "com.osp.app.signin"

    invoke-static {p0, v0}, Lcom/sec/pcw/device/util/b;->c(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 64
    const-string v0, "PCWCLIENTTRACE_AccountUtil"

    const-string v1, "[hasSamungAccount] - Samsung Account exist"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    .line 67
    :cond_0
    const-string v0, "PCWCLIENTTRACE_AccountUtil"

    const-string v1, "[hasSamungAccount] - No Samsung Account"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 158
    const-string v2, "PCWCLIENTTRACE_AccountUtil"

    const-string v3, "[isSAVersionHigherThanOrEqual]"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 164
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 165
    const-string v3, "com.osp.app.signin"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 166
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 168
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 169
    const-string v3, "\\."

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 170
    const-string v3, "\\."

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 171
    const-string v3, "PCWCLIENTTRACE_AccountUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "\trequireVersion : "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v3, "PCWCLIENTTRACE_AccountUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "\tcurrentVersion : "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 177
    :goto_0
    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    .line 195
    :cond_0
    :goto_1
    return v0

    .line 180
    :cond_1
    array-length v3, v5

    if-le v3, v2, :cond_5

    .line 181
    aget-object v3, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move v4, v3

    .line 182
    :goto_2
    array-length v3, v6

    if-le v3, v2, :cond_4

    .line 183
    aget-object v3, v6, v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 184
    :goto_3
    if-lt v4, v3, :cond_0

    .line 185
    if-le v4, v3, :cond_2

    move v0, v1

    goto :goto_1

    .line 177
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 192
    const-string v2, "PCWCLIENTTRACE_AccountUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\tError : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v1

    .line 195
    goto :goto_1

    :cond_4
    move v3, v1

    goto :goto_3

    :cond_5
    move v4, v1

    goto :goto_2
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    const-string v0, "com.google"

    invoke-static {p0, v0}, Lcom/sec/pcw/device/util/b;->c(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 82
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 85
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 139
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 140
    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 112
    const-string v0, ""

    .line 113
    const-string v1, "com.osp.app.signin"

    invoke-static {p0, v1}, Lcom/sec/pcw/device/util/b;->c(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 115
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    const/4 v0, 0x0

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 125
    :cond_0
    :goto_0
    return-object v0

    .line 118
    :cond_1
    new-instance v1, Lcom/sec/pcw/device/osp/a;

    invoke-direct {v1, p0}, Lcom/sec/pcw/device/osp/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/pcw/device/osp/a;->c()Lcom/sec/pcw/device/osp/d;

    move-result-object v1

    .line 119
    invoke-virtual {v1}, Lcom/sec/pcw/device/osp/d;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 120
    const-string v0, "PCWCLIENTTRACE_AccountUtil"

    const-string v2, "samsung account does not exist, but it is attempted restoring from secure data!!"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {v1}, Lcom/sec/pcw/device/osp/d;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
