.class public final Lcom/sec/pcw/device/c/h;
.super Ljava/lang/Object;
.source "SecureHandler.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/pcw/device/osp/a;

.field private e:Landroid/content/Context;

.field private f:Ljava/lang/String;

.field private g:Lcom/sec/pcw/device/b/d;

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->a:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->b:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->c:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->d:Lcom/sec/pcw/device/osp/a;

    .line 36
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->e:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->f:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->g:Lcom/sec/pcw/device/b/d;

    .line 44
    iput-boolean v1, p0, Lcom/sec/pcw/device/c/h;->h:Z

    .line 49
    iput-boolean v1, p0, Lcom/sec/pcw/device/c/h;->i:Z

    .line 55
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p3, :cond_1

    .line 56
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "the context or the message or the message id is null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->e:Landroid/content/Context;

    .line 60
    iput-object p5, p0, Lcom/sec/pcw/device/c/h;->a:Ljava/lang/String;

    .line 61
    iput-object p3, p0, Lcom/sec/pcw/device/c/h;->b:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/sec/pcw/device/c/h;->c:Ljava/lang/String;

    .line 63
    new-instance v0, Lcom/sec/pcw/device/osp/a;

    iget-object v1, p0, Lcom/sec/pcw/device/c/h;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/device/osp/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/pcw/device/c/h;->d:Lcom/sec/pcw/device/osp/a;

    .line 64
    iput-object p4, p0, Lcom/sec/pcw/device/c/h;->f:Ljava/lang/String;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/pcw/device/c/h;Lcom/sec/pcw/device/b/d;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/pcw/device/c/h;->g:Lcom/sec/pcw/device/b/d;

    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/device/c/h;Z)V
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/pcw/device/c/h;->i:Z

    return-void
.end method

.method static synthetic b(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/pcw/device/c/h;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 159
    const-string v0, "PCWCLIENTTRACE_SecureHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mIsMessageSentToDMDS : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/pcw/device/c/h;->h:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nsameMessageID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/pcw/device/c/h;->i:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/pcw/device/c/h;->h:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/pcw/device/c/h;->i:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/pcw/device/c/h;->h:Z

    const-string v0, "PCWCLIENTTRACE_SecureHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "callDMDSClient - afterDeliveryReporting : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/c/h;->b:Ljava/lang/String;

    const-string v2, "PCWCLIENTTRACE_PushMsgHandler"

    const-string v3, "callDMClient"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PCWCLIENTTRACE_PushMsgHandler"

    const-string v3, "CHECKPOINT2 - CALL DM"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "pdus"

    const-string v4, "UTF8"

    invoke-virtual {v1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {v2}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Intent;)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/c/h;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/pcw/device/c/h;->b:Ljava/lang/String;

    const-string v3, "PCWCLIENTTRACE_PushMsgHandler"

    const-string v4, "callDSClient"

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.IP_PUSH_DS_NOTI_RECEIVED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "PAY"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "push_message"

    const-string v4, "UTF8"

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :goto_1
    invoke-static {v3}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Intent;)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string v1, "push_message"

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    :cond_2
    const-string v0, "PCWCLIENTTRACE_SecureHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "we dont call callDMDSClient because sameMessageID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/pcw/device/c/h;->i:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/pcw/device/c/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/pcw/device/c/h;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/pcw/device/c/h;)Lcom/sec/pcw/device/b/d;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->g:Lcom/sec/pcw/device/b/d;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/pcw/device/c/h;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/pcw/device/c/h;->i:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/pcw/device/c/h;)Lcom/sec/pcw/device/osp/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/pcw/device/c/h;->d:Lcom/sec/pcw/device/osp/a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/device/c/h$1;

    invoke-direct {v1, p0}, Lcom/sec/pcw/device/c/h$1;-><init>(Lcom/sec/pcw/device/c/h;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 152
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 153
    return-void
.end method
