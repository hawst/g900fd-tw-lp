.class public final Lcom/sec/pcw/device/osp/a;
.super Ljava/lang/Object;
.source "AccountAppFacadeDelegate.java"


# instance fields
.field a:Lcom/sec/pcw/device/osp/AccountAppFacade;

.field b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/pcw/device/osp/a;->b:Z

    .line 20
    const-string v0, "1.3.053"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 22
    new-instance v1, Lcom/sec/pcw/device/osp/c;

    const-string v2, "PCW"

    const-string v3, "dlw5n54c92"

    const-string v4, "72F2B3FEBD1B1C59A2255784AD9AFF9D"

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/osp/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2, v1, v0}, Lcom/sec/pcw/device/osp/a;->a(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;Z)V

    .line 25
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;Z)V
    .locals 2

    .prologue
    .line 50
    if-eqz p3, :cond_0

    .line 53
    new-instance v0, Lcom/sec/pcw/device/osp/b;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/pcw/device/osp/b;-><init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/pcw/device/osp/a;->b:Z

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Lcom/sec/pcw/device/osp/AccountAppFacade;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/pcw/device/osp/AccountAppFacade;-><init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/pcw/device/osp/a;->b:Z

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    .line 35
    const-string v1, "1.3.053"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 37
    iget-boolean v2, p0, Lcom/sec/pcw/device/osp/a;->b:Z

    if-eq v2, v1, :cond_0

    .line 39
    iget-object v2, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    iget-object v2, v2, Lcom/sec/pcw/device/osp/AccountAppFacade;->b:Lcom/sec/pcw/device/osp/c;

    .line 41
    invoke-direct {p0, v0, v2, v1}, Lcom/sec/pcw/device/osp/a;->a(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;Z)V

    .line 43
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/sec/pcw/device/osp/d;
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/a;->e()V

    .line 68
    iget-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->b()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/sec/pcw/device/osp/d;
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/a;->e()V

    .line 73
    iget-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->c()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/sec/pcw/device/osp/d;
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/a;->e()V

    .line 78
    iget-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->d()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 87
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeDelegate"

    const-string v1, "clearAuthInfo"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/pcw/device/osp/a;->a:Lcom/sec/pcw/device/osp/AccountAppFacade;

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->e()V

    .line 90
    return-void
.end method
