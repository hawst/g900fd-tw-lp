.class public final Lcom/sec/pcw/device/b/e;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PushFilterDBOpenHelper.java"


# static fields
.field static a:Ljava/lang/String;

.field static b:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

.field static c:I


# instance fields
.field d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "pushfilter.db"

    sput-object v0, Lcom/sec/pcw/device/b/e;->a:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/device/b/e;->b:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    .line 13
    const/4 v0, 0x1

    sput v0, Lcom/sec/pcw/device/b/e;->c:I

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 22
    sget-object v0, Lcom/sec/pcw/device/b/e;->a:Ljava/lang/String;

    sget-object v1, Lcom/sec/pcw/device/b/e;->b:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    sget v2, Lcom/sec/pcw/device/b/e;->c:I

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 23
    iput-object p1, p0, Lcom/sec/pcw/device/b/e;->d:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 29
    const-string v0, "CREATE TABLE pushfilter ( _id INTEGER PRIMARY KEY autoincrement, time INTEGER , messageid TEXT,savetime DATETIME DEFAULT (date(\'now\')))"

    .line 34
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    const-string v1, "PCWCLIENTTRACE_PushFilterDBOpenHelper"

    const-string v2, "Create DB FAIL"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 43
    const-string v0, "DROP TABLE IF EXISTS pushfilter"

    .line 44
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/pcw/device/b/e;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    return-void
.end method
