.class public Lcom/sec/pcw/device/util/SecurePreferencesJNI;
.super Ljava/lang/Object;
.source "SecurePreferencesJNI.java"


# static fields
.field private static a:Z

.field private static b:Lcom/sec/pcw/device/util/SecurePreferencesJNI;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 10
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    .line 22
    :try_start_0
    const-string v0, "terrier"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 127
    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->b:Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    return-void

    .line 26
    :catch_0
    move-exception v0

    const-string v0, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v1, "failed to loading secure library"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    sput-boolean v2, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_0

    .line 29
    :catch_1
    move-exception v0

    const-string v0, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v1, "failed to loading secure library"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    sput-boolean v2, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sec/pcw/device/util/SecurePreferencesJNI;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->b:Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    invoke-direct {v0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;-><init>()V

    sput-object v0, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->b:Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    .line 134
    :cond_0
    sget-object v0, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->b:Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    return-object v0
.end method

.method private native getRequiredAuthFlag()I
.end method

.method private native getServiceFlag()I
.end method

.method private native getString()Ljava/lang/String;
.end method

.method private native removeString()V
.end method

.method private native setString(Ljava/lang/String;)I
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v5, 0x1

    .line 185
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "[SetString]"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    sget-boolean v1, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    if-eqz v1, :cond_0

    .line 189
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "SetString : secure API is not supported!!"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :goto_0
    return v0

    .line 194
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->getRequiredAuthFlag()I

    move-result v1

    if-ne v5, v1, :cond_1

    invoke-direct {p0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->getServiceFlag()I

    move-result v1

    if-ne v5, v1, :cond_1

    .line 195
    invoke-direct {p0, p1}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->setString(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 207
    :goto_1
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SetString] ret : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_1
    :try_start_1
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "SetString : Service Flag or Required Auth Flag is not ON, so SetString() is ignored"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 199
    :catch_0
    move-exception v1

    .line 200
    const-string v2, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SetString : secure API is not supported!! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sput-boolean v5, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_1

    .line 202
    :catch_1
    move-exception v1

    .line 203
    const-string v2, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SetString : secure API is not supported!! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    sput-boolean v5, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 217
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "[GetString-S]"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    sget-boolean v1, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    if-eqz v1, :cond_0

    .line 221
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "GetString : secure API is not supported!!"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :goto_0
    return-object v0

    .line 226
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->getString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 235
    :goto_1
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "[GetString-E]"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 227
    :catch_0
    move-exception v1

    .line 228
    const-string v2, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetString : secure API is not supported!! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    sput-boolean v5, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_1

    .line 230
    :catch_1
    move-exception v1

    .line 231
    const-string v2, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetString : secure API is not supported!! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    sput-boolean v5, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_1
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 244
    const-string v2, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v3, "[RemoveString]"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-boolean v2, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    if-eqz v2, :cond_1

    .line 247
    const-string v1, "PCWCLIENTTRACE_SecurePreferencesJNI"

    const-string v2, "RemoveString : secure API is not supported!!"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    :goto_0
    return v0

    .line 252
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->removeString()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 261
    :goto_1
    sget-boolean v2, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 253
    :catch_0
    move-exception v2

    .line 254
    const-string v3, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RemoveString : secure API is not supported!! "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sput-boolean v1, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_1

    .line 256
    :catch_1
    move-exception v2

    .line 257
    const-string v3, "PCWCLIENTTRACE_SecurePreferencesJNI"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RemoveString : secure API is not supported!! "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sput-boolean v1, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a:Z

    goto :goto_1
.end method

.method public native getVersion()Ljava/lang/String;
.end method
