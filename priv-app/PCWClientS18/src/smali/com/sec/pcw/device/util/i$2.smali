.class final Lcom/sec/pcw/device/util/i$2;
.super Ljava/lang/Object;
.source "SppAPI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/util/i;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/util/i;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/util/i;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/util/i$2;->a:Lcom/sec/pcw/device/util/i;

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$2;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->b(Lcom/sec/pcw/device/util/i;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 82
    const-string v1, "PCWCLIENTTRACE_SppAPI"

    const-string v2, "startService requested"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v1, "PCWCLIENTTRACE_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ComponentName : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/sec/pcw/device/util/i$2;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v1}, Lcom/sec/pcw/device/util/i;->b(Lcom/sec/pcw/device/util/i;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/device/util/i$2;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v2}, Lcom/sec/pcw/device/util/i;->c(Lcom/sec/pcw/device/util/i;)Landroid/content/ServiceConnection;

    move-result-object v2

    .line 92
    const/4 v3, 0x1

    .line 91
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 94
    const-string v1, "PCWCLIENTTRACE_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindService(mPushClientConnection) requested. bResult="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method
