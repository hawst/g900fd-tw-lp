.class final Lcom/sec/pcw/device/util/i$3;
.super Ljava/lang/Object;
.source "SppAPI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/util/i;->close()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/util/i;

.field private final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/util/i;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/util/i$3;->a:Lcom/sec/pcw/device/util/i;

    iput-object p2, p0, Lcom/sec/pcw/device/util/i$3;->b:Landroid/content/Context;

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$3;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/pcw/device/util/i;)Landroid/os/ConditionVariable;

    move-result-object v0

    const-wide/32 v1, 0xea60

    invoke-virtual {v0, v1, v2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->a(Lcom/sec/a/a/a;)V

    .line 149
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$3;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v0}, Lcom/sec/pcw/device/util/i;->d(Lcom/sec/pcw/device/util/i;)V

    .line 151
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "unbindService(mPushClientConnection) requested."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/sec/pcw/device/util/i$3;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/util/i$3;->a:Lcom/sec/pcw/device/util/i;

    invoke-static {v1}, Lcom/sec/pcw/device/util/i;->c(Lcom/sec/pcw/device/util/i;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 155
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "unbindService(mPushClientConnection) finished."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    const-string v0, "PCWCLIENTTRACE_SppAPI"

    const-string v1, "unbindService(mPushClientConnection) failed. - Service not registered"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
