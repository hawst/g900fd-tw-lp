.class final Lcom/sec/pcw/device/service/GCMRegistrationService$1;
.super Ljava/lang/Object;
.source "GCMRegistrationService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/service/GCMRegistrationService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/service/GCMRegistrationService;

.field private final synthetic b:Z

.field private final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/service/GCMRegistrationService;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->a:Lcom/sec/pcw/device/service/GCMRegistrationService;

    iput-boolean p2, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->b:Z

    iput-object p3, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->c:Ljava/lang/String;

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "PCWCLIENTTRACE_GCMRegistrationService"

    const-string v1, "[GCM]start Thread"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-boolean v0, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->b:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->a:Lcom/sec/pcw/device/service/GCMRegistrationService;

    iget-object v1, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/pcw/device/c/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->a:Lcom/sec/pcw/device/service/GCMRegistrationService;

    const-string v1, "GCM"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/c/f;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    const-string v1, "BIZ-0010"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 53
    const-string v1, "BIZ-0011"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    const-string v1, "BIZ-0012"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    :cond_1
    const-string v0, "PCWCLIENTTRACE_GCMRegistrationService"

    const-string v1, "[GCM]stopSelf"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/sec/pcw/device/service/GCMRegistrationService$1;->a:Lcom/sec/pcw/device/service/GCMRegistrationService;

    invoke-virtual {v0}, Lcom/sec/pcw/device/service/GCMRegistrationService;->stopSelf()V

    .line 58
    :cond_2
    return-void
.end method
