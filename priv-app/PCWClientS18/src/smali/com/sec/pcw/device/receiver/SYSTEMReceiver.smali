.class public Lcom/sec/pcw/device/receiver/SYSTEMReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SYSTEMReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/receiver/SYSTEMReceiver$a;
    }
.end annotation


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Z

.field private static final c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a:Landroid/content/Context;

    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b:Z

    .line 267
    new-instance v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$1;

    invoke-direct {v0}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$1;-><init>()V

    sput-object v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->c:Landroid/os/Handler;

    .line 293
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 251
    invoke-static {p0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 256
    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-lt v0, v2, :cond_0

    .line 262
    invoke-static {p0}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b(Landroid/content/Context;)V

    .line 263
    invoke-static {p0}, Lcom/sec/pcw/device/c/d;->b(Landroid/content/Context;)V

    .line 264
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "ACTION_PACKAGE_REPLACED_END"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-void

    .line 257
    :cond_0
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;Ljava/lang/String;)Z

    .line 258
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/util/g;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 259
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->e(Landroid/content/Context;)V

    .line 303
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->g(Landroid/content/Context;)V

    .line 304
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;)Z

    .line 305
    const-string v0, "com.sec.pcw.device"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 306
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->j(Landroid/content/Context;)V

    .line 307
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->l(Landroid/content/Context;)V

    .line 308
    const-string v0, "com.sec.pcw.device"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "network_backoff"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 309
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 46
    sget-boolean v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b:Z

    return v0
.end method

.method static synthetic c()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 71
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 72
    :cond_0
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "intent or intent.getAction() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_1
    :goto_0
    return-void

    .line 76
    :cond_2
    sput-object p1, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a:Landroid/content/Context;

    .line 78
    invoke-static {p1}, Lcom/sec/pcw/device/util/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 79
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "Action ignored because FMM just support in the case of master account"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-static {p1}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 85
    const-string v2, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onReceive] - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 89
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v2, "ACTION_BOOTUP - Version: 4.80"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACTION_BOOTUP - Push type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-static {p1}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b(Landroid/content/Context;)V

    .line 96
    invoke-static {p1}, Lcom/sec/pcw/device/c/d;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 99
    :cond_4
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 101
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 103
    const-string v1, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mPkgName:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v1, "com.sec.spp.push"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    const-string v0, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 108
    const-string v1, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 110
    if-eqz v1, :cond_5

    if-eqz v0, :cond_7

    .line 111
    :cond_5
    if-eqz v0, :cond_6

    .line 112
    const-string v2, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SPPClient] replacing : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", dataRemoved : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_6
    const-string v0, "SPP"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;Ljava/lang/String;)Z

    .line 116
    const-string v0, "SPP"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/g;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 117
    const-string v0, "SPP"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :cond_7
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "SPPClient uninstalled for updating. the deregistraion for spp is ignored."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 127
    :cond_8
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 129
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 131
    const-string v1, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mPkgName:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v1, "com.sec.spp.push"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    const-string v0, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 137
    if-nez v0, :cond_9

    .line 138
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "SPPClient installed . Request register"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v0, "SPP"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/g;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 145
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/device/c/d;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 142
    :cond_9
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "SPPClient updated. The registraion for spp is ignored."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 151
    :cond_a
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 153
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "ACTION_PACKAGE_REPLACED_START"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 155
    :goto_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    new-instance v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$a;

    invoke-direct {v0}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$a;-><init>()V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 159
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "start checkReactivationLockOn"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    sput-boolean v6, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b:Z

    .line 161
    invoke-static {}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a()Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->getVersion()Ljava/lang/String;

    move-result-object v0

    .line 162
    const-string v1, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getVersion for supportReactivationLock : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    sput-boolean v5, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b:Z

    .line 164
    const-string v1, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v2, "end checkReactivationLockOn"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-static {v0}, Lcom/sec/pcw/device/util/h;->a(Ljava/lang/String;)Z

    move-result v0

    .line 166
    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;Z)V

    .line 168
    invoke-static {p1}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 154
    :cond_b
    const/4 v0, 0x0

    goto :goto_2

    .line 171
    :cond_c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.pcw.device.CHECK_REACTIVATION_PKG_REPLACED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 172
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "ACTION_CHECK_REACTIVATION_PKG_REPLACED"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {p1}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 176
    :cond_d
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 179
    const-string v0, "noConnectivity"

    .line 178
    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 181
    sget-object v1, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->c:Landroid/os/Handler;

    if-eqz v1, :cond_f

    .line 182
    if-eqz v0, :cond_e

    .line 183
    const-string v1, "PCWCLIENTTRACE_SYSTEMReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "noConnectivity : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-static {p1}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 187
    :cond_e
    sget-object v0, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->c:Landroid/os/Handler;

    .line 188
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 187
    invoke-virtual {v0, v6, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 192
    sget-object v1, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->c:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 193
    sget-object v1, Lcom/sec/pcw/device/receiver/SYSTEMReceiver;->c:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 196
    :cond_f
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "[error] mConnectivityHandler is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    :cond_10
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 203
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "LOGIN_ACCOUNTS_CHANGED_ACTION"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-static {p1}, Lcom/sec/pcw/device/c/d;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 205
    :cond_11
    const-string v0, "android.settings.reactivationlock_on"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 206
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "save secure data"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$2;-><init>(Lcom/sec/pcw/device/receiver/SYSTEMReceiver;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 224
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 226
    :cond_12
    const-string v0, "android.settings.remotecontrol_on"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 227
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "save secure data"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/pcw/device/receiver/SYSTEMReceiver$3;-><init>(Lcom/sec/pcw/device/receiver/SYSTEMReceiver;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 243
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 245
    :cond_13
    const-string v0, "android.settings.reactivationlock_off"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "android.settings.remotecontrol_off"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    :cond_14
    const-string v0, "PCWCLIENTTRACE_SYSTEMReceiver"

    const-string v1, "action ignored!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
