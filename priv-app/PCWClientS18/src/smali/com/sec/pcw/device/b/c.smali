.class public final Lcom/sec/pcw/device/b/c;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DMDBOpenHelper.java"


# static fields
.field static a:Ljava/lang/String;

.field static b:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

.field private static f:Lcom/sec/pcw/device/b/c;


# instance fields
.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    const-string v0, "value.db"

    sput-object v0, Lcom/sec/pcw/device/b/c;->a:Ljava/lang/String;

    .line 13
    sput-object v1, Lcom/sec/pcw/device/b/c;->b:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    .line 20
    sput-object v1, Lcom/sec/pcw/device/b/c;->f:Lcom/sec/pcw/device/b/c;

    .line 25
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    sget-object v0, Lcom/sec/pcw/device/b/c;->a:Ljava/lang/String;

    sget-object v1, Lcom/sec/pcw/device/b/c;->b:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 17
    iput-object v3, p0, Lcom/sec/pcw/device/b/c;->c:Ljava/lang/String;

    .line 18
    iput-object v3, p0, Lcom/sec/pcw/device/b/c;->d:Ljava/lang/String;

    .line 19
    iput-object v3, p0, Lcom/sec/pcw/device/b/c;->e:Ljava/lang/String;

    .line 37
    const-string v0, "PCWCLIENTTRACE_DMDBOpenHelper"

    const-string v1, "new DMDBOpenHelper instance"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/pcw/device/b/c;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/pcw/device/b/c;->f:Lcom/sec/pcw/device/b/c;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/pcw/device/b/c;

    invoke-direct {v0, p0}, Lcom/sec/pcw/device/b/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/pcw/device/b/c;->f:Lcom/sec/pcw/device/b/c;

    .line 31
    :cond_0
    sget-object v0, Lcom/sec/pcw/device/b/c;->f:Lcom/sec/pcw/device/b/c;

    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 42
    const-string v0, "PCWCLIENTTRACE_DMDBOpenHelper"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v0, "CREATE TABLE urlinfo ( _id INTEGER PRIMARY KEY autoincrement, mvalue TEXT , svalue TEXT ,gvalue TEXT, pvalue TEXT ,savetime DATETIME DEFAULT (date(\'now\')))"

    .line 49
    const-string v1, "CREATE TABLE deviceidinfo ( _id INTEGER PRIMARY KEY autoincrement, deviceid TEXT ,reactivation TEXT ,savetime DATETIME DEFAULT (date(\'now\')))"

    .line 56
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    const-string v0, "PCWCLIENTTRACE_DMDBOpenHelper"

    const-string v2, "onCreate CREATE TABLE urlinfo"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    :try_start_1
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 65
    const-string v0, "PCWCLIENTTRACE_DMDBOpenHelper"

    const-string v1, "onCreate CREATE TABLE deviceidinfo"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 70
    :goto_1
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    const-string v2, "PCWCLIENTTRACE_DMDBOpenHelper"

    const-string v3, "onCreate CREATE TABLE urlinfo FAIL"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    const-string v1, "PCWCLIENTTRACE_DMDBOpenHelper"

    const-string v2, "onCreate CREATE TABLE deviceidinfo FAIL"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 83
    const-string v0, "PCWCLIENTTRACE_DMDBOpenHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDowngrade oldVersion="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newVersion="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "DROP TABLE IF EXISTS urlinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 85
    const-string v0, "DROP TABLE IF EXISTS deviceidinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0, p1}, Lcom/sec/pcw/device/b/c;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 87
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 74
    const-string v0, "PCWCLIENTTRACE_DMDBOpenHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onUpgrade oldVersion="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newVersion="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v0, "DROP TABLE IF EXISTS urlinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 76
    const-string v0, "DROP TABLE IF EXISTS deviceidinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0, p1}, Lcom/sec/pcw/device/b/c;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 78
    return-void
.end method
