.class public Lcom/sec/pcw/device/receiver/PCWReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PCWReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/receiver/PCWReceiver$a;
    }
.end annotation


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Ljava/lang/String;

.field private static c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    sput-object v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->a:Landroid/content/Context;

    .line 73
    sput-object v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->b:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->a:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 193
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "called onRegistrationComplete"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-static {p0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 196
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/pcw/device/receiver/PCWReceiver$1;

    invoke-direct {v2, p0}, Lcom/sec/pcw/device/receiver/PCWReceiver$1;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 216
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 218
    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-lt v0, v2, :cond_0

    .line 232
    return-void

    .line 221
    :cond_0
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/util/g;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 223
    if-eqz v3, :cond_1

    .line 224
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 225
    :cond_1
    const-string v3, "PCWCLIENTTRACE_PCWReceiver"

    const-string v4, "Registration id is empty or null"

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 218
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_2
    const/4 v3, 0x1

    aget-object v4, v1, v0

    invoke-static {p0, p1, v3, v4}, Lcom/sec/pcw/device/c/f;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 74
    sget-boolean v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->c:Z

    return v0
.end method

.method static synthetic c()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->c:Z

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 100
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 101
    :cond_0
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "intent or intent.getAction() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_1
    :goto_0
    return-void

    .line 105
    :cond_2
    sput-object p1, Lcom/sec/pcw/device/receiver/PCWReceiver;->a:Landroid/content/Context;

    .line 107
    invoke-static {p1}, Lcom/sec/pcw/device/util/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 108
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "Action ignored because FMM just support in the case of master account"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_3
    invoke-static {p1}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v4

    .line 114
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 116
    invoke-static {p1}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 117
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "Action ignored because this intent is not SA"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_5

    .line 121
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    .line 122
    const-string v1, "ACTION_REGISTRATION_COMPLETED - Bundle object(login_id) is null."

    .line 121
    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "login_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/pcw/device/receiver/PCWReceiver;->b:Ljava/lang/String;

    .line 127
    const-string v1, "PCWCLIENTTRACE_PCWReceiver"

    const-string v2, "ACTION_REGISTRATION_COMPLETED"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v1, "PCWCLIENTTRACE_PCWReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "\tuserId : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    new-instance v1, Lcom/sec/pcw/device/receiver/PCWReceiver$a;

    invoke-direct {v1}, Lcom/sec/pcw/device/receiver/PCWReceiver$a;-><init>()V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 131
    const-string v1, "PCWCLIENTTRACE_PCWReceiver"

    const-string v2, "start checkReactivationLockOn"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/pcw/device/receiver/PCWReceiver;->c:Z

    .line 133
    invoke-static {}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a()Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->getVersion()Ljava/lang/String;

    move-result-object v1

    .line 134
    const-string v2, "PCWCLIENTTRACE_PCWReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getVersion for supportReactivationLock : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    sput-boolean v3, Lcom/sec/pcw/device/receiver/PCWReceiver;->c:Z

    .line 136
    const-string v2, "PCWCLIENTTRACE_PCWReceiver"

    const-string v3, "end checkReactivationLockOn"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-static {v1}, Lcom/sec/pcw/device/util/h;->a(Ljava/lang/String;)Z

    move-result v1

    .line 138
    invoke-static {p1, v1}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;Z)V

    .line 140
    invoke-static {p1, v0}, Lcom/sec/pcw/device/receiver/PCWReceiver;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.CHECK_REACTIVATION_SA_LOGIN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 143
    const-string v0, "login_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string v1, "PCWCLIENTTRACE_PCWReceiver"

    const-string v2, "ACTION_CHECK_REACTIVATION_SA_LOGIN"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-static {p1, v0}, Lcom/sec/pcw/device/receiver/PCWReceiver;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 146
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.REGISTRATION_CANCELED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "ACTION_REGISTRATION_CANCELED"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {p1}, Lcom/sec/pcw/device/util/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 149
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "Action ignored because this intent is not SA"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 152
    :cond_8
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_9

    .line 153
    const-string v0, "PCWCLIENTTRACE_PCWReceiver"

    const-string v1, "\tBundle object(login_id) is null."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :goto_1
    invoke-static {p1}, Lcom/sec/pcw/device/util/g;->o(Landroid/content/Context;)Z

    .line 166
    invoke-static {}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->a()Lcom/sec/pcw/device/util/SecurePreferencesJNI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/pcw/device/util/SecurePreferencesJNI;->c()Z

    .line 168
    :try_start_0
    new-instance v0, Lcom/sec/pcw/device/b/b;

    invoke-direct {v0, p1}, Lcom/sec/pcw/device/b/b;-><init>(Landroid/content/Context;)V

    .line 169
    invoke-virtual {v0}, Lcom/sec/pcw/device/b/b;->b()V

    .line 170
    invoke-virtual {v0}, Lcom/sec/pcw/device/b/b;->c()V

    .line 171
    const-string v0, "com.sec.pcw.device"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "delivery_urlkey"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 172
    const-string v0, "com.sec.pcw.device"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "intent_db_exist"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_2
    array-length v5, v4

    move v0, v3

    :goto_3
    if-lt v0, v5, :cond_a

    .line 186
    invoke-static {p1, v3}, Lcom/sec/pcw/device/util/g;->a(Landroid/content/Context;Z)V

    .line 187
    const-string v0, "com.sec.pcw.device"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "samsung_account"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 188
    new-instance v0, Lcom/sec/pcw/device/osp/a;

    invoke-direct {v0, p1}, Lcom/sec/pcw/device/osp/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/a;->d()V

    goto/16 :goto_0

    .line 155
    :cond_9
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "login_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    const-string v1, "PCWCLIENTTRACE_PCWReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "\tuserId : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v1, "PCWCLIENTTRACE_RegistrationHandler"

    const-string v2, "[sendUnregistrationDSClient]"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PCWCLIENTTRACE_RegistrationHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "sendUnregistrationDSClient: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SETTING_DS_USERID_SIGNOUT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "user_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v1}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Intent;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    const-string v1, "PCWCLIENTTRACE_PCWReceiver"

    const-string v2, "DB delete fail or Preference clear fail"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 179
    :cond_a
    aget-object v1, v4, v0

    invoke-static {p1, v1}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;Ljava/lang/String;)Z

    .line 180
    aget-object v1, v4, v0

    invoke-static {p1, v1}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 181
    aget-object v1, v4, v0

    invoke-static {p1, v1}, Lcom/sec/pcw/device/util/g;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 183
    aget-object v1, v4, v0

    .line 182
    const-string v2, "PCWCLIENTTRACE_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[deregisterPushRegistrationID] - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string v2, "SPP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x0

    :try_start_1
    new-instance v1, Lcom/sec/pcw/device/util/i;

    invoke-direct {v1, p1}, Lcom/sec/pcw/device/util/i;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v2, "fb0bdc9021c264df"

    invoke-virtual {v1, v2}, Lcom/sec/pcw/device/util/i;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v1}, Lcom/sec/pcw/device/util/i;->close()V

    .line 178
    :cond_b
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    .line 182
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_5
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/sec/pcw/device/util/i;->close()V

    :cond_c
    throw v0

    :cond_d
    const-string v2, "GCM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v1, "PCWCLIENTTRACE_GCMHandler"

    const-string v2, "[decertifyGCM]"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "app"

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    invoke-static {p1, v3, v6, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_4

    :cond_e
    const-string v2, "C2DM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "PCWCLIENTTRACE_C2DMHandler"

    const-string v2, "[decertifyC2DM]"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "app"

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    invoke-static {p1, v3, v6, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_5
.end method
