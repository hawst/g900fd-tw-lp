.class final Lcom/sec/pcw/device/osp/b$a$a;
.super Lcom/a/a/a/a$a;
.source "AccountAppFacadeServiceIF.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/osp/b$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/osp/b$a;


# direct methods
.method constructor <init>(Lcom/sec/pcw/device/osp/b$a;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-direct {p0}, Lcom/a/a/a/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IZLandroid/os/Bundle;)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 260
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ISACallback :: onReceiveAccessToken :: isSuccess : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v1

    new-instance v2, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v2}, Lcom/sec/pcw/device/osp/d;-><init>()V

    iput-object v2, v1, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    .line 264
    if-eqz p2, :cond_3

    .line 266
    :try_start_0
    const-string v1, "access_token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 267
    const-string v2, "user_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 268
    const-string v3, "birthday"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 269
    iget-object v4, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v4}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/pcw/device/osp/b;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/pcw/device/util/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 270
    const-string v5, "mcc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 271
    const-string v6, "server_url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 272
    const-string v7, "api_server_url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 273
    const-string v8, "auth_server_url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 274
    const-string v9, "cc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 275
    const-string v10, "refresh_token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 276
    const-string v11, "device_physical_address_text"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 278
    const-string v12, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "ISACallback :: onReceiveAccessToken :: accessToken : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v12, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "ISACallback :: onReceiveAccessToken :: userId : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string v12, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "ISACallback :: onReceiveAccessToken :: birthday : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ISACallback :: onReceiveAccessToken :: emailId : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v12}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ISACallback :: onReceiveAccessToken :: mcc : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v12, "ISACallback :: onReceiveAccessToken :: serverUrl : "

    invoke-direct {v5, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ISACallback :: onReceiveAccessToken :: api_server_url : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ISACallback :: onReceiveAccessToken :: auth_server_url : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ISACallback :: onReceiveAccessToken :: cc : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ISACallback :: onReceiveAccessToken :: refreshToken : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ISACallback :: onReceiveAccessToken :: devicePhysicalAddressText : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget-object v3, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v3}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v3, v2}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 291
    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v2}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v2, v4}, Lcom/sec/pcw/device/osp/d;->d(Ljava/lang/String;)V

    .line 292
    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v2}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v2, v11}, Lcom/sec/pcw/device/osp/d;->f(Ljava/lang/String;)V

    .line 297
    const-string v2, "CHN"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CHU"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CHM"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CTC"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CHZ"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 298
    :cond_0
    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v2}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    const-string v3, "chn.ospserver.net"

    invoke-virtual {v2, v3}, Lcom/sec/pcw/device/osp/d;->e(Ljava/lang/String;)V

    .line 303
    :goto_0
    const-string v2, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "token : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", auth info : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v3}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v3}, Lcom/sec/pcw/device/osp/d;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v2, "Success response to auth info update, from samsung account app."

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/pcw/device/osp/b;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :goto_1
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b;->a(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 324
    :try_start_1
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b;->a(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 325
    :try_start_2
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b;->a(Lcom/sec/pcw/device/osp/b;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 324
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 327
    :try_start_3
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v2, "onReceiveAccessToken :: tokenRequestLock.notify() is successful"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 332
    :cond_1
    :goto_2
    return-void

    .line 300
    :cond_2
    :try_start_4
    iget-object v2, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v2}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/pcw/device/osp/b;->f:Lcom/sec/pcw/device/osp/d;

    const-string v3, "www.ospserver.net"

    invoke-virtual {v2, v3}, Lcom/sec/pcw/device/osp/d;->e(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 318
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 309
    :cond_3
    :try_start_5
    const-string v1, "error_code"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 310
    const-string v2, "error_message"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 312
    const-string v3, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ISACallback :: onReceiveAccessToken :: errorCode : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-string v1, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ISACallback :: onReceiveAccessToken :: errorMessage : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v1, p0, Lcom/sec/pcw/device/osp/b$a$a;->a:Lcom/sec/pcw/device/osp/b$a;

    invoke-static {v1}, Lcom/sec/pcw/device/osp/b$a;->a(Lcom/sec/pcw/device/osp/b$a;)Lcom/sec/pcw/device/osp/b;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/pcw/device/osp/b;->d:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 324
    :catchall_0
    move-exception v1

    :try_start_6
    monitor-exit v2

    throw v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public final b(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 336
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "ISACallback :: onReceiveChecklistValidation ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    return-void
.end method

.method public final c(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 341
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "ISACallback :: onReceiveDisclaimerAgreement ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method public final d(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 346
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "ISACallback :: onReceiveAuthCode ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method public final e(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 351
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "ISACallback :: onReceiveSCloudAccessToken ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method public final f(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 356
    const-string v0, "PCWCLIENTTRACE_AccountAppFacadeServiceIF"

    const-string v1, "ISACallback :: onReceivePasswordConfirmation ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    return-void
.end method
