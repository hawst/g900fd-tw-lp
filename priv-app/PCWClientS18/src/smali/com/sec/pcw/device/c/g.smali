.class public final Lcom/sec/pcw/device/c/g;
.super Ljava/lang/Object;
.source "SPPHandler.java"


# static fields
.field private static synthetic b:[I


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 24
    sget-object v0, Lcom/sec/pcw/device/c/g;->b:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/pcw/device/a/a$a;->values()[Lcom/sec/pcw/device/a/a$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->h:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1d

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->n:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1c

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->r:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1b

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->z:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1a

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->o:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_19

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->e:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_18

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->g:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_17

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->l:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_16

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->m:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_15

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->u:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_14

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->t:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_13

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->p:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_12

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->v:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_11

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->s:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_10

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->c:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_f

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->d:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_e

    :goto_10
    :try_start_10
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->f:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_d

    :goto_11
    :try_start_11
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->q:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_c

    :goto_12
    :try_start_12
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->y:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_b

    :goto_13
    :try_start_13
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->x:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_a

    :goto_14
    :try_start_14
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->w:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_9

    :goto_15
    :try_start_15
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->i:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_8

    :goto_16
    :try_start_16
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->b:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_7

    :goto_17
    :try_start_17
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->a:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_6

    :goto_18
    :try_start_18
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->k:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_5

    :goto_19
    :try_start_19
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->j:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_4

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->D:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_3

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->B:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_2

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->A:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_1

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/sec/pcw/device/a/a$a;->C:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_0

    :goto_1e
    sput-object v0, Lcom/sec/pcw/device/c/g;->b:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_1e

    :catch_1
    move-exception v1

    goto :goto_1d

    :catch_2
    move-exception v1

    goto :goto_1c

    :catch_3
    move-exception v1

    goto :goto_1b

    :catch_4
    move-exception v1

    goto :goto_1a

    :catch_5
    move-exception v1

    goto :goto_19

    :catch_6
    move-exception v1

    goto :goto_18

    :catch_7
    move-exception v1

    goto :goto_17

    :catch_8
    move-exception v1

    goto :goto_16

    :catch_9
    move-exception v1

    goto :goto_15

    :catch_a
    move-exception v1

    goto :goto_14

    :catch_b
    move-exception v1

    goto/16 :goto_13

    :catch_c
    move-exception v1

    goto/16 :goto_12

    :catch_d
    move-exception v1

    goto/16 :goto_11

    :catch_e
    move-exception v1

    goto/16 :goto_10

    :catch_f
    move-exception v1

    goto/16 :goto_f

    :catch_10
    move-exception v1

    goto/16 :goto_e

    :catch_11
    move-exception v1

    goto/16 :goto_d

    :catch_12
    move-exception v1

    goto/16 :goto_c

    :catch_13
    move-exception v1

    goto/16 :goto_b

    :catch_14
    move-exception v1

    goto/16 :goto_a

    :catch_15
    move-exception v1

    goto/16 :goto_9

    :catch_16
    move-exception v1

    goto/16 :goto_8

    :catch_17
    move-exception v1

    goto/16 :goto_7

    :catch_18
    move-exception v1

    goto/16 :goto_6

    :catch_19
    move-exception v1

    goto/16 :goto_5

    :catch_1a
    move-exception v1

    goto/16 :goto_4

    :catch_1b
    move-exception v1

    goto/16 :goto_3

    :catch_1c
    move-exception v1

    goto/16 :goto_2

    :catch_1d
    move-exception v1

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 49
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "[handleRegistration]"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v0, "com.sec.spp.Status"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 66
    const-string v1, "Error"

    sget-object v2, Lcom/sec/pcw/device/a/a$a;->a:Lcom/sec/pcw/device/a/a$a;

    invoke-virtual {v2}, Lcom/sec/pcw/device/a/a$a;->a()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 68
    const-string v2, "appId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 69
    const-string v3, "RegistrationID"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 71
    const-string v4, "PCWCLIENTTRACE_SPPHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "status = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v4, "PCWCLIENTTRACE_SPPHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "errorCode = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Lcom/sec/pcw/device/a/a$a;->a(I)Lcom/sec/pcw/device/a/a$a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/pcw/device/a/a$a;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v4, "PCWCLIENTTRACE_SPPHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "appId = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v2, "PCWCLIENTTRACE_SPPHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "regId = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    packed-switch v0, :pswitch_data_0

    .line 148
    :goto_0
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "ignored!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :goto_1
    return-void

    .line 80
    :pswitch_0
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "Config.PUSH_REGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    if-eqz v3, :cond_0

    const-string v0, ""

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "Registration Completed."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;)Z

    .line 89
    iget-object v0, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    const-string v1, "SPP"

    invoke-static {v0, v3, v1}, Lcom/sec/pcw/device/c/e;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 93
    :cond_0
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "Registration ID is null or empty!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 102
    :pswitch_1
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v2, "Config.PUSH_REGISTRATION_FAIL"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-static {}, Lcom/sec/pcw/device/c/g;->a()[I

    move-result-object v0

    invoke-static {v1}, Lcom/sec/pcw/device/a/a$a;->a(I)Lcom/sec/pcw/device/a/a$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pcw/device/a/a$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 118
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.pcw.device.SPP_REGISTRATION_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    const-string v2, "com.sec.pcw.device"

    invoke-virtual {v1, v2, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "spp_backoff"

    const/16 v3, 0x3e8

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 120
    iget-object v2, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    const-string v2, "com.sec.pcw.device"

    invoke-virtual {v1, v2, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spp_backoff"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 113
    :sswitch_0
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "ignored!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 131
    :pswitch_2
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "Config.PUSH_DEREGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;)Z

    goto/16 :goto_1

    .line 142
    :pswitch_3
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "Config.PUSH_DEREGISTRATION_FAIL"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 104
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x1c -> :sswitch_0
        0x1d -> :sswitch_0
    .end sparse-switch
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 162
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "[handlePushMsg]"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "appId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    const-string v1, "notificationId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 166
    const-string v2, "msg"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 167
    const-string v3, "ack"

    invoke-virtual {p1, v3, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 168
    const-string v4, "sender"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 169
    const-string v5, "appData"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 170
    const-string v6, "timeStamp"

    const-wide/16 v7, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 171
    const-string v7, "sessionInfo"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 172
    const-string v8, "connectionTerm"

    invoke-virtual {p1, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 174
    const-string v9, "PCWCLIENTTRACE_SPPHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "appId = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "notiId = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "msg = "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "ack = "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "sender = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "appData = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "timestamp = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "sessionInfo = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "connectionTerm = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/c/g;->a:Landroid/content/Context;

    const-string v1, "SPP"

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    return-void

    .line 190
    :catch_0
    move-exception v0

    .line 192
    const-string v1, "PCWCLIENTTRACE_SPPHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception is occured in PushMsgHandler.handleMsg : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_0
    const-string v0, "PCWCLIENTTRACE_SPPHandler"

    const-string v1, "message is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
