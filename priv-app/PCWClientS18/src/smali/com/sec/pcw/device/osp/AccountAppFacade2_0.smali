.class public Lcom/sec/pcw/device/osp/AccountAppFacade2_0;
.super Lcom/sec/pcw/device/osp/AccountAppFacade;
.source "AccountAppFacade2_0.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;
    }
.end annotation


# instance fields
.field protected e:Landroid/content/BroadcastReceiver;

.field protected f:Lcom/sec/pcw/device/osp/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/pcw/device/osp/AccountAppFacade;-><init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    .line 37
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/pcw/device/osp/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "app info is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "unregister AuthInfo UpdateReceiver v2.0"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    iput-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    .line 97
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 94
    iput-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v0

    .line 94
    iput-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    .line 95
    throw v0
.end method

.method private i()Lcom/sec/pcw/device/osp/d;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 271
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->a:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 273
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->R:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 274
    sget-object v2, Lcom/sec/pcw/device/util/a/a;->Q:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 275
    sget-object v3, Lcom/sec/pcw/device/util/a/a;->r:[B

    invoke-static {v3}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 276
    sget-object v4, Lcom/sec/pcw/device/util/a/a;->S:[B

    invoke-static {v4}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    new-instance v4, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v4}, Lcom/sec/pcw/device/osp/d;-><init>()V

    .line 279
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 281
    invoke-virtual {p0, v1}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/sec/pcw/device/osp/d;->e(Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0, v2}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/sec/pcw/device/osp/d;->d(Ljava/lang/String;)V

    .line 283
    invoke-virtual {p0, v3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 284
    invoke-virtual {p0, v0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/pcw/device/osp/d;->f(Ljava/lang/String;)V

    .line 287
    :cond_0
    return-object v4
.end method


# virtual methods
.method protected final declared-synchronized a()V
    .locals 3

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "register AuthInfo UpdateReceiver v2.0"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;

    invoke-direct {v0, p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;-><init>(Lcom/sec/pcw/device/osp/AccountAppFacade2_0;)V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    .line 79
    new-instance v0, Landroid/content/IntentFilter;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->u:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_0
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(JLjava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 51
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "request AuthInfo Update"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 53
    iput-boolean v4, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->d:Z

    .line 54
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a()V

    .line 55
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->t:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->v:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b:Lcom/sec/pcw/device/osp/c;

    invoke-virtual {v2}, Lcom/sec/pcw/device/osp/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->w:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b:Lcom/sec/pcw/device/osp/c;

    invoke-virtual {v2}, Lcom/sec/pcw/device/osp/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->x:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/pcw/device/util/a/a;->C:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->y:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->z:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/pcw/device/util/a/a;->D:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->A:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/sec/pcw/device/util/a/a;->G:[B

    invoke-static {v3}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    sget-object v4, Lcom/sec/pcw/device/util/a/a;->O:[B

    invoke-static {v4}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/sec/pcw/device/util/a/a;->I:[B

    invoke-static {v4}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/sec/pcw/device/util/a/a;->K:[B

    invoke-static {v4}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p3, :cond_0

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->B:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 58
    :cond_1
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wait update auth info.. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :try_start_1
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V

    .line 61
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->h()V

    .line 71
    iget-boolean v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->d:Z

    return v0

    .line 61
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Lcom/sec/pcw/device/osp/d;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->c()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/pcw/device/osp/d;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->i()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v2

    :goto_0
    const/4 v4, 0x3

    if-lt v1, v4, :cond_1

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v0}, Lcom/sec/pcw/device/osp/d;-><init>()V

    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e()V

    .line 149
    :cond_0
    return-object v0

    .line 147
    :cond_1
    const-string v4, "PCWCLIENTTRACE_AccountAppFacade2_0"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[getUpdatedAuthInfo] count : ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/32 v4, 0xea60

    invoke-virtual {p0, v4, v5, v3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(JLjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Lcom/sec/pcw/device/osp/d;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "auth info is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    sget-object v3, Lcom/sec/pcw/device/util/a/a;->a:[B

    invoke-static {v3}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sget-object v2, Lcom/sec/pcw/device/util/a/a;->R:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget-object v2, Lcom/sec/pcw/device/util/a/a;->Q:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget-object v2, Lcom/sec/pcw/device/util/a/a;->r:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget-object v2, Lcom/sec/pcw/device/util/a/a;->S:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public final c()Lcom/sec/pcw/device/osp/d;
    .locals 3

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->i()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/pcw/device/util/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 164
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 165
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/pcw/device/osp/d;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 169
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "this is invalid auth info, but try restoring from secure data"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->d()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    .line 181
    :cond_2
    return-object v0
.end method

.method public final d()Lcom/sec/pcw/device/osp/d;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 190
    new-instance v0, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v0}, Lcom/sec/pcw/device/osp/d;-><init>()V

    .line 191
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    const-string v2, "com.sec.pcw.device"

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "setemp"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    invoke-static {}, Lcom/sec/pcw/device/util/g;->a()Ljava/lang/String;

    move-result-object v1

    .line 195
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    invoke-virtual {v0, v1}, Lcom/sec/pcw/device/osp/d;->g(Ljava/lang/String;)Lcom/sec/pcw/device/osp/d;

    .line 197
    invoke-virtual {p0, v1}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    const-string v3, "com.sec.pcw.device"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "setemp"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/pcw/device/osp/d;->g(Ljava/lang/String;)Lcom/sec/pcw/device/osp/d;

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->a:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 294
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 296
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->R:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 297
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->Q:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 298
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->r:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 299
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->S:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 300
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 301
    return-void
.end method

.method protected final f()Lcom/sec/pcw/device/osp/d;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    return-object v0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->h()V

    .line 312
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    .line 397
    invoke-virtual {v0}, Lcom/sec/pcw/device/osp/d;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
