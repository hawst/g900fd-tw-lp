.class public final Lcom/sec/pcw/device/c/d;
.super Ljava/lang/Object;
.source "PCWHandler.java"


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 38
    const-string v2, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "[initGoogleAccountAndRequestC2DM]"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    .line 44
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "PCWCLIENTTRACE_PCWHandler"

    const-string v2, "First Google Account Registration"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v1, "PCWCLIENTTRACE_PCWHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Google Account : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-static {p0, v0}, Lcom/sec/pcw/device/util/g;->f(Landroid/content/Context;Ljava/lang/String;)Z

    .line 58
    const-string v0, "android.intent.action.GOOGLEACCOUNT_REGISTED"

    invoke-static {p0, v0}, Lcom/sec/pcw/device/c/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 96
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v0

    .line 40
    goto :goto_0

    .line 65
    :cond_2
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 66
    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 67
    const-string v3, "PCWCLIENTTRACE_PCWHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Google Account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-static {p0, v2}, Lcom/sec/pcw/device/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 71
    const-string v3, "PCWCLIENTTRACE_PCWHandler"

    const-string v4, "Google Account Deleted."

    invoke-static {v3, v4}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "PCWCLIENTTRACE_PCWHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Google Account : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->n(Landroid/content/Context;)Z

    .line 84
    :cond_3
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_2
    if-eqz v0, :cond_0

    .line 85
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v1, "Google Account & C2DM Unregistration"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-static {p0}, Lcom/sec/pcw/device/util/g;->n(Landroid/content/Context;)Z

    .line 89
    const-string v0, "android.intent.action.GOOGLEACCOUNT_REMOVED"

    invoke-static {p0, v0}, Lcom/sec/pcw/device/c/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/sec/pcw/device/util/h;->a()Z

    goto :goto_1

    :cond_4
    move v0, v1

    .line 84
    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 105
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[sendBroadcastForSetting] - action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 135
    if-nez p0, :cond_1

    .line 137
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v1, "[ensureRegistration] - Context is invalid"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    invoke-static {p0}, Lcom/sec/pcw/device/util/d;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 145
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v1, "[ensureRegistration] - netwrok is not available. action ignored"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_2
    invoke-static {p0}, Lcom/sec/pcw/device/util/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {p0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 155
    const-string v2, "PCWCLIENTTRACE_PCWHandler"

    const-string v3, "[ensureRegistration] - Samsung account exist"

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/pcw/device/c/d$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/pcw/device/c/d$1;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 256
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 262
    :cond_3
    const-string v0, "PCWCLIENTTRACE_PCWHandler"

    const-string v2, "[ensureRegistration] - No Samsung account"

    invoke-static {v0, v2}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const/4 v0, 0x0

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_0

    .line 266
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/util/g;->h(Landroid/content/Context;Ljava/lang/String;)Z

    .line 267
    aget-object v3, v1, v0

    invoke-static {p0, v3}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
