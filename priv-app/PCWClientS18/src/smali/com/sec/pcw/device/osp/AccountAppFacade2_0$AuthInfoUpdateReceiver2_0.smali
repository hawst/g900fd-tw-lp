.class public Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;
.super Landroid/content/BroadcastReceiver;
.source "AccountAppFacade2_0.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/osp/AccountAppFacade2_0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AuthInfoUpdateReceiver2_0"
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;


# direct methods
.method protected constructor <init>(Lcom/sec/pcw/device/osp/AccountAppFacade2_0;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 331
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "onReceive() "

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 334
    :cond_0
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "intent or intent.getAction() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_1
    :goto_0
    return-void

    .line 338
    :cond_2
    sget-object v0, Lcom/sec/pcw/device/util/a/a;->v:[B

    invoke-static {v0}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v1, v1, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->b:Lcom/sec/pcw/device/osp/c;

    invoke-virtual {v1}, Lcom/sec/pcw/device/osp/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 342
    sget-object v0, Lcom/sec/pcw/device/util/a/a;->E:[B

    invoke-static {v0}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v0

    const/16 v1, -0x3e7

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 343
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    new-instance v2, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v2}, Lcom/sec/pcw/device/osp/d;-><init>()V

    iput-object v2, v1, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    .line 345
    packed-switch v0, :pswitch_data_0

    .line 372
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->P:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 373
    const-string v2, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v3, "Fail response to auth info update, from samsung account app."

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v2, "PCWCLIENTTRACE_AccountAppFacade2_0"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[ code : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", message : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->d:Z

    .line 379
    :goto_1
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 382
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :try_start_1
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v0, v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 382
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 348
    :pswitch_0
    sget-object v0, Lcom/sec/pcw/device/util/a/a;->F:[B

    invoke-static {v0}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->G:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    sget-object v2, Lcom/sec/pcw/device/util/a/a;->I:[B

    invoke-static {v2}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 351
    sget-object v3, Lcom/sec/pcw/device/util/a/a;->O:[B

    invoke-static {v3}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 352
    sget-object v4, Lcom/sec/pcw/device/util/a/a;->K:[B

    invoke-static {v4}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 358
    iget-object v5, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v5, v5, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v5, v4}, Lcom/sec/pcw/device/osp/d;->e(Ljava/lang/String;)V

    .line 359
    iget-object v4, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v4, v4, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v4, v1}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 360
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v1, v1, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v1, v2}, Lcom/sec/pcw/device/osp/d;->d(Ljava/lang/String;)V

    .line 361
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v1, v1, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v1, v3}, Lcom/sec/pcw/device/osp/d;->f(Ljava/lang/String;)V

    .line 363
    const-string v1, "PCWCLIENTTRACE_AccountAppFacade2_0"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "token : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", auth info : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    iget-object v2, v2, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->f:Lcom/sec/pcw/device/osp/d;

    invoke-virtual {v2}, Lcom/sec/pcw/device/osp/d;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade2_0"

    const-string v1, "Success response to auth info update, from samsung account app."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0$AuthInfoUpdateReceiver2_0;->a:Lcom/sec/pcw/device/osp/AccountAppFacade2_0;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/pcw/device/osp/AccountAppFacade2_0;->d:Z

    goto/16 :goto_1

    .line 345
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
