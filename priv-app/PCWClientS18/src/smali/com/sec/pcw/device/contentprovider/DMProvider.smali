.class public Lcom/sec/pcw/device/contentprovider/DMProvider;
.super Landroid/content/ContentProvider;
.source "DMProvider.java"


# static fields
.field static final a:Landroid/content/UriMatcher;

.field static final b:Landroid/net/Uri;

.field static final c:Landroid/net/Uri;


# instance fields
.field d:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 18
    const-string v0, "content://com.sec.pcw.device/value"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->b:Landroid/net/Uri;

    .line 19
    const-string v0, "content://com.sec.pcw.device/deviceid"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->c:Landroid/net/Uri;

    .line 23
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 24
    sput-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.pcw.device"

    const-string v2, "value"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 25
    sget-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.pcw.device"

    const-string v2, "deviceid"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 88
    sget-object v1, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 96
    :goto_0
    invoke-virtual {p0}, Lcom/sec/pcw/device/contentprovider/DMProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 97
    return v0

    .line 90
    :pswitch_0
    iget-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "urlinfo"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 93
    :pswitch_1
    iget-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "deviceidinfo"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 60
    .line 61
    sget-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v1

    .line 82
    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    iget-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "urlinfo"

    invoke-virtual {v0, v2, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 65
    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->b:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lcom/sec/pcw/device/contentprovider/DMProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 73
    :pswitch_1
    iget-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "deviceidinfo"

    invoke-virtual {v0, v2, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 74
    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 76
    sget-object v0, Lcom/sec/pcw/device/contentprovider/DMProvider;->c:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/sec/pcw/device/contentprovider/DMProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/pcw/device/contentprovider/DMProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/device/b/c;->a(Landroid/content/Context;)Lcom/sec/pcw/device/b/c;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/sec/pcw/device/b/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    .line 33
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 43
    const-string v1, "PCWCLIENTTRACE_DMContentProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[URIMatcher] - result : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    sget-object v1, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT * from urlinfo"

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    .line 48
    :cond_0
    sget-object v1, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 50
    iget-object v1, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT * from deviceidinfo"

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_1
    const-string v1, "PCWCLIENTTRACE_DMContentProvider"

    const-string v2, "URI info fail"

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 101
    const/4 v0, 0x0

    .line 103
    sget-object v1, Lcom/sec/pcw/device/contentprovider/DMProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 111
    :goto_0
    invoke-virtual {p0}, Lcom/sec/pcw/device/contentprovider/DMProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 112
    return v0

    .line 105
    :pswitch_0
    iget-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "urlinfo"

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 108
    :pswitch_1
    iget-object v0, p0, Lcom/sec/pcw/device/contentprovider/DMProvider;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "deviceidinfo"

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
