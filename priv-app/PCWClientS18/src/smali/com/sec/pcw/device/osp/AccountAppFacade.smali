.class public Lcom/sec/pcw/device/osp/AccountAppFacade;
.super Ljava/lang/Object;
.source "AccountAppFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;
    }
.end annotation


# static fields
.field private static final e:Landroid/net/Uri;

.field private static final f:Landroid/net/Uri;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/sec/pcw/device/osp/c;

.field protected c:Landroid/content/BroadcastReceiver;

.field protected d:Z

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/pcw/device/util/a/a;->m:[B

    invoke-static {v0}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 30
    sput-object v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->e:Landroid/net/Uri;

    .line 35
    sget-object v0, Lcom/sec/pcw/device/util/a/a;->n:[B

    invoke-static {v0}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 34
    sput-object v0, Lcom/sec/pcw/device/osp/AccountAppFacade;->f:Landroid/net/Uri;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/pcw/device/osp/c;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->g:Ljava/lang/String;

    .line 51
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/pcw/device/osp/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "app info is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    iput-object p1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->b:Lcom/sec/pcw/device/osp/c;

    .line 58
    :try_start_0
    invoke-static {p1}, Lcom/sec/pcw/device/util/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v1, "Failed to retrive content key"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 281
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 285
    new-array v2, v4, [Ljava/lang/String;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->k:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v5

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/sec/pcw/device/util/a/a;->l:[B

    invoke-static {v3}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " = ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 287
    new-array v4, v4, [Ljava/lang/String;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->j:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 290
    :try_start_0
    sget-object v1, Lcom/sec/pcw/device/osp/AccountAppFacade;->f:Landroid/net/Uri;

    .line 291
    const/4 v5, 0x0

    .line 290
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 292
    if-eqz v2, :cond_3

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 294
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 295
    invoke-virtual {p0, v1}, Lcom/sec/pcw/device/osp/AccountAppFacade;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    move-object v0, v1

    .line 301
    :goto_0
    if-eqz v2, :cond_0

    .line 302
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_0
    :goto_1
    return-object v0

    .line 298
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    .line 299
    :goto_2
    :try_start_3
    const-string v2, "PCWCLIENTTRACE_AccountAppFacade"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get samsung account user id. - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 301
    if-eqz v6, :cond_0

    .line 302
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 300
    :catchall_0
    move-exception v0

    .line 301
    :goto_3
    if-eqz v6, :cond_1

    .line 302
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 304
    :cond_1
    throw v0

    .line 300
    :catchall_1
    move-exception v0

    move-object v6, v2

    goto :goto_3

    .line 298
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    move-object v6, v2

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v6, v2

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 353
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->g:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/pcw/device/util/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 361
    :goto_0
    return-object p1

    .line 357
    :catch_0
    move-exception v0

    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v1, "Failed to encryption. return plain text."

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected declared-synchronized a()V
    .locals 3

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v1, "register AuthInfoUpdateReceiver"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;

    invoke-direct {v0, p0}, Lcom/sec/pcw/device/osp/AccountAppFacade$AuthInfoUpdateReceiver;-><init>(Lcom/sec/pcw/device/osp/AccountAppFacade;)V

    iput-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    .line 101
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 102
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->c:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 103
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->d:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Lcom/sec/pcw/device/osp/d;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->c()Lcom/sec/pcw/device/osp/d;

    move-result-object v0

    .line 150
    return-object v0
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 373
    const/4 v0, 0x0

    .line 375
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->g:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/sec/pcw/device/util/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 381
    :goto_0
    return-object v0

    .line 377
    :catch_0
    move-exception v1

    const-string v1, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v2, "Failed to token info decryption. return plain text."

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()Lcom/sec/pcw/device/osp/d;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 161
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->a:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->r:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v1, v2}, Lcom/sec/pcw/device/osp/d;-><init>(Z)V

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 164
    :cond_0
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->h()Ljava/lang/String;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/sec/pcw/device/osp/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 166
    :cond_1
    invoke-virtual {v1, v3}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/device/util/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/pcw/device/osp/d;->f(Ljava/lang/String;)V

    .line 171
    return-object v1
.end method

.method public d()Lcom/sec/pcw/device/osp/d;
    .locals 1

    .prologue
    .line 175
    new-instance v0, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v0}, Lcom/sec/pcw/device/osp/d;-><init>()V

    .line 176
    return-object v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->a:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 267
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 270
    sget-object v1, Lcom/sec/pcw/device/util/a/a;->r:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 272
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 273
    return-void
.end method

.method protected f()Lcom/sec/pcw/device/osp/d;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 314
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 315
    new-instance v7, Lcom/sec/pcw/device/osp/d;

    invoke-direct {v7, v5}, Lcom/sec/pcw/device/osp/d;-><init>(Z)V

    .line 318
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->h:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v5

    sget-object v1, Lcom/sec/pcw/device/util/a/a;->i:[B

    invoke-static {v1}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/sec/pcw/device/util/a/a;->g:[B

    invoke-static {v3}, Lcom/sec/pcw/device/util/a/a;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " = ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 320
    new-array v4, v4, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->b:Lcom/sec/pcw/device/osp/c;

    invoke-virtual {v1}, Lcom/sec/pcw/device/osp/c;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 322
    :try_start_0
    sget-object v1, Lcom/sec/pcw/device/osp/AccountAppFacade;->e:Landroid/net/Uri;

    .line 323
    const/4 v5, 0x0

    .line 322
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 324
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/pcw/device/osp/d;->b(Ljava/lang/String;)V

    .line 326
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 328
    invoke-virtual {p0, v0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/pcw/device/osp/d;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 334
    :cond_0
    if-eqz v1, :cond_1

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 339
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/pcw/device/osp/d;->a(Ljava/lang/String;)V

    .line 340
    invoke-virtual {p0}, Lcom/sec/pcw/device/osp/AccountAppFacade;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/pcw/device/osp/d;->e(Ljava/lang/String;)V

    .line 342
    return-object v7

    .line 331
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 332
    :goto_1
    :try_start_2
    const-string v2, "PCWCLIENTTRACE_AccountAppFacade"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get token and token secret. - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 334
    if-eqz v1, :cond_1

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    move-object v1, v6

    .line 334
    :goto_2
    if-eqz v1, :cond_2

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 337
    :cond_2
    throw v0

    .line 333
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 331
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 454
    const-string v0, "PCWCLIENTTRACE_AccountAppFacade"

    const-string v1, "unregister AuthInfo UpdateReceiver"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    .line 455
    :cond_0
    :goto_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 456
    return-void

    :catch_0
    move-exception v0

    .line 454
    iput-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->c:Landroid/content/BroadcastReceiver;

    throw v0
.end method

.method public g()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 508
    .line 512
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Value"

    aput-object v1, v2, v0

    .line 513
    const-string v3, "Key = ?"

    .line 514
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "ServerUrl"

    aput-object v1, v4, v0

    .line 516
    iget-object v0, p0, Lcom/sec/pcw/device/osp/AccountAppFacade;->a:Landroid/content/Context;

    .line 517
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 519
    const-string v1, "content://com.osp.contentprovider.ospcontentprovider/identity"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 520
    const/4 v5, 0x0

    .line 518
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 522
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 523
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 525
    if-eqz v6, :cond_2

    .line 527
    :try_start_2
    invoke-virtual {p0, v6}, Lcom/sec/pcw/device/osp/AccountAppFacade;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    move-object v0, v6

    .line 536
    :goto_0
    if-eqz v1, :cond_0

    .line 537
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 541
    :cond_0
    :goto_1
    return-object v0

    .line 529
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v6

    .line 533
    goto :goto_0

    .line 534
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    move-object v0, v6

    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 536
    if-eqz v2, :cond_0

    .line 537
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 535
    :catchall_0
    move-exception v0

    .line 536
    :goto_3
    if-eqz v6, :cond_1

    .line 537
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 539
    :cond_1
    throw v0

    .line 535
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v6, v2

    goto :goto_3

    .line 534
    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_0

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method
