.class final Lcom/sec/pcw/device/c/e$1;
.super Ljava/lang/Object;
.source "PushMsgHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Landroid/content/Context;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/pcw/device/c/e$1;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/pcw/device/c/e$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 238
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/device/c/e$1;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/pcw/device/c/e$1;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/sec/pcw/device/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    .line 240
    iget-object v2, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/device/util/h;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    .line 239
    invoke-static {v1, v0, v2, v3}, Lcom/sec/pcw/device/c/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 241
    const-string v2, "PCWCLIENTTRACE_PushMsgHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]PUSH_DELIVERY_REPORT - ErrorCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v2, "BIZ-0030"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "BIZ-0031"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 244
    new-instance v1, Lcom/sec/pcw/device/b/a;

    iget-object v2, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/pcw/device/b/a;-><init>(Landroid/content/Context;)V

    .line 245
    invoke-static {v0}, Lcom/sec/pcw/device/b/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    const-string v1, "PCWCLIENTTRACE_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]PUSH_DELIVERY_REPORT [Added to Database]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :goto_0
    iget-object v1, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/pcw/device/util/g;->k(Landroid/content/Context;)I

    move-result v1

    .line 255
    const-string v2, "PCWCLIENTTRACE_PushMsgHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]PUSH_DELIVERY_REPORT - backoffTimeMs: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.pcw.device.HTTP_REQUEST_RETRY"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    const-string v3, "body"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string v0, "uri"

    .line 260
    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/pcw/device/util/h;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 259
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    const-string v0, "pushType"

    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    iget-object v0, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    move-result v0

    .line 266
    iget-object v1, p0, Lcom/sec/pcw/device/c/e$1;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/g;->b(Landroid/content/Context;I)V

    .line 272
    :cond_0
    :goto_1
    return-void

    .line 251
    :cond_1
    const-string v1, "PCWCLIENTTRACE_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]PUSH_DELIVERY_REPORT [Added to Database was failed]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    const-string v1, "PCWCLIENTTRACE_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/device/c/e$1;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/device/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
