.class public final enum Lcom/sec/pcw/device/a/a$a;
.super Ljava/lang/Enum;
.source "Extra.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/device/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/pcw/device/a/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/sec/pcw/device/a/a$a;

.field public static final enum B:Lcom/sec/pcw/device/a/a$a;

.field public static final enum C:Lcom/sec/pcw/device/a/a$a;

.field public static final enum D:Lcom/sec/pcw/device/a/a$a;

.field private static F:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/pcw/device/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic G:[Lcom/sec/pcw/device/a/a$a;

.field public static final enum a:Lcom/sec/pcw/device/a/a$a;

.field public static final enum b:Lcom/sec/pcw/device/a/a$a;

.field public static final enum c:Lcom/sec/pcw/device/a/a$a;

.field public static final enum d:Lcom/sec/pcw/device/a/a$a;

.field public static final enum e:Lcom/sec/pcw/device/a/a$a;

.field public static final enum f:Lcom/sec/pcw/device/a/a$a;

.field public static final enum g:Lcom/sec/pcw/device/a/a$a;

.field public static final enum h:Lcom/sec/pcw/device/a/a$a;

.field public static final enum i:Lcom/sec/pcw/device/a/a$a;

.field public static final enum j:Lcom/sec/pcw/device/a/a$a;

.field public static final enum k:Lcom/sec/pcw/device/a/a$a;

.field public static final enum l:Lcom/sec/pcw/device/a/a$a;

.field public static final enum m:Lcom/sec/pcw/device/a/a$a;

.field public static final enum n:Lcom/sec/pcw/device/a/a$a;

.field public static final enum o:Lcom/sec/pcw/device/a/a$a;

.field public static final enum p:Lcom/sec/pcw/device/a/a$a;

.field public static final enum q:Lcom/sec/pcw/device/a/a$a;

.field public static final enum r:Lcom/sec/pcw/device/a/a$a;

.field public static final enum s:Lcom/sec/pcw/device/a/a$a;

.field public static final enum t:Lcom/sec/pcw/device/a/a$a;

.field public static final enum u:Lcom/sec/pcw/device/a/a$a;

.field public static final enum v:Lcom/sec/pcw/device/a/a$a;

.field public static final enum w:Lcom/sec/pcw/device/a/a$a;

.field public static final enum x:Lcom/sec/pcw/device/a/a$a;

.field public static final enum y:Lcom/sec/pcw/device/a/a$a;

.field public static final enum z:Lcom/sec/pcw/device/a/a$a;


# instance fields
.field private E:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 238
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "UNDEFINED"

    .line 241
    invoke-direct {v1, v2, v0, v0}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->a:Lcom/sec/pcw/device/a/a$a;

    .line 243
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "TIMEOUT"

    .line 246
    const/4 v3, -0x1

    invoke-direct {v1, v2, v5, v3}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->b:Lcom/sec/pcw/device/a/a$a;

    .line 247
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "NETWORK_NOT_AVAILABLE"

    const/4 v3, -0x2

    invoke-direct {v1, v2, v6, v3}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->c:Lcom/sec/pcw/device/a/a$a;

    .line 248
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "PROVISIONING_DATA_EXISTS"

    const/16 v3, -0x64

    invoke-direct {v1, v2, v7, v3}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->d:Lcom/sec/pcw/device/a/a$a;

    .line 249
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INITIALIZATION_ALREADY_COMPLETED"

    const/16 v3, -0x66

    invoke-direct {v1, v2, v8, v3}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->e:Lcom/sec/pcw/device/a/a$a;

    .line 250
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "PROVISIONING_FAIL"

    const/4 v3, 0x5

    const/16 v4, -0x67

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->f:Lcom/sec/pcw/device/a/a$a;

    .line 251
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INITIALIZATION_FAIL"

    const/4 v3, 0x6

    const/16 v4, -0x68

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->g:Lcom/sec/pcw/device/a/a$a;

    .line 252
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "APPLICATION_ALREADY_DEREGISTRATION"

    const/4 v3, 0x7

    const/16 v4, -0x69

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->h:Lcom/sec/pcw/device/a/a$a;

    .line 253
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "SUCCESS"

    const/16 v3, 0x8

    const/16 v4, 0x3e8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->i:Lcom/sec/pcw/device/a/a$a;

    .line 254
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "UNKOWN_MESSAGE_TYPE"

    const/16 v3, 0x9

    const/16 v4, 0x7d0

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->j:Lcom/sec/pcw/device/a/a$a;

    .line 255
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "UNEXPECTED_MESSAGE"

    const/16 v3, 0xa

    const/16 v4, 0x7d1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->k:Lcom/sec/pcw/device/a/a$a;

    .line 256
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INTERNAL_SERVER_ERROR"

    const/16 v3, 0xb

    const/16 v4, 0x7d2

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->l:Lcom/sec/pcw/device/a/a$a;

    .line 257
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INTERRUPTED"

    const/16 v3, 0xc

    const/16 v4, 0x7d3

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->m:Lcom/sec/pcw/device/a/a$a;

    .line 258
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "BAD_REQUEST_FOR_PROVISION"

    const/16 v3, 0xd

    const/16 v4, 0xbb8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->n:Lcom/sec/pcw/device/a/a$a;

    .line 259
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "FAIL_TO_AUTHENTICATE"

    const/16 v3, 0xe

    const/16 v4, 0xbb9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->o:Lcom/sec/pcw/device/a/a$a;

    .line 260
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INVALID_DEVICE_TOKEN_TO_REPROVISION"

    const/16 v3, 0xf

    const/16 v4, 0xbba

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->p:Lcom/sec/pcw/device/a/a$a;

    .line 261
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "PROVISION_EXCEPTION"

    const/16 v3, 0x10

    const/16 v4, 0xbbb

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->q:Lcom/sec/pcw/device/a/a$a;

    .line 262
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "CONNECTION_MAX_EXCEEDED"

    const/16 v3, 0x11

    const/16 v4, 0xfa0

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->r:Lcom/sec/pcw/device/a/a$a;

    .line 263
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INVALID_STATE"

    const/16 v3, 0x12

    const/16 v4, 0xfa1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->s:Lcom/sec/pcw/device/a/a$a;

    .line 264
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INVALID_DEVICE_TOKEN"

    const/16 v3, 0x13

    const/16 v4, 0xfa2

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->t:Lcom/sec/pcw/device/a/a$a;

    .line 265
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INVALID_APP_ID"

    const/16 v3, 0x14

    const/16 v4, 0xfa3

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->u:Lcom/sec/pcw/device/a/a$a;

    .line 266
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "INVALID_REG_ID"

    const/16 v3, 0x15

    const/16 v4, 0xfa4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->v:Lcom/sec/pcw/device/a/a$a;

    .line 267
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "RESET_BY_NEW_INITIALIZATION"

    const/16 v3, 0x16

    const/16 v4, 0xfa5

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->w:Lcom/sec/pcw/device/a/a$a;

    .line 268
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "REPROVISIONING_REQUIRED"

    const/16 v3, 0x17

    const/16 v4, 0xfa6

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->x:Lcom/sec/pcw/device/a/a$a;

    .line 269
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "REGISTRATION_FAILED"

    const/16 v3, 0x18

    const/16 v4, 0xfa7

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->y:Lcom/sec/pcw/device/a/a$a;

    .line 270
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "DEREGISTRATION_FAILED"

    const/16 v3, 0x19

    const/16 v4, 0xfa8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->z:Lcom/sec/pcw/device/a/a$a;

    .line 271
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "WRONG_DEVICE_TOKEN"

    const/16 v3, 0x1a

    const/16 v4, 0xfa9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->A:Lcom/sec/pcw/device/a/a$a;

    .line 272
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "WRONG_APP_ID"

    const/16 v3, 0x1b

    const/16 v4, 0xfaa

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->B:Lcom/sec/pcw/device/a/a$a;

    .line 273
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "WRONG_REG_ID"

    const/16 v3, 0x1c

    const/16 v4, 0xfab

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->C:Lcom/sec/pcw/device/a/a$a;

    .line 274
    new-instance v1, Lcom/sec/pcw/device/a/a$a;

    const-string v2, "UNSUPPORTED_PING_SPECIFICATION"

    const/16 v3, 0x1d

    const/16 v4, 0xfac

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/device/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->D:Lcom/sec/pcw/device/a/a$a;

    const/16 v1, 0x1e

    new-array v1, v1, [Lcom/sec/pcw/device/a/a$a;

    sget-object v2, Lcom/sec/pcw/device/a/a$a;->a:Lcom/sec/pcw/device/a/a$a;

    aput-object v2, v1, v0

    sget-object v2, Lcom/sec/pcw/device/a/a$a;->b:Lcom/sec/pcw/device/a/a$a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/sec/pcw/device/a/a$a;->c:Lcom/sec/pcw/device/a/a$a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/sec/pcw/device/a/a$a;->d:Lcom/sec/pcw/device/a/a$a;

    aput-object v2, v1, v7

    sget-object v2, Lcom/sec/pcw/device/a/a$a;->e:Lcom/sec/pcw/device/a/a$a;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->f:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->g:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->h:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->i:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->j:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->k:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->l:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->m:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->n:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->o:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->p:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->q:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->r:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->s:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->t:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->u:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->v:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->w:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->x:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->y:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->z:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->A:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->B:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->C:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    sget-object v3, Lcom/sec/pcw/device/a/a$a;->D:Lcom/sec/pcw/device/a/a$a;

    aput-object v3, v1, v2

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->G:[Lcom/sec/pcw/device/a/a$a;

    .line 286
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    sput-object v1, Lcom/sec/pcw/device/a/a$a;->F:Ljava/util/TreeMap;

    .line 287
    invoke-static {}, Lcom/sec/pcw/device/a/a$a;->values()[Lcom/sec/pcw/device/a/a$a;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-lt v0, v2, :cond_0

    .line 291
    return-void

    .line 287
    :cond_0
    aget-object v3, v1, v0

    .line 289
    sget-object v4, Lcom/sec/pcw/device/a/a$a;->F:Ljava/util/TreeMap;

    new-instance v5, Ljava/lang/Integer;

    iget v6, v3, Lcom/sec/pcw/device/a/a$a;->E:I

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 280
    iput p3, p0, Lcom/sec/pcw/device/a/a$a;->E:I

    .line 281
    return-void
.end method

.method public static a(I)Lcom/sec/pcw/device/a/a$a;
    .locals 2

    .prologue
    .line 300
    sget-object v0, Lcom/sec/pcw/device/a/a$a;->F:Ljava/util/TreeMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/device/a/a$a;

    .line 302
    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/pcw/device/a/a$a;->a:Lcom/sec/pcw/device/a/a$a;

    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/pcw/device/a/a$a;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/pcw/device/a/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/device/a/a$a;

    return-object v0
.end method

.method public static values()[Lcom/sec/pcw/device/a/a$a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/pcw/device/a/a$a;->G:[Lcom/sec/pcw/device/a/a$a;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/pcw/device/a/a$a;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/sec/pcw/device/a/a$a;->E:I

    return v0
.end method
