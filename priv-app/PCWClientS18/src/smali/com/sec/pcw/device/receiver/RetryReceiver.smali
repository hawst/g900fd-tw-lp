.class public Lcom/sec/pcw/device/receiver/RetryReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RetryReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 65
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 66
    :cond_0
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "intent or intent.getAction() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_1
    :goto_0
    return-void

    .line 70
    :cond_2
    invoke-static {p1}, Lcom/sec/pcw/device/util/d;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 71
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network is not available. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ignored"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.C2DM_REGISTRATION_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 76
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "ACTION_C2DM_RETRY"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v0, "C2DM"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.GCM_REGISTRATION_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 79
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "ACTION_GCM_RETRY"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "GCM"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.SPP_REGISTRATION_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 82
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "ACTION_SPP_RETRY"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "SPP"

    invoke-static {p1, v0}, Lcom/sec/pcw/device/c/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.NETWORK_ERROR_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 85
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "ACTION_NETWORK_ERROR_RETRY"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-static {p1}, Lcom/sec/pcw/device/c/d;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 87
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.TOKEN_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 88
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "ACTION_TOKEN_RETRY"

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.pcw.device.TOKEN_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/sec/pcw/device/util/h;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 90
    invoke-static {p1}, Lcom/sec/pcw/device/c/d;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 91
    :cond_8
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.pcw.device.HTTP_REQUEST_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    const-string v0, "body"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 93
    const-string v0, "uri"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 94
    const-string v0, "pushType"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 96
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    const-string v1, "ACTION_HTTP_REQUEST_RETRY: "

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "PCWCLIENTTRACE_RetryReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\turi: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/sec/pcw/device/receiver/RetryReceiver$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/pcw/device/receiver/RetryReceiver$1;-><init>(Lcom/sec/pcw/device/receiver/RetryReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 177
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method
