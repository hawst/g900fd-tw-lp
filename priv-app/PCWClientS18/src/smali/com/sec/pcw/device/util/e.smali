.class public final Lcom/sec/pcw/device/util/e;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field private static a:Z

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/pcw/device/util/e;->a:Z

    .line 30
    const/4 v0, 0x3

    sput v0, Lcom/sec/pcw/device/util/e;->b:I

    .line 36
    :try_start_0
    const-string v0, "FMM_PCW_LOGON"

    sget-boolean v1, Lcom/sec/pcw/device/util/e;->a:Z

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/pcw/device/util/e;->a:Z

    .line 37
    const-string v0, "FMM_PCW_LOGLEVEL"

    sget v1, Lcom/sec/pcw/device/util/e;->b:I

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/pcw/device/util/e;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    const-string v0, "PCWCLIENTTRACE_LOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DEFAULT_LOGON : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/pcw/device/util/e;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    const-string v0, "PCWCLIENTTRACE_LOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DEFAULT_LOGLEVEL : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/pcw/device/util/e;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    return-void

    .line 41
    :catch_0
    move-exception v0

    const-string v0, "PCWCLIENTTRACE_LOG"

    const-string v1, "Reading SystemProperties is failed."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 50
    sget-boolean v0, Lcom/sec/pcw/device/util/e;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/pcw/device/util/e;->b:I

    if-gt v0, p2, :cond_0

    .line 52
    packed-switch p2, :pswitch_data_0

    .line 62
    :pswitch_0
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 55
    :pswitch_1
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 58
    :pswitch_2
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :pswitch_3
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    :pswitch_4
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 82
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x5

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x6

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/device/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 97
    return-void
.end method
