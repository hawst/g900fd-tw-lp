.class public Lcom/sec/android/mmapp/util/MusicListUtils;
.super Ljava/lang/Object;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$AllTrackQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;,
        Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;,
        Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;,
        Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    }
.end annotation


# static fields
.field public static final ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final ALBUMS_PICK_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final ALBUM_ORDER_COLUMN:Ljava/lang/String;

.field public static final ALBUM_PROJECTION:[Ljava/lang/String;

.field public static final ALBUM_TAB:I = 0x10002

.field public static final ALBUM_TRACK:I = 0x20002

.field public static ALBUM_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final ALL_TRACK:I = 0x20001

.field public static ALL_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final ARTISTS_PICK_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final ARTIST_ORDER_COLUMN:Ljava/lang/String;

.field public static final ARTIST_TAB:I = 0x10003

.field public static final ARTIST_TRACK:I = 0x20003

.field public static ARTIST_TRACK_PROJECTION:[Ljava/lang/String; = null

.field private static final AUTHORITY:Ljava/lang/String; = "media"

.field public static final BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final CONTENT_AUTHORITY_SLASH:Ljava/lang/String; = "content://media/"

.field public static final COUNT_OF_SONGS_ID:J = -0x65L

.field public static final DEFAULT_MUSIC_QUERY_WHERE:Ljava/lang/String; = "title != \'\' AND is_music=1"

.field public static final DEFAULT_PICK_MUSIC_QUERY_WHERE:Ljava/lang/String; = "title != \'\'"

.field public static final DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

.field private static final EXTERNAL:Ljava/lang/String; = "external"

.field public static final FOLDERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final FOLDERS_PICK_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final FOLDER_COUNT:Ljava/lang/String; = "count"

.field public static final FOLDER_PROJECTION:[Ljava/lang/String;

.field public static final FOLDER_TAB:I = 0x10007

.field public static final FOLDER_TRACK:I = 0x20007

.field public static FOLDER_TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final IS_SECRETBOX:Ljava/lang/String; = "is_secretbox"

.field public static final LIST_CONTENT_TYPE_ALBUM:I = 0x2

.field public static final LIST_CONTENT_TYPE_ALL:I = 0x1

.field public static final LIST_CONTENT_TYPE_ARTIST:I = 0x3

.field public static final LIST_CONTENT_TYPE_FOLDER:I = 0x7

.field private static final LIST_CONTENT_TYPE_MASK:I = 0xff

.field public static final LIST_TYPE_FILTER:I = -0xf0001

.field private static final LIST_TYPE_MASK:I = 0x30000

.field public static final LIST_TYPE_TAB:I = 0x10000

.field public static final LIST_TYPE_TRACK:I = 0x20000

.field private static final NORMAL_LIST_TYPE_MASK:I = 0x3ffff

.field public static final SAMSUNG_ARTIST_PROJECTION:[Ljava/lang/String;

.field public static SAMSUNG_FOLDER_PROJECTION:[Ljava/lang/String; = null

.field public static final TAG_MUSIC_LIST:Ljava/lang/String; = "music_list"

.field public static final TITLE_ORDER_COLUMN:Ljava/lang/String;

.field public static TRACK_PROJECTION:[Ljava/lang/String; = null

.field public static final UNDEFINDED_LIST_TYPE:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    const-class v0, Lcom/sec/android/mmapp/util/MusicListUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->CLASSNAME:Ljava/lang/String;

    .line 243
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_0

    const-string v0, "title_pinyin"

    :goto_0
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    .line 246
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_1

    const-string v0, "artist_pinyin"

    :goto_1
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    .line 249
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_2

    const-string v0, "album_pinyin"

    :goto_2
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    .line 252
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_3

    const-string v0, "_display_name_pinyin"

    :goto_3
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    .line 255
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_4

    const-string v0, "bucket_display_name_pinyin"

    :goto_4
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    .line 473
    const-string v0, "content://media/external/audio/media/music_albums"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 476
    const-string v0, "content://media/external/audio/media/music_artists_album_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 479
    const-string v0, "content://media/external/audio/media/music_folders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 482
    const-string v0, "content://media/external/audio/media/music_pick_albums"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUMS_PICK_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 485
    const-string v0, "content://media/external/audio/media/music_pick_artists_album_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTISTS_PICK_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 488
    const-string v0, "content://media/external/audio/media/music_pick_folders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDERS_PICK_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 520
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_5

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    aput-object v1, v0, v6

    :goto_5
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_PROJECTION:[Ljava/lang/String;

    .line 529
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDER_PROJECTION:[Ljava/lang/String;

    .line 536
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const-string v1, "bucket_id"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/mmapp/util/MusicListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    :goto_6
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->SAMSUNG_FOLDER_PROJECTION:[Ljava/lang/String;

    .line 547
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_7

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    aput-object v1, v0, v6

    :goto_7
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->SAMSUNG_ARTIST_PROJECTION:[Ljava/lang/String;

    .line 559
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/mmapp/util/MusicListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/mmapp/util/MusicListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    aput-object v2, v0, v1

    :goto_8
    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->TRACK_PROJECTION:[Ljava/lang/String;

    .line 572
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "is_secretbox"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALL_TRACK_PROJECTION:[Ljava/lang/String;

    .line 577
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v6

    const-string v1, "album_id"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_TRACK_PROJECTION:[Ljava/lang/String;

    .line 583
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTIST_TRACK_PROJECTION:[Ljava/lang/String;

    .line 589
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDER_TRACK_PROJECTION:[Ljava/lang/String;

    return-void

    .line 243
    :cond_0
    const-string v0, "title"

    goto/16 :goto_0

    .line 246
    :cond_1
    const-string v0, "artist"

    goto/16 :goto_1

    .line 249
    :cond_2
    const-string v0, "album"

    goto/16 :goto_2

    .line 252
    :cond_3
    const-string v0, "_display_name"

    goto/16 :goto_3

    .line 255
    :cond_4
    const-string v0, "bucket_display_name"

    goto/16 :goto_4

    .line 520
    :cond_5
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    goto/16 :goto_5

    .line 536
    :cond_6
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const-string v1, "bucket_id"

    aput-object v1, v0, v6

    const-string v1, "_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "count"

    aput-object v2, v0, v1

    goto/16 :goto_6

    .line 547
    :cond_7
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    goto/16 :goto_7

    .line 559
    :cond_8
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    goto/16 :goto_8
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Z)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Z

    .prologue
    .line 38
    invoke-static {p0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getDefaultMusicQueryWhere(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Z)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Z

    .prologue
    .line 38
    invoke-static {p0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getAlbumTabQueryArgsUri(Z)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Z)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Z

    .prologue
    .line 38
    invoke-static {p0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getArtistTabQueryArgsUri(Z)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Z)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Z

    .prologue
    .line 38
    invoke-static {p0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getFolderTabQueryArgsUri(Z)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static final getAlbumTabQueryArgsUri(Z)Landroid/net/Uri;
    .locals 1
    .param p0, "isMusic"    # Z

    .prologue
    .line 492
    if-eqz p0, :cond_0

    .line 493
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUMS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 495
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUMS_PICK_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static final getArtistTabQueryArgsUri(Z)Landroid/net/Uri;
    .locals 1
    .param p0, "isMusic"    # Z

    .prologue
    .line 499
    if-eqz p0, :cond_0

    .line 500
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTISTS_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 502
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTISTS_PICK_ALBUM_ID_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static getDefaultMusicQueryWhere(Z)Ljava/lang/String;
    .locals 1
    .param p0, "isMusic"    # Z

    .prologue
    .line 602
    if-eqz p0, :cond_0

    .line 603
    const-string v0, "title != \'\' AND is_music=1"

    .line 605
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "title != \'\'"

    goto :goto_0
.end method

.method private static final getFolderTabQueryArgsUri(Z)Landroid/net/Uri;
    .locals 1
    .param p0, "isMusic"    # Z

    .prologue
    .line 506
    if-eqz p0, :cond_0

    .line 507
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDERS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 509
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDERS_PICK_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static getListContentType(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 86
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static getListType(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 63
    const/high16 v0, 0x30000

    and-int/2addr v0, p0

    return v0
.end method

.method public static getMusicListInfo(ILjava/lang/String;Z)Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    .locals 4
    .param p0, "list"    # I
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isMusic"    # Z

    .prologue
    .line 224
    invoke-static {p0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getListType(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 232
    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicListInfo > invalid list type : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Did you miss something?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const/4 v0, 0x0

    .line 238
    .local v0, "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    :goto_0
    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicListInfo > list type : 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keyWord : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    return-object v0

    .line 226
    .end local v0    # "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    :sswitch_0
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;-><init>(IZ)V

    .line 227
    .restart local v0    # "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    goto :goto_0

    .line 229
    .end local v0    # "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    :sswitch_1
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;-><init>(ILjava/lang/String;Z)V

    .line 230
    .restart local v0    # "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    goto :goto_0

    .line 224
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getPreDefinedList(I)I
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 102
    const v0, 0x3ffff

    and-int/2addr v0, p0

    return v0
.end method

.method public static getSubTrackList(I)I
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 54
    const v0, -0xf0001

    and-int/2addr v0, p0

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    return v0
.end method

.method public static getTabName(I)I
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 134
    invoke-static {p0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getPreDefinedList(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 152
    const v0, 0x7f0c000a

    .line 155
    .local v0, "tabNameId":I
    :goto_0
    return v0

    .line 136
    .end local v0    # "tabNameId":I
    :sswitch_0
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v1, :cond_0

    .line 137
    const v0, 0x7f0c0048

    .restart local v0    # "tabNameId":I
    goto :goto_0

    .line 139
    .end local v0    # "tabNameId":I
    :cond_0
    const v0, 0x7f0c000a

    .line 141
    .restart local v0    # "tabNameId":I
    goto :goto_0

    .line 143
    .end local v0    # "tabNameId":I
    :sswitch_1
    const v0, 0x7f0c0009

    .line 144
    .restart local v0    # "tabNameId":I
    goto :goto_0

    .line 146
    .end local v0    # "tabNameId":I
    :sswitch_2
    const v0, 0x7f0c000c

    .line 147
    .restart local v0    # "tabNameId":I
    goto :goto_0

    .line 149
    .end local v0    # "tabNameId":I
    :sswitch_3
    const v0, 0x7f0c0024

    .line 150
    .restart local v0    # "tabNameId":I
    goto :goto_0

    .line 134
    nop

    :sswitch_data_0
    .sparse-switch
        0x10002 -> :sswitch_1
        0x10003 -> :sswitch_2
        0x10007 -> :sswitch_3
        0x20001 -> :sswitch_0
    .end sparse-switch
.end method

.method public static isTab(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 67
    const/high16 v0, 0x30000

    and-int/2addr v0, p0

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTrack(I)Z
    .locals 2
    .param p0, "list"    # I

    .prologue
    .line 71
    const/high16 v0, 0x30000

    and-int/2addr v0, p0

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mainView"    # Landroid/view/View;
    .param p2, "wd"    # Landroid/view/Window;
    .param p3, "gravity"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 610
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p2, v1}, Landroid/view/Window;->setDimAmount(F)V

    .line 611
    invoke-virtual {p2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 612
    .local v0, "param":Landroid/view/WindowManager$LayoutParams;
    iput p3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 613
    iput p4, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 614
    iput p5, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 615
    invoke-virtual {p2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 616
    if-eqz p1, :cond_0

    .line 617
    invoke-virtual {p1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 619
    :cond_0
    return-void
.end method
