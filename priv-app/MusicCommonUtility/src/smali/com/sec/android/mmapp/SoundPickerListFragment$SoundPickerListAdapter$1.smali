.class Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;
.super Landroid/os/Handler;
.source "SoundPickerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)V
    .locals 0

    .prologue
    .line 1775
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1778
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    iget-object v1, v1, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->access$2700(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    iget-object v1, v1, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mDuration:I
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2800(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v1

    if-lez v1, :cond_0

    .line 1779
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    iget-object v1, v1, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    iget-object v2, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mDuration:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2800(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v2

    div-int v0, v1, v2

    .line 1780
    .local v0, "progress":I
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->access$2700(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1781
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;->this$1:Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgressUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->access$2900(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1783
    .end local v0    # "progress":I
    :cond_0
    return-void
.end method
