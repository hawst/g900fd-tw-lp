.class public final Lcom/sec/android/mmapp/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final IDS_MP_POP_LOW_BATTERY:I = 0x7f0c0000

.field public static final IDS_MUSIC_BODY_LIST_UNKNOWN_ALBUM:I = 0x7f0c0001

.field public static final IDS_MUSIC_BODY_LIST_UNKNOWN_ARTIST:I = 0x7f0c0002

.field public static final NNN_hours:I = 0x7f0c0003

.field public static final NNN_minutes:I = 0x7f0c0004

.field public static final NNN_seconds:I = 0x7f0c0005

.field public static final NNN_tab_n_of_total:I = 0x7f0c0006

.field public static final SSS_tab:I = 0x7f0c0007

.field public static final SSS_tab_currently_set:I = 0x7f0c0008

.field public static final albums:I = 0x7f0c0009

.field public static final all:I = 0x7f0c000a

.field public static final app_name:I = 0x7f0c000b

.field public static final artists:I = 0x7f0c000c

.field public static final auto_recommendation:I = 0x7f0c000d

.field public static final auto_recommendation_help:I = 0x7f0c000e

.field public static final blank:I = 0x7f0c000f

.field public static final cancel:I = 0x7f0c0010

.field public static final data_check_help_dont_show_again:I = 0x7f0c0011

.field public static final data_check_help_mobile_body:I = 0x7f0c0012

.field public static final data_check_help_mobile_title:I = 0x7f0c0013

.field public static final data_check_help_wifi_body:I = 0x7f0c0014

.field public static final data_check_help_wifi_title:I = 0x7f0c0015

.field public static final deleted:I = 0x7f0c0016

.field public static final drm_acquiring_license:I = 0x7f0c0017

.field public static final drm_fail_acquire_license:I = 0x7f0c0018

.field public static final drm_license_expired:I = 0x7f0c0019

.field public static final drm_no_data_connectivity:I = 0x7f0c001a

.field public static final drm_no_longer_available:I = 0x7f0c001b

.field public static final drm_play_now:I = 0x7f0c001c

.field public static final drm_server_experiencing_problem_try_later:I = 0x7f0c001d

.field public static final drm_unable_access_server_roaming_flight_mode:I = 0x7f0c001e

.field public static final drm_want_delete:I = 0x7f0c001f

.field public static final drm_want_unlock:I = 0x7f0c0020

.field public static final error_not_support_type:I = 0x7f0c0021

.field public static final error_unknown:I = 0x7f0c0022

.field public static final file_is_not_exit:I = 0x7f0c0023

.field public static final folders:I = 0x7f0c0024

.field public static final list_menu_search:I = 0x7f0c0025

.field public static final loading:I = 0x7f0c0026

.field public static final module_name:I = 0x7f0c0027

.field public static final network_error_occurred_msg:I = 0x7f0c0028

.field public static final no_albums:I = 0x7f0c0029

.field public static final no_artists:I = 0x7f0c002a

.field public static final no_folders:I = 0x7f0c002b

.field public static final no_results:I = 0x7f0c002c

.field public static final no_songs:I = 0x7f0c002d

.field public static final number_of_albums:I = 0x7f0c002e

.field public static final number_of_artists:I = 0x7f0c002f

.field public static final number_of_folders:I = 0x7f0c0030

.field public static final number_of_items_selected:I = 0x7f0c0031

.field public static final number_of_songs:I = 0x7f0c0032

.field public static final ok:I = 0x7f0c0033

.field public static final option_alarm_tone:I = 0x7f0c0034

.field public static final option_caller_ringtone:I = 0x7f0c0035

.field public static final option_phone_ringtone:I = 0x7f0c0036

.field public static final option_set_as:I = 0x7f0c0037

.field public static final option_set_as_wifi:I = 0x7f0c0038

.field public static final palm_touch_toast_finish:I = 0x7f0c0039

.field public static final palm_touch_toast_start:I = 0x7f0c003a

.field public static final palm_tutorial_title:I = 0x7f0c003b

.field public static final personal_item_set_as_error:I = 0x7f0c003c

.field public static final play_from_beginning:I = 0x7f0c003d

.field public static final playback_failed:I = 0x7f0c003e

.field public static final processing:I = 0x7f0c003f

.field public static final ringtone_added:I = 0x7f0c0040

.field public static final select_all:I = 0x7f0c0041

.field public static final select_sim:I = 0x7f0c0042

.field public static final sound_picker:I = 0x7f0c0043

.field public static final sound_player:I = 0x7f0c0044

.field public static final stms_appgroup:I = 0x7f0c0045

.field public static final stms_version:I = 0x7f0c0046

.field public static final svoice_in_notification:I = 0x7f0c0047

.field public static final tracks:I = 0x7f0c0048

.field public static final tts_close_button:I = 0x7f0c0049

.field public static final tts_ff_button:I = 0x7f0c004a

.field public static final tts_pause_button:I = 0x7f0c004b

.field public static final tts_play_button:I = 0x7f0c004c

.field public static final tts_rew_button:I = 0x7f0c004d

.field public static final tutorial_complete:I = 0x7f0c004e

.field public static final unable_to_find_item:I = 0x7f0c004f

.field public static final unable_to_play_during_call:I = 0x7f0c0050

.field public static final unknown:I = 0x7f0c0051

.field public static final voice_call_ringtone:I = 0x7f0c0052

.field public static final voice_call_ringtone2:I = 0x7f0c0053

.field public static final voice_call_ringtone_slot1:I = 0x7f0c0054

.field public static final voice_call_ringtone_slot2:I = 0x7f0c0055

.field public static final wlan_connection_required_for_chn:I = 0x7f0c0056


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
