.class public Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;
.super Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackListInfo"
.end annotation


# direct methods
.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 3
    .param p1, "list"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "isMusic"    # Z

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;-><init>()V

    .line 168
    const v0, 0x7f0c002d

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;->noItemTextId:I

    .line 169
    const v0, 0x7f0b0009

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;->numberOfTextId:I

    .line 171
    invoke-static {p1}, Lcom/sec/android/mmapp/util/MusicListUtils;->getListContentType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 187
    :pswitch_0
    # getter for: Lcom/sec/android/mmapp/util/MusicListUtils;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/util/MusicListUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TrackListInfo > invalid track list : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Did you miss something? keyWord is < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :goto_0
    return-void

    .line 173
    :pswitch_1
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$AllTrackQueryArgs;

    invoke-direct {v0, p3}, Lcom/sec/android/mmapp/util/MusicListUtils$AllTrackQueryArgs;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 176
    :pswitch_2
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 181
    :pswitch_3
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 184
    :pswitch_4
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;

    invoke-direct {v0, p2, p3}, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
