.class public Lcom/sec/android/mmapp/util/MusicListUtils$AllTrackQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isMusic"    # Z

    .prologue
    .line 365
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    .line 366
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-nez v0, :cond_0

    .line 367
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALL_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AllTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 369
    :cond_0
    return-void
.end method
