.class Lcom/sec/android/mmapp/AudioPreview$13;
.super Landroid/content/BroadcastReceiver;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 1945
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$13;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 1949
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1950
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1951
    const-string v3, "status"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1953
    .local v2, "batteryStatus":I
    const-string v3, "level"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1954
    .local v1, "batteryLevel":I
    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    if-gt v1, v5, :cond_1

    .line 1956
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$13;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/high16 v4, 0x7f0c0000

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1958
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$13;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$000(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1959
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$13;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 1961
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$13;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 1964
    .end local v1    # "batteryLevel":I
    .end local v2    # "batteryStatus":I
    :cond_1
    return-void
.end method
