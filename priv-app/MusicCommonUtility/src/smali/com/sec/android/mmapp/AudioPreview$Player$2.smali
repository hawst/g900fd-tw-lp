.class Lcom/sec/android/mmapp/AudioPreview$Player$2;
.super Landroid/os/Handler;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview$Player;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCurrentVolume:F

.field final synthetic this$1:Lcom/sec/android/mmapp/AudioPreview$Player;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 1

    .prologue
    .line 2569
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2570
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x14

    const/4 v5, 0x4

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 2574
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 2616
    :cond_0
    :goto_0
    return-void

    .line 2576
    :pswitch_0
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMpHandler() - MP_HANDLER_FADEIN - mCurrentVolume: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 2579
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    .line 2580
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6100(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2581
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start(ZZ)V
    invoke-static {v1, v4, v6}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6200(Lcom/sec/android/mmapp/AudioPreview$Player;ZZ)V

    .line 2582
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5900(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2584
    :cond_1
    iget v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mVolumeIncrement:F
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6300(Lcom/sec/android/mmapp/AudioPreview$Player;)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    .line 2585
    iget v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6400(Lcom/sec/android/mmapp/AudioPreview$Player;)F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 2586
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5900(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2590
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6100(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 2588
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6400(Lcom/sec/android/mmapp/AudioPreview$Player;)F

    move-result v1

    iput v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->mCurrentVolume:F

    goto :goto_1

    .line 2594
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6100(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2595
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    if-ne v1, v5, :cond_5

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsSeekNow:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$6500(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2596
    const/4 v0, 0x0

    .line 2597
    .local v0, "progress":I
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    if-lez v1, :cond_3

    .line 2598
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getPositionOfSeekBar()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6600(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    .line 2599
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mProgressBarHandler: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2601
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v1, v0}, Lcom/sec/android/mmapp/AudioPreview;->access$5102(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2602
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2603
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v2, v2, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$5100(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2605
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v2, v2, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v3

    int-to-long v4, v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;
    invoke-static {v2, v4, v5}, Lcom/sec/android/mmapp/AudioPreview;->access$5200(Lcom/sec/android/mmapp/AudioPreview;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2606
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, v1, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v2, v2, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v3, v3, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2610
    .end local v0    # "progress":I
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$2;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5900(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 2574
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
