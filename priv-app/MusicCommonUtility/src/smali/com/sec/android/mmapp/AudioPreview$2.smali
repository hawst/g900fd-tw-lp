.class Lcom/sec/android/mmapp/AudioPreview$2;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/AudioPreview;->initiateView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 694
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$2;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 697
    if-nez p2, :cond_0

    .line 698
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$2;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$2;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 700
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$2;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mDownTime:J
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1800(Lcom/sec/android/mmapp/AudioPreview;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x12c

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$2;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2600(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 705
    :cond_0
    return-void
.end method
