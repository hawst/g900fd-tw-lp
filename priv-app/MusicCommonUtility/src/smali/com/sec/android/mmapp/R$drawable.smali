.class public final Lcom/sec/android/mmapp/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_check_box_animator_list:I = 0x7f020000

.field public static final close_btn:I = 0x7f020001

.field public static final f_album_thumbnail_ic_f:I = 0x7f020002

.field public static final ff_btn:I = 0x7f020003

.field public static final ff_only_btn:I = 0x7f020004

.field public static final gallery_album_thumbnail_ic_folder:I = 0x7f020005

.field public static final header_cancel_btn:I = 0x7f020006

.field public static final header_check_btn:I = 0x7f020007

.field public static final help_popup_picker_bg_w_01:I = 0x7f020008

.field public static final hover_music_bg_c:I = 0x7f020009

.field public static final hover_music_bg_l:I = 0x7f02000a

.field public static final hover_music_bg_r:I = 0x7f02000b

.field public static final hover_popup_black_bg:I = 0x7f02000c

.field public static final list_btn_bg:I = 0x7f02000d

.field public static final list_divider:I = 0x7f02000e

.field public static final list_progress_bg:I = 0x7f02000f

.field public static final mainmenu_icon_clock:I = 0x7f020010

.field public static final mainmenu_icon_contacts:I = 0x7f020011

.field public static final mainmenu_icon_phone:I = 0x7f020012

.field public static final music_conversation_bg:I = 0x7f020013

.field public static final music_default_cover_img:I = 0x7f020014

.field public static final music_list_btn_pause_normal:I = 0x7f020015

.field public static final music_list_btn_play_normal:I = 0x7f020016

.field public static final music_list_personal_icon:I = 0x7f020017

.field public static final music_miniplayer_album_thumbnail_bg:I = 0x7f020018

.field public static final music_miniplayer_control_ff:I = 0x7f020019

.field public static final music_miniplayer_control_ff_focus:I = 0x7f02001a

.field public static final music_miniplayer_control_ff_only:I = 0x7f02001b

.field public static final music_miniplayer_control_ff_only_focus:I = 0x7f02001c

.field public static final music_miniplayer_control_ff_only_press:I = 0x7f02001d

.field public static final music_miniplayer_control_ff_press:I = 0x7f02001e

.field public static final music_miniplayer_control_pause:I = 0x7f02001f

.field public static final music_miniplayer_control_pause_focus:I = 0x7f020020

.field public static final music_miniplayer_control_pause_press:I = 0x7f020021

.field public static final music_miniplayer_control_play:I = 0x7f020022

.field public static final music_miniplayer_control_play_focus:I = 0x7f020023

.field public static final music_miniplayer_control_play_press:I = 0x7f020024

.field public static final music_miniplayer_control_popup_bg:I = 0x7f020025

.field public static final music_miniplayer_control_popup_bg_dark:I = 0x7f020026

.field public static final music_miniplayer_control_rw:I = 0x7f020027

.field public static final music_miniplayer_control_rw_focus:I = 0x7f020028

.field public static final music_miniplayer_control_rw_only:I = 0x7f020029

.field public static final music_miniplayer_control_rw_only_focus:I = 0x7f02002a

.field public static final music_miniplayer_control_rw_only_press:I = 0x7f02002b

.field public static final music_miniplayer_control_rw_press:I = 0x7f02002c

.field public static final music_miniplayer_control_stop:I = 0x7f02002d

.field public static final music_miniplayer_control_stop_focus:I = 0x7f02002e

.field public static final music_miniplayer_control_stop_press:I = 0x7f02002f

.field public static final music_miniplayer_progress_bar:I = 0x7f020030

.field public static final music_miniplayer_progress_bar_02:I = 0x7f020031

.field public static final music_miniplayer_progress_bg:I = 0x7f020032

.field public static final music_play_icon_close:I = 0x7f020033

.field public static final music_play_icon_close_focus:I = 0x7f020034

.field public static final music_play_icon_close_press:I = 0x7f020035

.field public static final music_play_tw_progressive_point:I = 0x7f020036

.field public static final music_player_progress:I = 0x7f020037

.field public static final music_player_progressbar:I = 0x7f020038

.field public static final music_player_progressive_focus:I = 0x7f020039

.field public static final music_player_progressive_ing:I = 0x7f02003a

.field public static final music_player_progressive_press:I = 0x7f02003b

.field public static final music_player_seekbar:I = 0x7f02003c

.field public static final music_search_icon_album:I = 0x7f02003d

.field public static final music_search_icon_artist:I = 0x7f02003e

.field public static final music_search_icon_songs:I = 0x7f02003f

.field public static final no_item_bg_land:I = 0x7f020040

.field public static final no_item_bg_port:I = 0x7f020041

.field public static final palm_swipe_mute_music_01:I = 0x7f020042

.field public static final palm_swipe_mute_music_02:I = 0x7f020043

.field public static final pause_btn:I = 0x7f020044

.field public static final play_btn:I = 0x7f020045

.field public static final player_icon_noitem:I = 0x7f020046

.field public static final rew_btn:I = 0x7f020047

.field public static final rew_only_btn:I = 0x7f020048

.field public static final sound_player_ic_privatemode:I = 0x7f020049

.field public static final sound_player_personal_icon:I = 0x7f02004a

.field public static final soundpicker_dark_progress:I = 0x7f02004b

.field public static final soundpicker_dark_progress_bg:I = 0x7f02004c

.field public static final soundpicker_progress:I = 0x7f02004d

.field public static final soundpicker_progress_bg:I = 0x7f02004e

.field public static final stat_notify_voice_input:I = 0x7f02004f

.field public static final tab_indicator_selector:I = 0x7f020050

.field public static final tw_action_bar_icon_cancel_02_disabled_holo_light:I = 0x7f020051

.field public static final tw_action_bar_icon_cancel_02_holo_light:I = 0x7f020052

.field public static final tw_action_bar_icon_check_disabled_holo_light:I = 0x7f020053

.field public static final tw_action_bar_icon_check_holo_light:I = 0x7f020054

.field public static final tw_action_bar_icon_search_holo_light:I = 0x7f020055

.field public static final tw_action_bar_sub_tab_bg_01_holo_dark:I = 0x7f020056

.field public static final tw_action_bar_tab_bg_holo_light:I = 0x7f020057

.field public static final tw_action_bar_tab_selected_bg_holo_dark:I = 0x7f020058

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f020059

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f02005a

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f02005b

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f02005c

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f02005d

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f02005e

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f02005f

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f020060

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f020061

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f020062

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f020063

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f020064

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f020065

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f020066

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f020067

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f020068

.field public static final tw_list_divider_holo_dark:I = 0x7f020069

.field public static final tw_list_divider_holo_light:I = 0x7f02006a

.field public static final tw_list_focused_holo_light:I = 0x7f02006b

.field public static final tw_list_header_bg_right_focused_holo_light:I = 0x7f02006c

.field public static final tw_list_header_bg_right_holo_light:I = 0x7f02006d

.field public static final tw_list_header_bg_right_pressed_holo_light:I = 0x7f02006e

.field public static final tw_list_pressed_holo_light:I = 0x7f02006f

.field public static final tw_list_section_divider_holo_light:I = 0x7f020070

.field public static final tw_list_selected_holo_light:I = 0x7f020071

.field public static final tw_popup_bg_holo_dark:I = 0x7f020072

.field public static final tw_preference_list_section_divider_holo_light:I = 0x7f020073

.field public static final tw_searchfiled_background_holo_dark:I = 0x7f020074

.field public static final tw_select_all_bg_holo_light:I = 0x7f020075

.field public static final tw_sub_action_bar_bg_holo_light:I = 0x7f020076

.field public static final tw_subtab_bg_holo_dark:I = 0x7f020077

.field public static final tw_tab_divider_holo_dark:I = 0x7f020078

.field public static final tw_tab_pressed_holo_dark:I = 0x7f020079

.field public static final tw_tab_selected_focused_holo_dark:I = 0x7f02007a

.field public static final tw_tab_selected_focused_pressed_holo_dark:I = 0x7f02007b

.field public static final tw_tab_unselected_bar_holo:I = 0x7f02007c

.field public static final unbounded_ripple:I = 0x7f02007d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
