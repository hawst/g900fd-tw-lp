.class public final Lcom/sec/android/mmapp/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final alarm_tone:I = 0x7f0f0050

.field public static final alarm_tone_icon:I = 0x7f0f0052

.field public static final alarm_tone_radio:I = 0x7f0f0051

.field public static final alarm_tone_text:I = 0x7f0f0053

.field public static final album_art:I = 0x7f0f000a

.field public static final albumart:I = 0x7f0f0020

.field public static final albumart_button:I = 0x7f0f0021

.field public static final albumart_layout:I = 0x7f0f001f

.field public static final auto_divider:I = 0x7f0f003e

.field public static final auto_recommendation:I = 0x7f0f002c

.field public static final auto_recommendation_check_box:I = 0x7f0f002b

.field public static final auto_recommendation_help:I = 0x7f0f002d

.field public static final auto_recommendation_selection:I = 0x7f0f002a

.field public static final caller_ringtone:I = 0x7f0f004b

.field public static final caller_ringtone_divider:I = 0x7f0f004f

.field public static final caller_ringtone_icon:I = 0x7f0f004d

.field public static final caller_ringtone_radio:I = 0x7f0f004c

.field public static final caller_ringtone_text:I = 0x7f0f004e

.field public static final checkbox:I = 0x7f0f001c

.field public static final checked:I = 0x7f0f0054

.field public static final close_btn:I = 0x7f0f0009

.field public static final conrtol_panel_layout:I = 0x7f0f0005

.field public static final container:I = 0x7f0f002e

.field public static final custom_divider:I = 0x7f0f001e

.field public static final custom_toast_layout:I = 0x7f0f000f

.field public static final duration_time_text:I = 0x7f0f0044

.field public static final ff_btn:I = 0x7f0f0008

.field public static final help_touch_text:I = 0x7f0f0033

.field public static final icon:I = 0x7f0f0011

.field public static final internalEmpty:I = 0x7f0f0016

.field public static final list:I = 0x7f0f0015

.field public static final listContainer:I = 0x7f0f0014

.field public static final list_animation:I = 0x7f0f0022

.field public static final list_item_divider_bottom:I = 0x7f0f001d

.field public static final list_item_title_layout:I = 0x7f0f0018

.field public static final list_item_title_text:I = 0x7f0f001a

.field public static final list_item_track_layout:I = 0x7f0f001b

.field public static final list_main_header_select_all_layout:I = 0x7f0f0024

.field public static final list_subtitle_divider:I = 0x7f0f0019

.field public static final menu_cancel:I = 0x7f0f000c

.field public static final menu_empty:I = 0x7f0f000e

.field public static final menu_ok:I = 0x7f0f000d

.field public static final music_list_header_select_all_stub:I = 0x7f0f0028

.field public static final music_picker_list_view:I = 0x7f0f0029

.field public static final name:I = 0x7f0f0012

.field public static final no_item_text:I = 0x7f0f0027

.field public static final normal:I = 0x7f0f0037

.field public static final normal_radio:I = 0x7f0f0038

.field public static final offset_time_text:I = 0x7f0f0041

.field public static final palm_tutorial_img_1:I = 0x7f0f0031

.field public static final palm_tutorial_img_2:I = 0x7f0f0032

.field public static final personal_icon:I = 0x7f0f0001

.field public static final phone_ringtone:I = 0x7f0f003f

.field public static final phone_ringtone_divider:I = 0x7f0f004a

.field public static final phone_ringtone_icon:I = 0x7f0f0048

.field public static final phone_ringtone_radio:I = 0x7f0f0047

.field public static final phone_ringtone_text:I = 0x7f0f0049

.field public static final play_pause_btn:I = 0x7f0f0007

.field public static final play_time:I = 0x7f0f0003

.field public static final progress:I = 0x7f0f0030

.field public static final progressContainer:I = 0x7f0f0013

.field public static final progress_bar:I = 0x7f0f0002

.field public static final progress_bg:I = 0x7f0f0042

.field public static final progress_info:I = 0x7f0f0040

.field public static final query_browser_main_layout:I = 0x7f0f0035

.field public static final radio:I = 0x7f0f002f

.field public static final recommend:I = 0x7f0f003a

.field public static final recommend_radio:I = 0x7f0f003b

.field public static final remain_time:I = 0x7f0f0004

.field public static final resolver_grid:I = 0x7f0f0036

.field public static final rew_btn:I = 0x7f0f0006

.field public static final search_view:I = 0x7f0f0034

.field public static final select_all_checkbox:I = 0x7f0f0025

.field public static final select_all_text:I = 0x7f0f0026

.field public static final setAsDivider:I = 0x7f0f0046

.field public static final set_as:I = 0x7f0f0045

.field public static final starting_time_text:I = 0x7f0f0043

.field public static final text:I = 0x7f0f0010

.field public static final text1:I = 0x7f0f0017

.field public static final text2:I = 0x7f0f0023

.field public static final text_main_1:I = 0x7f0f0039

.field public static final text_main_2:I = 0x7f0f003c

.field public static final text_sub_2:I = 0x7f0f003d

.field public static final time_layout:I = 0x7f0f000b

.field public static final title:I = 0x7f0f0000

.field public static final unchecked:I = 0x7f0f0055


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
