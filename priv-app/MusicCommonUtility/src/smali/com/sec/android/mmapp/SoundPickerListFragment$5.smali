.class Lcom/sec/android/mmapp/SoundPickerListFragment$5;
.super Landroid/os/Handler;
.source "SoundPickerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 600
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$5;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 603
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 604
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$5;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 605
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_1

    .line 606
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 607
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$5;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->isItemSelected(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 609
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 606
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 611
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_1

    .line 614
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$5;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # invokes: Lcom/sec/android/mmapp/SoundPickerListFragment;->updateSelectedCount()V
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1000(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    .line 616
    .end local v1    # "lv":Landroid/widget/AbsListView;
    :cond_2
    return-void
.end method
