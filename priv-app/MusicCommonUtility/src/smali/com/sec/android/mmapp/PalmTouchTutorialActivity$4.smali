.class Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;
.super Ljava/lang/Object;
.source "PalmTouchTutorialActivity.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/PalmTouchTutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 252
    packed-switch p1, :pswitch_data_0

    .line 264
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v0, v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 256
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # invokes: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->stopSound()V
    invoke-static {v0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$100(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    goto :goto_0

    .line 259
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v0, v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # invokes: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->startSound()V
    invoke-static {v0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$500(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    goto :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
