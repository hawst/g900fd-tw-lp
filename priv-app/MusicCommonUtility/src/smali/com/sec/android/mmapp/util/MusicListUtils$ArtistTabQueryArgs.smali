.class public Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtistTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "isMusic"    # Z

    .prologue
    const/4 v1, 0x0

    .line 437
    invoke-direct {p0}, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;-><init>()V

    .line 438
    # invokes: Lcom/sec/android/mmapp/util/MusicListUtils;->getArtistTabQueryArgsUri(Z)Landroid/net/Uri;
    invoke-static {p1}, Lcom/sec/android/mmapp/util/MusicListUtils;->access$300(Z)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->uri:Landroid/net/Uri;

    .line 439
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->SAMSUNG_ARTIST_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->projection:[Ljava/lang/String;

    .line 440
    iput-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->selection:Ljava/lang/String;

    .line 441
    iput-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 442
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 443
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTIST_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 445
    const-string v0, "_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 446
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 447
    const-string v0, "artist"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 448
    return-void
.end method
