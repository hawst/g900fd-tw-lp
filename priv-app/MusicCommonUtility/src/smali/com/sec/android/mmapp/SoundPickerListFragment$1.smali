.class Lcom/sec/android/mmapp/SoundPickerListFragment$1;
.super Ljava/lang/Object;
.source "SoundPickerListFragment.java"

# interfaces
.implements Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(II)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "offset"    # I

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$000(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 414
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mResultListener: call ProgressDialog.cancel"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$000(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayerLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 418
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mRequestedMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$400(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eq v0, v2, :cond_2

    .line 419
    :cond_1
    monitor-exit v1

    .line 431
    :goto_0
    return-void

    .line 421
    :cond_2
    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    .line 422
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # setter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mOffset:I
    invoke-static {v0, p2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$502(Lcom/sec/android/mmapp/SoundPickerListFragment;I)I

    .line 423
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mOffset:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$500(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 428
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mListUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$600(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 426
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_1

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
