.class Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;
.implements Landroid/widget/SeekBar$OnSeekBarHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SeekBarHoverListener"
.end annotation


# instance fields
.field private mHoverPosition:I

.field private mSeekInfo:Landroid/view/View;

.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 3298
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3345
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mHoverPosition:I

    .line 3346
    const/4 v0, 0x0

    return v0
.end method

.method public onHoverChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 3320
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 3321
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->updateProgressbarPreviewView(Landroid/widget/SeekBar;I)V

    .line 3323
    :cond_0
    return-void
.end method

.method public onStartTrackingHover(Landroid/widget/SeekBar;I)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    .line 3310
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3311
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040006

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    .line 3313
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 3314
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->updateProgressbarPreviewView(Landroid/widget/SeekBar;I)V

    .line 3316
    :cond_0
    return-void
.end method

.method public onStopTrackingHover(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 3306
    return-void
.end method

.method public updateProgressbarPreviewView(Landroid/widget/SeekBar;I)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    .line 3326
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v2, v4, 0x2

    .line 3327
    .local v2, "offset_x":I
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v4}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v3, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 3328
    .local v3, "screenWidth":I
    int-to-double v4, p2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v6}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v6

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I
    invoke-static {v6}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-long v0, v4

    .line 3330
    .local v0, "newPosition":J
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;
    invoke-static {v5, v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$5200(Lcom/sec/android/mmapp/AudioPreview;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3333
    :goto_0
    iget v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mHoverPosition:I

    if-ge v4, v2, :cond_0

    .line 3334
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    const v5, 0x7f02000a

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3340
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    .line 3341
    return-void

    .line 3335
    :cond_0
    iget v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mHoverPosition:I

    sub-int v4, v3, v4

    if-ge v4, v2, :cond_1

    .line 3336
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    const v5, 0x7f02000b

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 3338
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;->mSeekInfo:Landroid/view/View;

    const v5, 0x7f020009

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 3331
    :catch_0
    move-exception v4

    goto :goto_0
.end method
