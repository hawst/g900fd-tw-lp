.class Lcom/sec/android/mmapp/SoundPickerListFragment$11;
.super Ljava/lang/Object;
.source "SoundPickerListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;->makeSelectHeaderView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 998
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1002
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1004
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setAllItemChecked(Z)V

    .line 1008
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # invokes: Lcom/sec/android/mmapp/SoundPickerListFragment;->updateSelectedCount()V
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1000(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    .line 1009
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 1012
    :cond_0
    return-void

    .line 1006
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$11;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setAllItemChecked(Z)V

    goto :goto_0
.end method
