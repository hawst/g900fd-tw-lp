.class Lcom/sec/android/mmapp/library/SimpleMotionManager$1;
.super Ljava/lang/Object;
.source "SimpleMotionManager.java"

# interfaces
.implements Lcom/samsung/android/motion/MRListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/library/SimpleMotionManager;-><init>(Landroid/content/Context;Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/library/SimpleMotionManager;

.field final synthetic val$onMotionListener:Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/library/SimpleMotionManager;Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/mmapp/library/SimpleMotionManager$1;->this$0:Lcom/sec/android/mmapp/library/SimpleMotionManager;

    iput-object p2, p0, Lcom/sec/android/mmapp/library/SimpleMotionManager$1;->val$onMotionListener:Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 4
    .param p1, "motionEvent"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 138
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    .line 139
    .local v0, "motion":I
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SimpleMotionManager$1;->val$onMotionListener:Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;

    invoke-interface {v1, v0}, Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;->onMotionEvent(I)V

    .line 140
    # getter for: Lcom/sec/android/mmapp/library/SimpleMotionManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/library/SimpleMotionManager;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMotionListener motion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method
