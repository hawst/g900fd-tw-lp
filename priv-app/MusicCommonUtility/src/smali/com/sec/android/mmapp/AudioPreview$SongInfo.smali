.class Lcom/sec/android/mmapp/AudioPreview$SongInfo;
.super Ljava/lang/Object;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SongInfo"
.end annotation


# instance fields
.field private albumId:I

.field private filePath:Ljava/lang/String;

.field private isMidiFile:Z

.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;

.field private title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 3288
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/AudioPreview$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p2, "x1"    # Lcom/sec/android/mmapp/AudioPreview$1;

    .prologue
    .line 3288
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    .prologue
    .line 3288
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->isMidiFile:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 3288
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->isMidiFile:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    .prologue
    .line 3288
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3288
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    .prologue
    .line 3288
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3288
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    .prologue
    .line 3288
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/mmapp/AudioPreview$SongInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .param p1, "x1"    # I

    .prologue
    .line 3288
    iput p1, p0, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I

    return p1
.end method
