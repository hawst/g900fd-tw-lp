.class Lcom/sec/android/mmapp/AudioPreview$14;
.super Landroid/content/BroadcastReceiver;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 2001
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v9, 0xbb8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2004
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RemoteControllReceiver - onReceive() - action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2007
    .local v0, "action":Ljava/lang/String;
    const-string v4, "com.sec.android.mmapp.audiopreview.BECOMING_NOSIY"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2008
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 2109
    :cond_0
    :goto_0
    return-void

    .line 2009
    :cond_1
    const-string v4, "com.sec.android.mmapp.audiopreview.MEDIA_BUTTON"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2010
    const-string v4, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/KeyEvent;

    .line 2011
    .local v1, "event":Landroid/view/KeyEvent;
    if-eqz v1, :cond_0

    .line 2012
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 2013
    .local v3, "keyCode":I
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    .line 2015
    .local v2, "keyAction":I
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive() - event: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " keyCode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " keyAction: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018
    if-nez v2, :cond_6

    .line 2019
    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    .line 2022
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 2023
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 2025
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2026
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 2028
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v4, v5}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2029
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/sec/android/mmapp/AudioPreview;->access$4100(Lcom/sec/android/mmapp/AudioPreview;)Landroid/net/Uri;

    move-result-object v5

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v4, v5, v7}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_0

    .line 2035
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto/16 :goto_0

    .line 2038
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto/16 :goto_0

    .line 2041
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2042
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto/16 :goto_0

    .line 2044
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v4, v5}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2045
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/sec/android/mmapp/AudioPreview;->access$4100(Lcom/sec/android/mmapp/AudioPreview;)Landroid/net/Uri;

    move-result-object v5

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v4, v5, v7}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_0

    .line 2050
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2600(Lcom/sec/android/mmapp/AudioPreview;)V

    goto/16 :goto_0

    .line 2053
    :sswitch_5
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v4

    if-le v4, v9, :cond_5

    .line 2055
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v4, v8}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 2056
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;
    invoke-static {v5, v8}, Lcom/sec/android/mmapp/AudioPreview;->access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2057
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2061
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2500(Lcom/sec/android/mmapp/AudioPreview;)V

    goto/16 :goto_0

    .line 2065
    :sswitch_6
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2066
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v4

    const-string v5, "KEYCODE_MEDIA_REWIND"

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4, v7}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2068
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 2072
    :sswitch_7
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2073
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v4

    const-string v5, "KEYCODE_MEDIA_FAST_FORWARD"

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2074
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4, v7}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2075
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 2081
    :cond_6
    if-ne v2, v7, :cond_0

    .line 2082
    packed-switch v3, :pswitch_data_0

    goto/16 :goto_0

    .line 2084
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1500(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2085
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 2086
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z
    invoke-static {v4, v8}, Lcom/sec/android/mmapp/AudioPreview;->access$1502(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2088
    :cond_7
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J
    invoke-static {v6}, Lcom/sec/android/mmapp/AudioPreview;->access$2700(Lcom/sec/android/mmapp/AudioPreview;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x12c

    cmp-long v4, v4, v6

    if-gez v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2090
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v4

    if-le v4, v9, :cond_9

    .line 2092
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v4, v8}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 2093
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;
    invoke-static {v5, v8}, Lcom/sec/android/mmapp/AudioPreview;->access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2098
    :cond_8
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4, v8}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_0

    .line 2095
    :cond_9
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2500(Lcom/sec/android/mmapp/AudioPreview;)V

    goto :goto_1

    .line 2101
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$14;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v4, v8}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_0

    .line 2019
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_1
        0x57 -> :sswitch_4
        0x58 -> :sswitch_5
        0x59 -> :sswitch_6
        0x5a -> :sswitch_7
        0x7e -> :sswitch_3
        0x7f -> :sswitch_2
    .end sparse-switch

    .line 2082
    :pswitch_data_0
    .packed-switch 0x59
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
