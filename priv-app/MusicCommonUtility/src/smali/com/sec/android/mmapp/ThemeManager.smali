.class public Lcom/sec/android/mmapp/ThemeManager;
.super Ljava/lang/Object;
.source "ThemeManager.java"


# static fields
.field public static final THEME_BLACK:I = 0x1

.field public static final THEME_WHITE:I

.field private static sTheme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/mmapp/ThemeManager;->sTheme:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTheme()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lcom/sec/android/mmapp/ThemeManager;->sTheme:I

    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/mmapp/ThemeManager;->sTheme:I

    .line 18
    return-void
.end method
