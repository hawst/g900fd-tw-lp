.class public Lcom/sec/android/mmapp/MMAppApplication;
.super Landroid/app/Application;
.source "MMAppApplication.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 8
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 9
    invoke-static {p0}, Lcom/sec/android/mmapp/ThemeManager;->init(Landroid/content/Context;)V

    .line 10
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MMAppApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/MMAppApplication;->setTheme(I)V

    .line 11
    return-void
.end method
