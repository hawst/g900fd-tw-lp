.class final Lcom/sec/android/mmapp/util/AlbumArtLoader$1;
.super Landroid/os/Handler;
.source "AlbumArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/AlbumArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Looper;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 76
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_1

    .line 77
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;

    .line 78
    .local v0, "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    iget-object v3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->iv:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;

    .line 79
    .local v2, "args":Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;
    iget-wide v4, v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    iget-wide v6, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->albumId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 80
    iget-object v3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->d:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_0

    .line 81
    iget-object v3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->context:Landroid/content/Context;

    iget-wide v4, v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    invoke-static {v3, v4, v5}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->getDefaultDrawableImage(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->d:Landroid/graphics/drawable/Drawable;

    .line 83
    :cond_0
    iget-object v3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->iv:Landroid/widget/ImageView;

    iget-object v4, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 85
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 86
    .local v1, "anim":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 87
    iget-object v3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->iv:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 90
    .end local v0    # "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    .end local v1    # "anim":Landroid/view/animation/AlphaAnimation;
    .end local v2    # "args":Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;
    :cond_1
    return-void
.end method
