.class Lcom/sec/android/mmapp/SetAsActivity$1;
.super Ljava/lang/Object;
.source "SetAsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/SetAsActivity;->showChooseSimCardDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SetAsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SetAsActivity;)V
    .locals 0

    .prologue
    .line 613
    iput-object p1, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "optionPopupDialogId"    # Landroid/content/DialogInterface;
    .param p2, "position"    # I

    .prologue
    .line 617
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showChooseSimCardDialog position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    if-nez p2, :cond_1

    .line 620
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$000(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 623
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/mmapp/SetAsActivity;->access$100(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "recommendation_time"

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$200(Lcom/sec/android/mmapp/SetAsActivity;)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 637
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "DEBUG_RINGTONE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MusicPlayer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/sec/android/mmapp/SetAsActivity;->access$000(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 640
    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SetAsActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDualModeRingtone() - PhoneRingtone - recommendation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$100(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$000(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOffset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$200(Lcom/sec/android/mmapp/SetAsActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    const v2, 0x7f0c0040

    invoke-virtual {v1, v2}, Lcom/sec/android/mmapp/SetAsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 646
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V

    .line 647
    return-void

    .line 628
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$000(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 631
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/mmapp/SetAsActivity;->access$100(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "recommendation_time_2"

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity$1;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SetAsActivity;->access$200(Lcom/sec/android/mmapp/SetAsActivity;)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0
.end method
