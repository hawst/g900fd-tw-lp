.class Lcom/sec/android/mmapp/AudioPreview$1;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/AudioPreview;->initiateView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 664
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v4, 0x0

    .line 667
    if-nez p2, :cond_1

    .line 668
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 669
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0, v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 670
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1500(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z
    invoke-static {v0, v4}, Lcom/sec/android/mmapp/AudioPreview;->access$1502(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 674
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mDownTime:J
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1800(Lcom/sec/android/mmapp/AudioPreview;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x12c

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 675
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    const/16 v1, 0xbb8

    if-le v0, v1, :cond_2

    .line 677
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v0, v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;
    invoke-static {v1, v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 688
    :cond_1
    :goto_0
    return-void

    .line 683
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$1;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2500(Lcom/sec/android/mmapp/AudioPreview;)V

    goto :goto_0
.end method
