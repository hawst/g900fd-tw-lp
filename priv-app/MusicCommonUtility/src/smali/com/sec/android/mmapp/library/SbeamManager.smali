.class public Lcom/sec/android/mmapp/library/SbeamManager;
.super Ljava/lang/Object;
.source "SbeamManager.java"


# static fields
.field private static final ABEAM_MODE_NORMAL:I = 0x6

.field private static final ACTION_DIRECT_SHARE:Ljava/lang/String; = "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

.field private static final ACTION_DIRECT_SHARE_HELP_POPUP:Ljava/lang/String; = "com.sec.android.directshare.DirectSharePopUp"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final EXTRA_POPUP_MODE:Ljava/lang/String; = "POPUP_MODE"

.field private static final EXTRA_SBEAM_CLOUD:Ljava/lang/String; = "from_cloud_file"

.field private static final EXTRA_SBEAM_DRM:Ljava/lang/String; = "from_drm_file"

.field private static final EXTRA_SBEAM_NO_FILE:Ljava/lang/String; = "no_file_selected"

.field private static final EXTRA_SBEAM_OFF:Ljava/lang/String; = "s_beam_off"

.field private static final GLOBAL_SETTINGS_PREFERENCE_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field private static final MIME_TYPE_DIRECT_SHARE_MUSIC:Ljava/lang/String; = "text/DirectShareMusic"

.field private static final PREF_KEY_SBEAM:Ljava/lang/String; = "SBeam_on_off"

.field private static final PREF_SBEAM_NAME:Ljava/lang/String; = "pref_sbeam"

.field private static final SBEAM_APPLICATION_PACKAGE:Ljava/lang/String; = "com.sec.android.directshare"

.field private static final SBEAM_MODE_CLOUD:I = 0x5

.field private static final SBEAM_MODE_DRM:I = 0x4

.field private static final SBEAM_MODE_NORMAL:I = 0x1

.field private static final SBEAM_MODE_NO_FILE:I = 0x3

.field private static final SBEAM_MODE_OFF:I = 0x2

.field private static final STREAM_PATH:Ljava/lang/String; = "http"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

.field private mMode:I

.field private mPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/mmapp/library/SbeamManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 4
    .param p1, "a"    # Landroid/app/Activity;

    .prologue
    const/4 v3, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 119
    .local v0, "nfcAdapter":Landroid/nfc/NfcAdapter;
    if-nez v0, :cond_0

    .line 120
    sget-object v1, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "This device does not support NFC"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :goto_0
    return-void

    .line 123
    :cond_0
    iput-object p1, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;

    .line 124
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/mmapp/library/DrmManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/DrmManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

    .line 127
    new-instance v1, Lcom/sec/android/mmapp/library/SbeamManager$1;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/mmapp/library/SbeamManager$1;-><init>(Lcom/sec/android/mmapp/library/SbeamManager;Landroid/nfc/NfcAdapter;)V

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p1, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 167
    new-instance v1, Lcom/sec/android/mmapp/library/SbeamManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mmapp/library/SbeamManager$2;-><init>(Lcom/sec/android/mmapp/library/SbeamManager;)V

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p1, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/library/SbeamManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/mmapp/library/SbeamManager;->isEnableSBeam()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/library/SbeamManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/library/SbeamManager;Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/library/SbeamManager;->createMimeRecord(Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/library/SbeamManager;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/library/SbeamManager;)Lcom/sec/android/mmapp/library/DrmManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/mmapp/library/SbeamManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/mmapp/library/SbeamManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SbeamManager;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    return p1
.end method

.method private createMimeRecord(Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;
    .locals 8
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 234
    new-array v3, v6, [Ljava/lang/String;

    aput-object p2, v3, v5

    invoke-direct {p0, p1, v3}, Lcom/sec/android/mmapp/library/SbeamManager;->getDirectShareMessage(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 239
    .local v1, "pathWithMac":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/mmapp/library/SbeamManager;->isEnableSBeam()Z

    move-result v3

    if-nez v3, :cond_0

    .line 240
    iput v7, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    .line 241
    const-string v3, "s_beam_off"

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/library/SbeamManager;->startDirectShareHelpPopup(Ljava/lang/String;)V

    .line 264
    :goto_0
    return-object v2

    .line 243
    :cond_0
    const-string v3, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 244
    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    .line 245
    const-string v3, "no_file_selected"

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/library/SbeamManager;->startDirectShareHelpPopup(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_1
    if-eqz p2, :cond_2

    const-string v3, "http"

    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 250
    const/4 v3, 0x5

    iput v3, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    .line 251
    const-string v3, "from_cloud_file"

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/library/SbeamManager;->startDirectShareHelpPopup(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

    iget-object v4, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/mmapp/library/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 254
    const/4 v3, 0x4

    iput v3, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    .line 255
    const-string v3, "from_drm_file"

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/library/SbeamManager;->startDirectShareHelpPopup(Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :cond_3
    iput v6, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    .line 259
    const-string p1, "text/DirectShareMusic"

    .line 260
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 263
    .local v0, "message":[B
    sget-object v2, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createMimeRecord mMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v2, Landroid/nfc/NdefRecord;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    new-array v4, v5, [B

    invoke-direct {v2, v7, v3, v4, v0}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    goto :goto_0
.end method

.method private getDirectShareMessage(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "filePath"    # [Ljava/lang/String;

    .prologue
    .line 287
    const-string v11, ""

    .line 288
    .local v11, "ret":Ljava/lang/String;
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 290
    .local v9, "obj":Lorg/json/JSONObject;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;

    const-string v16, "wifi"

    invoke-virtual/range {v15 .. v16}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/wifi/WifiManager;

    .line 292
    .local v14, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v14}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v13

    .line 293
    .local v13, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v13, :cond_0

    .line 294
    invoke-virtual {v13}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x2

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 295
    .local v6, "firstMac":Ljava/lang/String;
    invoke-virtual {v13}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x2

    invoke-virtual {v13}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 297
    .local v8, "lastMac":Ljava/lang/String;
    const/4 v15, 0x0

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    const-string v16, "0"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 298
    const-string v15, "mac"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "0"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v6, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    or-int/lit8 v17, v17, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 306
    .end local v6    # "firstMac":Ljava/lang/String;
    .end local v8    # "lastMac":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string v15, "mimeType"

    move-object/from16 v0, p1

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 307
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 309
    .local v3, "_list":Lorg/json/JSONArray;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p2

    array-length v15, v0

    if-le v15, v7, :cond_2

    .line 310
    new-instance v2, Ljava/io/File;

    aget-object v15, p2, v7

    invoke-direct {v2, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    .local v2, "_f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    .line 312
    .local v12, "temp":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    .line 313
    .local v5, "exPath":Ljava/io/File;
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 314
    .local v10, "obj2":Lorg/json/JSONObject;
    const-string v15, "fileName"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 315
    const-string v15, "subPath"

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 316
    const-string v15, "fileLen"

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v10, v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 317
    const-string v15, "filepath"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 318
    invoke-virtual {v3, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 309
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 302
    .end local v2    # "_f":Ljava/io/File;
    .end local v3    # "_list":Lorg/json/JSONArray;
    .end local v5    # "exPath":Ljava/io/File;
    .end local v7    # "i":I
    .end local v10    # "obj2":Lorg/json/JSONObject;
    .end local v12    # "temp":Ljava/lang/String;
    .restart local v6    # "firstMac":Ljava/lang/String;
    .restart local v8    # "lastMac":Ljava/lang/String;
    :cond_1
    const-string v15, "mac"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v6, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    or-int/lit8 v17, v17, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 322
    .end local v6    # "firstMac":Ljava/lang/String;
    .end local v8    # "lastMac":Ljava/lang/String;
    .end local v13    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v14    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v4

    .line 324
    .local v4, "e":Ljava/lang/NumberFormatException;
    sget-object v15, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    .end local v11    # "ret":Ljava/lang/String;
    :goto_2
    return-object v11

    .line 320
    .restart local v3    # "_list":Lorg/json/JSONArray;
    .restart local v7    # "i":I
    .restart local v11    # "ret":Ljava/lang/String;
    .restart local v13    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v14    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_2
    :try_start_1
    const-string v15, "list"

    invoke-virtual {v9, v15, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 321
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v11

    goto :goto_2

    .line 325
    .end local v3    # "_list":Lorg/json/JSONArray;
    .end local v7    # "i":I
    .end local v13    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v14    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_1
    move-exception v4

    .line 327
    .local v4, "e":Lorg/json/JSONException;
    sget-object v15, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 328
    .end local v4    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v4

    .line 330
    .local v4, "e":Ljava/lang/NullPointerException;
    sget-object v15, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private isEnableSBeam()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 202
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;

    const-string v5, "com.android.settings"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 203
    .local v0, "context":Landroid/content/Context;
    const-string v4, "pref_sbeam"

    const/4 v5, 0x5

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 205
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v4, "SBeam_on_off"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 211
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    :goto_0
    return v3

    .line 206
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v4, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/SecurityException;
    sget-object v4, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startDirectShareHelpPopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 273
    sget-object v1, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startDirectShareHelpPopup - extra: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "POPUP_MODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 277
    return-void
.end method


# virtual methods
.method public setFilePath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 224
    sget-object v0, Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFilePath - path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iput-object p1, p0, Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;

    .line 226
    return-void
.end method
