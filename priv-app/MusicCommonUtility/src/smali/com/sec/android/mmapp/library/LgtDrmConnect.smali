.class Lcom/sec/android/mmapp/library/LgtDrmConnect;
.super Ljava/lang/Object;
.source "LgtDrmConnect.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static CONTENT_RO:I = 0x0

.field private static final NAME_VALUE_SEPARATOR:Ljava/lang/String; = "="

.field private static final PARAMETER_SEPARATOR:Ljava/lang/String; = "&"

.field public static PARENT_RO:I = 0x0

.field private static ROServerUrl:[Ljava/lang/String; = null

.field public static final RO_AIRPLANE:I = -0x2

.field public static final RO_ERROR:I = -0x1

.field public static final RO_NOT_REGISTERED_USER:I = -0x3

.field public static final RO_REGISTERED:I = 0x0

.field private static WAPServerUrl:[Ljava/lang/String; = null

.field static final digits:Ljava/lang/String; = "0123456789ABCDEF"

.field private static sSelectServer:I


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mIsActivity:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 58
    const-class v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CLASSNAME:Ljava/lang/String;

    .line 66
    sput v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CONTENT_RO:I

    .line 68
    sput v2, Lcom/sec/android/mmapp/library/LgtDrmConnect;->PARENT_RO:I

    .line 223
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "http://211.115.75.161:6708/musicon/ngb.NgbRo.web"

    aput-object v1, v0, v3

    const-string v1, "http://www.musicon.co.kr/musicon/ngb.NgbRo.web"

    aput-object v1, v0, v2

    const-string v1, "http://203.248.248.176/musicon/ngb.NgbRo.web"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    .line 229
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "wapurl://211.115.75.161:6708/musicon/wap.WAPMain.web"

    aput-object v1, v0, v3

    const-string v1, "wapurl://wapstore.musicon.co.kr/musicon/wap.WAPMain.web"

    aput-object v1, v0, v2

    const-string v1, "wapurl://203.248.248.176/musicon/wap.WAPMain.web"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->WAPServerUrl:[Ljava/lang/String;

    .line 236
    sput v2, Lcom/sec/android/mmapp/library/LgtDrmConnect;->sSelectServer:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-class v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->TAG:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mIsActivity:Z

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mDialog:Landroid/app/AlertDialog;

    .line 87
    iput-object p1, p0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mContext:Landroid/content/Context;

    .line 93
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 94
    return-void
.end method

.method public static checkAirplaneMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 254
    const/4 v1, 0x0

    .line 257
    .local v1, "isAirplaneMode":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 263
    :goto_0
    if-ne v1, v2, :cond_0

    .line 266
    :goto_1
    return v2

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static checkRoaming(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 240
    const/4 v2, 0x0

    .line 241
    .local v2, "roamingArea":Z
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 245
    .local v1, "mPhone":Landroid/telephony/TelephonyManager;
    :try_start_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 250
    :goto_0
    return v2

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 350
    const/4 v1, 0x0

    .line 352
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    const-string v2, "ISO-8859-1"

    invoke-static {p0, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "%3D"

    const-string v4, "="

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 356
    :goto_0
    return-object v1

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public static format(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/http/NameValuePair;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 335
    .local p0, "parameters":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/http/NameValuePair;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    .local v4, "result":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/http/NameValuePair;

    .line 337
    .local v3, "parameter":Lorg/apache/http/NameValuePair;
    invoke-interface {v3}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mmapp/library/LgtDrmConnect;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "encodedName":Ljava/lang/String;
    invoke-interface {v3}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 339
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/sec/android/mmapp/library/LgtDrmConnect;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    .local v1, "encodedValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 341
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    :cond_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    const-string v6, "="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 339
    .end local v1    # "encodedValue":Ljava/lang/String;
    :cond_1
    const-string v1, ""

    goto :goto_1

    .line 346
    .end local v0    # "encodedName":Ljava/lang/String;
    .end local v3    # "parameter":Lorg/apache/http/NameValuePair;
    .end local v5    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getHtmlString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "devId"    # Ljava/lang/String;

    .prologue
    .line 206
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "htmlDev":Ljava/lang/String;
    const-string v1, "&lt;"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    const-string v1, "&lt;"

    const-string v2, "<"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    :cond_0
    const-string v1, "&gt;"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    const-string v1, "&gt;"

    const-string v2, ">"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    :cond_1
    const-string v1, "&quot;"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 214
    const-string v1, "&quot;"

    const-string v2, "\""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    :cond_2
    const-string v1, "&nbsp;"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 217
    const-string v1, "&nbsp;"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :cond_3
    return-object v0
.end method

.method public static getMusicONROURL(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    .line 272
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    sget v5, Lcom/sec/android/mmapp/library/LgtDrmConnect;->sSelectServer:I

    aget-object v1, v4, v5

    .line 273
    .local v1, "url":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "lgt_musicOn_serverUrl"

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 275
    .local v3, "userUrl":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 277
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UserURL : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v4, "http://211.115.75.161"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 280
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v1, v4, v5

    move-object v2, v1

    .line 304
    .end local v1    # "url":Ljava/lang/String;
    .local v2, "url":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 282
    .end local v2    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "http://www.musicon.co.kr"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 283
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    aget-object v1, v4, v7

    move-object v2, v1

    .line 284
    .end local v1    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 285
    .end local v2    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    :cond_1
    const-string v4, "http://203.248.248.176"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 286
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v1, v4, v5

    move-object v2, v1

    .line 287
    .end local v1    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 291
    .end local v2    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    :cond_2
    invoke-static {v1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 292
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    aget-object v1, v4, v7

    .line 296
    :cond_3
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :goto_1
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MusicON url : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 304
    .end local v1    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 297
    .end local v2    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/net/MalformedURLException;
    sget-object v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->ROServerUrl:[Ljava/lang/String;

    aget-object v1, v4, v7

    .line 299
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getMusicONWAPURL(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 308
    sget-object v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->WAPServerUrl:[Ljava/lang/String;

    sget v4, Lcom/sec/android/mmapp/library/LgtDrmConnect;->sSelectServer:I

    aget-object v0, v3, v4

    .line 309
    .local v0, "url":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "lgt_musicOn_serverUrl"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, "userUrl":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 313
    sget-object v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UserURL : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const-string v3, "http://211.115.75.161"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 316
    sget-object v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->WAPServerUrl:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v0, v3, v4

    move-object v1, v0

    .line 328
    .end local v0    # "url":Ljava/lang/String;
    .local v1, "url":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 318
    .end local v1    # "url":Ljava/lang/String;
    .restart local v0    # "url":Ljava/lang/String;
    :cond_0
    const-string v3, "http://www.musicon.co.kr"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 319
    sget-object v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->WAPServerUrl:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v0, v3, v4

    move-object v1, v0

    .line 320
    .end local v0    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    goto :goto_0

    .line 321
    .end local v1    # "url":Ljava/lang/String;
    .restart local v0    # "url":Ljava/lang/String;
    :cond_1
    const-string v3, "http://203.248.248.176"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 322
    sget-object v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->WAPServerUrl:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v0, v3, v4

    move-object v1, v0

    .line 323
    .end local v0    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    goto :goto_0

    .line 327
    .end local v1    # "url":Ljava/lang/String;
    .restart local v0    # "url":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MusicON url : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 328
    .end local v0    # "url":Ljava/lang/String;
    .restart local v1    # "url":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public post(Lcom/sec/android/mmapp/library/LgtDrmManager;ILjava/util/ArrayList;Ljava/lang/String;)I
    .locals 28
    .param p1, "dm"    # Lcom/sec/android/mmapp/library/LgtDrmManager;
    .param p2, "roType"    # I
    .param p4, "contId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/mmapp/library/LgtDrmManager;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 97
    .local p3, "msgArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/sec/android/mmapp/library/LgtDrmConnect;->checkAirplaneMode(Landroid/content/Context;)Z

    move-result v23

    if-nez v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/sec/android/mmapp/library/LgtDrmConnect;->checkRoaming(Landroid/content/Context;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 98
    :cond_0
    const/16 v20, -0x2

    .line 202
    :cond_1
    :goto_0
    return v20

    .line 100
    :cond_2
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v23

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_3

    .line 101
    const/16 v20, -0x1

    goto :goto_0

    .line 104
    :cond_3
    const/16 v20, -0x1

    .line 105
    .local v20, "returnValue":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->TAG:Ljava/lang/String;

    move-object/from16 v24, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "devId : "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v23, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " roType : "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " ContId : "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    new-instance v15, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/sec/android/mmapp/library/LgtDrmConnect;->getMusicONROURL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v15, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 108
    .local v15, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    new-instance v14, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v14}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 109
    .local v14, "httpParams":Lorg/apache/http/params/HttpParams;
    const/16 v23, 0x2710

    move/from16 v0, v23

    invoke-static {v14, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 110
    const/16 v23, 0x2710

    move/from16 v0, v23

    invoke-static {v14, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 111
    new-instance v13, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v13, v14}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 112
    .local v13, "httpClient":Lorg/apache/http/client/HttpClient;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const-string v24, "phone"

    invoke-virtual/range {v23 .. v24}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/telephony/TelephonyManager;

    .line 114
    .local v22, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v22 .. v22}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v16

    .line 116
    .local v16, "number":Ljava/lang/String;
    if-eqz v16, :cond_7

    .line 118
    const/16 v23, 0x0

    const/16 v24, 0x3

    :try_start_0
    move-object/from16 v0, v16

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const-string v24, "+82"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 119
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "0"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v24, 0x3

    const/16 v25, 0x5

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "0"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v24, 0x5

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 130
    .local v3, "ctn":Ljava/lang/String;
    :goto_1
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v17, "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/message/BasicNameValuePair;>;"
    sget v23, Lcom/sec/android/mmapp/library/LgtDrmConnect;->CONTENT_RO:I

    move/from16 v0, p2

    move/from16 v1, v23

    if-ne v0, v1, :cond_8

    .line 132
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "CMD"

    const-string v25, "CN_CONTENT_RO"

    invoke-direct/range {v23 .. v25}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "phoneNumber"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "dcid"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "proxy"

    const-string v25, "false"

    invoke-direct/range {v23 .. v25}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "deviceURI"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v24, Lorg/apache/http/message/BasicNameValuePair;

    const-string v25, "deviceID"

    const/16 v23, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "isRegistered"

    const-string v25, "Y"

    invoke-direct/range {v23 .. v25}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->TAG:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "LGT param : CN_CONTENT_RO, ctn : %s, dcid : %s, deviceURI : %s, DeviceID : %s"

    const/16 v25, 0x4

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v3, v25, v26

    const/16 v26, 0x1

    aput-object p4, v25, v26

    const/16 v26, 0x2

    aput-object v3, v25, v26

    const/16 v26, 0x3

    const/16 v27, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_4
    :goto_2
    :try_start_1
    new-instance v7, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    .line 159
    .local v7, "ent":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    invoke-virtual {v15, v7}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 160
    invoke-interface {v13, v15}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v19

    .line 161
    .local v19, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    .line 163
    .local v8, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v8, :cond_d

    .line 164
    invoke-static {v8}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v18

    .line 165
    .local v18, "res":Ljava/lang/String;
    const-string v23, "<ErrorCode>"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v23

    const-string v24, "<ErrorCode>"

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    add-int v11, v23, v24

    .line 166
    .local v11, "errorStart":I
    const-string v23, "</ErrorCode>"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/NetworkOnMainThreadException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v10

    .line 167
    .local v10, "errorEnd":I
    if-ltz v11, :cond_5

    if-gez v10, :cond_a

    .line 168
    :cond_5
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 121
    .end local v3    # "ctn":Ljava/lang/String;
    .end local v7    # "ent":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "errorEnd":I
    .end local v11    # "errorStart":I
    .end local v17    # "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/message/BasicNameValuePair;>;"
    .end local v18    # "res":Ljava/lang/String;
    .end local v19    # "response":Lorg/apache/http/HttpResponse;
    :cond_6
    :try_start_2
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v24, 0x0

    const/16 v25, 0x3

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "0"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v24, 0x3

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    .restart local v3    # "ctn":Ljava/lang/String;
    goto/16 :goto_1

    .line 123
    .end local v3    # "ctn":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 124
    .local v5, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 127
    .end local v5    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_7
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 143
    .restart local v3    # "ctn":Ljava/lang/String;
    .restart local v17    # "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/message/BasicNameValuePair;>;"
    :cond_8
    sget v23, Lcom/sec/android/mmapp/library/LgtDrmConnect;->PARENT_RO:I

    move/from16 v0, p2

    move/from16 v1, v23

    if-ne v0, v1, :cond_4

    .line 144
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v23

    const/16 v24, 0x3

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_9

    .line 145
    const-string v23, "LGT_DRM_OFFER_000002"

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_9
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "CMD"

    const-string v25, "CN_PARENT_RO"

    invoke-direct/range {v23 .. v25}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "phoneNumber"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "dcid"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "proxy"

    const-string v25, "false"

    invoke-direct/range {v23 .. v25}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "deviceURI"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    new-instance v24, Lorg/apache/http/message/BasicNameValuePair;

    const-string v25, "deviceID"

    const/16 v23, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v24, Lorg/apache/http/message/BasicNameValuePair;

    const-string v25, "offerId"

    const/16 v23, 0x2

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v23, Lorg/apache/http/message/BasicNameValuePair;

    const-string v24, "isRegistered"

    const-string v25, "Y"

    invoke-direct/range {v23 .. v25}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 171
    .restart local v7    # "ent":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v10    # "errorEnd":I
    .restart local v11    # "errorStart":I
    .restart local v18    # "res":Ljava/lang/String;
    .restart local v19    # "response":Lorg/apache/http/HttpResponse;
    :cond_a
    :try_start_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 172
    .local v9, "error":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->TAG:Ljava/lang/String;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "ErrorCode : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v23, "0000"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 174
    const-string v23, "&lt;roap:roResponse "

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    .line 175
    .local v21, "start":I
    const-string v23, "&lt;/roap:roResponse&gt;"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v23

    const-string v24, "&lt;/roap:roResponse&gt;"

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    add-int v6, v23, v24

    .line 177
    .local v6, "end":I
    if-ltz v21, :cond_b

    if-gez v6, :cond_c

    .line 178
    :cond_b
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 180
    :cond_c
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 181
    .local v4, "data":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/mmapp/library/LgtDrmConnect;->getHtmlString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 182
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/sec/android/mmapp/library/LgtDrmManager;->handleRoap(Ljava/lang/String;)I
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/os/NetworkOnMainThreadException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v12

    .line 183
    .local v12, "handleError":I
    if-nez v12, :cond_d

    .line 184
    const/16 v20, 0x0

    .line 198
    .end local v4    # "data":Ljava/lang/String;
    .end local v6    # "end":I
    .end local v7    # "ent":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v9    # "error":Ljava/lang/String;
    .end local v10    # "errorEnd":I
    .end local v11    # "errorStart":I
    .end local v12    # "handleError":I
    .end local v18    # "res":Ljava/lang/String;
    .end local v19    # "response":Lorg/apache/http/HttpResponse;
    .end local v21    # "start":I
    :cond_d
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mIsActivity:Z

    move/from16 v23, v0

    if-eqz v23, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/app/AlertDialog;->dismiss()V

    .line 200
    const/16 v23, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mmapp/library/LgtDrmConnect;->mDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 186
    .restart local v7    # "ent":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v9    # "error":Ljava/lang/String;
    .restart local v10    # "errorEnd":I
    .restart local v11    # "errorStart":I
    .restart local v18    # "res":Ljava/lang/String;
    .restart local v19    # "response":Lorg/apache/http/HttpResponse;
    :cond_e
    :try_start_4
    const-string v23, "1124"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/os/NetworkOnMainThreadException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v23

    if-eqz v23, :cond_d

    .line 187
    const/16 v20, -0x3

    goto/16 :goto_0

    .line 190
    .end local v7    # "ent":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v9    # "error":Ljava/lang/String;
    .end local v10    # "errorEnd":I
    .end local v11    # "errorStart":I
    .end local v18    # "res":Ljava/lang/String;
    .end local v19    # "response":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v5

    .line 191
    .local v5, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v5}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_3

    .line 192
    .end local v5    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v5

    .line 193
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 194
    .end local v5    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 195
    .local v5, "e":Landroid/os/NetworkOnMainThreadException;
    invoke-virtual {v5}, Landroid/os/NetworkOnMainThreadException;->printStackTrace()V

    goto :goto_3
.end method
