.class public Lcom/sec/android/mmapp/MusicSetAsAlertActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "MusicSetAsAlertActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;,
        Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static IDX_ALARM_TONE:I

.field private static IDX_CALL_RINGTONE:I

.field private static IDX_INDIVIDUAL_RINGTONE:I


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mDoFinishWhenClickItem:Z

.field private mGrid:Landroid/widget/GridView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedUri:Landroid/net/Uri;

.field private mTitle:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->CLASSNAME:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_CALL_RINGTONE:I

    .line 56
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_INDIVIDUAL_RINGTONE:I

    .line 58
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_ALARM_TONE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mDoFinishWhenClickItem:Z

    .line 193
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private getFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v2, v4

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 254
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 256
    .local v7, "path":Ljava/lang/String;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 260
    :cond_0
    if-eqz v6, :cond_1

    .line 261
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 264
    :cond_1
    return-object v7

    .line 260
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 261
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private hasAvailableApp(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isVoiceCapable()Z
    .locals 2

    .prologue
    .line 131
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 132
    .local v0, "telephony":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/sec/android/mmapp/library/TelephonyManagerCompat;->isVoiceCapable(Landroid/telephony/TelephonyManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setRingTone(Landroid/net/Uri;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 268
    if-eqz p1, :cond_0

    .line 269
    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "DEBUG_RINGTONE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MusicPlayer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 274
    :cond_0
    const v0, 0x7f0c0040

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 275
    return-void
.end method

.method private setRingtone(I)V
    .locals 6
    .param p1, "menuId"    # I

    .prologue
    const/4 v5, 0x0

    .line 200
    iget-object v2, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mSelectedUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 221
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v1, "values":Landroid/content/ContentValues;
    :try_start_0
    sget v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_INDIVIDUAL_RINGTONE:I

    if-eq p1, v2, :cond_1

    sget v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_CALL_RINGTONE:I

    if-ne p1, v2, :cond_3

    .line 207
    :cond_1
    const-string v2, "is_ringtone"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 217
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mSelectedUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 220
    iget-object v2, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mSelectedUri:Landroid/net/Uri;

    invoke-direct {p0, p1, v2}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->showSetAsResult(ILandroid/net/Uri;)V

    goto :goto_0

    .line 208
    :cond_3
    :try_start_1
    sget v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_ALARM_TONE:I

    if-ne p1, v2, :cond_2

    .line 209
    const-string v2, "is_alarm"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 211
    :catch_0
    move-exception v0

    .line 212
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 213
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 214
    .local v0, "ex":Ljava/lang/UnsupportedOperationException;
    sget-object v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UnsupportedOperationException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private showSetAsResult(ILandroid/net/Uri;)V
    .locals 5
    .param p1, "menuId"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 224
    sget v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_CALL_RINGTONE:I

    if-ne p1, v2, :cond_1

    .line 225
    invoke-direct {p0, p2}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->setRingTone(Landroid/net/Uri;)V

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    sget v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_INDIVIDUAL_RINGTONE:I

    if-ne p1, v2, :cond_2

    .line 227
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "ringtone_uri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string v2, "vnd.android.cursor.item/contact"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not found contact application :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    sget v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_ALARM_TONE:I

    if-ne p1, v2, :cond_0

    .line 238
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "alarm://com.sec.android.app.clockpackage/alarmlist/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 241
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v2, "alarm_uri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 244
    :catch_1
    move-exception v0

    .line 245
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not found alarm application :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 150
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mTitle:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Alert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f020010

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 62
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 64
    .local v1, "data":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 66
    const-string v4, "uri"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    iput-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mSelectedUri:Landroid/net/Uri;

    .line 67
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mSelectedUri:Landroid/net/Uri;

    if-nez v4, :cond_0

    .line 68
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Did you miss MusicIntent.EXTRA_URI as extra value?"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 72
    :cond_0
    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    iput-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 73
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    .line 75
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 76
    .local v3, "i":Landroid/content/Intent;
    const-string v4, "vnd.android.cursor.item/contact"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->hasAvailableApp(Landroid/content/Intent;)Z

    move-result v2

    .line 79
    .local v2, "hasContact":Z
    invoke-direct {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->isVoiceCapable()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 80
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    const v5, 0x7f0c0036

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    const v5, 0x7f020012

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    if-eqz v2, :cond_1

    .line 83
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    const v5, 0x7f0c0035

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    const v5, 0x7f020011

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    const v5, 0x7f0c0034

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    sput v6, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_CALL_RINGTONE:I

    .line 89
    if-eqz v2, :cond_3

    .line 90
    sput v8, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_INDIVIDUAL_RINGTONE:I

    .line 91
    sput v9, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_ALARM_TONE:I

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    .line 107
    .local v0, "ap":Lcom/android/internal/app/AlertController$AlertParams;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0037

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 108
    iget-object v4, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    iput-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mTitle:Ljava/lang/CharSequence;

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040012

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    .line 110
    iget-object v4, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    const v5, 0x7f0f0036

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/GridView;

    iput-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mGrid:Landroid/widget/GridView;

    .line 111
    new-instance v4, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;

    invoke-direct {v4, p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;-><init>(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)V

    iput-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mAdapter:Landroid/widget/BaseAdapter;

    .line 112
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mGrid:Landroid/widget/GridView;

    iget-object v5, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 113
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v4, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 115
    invoke-direct {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->isVoiceCapable()Z

    move-result v4

    if-nez v4, :cond_2

    .line 116
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v4, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 117
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v4, v9}, Landroid/widget/GridView;->setStretchMode(I)V

    .line 120
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->setupAlert()V

    .line 121
    return-void

    .line 93
    .end local v0    # "ap":Lcom/android/internal/app/AlertController$AlertParams;
    :cond_3
    sput v7, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_INDIVIDUAL_RINGTONE:I

    .line 94
    sput v8, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_ALARM_TONE:I

    goto :goto_0

    .line 97
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    const v5, 0x7f0c0034

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v4, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    sput v7, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_CALL_RINGTONE:I

    .line 100
    sput v7, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_INDIVIDUAL_RINGTONE:I

    .line 101
    sput v6, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->IDX_ALARM_TONE:I

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mDoFinishWhenClickItem:Z

    .line 138
    invoke-direct {p0, p3}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->setRingtone(I)V

    .line 141
    iget-boolean v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mDoFinishWhenClickItem:Z

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->finish()V

    .line 144
    :cond_0
    return-void
.end method
