.class Lcom/sec/android/mmapp/AudioPreview$Player;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Player"
.end annotation


# static fields
.field private static final DEFUALT_MAX_VOLUME:F = 1.0f

.field private static final MP_HANDLER_FADEIN:I = 0x0

.field private static final MP_HANDLER_UPDATE_PROGRESS:I = 0x1


# instance fields
.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mDoPlay:Z

.field private mDuration:I

.field private mMaxVolume:F

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private final mMpHandler:Landroid/os/Handler;

.field private mPausedByTransientLossOfFocus:Z

.field private mToStartByAudioFocusGain:Z

.field private mVolumeIncrement:F

.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method public constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2143
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z

    .line 2125
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mToStartByAudioFocusGain:Z

    .line 2503
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$Player$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$Player$1;-><init>(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2569
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$Player$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$Player$2;-><init>(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    .line 2144
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2145
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2147
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2148
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2149
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 2150
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2152
    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {p1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2153
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/mmapp/AudioPreview$Player;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 2112
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Z

    .prologue
    .line 2112
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->start()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/AudioPreview$Player;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mToStartByAudioFocusGain:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # Z

    .prologue
    .line 2112
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mToStartByAudioFocusGain:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # I

    .prologue
    .line 2112
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/AudioPreview$Player;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # Z

    .prologue
    .line 2112
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$Player;->start(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/mmapp/AudioPreview$Player;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->reset()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->release()V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/android/mmapp/AudioPreview$Player;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$5802(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # Z

    .prologue
    .line 2112
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$5900(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/android/mmapp/AudioPreview$Player;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 2112
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview$Player;->start(ZZ)V

    return-void
.end method

.method static synthetic access$6300(Lcom/sec/android/mmapp/AudioPreview$Player;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mVolumeIncrement:F

    return v0
.end method

.method static synthetic access$6400(Lcom/sec/android/mmapp/AudioPreview$Player;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    return v0
.end method

.method static synthetic access$6600(Lcom/sec/android/mmapp/AudioPreview$Player;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getPositionOfSeekBar()I

    move-result v0

    return v0
.end method

.method static synthetic access$6800(Lcom/sec/android/mmapp/AudioPreview$Player;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$Player;

    .prologue
    .line 2112
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getDuration()I

    move-result v0

    return v0
.end method

.method private canPlayState()Z
    .locals 2

    .prologue
    .line 2416
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I

    move-result v0

    .line 2417
    .local v0, "playerState":I
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkDrm(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 2156
    if-nez p1, :cond_0

    .line 2160
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v3

    const-string v4, "checkDrm() - filePath is null, return true"

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2179
    :goto_0
    return v1

    .line 2163
    :cond_0
    const/4 v1, 0x1

    .line 2164
    .local v1, "valid":Z
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4200(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/library/DrmManager;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/sec/android/mmapp/library/DrmManager;->getDrmPopupInfo(Ljava/lang/String;Z)Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    move-result-object v0

    .line 2165
    .local v0, "info":Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;
    iget v2, v0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->type:I

    if-eqz v2, :cond_1

    .line 2166
    iget v2, v0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->type:I

    const/16 v3, 0x18

    if-ne v2, v3, :cond_2

    .line 2167
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$4300(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2168
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mInfo:Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;
    invoke-static {v2, v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4402(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;)Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    .line 2171
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v2

    const-string v3, "checkDrm() - playReadyDrm, try again"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v2, p1}, Lcom/sec/android/mmapp/AudioPreview;->access$4002(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    .line 2176
    const/4 v1, 0x0

    .line 2178
    :cond_1
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkDrm() - filePath: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " valid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2173
    :cond_2
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->newInstance(Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;)Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/AudioPreview;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getCurrentPosition()I
    .locals 2

    .prologue
    .line 2422
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 2426
    .local v0, "currentPosition":I
    return v0
.end method

.method private getDuration()I
    .locals 1

    .prologue
    .line 2430
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method private getMediaPlayerState()I
    .locals 1

    .prologue
    .line 2412
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    return v0
.end method

.method private getPositionOfSeekBar()I
    .locals 4

    .prologue
    .line 2367
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    if-lez v0, :cond_0

    .line 2368
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 2370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPlaying()Z
    .locals 1

    .prologue
    .line 2346
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method private pause()V
    .locals 4

    .prologue
    .line 2315
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() - previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2316
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2343
    :goto_0
    return-void

    .line 2323
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2324
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 2325
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x5

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2327
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    if-lez v0, :cond_1

    .line 2328
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getPositionOfSeekBar()I

    move-result v1

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$5102(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2331
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2332
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$5100(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2334
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;
    invoke-static {v1, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$5200(Lcom/sec/android/mmapp/AudioPreview;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2335
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2338
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4300(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2339
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4802(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto :goto_0
.end method

.method private release()V
    .locals 3

    .prologue
    .line 2396
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "release() - previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2398
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2399
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2400
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4600(Lcom/sec/android/mmapp/AudioPreview;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2401
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 2402
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 2403
    return-void
.end method

.method private reset()V
    .locals 3

    .prologue
    .line 2406
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset() - previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 2408
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2409
    return-void
.end method

.method private seekTo(I)V
    .locals 4
    .param p1, "msec"    # I

    .prologue
    .line 2350
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "seek() - seek: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "msec"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 2352
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2354
    :cond_0
    const/4 v0, 0x0

    .line 2355
    .local v0, "progress":I
    iget v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    if-lez v1, :cond_1

    .line 2356
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->getPositionOfSeekBar()I

    move-result v0

    .line 2358
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v1, v0}, Lcom/sec/android/mmapp/AudioPreview;->access$5102(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2359
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2360
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2363
    .end local v0    # "progress":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlaybackState(I)V
    invoke-static {v1, p1}, Lcom/sec/android/mmapp/AudioPreview;->access$5300(Lcom/sec/android/mmapp/AudioPreview;I)V

    .line 2364
    return-void
.end method

.method private setAudioStreamType(I)V
    .locals 1
    .param p1, "streamType"    # I

    .prologue
    .line 2208
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2209
    return-void
.end method

.method private setDataSource(Landroid/net/Uri;Z)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "doPlay"    # Z

    .prologue
    const v4, 0x7f0c003e

    .line 2183
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource() - path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doPlay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " previous filePath: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->reset()V

    .line 2187
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2188
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2190
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;
    invoke-static {v1, p1}, Lcom/sec/android/mmapp/AudioPreview;->access$4102(Lcom/sec/android/mmapp/AudioPreview;Landroid/net/Uri;)Landroid/net/Uri;

    .line 2191
    iput-boolean p2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDoPlay:Z

    .line 2192
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 2193
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v2, 0x3

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2205
    :goto_0
    return-void

    .line 2194
    :catch_0
    move-exception v0

    .line 2195
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource() - IOException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2196
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4500(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2197
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    goto :goto_0

    .line 2199
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 2200
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource() - IllegalStateException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4500(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2202
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    goto :goto_0
.end method

.method private setMaxVolumeLevel()V
    .locals 3

    .prologue
    .line 2304
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4900(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2305
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSecAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$5000(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/library/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mmapp/library/SecAudioManager;->getMidiVolume()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    .line 2309
    :goto_0
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mVolumeIncrement:F

    .line 2310
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeMaxVolumeLevel() - isMidiFile: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$4900(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMaxVolume: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mVolumeIncrement: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mVolumeIncrement:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2312
    return-void

    .line 2307
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    goto :goto_0
.end method

.method private start()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2213
    invoke-direct {p0, v0, v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->start(ZZ)V

    .line 2214
    return-void
.end method

.method private start(Z)V
    .locals 1
    .param p1, "fadeInEffect"    # Z

    .prologue
    .line 2218
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->start(ZZ)V

    .line 2219
    return-void
.end method

.method private start(ZZ)V
    .locals 7
    .param p1, "fadeInEffect"    # Z
    .param p2, "calledFromMpHandler"    # Z

    .prologue
    const v6, 0x7f0c0050

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2234
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start() - previous state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fadeInEffect: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fadeInEffectMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2239
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2240
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1, v6}, Lcom/sec/android/mmapp/AudioPreview;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2242
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 2300
    :goto_0
    return-void

    .line 2247
    :cond_0
    if-eqz p1, :cond_1

    .line 2248
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->setMaxVolumeLevel()V

    .line 2249
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2250
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 2254
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4600(Lcom/sec/android/mmapp/AudioPreview;)Landroid/media/AudioManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-nez v1, :cond_2

    .line 2256
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    const-string v2, "start() - requestAudioFocus is failed"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2263
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2267
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4500(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2268
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    goto :goto_0

    .line 2274
    :cond_2
    if-nez p2, :cond_3

    .line 2276
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->setMaxVolumeLevel()V

    .line 2277
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMaxVolume:F

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2280
    :cond_3
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isGateEnable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2281
    const-string v1, "GATE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<GATE-M> AUDI_PLAYING: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</GATE-M>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2287
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 2288
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2289
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 2292
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2293
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2294
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v2, 0x4

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2295
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4300(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2296
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z
    invoke-static {v1, v4}, Lcom/sec/android/mmapp/AudioPreview;->access$4802(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_0
.end method

.method private stop()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2374
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop() - previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2379
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2380
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 2381
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x6

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2382
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z
    invoke-static {v0, v4}, Lcom/sec/android/mmapp/AudioPreview;->access$4802(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2384
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$5102(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2385
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2386
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2388
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;
    invoke-static {v1, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2389
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2391
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4300(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2392
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlaybackState(I)V
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$5300(Lcom/sec/android/mmapp/AudioPreview;I)V

    .line 2393
    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    .line 2450
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBufferingUpdate - mp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " percent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2451
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2452
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    mul-int/lit16 v1, p2, 0x3e8

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 2454
    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 2458
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCompletion - mp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2461
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2463
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    int-to-long v2, v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;
    invoke-static {v1, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$5200(Lcom/sec/android/mmapp/AudioPreview;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2464
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2467
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x7

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2469
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mUiUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$5400(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2470
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v4, 0x1

    .line 2435
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onError - mp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " what: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extra: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2437
    const v0, 0x7f0c003e

    .line 2439
    .local v0, "errString":I
    if-ne p2, v4, :cond_0

    const/16 v1, -0x3ed

    if-ne p3, v1, :cond_0

    .line 2440
    const v0, 0x7f0c0028

    .line 2443
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4500(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2444
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 2445
    return v4
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 2474
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPrepared - mp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDoPlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDoPlay:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mProgressPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$5100(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I

    .line 2477
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    .line 2478
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I

    int-to-long v2, v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->setRemainText(J)V
    invoke-static {v0, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$5500(Lcom/sec/android/mmapp/AudioPreview;J)V

    .line 2480
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$5100(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    if-lez v0, :cond_0

    .line 2481
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$5100(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getNewPosition(I)D
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$5600(Lcom/sec/android/mmapp/AudioPreview;I)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 2483
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updateMetaData()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$5700(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2484
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->updatePlaybackState(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$5300(Lcom/sec/android/mmapp/AudioPreview;I)V

    .line 2486
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDoPlay:Z

    if-eqz v0, :cond_1

    .line 2487
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->start()V

    .line 2488
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mDoPlay:Z

    .line 2490
    :cond_1
    return-void
.end method

.method onStart()V
    .locals 2

    .prologue
    .line 2559
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2560
    return-void
.end method

.method onStop()V
    .locals 2

    .prologue
    .line 2566
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2567
    return-void
.end method

.method public pauseWithDisablePlayByAudioFocus()V
    .locals 1

    .prologue
    .line 2497
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2498
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V

    .line 2500
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z

    .line 2501
    return-void
.end method
