.class public Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FolderTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isMusic"    # Z

    .prologue
    .line 406
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    .line 407
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-nez v0, :cond_0

    .line 408
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->FOLDER_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 410
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND bucket_id=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;->selection:Ljava/lang/String;

    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 413
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 415
    const-string v0, "_display_name"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 416
    return-void
.end method
