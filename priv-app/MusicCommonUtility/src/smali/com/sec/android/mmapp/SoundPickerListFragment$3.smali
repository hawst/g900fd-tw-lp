.class Lcom/sec/android/mmapp/SoundPickerListFragment$3;
.super Ljava/lang/Object;
.source "SoundPickerListFragment.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;->setSelected(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$3;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "arg0"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 502
    # setter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z
    invoke-static {v4}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$702(Z)Z

    .line 503
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$3;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$3;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    const v2, 0x7f0c003e

    invoke-virtual {v1, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 505
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$3;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 507
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$3;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 511
    :goto_0
    return v4

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$3;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
