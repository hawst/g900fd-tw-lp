.class Lcom/sec/android/mmapp/AudioPreview$11;
.super Landroid/content/BroadcastReceiver;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 1892
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1895
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1896
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1897
    .local v1, "data":Landroid/net/Uri;
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMediaReceiver - action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1898
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1900
    :cond_0
    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1902
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1904
    .local v2, "removedDevice":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/storage/extSdCard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "/storage/extSdCard"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1906
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 1907
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 1916
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v4}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mmapp/library/PrivateMode;->getRootDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mmapp/library/PrivateMode;->getRootDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1919
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 1920
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 1925
    .end local v2    # "removedDevice":Ljava/lang/String;
    :cond_2
    return-void

    .line 1908
    .restart local v2    # "removedDevice":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/storage/UsbDrive"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "/storage/UsbDrive"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1910
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v3

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 1911
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    const v4, 0x7f0c003e

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1913
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$11;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    goto :goto_0
.end method
