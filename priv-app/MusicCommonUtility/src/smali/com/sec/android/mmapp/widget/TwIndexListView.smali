.class public Lcom/sec/android/mmapp/widget/TwIndexListView;
.super Landroid/widget/ListView;
.source "TwIndexListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView$OnIndexSelectedListener;


# instance fields
.field private mHasShuffleView:Z

.field private mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

.field private mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

.field public mScrollState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 38
    iput-boolean v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mHasShuffleView:Z

    .line 40
    iput v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mScrollState:I

    .line 49
    invoke-virtual {p0, p0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput-boolean v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mHasShuffleView:Z

    .line 40
    iput v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mScrollState:I

    .line 54
    invoke-virtual {p0, p0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput-boolean v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mHasShuffleView:Z

    .line 40
    iput v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mScrollState:I

    .line 59
    invoke-virtual {p0, p0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 60
    return-void
.end method


# virtual methods
.method public onIndexSelected(I)V
    .locals 1
    .param p1, "sectionIndex"    # I

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mHasShuffleView:Z

    if-eqz v0, :cond_0

    .line 118
    add-int/lit8 p1, p1, 0x1

    .line 120
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setSelection(I)V

    .line 121
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->onScroll(Landroid/widget/AbsListView;III)V

    .line 129
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mScrollState:I

    if-eq v0, p2, :cond_0

    .line 134
    iput p2, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mScrollState:I

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 139
    :cond_1
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 33
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    if-eqz v1, :cond_1

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 66
    .local v0, "oldAdapter":Landroid/widget/Adapter;
    if-eqz v0, :cond_0

    .line 67
    iget-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 69
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    .line 71
    .end local v0    # "oldAdapter":Landroid/widget/Adapter;
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 72
    return-void
.end method

.method public setHasShuffleView(Z)V
    .locals 0
    .param p1, "hasShuffleView"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mHasShuffleView:Z

    .line 111
    return-void
.end method

.method public setIndexer(Landroid/database/Cursor;I)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "columnIndex"    # I

    .prologue
    .line 80
    if-eqz p1, :cond_1

    .line 81
    const/4 v2, 0x0

    .line 82
    .local v2, "langIndex":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f060000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "index":[Ljava/lang/String;
    new-instance v3, Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-direct {v3, p1, p2, v1, v2}, Lcom/sec/android/touchwiz/widget/TwCursorIndexer;-><init>(Landroid/database/Cursor;I[Ljava/lang/String;I)V

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 87
    .local v0, "adapter":Landroid/widget/Adapter;
    if-eqz v0, :cond_0

    .line 88
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-interface {v0, v3}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 90
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-eqz v3, :cond_1

    .line 91
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    iget-object v4, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setIndexer(Lcom/sec/android/touchwiz/widget/TwAbstractIndexer;)V

    .line 94
    .end local v0    # "adapter":Landroid/widget/Adapter;
    .end local v1    # "index":[Ljava/lang/String;
    .end local v2    # "langIndex":I
    :cond_1
    return-void
.end method

.method public setTwScrollView(Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;)V
    .locals 2
    .param p1, "sv"    # Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    .line 101
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    iget-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setIndexer(Lcom/sec/android/touchwiz/widget/TwAbstractIndexer;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListView;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setOnIndexSelectedListener(Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView$OnIndexSelectedListener;)V

    .line 107
    return-void
.end method
