.class Lcom/sec/android/mmapp/AudioPreview$Player$1;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview$Player;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mmapp/AudioPreview$Player;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview$Player;)V
    .locals 0

    .prologue
    .line 2503
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 5
    .param p1, "focusChange"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2506
    packed-switch p1, :pswitch_data_0

    .line 2546
    :pswitch_0
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549
    :cond_0
    :goto_0
    return-void

    .line 2508
    :pswitch_1
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2509
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v0, v0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 2510
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # setter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5802(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z

    .line 2511
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 2516
    :pswitch_2
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2517
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mMpHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5900(Lcom/sec/android/mmapp/AudioPreview$Player;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2519
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v0, v0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 2520
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # setter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5802(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z

    .line 2521
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 2525
    :pswitch_3
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioFocus: received AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2526
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v0, v0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5800(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2537
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # setter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$5802(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z

    .line 2538
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v0, v0, Lcom/sec/android/mmapp/AudioPreview$Player;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsOnStarted:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$6000(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2539
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$300(Lcom/sec/android/mmapp/AudioPreview$Player;Z)V

    goto :goto_0

    .line 2541
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$Player$1;->this$1:Lcom/sec/android/mmapp/AudioPreview$Player;

    # setter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mToStartByAudioFocusGain:Z
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$202(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z

    goto :goto_0

    .line 2506
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
