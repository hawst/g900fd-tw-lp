.class public Lcom/sec/android/mmapp/SetAsActivity;
.super Landroid/app/Activity;
.source "SetAsActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;


# static fields
.field private static final ALARM_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.clockpackage"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final COTACT_PACKAGE_NAME:Ljava/lang/String; = "com.android.contacts"

.field private static final FLAG_SUPPORT_NUMERIC_KEYPAD:Z = false

.field private static final PHONE_PACKAGE_NAME:Ljava/lang/String; = "com.android.phone"

.field public static final SIMSLOT1:I = 0x0

.field public static final SIMSLOT2:I = 0x1

.field private static final SIM_SLOT_1:Ljava/lang/String; = "gsm.sim.state"

.field private static final SIM_SLOT_2:Ljava/lang/String; = "gsm.sim.state2"

.field private static final SIM_SLOT_DSDS:Ljava/lang/String; = "gsm.sim.state_1"

.field private static final SIM_SLOT_NUM:Ljava/lang/String; = "ril.MSIMM"

.field private static final SIM_STATE_ABSENT:Ljava/lang/String; = "ABSENT"

.field private static final SIM_STATE_READY:Ljava/lang/String; = "READY"


# instance fields
.field private mAlarmtoneRadioButton:Landroid/widget/RadioButton;

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

.field private mFilePath:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mIsMultiSIMSetAsMode:Z

.field private mMainView:Landroid/view/View;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMenu:Landroid/view/Menu;

.field private mNormalRadioButton:Landroid/widget/RadioButton;

.field private mOffset:I

.field private mOkButton:Landroid/view/MenuItem;

.field private mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressInfoView:Landroid/view/View;

.field private mRecommendRadioButton:Landroid/widget/RadioButton;

.field private mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

.field private final mResultListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

.field private mUri:Landroid/net/Uri;

.field private simCardDialog:Landroid/app/AlertDialog$Builder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/android/mmapp/SetAsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    .line 726
    new-instance v0, Lcom/sec/android/mmapp/SetAsActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SetAsActivity$2;-><init>(Lcom/sec/android/mmapp/SetAsActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mResultListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    .line 745
    new-instance v0, Lcom/sec/android/mmapp/SetAsActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SetAsActivity$3;-><init>(Lcom/sec/android/mmapp/SetAsActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 764
    new-instance v0, Lcom/sec/android/mmapp/SetAsActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SetAsActivity$4;-><init>(Lcom/sec/android/mmapp/SetAsActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/mmapp/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->play()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/SetAsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/mmapp/SetAsActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    return p1
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/mmapp/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/mmapp/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->setDataSource()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/mmapp/SetAsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SetAsActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->initProgressInfo()V

    return-void
.end method

.method private doSetAs()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 528
    sget-object v4, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v5, "doSetAs()"

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 530
    const-string v4, "gsm.sim.state"

    const-string v5, "ABSENT"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 531
    .local v2, "sim1State":Ljava/lang/String;
    const-string v4, "gsm.sim.state2"

    const-string v5, "ABSENT"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 532
    .local v3, "sim2State":Ljava/lang/String;
    const-string v4, "READY"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "READY"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 533
    iput-boolean v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    .line 534
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->showChooseSimCardDialog()V

    .line 590
    .end local v2    # "sim1State":Ljava/lang/String;
    .end local v3    # "sim2State":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 538
    .restart local v2    # "sim1State":Ljava/lang/String;
    .restart local v3    # "sim2State":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/android/mmapp/SetAsActivity;->isSIMInsertedOnlyInSlot2()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "READY"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 539
    :cond_2
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-static {p0, v4, v5}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 540
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "recommendation_time_2"

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 549
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "DEBUG_RINGTONE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MusicPlayer : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 551
    sget-object v4, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doSetAs() - PhoneRingtone - recommendation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v6}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mUri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mOffset: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const v4, 0x7f0c0040

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 544
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-static {p0, v6, v4}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 545
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 546
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "recommendation_time"

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 556
    .end local v2    # "sim1State":Ljava/lang/String;
    .end local v3    # "sim2State":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 557
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 558
    .local v1, "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 559
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "highlight_offset"

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    .line 562
    :cond_6
    const-string v4, "ringtone_uri"

    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 563
    const-string v4, "vnd.android.cursor.item/contact"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 565
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 569
    :goto_2
    sget-object v4, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doSetAs() - CallerRingtone - recommendation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v6}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mUri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mOffset: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 566
    :catch_0
    move-exception v0

    .line 567
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v4, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doSetAs() - Not found contact application :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 571
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 573
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    const-string v5, "alarm://com.sec.android.app.clockpackage/alarmlist/"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 576
    .restart local v1    # "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 577
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "highlight_offset"

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    .line 580
    :cond_8
    const-string v4, "alarm_uri"

    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 582
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 586
    :goto_3
    sget-object v4, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doSetAs() - AlarmTone - recommendation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v6}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mUri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 583
    :catch_1
    move-exception v0

    .line 584
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    sget-object v4, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doSetAs() - Not found alarm application :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 331
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 333
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 334
    :try_start_0
    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 339
    :cond_0
    :goto_0
    return-object v1

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAppIcon() - not found exception!! : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x0

    .line 654
    if-nez p1, :cond_2

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V

    :cond_0
    :goto_0
    move-object v0, v7

    .line 675
    :cond_1
    :goto_1
    return-object v0

    .line 657
    :cond_2
    const/4 v6, 0x0

    .line 659
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 662
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 663
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 670
    if-eqz v6, :cond_1

    .line 671
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 665
    :cond_3
    const v0, 0x7f0c004f

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 670
    if-eqz v6, :cond_0

    .line 671
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 670
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 671
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private getTimeString(J)Ljava/lang/String;
    .locals 9
    .param p1, "msec"    # J

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 777
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gtz v3, :cond_0

    .line 778
    const/4 v2, 0x0

    .line 779
    .local v2, "sec":I
    const/4 v1, 0x0

    .line 780
    .local v1, "min":I
    const/4 v0, 0x0

    .line 795
    .local v0, "hr":I
    :goto_0
    if-nez v0, :cond_2

    .line 796
    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 798
    :goto_1
    return-object v3

    .line 783
    .end local v0    # "hr":I
    .end local v1    # "min":I
    .end local v2    # "sec":I
    :cond_0
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v2, v4

    .line 784
    .restart local v2    # "sec":I
    if-nez v2, :cond_1

    .line 785
    const/4 v1, 0x0

    .line 786
    .restart local v1    # "min":I
    const/4 v0, 0x0

    .line 790
    :goto_2
    rem-int/lit8 v2, v2, 0x3c

    .line 791
    div-int/lit8 v0, v1, 0x3c

    .line 792
    .restart local v0    # "hr":I
    rem-int/lit8 v1, v1, 0x3c

    goto :goto_0

    .line 788
    .end local v0    # "hr":I
    .end local v1    # "min":I
    :cond_1
    div-int/lit8 v1, v2, 0x3c

    .restart local v1    # "min":I
    goto :goto_2

    .line 798
    .restart local v0    # "hr":I
    :cond_2
    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private initProgressInfo()V
    .locals 10

    .prologue
    const v9, 0x7f090076

    const/4 v8, 0x0

    .line 803
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v5, :cond_0

    .line 832
    :goto_0
    return-void

    .line 806
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 808
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 809
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v6, 0x7f0f0030

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 810
    .local v3, "progress":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v6, 0x7f0f0043

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 812
    .local v4, "startingTimeText":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v6, 0x7f0f0041

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 813
    .local v1, "offsetTimeText":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    const v6, 0x7f0f0044

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 816
    .local v0, "durationTimeText":Landroid/widget/TextView;
    iget v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 820
    .local v2, "paddingValue":I
    invoke-virtual {v3, v2, v8, v8, v8}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 821
    const-wide/16 v6, 0x0

    invoke-direct {p0, v6, v7}, Lcom/sec/android/mmapp/SetAsActivity;->getTimeString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 823
    iget v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    int-to-long v6, v5

    invoke-direct {p0, v6, v7}, Lcom/sec/android/mmapp/SetAsActivity;->getTimeString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 824
    add-int/lit8 v5, v2, -0x1b

    invoke-virtual {v1, v5, v8, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 826
    iget-object v5, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v5

    int-to-long v6, v5

    invoke-direct {p0, v6, v7}, Lcom/sec/android/mmapp/SetAsActivity;->getTimeString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 827
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090074

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v6

    sub-float/2addr v5, v6

    float-to-int v2, v5

    .line 831
    invoke-virtual {v0, v2, v8, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0
.end method

.method private static isSIMInsertedOnlyInSlot2()Z
    .locals 3

    .prologue
    .line 593
    const-string v2, "ril.MSIMM"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 594
    .local v1, "simm":Ljava/lang/String;
    const/4 v0, 0x1

    .line 595
    .local v0, "onlyInSlot2":Z
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 596
    const/4 v0, 0x1

    .line 600
    :goto_0
    return v0

    .line 598
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVoiceCapable()Z
    .locals 2

    .prologue
    .line 840
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 841
    .local v0, "telephony":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/sec/android/mmapp/library/TelephonyManagerCompat;->isVoiceCapable(Landroid/telephony/TelephonyManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private play()V
    .locals 3

    .prologue
    .line 680
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 681
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 682
    const v1, 0x7f0c0050

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 689
    :goto_0
    return-void

    .line 687
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->requestAudioFocus()V

    .line 688
    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method

.method private releaseRecommander()V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->close()Z

    .line 715
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .line 717
    :cond_0
    return-void
.end method

.method private requestAudioFocus()V
    .locals 4

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-nez v0, :cond_0

    .line 722
    sget-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "requestAudioFocus fail. it may be during call"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_0
    return-void
.end method

.method private setAppIconImage()V
    .locals 5

    .prologue
    .line 318
    const v4, 0x7f0f0048

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 319
    .local v3, "phone":Landroid/widget/ImageView;
    const-string v4, "com.android.phone"

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 320
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 322
    const v4, 0x7f0f004d

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 323
    .local v1, "contact":Landroid/widget/ImageView;
    const-string v4, "com.android.contacts"

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 325
    const v4, 0x7f0f0052

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 326
    .local v0, "alarm":Landroid/widget/ImageView;
    const-string v4, "com.sec.android.app.clockpackage"

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/SetAsActivity;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 327
    return-void
.end method

.method private setDataSource()V
    .locals 3

    .prologue
    .line 692
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 694
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/mmapp/SetAsActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 695
    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 696
    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 697
    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 702
    :goto_0
    return-void

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Ljava/io/IOException;
    const v1, 0x7f0c003e

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private showChooseSimCardDialog()V
    .locals 6

    .prologue
    .line 604
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "select_name_1"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 605
    .local v2, "strSim1":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "select_name_2"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 607
    .local v3, "strSim2":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v1, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v4, 0x1

    aput-object v3, v1, v4

    .line 611
    .local v1, "items":[Ljava/lang/CharSequence;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    .line 612
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    const v5, 0x7f0c0042

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 613
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    new-instance v5, Lcom/sec/android/mmapp/SetAsActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/mmapp/SetAsActivity$1;-><init>(Lcom/sec/android/mmapp/SetAsActivity;)V

    invoke-virtual {v4, v1, v5}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 649
    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->simCardDialog:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 650
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 651
    return-void
.end method

.method private stopMediaPlayer()V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 707
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 708
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 710
    :cond_0
    return-void
.end method

.method private updateRadioButon(Landroid/widget/RadioButton;)V
    .locals 3
    .param p1, "selectedRadioButton"    # Landroid/widget/RadioButton;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 506
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_3

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 518
    :cond_1
    :goto_0
    invoke-virtual {p1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 520
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOkButton:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    .line 525
    :cond_2
    :goto_1
    return-void

    .line 510
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_1

    .line 513
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 514
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 520
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 523
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOkButton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 262
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 263
    const v0, 0x7f050001

    const v1, 0x7f050005

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->overridePendingTransition(II)V

    .line 265
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 256
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 257
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 412
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 414
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 417
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 424
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 419
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 420
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->setDataSource()V

    .line 421
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->play()V

    goto :goto_1

    .line 430
    :sswitch_1
    iget v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    if-lez v0, :cond_4

    .line 432
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 435
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 465
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 437
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 438
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->setDataSource()V

    .line 439
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 440
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->play()V

    goto :goto_2

    .line 444
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    if-nez v0, :cond_5

    .line 445
    new-instance v0, Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-direct {v0}, Lcom/sec/android/mmapp/library/RingtoneRecommender;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .line 446
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->isOpen()Z

    move-result v0

    if-nez v0, :cond_2

    .line 447
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->open(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 460
    :pswitch_0
    const v0, 0x7f0c0022

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 449
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommender:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mResultListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->doExtract(Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;)Z

    .line 450
    const/4 v0, 0x0

    const v1, 0x7f0c003f

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_2

    .line 454
    :pswitch_2
    const v0, 0x7f0c0021

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 472
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 474
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    .line 479
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    .line 486
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->updateRadioButon(Landroid/widget/RadioButton;)V

    .line 488
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    .line 412
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0037 -> :sswitch_0
        0x7f0f003a -> :sswitch_1
        0x7f0f003f -> :sswitch_2
        0x7f0f004b -> :sswitch_3
        0x7f0f0050 -> :sswitch_4
    .end sparse-switch

    .line 447
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 836
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 837
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v2, 0x7f090040

    .line 295
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMenu:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->setButtonStyle(Landroid/view/Menu;)V

    .line 297
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 298
    const/16 v3, 0x35

    .line 302
    .local v3, "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 304
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 311
    .local v5, "height":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mmapp/util/MusicListUtils;->updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V

    .line 314
    .end local v3    # "gravity":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 315
    return-void

    .line 306
    .restart local v3    # "gravity":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 308
    .restart local v4    # "width":I
    const/4 v5, -0x1

    .restart local v5    # "height":I
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 16
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 131
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v1, :cond_0

    .line 132
    const v1, 0x7f050004

    const v2, 0x7f050001

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mmapp/SetAsActivity;->overridePendingTransition(II)V

    .line 134
    :cond_0
    sget-object v1, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const v1, 0x7f040014

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->setContentView(I)V

    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    .line 138
    .local v8, "bar":Landroid/app/ActionBar;
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v1, :cond_2

    .line 139
    const-string v1, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/telephony/TelephonyManager;

    .line 140
    .local v15, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v15, :cond_1

    invoke-virtual {v15}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v15}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 141
    :cond_1
    const v1, 0x7f0c0038

    invoke-virtual {v8, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 144
    .end local v15    # "tm":Landroid/telephony/TelephonyManager;
    :cond_2
    const/16 v1, 0xa

    invoke-virtual {v8, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 145
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-nez v1, :cond_3

    .line 146
    const/16 v1, 0xe

    invoke-virtual {v8, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 149
    :cond_3
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v1, :cond_4

    .line 150
    const/16 v4, 0x35

    .line 153
    .local v4, "gravity":I
    const v1, 0x7f0f002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mMainView:Landroid/view/View;

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 156
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 157
    .local v5, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 163
    .local v6, "height":I
    :goto_0
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->setFinishOnTouchOutside(Z)V

    .line 164
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mmapp/SetAsActivity;->mMainView:Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-static/range {v1 .. v6}, Lcom/sec/android/mmapp/util/MusicListUtils;->updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V

    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v11

    .line 170
    .local v11, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 173
    .end local v4    # "gravity":I
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v11    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_4
    const-string v1, "audio"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    .line 176
    .local v10, "data":Landroid/os/Bundle;
    if-eqz v10, :cond_6

    .line 177
    const-string v1, "extra_uri"

    invoke-virtual {v10, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    .line 178
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_6

    .line 179
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Did you miss MusicIntent.EXTRA_URI as extra value?"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 159
    .end local v10    # "data":Landroid/os/Bundle;
    .restart local v4    # "gravity":I
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 161
    .restart local v5    # "width":I
    const/4 v6, -0x1

    .restart local v6    # "height":I
    goto :goto_0

    .line 183
    .end local v4    # "gravity":I
    .end local v5    # "width":I
    .end local v6    # "height":I
    .restart local v10    # "data":Landroid/os/Bundle;
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->getPath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mFilePath:Ljava/lang/String;

    .line 185
    const v1, 0x7f0f003f

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 186
    .local v13, "phoneRingtone":Landroid/view/View;
    const v1, 0x7f0f004b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 187
    .local v9, "callerRingtone":Landroid/view/View;
    const v1, 0x7f0f0050

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 188
    .local v7, "alarmTone":Landroid/view/View;
    const v1, 0x7f0f0037

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 189
    .local v12, "normal":Landroid/view/View;
    const v1, 0x7f0f003a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 190
    .local v14, "recommend":Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->setAppIconImage()V

    .line 193
    const/4 v1, 0x0

    invoke-virtual {v14, v1}, Landroid/view/View;->setVisibility(I)V

    .line 194
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Landroid/view/View;->setVisibility(I)V

    .line 195
    const v1, 0x7f0f003e

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    const v1, 0x7f0f0045

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 197
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v1, :cond_7

    .line 198
    const v1, 0x7f0f0046

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 201
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    const v1, 0x7f0f0038

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    .line 212
    const v1, 0x7f0f003b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    .line 213
    const v1, 0x7f0f0047

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    .line 214
    const v1, 0x7f0f004c

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    .line 215
    const v1, 0x7f0f0051

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    .line 217
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mmapp/SetAsActivity;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_8

    .line 218
    const/16 v1, 0x8

    invoke-virtual {v13, v1}, Landroid/view/View;->setVisibility(I)V

    .line 219
    const v1, 0x7f0f004a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 220
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    const v1, 0x7f0f004f

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 225
    :cond_8
    const v1, 0x7f0f0040

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SetAsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mProgressInfoView:Landroid/view/View;

    .line 227
    if-eqz p1, :cond_9

    .line 228
    const-string v1, "offset"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    .line 230
    :cond_9
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 281
    const v0, 0x7f0f000d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOkButton:Landroid/view/MenuItem;

    .line 282
    iput-object p1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMenu:Landroid/view/Menu;

    .line 283
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mMenu:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SetAsActivity;->setButtonStyle(Landroid/view/Menu;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mNormalRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOkButton:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 290
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOkButton:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 269
    sget-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->releaseRecommander()V

    .line 271
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 272
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 273
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 274
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 864
    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    .line 368
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V

    .line 407
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    .line 370
    :pswitch_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mmapp/SetAsActivity;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 371
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 372
    const v3, 0x7f0c004f

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/SetAsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V

    goto :goto_0

    .line 377
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 379
    .local v2, "values":Landroid/content/ContentValues;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/mmapp/SetAsActivity;->mPhoneRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mmapp/SetAsActivity;->mCallerRingtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 381
    :cond_2
    const-string v3, "is_ringtone"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 390
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/SetAsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 391
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->doSetAs()V

    .line 392
    iget-boolean v3, p0, Lcom/sec/android/mmapp/SetAsActivity;->mIsMultiSIMSetAsMode:Z

    if-nez v3, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V

    goto :goto_0

    .line 382
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAlarmtoneRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 383
    const-string v3, "is_alarm"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 385
    :catch_0
    move-exception v0

    .line 386
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalArgumentException occured :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 387
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 388
    .local v0, "ex":Ljava/lang/UnsupportedOperationException;
    sget-object v3, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UnsupportedOperationException occured :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 397
    .end local v0    # "ex":Ljava/lang/UnsupportedOperationException;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "values":Landroid/content/ContentValues;
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->finish()V

    .line 398
    sget-boolean v3, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v3, :cond_0

    .line 399
    const v3, 0x7f050001

    const v4, 0x7f050005

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/mmapp/SetAsActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    .line 368
    :pswitch_data_0
    .packed-switch 0x7f0f000c
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 247
    sget-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->releaseRecommander()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 250
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 251
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 252
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mRecommendRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    if-lez v0, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V

    .line 238
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->setDataSource()V

    .line 239
    invoke-direct {p0}, Lcom/sec/android/mmapp/SetAsActivity;->initProgressInfo()V

    .line 242
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 243
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 499
    sget-object v0, Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const-string v0, "offset"

    iget v1, p0, Lcom/sec/android/mmapp/SetAsActivity;->mOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 501
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 502
    return-void
.end method

.method public setButtonStyle(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0f000d

    const v4, 0x7f0f000c

    const/4 v3, 0x6

    const/4 v2, 0x2

    .line 343
    if-nez p1, :cond_0

    .line 364
    :goto_0
    return-void

    .line 347
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SetAsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 349
    .local v0, "orientation":I
    if-ne v0, v2, :cond_1

    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-nez v1, :cond_1

    .line 350
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020007

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 354
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020006

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    .line 359
    :cond_1
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 361
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0
.end method
