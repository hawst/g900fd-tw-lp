.class Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "PalmTouchTutorialActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/PalmTouchTutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 206
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.PALM_DOWN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v3, v3, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->isAudioPathBTHeadphone()Z

    move-result v3

    if-nez v3, :cond_1

    .line 209
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # setter for: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mPlaySound:Z
    invoke-static {v3, v5}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$002(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;Z)Z

    .line 210
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # invokes: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->stopSound()V
    invoke-static {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$100(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    .line 211
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # getter for: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage1:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$200(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # getter for: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage2:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$300(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 212
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # getter for: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage1:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$200(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/ImageView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 213
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # getter for: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage2:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$300(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 215
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    # getter for: Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mTouchHelpText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->access$400(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f0c0039

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 221
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 222
    .local v0, "_intent":Landroid/content/Intent;
    const-string v3, "PalmMotionTest"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 223
    .local v1, "_value":Z
    if-eqz v1, :cond_1

    .line 224
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v3, v3, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v4, v4, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->work:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 225
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v3, v3, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;->this$0:Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    iget-object v4, v4, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->showCompletionToast:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 231
    .end local v0    # "_intent":Landroid/content/Intent;
    .end local v1    # "_value":Z
    :cond_1
    return-void
.end method
