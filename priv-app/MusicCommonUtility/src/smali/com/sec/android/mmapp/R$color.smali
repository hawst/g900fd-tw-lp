.class public final Lcom/sec/android/mmapp/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_bg_color:I = 0x7f080000

.field public static final actionbar_cancel_done:I = 0x7f080001

.field public static final activity_color:I = 0x7f080028

.field public static final audio_preview_bg:I = 0x7f080002

.field public static final checked_bitmap_color_list:I = 0x7f08002b

.field public static final dialog_main_text:I = 0x7f080003

.field public static final divider_color:I = 0x7f080029

.field public static final everglade_action_bar_menu_color:I = 0x7f08002c

.field public static final everglade_default_text_color:I = 0x7f080004

.field public static final everglade_default_text_disabled_color:I = 0x7f080005

.field public static final everglade_default_text_pressed_color:I = 0x7f080006

.field public static final everglades3:I = 0x7f080007

.field public static final everglades_bg:I = 0x7f080008

.field public static final hover_common_view_text_color_for_black_theme:I = 0x7f080009

.field public static final hover_common_view_text_color_for_white_theme:I = 0x7f08000a

.field public static final hover_seekbar_view_text_color:I = 0x7f08000b

.field public static final list_divider_text_color:I = 0x7f08000c

.field public static final list_header_title_text:I = 0x7f08000d

.field public static final list_item_title_background:I = 0x7f08000e

.field public static final list_item_title_divider_color:I = 0x7f08000f

.field public static final list_main_header_text:I = 0x7f080010

.field public static final matched_text_color:I = 0x7f080011

.field public static final music_miniplayer_control_popup_bg:I = 0x7f080012

.field public static final music_miniplayer_progress_bg:I = 0x7f080013

.field public static final music_miniplayer_progress_primary:I = 0x7f080014

.field public static final music_picker_list_item_text_color:I = 0x7f08002d

.field public static final music_picker_secondary_text_color:I = 0x7f08002e

.field public static final music_player_duration_time:I = 0x7f080015

.field public static final music_player_fixed_time:I = 0x7f080016

.field public static final music_player_list_conut_info_background:I = 0x7f080017

.field public static final music_player_list_count:I = 0x7f080018

.field public static final music_player_list_divider:I = 0x7f080019

.field public static final music_player_list_playing:I = 0x7f08001a

.field public static final music_player_list_text1:I = 0x7f08001b

.field public static final music_player_list_text2:I = 0x7f08001c

.field public static final music_player_seek_time:I = 0x7f08001d

.field public static final music_player_starting_progress:I = 0x7f08001e

.field public static final no_item_text:I = 0x7f08001f

.field public static final set_as_list_item_text_color:I = 0x7f08002f

.field public static final set_as_offset_time_text:I = 0x7f08002a

.field public static final set_as_recommand_explain_color:I = 0x7f080020

.field public static final set_as_text_color:I = 0x7f080021

.field public static final set_sub_text:I = 0x7f080022

.field public static final set_sub_text_color:I = 0x7f080030

.field public static final tab_text_color_selector:I = 0x7f080031

.field public static final text_color_1:I = 0x7f080023

.field public static final text_color_2:I = 0x7f080024

.field public static final text_color_3:I = 0x7f080025

.field public static final title_text_color_selector:I = 0x7f080032

.field public static final tw_black:I = 0x7f080026

.field public static final tw_white:I = 0x7f080027

.field public static final unchecked_bitmap_color_list:I = 0x7f080033


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
