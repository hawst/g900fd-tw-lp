.class Lcom/sec/android/mmapp/SetAsActivity$3;
.super Ljava/lang/Object;
.source "SetAsActivity.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SetAsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SetAsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SetAsActivity;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/sec/android/mmapp/SetAsActivity$3;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 748
    packed-switch p1, :pswitch_data_0

    .line 758
    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SetAsActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 752
    :pswitch_0
    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SetAsActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioFocus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$3;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SetAsActivity;->access$600(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$3;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # getter for: Lcom/sec/android/mmapp/SetAsActivity;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SetAsActivity;->access$600(Lcom/sec/android/mmapp/SetAsActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/sec/android/mmapp/SetAsActivity$3;->this$0:Lcom/sec/android/mmapp/SetAsActivity;

    # invokes: Lcom/sec/android/mmapp/SetAsActivity;->stopMediaPlayer()V
    invoke-static {v0}, Lcom/sec/android/mmapp/SetAsActivity;->access$700(Lcom/sec/android/mmapp/SetAsActivity;)V

    goto :goto_0

    .line 748
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
