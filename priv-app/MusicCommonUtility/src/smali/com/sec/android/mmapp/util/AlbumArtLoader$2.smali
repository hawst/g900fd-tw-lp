.class Lcom/sec/android/mmapp/util/AlbumArtLoader$2;
.super Landroid/os/Handler;
.source "AlbumArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/util/AlbumArtLoader;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/util/AlbumArtLoader;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/util/AlbumArtLoader;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader$2;->this$0:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 133
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;

    .line 134
    .local v0, "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 144
    :goto_0
    iget v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->scrollState:I

    if-eqz v2, :cond_0

    .line 147
    const-wide/16 v2, 0xc8

    :try_start_0
    invoke-static {v2, v3}, Landroid/os/HandlerThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_0
    :goto_1
    # getter for: Lcom/sec/android/mmapp/util/AlbumArtLoader;->sListAlbumArtHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->access$000()Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/sec/android/mmapp/util/AlbumArtLoader;->sListAlbumArtHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->access$000()Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    iget v5, p1, Landroid/os/Message;->arg1:I

    const/4 v6, -0x1

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 154
    :cond_1
    return-void

    .line 136
    :pswitch_0
    iget-wide v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->albumId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 139
    iget-object v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->context:Landroid/content/Context;

    iget-wide v4, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->albumId:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->getArtworkAndMakeCache(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 148
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 134
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
