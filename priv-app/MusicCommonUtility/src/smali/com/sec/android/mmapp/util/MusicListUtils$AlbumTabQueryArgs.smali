.class public Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "isMusic"    # Z

    .prologue
    const/4 v1, 0x0

    .line 421
    invoke-direct {p0}, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;-><init>()V

    .line 422
    # invokes: Lcom/sec/android/mmapp/util/MusicListUtils;->getAlbumTabQueryArgsUri(Z)Landroid/net/Uri;
    invoke-static {p1}, Lcom/sec/android/mmapp/util/MusicListUtils;->access$200(Z)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->uri:Landroid/net/Uri;

    .line 423
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->projection:[Ljava/lang/String;

    .line 424
    iput-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->selection:Ljava/lang/String;

    .line 425
    iput-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 426
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 427
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 429
    const-string v0, "_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 430
    const-string v0, "_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 431
    const-string v0, "album"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 432
    const-string v0, "artist"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;->text2Col:Ljava/lang/String;

    .line 433
    return-void
.end method
