.class Lcom/sec/android/mmapp/AudioPreview$5;
.super Landroid/os/Handler;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 1668
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const-wide/16 v4, 0x12c

    const/4 v3, 0x0

    .line 1671
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msg.what: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mIsPressedBtn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1704
    :goto_0
    return-void

    .line 1674
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1675
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3600(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->rewind(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3700(Lcom/sec/android/mmapp/AudioPreview;I)V

    .line 1676
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3600(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    if-ge v0, v8, :cond_0

    .line 1677
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # operator++ for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3608(Lcom/sec/android/mmapp/AudioPreview;)I

    .line 1679
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1680
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 1682
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1684
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$3602(Lcom/sec/android/mmapp/AudioPreview;I)I

    goto :goto_0

    .line 1688
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1689
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3600(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->fastForward(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3800(Lcom/sec/android/mmapp/AudioPreview;I)V

    .line 1690
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3600(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v0

    if-ge v0, v8, :cond_3

    .line 1691
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # operator++ for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$3608(Lcom/sec/android/mmapp/AudioPreview;)I

    .line 1693
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1694
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    if-ne v0, v7, :cond_5

    .line 1696
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1698
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$5;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I
    invoke-static {v0, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$3602(Lcom/sec/android/mmapp/AudioPreview;I)I

    goto/16 :goto_0

    .line 1672
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
