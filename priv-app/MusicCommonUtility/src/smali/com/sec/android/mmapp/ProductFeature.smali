.class public Lcom/sec/android/mmapp/ProductFeature;
.super Ljava/lang/Object;
.source "ProductFeature.java"


# static fields
.field public static final IS_AMERICANO:Z

.field public static final IS_MASS:Z

.field public static final IS_TABLET:Z

.field public static final IS_TABLET_AMERICANO:Z

.field public static final SUPPORT_LATEST_PHONE_UI:Z

.field public static final SUPPORT_STREAMING_LAYOUT:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 13
    invoke-static {}, Lcom/sec/android/mmapp/ProductFeature;->isTablet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    .line 21
    const-string v0, "ro.build.product"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "chagall"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.build.product"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "klimt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_STREAMING_LAYOUT:Z

    .line 26
    sput-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    return-void

    .line 21
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method private static isTablet()Z
    .locals 2

    .prologue
    .line 36
    const-string v0, "ro.build.scafe"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "latte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
