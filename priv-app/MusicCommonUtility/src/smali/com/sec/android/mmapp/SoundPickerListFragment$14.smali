.class Lcom/sec/android/mmapp/SoundPickerListFragment$14;
.super Landroid/content/BroadcastReceiver;
.source "SoundPickerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 1394
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$14;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1397
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1398
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mMediaReceiver:onReceive() is called, action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$14;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    .line 1400
    .local v1, "selected":I
    if-gtz v1, :cond_0

    .line 1401
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$14;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->resetSelectedId()V

    .line 1402
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$14;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 1404
    :cond_0
    return-void
.end method
