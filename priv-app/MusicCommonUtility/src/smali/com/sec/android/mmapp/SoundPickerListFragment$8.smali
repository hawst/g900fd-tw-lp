.class Lcom/sec/android/mmapp/SoundPickerListFragment$8;
.super Ljava/lang/Object;
.source "SoundPickerListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

.field final synthetic val$a:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 833
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    iput-object p2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->val$a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 836
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->val$a:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getPlayMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 846
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # invokes: Lcom/sec/android/mmapp/SoundPickerListFragment;->updatePlayModeButtonText()V
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1400(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    .line 847
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1500(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 848
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1500(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 853
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1600(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v0

    .line 854
    .local v0, "playId":J
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 855
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidateViews()V

    .line 856
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # invokes: Lcom/sec/android/mmapp/SoundPickerListFragment;->autoStartAfterToggle(J)V
    invoke-static {v2, v0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1700(Lcom/sec/android/mmapp/SoundPickerListFragment;J)V

    .line 857
    return-void

    .line 838
    .end local v0    # "playId":J
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->val$a:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v2, v4}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setPlayMode(I)V

    goto :goto_0

    .line 841
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;->val$a:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setPlayMode(I)V

    goto :goto_0

    .line 836
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
