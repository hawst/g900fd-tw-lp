.class Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;
.super Ljava/lang/Object;
.source "SoundPickerTabActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerTabActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabListener"
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

.field private final mList:I

.field private final mTag:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/mmapp/SoundPickerTabActivity;Landroid/app/Activity;I)V
    .locals 4
    .param p2, "a"    # Landroid/app/Activity;
    .param p3, "list"    # I

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-object p2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mActivity:Landroid/app/Activity;

    .line 236
    iput p3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    .line 237
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mTag:Ljava/lang/String;

    .line 242
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mmapp/SoundPickerListFragment;

    iput-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    .line 244
    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TabListener mFragment : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mTag : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->isDetached()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 247
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 248
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 250
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 4
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 314
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    const v1, 0x20001

    if-ne v0, v1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$500(Lcom/sec/android/mmapp/SoundPickerTabActivity;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setSelected(Landroid/app/Fragment;)V

    .line 319
    :cond_0
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 8
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    const v7, 0x7f0f0029

    const v6, 0x20001

    const/4 v5, 0x1

    .line 254
    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTabSelected mFragment : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mTag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mTag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    if-nez v2, :cond_3

    .line 256
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getNewInstance(ILjava/lang/String;)Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    .line 257
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->mTabSelectedCount:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$100(Lcom/sec/android/mmapp/SoundPickerTabActivity;)I

    move-result v2

    if-nez v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2, v5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setFirstCreated(Z)V

    .line 260
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mTag:Ljava/lang/String;

    invoke-virtual {p2, v7, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 277
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    # operator++ for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->mTabSelectedCount:I
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$108(Lcom/sec/android/mmapp/SoundPickerTabActivity;)I

    .line 278
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    if-ne v2, v6, :cond_2

    .line 279
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mActivity:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v3, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setOnLoadFinishListener(Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;)V

    .line 281
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getTabCount()I

    move-result v3

    # invokes: Lcom/sec/android/mmapp/SoundPickerTabActivity;->getSelectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;
    invoke-static {v2, p1, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$200(Lcom/sec/android/mmapp/SoundPickerTabActivity;Landroid/app/ActionBar$Tab;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 282
    return-void

    .line 262
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {p2, v2}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 263
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2, v5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setFragmentSelected(Z)V

    .line 264
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    if-eq v2, v6, :cond_4

    .line 266
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    invoke-static {v2}, Lcom/sec/android/mmapp/util/MusicListUtils;->getSubTrackList(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "subList":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mmapp/SoundPickerListFragment;

    .line 269
    .local v0, "fg":Lcom/sec/android/mmapp/SoundPickerListFragment;
    if-eqz v0, :cond_1

    .line 270
    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTabSelected do attach!! fg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-virtual {p2, v7, v0, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 274
    .end local v0    # "fg":Lcom/sec/android/mmapp/SoundPickerListFragment;
    .end local v1    # "subList":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setFirstCreated(Z)V

    goto :goto_0
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 5
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 286
    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTabUnselected mFragment : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    if-eqz v2, :cond_0

    .line 288
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {p2, v2}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 289
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setFragmentSelected(Z)V

    .line 291
    :cond_0
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    const v3, 0x20001

    if-ne v2, v3, :cond_2

    .line 292
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsChangingScreen:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$300(Lcom/sec/android/mmapp/SoundPickerTabActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    if-eqz v2, :cond_1

    .line 294
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mFragment:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 310
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getTabCount()I

    move-result v3

    # invokes: Lcom/sec/android/mmapp/SoundPickerTabActivity;->getUnselectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;
    invoke-static {v2, p1, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$400(Lcom/sec/android/mmapp/SoundPickerTabActivity;Landroid/app/ActionBar$Tab;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 311
    return-void

    .line 297
    :cond_2
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mList:I

    invoke-static {v2}, Lcom/sec/android/mmapp/util/MusicListUtils;->getSubTrackList(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "subList":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 299
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_1

    .line 301
    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTabUnselected do detach!! fg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    # getter for: Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsChangingScreen:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->access$300(Lcom/sec/android/mmapp/SoundPickerTabActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    instance-of v2, v0, Lcom/sec/android/mmapp/SoundPickerListFragment;

    if-eqz v2, :cond_1

    .line 305
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;->this$0:Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    goto :goto_0
.end method
