.class public Lcom/sec/android/mmapp/util/AlbumArtUtils;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final DEFAULT_ALBUM_ART:I = 0x7f020014

.field private static final sArtCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static final sArtworkUri:Landroid/net/Uri;

.field private static final sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

.field public static final sDefalutAlbumArtId:[I

.field private static volatile sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    .line 40
    const-string v0, "content://media/external/audio/albumart"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtworkUri:Landroid/net/Uri;

    .line 42
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    .line 191
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f020014

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefalutAlbumArtId:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static clearAlbumArtCache()V
    .locals 2

    .prologue
    .line 47
    sget-object v1, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    monitor-enter v1

    .line 48
    :try_start_0
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 49
    monitor-exit v1

    .line 50
    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getArtworkAndMakeCache(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artIndex"    # J

    .prologue
    .line 127
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 128
    .local v2, "res":Landroid/content/res/Resources;
    const v5, 0x7f09007b

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 129
    .local v3, "size":I
    invoke-static {p0, p1, p2, v3, v3}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 131
    .local v0, "b":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 132
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    .line 133
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 134
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    sget-object v6, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    monitor-enter v6

    .line 136
    :try_start_0
    sget-object v5, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/Drawable;

    .line 137
    .local v4, "value":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_0

    .line 138
    sget-object v5, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    :cond_0
    monitor-exit v6

    .line 156
    :goto_0
    return-object v1

    .line 142
    .end local v4    # "value":Landroid/graphics/drawable/Drawable;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 145
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->getDefaultDrawableImage(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 146
    sget-object v6, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    monitor-enter v6

    .line 148
    :try_start_1
    sget-object v5, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/Drawable;

    .line 149
    .restart local v4    # "value":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_2

    .line 150
    sget-object v5, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    :goto_1
    monitor-exit v6

    goto :goto_0

    .end local v4    # "value":Landroid/graphics/drawable/Drawable;
    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v5

    .line 152
    .restart local v4    # "value":Landroid/graphics/drawable/Drawable;
    :cond_2
    move-object v1, v4

    goto :goto_1
.end method

.method private static getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    const/4 v6, 0x0

    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 69
    .local v3, "res":Landroid/content/ContentResolver;
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtworkUri:Landroid/net/Uri;

    invoke-static {v7, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 70
    .local v5, "uri":Landroid/net/Uri;
    if-eqz v5, :cond_6

    .line 71
    const/4 v2, 0x0

    .line 73
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    const-string v7, "r"

    invoke-virtual {v3, v5, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 74
    if-nez v2, :cond_2

    .line 75
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    const-string v8, "getArtworkQuick() : file does not exist"

    invoke-static {v7, v8}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    if-eqz v2, :cond_0

    .line 116
    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    move-object v0, v6

    .line 123
    .end local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    :cond_1
    :goto_1
    return-object v0

    .line 118
    .restart local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v1

    .line 119
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 79
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    const/4 v8, 0x1

    iput-boolean v8, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 80
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v7, v8, v9}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 90
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    const/4 v8, 0x2

    iput v8, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 91
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    const/4 v8, 0x0

    iput-boolean v8, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 92
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v7, v8, v9}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 95
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    .line 97
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iget v7, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v7, p3, :cond_3

    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iget v7, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v7, p4, :cond_5

    .line 98
    :cond_3
    const/4 v7, 0x1

    invoke-static {v0, p3, p4, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 101
    .local v4, "tmp":Landroid/graphics/Bitmap;
    if-eq v4, v0, :cond_4

    .line 102
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    :cond_4
    move-object v0, v4

    .line 115
    .end local v4    # "tmp":Landroid/graphics/Bitmap;
    :cond_5
    if-eqz v2, :cond_1

    .line 116
    :try_start_3
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 118
    :catch_1
    move-exception v1

    .line 119
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 109
    .end local v0    # "b":Landroid/graphics/Bitmap;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 110
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkQuick "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 115
    if-eqz v2, :cond_6

    .line 116
    :try_start_5
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    :cond_6
    :goto_2
    move-object v0, v6

    .line 123
    goto :goto_1

    .line 118
    .restart local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    :catch_3
    move-exception v1

    .line 119
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 111
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_6
    sget-object v7, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkQuick "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 115
    if-eqz v2, :cond_6

    .line 116
    :try_start_7
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_2

    .line 118
    :catch_5
    move-exception v1

    .line 119
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 114
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 115
    if-eqz v2, :cond_7

    .line 116
    :try_start_8
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 120
    :cond_7
    :goto_3
    throw v6

    .line 118
    :catch_6
    move-exception v1

    .line 119
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public static getCachedArtworkWithoutMaking(J)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p0, "artIndex"    # J

    .prologue
    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    monitor-enter v3

    .line 55
    :try_start_0
    sget-object v2, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sArtCache:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v1, v0

    .line 56
    monitor-exit v3

    .line 57
    return-object v1

    .line 56
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getDefaultDrawableImage(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    .line 160
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getDefaultDrawableImage----------------------------------------------------"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 163
    const-wide/16 p1, 0x0

    .line 166
    :cond_0
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_2

    .line 167
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getDefaultDrawableImage is null, out of sync"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-class v1, Lcom/sec/android/mmapp/util/AlbumArtUtils;

    monitor-enter v1

    .line 169
    :try_start_0
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_1

    .line 170
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    const-string v2, "getDefaultDrawableImage is null, in sync 1"

    invoke-static {v0, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-static {p0}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->makeDefaultDrwableImage(Landroid/content/Context;)V

    .line 173
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_2
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    long-to-int v1, p1

    sget-object v2, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefalutAlbumArtId:[I

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0

    .line 173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static makeDefaultDrwableImage(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    const-string v4, "makeDefaultDrwableImage start"

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    if-nez v3, :cond_0

    .line 198
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 199
    .local v2, "r":Landroid/content/res/Resources;
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefalutAlbumArtId:[I

    array-length v3, v3

    new-array v3, v3, [Landroid/graphics/drawable/BitmapDrawable;

    sput-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    .line 200
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefalutAlbumArtId:[I

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 201
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefalutAlbumArtId:[I

    aget v3, v3, v1

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 202
    .local v0, "b":Landroid/graphics/Bitmap;
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v3, v1

    .line 203
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    aget-object v3, v3, v1

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setFilterBitmap(Z)V

    .line 204
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->sDefaultAlbumArtDrawable:[Landroid/graphics/drawable/BitmapDrawable;

    aget-object v3, v3, v1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setDither(Z)V

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 207
    .end local v0    # "b":Landroid/graphics/Bitmap;
    .end local v1    # "i":I
    .end local v2    # "r":Landroid/content/res/Resources;
    :cond_0
    sget-object v3, Lcom/sec/android/mmapp/util/AlbumArtUtils;->CLASSNAME:Ljava/lang/String;

    const-string v4, "makeDefaultDrwableImage end"

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    return-void
.end method
