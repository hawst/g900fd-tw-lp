.class Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isMusic"    # Z

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;-><init>()V

    .line 341
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->uri:Landroid/net/Uri;

    .line 342
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->projection:[Ljava/lang/String;

    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 344
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->TITLE_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 345
    # invokes: Lcom/sec/android/mmapp/util/MusicListUtils;->getDefaultMusicQueryWhere(Z)Ljava/lang/String;
    invoke-static {p2}, Lcom/sec/android/mmapp/util/MusicListUtils;->access$100(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->selection:Ljava/lang/String;

    .line 346
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 355
    :goto_0
    const-string v0, "_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->audioIdCol:Ljava/lang/String;

    .line 356
    const-string v0, "title"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->text1Col:Ljava/lang/String;

    .line 357
    const-string v0, "artist"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->text2Col:Ljava/lang/String;

    .line 358
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 359
    return-void

    .line 349
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;->selectionArgs:[Ljava/lang/String;

    goto :goto_0
.end method
