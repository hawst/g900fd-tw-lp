.class public Lcom/sec/android/mmapp/SoundPickerListFragment;
.super Lcom/sec/android/mmapp/widget/TwIndexListFragment;
.source "SoundPickerListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;,
        Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/mmapp/widget/TwIndexListFragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/media/MediaPlayer$OnCompletionListener;"
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DO_UPDATE:I = 0x0

.field private static final DO_UPDATE_FORCE:I = 0x1

.field private static final ENABLE_RINGTONE_RECOMMENDER:Ljava/lang/String; = "enable_ringtone_recommender"

.field private static final END_SUBLIST:I = 0x3

.field private static final KEY:Ljava/lang/String; = "key"

.field private static final LIST:Ljava/lang/String; = "list"

.field public static final NORMAL_PLAY_MODE:I = 0x0

.field public static final RECEOMMENDER_PLAY_MODE:I = 0x1

.field private static final UPDATE_SELECTION:I = 0x2

.field private static sIsError:Z


# instance fields
.field private final PERSONAL_FOLDER:Ljava/lang/String;

.field private mAdapter:Landroid/widget/CursorAdapter;

.field private mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

.field private mAlbumIdIdx:I

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioIdIdx:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mCountOfData:I

.field private mCustomOkButton:Landroid/widget/TextView;

.field private mDuration:I

.field private mEnableRingtoneRecommender:Z

.field private final mEndSublistHandler:Landroid/os/Handler;

.field private mIndexIdx:I

.field private mIsFirstCreated:Z

.field private mIsHiddenPersonal:Z

.field private mIsMultiplePicker:Z

.field private mIsPause:Z

.field private mIsSelectedFragment:Z

.field private mKey:Ljava/lang/String;

.field private mKeyWordIdx:I

.field private mList:I

.field private final mListUpdateHandler:Landroid/os/Handler;

.field private mLoadFinishListener:Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;

.field private final mLocaleReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMediaPlayerLock:Ljava/lang/Object;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMenu:Landroid/view/Menu;

.field private mNumberTextId:I

.field private mOffset:I

.field private mOkButton:Landroid/view/MenuItem;

.field private final mOkButtonUpdateHandler:Landroid/os/Handler;

.field private mPlayingId:J

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

.field private mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

.field private mRequestedMediaPlayer:Landroid/media/MediaPlayer;

.field private final mResultListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

.field private mSecAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

.field private final mSelectedCountUpdateHandler:Landroid/os/Handler;

.field private mSelectedId:J

.field private mSelectedUri:Landroid/net/Uri;

.field private mText1Idx:I

.field private mText2Idx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    .line 108
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;-><init>()V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsSelectedFragment:Z

    .line 153
    iput-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    .line 165
    iput-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J

    .line 167
    iput-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    .line 171
    iput-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    .line 173
    iput-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsHiddenPersonal:Z

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mmapp/library/PrivateMode;->getRootDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->PERSONAL_FOLDER:Ljava/lang/String;

    .line 191
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayerLock:Ljava/lang/Object;

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 409
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$1;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mResultListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    .line 434
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$2;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mListUpdateHandler:Landroid/os/Handler;

    .line 590
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$4;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mEndSublistHandler:Landroid/os/Handler;

    .line 600
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$5;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedCountUpdateHandler:Landroid/os/Handler;

    .line 625
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$6;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    .line 1394
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$14;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$14;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 1407
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$15;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$15;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    .line 1448
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$16;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1507
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->updateSelectedCount()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/mmapp/SoundPickerListFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    return-wide v0
.end method

.method static synthetic access$1400(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->updatePlayModeButtonText()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/mmapp/SoundPickerListFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/sec/android/mmapp/SoundPickerListFragment;J)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;
    .param p1, "x1"    # J

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->autoStartAfterToggle(J)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->handleOkButton()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mText2Idx:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mNumberTextId:I

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mText1Idx:I

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioIdIdx:I

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/mmapp/SoundPickerListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->PERSONAL_FOLDER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mDuration:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumIdIdx:I

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/mmapp/SoundPickerListFragment;)Lcom/sec/android/mmapp/util/AlbumArtLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRequestedMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/SoundPickerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOffset:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/mmapp/SoundPickerListFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOffset:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mListUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 94
    sget-boolean v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    return v0
.end method

.method static synthetic access$702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 94
    sput-boolean p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    return p0
.end method

.method static synthetic access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    return-object v0
.end method

.method private addNumberOfView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 271
    if-nez p1, :cond_1

    .line 291
    .end local p1    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 275
    .restart local p1    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 276
    .local v0, "count":I
    if-eqz v0, :cond_0

    .line 280
    new-instance v2, Landroid/database/MatrixCursor;

    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v4, v4, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->projection:[Ljava/lang/String;

    invoke-direct {v2, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 281
    .local v2, "countOfItems":Landroid/database/MatrixCursor;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v4, v4, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->projection:[Ljava/lang/String;

    array-length v4, v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 282
    .local v1, "countItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v4, v4, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->projection:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_3

    .line 283
    if-nez v3, :cond_2

    .line 284
    const-wide/16 v4, -0x65

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 286
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 289
    :cond_3
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 290
    iput v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I

    .line 291
    new-instance v4, Landroid/database/MergeCursor;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/database/Cursor;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-direct {v4, v5}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    move-object p1, v4

    goto :goto_0
.end method

.method private autoStartAfterToggle(J)V
    .locals 7
    .param p1, "playId"    # J

    .prologue
    const/4 v6, 0x0

    .line 1101
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gez v3, :cond_0

    .line 1139
    :goto_0
    return-void

    .line 1105
    :cond_0
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1107
    .local v2, "playUri":Landroid/net/Uri;
    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1108
    sput-boolean v6, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    .line 1110
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1111
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 1112
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1113
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1114
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepare()V

    .line 1115
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v4, Lcom/sec/android/mmapp/SoundPickerListFragment$13;

    invoke-direct {v4, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$13;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1130
    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->play(Ljava/lang/String;)V

    .line 1131
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v3

    iput v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mDuration:I

    .line 1132
    iput-wide p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1133
    .end local v1    # "path":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1135
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    const-string v4, "Unable to play track"

    invoke-static {v3, v4, v0}, Lcom/sec/android/mmapp/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1136
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c003e

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 562
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v1

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 566
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 568
    .local v7, "path":Ljava/lang/String;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v8, :cond_0

    .line 569
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 570
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 573
    :cond_0
    if-eqz v6, :cond_1

    .line 574
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 577
    :cond_1
    return-object v7

    .line 573
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 574
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getColumnIndicesAndSetIndex(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 302
    if-eqz p1, :cond_1

    .line 303
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioIdIdx:I

    .line 304
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->keyWord:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKeyWordIdx:I

    .line 305
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->albumIdCol:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumIdIdx:I

    .line 306
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mText1Idx:I

    .line 307
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mText2Idx:I

    .line 308
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->indexBy:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 309
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->indexBy:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIndexIdx:I

    .line 311
    :cond_0
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIndexIdx:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 312
    iget v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIndexIdx:I

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setIndexer(Landroid/database/Cursor;I)V

    .line 315
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0f0014

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 316
    .local v1, "rootView":Landroid/widget/FrameLayout;
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 317
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_3

    .line 318
    :cond_2
    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 319
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setIndexViewVisibility(I)V

    .line 325
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 326
    return-void

    .line 323
    :cond_3
    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setIndexViewVisibility(I)V

    goto :goto_0
.end method

.method private getKeyWord(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1420
    const-string v2, ""

    .line 1421
    .local v2, "keyWord":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1422
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    iget v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKeyWordIdx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 1423
    iget v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKeyWordIdx:I

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1425
    :try_start_0
    iget v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKeyWordIdx:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1431
    :cond_0
    :goto_0
    return-object v2

    .line 1426
    :catch_0
    move-exception v1

    .line 1427
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getNewInstance(ILjava/lang/String;)Lcom/sec/android/mmapp/SoundPickerListFragment;
    .locals 3
    .param p0, "list"    # I
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 196
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 197
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    const-string v2, "key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    new-instance v1, Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;-><init>()V

    .line 200
    .local v1, "fg":Lcom/sec/android/mmapp/SoundPickerListFragment;
    invoke-virtual {v1, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 201
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setRetainInstance(Z)V

    .line 202
    return-object v1
.end method

.method private getSelectedItemPath([J)Ljava/util/ArrayList;
    .locals 8
    .param p1, "ids"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1303
    if-eqz p1, :cond_0

    array-length v7, p1

    if-nez v7, :cond_2

    .line 1304
    :cond_0
    const/4 v5, 0x0

    .line 1314
    :cond_1
    return-object v5

    .line 1306
    :cond_2
    const/4 v6, 0x0

    .line 1307
    .local v6, "uri":Landroid/net/Uri;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1309
    .local v5, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p1

    .local v0, "arr$":[J
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-wide v2, v0, v1

    .line 1310
    .local v2, "id":J
    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 1311
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {p0, v7, v6}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1309
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private handleOkButton()Z
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 1277
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1278
    .local v0, "a":Landroid/app/Activity;
    const/4 v2, 0x0

    .line 1279
    .local v2, "outcome":Z
    iget-boolean v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v4, :cond_2

    .line 1280
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1281
    .local v3, "result":Landroid/content/Intent;
    iget-boolean v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsHiddenPersonal:Z

    if-eqz v4, :cond_1

    .line 1282
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getAlignedSelectedItemId()[J

    move-result-object v1

    .line 1283
    .local v1, "ids":[J
    const-string v4, "selectedList"

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getSelectedItemPath([J)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1287
    .end local v1    # "ids":[J
    :goto_0
    invoke-virtual {v0, v8, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1288
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1289
    const/4 v2, 0x1

    .line 1299
    .end local v3    # "result":Landroid/content/Intent;
    :cond_0
    :goto_1
    return v2

    .line 1285
    .restart local v3    # "result":Landroid/content/Intent;
    :cond_1
    const-string v4, "selectedList"

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getAlignedSelectedItemId()[J

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    goto :goto_0

    .line 1291
    .end local v3    # "result":Landroid/content/Intent;
    :cond_2
    iget-wide v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    .line 1292
    sget-object v4, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onClick done selected Uri is : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " this : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedUri:Landroid/net/Uri;

    iget v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOffset:I

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->getResultIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v0, v8, v4}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1296
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method private initCustomActionBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 897
    sget-boolean v2, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v2, :cond_0

    .line 898
    iget-boolean v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v2, :cond_1

    .line 899
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->makeSelectHeaderView()V

    .line 939
    :cond_0
    :goto_0
    return-void

    .line 901
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 902
    .local v0, "bar":Landroid/app/ActionBar;
    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 903
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040002

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 905
    .local v1, "customView":Landroid/view/View;
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 906
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 908
    sget-boolean v2, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v2, :cond_2

    .line 909
    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 910
    const v2, 0x7f0f000e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 913
    :cond_2
    const v2, 0x7f0f000d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    .line 914
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 915
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    const v3, 0x3f333333    # 0.7f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 916
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/mmapp/SoundPickerListFragment$9;

    invoke-direct {v3, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$9;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 923
    const v2, 0x7f0f000c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/android/mmapp/SoundPickerListFragment$10;

    invoke-direct {v3, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$10;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private isEnableSelect()Z
    .locals 2

    .prologue
    .line 1044
    const/4 v0, 0x0

    .line 1045
    .local v0, "result":Z
    iget v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v1}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v1, :cond_0

    .line 1046
    const/4 v0, 0x1

    .line 1048
    :cond_0
    return v0
.end method

.method private makeSelectHeaderView()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 973
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 975
    .local v0, "a":Landroid/app/Activity;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f04000b

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 977
    .local v3, "actionModeView":Landroid/view/View;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    const/4 v8, -0x1

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v6, v0

    .line 981
    check-cast v6, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getSelectedItemsCount()I

    move-result v5

    .line 983
    .local v5, "totalSelectedcount":I
    const v6, 0x7f0f0026

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 984
    .local v2, "actionBarTitle":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0031

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 986
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 987
    .local v1, "ab":Landroid/app/ActionBar;
    if-eqz v1, :cond_0

    .line 988
    const/16 v6, 0x10

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 989
    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 991
    :cond_0
    const v6, 0x7f0f0024

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 992
    .local v4, "selectAllLayout":Landroid/view/View;
    const v6, 0x7f0f0025

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 993
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 994
    iget v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v6}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 995
    invoke-virtual {v4, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 996
    const v6, 0x3f19999a    # 0.6f

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    .line 1041
    :goto_0
    return-void

    .line 998
    :cond_1
    new-instance v6, Lcom/sec/android/mmapp/SoundPickerListFragment$11;

    invoke-direct {v6, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$11;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private play(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 345
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 346
    const v1, 0x7f0c0050

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 357
    :goto_0
    return-void

    .line 351
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->requestAudioFocus()V

    .line 352
    if-eqz p1, :cond_1

    .line 353
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->startMediaPlayer(Ljava/lang/String;)V

    goto :goto_0

    .line 355
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method

.method private registerLocaleReceiver()V
    .locals 3

    .prologue
    .line 1389
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1390
    .local v0, "localeFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1391
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1392
    return-void
.end method

.method private registerMediaScannerReceiver()V
    .locals 3

    .prologue
    .line 1378
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1379
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1380
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1381
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1382
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1383
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1384
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1385
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1386
    return-void
.end method

.method private releaseRecommander()V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->close()Z

    .line 398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 403
    :cond_1
    return-void
.end method

.method private requestAudioFocus()V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-nez v0, :cond_0

    .line 332
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    const-string v1, "requestAudioFocus fail. it may be during call"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_0
    return-void
.end method

.method private setNoItemBackground(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 1253
    return-void
.end method

.method private setSelected(IJ)V
    .locals 10
    .param p1, "position"    # I
    .param p2, "newId"    # J

    .prologue
    .line 458
    const-wide/16 v6, 0x0

    cmp-long v5, p2, v6

    if-gez v5, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 464
    .local v3, "newUri":Landroid/net/Uri;
    iget-wide v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J

    cmp-long v5, p2, v6

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v5, :cond_7

    .line 465
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 466
    new-instance v5, Landroid/media/MediaPlayer;

    invoke-direct {v5}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 467
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    .line 469
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 470
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 476
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mmapp/library/DrmManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/DrmManager;

    move-result-object v1

    .line 477
    .local v1, "drmManager":Lcom/sec/android/mmapp/library/DrmManager;
    invoke-virtual {v1, v4}, Lcom/sec/android/mmapp/library/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v1, v4}, Lcom/sec/android/mmapp/library/DrmManager;->supportRingtone(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 479
    const v5, 0x7f0c003e

    const/4 v6, 0x0

    invoke-static {v0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 517
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "drmManager":Lcom/sec/android/mmapp/library/DrmManager;
    .end local v4    # "path":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 519
    .local v2, "e":Ljava/io/IOException;
    iget-wide p2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    .line 520
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedUri:Landroid/net/Uri;

    .line 521
    sget-object v5, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    const-string v6, "Unable to play track"

    invoke-static {v5, v6, v2}, Lcom/sec/android/mmapp/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 522
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c003e

    invoke-virtual {p0, v6}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 546
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :goto_1
    iput-wide p2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    .line 547
    iput-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedUri:Landroid/net/Uri;

    .line 548
    iget-wide v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_4

    sget-boolean v5, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    if-nez v5, :cond_4

    .line 549
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, p1, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 550
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    if-nez v5, :cond_b

    .line 551
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 552
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 558
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->invalidateViews()V

    goto/16 :goto_0

    .line 487
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "drmManager":Lcom/sec/android/mmapp/library/DrmManager;
    .restart local v4    # "path":Ljava/lang/String;
    :cond_5
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->PERSONAL_FOLDER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f0c003c

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->invalidateViews()V

    goto/16 :goto_0

    .line 495
    :cond_6
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, v4}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 496
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 497
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 498
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->prepare()V

    .line 499
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v6, Lcom/sec/android/mmapp/SoundPickerListFragment$3;

    invoke-direct {v6, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$3;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 514
    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/SoundPickerListFragment;->play(Ljava/lang/String;)V

    .line 515
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mDuration:I

    .line 516
    iput-wide p2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 525
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "drmManager":Lcom/sec/android/mmapp/library/DrmManager;
    .end local v4    # "path":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v5, :cond_3

    .line 526
    iget-wide v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J

    cmp-long v5, p2, v6

    if-nez v5, :cond_a

    .line 527
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 528
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 529
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->pause()V

    goto/16 :goto_1

    .line 531
    :cond_8
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRequestedMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eq v5, v6, :cond_9

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v5}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getPlayMode()I

    move-result v5

    if-ne v6, v5, :cond_9

    .line 535
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 536
    .restart local v4    # "path":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/SoundPickerListFragment;->play(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 538
    .end local v4    # "path":Ljava/lang/String;
    :cond_9
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->play(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 542
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    goto/16 :goto_1

    .line 554
    :cond_b
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_2
.end method

.method private startMediaPlayer(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 360
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->releaseRecommander()V

    .line 362
    new-instance v2, Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-direct {v2}, Lcom/sec/android/mmapp/library/RingtoneRecommender;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .line 364
    iput v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOffset:I

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 367
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 368
    .local v1, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getPlayMode()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 370
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-virtual {v2}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->isOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 371
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-virtual {v2, p1}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->open(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 386
    :pswitch_0
    const v2, 0x7f0c0022

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 387
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 373
    :pswitch_1
    const/4 v2, 0x0

    const v3, 0x7f0c003f

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v0, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 375
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRecommander:Lcom/sec/android/mmapp/library/RingtoneRecommender;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mResultListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->doExtract(Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;)Z

    .line 376
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    iput-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mRequestedMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 379
    :pswitch_2
    const v2, 0x7f0c0021

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 381
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_0

    .line 391
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    .line 371
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updatePlayModeButtonText()V
    .locals 3

    .prologue
    .line 1435
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1436
    .local v0, "a":Landroid/app/Activity;
    check-cast v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    .end local v0    # "a":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getPlayMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1446
    :goto_0
    return-void

    .line 1438
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 1441
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 1436
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateSelectedCount()V
    .locals 12

    .prologue
    const v11, 0x7f0c0031

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1052
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1053
    .local v0, "a":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 1098
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v6, v0

    .line 1057
    check-cast v6, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getSelectedItemsCount()I

    move-result v5

    .line 1058
    .local v5, "totalSelectedcount":I
    iget-boolean v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v6, :cond_8

    .line 1059
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v6

    const v9, 0x7f0f0026

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1061
    .local v1, "actionBarTitle":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 1062
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-array v9, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v8

    invoke-virtual {v6, v11, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1065
    :cond_2
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    if-eqz v6, :cond_3

    .line 1066
    iget-object v9, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    if-lez v5, :cond_7

    move v6, v7

    :goto_1
    invoke-interface {v9, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1072
    .end local v1    # "actionBarTitle":Landroid/widget/TextView;
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v2

    .line 1073
    .local v2, "currentSelectedcount":I
    iget v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I

    if-ne v2, v6, :cond_9

    .line 1074
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1079
    :goto_3
    sget-boolean v6, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    :cond_4
    sget-boolean v6, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    if-eqz v6, :cond_0

    .line 1084
    :cond_5
    const/4 v3, 0x0

    .line 1085
    .local v3, "enabled":Z
    if-lez v5, :cond_6

    .line 1086
    const/4 v3, 0x1

    .line 1088
    :cond_6
    sget-boolean v6, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v6, :cond_b

    .line 1089
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1090
    if-eqz v3, :cond_a

    .line 1091
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0

    .end local v2    # "currentSelectedcount":I
    .end local v3    # "enabled":Z
    .restart local v1    # "actionBarTitle":Landroid/widget/TextView;
    :cond_7
    move v6, v8

    .line 1066
    goto :goto_1

    .line 1069
    .end local v1    # "actionBarTitle":Landroid/widget/TextView;
    :cond_8
    invoke-virtual {v0, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v9, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v8

    invoke-static {v6, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1070
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1076
    .end local v4    # "title":Ljava/lang/String;
    .restart local v2    # "currentSelectedcount":I
    :cond_9
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_3

    .line 1093
    .restart local v3    # "enabled":Z
    :cond_a
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    const v7, 0x3f333333    # 0.7f

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_0

    .line 1096
    :cond_b
    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method


# virtual methods
.method public getAlignedSelectedItemId()[J
    .locals 8

    .prologue
    .line 1323
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    .line 1324
    .local v4, "lv":Landroid/widget/ListView;
    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v5

    .line 1325
    .local v5, "sp":Landroid/util/SparseBooleanArray;
    if-nez v5, :cond_1

    .line 1326
    const/4 v6, 0x0

    new-array v3, v6, [J

    .line 1340
    :cond_0
    return-object v3

    .line 1328
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1329
    .local v2, "idStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 1330
    invoke-virtual {v5, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1331
    invoke-virtual {v5, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1329
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1334
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1335
    .local v0, "count":I
    new-array v3, v0, [J

    .line 1337
    .local v3, "ids":[J
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    .line 1338
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    .line 1337
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 756
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityCreated : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    .line 760
    .local v9, "fm":Landroid/app/FragmentManager;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual {v9}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-ge v10, v0, :cond_0

    .line 761
    invoke-virtual {v9}, Landroid/app/FragmentManager;->popBackStack()V

    .line 760
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 764
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "enable_ringtone_recommender"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mEnableRingtoneRecommender:Z

    .line 767
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 768
    .local v10, "i":Landroid/content/Intent;
    const-string v0, "isMultiple"

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    .line 769
    const-string v0, "isHiddenPersonal"

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsHiddenPersonal:Z

    .line 771
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v0, :cond_1

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    .line 773
    .local v7, "bar":Landroid/app/ActionBar;
    const/16 v0, 0xe

    invoke-virtual {v7, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 777
    .end local v7    # "bar":Landroid/app/ActionBar;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    .line 778
    .local v8, "data":Landroid/os/Bundle;
    const-string v0, "list"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    .line 779
    const-string v0, "key"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKey:Ljava/lang/String;

    .line 781
    const-string v0, "isMusic"

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 782
    .local v12, "isMusic":Z
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKey:Ljava/lang/String;

    invoke-static {v0, v1, v12}, Lcom/sec/android/mmapp/util/MusicListUtils;->getMusicListInfo(ILjava/lang/String;Z)Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;

    move-result-object v11

    .line 783
    .local v11, "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    if-nez v11, :cond_3

    .line 891
    :goto_2
    return-void

    .line 764
    .end local v8    # "data":Landroid/os/Bundle;
    .end local v11    # "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    .end local v12    # "isMusic":Z
    .local v10, "i":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 785
    .restart local v8    # "data":Landroid/os/Bundle;
    .local v10, "i":Landroid/content/Intent;
    .restart local v11    # "info":Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
    .restart local v12    # "isMusic":Z
    :cond_3
    iget-object v0, v11, Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    .line 786
    iget v0, v11, Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;->numberOfTextId:I

    iput v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mNumberTextId:I

    .line 788
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 789
    .local v2, "a":Landroid/app/Activity;
    instance-of v0, v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    if-eqz v0, :cond_5

    .line 791
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 792
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityCreated :isDetached "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->isDetached()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " isRemoving "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->isRemoving()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v0, :cond_6

    .line 795
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 796
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->makeSelectHeaderView()V

    .line 807
    :goto_3
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 815
    :cond_4
    :goto_4
    const v0, 0x7f0f002a

    invoke-virtual {v2, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 824
    .local v6, "autoRecommendationSelection":Landroid/view/View;
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$7;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 831
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mEnableRingtoneRecommender:Z

    if-eqz v0, :cond_5

    .line 832
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 833
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$8;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment$8;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;Landroid/app/Activity;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 860
    const v0, 0x7f0f002b

    invoke-virtual {v2, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

    .line 862
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 863
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->updatePlayModeButtonText()V

    .line 864
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getListType(I)I

    move-result v0

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsFirstCreated:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAutoRecommendationCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    .line 868
    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    .line 873
    .end local v6    # "autoRecommendationSelection":Landroid/view/View;
    :cond_5
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    const v3, 0x7f04000e

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    .line 874
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 875
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setListShown(Z)V

    .line 878
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, v11, Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;->noItemTextId:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mKey:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setEmptyText(Ljava/lang/CharSequence;ILjava/lang/String;)V

    .line 882
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 883
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->registerMediaScannerReceiver()V

    .line 884
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->registerLocaleReceiver()V

    .line 886
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->initCustomActionBar()V

    .line 887
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setCustomActionBarLayoutParams()V

    .line 888
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setNoItemBackground(I)V

    .line 890
    invoke-super {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    goto/16 :goto_2

    .line 798
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto/16 :goto_3

    .line 809
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v0, :cond_4

    .line 810
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->makeSelectHeaderView()V

    .line 811
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->updateSelectedCount()V

    goto/16 :goto_4
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 711
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttach : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    invoke-super {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onAttach(Landroid/app/Activity;)V

    .line 713
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-ne v0, p1, :cond_0

    .line 583
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->releaseMediaPlayer()V

    .line 584
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    if-nez v0, :cond_0

    .line 585
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 588
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1235
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMenu:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setButtonStyle(Landroid/view/Menu;)V

    .line 1236
    invoke-super {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1239
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1244
    :goto_0
    return-void

    .line 1242
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setCustomActionBarLayoutParams()V

    .line 1243
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setNoItemBackground(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 717
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    invoke-static {}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/sec/android/mmapp/util/AlbumArtLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    .line 720
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mmapp/library/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/SecAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSecAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

    .line 722
    if-eqz p1, :cond_1

    .line 723
    const-string v0, "selectedId"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    .line 724
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 725
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedUri:Landroid/net/Uri;

    .line 728
    :cond_0
    const-string v0, "isError"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    .line 729
    sget-boolean v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    if-eqz v0, :cond_1

    .line 730
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v0, :cond_2

    .line 731
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 733
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 743
    :cond_1
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setHasOptionsMenu(Z)V

    .line 744
    invoke-super {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 745
    return-void

    .line 736
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    sget-object v1, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v2, v2, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v3, v3, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v4, v4, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v5, v5, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mQueryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    iget-object v6, v6, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    .local v0, "cl":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 218
    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v2, 0x7f0f000d

    const/4 v1, 0x0

    .line 1209
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v0, :cond_2

    .line 1210
    const/high16 v0, 0x7f0e0000

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1211
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    .line 1212
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1213
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    const/4 v2, 0x6

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1215
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getSelectedItemsCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1216
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMenu:Landroid/view/Menu;

    .line 1217
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMenu:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setButtonStyle(Landroid/view/Menu;)V

    .line 1230
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1215
    goto :goto_0

    .line 1218
    :cond_2
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1219
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-nez v0, :cond_0

    .line 1220
    const v0, 0x7f0e0002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1221
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    .line 1222
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-nez v0, :cond_3

    iget-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    if-gtz v0, :cond_5

    .line 1224
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1226
    :cond_5
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMenu:Landroid/view/Menu;

    .line 1227
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMenu:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setButtonStyle(Landroid/view/Menu;)V

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 749
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->release()V

    .line 1193
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 1195
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onDestroy()V

    .line 1196
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 1182
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroyView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1185
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1186
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1187
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onDestroyView()V

    .line 1188
    return-void
.end method

.method public onDetach()V
    .locals 3

    .prologue
    .line 1200
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDetach : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onDetach()V

    .line 1203
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 1345
    sget-object v5, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isThis fragment paused ? "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " onListItemClick position : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " this : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    iget-boolean v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsSelectedFragment:Z

    if-nez v5, :cond_1

    .line 1375
    :cond_0
    :goto_0
    return-void

    .line 1350
    :cond_1
    iget v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v5}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1351
    iget-boolean v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v5, :cond_3

    .line 1352
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    .line 1353
    .local v3, "lv":Landroid/widget/ListView;
    invoke-virtual {v3, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1354
    const/4 v5, 0x1

    invoke-virtual {v3, p3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1358
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v3, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v6

    invoke-virtual {v5, v6, p4, p5}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->updateSelectedIDs(ZJ)V

    .line 1360
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->updateSelectedCount()V

    goto :goto_0

    .line 1356
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v3, p3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_1

    .line 1362
    .end local v3    # "lv":Landroid/widget/ListView;
    :cond_3
    invoke-direct {p0, p3, p4, p5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setSelected(IJ)V

    goto :goto_0

    .line 1366
    :cond_4
    invoke-direct {p0, p3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v2

    .line 1367
    .local v2, "key":Ljava/lang/String;
    iget v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v5}, Lcom/sec/android/mmapp/util/MusicListUtils;->getSubTrackList(I)I

    move-result v4

    .line 1368
    .local v4, "subList":I
    invoke-static {v4, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getNewInstance(ILjava/lang/String;)Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-result-object v0

    .line 1369
    .local v0, "fg":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1370
    .local v1, "ft":Landroid/app/FragmentTransaction;
    const v5, 0x7f0f0029

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v0, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1372
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1373
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 6
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v4, 0x0

    .line 223
    sget-object v2, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLoadFinished : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " data.getCount(): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_4

    const-string v1, "null"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 227
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v1, :cond_5

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 229
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    if-eqz v1, :cond_1

    .line 230
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 236
    .end local v0    # "a":Landroid/app/Activity;
    :cond_1
    :goto_1
    invoke-direct {p0, p2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-direct {p0, p2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->addNumberOfView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 238
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setListShown(Z)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 240
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButtonUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 241
    iget-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v1, :cond_2

    .line 242
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedCountUpdateHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 244
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mLoadFinishListener:Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;

    if-eqz v1, :cond_3

    .line 245
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mLoadFinishListener:Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;

    invoke-interface {v1, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;->onLoadFinished(Lcom/sec/android/mmapp/SoundPickerListFragment;)V

    .line 247
    :cond_3
    return-void

    .line 223
    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 232
    :cond_5
    iget v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I

    invoke-static {v1}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mEndSublistHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 94
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoaderReset : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 264
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1257
    iget-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    if-nez v1, :cond_0

    .line 1258
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1273
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_1
    return v1

    .line 1260
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->handleOkButton()Z

    move-result v0

    .line 1261
    .local v0, "result":Z
    if-eqz v0, :cond_0

    .line 1262
    const/4 v1, 0x1

    goto :goto_1

    .line 1267
    .end local v0    # "result":Z
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1258
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f0f000c -> :sswitch_1
        0x7f0f000d -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1167
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    .line 1170
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onPause()V

    .line 1171
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 1159
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 1161
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onResume()V

    .line 1162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    .line 1163
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1150
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const-string v0, "selectedId"

    iget-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1153
    const-string v0, "isError"

    sget-boolean v1, Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1154
    invoke-super {p0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1155
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 1143
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onStart()V

    .line 1146
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 1175
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStop : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->releaseRecommander()V

    .line 1177
    invoke-super {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->onStop()V

    .line 1178
    return-void
.end method

.method public releaseMediaPlayer()V
    .locals 4

    .prologue
    .line 699
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 700
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 702
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 703
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 704
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J

    .line 706
    :cond_0
    monitor-exit v1

    .line 707
    return-void

    .line 706
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resetSelectedId()V
    .locals 2

    .prologue
    .line 676
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J

    .line 677
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z

    if-nez v0, :cond_0

    .line 678
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setAllItemChecked(Z)V

    .line 680
    :cond_0
    return-void
.end method

.method public setAllItemChecked(Z)V
    .locals 4
    .param p1, "isChecked"    # Z

    .prologue
    .line 665
    iget v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I

    if-lez v1, :cond_0

    .line 666
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I

    if-ge v0, v1, :cond_0

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v2

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->updateSelectedIDs(ZJ)V

    .line 669
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 666
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 672
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 673
    return-void
.end method

.method public setButtonStyle(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0f000c

    const v4, 0x7f0f000d

    const/4 v3, 0x6

    const/4 v2, 0x2

    .line 1472
    if-nez p1, :cond_1

    .line 1501
    :cond_0
    :goto_0
    return-void

    .line 1476
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 1477
    .local v0, "orientation":I
    if-ne v0, v2, :cond_3

    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-nez v1, :cond_3

    .line 1479
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-eqz v1, :cond_2

    .line 1480
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    .line 1484
    :cond_2
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020007

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1488
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020006

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0

    .line 1494
    :cond_3
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-nez v1, :cond_0

    .line 1495
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1497
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0
.end method

.method public setCustomActionBarLayoutParams()V
    .locals 7

    .prologue
    .line 945
    sget-boolean v4, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z

    if-nez v4, :cond_3

    .line 946
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 947
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v3, -0x1

    .line 948
    .local v3, "width":I
    const/4 v2, 0x3

    .line 949
    .local v2, "gravity":I
    sget-boolean v4, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 951
    :cond_0
    const/4 v3, -0x2

    .line 952
    const/4 v2, 0x5

    .line 954
    :cond_1
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 955
    .local v1, "cv":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 956
    instance-of v4, v1, Landroid/widget/LinearLayout;

    if-eqz v4, :cond_2

    move-object v4, v1

    .line 957
    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090041

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setDividerPadding(I)V

    .line 960
    :cond_2
    new-instance v4, Landroid/app/ActionBar$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v3, v5, v2}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 970
    .end local v0    # "bar":Landroid/app/ActionBar;
    .end local v1    # "cv":Landroid/view/View;
    .end local v2    # "gravity":I
    .end local v3    # "width":I
    :cond_3
    return-void
.end method

.method public setFirstCreated(Z)V
    .locals 0
    .param p1, "firstCreated"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsFirstCreated:Z

    .line 207
    return-void
.end method

.method public setFragmentSelected(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 1504
    iput-boolean p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsSelectedFragment:Z

    .line 1505
    return-void
.end method

.method public setOnLoadFinishListener(Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mLoadFinishListener:Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;

    .line 255
    return-void
.end method

.method public setSelected(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 446
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 447
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/CursorAdapter;->getItemId(I)J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 448
    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setSelected(IJ)V

    .line 452
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 453
    .local v1, "lv":Landroid/widget/ListView;
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 454
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 455
    return-void

    .line 446
    .end local v1    # "lv":Landroid/widget/ListView;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public stopMediaPlayer()V
    .locals 3

    .prologue
    .line 683
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 684
    .local v0, "adapter":Landroid/widget/ListAdapter;
    instance-of v1, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    if-eqz v1, :cond_0

    .line 685
    check-cast v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    invoke-virtual {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->releaseProgressHandler()V

    .line 687
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 688
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->cancel()V

    .line 691
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->releaseMediaPlayer()V

    .line 693
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_2

    .line 694
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 696
    :cond_2
    return-void
.end method
