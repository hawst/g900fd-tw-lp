.class public Lcom/sec/android/mmapp/library/SvoiceManager;
.super Ljava/lang/Object;
.source "SvoiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;,
        Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;
    }
.end annotation


# static fields
.field public static final ACTION_INTENT_LAUNCH_VOICE_SETTING_BARGEIN:Ljava/lang/String; = "android.intent.action.VOICE_SETTING_BARGEIN"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DELAY_SHOW_NOTIFICATION:I = 0xc8

.field private static final KEY_HDMI_AUDIO_OUTPUT:Ljava/lang/String; = "hdmi_audio_output"

.field public static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"

.field public static final KEY_VOICE_INPUT_CONTROL_MUSIC:Ljava/lang/String; = "voice_input_control_music"

.field public static final MUSIC_SPEECH_COMMAND:I = 0x4

.field public static final MUSIC_SPEECH_FIRST_COMMAND:I = 0x1

.field public static final MUSIC_SPEECH_LAST_COMMAND:I = 0x6

.field public static final MUSIC_SPEECH_NEXT:I = 0x1

.field public static final MUSIC_SPEECH_PAUSE:I = 0x3

.field public static final MUSIC_SPEECH_PLAY:I = 0x4

.field public static final MUSIC_SPEECH_PREV:I = 0x2

.field public static final MUSIC_SPEECH_VOLUME_DOWN:I = 0x6

.field public static final MUSIC_SPEECH_VOLUME_UP:I = 0x5

.field public static final NOTI_STATUS:I = 0x7010003

.field private static final SHOW_NOTIFICATION:I

.field private static sSecRecognizer:Lcom/sec/android/mmapp/library/SvoiceManager;


# instance fields
.field private mAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

.field private mContext:Landroid/content/Context;

.field private mIsStart:Z

.field private final mNotificationHandler:Landroid/os/Handler;

.field private mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field private mSvoiceListner:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/android/mmapp/library/SvoiceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mIsStart:Z

    .line 522
    new-instance v1, Lcom/sec/android/mmapp/library/SvoiceManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mmapp/library/SvoiceManager$2;-><init>(Lcom/sec/android/mmapp/library/SvoiceManager;)V

    iput-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mNotificationHandler:Landroid/os/Handler;

    .line 180
    :try_start_0
    new-instance v1, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 181
    iput-object p1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    .line 182
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/mmapp/library/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/SecAudioManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_2

    .line 194
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    new-instance v2, Lcom/sec/android/mmapp/library/SvoiceManager$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mmapp/library/SvoiceManager$1;-><init>(Lcom/sec/android/mmapp/library/SvoiceManager;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->InitBargeInRecognizer(Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;)V

    .line 219
    :cond_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "not support barge in recognizer in this model"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iput-object v3, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    goto :goto_0

    .line 186
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 187
    .local v0, "e":Ljava/lang/ExceptionInInitializerError;
    sget-object v1, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "not support barge in recognizer in this model"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iput-object v3, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    goto :goto_0

    .line 189
    .end local v0    # "e":Ljava/lang/ExceptionInInitializerError;
    :catch_2
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v1, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "not support barge in recognizer in this model"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iput-object v3, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/mmapp/library/SvoiceManager;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SvoiceManager;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/library/SvoiceManager;)Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SvoiceManager;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mSvoiceListner:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/library/SvoiceManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SvoiceManager;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mIsStart:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/library/SvoiceManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SvoiceManager;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/library/SvoiceManager;Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/SvoiceManager;
    .param p1, "x1"    # Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/library/SvoiceManager;->showBargeInNotification(Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/SvoiceManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 165
    invoke-static {p0}, Lcom/sec/android/mmapp/library/SvoiceManager;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "KNOX mode : return null"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v0, 0x0

    .line 175
    :goto_0
    return-object v0

    .line 172
    :cond_0
    sget-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->sSecRecognizer:Lcom/sec/android/mmapp/library/SvoiceManager;

    if-nez v0, :cond_1

    .line 173
    new-instance v0, Lcom/sec/android/mmapp/library/SvoiceManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/library/SvoiceManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->sSecRecognizer:Lcom/sec/android/mmapp/library/SvoiceManager;

    .line 175
    :cond_1
    sget-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->sSecRecognizer:Lcom/sec/android/mmapp/library/SvoiceManager;

    goto :goto_0
.end method

.method private hideBargeInNotification()V
    .locals 3

    .prologue
    .line 490
    iget-object v1, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 492
    .local v0, "manager":Landroid/app/NotificationManager;
    const v1, 0x7010003

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 493
    return-void
.end method

.method private isEnabledBargeIn()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 584
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "isBargeInEnabled"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 585
    .local v1, "enabed":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 586
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 587
    iget-object v2, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 598
    .end local v1    # "enabed":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 589
    :catch_0
    move-exception v0

    .line 590
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v2, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v4, "isEnabledBargeIn() - NoSuchMethodException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :cond_0
    :goto_1
    move v2, v3

    .line 598
    goto :goto_0

    .line 591
    :catch_1
    move-exception v0

    .line 592
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v4, "isEnabledBargeIn() - IllegalArgumentException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 593
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 594
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v2, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v4, "isEnabledBargeIn() - IllegalAccessException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 595
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 596
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v2, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v4, "isEnabledBargeIn() - InvocationTargetException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static isExistsSvoiceApp(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 547
    const/4 v1, 0x0

    .line 549
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz p0, :cond_0

    .line 550
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.vlingo.midas"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 557
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 558
    const/4 v2, 0x0

    .line 560
    :goto_1
    return v2

    .line 553
    :catch_0
    move-exception v0

    .line 554
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 560
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private static isKnoxMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 532
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sec_container_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    const/4 v0, 0x1

    .line 536
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSupportSvoice(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "country"    # Ljava/lang/String;
    .param p2, "lan"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 504
    const-string v1, "pt"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BR"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Taiwan"

    sget-object v2, Lcom/sec/android/mmapp/library/MusicFeatures;->SYSTEM_CONTRY_CODE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Hong Kong"

    sget-object v2, Lcom/sec/android/mmapp/library/MusicFeatures;->SYSTEM_CONTRY_CODE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 509
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->FRENCH:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->ITALIAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ru"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "es"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "es-rES"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "es-rUS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "zh"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private showBargeInNotification(Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;)V
    .locals 17
    .param p1, "info"    # Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;

    .prologue
    .line 420
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 421
    .local v8, "res":Landroid/content/res/Resources;
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 422
    .local v1, "config":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 423
    .local v3, "globalLocale":Ljava/util/Locale;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "voicetalk_language"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 425
    .local v10, "voiceLocale":Ljava/lang/String;
    if-nez v10, :cond_0

    .line 429
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 430
    .local v4, "lan":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    .line 431
    .local v2, "country":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/sec/android/mmapp/library/SvoiceManager;->isSupportSvoice(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 432
    move-object v10, v4

    .line 438
    .end local v2    # "country":Ljava/lang/String;
    .end local v4    # "lan":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string v13, "v-es-LA"

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 439
    const-string v10, "es"

    .line 445
    :cond_1
    const-string v13, "zh"

    invoke-virtual {v10, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string v13, "pt"

    invoke-virtual {v10, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 446
    :cond_2
    const/4 v11, 0x0

    .line 447
    .local v11, "voiceLocaleArray":[Ljava/lang/String;
    const-string v13, "-"

    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 450
    array-length v13, v11

    const/4 v14, 0x1

    if-le v13, v14, :cond_3

    .line 451
    new-instance v13, Ljava/util/Locale;

    const/4 v14, 0x0

    aget-object v14, v11, v14

    const/4 v15, 0x1

    aget-object v15, v11, v15

    invoke-direct {v13, v14, v15}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v13, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 469
    .end local v11    # "voiceLocaleArray":[Ljava/lang/String;
    :cond_3
    :goto_1
    const/4 v13, 0x0

    invoke-virtual {v8, v1, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 472
    iput-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 473
    const/4 v13, 0x0

    invoke-virtual {v8, v1, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 475
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    new-instance v15, Landroid/content/Intent;

    const-string v16, "android.intent.action.VOICE_SETTING_BARGEIN"

    invoke-direct/range {v15 .. v16}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v16, 0x0

    invoke-static/range {v13 .. v16}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 477
    .local v7, "pi":Landroid/app/PendingIntent;
    new-instance v13, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    invoke-direct {v13, v14}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p1

    iget v14, v0, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->iconId:I

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->notiString:Ljava/lang/String;

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->appName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->notiString:Ljava/lang/String;

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-virtual {v13, v7}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v6

    .line 481
    .local v6, "notification":Landroid/app/Notification;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    const-string v14, "notification"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 483
    .local v5, "manager":Landroid/app/NotificationManager;
    const v13, 0x7010003

    invoke-virtual {v5, v13, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 484
    return-void

    .line 434
    .end local v5    # "manager":Landroid/app/NotificationManager;
    .end local v6    # "notification":Landroid/app/Notification;
    .end local v7    # "pi":Landroid/app/PendingIntent;
    .restart local v2    # "country":Ljava/lang/String;
    .restart local v4    # "lan":Ljava/lang/String;
    :cond_4
    const-string v10, "en"

    goto/16 :goto_0

    .line 454
    .end local v2    # "country":Ljava/lang/String;
    .end local v4    # "lan":Ljava/lang/String;
    :cond_5
    const-string v13, "pt-BR"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_6

    const-string v13, "es-ES"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_6

    const-string v13, "es-US"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 456
    :cond_6
    const-string v13, "-"

    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 458
    .local v9, "splitVoiceLocale":[Ljava/lang/String;
    if-eqz v9, :cond_3

    .line 459
    const/4 v12, 0x0

    .line 460
    .local v12, "voicetalkLocale":Ljava/util/Locale;
    new-instance v12, Ljava/util/Locale;

    .end local v12    # "voicetalkLocale":Ljava/util/Locale;
    const/4 v13, 0x0

    aget-object v13, v9, v13

    const/4 v14, 0x1

    aget-object v14, v9, v14

    invoke-direct {v12, v13, v14}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    .restart local v12    # "voicetalkLocale":Ljava/util/Locale;
    iput-object v12, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_1

    .line 465
    .end local v9    # "splitVoiceLocale":[Ljava/lang/String;
    .end local v12    # "voicetalkLocale":Ljava/util/Locale;
    :cond_7
    new-instance v13, Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v10, v14, v15}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v13, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_1
.end method


# virtual methods
.method public isVoiceControlEnabled()Z
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/mmapp/library/SvoiceManager;->isEnabledBargeIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    const/4 v0, 0x1

    .line 572
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startRecognition(Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;)V
    .locals 12
    .param p1, "info"    # Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;
    .param p2, "svoiceListner"    # Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

    .prologue
    .line 246
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "startRecognition svoiceListner : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "startRecognition mRecognizer is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " svoiceListner : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 251
    .local v5, "pm":Landroid/content/pm/PackageManager;
    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.intent.action.VOICE_SETTING_BARGEIN"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 254
    .local v7, "ri":Landroid/content/pm/ResolveInfo;
    if-nez v7, :cond_1

    .line 255
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "startRecognition : Intent action is ActivityNotFound "

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v9, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/sec/android/mmapp/library/SvoiceManager;->isEnabledBargeIn()Z

    move-result v9

    if-nez v9, :cond_2

    .line 261
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "startRecognition : BargeIn doesn\'t enable"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :cond_2
    iput-object p2, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mSvoiceListner:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

    .line 266
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 267
    .local v6, "resolver":Landroid/content/ContentResolver;
    const-string v9, "voice_input_control"

    const/4 v10, 0x0

    invoke-static {v6, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 268
    .local v8, "voice":I
    if-nez v8, :cond_3

    .line 269
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "voice input setting was off"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 274
    :cond_3
    const-string v9, "voice_input_control_music"

    const/4 v10, 0x0

    invoke-static {v6, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 275
    .local v4, "music":I
    if-nez v4, :cond_4

    .line 276
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "voice input music setting was off"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-boolean v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mIsStart:Z

    if-eqz v9, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/mmapp/library/SvoiceManager;->stopRecognition()V

    goto :goto_0

    .line 283
    :cond_4
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mContext:Landroid/content/Context;

    const-string v10, "keyguard"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 285
    .local v2, "km":Landroid/app/KeyguardManager;
    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 290
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "key guard locked, voice input will not start"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :cond_5
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

    invoke-virtual {v9}, Lcom/sec/android/mmapp/library/SecAudioManager;->isFMActive()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 295
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "FM radio activated, voice input will not start"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 299
    :cond_6
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

    invoke-virtual {v9}, Lcom/sec/android/mmapp/library/SecAudioManager;->isRecordActive()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 300
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "Rec activated, voice input will not start"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :cond_7
    const-string v9, "hdmi_audio_output"

    const/4 v10, 0x0

    invoke-static {v6, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 305
    .local v1, "hdmiValue":I
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

    invoke-virtual {v9}, Lcom/sec/android/mmapp/library/SecAudioManager;->isHDMIConnect()Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v9, 0x1

    if-ne v1, v9, :cond_8

    .line 306
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "HDMI connected, voice input will not start"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 311
    :cond_8
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(I)V

    .line 312
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mIsStart:Z

    .line 313
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mNotificationHandler:Landroid/os/Handler;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 314
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mNotificationHandler:Landroid/os/Handler;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 316
    :cond_9
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v9}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 317
    .local v3, "msg":Landroid/os/Message;
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 318
    const/4 v9, 0x0

    iput v9, v3, Landroid/os/Message;->what:I

    .line 319
    iget-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mNotificationHandler:Landroid/os/Handler;

    const-wide/16 v10, 0xc8

    invoke-virtual {v9, v3, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 320
    .end local v3    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "not support barge in recognizer in this model"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    goto/16 :goto_0

    .line 323
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/ExceptionInInitializerError;
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "not support barge in recognizer in this model"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    goto/16 :goto_0

    .line 326
    .end local v0    # "e":Ljava/lang/ExceptionInInitializerError;
    :catch_2
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v9, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    const-string v10, "not support barge in recognizer in this model"

    invoke-static {v9, v10}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    goto/16 :goto_0
.end method

.method public stopRecognition()V
    .locals 3

    .prologue
    .line 341
    sget-object v0, Lcom/sec/android/mmapp/library/SvoiceManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopRecognition mRecognizer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v0, :cond_0

    .line 344
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mSvoiceListner:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

    .line 345
    iget-object v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/library/SvoiceManager;->mIsStart:Z

    .line 347
    invoke-direct {p0}, Lcom/sec/android/mmapp/library/SvoiceManager;->hideBargeInNotification()V

    .line 349
    :cond_0
    return-void
.end method
