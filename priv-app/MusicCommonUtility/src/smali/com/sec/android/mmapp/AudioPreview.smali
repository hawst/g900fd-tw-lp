.class public Lcom/sec/android/mmapp/AudioPreview;
.super Landroid/app/Activity;
.source "AudioPreview.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;,
        Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;,
        Lcom/sec/android/mmapp/AudioPreview$SongInfo;,
        Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;,
        Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;,
        Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;,
        Lcom/sec/android/mmapp/AudioPreview$Player;
    }
.end annotation


# static fields
.field public static final ACTION_AUDIO_BECOMING_NOISY_SEC:Ljava/lang/String; = "android.media.AUDIO_BECOMING_NOISY_SEC"

.field public static final ACTION_PALM_DOWN:Ljava/lang/String; = "android.intent.action.PALM_DOWN"

.field private static final ACTION_PAUSE:Ljava/lang/String; = "com.sec.android.mmapp.audiopreview.PAUSE"

.field public static final ACTION_REMOTE_CONTROL_BECOMING_NOISY:Ljava/lang/String; = "com.sec.android.mmapp.audiopreview.BECOMING_NOSIY"

.field public static final ACTION_REMOTE_CONTROL_MEDIA_BUTTON:Ljava/lang/String; = "com.sec.android.mmapp.audiopreview.MEDIA_BUTTON"

.field private static final ALBUM_ART_BASE_URI:Landroid/net/Uri;

.field public static final BROADCAST_PERMISSTION:Ljava/lang/String; = "com.sec.android.mmap.broastcasting.permission"

.field private static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field private static final CHN_SECURITY_MAILBOX_URI:Ljava/lang/String; = "content://security_mms/part/"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final KEY_ANSWERING_MEMO_DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field private static final KEY_IS_ANSWERING_MEMO:Ljava/lang/String; = "isAnsweringMemo"

.field private static final LOW_BATTERY_THRESHOLD:I = 0x1

.field private static final MAX_PROGRESS_BAR_VALUE:I = 0x3e8

.field private static final MEDIA_ERROR_CONNECTION_LOST:I = -0x3ed

.field private static final MEDIA_PLAYER_STATE_COMPLETED:I = 0x7

.field private static final MEDIA_PLAYER_STATE_ERROR:I = 0x8

.field private static final MEDIA_PLAYER_STATE_IDLE:I = 0x0

.field private static final MEDIA_PLAYER_STATE_INITIALIZED:I = 0x1

.field private static final MEDIA_PLAYER_STATE_PAUSED:I = 0x5

.field private static final MEDIA_PLAYER_STATE_PREPARED:I = 0x2

.field private static final MEDIA_PLAYER_STATE_PREPARING:I = 0x3

.field private static final MEDIA_PLAYER_STATE_STARTED:I = 0x4

.field private static final MEDIA_PLAYER_STATE_STOPPED:I = 0x6

.field private static final MMS_URI:Ljava/lang/String; = "content://mms/part/"

.field private static final SECURITY_MMS_CONTENT_URI:Ljava/lang/String; = "content://security_mms/part"

.field private static final SEEK_HANDLER_FF:I = 0x1

.field private static final SEEK_HANDLER_REW:I = 0x0

.field private static final SEEK_INTERVAL:[I

.field private static final SORT_ORDER_DATE:I = 0x0

.field private static final SORT_ORDER_NAME:I = 0x2

.field private static final SORT_ORDER_SIZE:I = 0x3

.field private static final SORT_ORDER_TYPE:I = 0x1

.field public static SOUNDPLAYER_MEDIA_BUTTON:Ljava/lang/String; = null

.field private static final SPAMMMS_URI:Ljava/lang/String; = "content://spammms/spampart/"

.field private static final SUPPORT_REMOTE_CONTROL_CLIENT:Z

.field private static final UNDEFINED:I = -0x1

.field private static final UPDATE_COMPLETION:I


# instance fields
.field ffBtn:Landroid/widget/ImageView;

.field private isDpadLeftRightKeyPressed:Z

.field private isSpaceKeyDownPressed:Z

.field private mAcquireProgressBar:Landroid/app/ProgressDialog;

.field private mAlbumView:Landroid/widget/ImageView;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mAudioReceiver:Landroid/content/BroadcastReceiver;

.field private final mCommandReceiver:Landroid/content/BroadcastReceiver;

.field private mControlClient:Landroid/media/RemoteControlClient;

.field private mDownTime:J

.field private mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

.field private mFilePath:Ljava/lang/String;

.field private mInfo:Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

.field private mIsMidiFile:Z

.field private mIsOnStarted:Z

.field private mIsPressedBtn:Z

.field private mIsSeekNow:Z

.field private mIsStreamMusic:Ljava/lang/Boolean;

.field private mList:[J

.field private mListPosition:I

.field private final mLowBatteryReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaPlayerState:I

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaSession:Landroid/media/session/MediaSession;

.field private mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

.field private final mMotionListener:Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;

.field private mMotionManager:Lcom/sec/android/mmapp/library/SimpleMotionManager;

.field private mPauseState:Z

.field private mPlayPauseBtn:Landroid/widget/ImageView;

.field private mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

.field private mPlayerPausedForSeek:Z

.field private mPressed:Z

.field private mPressedAt:J

.field private mProgressBar:Landroid/widget/SeekBar;

.field private final mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mProgressPosition:I

.field private mProgressText:Landroid/widget/TextView;

.field private mRegisteredRemoteControlReceiver:Z

.field private mRemainText:Landroid/widget/TextView;

.field private final mRemoteControlReceiver:Landroid/content/BroadcastReceiver;

.field private final mSbHoverListener:Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;

.field private mSbeamManager:Lcom/sec/android/mmapp/library/SbeamManager;

.field private mSecAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

.field mSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mSeekHandler:Landroid/os/Handler;

.field private mSeekIntervalIndex:I

.field private mSvoice:Lcom/sec/android/mmapp/library/SvoiceManager;

.field private mTitleText:Landroid/widget/TextView;

.field private mToast:Landroid/widget/Toast;

.field private final mToastHandler:Landroid/os/Handler;

.field private final mUiUpdateHandler:Landroid/os/Handler;

.field private mUri:Landroid/net/Uri;

.field private final mVoiceListener:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

.field private pressDownTime:J

.field rewBtn:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 99
    const-class v0, Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    .line 166
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/mmapp/AudioPreview;->SEEK_INTERVAL:[I

    .line 173
    const-string v0, "content://media/external/audio/albumart"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/AudioPreview;->ALBUM_ART_BASE_URI:Landroid/net/Uri;

    .line 225
    const-string v0, "com.sec.android.app.soundplayer.MediaKey"

    sput-object v0, Lcom/sec/android/mmapp/AudioPreview;->SOUNDPLAYER_MEDIA_BUTTON:Ljava/lang/String;

    .line 247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/mmapp/AudioPreview;->SUPPORT_REMOTE_CONTROL_CLIENT:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 166
    :array_0
    .array-data 4
        0x3e8
        0x7d0
        0xfa0
        0x1f40
        0x3e80
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 227
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z

    .line 235
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    .line 237
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressedAt:J

    .line 239
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsSeekNow:Z

    .line 257
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSbHoverListener:Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;

    .line 263
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsStreamMusic:Ljava/lang/Boolean;

    .line 809
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$3;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 1653
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$4;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMotionListener:Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;

    .line 1668
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$5;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$5;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    .line 1707
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$6;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$6;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mUiUpdateHandler:Landroid/os/Handler;

    .line 1787
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$7;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$7;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mVoiceListener:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

    .line 1882
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$10;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$10;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    .line 1892
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$11;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$11;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 1929
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$12;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$12;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mCommandReceiver:Landroid/content/BroadcastReceiver;

    .line 1945
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$13;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$13;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mLowBatteryReceiver:Landroid/content/BroadcastReceiver;

    .line 2001
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$14;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$14;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemoteControlReceiver:Landroid/content/BroadcastReceiver;

    .line 2620
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$15;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$15;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 2688
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$16;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$16;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;

    .line 3478
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->isSpaceKeyDownPressed:Z

    .line 3480
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/mmapp/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/mmapp/AudioPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/mmapp/AudioPreview;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mDownTime:J

    return-wide v0
.end method

.method static synthetic access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/mmapp/AudioPreview;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J

    return-wide v0
.end method

.method static synthetic access$2702(Lcom/sec/android/mmapp/AudioPreview;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # J

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J

    return-wide p1
.end method

.method static synthetic access$2800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/mmapp/AudioPreview;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I

    return v0
.end method

.method static synthetic access$3502(Lcom/sec/android/mmapp/AudioPreview;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I

    return p1
.end method

.method static synthetic access$3600(Lcom/sec/android/mmapp/AudioPreview;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I

    return v0
.end method

.method static synthetic access$3602(Lcom/sec/android/mmapp/AudioPreview;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I

    return p1
.end method

.method static synthetic access$3608(Lcom/sec/android/mmapp/AudioPreview;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/mmapp/AudioPreview;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->rewind(I)V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/android/mmapp/AudioPreview;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->fastForward(I)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/mmapp/AudioPreview;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->sendMusicCommand(I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/android/mmapp/AudioPreview;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/sec/android/mmapp/AudioPreview;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/library/DrmManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V

    return-void
.end method

.method static synthetic access$4402(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;)Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mInfo:Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/mmapp/AudioPreview;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$4802(Lcom/sec/android/mmapp/AudioPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z

    return p1
.end method

.method static synthetic access$4900(Lcom/sec/android/mmapp/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/library/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSecAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/mmapp/AudioPreview;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/mmapp/AudioPreview;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/mmapp/AudioPreview;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # J

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/mmapp/AudioPreview;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->updatePlaybackState(I)V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mUiUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/sec/android/mmapp/AudioPreview;J)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # J

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview;->setRemainText(J)V

    return-void
.end method

.method static synthetic access$5600(Lcom/sec/android/mmapp/AudioPreview;I)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->getNewPosition(I)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$5700(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->updateMetaData()V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/android/mmapp/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsOnStarted:Z

    return v0
.end method

.method static synthetic access$6500(Lcom/sec/android/mmapp/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsSeekNow:Z

    return v0
.end method

.method static synthetic access$6502(Lcom/sec/android/mmapp/AudioPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsSeekNow:Z

    return p1
.end method

.method static synthetic access$6700(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$6702(Lcom/sec/android/mmapp/AudioPreview;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$7000(Lcom/sec/android/mmapp/AudioPreview;)Landroid/media/session/MediaSession;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    return-object v0
.end method

.method static synthetic access$7500(Lcom/sec/android/mmapp/AudioPreview;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    return v0
.end method

.method static synthetic access$7600(Lcom/sec/android/mmapp/AudioPreview;Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v0

    return-object v0
.end method

.method private canSupportSvoice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "lan"    # Ljava/lang/String;

    .prologue
    .line 627
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->FRENCH:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->ITALIAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ru"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "es"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "es-rES"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "es-rUS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "zh"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private changeVolume(I)V
    .locals 2
    .param p1, "volumeAdjust"    # I

    .prologue
    .line 1826
    packed-switch p1, :pswitch_data_0

    .line 1860
    :goto_0
    :pswitch_0
    return-void

    .line 1828
    :pswitch_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/mmapp/AudioPreview$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mmapp/AudioPreview$8;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1843
    :pswitch_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/mmapp/AudioPreview$9;

    invoke-direct {v1, p0}, Lcom/sec/android/mmapp/AudioPreview$9;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1826
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private convertFileUriToContentUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    .line 1546
    const/4 v7, 0x0

    .line 1547
    .local v7, "contentUri":Landroid/net/Uri;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1549
    .local v1, "baseUri":Landroid/net/Uri;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    .line 1552
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "_data COLLATE NOCASE = ?"

    .line 1553
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 1556
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1559
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1560
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1561
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1565
    :cond_0
    if-eqz v6, :cond_1

    .line 1566
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1570
    :cond_1
    if-nez v7, :cond_3

    .line 1572
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1574
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1575
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1576
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 1580
    :cond_2
    if-eqz v6, :cond_3

    .line 1581
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1586
    :cond_3
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "convertFileUriToContentUri() - result: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " -> "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    return-object v7

    .line 1565
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 1566
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1580
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_5

    .line 1581
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private fastForward(I)V
    .locals 4
    .param p1, "seekIntervalIndex"    # I

    .prologue
    .line 981
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->SEEK_INTERVAL:[I

    aget v2, v2, p1

    add-int v0, v1, v2

    .line 982
    .local v0, "msec":I
    if-lez p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 983
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    .line 984
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z

    .line 986
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v1, v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 987
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 988
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 990
    return-void
.end method

.method private getArtwork(JJ)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "audioId"    # J
    .param p3, "albumId"    # J

    .prologue
    .line 1357
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getArtwork(JJZ)Landroid/graphics/Bitmap;
    .locals 17
    .param p1, "audioId"    # J
    .param p3, "albumId"    # J
    .param p5, "resize"    # Z

    .prologue
    .line 1369
    const/4 v2, 0x0

    .line 1370
    .local v2, "bm":Landroid/graphics/Bitmap;
    const-wide/16 v4, 0x0

    cmp-long v3, p3, v4

    if-lez v3, :cond_6

    .line 1371
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 1372
    .local v13, "res":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/mmapp/AudioPreview;->ALBUM_ART_BASE_URI:Landroid/net/Uri;

    move-wide/from16 v0, p3

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    .line 1373
    .local v16, "uri":Landroid/net/Uri;
    if-eqz v16, :cond_3

    .line 1374
    const/4 v11, 0x0

    .line 1376
    .local v11, "in":Ljava/io/InputStream;
    :try_start_0
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v11

    .line 1379
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090015

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 1381
    .local v15, "targetSize":I
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getBitmapOptions(Landroid/net/Uri;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v12

    .line 1382
    .local v12, "options":Landroid/graphics/BitmapFactory$Options;
    if-eqz p5, :cond_0

    .line 1383
    iget v3, v12, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v12, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v3, v4}, Lcom/sec/android/mmapp/AudioPreview;->getSampleSize(III)I

    move-result v3

    iput v3, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1385
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1387
    const/4 v3, 0x0

    invoke-static {v11, v3, v12}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1388
    if-eqz v2, :cond_2

    const/4 v3, -0x1

    if-eq v15, v3, :cond_2

    if-eqz p5, :cond_2

    .line 1389
    const/4 v3, 0x1

    invoke-static {v2, v15, v15, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1393
    .local v14, "resizeBm":Landroid/graphics/Bitmap;
    invoke-virtual {v14, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1394
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1396
    :cond_1
    move-object v2, v14

    .line 1410
    .end local v14    # "resizeBm":Landroid/graphics/Bitmap;
    :cond_2
    if-eqz v11, :cond_3

    .line 1411
    :try_start_1
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1425
    .end local v11    # "in":Ljava/io/InputStream;
    .end local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v13    # "res":Landroid/content/ContentResolver;
    .end local v15    # "targetSize":I
    .end local v16    # "uri":Landroid/net/Uri;
    :cond_3
    :goto_0
    return-object v2

    .line 1413
    .restart local v11    # "in":Ljava/io/InputStream;
    .restart local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v13    # "res":Landroid/content/ContentResolver;
    .restart local v15    # "targetSize":I
    .restart local v16    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 1414
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1398
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v15    # "targetSize":I
    :catch_1
    move-exception v10

    .line 1402
    .local v10, "ex":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mmapp/AudioPreview;->getArtworkFromFile(JJZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1403
    if-eqz v2, :cond_4

    .line 1404
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    if-nez v3, :cond_4

    .line 1405
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 1410
    :cond_4
    if-eqz v11, :cond_3

    .line 1411
    :try_start_3
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1413
    :catch_2
    move-exception v9

    .line 1414
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1409
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "ex":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    .line 1410
    if-eqz v11, :cond_5

    .line 1411
    :try_start_4
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1415
    :cond_5
    :goto_1
    throw v3

    .line 1413
    :catch_3
    move-exception v9

    .line 1414
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1421
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "in":Ljava/io/InputStream;
    .end local v13    # "res":Landroid/content/ContentResolver;
    .end local v16    # "uri":Landroid/net/Uri;
    :cond_6
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-ltz v3, :cond_3

    .line 1422
    const-wide/16 v6, -0x1

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move/from16 v8, p5

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/mmapp/AudioPreview;->getArtworkFromFile(JJZ)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method private getArtworkFromFile(JJZ)Landroid/graphics/Bitmap;
    .locals 15
    .param p1, "songId"    # J
    .param p3, "albumId"    # J
    .param p5, "resize"    # Z

    .prologue
    .line 1429
    const/4 v2, 0x0

    .line 1430
    .local v2, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 1431
    .local v3, "context":Landroid/content/Context;
    const-wide/16 v12, 0x0

    cmp-long v11, p3, v12

    if-gez v11, :cond_0

    const-wide/16 v12, 0x0

    cmp-long v11, p1, v12

    if-gez v11, :cond_0

    .line 1432
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "Must specify an album or a song id"

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 1436
    :cond_0
    const/4 v9, 0x0

    .line 1437
    .local v9, "uri":Landroid/net/Uri;
    const-wide/16 v12, 0x0

    cmp-long v11, p3, v12

    if-gez v11, :cond_5

    .line 1438
    :try_start_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "content://media/external/audio/media/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p1

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/albumart"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1442
    :goto_0
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v12, "r"

    invoke-virtual {v11, v9, v12}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    .line 1443
    .local v7, "pfd":Landroid/os/ParcelFileDescriptor;
    if-eqz v7, :cond_4

    .line 1445
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090015

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 1447
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090012

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1450
    .local v5, "height":I
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1454
    .local v6, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v11, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v11, v6, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1455
    const/4 v11, 0x0

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 1456
    const/4 v11, 0x1

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1457
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1458
    if-eqz p5, :cond_1

    .line 1459
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v12, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/mmapp/AudioPreview;->getSampleSize(III)I

    move-result v11

    iput v11, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1460
    :cond_1
    const/4 v11, 0x0

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1462
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1464
    if-eqz v2, :cond_4

    const/4 v11, -0x1

    if-eq v10, v11, :cond_4

    if-eqz p5, :cond_4

    .line 1466
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v11, v10, :cond_2

    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v11, v5, :cond_4

    const/4 v11, -0x1

    if-eq v10, v11, :cond_4

    .line 1467
    :cond_2
    const/4 v11, 0x1

    invoke-static {v2, v10, v5, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1470
    .local v8, "tmp":Landroid/graphics/Bitmap;
    invoke-virtual {v8, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1471
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1473
    :cond_3
    move-object v2, v8

    .line 1481
    .end local v5    # "height":I
    .end local v6    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "pfd":Landroid/os/ParcelFileDescriptor;
    .end local v8    # "tmp":Landroid/graphics/Bitmap;
    .end local v10    # "width":I
    :cond_4
    :goto_1
    return-object v2

    .line 1440
    :cond_5
    sget-object v11, Lcom/sec/android/mmapp/AudioPreview;->ALBUM_ART_BASE_URI:Landroid/net/Uri;

    move-wide/from16 v0, p3

    invoke-static {v11, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    goto :goto_0

    .line 1477
    :catch_0
    move-exception v4

    .line 1479
    .local v4, "e":Ljava/io/FileNotFoundException;
    sget-object v11, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getArtworkFromFile() : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getBitmapOptions(Landroid/net/Uri;)Landroid/graphics/BitmapFactory$Options;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 1491
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1492
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1493
    .local v3, "res":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .line 1495
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 1498
    const/4 v4, 0x1

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1499
    const/4 v4, 0x0

    invoke-static {v1, v4, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1504
    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1506
    if-eqz v1, :cond_0

    .line 1507
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1513
    :cond_0
    :goto_0
    return-object v2

    .line 1509
    :catch_0
    move-exception v0

    .line 1510
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBitmapOptions() - IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1500
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1501
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1504
    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1506
    if-eqz v1, :cond_0

    .line 1507
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1509
    :catch_2
    move-exception v0

    .line 1510
    sget-object v4, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBitmapOptions() - IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1504
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1506
    if-eqz v1, :cond_1

    .line 1507
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1511
    :cond_1
    :goto_1
    throw v4

    .line 1509
    :catch_3
    move-exception v0

    .line 1510
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v5, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getBitmapOptions() - IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getBucketId(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1296
    const/4 v6, 0x0

    .line 1297
    .local v6, "bucketId":Ljava/lang/String;
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "bucket_id"

    aput-object v0, v2, v1

    .line 1300
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "_data = ?"

    .line 1301
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 1304
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1306
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1308
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309
    const-string v0, "bucket_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 1312
    :cond_0
    if-eqz v7, :cond_1

    .line 1313
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1316
    :cond_1
    return-object v6

    .line 1312
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1313
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f0c0005

    const v7, 0x7f0c0004

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3593
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3594
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 3595
    .local v0, "list":[Ljava/lang/String;
    array-length v2, v0

    packed-switch v2, :pswitch_data_0

    .line 3611
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 3597
    :pswitch_0
    invoke-virtual {p0, v7}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3598
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3599
    invoke-virtual {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3602
    :pswitch_1
    const v2, 0x7f0c0003

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3603
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3604
    invoke-virtual {p0, v7}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3605
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3606
    invoke-virtual {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 3595
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getNewPosition(I)D
    .locals 6
    .param p1, "progressPosition"    # I

    .prologue
    .line 2649
    const-wide/16 v0, 0x0

    .line 2650
    .local v0, "newPosition":D
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v2, :cond_0

    .line 2651
    int-to-double v2, p1

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mDuration:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-double v0, v2

    .line 2652
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    .line 2653
    return-wide v0
.end method

.method private getPlayListInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    .locals 14
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "bucketId"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1233
    new-instance v9, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;

    const/4 v0, 0x0

    invoke-direct {v9, p0, v0}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;-><init>(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/AudioPreview$1;)V

    .line 1234
    .local v9, "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    const/4 v6, 0x0

    .line 1235
    .local v6, "c":Landroid/database/Cursor;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 1238
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "bucket_id = ?"

    .line 1239
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    .line 1243
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1245
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1246
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 1247
    .local v10, "len":I
    new-array v0, v10, [J

    # setter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->list:[J
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$602(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;[J)[J

    .line 1248
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v10, :cond_1

    .line 1249
    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->list:[J
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$600(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)[J

    move-result-object v0

    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    aput-wide v12, v0, v8

    .line 1250
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1251
    # setter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->listPosition:I
    invoke-static {v9, v8}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$702(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;I)I

    .line 1253
    :cond_0
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPlayListInfo() file path : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " current file path : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "_data"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1248
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1265
    .end local v8    # "i":I
    .end local v10    # "len":I
    :cond_1
    if-eqz v6, :cond_2

    .line 1266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1269
    :cond_2
    :goto_1
    if-eqz v9, :cond_3

    .line 1270
    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->list:[J
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$600(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)[J

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1271
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getListFromFileUri() - list length: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->list:[J
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$600(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)[J

    move-result-object v5

    array-length v5, v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " list position: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->listPosition:I
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$700(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    :goto_2
    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->listPosition:I
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$700(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 1280
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getListFromFileUri() - can\'t find matched path so playing one item"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    const/4 v9, 0x0

    .line 1285
    .end local v9    # "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    :cond_3
    return-object v9

    .line 1259
    .restart local v9    # "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    :catch_0
    move-exception v7

    .line 1260
    .local v7, "e":Ljava/lang/RuntimeException;
    const/4 v9, 0x0

    .line 1261
    :try_start_1
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPlayListInfo() method get RuntimeException, please check your DB or your codes the message is : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1265
    if-eqz v6, :cond_2

    .line 1266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1265
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 1266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1274
    :cond_5
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getListFromFileUri() - list length: null list position: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->listPosition:I
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$700(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getSampleSize(III)I
    .locals 8
    .param p1, "targetSize"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 1525
    const/4 v0, 0x1

    .line 1530
    .local v0, "sampelSize":I
    mul-int/lit8 v1, p1, 0x2

    if-ge p2, v1, :cond_0

    mul-int/lit8 v1, p1, 0x2

    if-lt p3, v1, :cond_1

    .line 1531
    :cond_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    int-to-double v4, p1

    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-double v6, v1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v1, v4

    int-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 1535
    :cond_1
    sget-object v1, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInSampleSize - sampleSize is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    return v0
.end method

.method private getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v12, 0x0

    .line 1156
    new-instance v9, Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    invoke-direct {v9, p0, v12}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;-><init>(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/AudioPreview$1;)V

    .line 1157
    .local v9, "info":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1158
    .local v11, "uriStr":Ljava/lang/String;
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    .line 1160
    .local v10, "isMediaProviderUri":Z
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSongInfo() - isMediaProviderUri: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " uri: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    const/4 v2, 0x0

    .line 1164
    .local v2, "projection":[Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 1165
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "_display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "album_id"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "mime_type"

    aput-object v1, v2, v0

    .line 1170
    .restart local v2    # "projection":[Ljava/lang/String;
    :cond_0
    const/4 v6, 0x0

    .line 1172
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1173
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1174
    const/4 v8, -0x1

    .line 1175
    .local v8, "index":I
    const-string v0, "content://mms/part/"

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "content://spammms/spampart/"

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "content://security_mms/part/"

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1177
    :cond_1
    const-string v0, "cl"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1182
    :goto_0
    if-ltz v8, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1183
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$802(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 1184
    const-string v0, "content://mms/part/"

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "content://spammms/spampart/"

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "content://security_mms/part/"

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1189
    :try_start_1
    new-instance v0, Ljava/lang/String;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "ISO-8859-1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    # setter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$802(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1197
    :cond_3
    :goto_1
    if-eqz v10, :cond_4

    .line 1198
    :try_start_2
    const-string v0, "album_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    # setter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$902(Lcom/sec/android/mmapp/AudioPreview$SongInfo;I)I

    .line 1199
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1102(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 1200
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->isMidiFile(Ljava/lang/String;)Z

    move-result v0

    # setter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->isMidiFile:Z
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1002(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Z)Z
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1215
    .end local v8    # "index":I
    :cond_4
    if-eqz v6, :cond_5

    .line 1216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1219
    :cond_5
    :goto_2
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSongInfo() - title: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " albumId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " filePath: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    .end local v9    # "info":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :goto_3
    return-object v9

    .line 1179
    .restart local v8    # "index":I
    .restart local v9    # "info":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :cond_6
    :try_start_3
    const-string v0, "_display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_0

    .line 1190
    :catch_0
    move-exception v7

    .line 1191
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSongInfo() method get RuntimeException,"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    const v0, 0x7f0c0051

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v9, v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$802(Lcom/sec/android/mmapp/AudioPreview$SongInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 1204
    .end local v7    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v8    # "index":I
    :catch_1
    move-exception v7

    .line 1207
    .local v7, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_4
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSongInfo() - UnsupportedOperationException: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1215
    if-eqz v6, :cond_7

    .line 1216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v9, v12

    goto :goto_3

    .line 1209
    .end local v7    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_2
    move-exception v7

    .line 1210
    .local v7, "e":Ljava/lang/NullPointerException;
    :try_start_5
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSongInfo() - Null pointer exception, if it is from provider, conforming the request is necessary : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1215
    if-eqz v6, :cond_5

    .line 1216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 1215
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_8

    .line 1216
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0
.end method

.method private getSortOrderType(I)Ljava/lang/String;
    .locals 3
    .param p1, "sortOrder"    # I

    .prologue
    .line 1327
    const/4 v0, 0x0

    .line 1328
    .local v0, "orderBy":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 1345
    :goto_0
    return-object v0

    .line 1330
    :pswitch_0
    const-string v0, "date_modified DESC"

    .line 1331
    goto :goto_0

    .line 1333
    :pswitch_1
    const-string v0, "mime_type ASC"

    .line 1334
    goto :goto_0

    .line 1336
    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v1, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v1, :cond_0

    const-string v1, "_display_name_pinyin"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1338
    goto :goto_0

    .line 1336
    :cond_0
    const-string v1, "_display_name"

    goto :goto_1

    .line 1340
    :pswitch_3
    const-string v0, "_size DESC"

    .line 1341
    goto :goto_0

    .line 1328
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSvoiceNotificationInfo()Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;
    .locals 17

    .prologue
    .line 1725
    new-instance v6, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;

    invoke-direct {v6}, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;-><init>()V

    .line 1726
    .local v6, "info":Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;
    const v13, 0x7f0c000b

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v6, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->appName:Ljava/lang/String;

    .line 1727
    const v13, 0x7f02004f

    iput v13, v6, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->iconId:I

    .line 1728
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 1729
    .local v8, "res":Landroid/content/res/Resources;
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 1730
    .local v2, "config":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    .line 1731
    .local v5, "globalLocale":Ljava/util/Locale;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "voicetalk_language"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1732
    .local v10, "voiceLocale":Ljava/lang/String;
    if-nez v10, :cond_4

    .line 1736
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    .line 1737
    .local v7, "lan":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    .line 1738
    .local v3, "country":Ljava/lang/String;
    const-string v13, "ro.csc.country_code"

    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1739
    .local v4, "country_code":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/mmapp/AudioPreview;->canSupportSvoice(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "pt"

    invoke-virtual {v13, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    const-string v13, "BR"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    :cond_0
    const-string v13, "Taiwan"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "Hong Kong"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 1742
    move-object v10, v7

    .line 1752
    .end local v3    # "country":Ljava/lang/String;
    .end local v4    # "country_code":Ljava/lang/String;
    .end local v7    # "lan":Ljava/lang/String;
    :cond_1
    :goto_0
    const-string v13, "zh"

    invoke-virtual {v10, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1753
    const/4 v11, 0x0

    .line 1755
    .local v11, "voiceLocaleArray":[Ljava/lang/String;
    :try_start_0
    const-string v13, "-"

    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1758
    new-instance v13, Ljava/util/Locale;

    const/4 v14, 0x0

    aget-object v14, v11, v14

    const/4 v15, 0x1

    aget-object v15, v11, v15

    invoke-direct {v13, v14, v15}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v13, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1777
    .end local v11    # "voiceLocaleArray":[Ljava/lang/String;
    :cond_2
    :goto_1
    const/4 v13, 0x0

    invoke-virtual {v8, v2, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 1778
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f060001

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 1780
    .local v1, "commands":[Ljava/lang/String;
    iput-object v5, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1781
    const/4 v13, 0x0

    invoke-virtual {v8, v2, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 1782
    const v13, 0x7f0c0047

    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x0

    aget-object v16, v1, v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x1

    aget-object v16, v1, v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const/16 v16, 0x2

    aget-object v16, v1, v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const/16 v16, 0x3

    aget-object v16, v1, v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x4

    aget-object v16, v1, v16

    aput-object v16, v14, v15

    const/4 v15, 0x5

    const/16 v16, 0x5

    aget-object v16, v1, v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/android/mmapp/AudioPreview;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v6, Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;->notiString:Ljava/lang/String;

    .line 1784
    return-object v6

    .line 1744
    .end local v1    # "commands":[Ljava/lang/String;
    .restart local v3    # "country":Ljava/lang/String;
    .restart local v4    # "country_code":Ljava/lang/String;
    .restart local v7    # "lan":Ljava/lang/String;
    :cond_3
    const-string v10, "en"

    goto :goto_0

    .line 1746
    .end local v3    # "country":Ljava/lang/String;
    .end local v4    # "country_code":Ljava/lang/String;
    .end local v7    # "lan":Ljava/lang/String;
    :cond_4
    const-string v13, "v-es-LA"

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1747
    const-string v10, "es"

    goto :goto_0

    .line 1762
    :cond_5
    const-string v13, "pt-BR"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1763
    const-string v13, "-"

    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1765
    .local v9, "splitVoiceLocale":[Ljava/lang/String;
    if-eqz v9, :cond_2

    .line 1766
    const/4 v12, 0x0

    .line 1767
    .local v12, "voicetalkLocale":Ljava/util/Locale;
    new-instance v12, Ljava/util/Locale;

    .end local v12    # "voicetalkLocale":Ljava/util/Locale;
    const/4 v13, 0x0

    aget-object v13, v9, v13

    const/4 v14, 0x1

    aget-object v14, v9, v14

    invoke-direct {v12, v13, v14}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769
    .restart local v12    # "voicetalkLocale":Ljava/util/Locale;
    iput-object v12, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_1

    .line 1772
    .end local v9    # "splitVoiceLocale":[Ljava/lang/String;
    .end local v12    # "voicetalkLocale":Ljava/util/Locale;
    :cond_6
    new-instance v13, Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v10, v14, v15}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v13, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_1

    .line 1759
    .restart local v11    # "voiceLocaleArray":[Ljava/lang/String;
    :catch_0
    move-exception v13

    goto/16 :goto_1
.end method

.method private getTimeString(I)Ljava/lang/String;
    .locals 2
    .param p1, "progressPosition"    # I

    .prologue
    .line 2657
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->getNewPosition(I)D

    move-result-wide v0

    double-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTimeString(J)Ljava/lang/String;
    .locals 9
    .param p1, "msec"    # J

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2663
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gtz v3, :cond_0

    .line 2664
    const/4 v2, 0x0

    .line 2665
    .local v2, "sec":I
    const/4 v1, 0x0

    .line 2666
    .local v1, "min":I
    const/4 v0, 0x0

    .line 2681
    .local v0, "hr":I
    :goto_0
    if-nez v0, :cond_2

    .line 2682
    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2684
    :goto_1
    return-object v3

    .line 2669
    .end local v0    # "hr":I
    .end local v1    # "min":I
    .end local v2    # "sec":I
    :cond_0
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v2, v4

    .line 2670
    .restart local v2    # "sec":I
    if-nez v2, :cond_1

    .line 2671
    const/4 v1, 0x0

    .line 2672
    .restart local v1    # "min":I
    const/4 v0, 0x0

    .line 2676
    :goto_2
    rem-int/lit8 v2, v2, 0x3c

    .line 2677
    div-int/lit8 v0, v1, 0x3c

    .line 2678
    .restart local v0    # "hr":I
    rem-int/lit8 v1, v1, 0x3c

    goto :goto_0

    .line 2674
    .end local v0    # "hr":I
    .end local v1    # "min":I
    :cond_1
    div-int/lit8 v1, v2, 0x3c

    .restart local v1    # "min":I
    goto :goto_2

    .line 2684
    .restart local v0    # "hr":I
    :cond_2
    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private handleFFBtn(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 921
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 922
    .local v0, "pressTime":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 948
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 924
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    if-nez v2, :cond_0

    .line 925
    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v3, "ff down"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 927
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 928
    iput v6, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I

    .line 929
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 930
    iput-boolean v7, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    .line 931
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mDownTime:J

    goto :goto_0

    .line 937
    :pswitch_2
    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v3, "ff up"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v2, :cond_1

    cmp-long v2, v0, v8

    if-gez v2, :cond_1

    .line 940
    invoke-virtual {p2, v6}, Landroid/view/View;->playSoundEffect(I)V

    .line 941
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V

    .line 943
    :cond_1
    iput-boolean v6, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    goto :goto_0

    .line 922
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private handleRewBtn(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 879
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 880
    .local v0, "pressTime":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 918
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 882
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    if-nez v2, :cond_0

    .line 883
    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v3, "rew down"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 885
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 886
    iput v6, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekIntervalIndex:I

    .line 887
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 888
    iput-boolean v7, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    .line 889
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mDownTime:J

    goto :goto_0

    .line 895
    :pswitch_2
    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v3, "rew up"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z

    if-eqz v2, :cond_1

    .line 897
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 898
    iput-boolean v6, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z

    .line 900
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v2, :cond_2

    cmp-long v2, v0, v8

    if-gez v2, :cond_2

    .line 902
    invoke-virtual {p2, v6}, Landroid/view/View;->playSoundEffect(I)V

    .line 903
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v2

    const/16 v3, 0xbb8

    if-le v2, v3, :cond_3

    .line 905
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v2, v6}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 906
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-direct {p0, v6}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 907
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 913
    :cond_2
    :goto_1
    iput-boolean v6, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z

    goto :goto_0

    .line 910
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V

    goto :goto_1

    .line 880
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private hideAcquireProgressBar()V
    .locals 1

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1633
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1634
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    .line 1636
    :cond_0
    return-void
.end method

.method private initiateStreamView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 712
    const/high16 v1, 0x7f0f0000

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    .line 713
    const v1, 0x7f0f0007

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    .line 714
    const v1, 0x7f0f0009

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 715
    .local v0, "closeBtn":Landroid/widget/ImageView;
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    const v1, 0x7f0f0003

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    .line 717
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 718
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 719
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->setHoverTooltipType(Landroid/view/View;)V

    .line 720
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 721
    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->setHoverTooltipType(Landroid/view/View;)V

    .line 722
    return-void
.end method

.method private initiateView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 643
    const/high16 v3, 0x7f0f0000

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    .line 644
    const v3, 0x7f0f0002

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    .line 645
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v5}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 646
    const v3, 0x7f0f000a

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mAlbumView:Landroid/widget/ImageView;

    .line 647
    const v3, 0x7f0f0007

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    .line 648
    const v3, 0x7f0f0006

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 649
    .local v2, "rewBtn":Landroid/widget/ImageView;
    const v3, 0x7f0f0008

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 650
    .local v1, "ffBtn":Landroid/widget/ImageView;
    const v3, 0x7f0f0009

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 651
    .local v0, "closeBtn":Landroid/widget/ImageView;
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 652
    const v3, 0x7f0f0003

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    .line 653
    const v3, 0x7f0f0004

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemainText:Landroid/widget/TextView;

    .line 654
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 655
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 656
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->setHoverSeekBarType(Landroid/widget/SeekBar;)V

    .line 657
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 658
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->setHoverTooltipType(Landroid/view/View;)V

    .line 660
    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 661
    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 662
    invoke-virtual {p0, v2, v5}, Lcom/sec/android/mmapp/AudioPreview;->setHoverInfoPrevNextType(Landroid/view/View;I)V

    .line 664
    new-instance v3, Lcom/sec/android/mmapp/AudioPreview$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mmapp/AudioPreview$1;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 690
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 691
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 692
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/mmapp/AudioPreview;->setHoverInfoPrevNextType(Landroid/view/View;I)V

    .line 694
    new-instance v3, Lcom/sec/android/mmapp/AudioPreview$2;

    invoke-direct {v3, p0}, Lcom/sec/android/mmapp/AudioPreview$2;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 707
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 708
    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->setHoverTooltipType(Landroid/view/View;)V

    .line 709
    return-void
.end method

.method private isMidiFile(Ljava/lang/String;)Z
    .locals 4
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1647
    const-string v1, "audio/midi"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "audio/mid"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "audio/imelody"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "audio/sp-midi"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1649
    .local v0, "isMidiFile":Z
    :goto_0
    sget-object v1, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isMidiFile() - mimeType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isMidiFile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    return v0

    .line 1647
    .end local v0    # "isMidiFile":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keyBoardRightLeftKeyHandling(Landroid/view/KeyEvent;Landroid/view/View;)V
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 3577
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 3578
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    if-nez v0, :cond_0

    .line 3579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    .line 3580
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressedAt:J

    .line 3585
    :cond_0
    :goto_0
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 3586
    .local v9, "r":Landroid/graphics/Rect;
    invoke-virtual {p2, v9}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 3587
    iget-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressedAt:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    invoke-virtual {v9}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    invoke-virtual {v9}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 3589
    .local v8, "motionEvent":Landroid/view/MotionEvent;
    invoke-virtual {p2, v8}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 3590
    return-void

    .line 3583
    .end local v8    # "motionEvent":Landroid/view/MotionEvent;
    .end local v9    # "r":Landroid/graphics/Rect;
    :cond_1
    iput-boolean v7, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    goto :goto_0
.end method

.method private playNextSong()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1030
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v3, v3

    if-eq v3, v9, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsStreamMusic:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1032
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->seekToZero()V

    .line 1071
    :cond_2
    :goto_0
    return-void

    .line 1035
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1036
    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v4, v4

    if-ne v3, v4, :cond_4

    .line 1038
    iput v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    .line 1041
    :goto_2
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v5, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v4, v4, v5

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1043
    .local v2, "uri":Landroid/net/Uri;
    invoke-direct {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v1

    .line 1044
    .local v1, "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_filename"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    .line 1035
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1040
    .end local v1    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_4
    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    goto :goto_2

    .line 1047
    .restart local v1    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_5
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 1048
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v4, v3, v4

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v3

    int-to-long v6, v3

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 1049
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->isMidiFile:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1000(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z

    .line 1052
    iput v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I

    .line 1053
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1054
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1055
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1058
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v3, v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1059
    iget-boolean v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z

    if-eqz v3, :cond_6

    .line 1060
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v3, v2, v8}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_0

    .line 1062
    :cond_6
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v3, v2, v9}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_0

    .line 1065
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->reset()V
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3200(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 1066
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V

    goto/16 :goto_0
.end method

.method private playPreviousSong()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1075
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v3, v3

    if-eq v3, v9, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsStreamMusic:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1077
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->seekToZero()V

    .line 1118
    :cond_2
    :goto_0
    return-void

    .line 1081
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1082
    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    if-nez v3, :cond_4

    .line 1084
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    .line 1088
    :goto_2
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v5, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v4, v4, v5

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1090
    .local v2, "uri":Landroid/net/Uri;
    invoke-direct {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v1

    .line 1091
    .local v1, "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 1092
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_filename"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    .line 1081
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1086
    .end local v1    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_4
    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    goto :goto_2

    .line 1095
    .restart local v1    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_5
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->isMidiFile:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1000(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z

    .line 1096
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v4, v3, v4

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v3

    int-to-long v6, v3

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 1099
    iput v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I

    .line 1100
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1101
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1102
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v3, v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1106
    iget-boolean v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPauseState:Z

    if-eqz v3, :cond_6

    .line 1107
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v3, v2, v8}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_0

    .line 1109
    :cond_6
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v3, v2, v9}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_0

    .line 1112
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->reset()V
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3200(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 1113
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V

    goto/16 :goto_0
.end method

.method private prepareMediaSession()V
    .locals 4

    .prologue
    .line 2829
    new-instance v0, Landroid/media/session/MediaSession;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    .line 2830
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    new-instance v1, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;-><init>(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/AudioPreview$1;)V

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;)V

    .line 2832
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v0}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    .line 2833
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const-wide/16 v2, 0x37f

    invoke-virtual {v0, v2, v3}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    .line 2838
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 2839
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 2841
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 2842
    return-void
.end method

.method private prepareRemoteControlClient()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2710
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2711
    .local v0, "mediaButton":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/mmapp/RemoteControlReceiver;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2713
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2715
    .local v1, "pi":Landroid/app/PendingIntent;
    new-instance v2, Landroid/media/RemoteControlClient;

    invoke-direct {v2, v1}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    .line 2716
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    const/16 v3, 0x1ff

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 2725
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    new-instance v3, Lcom/sec/android/mmapp/AudioPreview$17;

    invoke-direct {v3, p0}, Lcom/sec/android/mmapp/AudioPreview$17;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 2736
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    new-instance v3, Lcom/sec/android/mmapp/AudioPreview$18;

    invoke-direct {v3, p0}, Lcom/sec/android/mmapp/AudioPreview$18;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 2745
    return-void
.end method

.method private processIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 475
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    .line 476
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 477
    .local v7, "uriString":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->isMidiFile(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z

    .line 479
    const-string v8, "key_filename"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 480
    sget-object v8, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v9, "processIntent My files case"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    .line 483
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/sec/android/mmapp/AudioPreview;->getBucketId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "sort_order"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    invoke-direct {p0, v10}, Lcom/sec/android/mmapp/AudioPreview;->getSortOrderType(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/mmapp/AudioPreview;->getPlayListInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;

    move-result-object v3

    .line 485
    .local v3, "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    if-eqz v3, :cond_4

    .line 486
    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->list:[J
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$600(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)[J

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    .line 487
    # getter for: Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->listPosition:I
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;->access$700(Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;)I

    move-result v8

    iput v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    .line 489
    sget-object v8, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v10, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v10, v9, v10

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v4

    .line 491
    .local v4, "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 492
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v8, v8, v9

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v10

    int-to-long v10, v10

    invoke-direct {p0, v8, v9, v10, v11}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJ)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 493
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->isMidiFile:Z
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1000(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsMidiFile:Z

    .line 595
    .end local v3    # "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    .end local v4    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setPersonalIconVisible(Ljava/lang/String;)V

    .line 597
    const v8, 0x7f0f0006

    invoke-virtual {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->rewBtn:Landroid/widget/ImageView;

    .line 598
    const v8, 0x7f0f0008

    invoke-virtual {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->ffBtn:Landroid/widget/ImageView;

    .line 599
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsStreamMusic:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_1

    .line 600
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    array-length v8, v8

    if-le v8, v13, :cond_d

    .line 601
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->rewBtn:Landroid/widget/ImageView;

    const v9, 0x7f020047

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 602
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->ffBtn:Landroid/widget/ImageView;

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 611
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v8, v9}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 614
    iput v12, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressPosition:I

    .line 615
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v8, v9, v13}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    .line 618
    :cond_2
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mSbeamManager:Lcom/sec/android/mmapp/library/SbeamManager;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    if-eqz v8, :cond_3

    const-string v8, ""

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 619
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mSbeamManager:Lcom/sec/android/mmapp/library/SbeamManager;

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/sec/android/mmapp/library/SbeamManager;->setFilePath(Ljava/lang/String;)V

    .line 622
    :cond_3
    sget-object v8, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "processIntent() - uri: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " filePath: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mimeType: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :goto_2
    return-void

    .line 497
    .restart local v3    # "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    :cond_4
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 500
    .end local v3    # "info":Lcom/sec/android/mmapp/AudioPreview$PlayListInfo;
    :cond_5
    const-string v8, "http"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 501
    sget-object v8, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v9, "processIntent http case"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 504
    const-string v8, "from-myfiles"

    invoke-virtual {p1, v8, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 505
    const-string v8, "title_name"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 506
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_6

    .line 507
    invoke-direct {p0, v6}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 510
    .end local v6    # "title":Ljava/lang/String;
    :cond_6
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    goto/16 :goto_0

    .line 511
    :cond_7
    const-string v8, "file"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 512
    sget-object v8, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v9, "processIntent file case"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->convertFileUriToContentUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 515
    .local v0, "contentUri":Landroid/net/Uri;
    if-eqz v0, :cond_8

    .line 516
    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v4

    .line 517
    .restart local v4    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 518
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v8

    int-to-long v8, v8

    invoke-direct {p0, v10, v11, v8, v9}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJ)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 550
    .end local v4    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :goto_3
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 551
    .local v5, "stringUri":Ljava/lang/String;
    const-string v8, "#"

    const-string v9, "%23"

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 552
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    .line 554
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    .line 556
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 558
    :try_start_0
    new-instance v2, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 559
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 560
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mToastHandler:Landroid/os/Handler;

    const v9, 0x7f0c004f

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 561
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 564
    .end local v2    # "f":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 565
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 520
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v5    # "stringUri":Ljava/lang/String;
    :cond_8
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    goto :goto_3

    .line 569
    .end local v0    # "contentUri":Landroid/net/Uri;
    :cond_9
    sget-object v8, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 570
    sget-object v8, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v9, "processIntent media store case"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v4

    .line 573
    .restart local v4    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 574
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v8

    int-to-long v8, v8

    invoke-direct {p0, v10, v11, v8, v9}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJ)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 575
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->filePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$1100(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    goto/16 :goto_0

    .line 577
    .end local v4    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :cond_a
    sget-object v8, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v9, "processIntent other case"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v3

    .line 581
    .local v3, "info":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    if-eqz v3, :cond_c

    .line 582
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    .line 588
    :goto_4
    const-string v8, "content://mms/part/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string v8, "content://security_mms/part"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    :cond_b
    const-string v8, "title_name"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    const-string v8, "title_name"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 591
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    const-string v9, "title_name"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 584
    :cond_c
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->setTitleText(Ljava/lang/String;)V

    goto :goto_4

    .line 607
    .end local v3    # "info":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :cond_d
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->rewBtn:Landroid/widget/ImageView;

    const v9, 0x7f020048

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 608
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->ffBtn:Landroid/widget/ImageView;

    const v9, 0x7f020004

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method

.method private registerCommandReceiver()V
    .locals 2

    .prologue
    .line 1874
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1875
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.mmapp.audiopreview.PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1879
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mCommandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/mmapp/AudioPreview;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1880
    return-void
.end method

.method private registerMediaScannerReceiver()V
    .locals 2

    .prologue
    .line 1866
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1867
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1868
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1869
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1870
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/mmapp/AudioPreview;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1871
    return-void
.end method

.method private registerRemoteControlClient()V
    .locals 2

    .prologue
    .line 2702
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 2703
    return-void
.end method

.method private registerRemoteControlReceiver()V
    .locals 5

    .prologue
    .line 1977
    iget-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mRegisteredRemoteControlReceiver:Z

    if-nez v1, :cond_0

    .line 1978
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1979
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.mmapp.audiopreview.BECOMING_NOSIY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1980
    const-string v1, "com.sec.android.mmapp.audiopreview.MEDIA_BUTTON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1981
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemoteControlReceiver:Landroid/content/BroadcastReceiver;

    const-string v2, "com.sec.android.mmap.broastcasting.permission"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 1982
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/mmapp/RemoteControlReceiver;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 1984
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mRegisteredRemoteControlReceiver:Z

    .line 1986
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private resetProgressBarPosition()V
    .locals 2

    .prologue
    .line 951
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 952
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->invalidate()V

    .line 954
    return-void
.end method

.method private rewind(I)V
    .locals 4
    .param p1, "seekIntervalIndex"    # I

    .prologue
    .line 968
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    sget-object v2, Lcom/sec/android/mmapp/AudioPreview;->SEEK_INTERVAL:[I

    aget v2, v2, p1

    sub-int v0, v1, v2

    .line 969
    .local v0, "msec":I
    if-gtz v0, :cond_0

    .line 970
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->rewindToBeginning()V

    .line 975
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 976
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 978
    return-void

    .line 972
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v1, v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    goto :goto_0
.end method

.method private rewindToBeginning()V
    .locals 2

    .prologue
    .line 959
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->resetProgressBarPosition()V

    .line 960
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$000(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 962
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z

    .line 964
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 965
    return-void
.end method

.method private seekToZero()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1020
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 1022
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1026
    :cond_0
    return-void
.end method

.method private sendMusicCommand(I)V
    .locals 2
    .param p1, "command"    # I

    .prologue
    .line 1795
    packed-switch p1, :pswitch_data_0

    .line 1822
    :cond_0
    :goto_0
    return-void

    .line 1797
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V

    goto :goto_0

    .line 1800
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1801
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 1805
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V

    goto :goto_0

    .line 1808
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1810
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 1814
    :pswitch_4
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->changeVolume(I)V

    goto :goto_0

    .line 1817
    :pswitch_5
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->changeVolume(I)V

    goto :goto_0

    .line 1795
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private setAlbumArtView(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAlbumView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAlbumView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 355
    :cond_0
    return-void
.end method

.method private setPersonalIconVisible(Ljava/lang/String;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 744
    const v1, 0x7f0f0001

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 745
    .local v0, "secretIcon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 746
    if-eqz p1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mmapp/library/PrivateMode;->getRootDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 748
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 750
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setRemainText(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemainText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemainText:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/AudioPreview;->getTimeString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemainText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemainText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 349
    :cond_0
    return-void
.end method

.method private setTitleText(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    return-void
.end method

.method private showAcquireProgressBar()V
    .locals 2

    .prologue
    .line 1616
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1629
    :cond_0
    :goto_0
    return-void

    .line 1621
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 1622
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    .line 1623
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    const v1, 0x7f0c0017

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1624
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1626
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1628
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAcquireProgressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private togglePlayPauseButton()V
    .locals 3

    .prologue
    .line 993
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "togglePlayPause() - getSimpleMediaPlayerState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1016
    :cond_0
    :goto_0
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->updatePlayPauseBtn()V

    .line 1017
    return-void

    .line 997
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 1001
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto :goto_0

    .line 1005
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 995
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private unregisterRemoteControlClient()V
    .locals 2

    .prologue
    .line 2706
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 2707
    return-void
.end method

.method private unregisterRemoteControlReceiver()V
    .locals 4

    .prologue
    .line 1993
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRegisteredRemoteControlReceiver:Z

    if-eqz v0, :cond_0

    .line 1994
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRemoteControlReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1995
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/mmapp/RemoteControlReceiver;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 1997
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mRegisteredRemoteControlReceiver:Z

    .line 1999
    :cond_0
    return-void
.end method

.method private updateMetaData()V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 2764
    const/4 v12, 0x0

    .line 2765
    .local v12, "title":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2766
    .local v7, "bm":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 2769
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 2772
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    if-eqz v1, :cond_6

    .line 2773
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v2, v2, v3

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v11

    .line 2775
    .local v11, "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mList:[J

    iget v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I

    aget-wide v2, v1, v2

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v11}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v1

    int-to-long v4, v1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 2793
    .end local v11    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :cond_1
    :goto_0
    if-nez v7, :cond_2

    .line 2794
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 2795
    .local v10, "r":Landroid/content/res/Resources;
    const v1, 0x7f020014

    invoke-static {v10, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 2798
    .end local v10    # "r":Landroid/content/res/Resources;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v1, :cond_9

    .line 2799
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v9

    .line 2800
    .local v9, "ed":Landroid/media/RemoteControlClient$MetadataEditor;
    if-eqz v12, :cond_3

    .line 2801
    const/4 v1, 0x7

    invoke-virtual {v9, v1, v12}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 2804
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v1, :cond_4

    .line 2805
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getDuration()I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6800(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v9, v1, v2, v3}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 2807
    :cond_4
    if-eqz v7, :cond_5

    .line 2808
    invoke-direct {p0, v7}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 2809
    const/16 v1, 0x64

    invoke-virtual {v9, v1, v7}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 2811
    :cond_5
    invoke-virtual {v9}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 2826
    .end local v9    # "ed":Landroid/media/RemoteControlClient$MetadataEditor;
    :goto_1
    return-void

    .line 2777
    :cond_6
    const/4 v8, 0x0

    .line 2778
    .local v8, "contentUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_7

    .line 2779
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2781
    iget-object v8, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    .line 2787
    :cond_7
    :goto_2
    if-eqz v8, :cond_1

    .line 2788
    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v11

    .line 2789
    .restart local v11    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    const-wide/16 v2, 0x0

    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->albumId:I
    invoke-static {v11}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$900(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)I

    move-result v1

    int-to-long v4, v1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/mmapp/AudioPreview;->getArtwork(JJZ)Landroid/graphics/Bitmap;

    move-result-object v7

    goto :goto_0

    .line 2783
    .end local v11    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    :cond_8
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->convertFileUriToContentUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v8

    goto :goto_2

    .line 2813
    .end local v8    # "contentUri":Landroid/net/Uri;
    :cond_9
    new-instance v0, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v0}, Landroid/media/MediaMetadata$Builder;-><init>()V

    .line 2814
    .local v0, "b":Landroid/media/MediaMetadata$Builder;
    if-eqz v12, :cond_a

    .line 2815
    const-string v1, "android.media.metadata.TITLE"

    invoke-virtual {v0, v1, v12}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 2817
    :cond_a
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v1, :cond_b

    .line 2818
    const-string v1, "android.media.metadata.DURATION"

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getDuration()I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$6800(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 2820
    :cond_b
    if-eqz v7, :cond_c

    .line 2821
    invoke-direct {p0, v7}, Lcom/sec/android/mmapp/AudioPreview;->setAlbumArtView(Landroid/graphics/Bitmap;)V

    .line 2822
    const-string v1, "android.media.metadata.ALBUM_ART"

    invoke-virtual {v0, v1, v7}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    .line 2824
    :cond_c
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V

    goto :goto_1
.end method

.method private updatePlayPauseBtn()V
    .locals 4

    .prologue
    .line 1121
    sget-object v1, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updatePlayPauseBtn() - getSimpleMediaPlayerState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v3}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1126
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    .line 1130
    .local v0, "currentPosition":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1141
    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->updatePlaybackState(I)V

    .line 1142
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    const v2, 0x7f020045

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1143
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    const v2, 0x7f0c004c

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1146
    :cond_0
    :goto_1
    return-void

    .line 1128
    .end local v0    # "currentPosition":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "currentPosition":I
    goto :goto_0

    .line 1132
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->updatePlaybackState(I)V

    .line 1133
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    const v2, 0x7f020044

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1134
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayPauseBtn:Landroid/widget/ImageView;

    const v2, 0x7f0c004b

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/AudioPreview;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1135
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 1136
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mProgressBar:Landroid/widget/SeekBar;

    const/16 v2, 0x80

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/SeekBar;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_1

    .line 1130
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method private updatePlaybackState(I)V
    .locals 7
    .param p1, "time"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    .line 2748
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v2, :cond_2

    .line 2749
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I

    if-ne v3, v4, :cond_1

    :goto_0
    int-to-long v4, p1

    invoke-virtual {v2, v0, v4, v5, v6}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 2761
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2749
    goto :goto_0

    .line 2754
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    if-eqz v2, :cond_0

    .line 2755
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    iget v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I

    if-ne v3, v4, :cond_3

    :goto_2
    int-to-long v4, p1

    invoke-virtual {v2, v0, v4, v5, v6}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 2758
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2755
    goto :goto_2
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3558
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 3559
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 3560
    .local v1, "keyCode":I
    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 3561
    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 3562
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3563
    :cond_0
    const/4 v2, 0x0

    .line 3573
    :goto_0
    return v2

    .line 3566
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$000(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3567
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 3569
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 3573
    :cond_3
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public isHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3273
    if-nez p1, :cond_0

    .line 3274
    const/4 v0, 0x0

    .line 3276
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public onAcquireStatus(Ljava/lang/String;I)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    .line 1592
    packed-switch p2, :pswitch_data_0

    .line 1610
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->hideAcquireProgressBar()V

    .line 1613
    :cond_0
    :goto_0
    return-void

    .line 1594
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->showAcquireProgressBar()V

    goto :goto_0

    .line 1597
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v0, :cond_1

    .line 1598
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    .line 1600
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->hideAcquireProgressBar()V

    goto :goto_0

    .line 1603
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->hideAcquireProgressBar()V

    .line 1604
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mInfo:Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsOnStarted:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1605
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mInfo:Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->newInstance(Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;)Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 1592
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 726
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick() - id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 740
    :goto_0
    :pswitch_0
    return-void

    .line 729
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->togglePlayPauseButton()V

    goto :goto_0

    .line 732
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$000(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 735
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    goto :goto_0

    .line 727
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0007
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 267
    sget-object v4, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() - savedInstanceState: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 269
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    .line 270
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    if-nez v4, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 302
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->registerMediaScannerReceiver()V

    .line 304
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 305
    .local v1, "f":Landroid/content/IntentFilter;
    const-string v4, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 306
    const-string v4, "android.media.AUDIO_BECOMING_NOISY_SEC"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 307
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/mmapp/AudioPreview;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 309
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 310
    .local v2, "intentFilterBattery":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 311
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mLowBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/mmapp/AudioPreview;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 313
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->registerCommandReceiver()V

    .line 321
    invoke-static {p0}, Lcom/sec/android/mmapp/library/WindowManagerCompat;->setWindowStatusBarFlag(Landroid/app/Activity;)V

    .line 327
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->setVolumeControlStream(I)V

    .line 330
    sget-boolean v4, Lcom/sec/android/mmapp/AudioPreview;->SUPPORT_REMOTE_CONTROL_CLIENT:Z

    if-eqz v4, :cond_4

    .line 331
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->prepareRemoteControlClient()V

    .line 332
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->registerRemoteControlReceiver()V

    .line 336
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 337
    return-void

    .line 273
    .end local v1    # "f":Landroid/content/IntentFilter;
    .end local v2    # "intentFilterBattery":Landroid/content/IntentFilter;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 274
    .local v3, "uriString":Ljava/lang/String;
    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "from-myfiles"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 276
    const v4, 0x7f040001

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->setContentView(I)V

    .line 277
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->initiateStreamView()V

    .line 278
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsStreamMusic:Ljava/lang/Boolean;

    .line 284
    :goto_2
    new-instance v4, Lcom/sec/android/mmapp/AudioPreview$Player;

    invoke-direct {v4, p0}, Lcom/sec/android/mmapp/AudioPreview$Player;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    .line 285
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    .line 286
    invoke-static {v0}, Lcom/sec/android/mmapp/library/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/SecAudioManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mSecAudioManager:Lcom/sec/android/mmapp/library/SecAudioManager;

    .line 287
    invoke-static {v0}, Lcom/sec/android/mmapp/library/DrmManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/DrmManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

    .line 288
    sget-boolean v4, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_NOT_SUPPORT_PLAYREADY_DRM:Z

    if-nez v4, :cond_1

    .line 289
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;

    invoke-virtual {v4, p0}, Lcom/sec/android/mmapp/library/DrmManager;->setPlayReadyListener(Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;)V

    .line 291
    :cond_1
    new-instance v4, Lcom/sec/android/mmapp/library/SimpleMotionManager;

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview;->mMotionListener:Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;

    invoke-direct {v4, v0, v5}, Lcom/sec/android/mmapp/library/SimpleMotionManager;-><init>(Landroid/content/Context;Lcom/sec/android/mmapp/library/SimpleMotionManager$OnMotionListener;)V

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mMotionManager:Lcom/sec/android/mmapp/library/SimpleMotionManager;

    .line 293
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_3

    .line 300
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->processIntent(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 280
    :cond_2
    const/high16 v4, 0x7f040000

    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/AudioPreview;->setContentView(I)V

    .line 281
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->initiateView()V

    .line 282
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsStreamMusic:Ljava/lang/Boolean;

    goto :goto_2

    .line 294
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mmapp/library/SvoiceManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/mmapp/library/SvoiceManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mSvoice:Lcom/sec/android/mmapp/library/SvoiceManager;

    goto :goto_3

    .line 334
    .end local v3    # "uriString":Ljava/lang/String;
    .restart local v1    # "f":Landroid/content/IntentFilter;
    .restart local v2    # "intentFilterBattery":Landroid/content/IntentFilter;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->prepareMediaSession()V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 435
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->release()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$500(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 441
    iput-object v4, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_1

    .line 449
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v3, v6, v7, v2}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 456
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mCommandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mLowBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 459
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterRemoteControlReceiver()V

    .line 460
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 461
    return-void

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v0, v3, v6, v7, v2}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 452
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 777
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 781
    :cond_0
    const/16 v0, 0x17

    if-eq p2, v0, :cond_1

    const/16 v0, 0x42

    if-ne p2, v0, :cond_6

    .line 782
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_4

    .line 783
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_5

    .line 784
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    if-nez v0, :cond_3

    .line 785
    iput-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    .line 786
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressedAt:J

    .line 791
    :cond_3
    :goto_0
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 792
    .local v9, "r":Landroid/graphics/Rect;
    invoke-virtual {p1, v9}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 793
    iget-wide v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressedAt:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    invoke-virtual {v9}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    invoke-virtual {v9}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 796
    .local v8, "motionEvent":Landroid/view/MotionEvent;
    invoke-virtual {p1, v8}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 804
    .end local v8    # "motionEvent":Landroid/view/MotionEvent;
    .end local v9    # "r":Landroid/graphics/Rect;
    :cond_4
    :goto_1
    return v7

    .line 789
    :cond_5
    iput-boolean v7, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    goto :goto_0

    .line 799
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    if-eqz v0, :cond_4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 800
    iput-boolean v7, p0, Lcom/sec/android/mmapp/AudioPreview;->mPressed:Z

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 3510
    sparse-switch p1, :sswitch_data_0

    .line 3553
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 3512
    :sswitch_0
    iget-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->isSpaceKeyDownPressed:Z

    if-nez v1, :cond_0

    .line 3513
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->togglePlayPauseButton()V

    .line 3514
    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->isSpaceKeyDownPressed:Z

    goto :goto_0

    .line 3519
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3520
    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    .line 3521
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->rewBtn:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, Lcom/sec/android/mmapp/AudioPreview;->keyBoardRightLeftKeyHandling(Landroid/view/KeyEvent;Landroid/view/View;)V

    goto :goto_0

    .line 3526
    :sswitch_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3527
    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    .line 3528
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->ffBtn:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, Lcom/sec/android/mmapp/AudioPreview;->keyBoardRightLeftKeyHandling(Landroid/view/KeyEvent;Landroid/view/View;)V

    goto :goto_0

    .line 3533
    :sswitch_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3534
    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->changeVolume(I)V

    goto :goto_0

    .line 3539
    :sswitch_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3540
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->changeVolume(I)V

    goto :goto_0

    .line 3545
    :sswitch_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3546
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    goto :goto_0

    .line 3510
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x2d -> :sswitch_5
        0x3e -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 3484
    sparse-switch p1, :sswitch_data_0

    .line 3505
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 3486
    :sswitch_0
    iget-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    if-eqz v1, :cond_0

    .line 3487
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->rewBtn:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, Lcom/sec/android/mmapp/AudioPreview;->keyBoardRightLeftKeyHandling(Landroid/view/KeyEvent;Landroid/view/View;)V

    .line 3488
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    goto :goto_0

    .line 3493
    :sswitch_1
    iget-boolean v1, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    if-eqz v1, :cond_0

    .line 3494
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->ffBtn:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, Lcom/sec/android/mmapp/AudioPreview;->keyBoardRightLeftKeyHandling(Landroid/view/KeyEvent;Landroid/view/View;)V

    .line 3495
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->isDpadLeftRightKeyPressed:Z

    goto :goto_0

    .line 3500
    :sswitch_2
    iput-boolean v2, p0, Lcom/sec/android/mmapp/AudioPreview;->isSpaceKeyDownPressed:Z

    goto :goto_0

    .line 3484
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x3e -> :sswitch_2
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 359
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 360
    if-eqz p1, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->isPlaying()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$000(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 367
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview;->processIntent(Landroid/content/Intent;)V

    .line 369
    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 397
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSvoice:Lcom/sec/android/mmapp/library/SvoiceManager;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mSvoice:Lcom/sec/android/mmapp/library/SvoiceManager;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/library/SvoiceManager;->stopRecognition()V

    .line 402
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 403
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 373
    sget-object v1, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 376
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mSvoice:Lcom/sec/android/mmapp/library/SvoiceManager;

    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 379
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 380
    sget-object v1, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v2, "onResume():CallStateChecker.isCallIdle(context) == false"

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # getter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mToStartByAudioFocusGain:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$200(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/mmapp/AudioPreview$Player;->mToStartByAudioFocusGain:Z
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$202(Lcom/sec/android/mmapp/AudioPreview$Player;Z)Z

    .line 390
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$300(Lcom/sec/android/mmapp/AudioPreview$Player;Z)V

    .line 392
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->registerRemoteControlClient()V

    .line 393
    return-void

    .line 382
    .restart local v0    # "context":Landroid/content/Context;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mSvoice:Lcom/sec/android/mmapp/library/SvoiceManager;

    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->getSvoiceNotificationInfo()Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview;->mVoiceListener:Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mmapp/library/SvoiceManager;->startRecognition(Lcom/sec/android/mmapp/library/SvoiceManager$SvoiceNotiInfo;Lcom/sec/android/mmapp/library/SvoiceManager$OnSvoiceListener;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 407
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsOnStarted:Z

    .line 408
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->onStart()V

    .line 409
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 411
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 415
    sget-object v0, Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mIsOnStarted:Z

    .line 417
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->onStop()V

    .line 422
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 427
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview;->unregisterRemoteControlClient()V

    .line 430
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 431
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 759
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 769
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 761
    :pswitch_1
    invoke-direct {p0, p2, p1}, Lcom/sec/android/mmapp/AudioPreview;->handleRewBtn(Landroid/view/MotionEvent;Landroid/view/View;)V

    goto :goto_0

    .line 764
    :pswitch_2
    invoke-direct {p0, p2, p1}, Lcom/sec/android/mmapp/AudioPreview;->handleFFBtn(Landroid/view/MotionEvent;Landroid/view/View;)V

    goto :goto_0

    .line 759
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0006
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setHoverInfoPrevNextType(Landroid/view/View;I)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "type"    # I

    .prologue
    .line 3247
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->isHoveringUI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3248
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 3249
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 3250
    .local v0, "hover":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 3251
    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 3253
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 3254
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 3255
    new-instance v1, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;

    invoke-direct {v1, p0, p2, v0}, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;-><init>(Lcom/sec/android/mmapp/AudioPreview;ILandroid/widget/HoverPopupWindow;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 3258
    .end local v0    # "hover":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public setHoverSeekBarType(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "sb"    # Landroid/widget/SeekBar;

    .prologue
    .line 3261
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview;->isHoveringUI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3262
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 3263
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mSbHoverListener:Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 3264
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview;->mSbHoverListener:Lcom/sec/android/mmapp/AudioPreview$SeekBarHoverListener;

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 3265
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 3266
    .local v0, "hover":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 3267
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 3270
    .end local v0    # "hover":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public setHoverTooltipType(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3237
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/AudioPreview;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3238
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 3239
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 3244
    :cond_0
    return-void
.end method
