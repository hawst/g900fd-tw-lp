.class public Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;
.super Landroid/app/DialogFragment;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DrmPopupFragment"
.end annotation


# instance fields
.field private final mBuyListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mCancelListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mDeleteListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mPlayListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3000
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 3134
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$1;-><init>(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mBuyListener:Landroid/content/DialogInterface$OnClickListener;

    .line 3172
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;-><init>(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mDeleteListener:Landroid/content/DialogInterface$OnClickListener;

    .line 3219
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$3;-><init>(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mPlayListener:Landroid/content/DialogInterface$OnClickListener;

    .line 3228
    new-instance v0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$4;-><init>(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$7100(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3000
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->startBrowser(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$7200(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3000
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getAudioId(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getAudioId(Ljava/lang/String;)J
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 3199
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3200
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 3202
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "_data=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3207
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3208
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 3212
    if-eqz v6, :cond_0

    .line 3213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3216
    :cond_0
    :goto_0
    return-wide v2

    .line 3212
    :cond_1
    if-eqz v6, :cond_2

    .line 3213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3216
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 3212
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 3213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private getDrmDialogFactory()Landroid/app/AlertDialog;
    .locals 8

    .prologue
    const v7, 0x1040013

    const v6, 0x1040009

    const v5, 0x1080027

    .line 3032
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 3033
    .local v0, "data":Landroid/os/Bundle;
    const-string v3, "path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3035
    .local v2, "name":Ljava/lang/String;
    const-string v3, "type"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 3076
    :pswitch_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "text1"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 3083
    .local v1, "dialog":Landroid/app/AlertDialog;
    :goto_0
    return-object v1

    .line 3037
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :pswitch_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "text1"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "count"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v4, v2, v5}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getMessage(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mPlayListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v7, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 3044
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    goto :goto_0

    .line 3046
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :pswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "text1"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "text2"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mDeleteListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v7, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 3054
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    goto/16 :goto_0

    .line 3056
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :pswitch_3
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "text1"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mBuyListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v7, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 3061
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    goto/16 :goto_0

    .line 3035
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 3021
    const/16 v2, 0x2f

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 3023
    .local v0, "index":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 3024
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3028
    .local v1, "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 3026
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    const v2, 0x7f0c0051

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_0
.end method

.method private getMessage(I)Ljava/lang/String;
    .locals 2
    .param p1, "stringType"    # I

    .prologue
    .line 3087
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getMessage(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMessage(ILjava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p1, "stringType"    # I
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "count"    # I

    .prologue
    .line 3091
    const/4 v0, 0x0

    .line 3092
    .local v0, "message":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 3131
    :goto_0
    return-object v0

    .line 3094
    :pswitch_0
    const v1, 0x7f0c0020

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3095
    goto :goto_0

    .line 3097
    :pswitch_1
    const v1, 0x7f0c001b

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3098
    goto :goto_0

    .line 3100
    :pswitch_2
    const v1, 0x7f0c001f

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3101
    goto :goto_0

    .line 3103
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000d

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, p3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0c001c

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3106
    goto :goto_0

    .line 3108
    :pswitch_4
    const v1, 0x7f0c0018

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3109
    goto :goto_0

    .line 3111
    :pswitch_5
    const v1, 0x7f0c001e

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3112
    goto :goto_0

    .line 3114
    :pswitch_6
    const v1, 0x7f0c001d

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3115
    goto :goto_0

    .line 3117
    :pswitch_7
    const v1, 0x7f0c0017

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3118
    goto/16 :goto_0

    .line 3120
    :pswitch_8
    const v1, 0x7f0c001a

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3121
    goto/16 :goto_0

    .line 3123
    :pswitch_9
    const v1, 0x7f0c0019

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3124
    goto/16 :goto_0

    .line 3126
    :pswitch_a
    const v1, 0x7f0c003e

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3127
    goto/16 :goto_0

    .line 3092
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static newInstance(Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;)Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;
    .locals 4
    .param p0, "info"    # Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;

    .prologue
    .line 3003
    new-instance v1, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-direct {v1}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;-><init>()V

    .line 3004
    .local v1, "f":Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3005
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "type"

    iget v3, p0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->type:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3006
    const-string v2, "text1"

    iget v3, p0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->text1:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3007
    const-string v2, "text2"

    iget v3, p0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->text2:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3008
    const-string v2, "count"

    iget v3, p0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->count:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3009
    const-string v2, "url"

    iget-object v3, p0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->url:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3010
    const-string v2, "path"

    iget-object v3, p0, Lcom/sec/android/mmapp/library/DrmManager$PopupInfo;->path:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3011
    invoke-virtual {v1, v0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->setArguments(Landroid/os/Bundle;)V

    .line 3012
    return-object v1
.end method

.method private startBrowser(Ljava/lang/String;)V
    .locals 7
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 3143
    if-nez p1, :cond_1

    .line 3146
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0c0018

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 3170
    :cond_0
    :goto_0
    return-void

    .line 3151
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3152
    .local v2, "i":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3153
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3154
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 3155
    .local v0, "a":Landroid/app/Activity;
    const/4 v3, 0x0

    .line 3156
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_2

    .line 3157
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 3159
    :cond_2
    if-eqz v3, :cond_0

    const/high16 v4, 0x10000

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 3163
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3164
    :catch_0
    move-exception v1

    .line 3165
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DrmPopupFragment() - could not find a suitable activity for launching license url: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 3017
    invoke-direct {p0}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getDrmDialogFactory()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
