.class Lcom/sec/android/mmapp/library/SbeamManager$1;
.super Ljava/lang/Object;
.source "SbeamManager.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/library/SbeamManager;-><init>(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/library/SbeamManager;

.field final synthetic val$nfcAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/library/SbeamManager;Landroid/nfc/NfcAdapter;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    iput-object p2, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 8
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 130
    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/library/SbeamManager;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createNdefMessage - isEnableSBeam()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # invokes: Lcom/sec/android/mmapp/library/SbeamManager;->isEnableSBeam()Z
    invoke-static {v5}, Lcom/sec/android/mmapp/library/SbeamManager;->access$100(Lcom/sec/android/mmapp/library/SbeamManager;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/library/SbeamManager;->access$200(Lcom/sec/android/mmapp/library/SbeamManager;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 133
    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/library/SbeamManager;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "createNdefMessage - mPath is null"

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    :goto_0
    return-object v2

    .line 136
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # invokes: Lcom/sec/android/mmapp/library/SbeamManager;->isEnableSBeam()Z
    invoke-static {v3}, Lcom/sec/android/mmapp/library/SbeamManager;->access$100(Lcom/sec/android/mmapp/library/SbeamManager;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    const-string v4, "text/DirectShareMusic"

    iget-object v5, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mmapp/library/SbeamManager;->access$200(Lcom/sec/android/mmapp/library/SbeamManager;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/mmapp/library/SbeamManager;->createMimeRecord(Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;
    invoke-static {v3, v4, v5}, Lcom/sec/android/mmapp/library/SbeamManager;->access$300(Lcom/sec/android/mmapp/library/SbeamManager;Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v0

    .line 139
    .local v0, "message":Landroid/nfc/NdefRecord;
    if-eqz v0, :cond_0

    .line 146
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v4, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/mmapp/library/SbeamManager;->access$400(Lcom/sec/android/mmapp/library/SbeamManager;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/nfc/NfcAdapter;->setBeamPushUris([Landroid/net/Uri;Landroid/app/Activity;)V

    .line 147
    new-instance v2, Landroid/nfc/NdefMessage;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/nfc/NdefRecord;

    aput-object v0, v3, v6

    const-string v4, "com.sec.android.directshare"

    invoke-static {v4}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-direct {v2, v3}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0

    .line 154
    .end local v0    # "message":Landroid/nfc/NdefRecord;
    :cond_2
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/library/SbeamManager;->access$200(Lcom/sec/android/mmapp/library/SbeamManager;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 155
    .local v1, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;
    invoke-static {v3}, Lcom/sec/android/mmapp/library/SbeamManager;->access$500(Lcom/sec/android/mmapp/library/SbeamManager;)Lcom/sec/android/mmapp/library/DrmManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/library/SbeamManager;->access$200(Lcom/sec/android/mmapp/library/SbeamManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mmapp/library/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 156
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    new-array v4, v7, [Landroid/net/Uri;

    aput-object v1, v4, v6

    iget-object v5, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    # getter for: Lcom/sec/android/mmapp/library/SbeamManager;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/android/mmapp/library/SbeamManager;->access$400(Lcom/sec/android/mmapp/library/SbeamManager;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/nfc/NfcAdapter;->setBeamPushUris([Landroid/net/Uri;Landroid/app/Activity;)V

    .line 160
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mmapp/library/SbeamManager$1;->this$0:Lcom/sec/android/mmapp/library/SbeamManager;

    const/4 v4, 0x6

    # setter for: Lcom/sec/android/mmapp/library/SbeamManager;->mMode:I
    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/SbeamManager;->access$602(Lcom/sec/android/mmapp/library/SbeamManager;I)I

    goto :goto_0
.end method
