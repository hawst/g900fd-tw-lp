.class public Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FolderTabQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "isMusic"    # Z

    .prologue
    const/4 v1, 0x0

    .line 452
    invoke-direct {p0}, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;-><init>()V

    .line 453
    # invokes: Lcom/sec/android/mmapp/util/MusicListUtils;->getFolderTabQueryArgsUri(Z)Landroid/net/Uri;
    invoke-static {p1}, Lcom/sec/android/mmapp/util/MusicListUtils;->access$400(Z)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->uri:Landroid/net/Uri;

    .line 454
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->SAMSUNG_FOLDER_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->projection:[Ljava/lang/String;

    .line 455
    iput-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->selection:Ljava/lang/String;

    .line 456
    iput-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/mmapp/util/MusicListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " COLLATE LOCALIZED ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->orderBy:Ljava/lang/String;

    .line 458
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->BUCKET_DISPLAY_NAME_ORDER_COLUMN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->indexBy:Ljava/lang/String;

    .line 459
    const-string v0, "bucket_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->keyWord:Ljava/lang/String;

    .line 461
    const-string v0, "album_id"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->albumIdCol:Ljava/lang/String;

    .line 462
    const-string v0, "bucket_display_name"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->text1Col:Ljava/lang/String;

    .line 463
    const-string v0, "_data"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;->text2Col:Ljava/lang/String;

    .line 464
    return-void
.end method
