.class public Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isMusic"    # Z

    .prologue
    .line 374
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    .line 375
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-nez v0, :cond_0

    .line 376
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ALBUM_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 379
    :cond_0
    const-string v0, "track"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND album_id=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;->selection:Ljava/lang/String;

    .line 381
    const-string v0, "duration"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 382
    return-void
.end method
