.class Lcom/sec/android/mmapp/library/DrmManager$2;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/library/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/library/DrmManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/library/DrmManager;)V
    .locals 0

    .prologue
    .line 2209
    iput-object p1, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 5
    .param p1, "drmMangerClient"    # Landroid/drm/DrmManagerClient;
    .param p2, "event"    # Landroid/drm/DrmEvent;

    .prologue
    .line 2214
    const-string v2, "MusicDrm"

    const-string v3, "DrmManagerClient onEvent"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2230
    const-string v2, "drm_info_status_object"

    invoke-virtual {p2, v2}, Landroid/drm/DrmEvent;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2232
    .local v0, "att":Ljava/lang/Object;
    instance-of v2, v0, Landroid/drm/DrmInfoStatus;

    if-eqz v2, :cond_0

    .line 2234
    const-string v3, "MusicDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DrmInfoStatus status code : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v2, v0

    check-cast v2, Landroid/drm/DrmInfoStatus;

    iget v2, v2, Landroid/drm/DrmInfoStatus;->statusCode:I

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2236
    const-string v3, "MusicDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DrmInfoStatus info type : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v2, v0

    check-cast v2, Landroid/drm/DrmInfoStatus;

    iget v2, v2, Landroid/drm/DrmInfoStatus;->infoType:I

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    const-string v2, "MusicDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DrmInfoStatus getSubscriptionId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    check-cast v0, Landroid/drm/DrmInfoStatus;

    .end local v0    # "att":Ljava/lang/Object;
    iget-object v4, v0, Landroid/drm/DrmInfoStatus;->data:Landroid/drm/ProcessedData;

    invoke-virtual {v4}, Landroid/drm/ProcessedData;->getSubscriptionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2246
    :cond_0
    const-string v2, "MusicDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248
    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getType()I

    move-result v2

    const/16 v3, 0x3ea

    if-ne v2, v3, :cond_1

    .line 2283
    iget-object v2, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mClient:Landroid/drm/DrmManagerClient;
    invoke-static {v2}, Lcom/sec/android/mmapp/library/DrmManager;->access$200(Lcom/sec/android/mmapp/library/DrmManager;)Landroid/drm/DrmManagerClient;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/library/DrmManager;->access$100(Lcom/sec/android/mmapp/library/DrmManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v1

    .line 2285
    .local v1, "status":I
    iget-object v2, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # invokes: Lcom/sec/android/mmapp/library/DrmManager;->isValidInternal(I)Z
    invoke-static {v2, v1}, Lcom/sec/android/mmapp/library/DrmManager;->access$300(Lcom/sec/android/mmapp/library/DrmManager;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2287
    iget-object v2, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mOnPlayReadyListener:Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;
    invoke-static {v2}, Lcom/sec/android/mmapp/library/DrmManager;->access$400(Lcom/sec/android/mmapp/library/DrmManager;)Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2289
    iget-object v2, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mOnPlayReadyListener:Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;
    invoke-static {v2}, Lcom/sec/android/mmapp/library/DrmManager;->access$400(Lcom/sec/android/mmapp/library/DrmManager;)Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/library/DrmManager;->access$100(Lcom/sec/android/mmapp/library/DrmManager;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;->onAcquireStatus(Ljava/lang/String;I)V

    .line 2309
    .end local v1    # "status":I
    :cond_1
    :goto_0
    return-void

    .line 2297
    .restart local v1    # "status":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mOnPlayReadyListener:Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;
    invoke-static {v2}, Lcom/sec/android/mmapp/library/DrmManager;->access$400(Lcom/sec/android/mmapp/library/DrmManager;)Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2299
    iget-object v2, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mOnPlayReadyListener:Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;
    invoke-static {v2}, Lcom/sec/android/mmapp/library/DrmManager;->access$400(Lcom/sec/android/mmapp/library/DrmManager;)Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/library/DrmManager$2;->this$0:Lcom/sec/android/mmapp/library/DrmManager;

    # getter for: Lcom/sec/android/mmapp/library/DrmManager;->mPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/library/DrmManager;->access$100(Lcom/sec/android/mmapp/library/DrmManager;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/mmapp/library/DrmManager$OnPlayReadyListener;->onAcquireStatus(Ljava/lang/String;I)V

    goto :goto_0
.end method
