.class public Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;
.super Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtistTrackQueryArgs"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isMusic"    # Z

    .prologue
    .line 388
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$TrackQueryArgs;-><init>(Ljava/lang/String;Z)V

    .line 389
    sget-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-nez v0, :cond_0

    .line 390
    sget-object v0, Lcom/sec/android/mmapp/util/MusicListUtils;->ARTIST_TRACK_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;->projection:[Ljava/lang/String;

    .line 394
    :cond_0
    const-string v0, "album COLLATE LOCALIZED ASC, album_id, track"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;->orderBy:Ljava/lang/String;

    .line 396
    const-string v0, "album"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;->indexBy:Ljava/lang/String;

    .line 398
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND artist_id=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;->selection:Ljava/lang/String;

    .line 399
    const-string v0, "duration"

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTrackQueryArgs;->durationCol:Ljava/lang/String;

    .line 400
    return-void
.end method
