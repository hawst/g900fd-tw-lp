.class public Lcom/sec/android/mmapp/library/MusicFeatures;
.super Ljava/lang/Object;
.source "MusicFeatures.java"


# static fields
.field public static final FALG_SUPPORT_SWEEP_ON_TABS:Z

.field public static final FLAG_CHECK_CALL_VT_SUPPORT:Z = false

.field public static final FLAG_CHECK_IMEI_WHEN_HANDLE_ACTION_MEDIA_BUTTON:Z = false

.field public static final FLAG_CHECK_KOR_LGT:Z = false

.field public static final FLAG_CHECK_KOR_SKT:Z = false

.field public static final FLAG_CHECK_MTPACTIVITY:Z = false

.field public static final FLAG_DISABLE_MUSIC_STORE:Z = false

.field public static final FLAG_ENABLE_FIND_TAG:Z

.field public static final FLAG_ENABLE_SEEK_DELAYED_IN:Z = false

.field public static final FLAG_JPN_LYRIC:Z

.field public static final FLAG_MUSIC_LDB:Z

.field public static final FLAG_MUSIC_MID_VOLUME_CONTROL:Z = false
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_MUSIC_REPEAT_AB:Z

.field public static final FLAG_MUSIC_SUPPORT_SRS_SOUNDALIVE:Z = false

.field public static final FLAG_MUSIC_VIEW:Z = false

.field public static final FLAG_NOT_SUPPORT_PLAYREADY_DRM:Z

.field public static final FLAG_QC_LPA_ENABLE:Z = false

.field public static final FLAG_RIL_CALL_ANSWERING_MESSAGE:Z = false

.field public static final FLAG_SELECT_ALL_ACTIONBAR:Z = true

.field public static final FLAG_SMART_VOLUME:Z

.field public static final FLAG_SUPPORT_ADAPT_SOUND:Z = true

.field public static final FLAG_SUPPORT_ALLSHARE:Z = true

.field public static final FLAG_SUPPORT_AUTO_RECOMMENDATION:Z = true

.field public static final FLAG_SUPPORT_BIGPOND:Z

.field public static final FLAG_SUPPORT_CALL_RINGTONE_DUOS_CDMAGSM:Z = false

.field public static final FLAG_SUPPORT_CALL_RINGTONE_DUOS_CGG:Z = false

.field public static final FLAG_SUPPORT_DATA_CHECK_POPUP:Z

.field public static final FLAG_SUPPORT_DISPLAY_SQUARE_UPDATE_COUNT:Z = false

.field public static final FLAG_SUPPORT_DLNA_DMC_ONLY:Z = false

.field public static final FLAG_SUPPORT_DLNA_MULTI_SPEAKER:Z = false

.field public static final FLAG_SUPPORT_DSDS:Z = false

.field public static final FLAG_SUPPORT_DUALMODE:Z = false

.field public static final FLAG_SUPPORT_FINGER_AIR_VIEW:Z = true

.field public static final FLAG_SUPPORT_GESTURE_AIR_MOTION:Z = false

.field public static final FLAG_SUPPORT_INDEPENDENT_MIDI_VOLUME:Z = true

.field public static final FLAG_SUPPORT_MIRROR_CALL:Z = false

.field public static final FLAG_SUPPORT_MOTION_BOUNCE:Z

.field public static final FLAG_SUPPORT_MOTION_PALM_PAUSE:Z = false

.field public static final FLAG_SUPPORT_MOTION_TURN_OVER:Z = false

.field public static FLAG_SUPPORT_MOVE_TO_KNOX:Z = false
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_SUPPORT_MULTI_SIM:Z = false

.field public static final FLAG_SUPPORT_MULTI_SPEAKER:Z = false

.field public static final FLAG_SUPPORT_NEW_SOUNDALIVE:Z = true

.field public static final FLAG_SUPPORT_NUMERIC_KEYPAD:Z = false

.field public static final FLAG_SUPPORT_OPTION_END:Z = false

.field public static final FLAG_SUPPORT_PINYIN:Z

.field public static final FLAG_SUPPORT_PLAYING_MUSIC_DURING_CALL:Z = false

.field public static final FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

.field public static final FLAG_SUPPORT_SAMSUNG_MUSIC:Z = false

.field public static final FLAG_SUPPORT_SBEAM:Z = false

.field public static final FLAG_SUPPORT_SECRET_BOX:Z = true

.field public static final FLAG_SUPPORT_SET_BATTERY_ADC:Z = false

.field public static final FLAG_SUPPORT_SIDESYNC:Z = true
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_SUPPORT_SMART_CLIP_META:Z

.field public static final FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

.field public static final FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION:Z = true

.field public static final FLAG_SUPPORT_SVIEW_COVER_V2:Z

.field public static final FLAG_SUPPORT_SVOICE_FOR_GEAR:Z = true

.field public static final FLAG_SUPPORT_S_STREAM:Z = true

.field public static final FLAG_SUPPORT_UHQA:Z = true

.field public static final FLAG_SUPPORT_UNKNOWN_TRANS:Z

.field public static final FLAG_SUPPORT_UWA_CALL:Z = false

.field public static final FLAG_SUPPORT_WHITE_THEME:Z = true

.field public static final FLAG_SUPPORT_WIFIDISPLAY_OLD:Z = false

.field public static final FLAG_SUPPORT_WIFI_DISPLAY:Z = true

.field public static final FLAG_SUPPROT_ENHANCED_PLAY_SPEED:Z = true

.field public static final FLAG_SUPPROT_OMA13_ENCODING:Z = false

.field public static final IS_DEFAULT_LAND_MODEL:Z

.field public static final IS_MASS_PROJECT:Z = false

.field public static final MUSIC_FEATURE_ENABLE_DRM_RINGTONE_CHECK:Z = true

.field public static final MUSIC_RINGTONE_SIZE_LIMIT:I

.field public static final NUMBER_OF_SPEAKER:I = 0x1

.field public static final PRODUCT_NAME:Ljava/lang/String;

.field public static final SYSTEM_CONTRY_CODE:Ljava/lang/String;

.field public static final SYSTEM_DEVICE_NAME:Ljava/lang/String;

.field public static final SYSTEM_SALES_CODE:Ljava/lang/String;

.field public static final SYSTEM_VERSION_RELEASE:Ljava/lang/String;

.field private static final sCscFeature:Lcom/sec/android/app/CscFeature;

.field private static final sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 19
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    .line 21
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    .line 68
    const-string v0, "ATT"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

    .line 94
    const-string v0, "USA"

    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_NOT_SUPPORT_PLAYREADY_DRM:Z

    .line 102
    const-string v0, "JP"

    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_JPN_LYRIC:Z

    .line 110
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->SYSTEM_CONTRY_CODE:Ljava/lang/String;

    .line 118
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->SYSTEM_SALES_CODE:Ljava/lang/String;

    .line 144
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getOSDeviceName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->SYSTEM_DEVICE_NAME:Ljava/lang/String;

    .line 161
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getOSver()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->SYSTEM_VERSION_RELEASE:Ljava/lang/String;

    .line 178
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    .line 281
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_TranslateUnknownTitle"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_UNKNOWN_TRANS:Z

    .line 293
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_SupportPinyinSort"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    .line 302
    const-string v0, "China"

    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    .line 321
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_DisableFindTag"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_ENABLE_FIND_TAG:Z

    .line 330
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_EnableBigPondTop10Feeds"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_BIGPOND:Z

    .line 340
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_RingtoneSizeLimit"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/mmapp/library/MusicFeatures;->MUSIC_RINGTONE_SIZE_LIMIT:I

    .line 350
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_SPLIT_VIEW"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    .line 432
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_ENABLE_SMART_VOLUME"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SMART_VOLUME:Z

    .line 451
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_ENABLE_REPEAT_AB"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_MUSIC_REPEAT_AB:Z

    .line 461
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_ENABLE_LDB_LYLIC"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_MUSIC_LDB:Z

    .line 676
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isHProject()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_SMART_CLIP_META:Z

    .line 696
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isFProject()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_MOTION_BOUNCE:Z

    .line 704
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isHProject()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isFProject()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    sput-boolean v2, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_SVIEW_COVER_V2:Z

    .line 714
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isDefaultLandModel()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->IS_DEFAULT_LAND_MODEL:Z

    .line 775
    invoke-static {}, Lcom/sec/android/mmapp/library/MusicFeatures;->isHProject()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FLAG_SUPPORT_MOVE_TO_KNOX:Z

    .line 832
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v1, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_SWEEP_ON_TABS"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/mmapp/library/MusicFeatures;->FALG_SUPPORT_SWEEP_ON_TABS:Z

    return-void

    :cond_2
    move v0, v2

    .line 321
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static final getOSDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    const-string v0, "model.device.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static final getOSver()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    const-string v0, "ro.build.version.release"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static isDefaultLandModel()Z
    .locals 2

    .prologue
    .line 732
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "v1a"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "v2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "picasso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "matisse"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "mondrian"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFProject()Z
    .locals 2

    .prologue
    .line 693
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static final isGateEnable()Z
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v0

    return v0
.end method

.method public static isHProject()Z
    .locals 2

    .prologue
    .line 664
    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "Madrid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "ASH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/mmapp/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportMultiSim(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 539
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 540
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.multisim"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 542
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
