.class public Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;
.super Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TabListInfo"
.end annotation


# direct methods
.method public constructor <init>(IZ)V
    .locals 3
    .param p1, "list"    # I
    .param p2, "isMusic"    # Z

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/sec/android/mmapp/util/MusicListUtils$MusicListInfo;-><init>()V

    .line 197
    invoke-static {p1}, Lcom/sec/android/mmapp/util/MusicListUtils;->getListContentType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 214
    :pswitch_0
    # getter for: Lcom/sec/android/mmapp/util/MusicListUtils;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/util/MusicListUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TabListInfo > invalid tab list : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Did you miss something?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :goto_0
    return-void

    .line 199
    :pswitch_1
    const v0, 0x7f0c0029

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->noItemTextId:I

    .line 200
    const/high16 v0, 0x7f0b0000

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->numberOfTextId:I

    .line 201
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;

    invoke-direct {v0, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$AlbumTabQueryArgs;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 204
    :pswitch_2
    const v0, 0x7f0c002a

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->noItemTextId:I

    .line 205
    const v0, 0x7f0b0001

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->numberOfTextId:I

    .line 206
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;

    invoke-direct {v0, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$ArtistTabQueryArgs;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 209
    :pswitch_3
    const v0, 0x7f0c002b

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->noItemTextId:I

    .line 210
    const v0, 0x7f0b0003

    iput v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->numberOfTextId:I

    .line 211
    new-instance v0, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;

    invoke-direct {v0, p2}, Lcom/sec/android/mmapp/util/MusicListUtils$FolderTabQueryArgs;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$TabListInfo;->queryArgs:Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
