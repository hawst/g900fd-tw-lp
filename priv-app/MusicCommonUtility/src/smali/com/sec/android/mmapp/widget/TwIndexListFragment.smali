.class public Lcom/sec/android/mmapp/widget/TwIndexListFragment;
.super Landroid/app/Fragment;
.source "TwIndexListFragment.java"

# interfaces
.implements Lcom/sec/android/mmapp/widget/OnMusicFragmentInterface;


# static fields
.field public static final INDEX_VIEW_TAG:Ljava/lang/String; = "index"


# instance fields
.field mAdapter:Landroid/widget/ListAdapter;

.field mEmptyText:Ljava/lang/CharSequence;

.field private final mHandler:Landroid/os/Handler;

.field public mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

.field mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

.field mListContainer:Landroid/view/View;

.field mListShown:Z

.field private final mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field mProgressContainer:Landroid/view/View;

.field private final mRequestFocus:Ljava/lang/Runnable;

.field mStandardEmptyTextView:Landroid/widget/TextView;

.field mStandardEmptyView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mHandler:Landroid/os/Handler;

    .line 45
    new-instance v0, Lcom/sec/android/mmapp/widget/TwIndexListFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment$1;-><init>(Lcom/sec/android/mmapp/widget/TwIndexListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mRequestFocus:Ljava/lang/Runnable;

    .line 52
    new-instance v0, Lcom/sec/android/mmapp/widget/TwIndexListFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment$2;-><init>(Lcom/sec/android/mmapp/widget/TwIndexListFragment;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 80
    return-void
.end method

.method private ensureList()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 282
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    if-eqz v3, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 286
    .local v2, "root":Landroid/view/ViewGroup;
    if-eqz v2, :cond_0

    .line 291
    const v3, 0x7f0f0016

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyView:Landroid/view/View;

    .line 292
    const v3, 0x7f0f0027

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyTextView:Landroid/widget/TextView;

    .line 293
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyView:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 295
    const v3, 0x7f0f0013

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    .line 296
    const v3, 0x7f0f0014

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    .line 297
    const v3, 0x7f0f0015

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 298
    .local v1, "rawListView":Landroid/view/View;
    instance-of v3, v1, Lcom/sec/android/mmapp/widget/TwIndexListView;

    if-nez v3, :cond_2

    .line 299
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Content has view with id attribute \'R.id.list\' that is not a TwIndexListView class"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 302
    :cond_2
    check-cast v1, Lcom/sec/android/mmapp/widget/TwIndexListView;

    .end local v1    # "rawListView":Landroid/view/View;
    iput-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    .line 303
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    if-nez v3, :cond_3

    .line 304
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Your content must have a TwIndexListView whose id attribute is \'R.id.list\'"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 309
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mEmptyText:Ljava/lang/CharSequence;

    if-eqz v3, :cond_4

    .line 310
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mEmptyText:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    iget-object v4, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyView:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setEmptyView(Landroid/view/View;)V

    .line 314
    :cond_4
    new-instance v3, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    .line 315
    invoke-static {}, Lcom/sec/android/mmapp/ThemeManager;->getTheme()I

    move-result v3

    if-nez v3, :cond_6

    .line 319
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v3, v6}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setIndexScrollViewTheme(I)V

    .line 323
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    const-string v4, "index"

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setTag(Ljava/lang/Object;)V

    .line 324
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setSubscrollLimit(I)V

    .line 325
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v3, v5}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 326
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    iget-object v4, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v3, v4}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setTwScrollView(Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;)V

    .line 327
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 329
    iput-boolean v6, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListShown:Z

    .line 330
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    iget-object v4, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 331
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_7

    .line 332
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 333
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 334
    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 342
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    :cond_5
    :goto_2
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 321
    :cond_6
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v3, v5}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setIndexScrollViewTheme(I)V

    goto :goto_1

    .line 338
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    if-eqz v3, :cond_5

    .line 339
    invoke-direct {p0, v5, v5}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->setListShown(ZZ)V

    goto :goto_2
.end method

.method private setListShown(ZZ)V
    .locals 6
    .param p1, "shown"    # Z
    .param p2, "animate"    # Z

    .prologue
    const v5, 0x10a0001

    const/high16 v4, 0x10a0000

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 233
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 234
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    if-nez v0, :cond_0

    .line 235
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListShown:Z

    if-ne v0, p1, :cond_2

    .line 272
    :cond_1
    :goto_0
    return-void

    .line 240
    :cond_2
    iput-boolean p1, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListShown:Z

    .line 241
    if-eqz p1, :cond_4

    .line 242
    if-eqz p2, :cond_3

    .line 243
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 251
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->invalidate()V

    goto :goto_0

    .line 248
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_1

    .line 260
    :cond_4
    if-eqz p2, :cond_5

    .line 261
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 269
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 266
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 267
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_2
.end method


# virtual methods
.method public getCurrentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    return-object v0
.end method

.method public getListAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    return-object v0
.end method

.method public getSelectedItemId()J
    .locals 2

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 167
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->getSelectedItemId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 159
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public getViewCount()I
    .locals 1

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 395
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/widget/TwIndexListView;->getCount()I

    move-result v0

    return v0
.end method

.method public invalidateAllViews()V
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 401
    .local v0, "view":Landroid/widget/ListView;
    if-eqz v0, :cond_0

    .line 402
    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 403
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const v0, 0x7f040007

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 105
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListShown:Z

    .line 107
    iput-object v2, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListContainer:Landroid/view/View;

    iput-object v2, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mProgressContainer:Landroid/view/View;

    .line 108
    iput-object v2, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyView:Landroid/view/View;

    .line 109
    iput-object v2, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyTextView:Landroid/widget/TextView;

    .line 110
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 111
    return-void
.end method

.method public onFragmentWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 413
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 125
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 94
    return-void
.end method

.method public setEmptyText(Ljava/lang/CharSequence;ILjava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "listType"    # I
    .param p3, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 185
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mEmptyText:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    iget-object v1, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mStandardEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setEmptyView(Landroid/view/View;)V

    .line 192
    :cond_1
    iput-object p1, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mEmptyText:Ljava/lang/CharSequence;

    .line 193
    return-void
.end method

.method public setHasShuffleView(Z)V
    .locals 1
    .param p1, "hasShuffleView"    # Z

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setHasShuffleView(Z)V

    .line 386
    :cond_0
    return-void
.end method

.method public setIndexViewVisibility(I)V
    .locals 2
    .param p1, "visible"    # I

    .prologue
    const/4 v1, 0x0

    .line 355
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-nez v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 366
    :cond_0
    if-nez p1, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method public setIndexer(Landroid/database/Cursor;I)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "columnIndex"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setIndexer(Landroid/database/Cursor;I)V

    .line 352
    return-void
.end method

.method protected setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 131
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_2

    move v0, v1

    .line 132
    .local v0, "hadAdapter":Z
    :goto_0
    iput-object p1, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 133
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    if-eqz v3, :cond_1

    .line 134
    iget-object v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v3, p1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 135
    iget-boolean v3, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mListShown:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->setListShown(ZZ)V

    .line 141
    :cond_1
    return-void

    .end local v0    # "hadAdapter":Z
    :cond_2
    move v0, v2

    .line 131
    goto :goto_0
.end method

.method public setListShown(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 211
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->setListShown(ZZ)V

    .line 212
    return-void
.end method

.method public setListShownNoAnimation(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->setListShown(ZZ)V

    .line 220
    return-void
.end method

.method public setSelectable()V
    .locals 2

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 390
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setChoiceMode(I)V

    .line 391
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->ensureList()V

    .line 151
    iget-object v0, p0, Lcom/sec/android/mmapp/widget/TwIndexListFragment;->mList:Lcom/sec/android/mmapp/widget/TwIndexListView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mmapp/widget/TwIndexListView;->setSelection(I)V

    .line 152
    return-void
.end method
