.class public final Lcom/sec/android/mmapp/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final audio_preview_popup_control_close_height:I = 0x7f090000

.field public static final audio_preview_popup_control_close_margin_bottom:I = 0x7f09009a

.field public static final audio_preview_popup_control_close_margin_left:I = 0x7f090001

.field public static final audio_preview_popup_control_close_margin_right:I = 0x7f090002

.field public static final audio_preview_popup_control_close_margin_top:I = 0x7f090003

.field public static final audio_preview_popup_control_close_width:I = 0x7f090004

.field public static final audio_preview_popup_control_ff_height:I = 0x7f090005

.field public static final audio_preview_popup_control_ff_margin_left:I = 0x7f090006

.field public static final audio_preview_popup_control_ff_margin_top:I = 0x7f090007

.field public static final audio_preview_popup_control_ff_width:I = 0x7f090008

.field public static final audio_preview_popup_control_margin_left:I = 0x7f090009

.field public static final audio_preview_popup_control_margin_top:I = 0x7f09000a

.field public static final audio_preview_popup_control_play_height:I = 0x7f09000b

.field public static final audio_preview_popup_control_play_margin_left:I = 0x7f09000c

.field public static final audio_preview_popup_control_play_width:I = 0x7f09000d

.field public static final audio_preview_popup_control_rew_height:I = 0x7f09000e

.field public static final audio_preview_popup_control_rew_margin_left:I = 0x7f09000f

.field public static final audio_preview_popup_control_rew_margin_top:I = 0x7f090010

.field public static final audio_preview_popup_control_rew_width:I = 0x7f090011

.field public static final audio_preview_popup_control_thumbnail_height:I = 0x7f090012

.field public static final audio_preview_popup_control_thumbnail_margin_left:I = 0x7f090013

.field public static final audio_preview_popup_control_thumbnail_margin_top:I = 0x7f090014

.field public static final audio_preview_popup_control_thumbnail_width:I = 0x7f090015

.field public static final audio_preview_popup_controller_button_margin:I = 0x7f090016

.field public static final audio_preview_popup_height:I = 0x7f090017

.field public static final audio_preview_popup_progress_info_left_margin_edge:I = 0x7f09009b

.field public static final audio_preview_popup_progress_info_margin_edge:I = 0x7f090018

.field public static final audio_preview_popup_progress_info_margin_top:I = 0x7f090019

.field public static final audio_preview_popup_progress_info_text:I = 0x7f09001a

.field public static final audio_preview_popup_progress_margin_top:I = 0x7f09001b

.field public static final audio_preview_popup_progress_padding_edge:I = 0x7f09001c

.field public static final audio_preview_popup_progress_padding_left_edge:I = 0x7f09009c

.field public static final audio_preview_popup_progress_seekbar_height:I = 0x7f09001d

.field public static final audio_preview_popup_progress_seekbar_thumb_offset:I = 0x7f09001e

.field public static final audio_preview_popup_stream_control_close_height:I = 0x7f09001f

.field public static final audio_preview_popup_stream_control_close_margin_right:I = 0x7f090020

.field public static final audio_preview_popup_stream_control_close_width:I = 0x7f090021

.field public static final audio_preview_popup_stream_control_play_pause_margin_left:I = 0x7f090022

.field public static final audio_preview_popup_stream_control_thumbnail_margin_left:I = 0x7f090023

.field public static final audio_preview_popup_stream_play_time_margin_top:I = 0x7f090024

.field public static final audio_preview_popup_stream_play_time_text_size:I = 0x7f090025

.field public static final audio_preview_popup_stream_title_info_margin_left:I = 0x7f090026

.field public static final audio_preview_popup_stream_title_info_margin_top:I = 0x7f090027

.field public static final audio_preview_popup_stream_title_padding_top:I = 0x7f090028

.field public static final audio_preview_popup_stream_title_text_size:I = 0x7f090029

.field public static final audio_preview_popup_title_padding_edge:I = 0x7f09002a

.field public static final audio_preview_popup_title_padding_left_edge:I = 0x7f09009d

.field public static final audio_preview_popup_title_padding_top:I = 0x7f09002b

.field public static final audio_preview_popup_title_text:I = 0x7f09002c

.field public static final audio_preview_popup_width:I = 0x7f09002d

.field public static final audio_preview_stream_popup_control_play_pause_height:I = 0x7f09002e

.field public static final audio_preview_stream_popup_control_play_pause_width:I = 0x7f09002f

.field public static final audio_preview_stream_popup_control_thumbnail_height:I = 0x7f090030

.field public static final audio_preview_stream_popup_control_thumbnail_width:I = 0x7f090031

.field public static final audio_preview_stream_popup_height:I = 0x7f090032

.field public static final audio_preview_stream_popup_play_time_text_height:I = 0x7f090033

.field public static final audio_preview_stream_popup_title_text_height:I = 0x7f090034

.field public static final audio_preview_stream_popup_title_text_width:I = 0x7f090035

.field public static final audio_preview_stream_popup_width:I = 0x7f090036

.field public static final auto_recommendation_check_box_padding_Right:I = 0x7f090037

.field public static final auto_recommendation_help_text_size:I = 0x7f090038

.field public static final auto_recommendation_selection_height:I = 0x7f090039

.field public static final auto_recommendation_text_size:I = 0x7f09003a

.field public static final autorecommendation_padding_bottom:I = 0x7f09009e

.field public static final autorecommendation_padding_left:I = 0x7f09009f

.field public static final autorecommendation_padding_top:I = 0x7f0900a0

.field public static final autorecommendation_text_height:I = 0x7f0900a1

.field public static final create_dialog_activity_height:I = 0x7f09003b

.field public static final create_dialog_activity_land_height:I = 0x7f09003c

.field public static final create_dialog_activity_land_width:I = 0x7f09003d

.field public static final create_dialog_activity_width:I = 0x7f09003e

.field public static final create_dialog_set_as_height:I = 0x7f09003f

.field public static final create_dialog_set_as_width:I = 0x7f090040

.field public static final custom_action_bar_button_divider:I = 0x7f090041

.field public static final data_check_help_text_size:I = 0x7f09008e

.field public static final folder_height:I = 0x7f090042

.field public static final help_popup_picker_text_size:I = 0x7f090043

.field public static final help_popup_picker_text_top_margin:I = 0x7f090044

.field public static final help_text_margin_top:I = 0x7f0900a2

.field public static final hover_common_view_max_height:I = 0x7f090045

.field public static final hover_common_view_max_width:I = 0x7f090046

.field public static final hover_common_view_min_width:I = 0x7f090047

.field public static final hover_common_view_padding_bottom:I = 0x7f090048

.field public static final hover_common_view_padding_horizontal:I = 0x7f090049

.field public static final hover_common_view_padding_top:I = 0x7f09004a

.field public static final hover_common_view_text_size:I = 0x7f09004b

.field public static final hover_seekbar_max_height:I = 0x7f09004c

.field public static final hover_seekbar_max_width:I = 0x7f09004d

.field public static final hover_seekbar_text_size:I = 0x7f09004e

.field public static final listContainer_merginRight:I = 0x7f09004f

.field public static final listContainer_merginTop:I = 0x7f090050

.field public static final list_count_mergin:I = 0x7f090051

.field public static final list_header_title_text:I = 0x7f090052

.field public static final list_item_title_text_height:I = 0x7f090053

.field public static final list_left_right_margin:I = 0x7f09008f

.field public static final list_main_header_select_all_layout_height:I = 0x7f090054

.field public static final list_main_header_text:I = 0x7f090055

.field public static final list_text1_margin_left:I = 0x7f090056

.field public static final list_text1_margin_right:I = 0x7f090057

.field public static final list_text1_text:I = 0x7f090058

.field public static final list_text2_text_2:I = 0x7f090059

.field public static final music_action_bar_height:I = 0x7f09005a

.field public static final music_action_bar_tab_maxwidth:I = 0x7f09005b

.field public static final music_action_bar_tab_minwidth:I = 0x7f09005c

.field public static final music_action_tab_button_minwidth:I = 0x7f09005d

.field public static final music_action_tab_text_padding_left:I = 0x7f09005e

.field public static final music_conversation_text:I = 0x7f09005f

.field public static final music_conversation_text_margin_bottom:I = 0x7f090060

.field public static final music_conversation_text_margin_top:I = 0x7f090061

.field public static final no_item_text:I = 0x7f090062

.field public static final normal_height:I = 0x7f090063

.field public static final normal_list_albumart_size:I = 0x7f090064

.field public static final normal_list_height:I = 0x7f090065

.field public static final padding_Bottom_popup_picker:I = 0x7f090066

.field public static final padding_Left_popup_picker:I = 0x7f090067

.field public static final padding_Right_popup_picker:I = 0x7f090068

.field public static final padding_top_popup_picker:I = 0x7f090069

.field public static final palm_motion_text_margin_left:I = 0x7f09006a

.field public static final palm_motion_text_margin_right:I = 0x7f09006b

.field public static final palm_motion_text_width:I = 0x7f09006c

.field public static final palm_tuto_done_text_size:I = 0x7f09006d

.field public static final palm_tuto_done_text_top_Padding:I = 0x7f09006e

.field public static final palm_tutorial_album_size:I = 0x7f09006f

.field public static final picker_progress_top_margin:I = 0x7f090070

.field public static final select_all_checkbox_margin:I = 0x7f090071

.field public static final set_as_activity_textview_padding_left:I = 0x7f090072

.field public static final set_as_divider_size:I = 0x7f090073

.field public static final set_as_from_begin_row_height:I = 0x7f090090

.field public static final set_as_left_space:I = 0x7f090074

.field public static final set_as_offset_time_text_left_margin:I = 0x7f090075

.field public static final set_as_progress_bar_width:I = 0x7f090076

.field public static final set_as_progress_height:I = 0x7f090091

.field public static final set_as_progress_margin_bottom:I = 0x7f090092

.field public static final set_as_progress_margin_top:I = 0x7f090093

.field public static final set_as_progress_text_size:I = 0x7f090094

.field public static final set_as_radio_button_right_margin:I = 0x7f090095

.field public static final set_as_recommend_padding_bottom:I = 0x7f090077

.field public static final set_as_right_space:I = 0x7f090078

.field public static final set_as_sub_text_size:I = 0x7f090096

.field public static final set_as_text_height:I = 0x7f090097

.field public static final set_as_text_padding:I = 0x7f090098

.field public static final set_as_text_size:I = 0x7f090079

.field public static final set_as_view_left_margin:I = 0x7f090099

.field public static final sound_picker_checkbox_margin:I = 0x7f09007a

.field public static final sound_picker_item_albumart:I = 0x7f09007b

.field public static final sound_picker_item_albumart_layout:I = 0x7f09007c

.field public static final sound_picker_item_albumart_margin:I = 0x7f09007d

.field public static final sound_picker_item_height:I = 0x7f09007e

.field public static final sound_picker_item_left_margin:I = 0x7f09007f

.field public static final sound_picker_item_margin:I = 0x7f090080

.field public static final sound_picker_item_margin_right:I = 0x7f090081

.field public static final sound_picker_item_padding_bottom:I = 0x7f090082

.field public static final sound_picker_item_padding_right:I = 0x7f090083

.field public static final sound_picker_item_padding_top:I = 0x7f090084

.field public static final sound_picker_item_progress_height:I = 0x7f090085

.field public static final sound_picker_item_progress_margin:I = 0x7f090086

.field public static final sound_picker_item_progress_margin_top:I = 0x7f090087

.field public static final sound_picker_item_right_margin:I = 0x7f090088

.field public static final sound_picker_item_text1:I = 0x7f090089

.field public static final sound_picker_item_text1_margin_top:I = 0x7f09008a

.field public static final sound_picker_item_text1_padding_right:I = 0x7f09008b

.field public static final sound_picker_item_text2:I = 0x7f09008c

.field public static final sound_picker_list_mergin:I = 0x7f09008d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
