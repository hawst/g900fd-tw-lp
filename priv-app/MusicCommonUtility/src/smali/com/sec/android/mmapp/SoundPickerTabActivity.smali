.class public Lcom/sec/android/mmapp/SoundPickerTabActivity;
.super Landroid/app/Activity;
.source "SoundPickerTabActivity.java"

# interfaces
.implements Lcom/sec/android/mmapp/SoundPickerListFragment$OnLoadFinishListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# instance fields
.field private mDoUpdateTabs:Z

.field private mIsChangingScreen:Z

.field private mIsMultiplePicker:Z

.field private mIsSavedState:Z

.field private mMainView:Landroid/view/View;

.field private mPlayMode:I

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mSearchId:J

.field private mSelectedIDList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTabSelectedCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsChangingScreen:Z

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mDoUpdateTabs:Z

    .line 58
    iput-boolean v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsSavedState:Z

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsMultiplePicker:Z

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    .line 64
    iput v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mPlayMode:I

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    .line 225
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/SoundPickerTabActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerTabActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mTabSelectedCount:I

    return v0
.end method

.method static synthetic access$108(Lcom/sec/android/mmapp/SoundPickerTabActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerTabActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mTabSelectedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mTabSelectedCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/SoundPickerTabActivity;Landroid/app/ActionBar$Tab;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerTabActivity;
    .param p1, "x1"    # Landroid/app/ActionBar$Tab;
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getSelectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/SoundPickerTabActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerTabActivity;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsChangingScreen:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/SoundPickerTabActivity;Landroid/app/ActionBar$Tab;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerTabActivity;
    .param p1, "x1"    # Landroid/app/ActionBar$Tab;
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getUnselectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/SoundPickerTabActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerTabActivity;

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    return-wide v0
.end method

.method private addTab(Landroid/app/ActionBar;I)V
    .locals 3
    .param p1, "bar"    # Landroid/app/ActionBar;
    .param p2, "list"    # I

    .prologue
    .line 542
    invoke-static {p2}, Lcom/sec/android/mmapp/util/MusicListUtils;->getTabName(I)I

    move-result v0

    .line 543
    .local v0, "tabTextId":I
    invoke-virtual {p1}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    new-instance v2, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;

    invoke-direct {v2, p0, p0, p2}, Lcom/sec/android/mmapp/SoundPickerTabActivity$TabListener;-><init>(Lcom/sec/android/mmapp/SoundPickerTabActivity;Landroid/app/Activity;I)V

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 545
    return-void
.end method

.method private doUpdateTabs()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 517
    .local v0, "bar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    .line 518
    .local v1, "selected":I
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsChangingScreen:Z

    .line 519
    invoke-virtual {v0}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 520
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 521
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020057

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 523
    const v2, 0x20001

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 524
    const v2, 0x10002

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 525
    const v2, 0x10003

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 526
    const v2, 0x10007

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 527
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 528
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setTabTalkbackMessage()V

    .line 529
    iput-boolean v4, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsChangingScreen:Z

    .line 530
    invoke-direct {p0, v0, v4}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setHasEmbeddedTabs(Landroid/app/ActionBar;Z)V

    .line 531
    return-void
.end method

.method private getSelectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;
    .locals 6
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "tabCount"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 579
    .local v0, "sb":Ljava/lang/StringBuilder;
    const v1, 0x7f0c0008

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    const v1, 0x7f0c0006

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getUnselectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;
    .locals 6
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "tabCount"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 588
    .local v0, "sb":Ljava/lang/StringBuilder;
    const v1, 0x7f0c0007

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    const v1, 0x7f0c0006

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 592
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private launchQueryBrowser()V
    .locals 3

    .prologue
    .line 195
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 196
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "startSearch"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 197
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 198
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 199
    return-void
.end method

.method private setHasEmbeddedTabs(Landroid/app/ActionBar;Z)V
    .locals 7
    .param p1, "bar"    # Landroid/app/ActionBar;
    .param p2, "hasEmbeddedTabs"    # Z

    .prologue
    .line 608
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setHasEmbeddedTabs"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 610
    .local v1, "setHasEmbeddedTabs":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 611
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 612
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 623
    .end local v1    # "setHasEmbeddedTabs":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 615
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    const-string v3, "setHasEmbeddedTabs() - NoSuchMethodException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 616
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 617
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    const-string v3, "setHasEmbeddedTabs() - IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 618
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    const-string v3, "setHasEmbeddedTabs() - IllegalAccessException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 620
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 621
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v2, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    const-string v3, "setHasEmbeddedTabs() - InvocationTargetException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setTabTalkbackMessage()V
    .locals 5

    .prologue
    .line 569
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 570
    .local v0, "bar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->getTabCount()I

    move-result v3

    .line 571
    .local v3, "tabCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 572
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v2

    .line 573
    .local v2, "tab":Landroid/app/ActionBar$Tab;
    invoke-direct {p0, v2, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getUnselectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 571
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 575
    .end local v2    # "tab":Landroid/app/ActionBar$Tab;
    :cond_0
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 220
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 221
    const v0, 0x7f050002

    const v1, 0x7f050003

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->overridePendingTransition(II)V

    .line 223
    :cond_0
    return-void
.end method

.method public getPlayMode()I
    .locals 1

    .prologue
    .line 558
    iget v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mPlayMode:I

    return v0
.end method

.method public getSelectedIDList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSelectedItemsCount()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    .line 154
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public isItemSelected(Ljava/lang/String;)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_1

    .line 159
    :cond_0
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 15
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-super/range {p0 .. p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 75
    const/4 v4, 0x0

    .line 76
    .local v4, "Title_OK":I
    const/4 v2, 0x1

    .line 77
    .local v2, "Album_OK":I
    const/4 v3, 0x2

    .line 79
    .local v3, "Artist_OK":I
    if-nez p3, :cond_0

    .line 126
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    .line 85
    .local v7, "bar":Landroid/app/ActionBar;
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 120
    :pswitch_0
    const-string v12, "title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iput-wide v12, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    .line 121
    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_0

    .line 88
    :pswitch_1
    const-string v12, "artist"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 89
    .local v6, "artist":Ljava/lang/String;
    const-string v12, "album"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 91
    .local v5, "album":Ljava/lang/String;
    const/4 v10, 0x0

    .line 92
    .local v10, "key":Ljava/lang/String;
    const/4 v11, 0x0

    .line 94
    .local v11, "subList":I
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 95
    const/4 v12, 0x2

    invoke-virtual {v7, v12}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 96
    move-object v10, v6

    .line 97
    const v12, 0x10003

    invoke-static {v12}, Lcom/sec/android/mmapp/util/MusicListUtils;->getSubTrackList(I)I

    move-result v11

    .line 103
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 105
    invoke-static {v11, v10}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getNewInstance(ILjava/lang/String;)Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-result-object v8

    .line 106
    .local v8, "fg":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v9

    .line 107
    .local v9, "ft":Landroid/app/FragmentTransaction;
    const v12, 0x7f0f0029

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v12, v8, v13}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 109
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 110
    invoke-virtual {v9}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 98
    .end local v8    # "fg":Landroid/app/Fragment;
    .end local v9    # "ft":Landroid/app/FragmentTransaction;
    :cond_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 99
    const/4 v12, 0x1

    invoke-virtual {v7, v12}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 100
    move-object v10, v5

    .line 101
    const v12, 0x10002

    invoke-static {v12}, Lcom/sec/android/mmapp/util/MusicListUtils;->getSubTrackList(I)I

    move-result v11

    goto :goto_1

    .line 85
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 214
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 215
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 464
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged newConfig : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    .line 476
    .local v6, "bar":Landroid/app/ActionBar;
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v9, :cond_1

    .line 477
    invoke-virtual {v6, v7, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 486
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 488
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v9, :cond_3

    .line 494
    const/16 v3, 0x35

    .line 495
    .local v3, "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 497
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 505
    .local v5, "height":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mmapp/util/MusicListUtils;->updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V

    .line 509
    .end local v3    # "gravity":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_0
    invoke-direct {p0, v6, v8}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setHasEmbeddedTabs(Landroid/app/ActionBar;Z)V

    .line 510
    return-void

    .line 479
    :cond_1
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_2

    .line 480
    invoke-virtual {v6, v7, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_0

    .line 482
    :cond_2
    invoke-virtual {v6, v8, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_0

    .line 500
    :cond_3
    const/16 v3, 0x35

    .line 501
    .restart local v3    # "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 503
    .restart local v4    # "width":I
    const/4 v5, -0x1

    .restart local v5    # "height":I
    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 325
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 327
    .local v7, "i":Landroid/content/Intent;
    const-string v0, "isMultiple"

    invoke-virtual {v7, v0, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsMultiplePicker:Z

    .line 329
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setVolumeControlStream(I)V

    .line 331
    const v0, 0x7f04000d

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setContentView(I)V

    .line 333
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 338
    const v0, 0x7f0f002e

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mMainView:Landroid/view/View;

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v12, :cond_5

    .line 340
    const/16 v3, 0x35

    .line 341
    .local v3, "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 343
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 353
    .local v5, "height":I
    :goto_0
    invoke-virtual {p0, v12}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setFinishOnTouchOutside(Z)V

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mmapp/util/MusicListUtils;->updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 363
    .local v8, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 366
    .end local v3    # "gravity":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    .end local v8    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    .line 370
    .local v6, "bar":Landroid/app/ActionBar;
    invoke-virtual {v6, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 372
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_6

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 379
    :goto_1
    invoke-virtual {v6, v13}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 381
    const v0, 0x20001

    invoke-direct {p0, v6, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 382
    const v0, 0x10002

    invoke-direct {p0, v6, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 383
    const v0, 0x10003

    invoke-direct {p0, v6, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 384
    const v0, 0x10007

    invoke-direct {p0, v6, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->addTab(Landroid/app/ActionBar;I)V

    .line 386
    if-eqz p1, :cond_1

    .line 387
    const-string v0, "tab"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 388
    const-string v0, "autoRecommendation"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setPlayMode(I)V

    .line 391
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v12, :cond_7

    .line 392
    invoke-virtual {v6, v11, v11}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 401
    :goto_2
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-nez v0, :cond_3

    :cond_2
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_STREAMING_LAYOUT:Z

    if-eqz v0, :cond_9

    .line 402
    :cond_3
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 410
    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setTabTalkbackMessage()V

    .line 411
    invoke-virtual {v6}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v9

    .line 412
    .local v9, "selectedTab":Landroid/app/ActionBar$Tab;
    invoke-virtual {v6}, Landroid/app/ActionBar;->getTabCount()I

    move-result v0

    invoke-direct {p0, v9, v0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getSelectedTabTalkback(Landroid/app/ActionBar$Tab;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 419
    invoke-direct {p0, v6, v10}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setHasEmbeddedTabs(Landroid/app/ActionBar;Z)V

    .line 420
    return-void

    .line 347
    .end local v6    # "bar":Landroid/app/ActionBar;
    .end local v9    # "selectedTab":Landroid/app/ActionBar$Tab;
    :cond_5
    const/16 v3, 0x35

    .line 348
    .restart local v3    # "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 350
    .restart local v4    # "width":I
    const/4 v5, -0x1

    .restart local v5    # "height":I
    goto/16 :goto_0

    .line 375
    .end local v3    # "gravity":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    .restart local v6    # "bar":Landroid/app/ActionBar;
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 376
    const-string v0, ""

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 394
    :cond_7
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_8

    .line 395
    invoke-virtual {v6, v11, v11}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_2

    .line 397
    :cond_8
    invoke-virtual {v6, v10, v11}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_2

    .line 404
    :cond_9
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/mmapp/ThemeManager;->getTheme()I

    move-result v0

    if-ne v0, v12, :cond_4

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f0c0025

    const/4 v2, 0x0

    .line 171
    iget-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsMultiplePicker:Z

    if-nez v1, :cond_1

    .line 172
    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v3, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020055

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 174
    .local v0, "search":Landroid/view/MenuItem;
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v1, :cond_1

    .line 175
    :cond_0
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 179
    .end local v0    # "search":Landroid/view/MenuItem;
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 549
    sget-object v0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy mPlayer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 551
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 203
    packed-switch p1, :pswitch_data_0

    .line 208
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 205
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->launchQueryBrowser()V

    .line 206
    const/4 v0, 0x0

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x54
        :pswitch_0
    .end packed-switch
.end method

.method public onLoadFinished(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 4
    .param p1, "fragment"    # Lcom/sec/android/mmapp/SoundPickerListFragment;

    .prologue
    .line 563
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 564
    invoke-virtual {p0, p1}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->setSelected(Landroid/app/Fragment;)V

    .line 566
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 184
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 191
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 186
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->launchQueryBrowser()V

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x7f0c0025
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 436
    .local v0, "currentTab":I
    const v2, 0x20001

    if-eq v0, v2, :cond_0

    .line 437
    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getSubTrackList(I)I

    move-result v0

    .line 439
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 440
    .local v1, "fg":Landroid/app/Fragment;
    instance-of v2, v1, Lcom/sec/android/mmapp/SoundPickerListFragment;

    if-eqz v2, :cond_1

    .line 441
    check-cast v1, Lcom/sec/android/mmapp/SoundPickerListFragment;

    .end local v1    # "fg":Landroid/app/Fragment;
    invoke-virtual {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 443
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 444
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 424
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 425
    iget-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 431
    :goto_0
    return-void

    .line 428
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 429
    .local v0, "bar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    .line 430
    .local v1, "selected":I
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 535
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsSavedState:Z

    .line 536
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 537
    const-string v0, "tab"

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 538
    const-string v0, "autoRecommendation"

    invoke-virtual {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->getPlayMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 539
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 448
    iput-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mIsSavedState:Z

    .line 449
    iget-boolean v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mDoUpdateTabs:Z

    if-eqz v0, :cond_0

    .line 450
    invoke-direct {p0}, Lcom/sec/android/mmapp/SoundPickerTabActivity;->doUpdateTabs()V

    .line 451
    iput-boolean v1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mDoUpdateTabs:Z

    .line 453
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 454
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 458
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 459
    return-void
.end method

.method public setPlayMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 554
    iput p1, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mPlayMode:I

    .line 555
    return-void
.end method

.method public setSelected(Landroid/app/Fragment;)V
    .locals 4
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    const-wide/16 v2, -0x1

    .line 129
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 130
    check-cast p1, Lcom/sec/android/mmapp/SoundPickerListFragment;

    .end local p1    # "fragment":Landroid/app/Fragment;
    iget-wide v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->setSelected(J)V

    .line 131
    iput-wide v2, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSearchId:J

    .line 133
    :cond_0
    return-void
.end method

.method public updateSelectedIDs(ZJ)V
    .locals 2
    .param p1, "checked"    # Z
    .param p2, "value"    # J

    .prologue
    .line 136
    if-eqz p1, :cond_2

    .line 137
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
