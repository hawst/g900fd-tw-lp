.class Lcom/sec/android/mmapp/SoundPickerListFragment$6;
.super Landroid/os/Handler;
.source "SoundPickerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 625
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 628
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 630
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 631
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->resetSelectedId()V

    .line 635
    :cond_0
    const/4 v0, 0x0

    .line 636
    .local v0, "enabled":Z
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 637
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 638
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 639
    const/4 v0, 0x1

    .line 647
    :cond_1
    :goto_0
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v1, :cond_5

    .line 648
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 649
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 650
    if-eqz v0, :cond_4

    .line 651
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 661
    :cond_2
    :goto_1
    return-void

    .line 642
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1300(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->sIsError:Z
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$700()Z

    move-result v1

    if-nez v1, :cond_1

    .line 643
    const/4 v0, 0x1

    goto :goto_0

    .line 653
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCustomOkButton:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$800(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x3f333333    # 0.7f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_1

    .line 657
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 658
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$6;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mOkButton:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$900(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method
