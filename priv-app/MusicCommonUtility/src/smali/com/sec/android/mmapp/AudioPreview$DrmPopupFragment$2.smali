.class Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;)V
    .locals 0

    .prologue
    .line 3172
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v8, 0x0

    .line 3175
    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "path"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3177
    .local v4, "path":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    # invokes: Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getAudioId(Ljava/lang/String;)J
    invoke-static {v6, v4}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->access$7200(Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;Ljava/lang/String;)J

    move-result-wide v2

    .line 3178
    .local v2, "id":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 3179
    sget-object v6, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 3181
    .local v5, "uri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3182
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v0, v5, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3184
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3185
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3186
    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0c0016

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 3193
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->dismiss()V

    .line 3194
    iget-object v6, p0, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment$2;->this$0:Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;

    invoke-virtual {v6}, Lcom/sec/android/mmapp/AudioPreview$DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    .line 3195
    return-void

    .line 3190
    :cond_1
    const-string v6, "MusicDrm"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to delete file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
