.class public Lcom/sec/android/mmapp/library/RingtoneRecommender;
.super Ljava/lang/Object;
.source "RingtoneRecommender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final HIGHLIGHT_OFFSET:Ljava/lang/String; = "highlight_offset"

.field public static final OPEN_ERR_NOT_ENOUGH_MEMORY:I = -0x2

.field public static final OPEN_ERR_NOT_OPEN_FILE:I = -0x7

.field public static final OPEN_ERR_UNSUPPORT_FILE_TYPE:I = -0x3

.field public static final OPEN_SUCCESS:I = 0x0

.field public static final RESULT_EXTRACT:I = 0x5

.field public static final RESULT_QUIT:I = 0x6

.field public static final SMAT_MODE_FAST:I = 0x0

.field public static final SMAT_MODE_FULL:I = 0x1


# instance fields
.field private mIsOpen:Z

.field private mListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

.field private mMode:I

.field private final mRecommender:Lcom/samsung/audio/Smat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/mmapp/library/RingtoneRecommender;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-boolean v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    .line 92
    iput v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mMode:I

    .line 131
    new-instance v0, Lcom/samsung/audio/Smat;

    invoke-direct {v0}, Lcom/samsung/audio/Smat;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/mmapp/library/RingtoneRecommender;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/library/RingtoneRecommender;)Lcom/samsung/audio/Smat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/library/RingtoneRecommender;)Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/library/RingtoneRecommender;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    return-object v0
.end method

.method private extract()Z
    .locals 4

    .prologue
    .line 236
    iget-object v1, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v1}, Lcom/samsung/audio/Smat;->extract()I

    move-result v0

    .line 237
    .local v0, "result":I
    sget-object v1, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "extract() result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    if-nez v0, :cond_0

    .line 239
    new-instance v1, Lcom/sec/android/mmapp/library/RingtoneRecommender$1;

    const-string v2, "Recommender thread"

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mmapp/library/RingtoneRecommender$1;-><init>(Lcom/sec/android/mmapp/library/RingtoneRecommender;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/mmapp/library/RingtoneRecommender$1;->start()V

    .line 280
    const/4 v1, 0x1

    .line 282
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getResultIntent(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "offset"    # I

    .prologue
    .line 315
    sget-object v1, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getResultIntent() - uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " msec"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 317
    .local v0, "intent":Landroid/content/Intent;
    if-lez p1, :cond_0

    .line 318
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "highlight_offset"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 321
    :cond_0
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 322
    return-object v0
.end method


# virtual methods
.method public close()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 218
    sget-object v1, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "close() is opened ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-boolean v1, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v1}, Lcom/samsung/audio/Smat;->deinit()I

    move-result v1

    if-nez v1, :cond_0

    .line 221
    iput-boolean v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    .line 223
    const/4 v0, 0x1

    .line 226
    :cond_0
    return v0
.end method

.method public doExtract(Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    .prologue
    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    .line 205
    iput-object p1, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mListener:Lcom/sec/android/mmapp/library/RingtoneRecommender$OnHighlightResultListener;

    .line 206
    invoke-direct {p0}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->extract()Z

    move-result v0

    return v0
.end method

.method public isOpen()Z
    .locals 3

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isOpen() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-boolean v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    return v0
.end method

.method public open(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mmapp/library/RingtoneRecommender;->open(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public open(Ljava/lang/String;I)I
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 185
    iput p2, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mMode:I

    .line 186
    iget-object v1, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/audio/Smat;->init(Ljava/lang/String;I)I

    move-result v0

    .line 187
    .local v0, "result":I
    sget-object v1, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "open() result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    return v0
.end method

.method public quit()Z
    .locals 3

    .prologue
    .line 295
    sget-object v0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "quit() is opened : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-boolean v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mIsOpen:Z

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/sec/android/mmapp/library/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v0}, Lcom/samsung/audio/Smat;->quit()I

    move-result v0

    if-nez v0, :cond_0

    .line 298
    const/4 v0, 0x1

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
