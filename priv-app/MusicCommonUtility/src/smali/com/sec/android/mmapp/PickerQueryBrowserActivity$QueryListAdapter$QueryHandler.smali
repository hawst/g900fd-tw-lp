.class Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "PickerQueryBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2, "res"    # Landroid/content/ContentResolver;

    .prologue
    .line 602
    iput-object p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;->this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    .line 603
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 604
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 608
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;->this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->access$400(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;->this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->access$400(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;->this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    invoke-static {v2}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->access$500(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    move-result-object v2

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$600(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;->this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    invoke-static {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->access$500(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->init(Landroid/database/Cursor;)V

    .line 611
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;->this$1:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    invoke-static {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->access$500(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 612
    .local v0, "empty":Landroid/widget/TextView;
    const v1, 0x7f0c002c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 613
    return-void
.end method
