.class Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "SoundPickerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SoundPickerListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final MAX_PROGRESS:I = 0x3e8


# instance fields
.field private mPlayingButton:Landroid/widget/ImageView;

.field private mProgress:Landroid/widget/ProgressBar;

.field private final mProgressUpdateHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 1530
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    .line 1531
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 1775
    new-instance v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$1;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgressUpdateHandler:Landroid/os/Handler;

    .line 1532
    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    .prologue
    .line 1507
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;

    .prologue
    .line 1507
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgressUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getArtwork(Landroid/content/Context;Landroid/database/Cursor;Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "vh"    # Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    .param p4, "args"    # Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;

    .prologue
    .line 1826
    iget-wide v2, p4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->getCachedArtworkWithoutMaking(J)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1827
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_0

    .line 1828
    iget-object v2, p3, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1829
    new-instance v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;

    invoke-direct {v0}, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;-><init>()V

    .line 1830
    .local v0, "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    iget-object v2, p3, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->iv:Landroid/widget/ImageView;

    .line 1831
    iget-wide v2, p4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    iput-wide v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->albumId:J

    .line 1832
    iput-object p1, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->context:Landroid/content/Context;

    .line 1833
    iget v2, p4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->scrollState:I

    iput v2, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->scrollState:I

    .line 1835
    iget-object v2, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;
    invoke-static {v2}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$3100(Lcom/sec/android/mmapp/SoundPickerListFragment;)Lcom/sec/android/mmapp/util/AlbumArtLoader;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->getArtwork(ILcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;)V

    .line 1837
    .end local v0    # "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    :cond_0
    return-object v1
.end method

.method private isPersonalFile(Landroid/database/Cursor;)Z
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1753
    const/4 v2, 0x0

    .line 1754
    .local v2, "result":Z
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/mmapp/util/MusicListUtils;->getPreDefinedList(I)I

    move-result v3

    const v4, 0x10007

    if-ne v3, v4, :cond_1

    .line 1755
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mText2Idx:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2200(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1756
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->PERSONAL_FOLDER:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2600(Lcom/sec/android/mmapp/SoundPickerListFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1757
    const/4 v2, 0x1

    .line 1766
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 1760
    :cond_1
    const-string v3, "is_secretbox"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1761
    .local v0, "i":I
    if-lez v0, :cond_0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1762
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private setPersonalIcon(Landroid/view/View;Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "vh"    # Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1734
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/mmapp/util/MusicListUtils;->getPreDefinedList(I)I

    move-result v0

    const v1, 0x10007

    if-ne v0, v1, :cond_1

    .line 1737
    :cond_0
    invoke-direct {p0, p3}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->isPersonalFile(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1738
    const v0, 0x7f0f0001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->personalIcon:Landroid/widget/ImageView;

    .line 1740
    iget-object v0, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->personalIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1741
    iget-object v0, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->personalIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1750
    :cond_1
    :goto_0
    return-void

    .line 1744
    :cond_2
    iget-object v0, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->personalIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1745
    iget-object v0, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->personalIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1567
    const/4 v0, 0x0

    return v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 19
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1628
    const-wide/16 v14, -0x65

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-nez v14, :cond_1

    .line 1629
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "bindView mCountOfData : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1632
    const v14, 0x7f0f0017

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1633
    .local v10, "text":Landroid/widget/TextView;
    const/4 v14, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1634
    .local v4, "countOfItems":I
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mNumberTextId:I
    invoke-static {v15}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2300(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1636
    .local v8, "number":Ljava/lang/String;
    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1727
    .end local v4    # "countOfItems":I
    .end local v8    # "number":Ljava/lang/String;
    .end local v10    # "text":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 1640
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;

    .line 1642
    .local v13, "vh":Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v14

    if-nez v14, :cond_8

    .line 1643
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->divider:Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 1648
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v13, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->setThumbnailView(Landroid/content/Context;Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;Landroid/database/Cursor;)V

    .line 1649
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v13, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->setPersonalIcon(Landroid/view/View;Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;Landroid/database/Cursor;)V

    .line 1651
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mText1Idx:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2400(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1652
    .local v11, "text1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    const v15, 0x10003

    if-ne v14, v15, :cond_3

    .line 1653
    if-eqz v11, :cond_2

    const-string v14, "<unknown>"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1654
    :cond_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "<"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v15}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c0051

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ">"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1657
    :cond_3
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual {v14, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1658
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f08002d

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1661
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mText2Idx:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2200(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    if-ltz v14, :cond_7

    .line 1662
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mText2Idx:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2200(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1663
    .local v12, "text2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    const v15, 0x10007

    if-ne v14, v15, :cond_4

    .line 1665
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "bindView - folderTab - text2: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667
    const-string v14, ""

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 1668
    const-string v14, "/"

    const/4 v15, 0x1

    invoke-virtual {v12, v14, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 1669
    .local v9, "start":I
    const-string v14, "/"

    invoke-virtual {v12, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 1670
    .local v5, "end":I
    const/4 v14, -0x1

    if-eq v9, v14, :cond_4

    const/4 v14, -0x1

    if-eq v5, v14, :cond_4

    .line 1671
    invoke-virtual {v12, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1676
    .end local v5    # "end":I
    .end local v9    # "start":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    const v15, 0x10003

    if-eq v14, v15, :cond_6

    .line 1677
    if-eqz v12, :cond_5

    const-string v14, "<unknown>"

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1678
    :cond_5
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "<"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v15}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0c0051

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ">"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1682
    :cond_6
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v14, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1685
    .end local v12    # "text2":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    invoke-static {v14}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1690
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mAudioIdIdx:I
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2500(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1691
    .local v6, "id":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1692
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v15}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v15

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v15

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1693
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1698
    :goto_2
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " bindView id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " sel="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1300(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " playing="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1600(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " cursor="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/mmapp/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1701
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1300(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v14

    cmp-long v14, v6, v14

    if-nez v14, :cond_c

    .line 1702
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbBtn:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mPlayingButton:Landroid/widget/ImageView;

    .line 1703
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mPlayingButton:Landroid/widget/ImageView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1704
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v14

    if-eqz v14, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v14

    invoke-virtual {v14}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1705
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mPlayingButton:Landroid/widget/ImageView;

    const v15, 0x7f020015

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1709
    :goto_3
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f080032

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1718
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mPlayingId:J
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1600(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v14

    cmp-long v14, v6, v14

    if-nez v14, :cond_d

    .line 1719
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    .line 1720
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1721
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgressUpdateHandler:Landroid/os/Handler;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/os/Handler;->removeMessages(I)V

    .line 1722
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgressUpdateHandler:Landroid/os/Handler;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1645
    .end local v6    # "id":J
    .end local v11    # "text1":Ljava/lang/String;
    :cond_8
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1695
    .restart local v6    # "id":J
    .restart local v11    # "text1":Ljava/lang/String;
    :cond_9
    iget-object v15, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->radio:Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mSelectedId:J
    invoke-static {v14}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1300(Lcom/sec/android/mmapp/SoundPickerListFragment;)J

    move-result-wide v16

    cmp-long v14, v6, v16

    if-nez v14, :cond_a

    const/4 v14, 0x1

    :goto_5
    invoke-virtual {v15, v14}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_2

    :cond_a
    const/4 v14, 0x0

    goto :goto_5

    .line 1707
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mPlayingButton:Landroid/widget/ImageView;

    const v15, 0x7f020016

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 1712
    :cond_c
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbBtn:Landroid/widget/ImageView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 1724
    :cond_d
    iget-object v14, v13, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1547
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I
    invoke-static {v1}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 1548
    const/4 p2, 0x0

    .line 1550
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1552
    const/4 p2, 0x0

    .line 1555
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1556
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;

    .line 1557
    .local v0, "vh":Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_2

    .line 1558
    iget-object v1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getId()I

    move-result v1

    iget-object v2, v0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getId()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 1559
    const/4 p2, 0x0

    .line 1563
    .end local v0    # "vh":Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mCountOfData:I
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1538
    const/4 v0, 0x0

    .line 1540
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ResourceCursorAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 1572
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newView"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mmapp/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1574
    const-wide/16 v4, -0x65

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 1576
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1578
    .local v0, "li":Landroid/view/LayoutInflater;
    const v3, 0x7f040008

    invoke-virtual {v0, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1623
    .end local v0    # "li":Landroid/view/LayoutInflater;
    :goto_0
    return-object v1

    .line 1580
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1581
    .local v1, "v":Landroid/view/View;
    new-instance v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;

    invoke-direct {v2, p0}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;-><init>(Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;)V

    .line 1582
    .local v2, "vh":Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1583
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1584
    const v3, 0x7f0f001c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    .line 1585
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1594
    :cond_1
    :goto_1
    const v3, 0x7f0f0020

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    .line 1595
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    const v4, 0x10007

    if-ne v3, v4, :cond_2

    .line 1597
    const v3, 0x7f0f0011

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnailIcon:Landroid/widget/ImageView;

    .line 1598
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnailIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1600
    :cond_2
    const v3, 0x7f0f0021

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbBtn:Landroid/widget/ImageView;

    .line 1601
    const v3, 0x7f0f0030

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    .line 1602
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1603
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1604
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1605
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1607
    :cond_3
    const v3, 0x7f0f0023

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    .line 1608
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v3, :cond_4

    .line 1609
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mText2Idx:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2200(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    if-ltz v3, :cond_7

    .line 1610
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1615
    :cond_4
    :goto_2
    const v3, 0x7f0f0017

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    .line 1616
    const v3, 0x7f0f001d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->divider:Landroid/view/View;

    .line 1618
    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsMultiplePicker:Z
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1200(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mList:I
    invoke-static {v3}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$1100(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/mmapp/util/MusicListUtils;->isTrack(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1619
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->divider:Landroid/view/View;

    const v4, 0x7f08000f

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1622
    :cond_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1587
    :cond_6
    const v3, 0x7f0f002f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->radio:Landroid/widget/RadioButton;

    .line 1588
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->radio:Landroid/widget/RadioButton;

    if-eqz v3, :cond_1

    .line 1589
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->radio:Landroid/widget/RadioButton;

    invoke-virtual {v3, v8}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 1590
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->radio:Landroid/widget/RadioButton;

    invoke-virtual {v3, v9}, Landroid/widget/RadioButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1612
    :cond_7
    iget-object v3, v2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public releaseProgressHandler()V
    .locals 2

    .prologue
    .line 1787
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgressUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1788
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1789
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1791
    :cond_0
    return-void
.end method

.method protected setThumbnailView(Landroid/content/Context;Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vh"    # Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1801
    const-wide/16 v0, -0x1

    .line 1802
    .local v0, "albumId":J
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumIdIdx:I
    invoke-static {v5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$3000(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v5

    if-ltz v5, :cond_0

    .line 1803
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mAlbumIdIdx:I
    invoke-static {v5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$3000(Lcom/sec/android/mmapp/SoundPickerListFragment;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1806
    :cond_0
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 1807
    .local v4, "position":I
    new-instance v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;

    invoke-direct {v2}, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;-><init>()V

    .line 1808
    .local v2, "args":Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;
    iput v4, v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->position:I

    .line 1809
    iput-wide v0, v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    .line 1810
    iget-object v5, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbBtn:Landroid/widget/ImageView;

    iput-object v5, v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->button:Landroid/widget/ImageView;

    .line 1811
    iget-object v5, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v5}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mmapp/widget/TwIndexListView;

    iget v5, v5, Lcom/sec/android/mmapp/widget/TwIndexListView;->mScrollState:I

    iput v5, v2, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->scrollState:I

    .line 1813
    iget-object v5, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1814
    iget-object v5, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbBtn:Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    .line 1815
    iget-object v5, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbBtn:Landroid/widget/ImageView;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1819
    :cond_1
    invoke-direct {p0, p1, p3, p2, v2}, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter;->getArtwork(Landroid/content/Context;Landroid/database/Cursor;Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1820
    .local v3, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_2

    .line 1821
    iget-object v5, p2, Lcom/sec/android/mmapp/SoundPickerListFragment$SoundPickerListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1823
    :cond_2
    return-void
.end method
