.class Lcom/sec/android/mmapp/AudioPreview$17;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/AudioPreview;->prepareRemoteControlClient()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 2725
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$17;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlaybackPositionUpdate(J)V
    .locals 3
    .param p1, "newPositionMs"    # J

    .prologue
    .line 2730
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$17;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$17;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getMediaPlayerState()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$3100(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 2731
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$17;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    long-to-int v1, p1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 2733
    :cond_0
    return-void
.end method
