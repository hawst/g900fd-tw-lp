.class public Lcom/sec/android/mmapp/PalmTouchTutorialActivity;
.super Landroid/app/Activity;
.source "PalmTouchTutorialActivity.java"


# instance fields
.field protected final CLASSNAME:Ljava/lang/String;

.field private final TITLE_NAME:Ljava/lang/String;

.field private final mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field protected mAudioManager:Landroid/media/AudioManager;

.field mHandler:Landroid/os/Handler;

.field private mHelpImage1:Landroid/widget/ImageView;

.field private mHelpImage2:Landroid/widget/ImageView;

.field private mIsLandscape:Z

.field protected mMediaPlayer:Landroid/media/MediaPlayer;

.field private mPlaySound:Z

.field private mTouchHelpText:Landroid/widget/TextView;

.field private mUri:Ljava/lang/String;

.field showCompletionToast:Ljava/lang/Runnable;

.field work:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const-class v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage1:Landroid/widget/ImageView;

    .line 44
    iput-object v1, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage2:Landroid/widget/ImageView;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mIsLandscape:Z

    .line 65
    const-string v0, "Over the horizon"

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->TITLE_NAME:Ljava/lang/String;

    .line 182
    new-instance v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$1;-><init>(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->work:Ljava/lang/Runnable;

    .line 189
    new-instance v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$2;-><init>(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->showCompletionToast:Ljava/lang/Runnable;

    .line 200
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHandler:Landroid/os/Handler;

    .line 203
    new-instance v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$3;-><init>(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 249
    new-instance v0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity$4;-><init>(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/PalmTouchTutorialActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mPlaySound:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->stopSound()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mTouchHelpText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/PalmTouchTutorialActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/PalmTouchTutorialActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->startSound()V

    return-void
.end method

.method private startSound()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 138
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    if-nez v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    const-string v3, "StartSound - request audiofocus failed. "

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 144
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    const v2, 0x7f0c0050

    invoke-virtual {p0, v2}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->finish()V

    .line 173
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 157
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    .line 159
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    .line 160
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mUri:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 161
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V

    .line 163
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 168
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preparePlay - IOExceptionException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 166
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    const-string v3, "Can\'t find an uri for \'Over the horizon\'."

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 170
    :catch_1
    move-exception v1

    .line 171
    .local v1, "e":Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preparePlay - IllegalStateException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stopSound()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public isAudioPathBTHeadphone()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 235
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioManager:Landroid/media/AudioManager;

    if-nez v2, :cond_1

    .line 246
    :cond_0
    :goto_0
    return v0

    .line 238
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit16 v2, v2, 0x180

    if-nez v2, :cond_2

    .line 246
    .local v0, "isBt":Z
    :goto_1
    goto :goto_0

    .line 244
    .end local v0    # "isBt":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 298
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onConfigurationChanged() is called"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x400

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 70
    iget-boolean v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mIsLandscape:Z

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/sec/android/mmapp/library/MusicFeatures;->IS_DEFAULT_LAND_MODEL:Z

    if-eqz v3, :cond_4

    .line 71
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->setRequestedOrientation(I)V

    .line 76
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const-string v3, "audio"

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 84
    invoke-virtual {p0, v5}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->requestWindowFeature(I)Z

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 88
    const v3, 0x7f04000f

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->setContentView(I)V

    .line 89
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 90
    .local v1, "f":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.PALM_DOWN"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 93
    const v3, 0x7f0f0031

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage1:Landroid/widget/ImageView;

    .line 94
    const v3, 0x7f0f0032

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage2:Landroid/widget/ImageView;

    .line 96
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage1:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mHelpImage2:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    const v3, 0x7f0f0033

    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mTouchHelpText:Landroid/widget/TextView;

    .line 99
    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 100
    iput-boolean v5, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mPlaySound:Z

    .line 104
    new-instance v2, Landroid/media/RingtoneManager;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    .line 105
    .local v2, "rm":Landroid/media/RingtoneManager;
    const/4 v0, 0x0

    .line 107
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v2}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 110
    :cond_1
    const-string v3, "Over the horizon"

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 111
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mUri:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    .line 119
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 122
    :cond_3
    return-void

    .line 73
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "f":Landroid/content/IntentFilter;
    .end local v2    # "rm":Landroid/media/RingtoneManager;
    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 115
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v1    # "f":Landroid/content/IntentFilter;
    .restart local v2    # "rm":Landroid/media/RingtoneManager;
    :cond_5
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    .line 118
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_6

    .line 119
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v3
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mActivityUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 282
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 283
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 287
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 292
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 289
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->finish()V

    .line 290
    const/4 v0, 0x1

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->stopSound()V

    .line 272
    iget-object v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 273
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 274
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->mPlaySound:Z

    if-eqz v0, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/sec/android/mmapp/PalmTouchTutorialActivity;->startSound()V

    .line 129
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 130
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 135
    return-void
.end method
