.class Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;
.super Landroid/media/session/MediaSession$Callback;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaSessionCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method private constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 2844
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Landroid/media/session/MediaSession$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mmapp/AudioPreview;Lcom/sec/android/mmapp/AudioPreview$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/mmapp/AudioPreview;
    .param p2, "x1"    # Lcom/sec/android/mmapp/AudioPreview$1;

    .prologue
    .line 2844
    invoke-direct {p0, p1}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;-><init>(Lcom/sec/android/mmapp/AudioPreview;)V

    return-void
.end method

.method private onMediaKeyDown(Landroid/view/KeyEvent;)V
    .locals 8
    .param p1, "ke"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 2939
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7000(Lcom/sec/android/mmapp/AudioPreview;)Landroid/media/session/MediaSession;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v0

    .line 2940
    .local v0, "state":Landroid/media/session/PlaybackState;
    if-nez v0, :cond_1

    move-wide v2, v4

    .line 2941
    .local v2, "validActions":J
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2997
    :cond_0
    :goto_1
    return-void

    .line 2940
    .end local v2    # "validActions":J
    :cond_1
    invoke-virtual {v0}, Landroid/media/session/PlaybackState;->getActions()J

    move-result-wide v2

    goto :goto_0

    .line 2943
    .restart local v2    # "validActions":J
    :sswitch_0
    const-wide/16 v6, 0x4

    and-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    .line 2944
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->onPlay()V

    goto :goto_1

    .line 2948
    :sswitch_1
    const-wide/16 v6, 0x2

    and-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    .line 2949
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->onPause()V

    goto :goto_1

    .line 2953
    :sswitch_2
    const-wide/16 v6, 0x20

    and-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    .line 2954
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->onSkipToNext()V

    goto :goto_1

    .line 2958
    :sswitch_3
    const-wide/16 v6, 0x10

    and-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    .line 2959
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->onSkipToPrevious()V

    goto :goto_1

    .line 2963
    :sswitch_4
    const-wide/16 v6, 0x1

    and-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    .line 2964
    invoke-virtual {p0}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->onStop()V

    goto :goto_1

    .line 2968
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2969
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    const-string v4, "KEYCODE_MEDIA_REWIND"

    invoke-static {v1, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2970
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2971
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 2975
    :sswitch_6
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2976
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    const-string v4, "KEYCODE_MEDIA_FAST_FORWARD"

    invoke-static {v1, v4}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2977
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2978
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 2983
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaPlayerState:I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$3500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    const/4 v4, 0x4

    if-ne v1, v4, :cond_2

    .line 2984
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto/16 :goto_1

    .line 2986
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2987
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    goto/16 :goto_1

    .line 2989
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v1, v4}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2990
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$4100(Lcom/sec/android/mmapp/AudioPreview;)Landroid/net/Uri;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v1, v4, v6}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto/16 :goto_1

    .line 2941
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_7
        0x55 -> :sswitch_7
        0x56 -> :sswitch_4
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x59 -> :sswitch_5
        0x5a -> :sswitch_6
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 7
    .param p1, "mediaButtonIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 2894
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MediaSession onMediaButtonEvent() mediaButtonIntent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2897
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7000(Lcom/sec/android/mmapp/AudioPreview;)Landroid/media/session/MediaSession;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2899
    const-string v1, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 2900
    .local v0, "ke":Landroid/view/KeyEvent;
    if-eqz v0, :cond_0

    .line 2901
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2935
    .end local v0    # "ke":Landroid/view/KeyEvent;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v1

    :goto_1
    return v1

    .line 2903
    .restart local v0    # "ke":Landroid/view/KeyEvent;
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->onMediaKeyDown(Landroid/view/KeyEvent;)V

    .line 2904
    const/4 v1, 0x1

    goto :goto_1

    .line 2906
    :pswitch_1
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 2908
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1500(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2909
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 2910
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayerPausedForSeek:Z
    invoke-static {v1, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$1502(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    .line 2912
    :cond_1
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2700(Lcom/sec/android/mmapp/AudioPreview;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x12c

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2914
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v1

    const/16 v2, 0xbb8

    if-le v1, v2, :cond_3

    .line 2916
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v1, v6}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 2917
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;
    invoke-static {v2, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2922
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto :goto_0

    .line 2919
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$2500(Lcom/sec/android/mmapp/AudioPreview;)V

    goto :goto_2

    .line 2925
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v1, v6}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_0

    .line 2901
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2906
    :pswitch_data_1
    .packed-switch 0x59
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 2859
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2860
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->pause()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$400(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 2861
    return-void
.end method

.method public onPlay()V
    .locals 3

    .prologue
    .line 2848
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession onPlay()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2850
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2851
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->start()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1700(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 2855
    :cond_0
    :goto_0
    return-void

    .line 2852
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->checkDrm(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1200(Lcom/sec/android/mmapp/AudioPreview$Player;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2853
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4100(Lcom/sec/android/mmapp/AudioPreview;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->setDataSource(Landroid/net/Uri;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1300(Lcom/sec/android/mmapp/AudioPreview$Player;Landroid/net/Uri;Z)V

    goto :goto_0
.end method

.method public onSeekTo(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 2876
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaSession onSeekTo() pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2877
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    long-to-int v1, p1

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 2878
    return-void
.end method

.method public onSkipToNext()V
    .locals 2

    .prologue
    .line 2882
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession onSkipToNext()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2883
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2600(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2884
    return-void
.end method

.method public onSkipToPrevious()V
    .locals 2

    .prologue
    .line 2888
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession onSkipToPrevious()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2889
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2500(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 2890
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 2865
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession onStop()"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2866
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->stop()V
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$100(Lcom/sec/android/mmapp/AudioPreview$Player;)V

    .line 2869
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mDrmManager:Lcom/sec/android/mmapp/library/DrmManager;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$4200(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/library/DrmManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mFilePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$4000(Lcom/sec/android/mmapp/AudioPreview;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/library/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2870
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$MediaSessionCallback;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/AudioPreview;->finish()V

    .line 2872
    :cond_0
    return-void
.end method
