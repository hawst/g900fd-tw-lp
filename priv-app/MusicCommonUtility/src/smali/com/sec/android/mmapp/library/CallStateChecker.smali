.class public Lcom/sec/android/mmapp/library/CallStateChecker;
.super Ljava/lang/Object;
.source "CallStateChecker.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/mmapp/library/CallStateChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/library/CallStateChecker;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDualStanbyCallState(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    const/4 v0, 0x0

    .line 132
    .local v0, "callState":I
    const/4 v1, 0x0

    .line 133
    .local v1, "callState2":I
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 136
    .local v2, "tm1":Landroid/telephony/TelephonyManager;
    const/4 v3, 0x0

    .line 140
    .local v3, "tm2":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 142
    if-eqz v3, :cond_0

    .line 143
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    .line 145
    :cond_0
    if-eqz v0, :cond_1

    .line 148
    .end local v0    # "callState":I
    :goto_0
    return v0

    .restart local v0    # "callState":I
    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static getMultiSimCallState(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    const/4 v0, 0x0

    .line 118
    .local v0, "callState":I
    const/4 v1, 0x0

    .local v1, "simSlotNum":I
    :goto_0
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 119
    const-string v3, "phone"

    invoke-static {v3, v1}, Lcom/samsung/android/telephony/MultiSimManager;->appendSimSlot(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 122
    .local v2, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 127
    .end local v2    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_0
    return v0

    .line 118
    .restart local v2    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static isCallIdle(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-static {}, Lcom/sec/android/mmapp/library/CallStateChecker;->isVoipIdle()Z

    move-result v1

    .line 75
    .local v1, "isVoipIdle":Z
    invoke-static {p0}, Lcom/sec/android/mmapp/library/CallStateChecker;->isCommunicationIdle(Landroid/content/Context;)Z

    move-result v0

    .line 76
    .local v0, "isCommIdle":Z
    sget-object v2, Lcom/sec/android/mmapp/library/CallStateChecker;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isCallIdle() isVoipIdle : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isCommIdle(Such like google talk..) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isCommunicationIdle(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 108
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v1, 0x0

    .line 109
    .local v1, "mode":I
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 112
    :cond_0
    if-nez v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isRmsConnected(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 159
    const/4 v8, 0x0

    .line 160
    .local v8, "status":I
    const/4 v6, 0x0

    .line 162
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 163
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 164
    const-string v1, "content://com.lguplus.rms/service"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 166
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "connected"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 173
    :cond_0
    if-eqz v6, :cond_1

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 175
    const/4 v6, 0x0

    .line 178
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    if-ne v8, v9, :cond_3

    move v1, v9

    :goto_1
    return v1

    .line 170
    :catch_0
    move-exception v7

    .line 171
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "RMS"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    if-eqz v6, :cond_1

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 175
    const/4 v6, 0x0

    goto :goto_0

    .line 173
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 175
    const/4 v6, 0x0

    :cond_2
    throw v1

    .line 178
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static isUseSeeingCall(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const/4 v6, 0x0

    .line 189
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 190
    .local v9, "value":Ljava/lang/String;
    const/4 v8, 0x0

    .line 193
    .local v8, "ret":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 194
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 195
    const-string v1, "content://com.uplus.ipagent.SettingsProvider/system"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v10, "setting_pluscall_active"

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 202
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    :cond_0
    const-string v1, "value"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 205
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 211
    :cond_1
    if-eqz v6, :cond_2

    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 213
    const/4 v6, 0x0

    .line 217
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_2
    :goto_0
    if-eqz v9, :cond_4

    const-string v1, "1"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 218
    const/4 v8, 0x1

    .line 222
    :goto_1
    return v8

    .line 208
    :catch_0
    move-exception v7

    .line 209
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "UWA CALL Exception"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    if-eqz v6, :cond_2

    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 213
    const/4 v6, 0x0

    goto :goto_0

    .line 211
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 213
    const/4 v6, 0x0

    :cond_3
    throw v1

    .line 220
    :cond_4
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private static isVoipIdle()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    .line 95
    .local v0, "isVoipIdle":Z
    return v0
.end method
