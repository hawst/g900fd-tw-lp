.class public Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
.super Landroid/app/ListActivity;
.source "PickerQueryBrowserActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final EXTRA_START_SEARCH:Ljava/lang/String; = "startSearch"

.field public static final FROM_QUERY_BROWSER:Ljava/lang/String; = "fromQueryBrowser"

.field private static final MIME_TYPE_ALBUM:I = 0x2

.field private static final MIME_TYPE_ARTIST:I = 0x1

.field private static final MIME_TYPE_NOT_SUPPORT:I = -0x1

.field private static final MIME_TYPE_SONG:I = 0x3

.field private static sSavedMimeTypeIndex:Landroid/util/SparseIntArray;


# instance fields
.field private mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

.field private mAdapterSent:Z

.field private mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

.field private mFilterString:Ljava/lang/String;

.field private mIsKeyboardVisible:Z

.field private mKeyboardIntentFilter:Landroid/content/IntentFilter;

.field private mKeyboardReceiver:Landroid/content/BroadcastReceiver;

.field private mMainView:Landroid/view/View;

.field private mQueryCursor:Landroid/database/Cursor;

.field private mQueryText:Ljava/lang/String;

.field private final mReScanHandler:Landroid/os/Handler;

.field private final mScanListener:Landroid/content/BroadcastReceiver;

.field private mScrollState:I

.field private mSearchView:Landroid/widget/SearchView;

.field private mTrackList:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    .line 112
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mIsKeyboardVisible:Z

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScrollState:I

    .line 426
    new-instance v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$2;-><init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    .line 434
    new-instance v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$3;-><init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    .line 914
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mIsKeyboardVisible:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScrollState:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Lcom/sec/android/mmapp/util/AlbumArtLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    .param p1, "x1"    # Landroid/content/AsyncQueryHandler;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 74
    invoke-static {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-static {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getMimeTypeNum(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900()Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method private static getMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 562
    const-string v1, "mime_type"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 563
    .local v0, "mimeType":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 564
    const-string v0, "audio/"

    .line 566
    :cond_0
    return-object v0
.end method

.method private static getMimeTypeNum(Ljava/lang/String;)I
    .locals 1
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 577
    const-string v0, "artist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    const/4 v0, 0x1

    .line 584
    :goto_0
    return v0

    .line 579
    :cond_0
    const-string v0, "album"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 580
    const/4 v0, 0x2

    goto :goto_0

    .line 581
    :cond_1
    invoke-static {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->isTrackType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 582
    const/4 v0, 0x3

    goto :goto_0

    .line 584
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "async"    # Landroid/content/AsyncQueryHandler;
    .param p2, "filter"    # Ljava/lang/String;

    .prologue
    .line 490
    const/4 v3, 0x0

    .line 491
    .local v3, "search":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 492
    .local v4, "ccols":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 493
    .local v5, "selection":Ljava/lang/String;
    if-nez p2, :cond_0

    .line 494
    const-string p2, ""

    .line 497
    :cond_0
    const/4 v0, 0x7

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "ccols":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "mime_type"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "artist"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "album"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "title"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    const-string v1, "data1"

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "data2"

    aput-object v1, v4, v0

    .line 506
    .restart local v4    # "ccols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://media/external/audio/search/fancy/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 508
    const/4 v9, 0x0

    .line 509
    .local v9, "ret":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    .line 510
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    # setter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->access$402(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 511
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v6, v0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :goto_0
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v0, :cond_1

    .line 520
    if-eqz v9, :cond_3

    .line 521
    invoke-direct {p0, v9}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setMimeTypeIndex(Landroid/database/Cursor;)V

    .line 533
    :cond_1
    :goto_1
    return-object v9

    .line 515
    :cond_2
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v6, v0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_0

    .line 523
    :cond_3
    const/4 v8, 0x0

    .line 524
    .local v8, "mimeCursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v6, v0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 527
    invoke-direct {p0, v8}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setMimeTypeIndex(Landroid/database/Cursor;)V

    .line 528
    if-eqz v8, :cond_1

    .line 529
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public static isMediaScannerScanning(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 396
    const/4 v7, 0x0

    .line 397
    .local v7, "result":Z
    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "volume"

    aput-object v0, v2, v8

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 400
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 401
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 402
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 403
    const-string v0, "external"

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 405
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 408
    :cond_1
    return v7
.end method

.method private static isTrackType(Ljava/lang/String;)Z
    .locals 1
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 588
    const-string v0, "audio/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/ogg"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/x-ogg"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 392
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "limit"    # I

    .prologue
    const/4 v7, 0x0

    .line 373
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, " query"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 376
    .local v0, "resolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 377
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, " resolver is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v7

    .line 386
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :goto_0
    return-object v1

    .line 380
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    :cond_0
    if-lez p6, :cond_1

    .line 381
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 383
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 384
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :catch_0
    move-exception v6

    .line 385
    .local v6, "ex":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v6}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    move-object v1, v7

    .line 386
    goto :goto_0
.end method

.method private setKeyboardBroadcastReceiver()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    .line 320
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 324
    new-instance v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$1;-><init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 332
    return-void
.end method

.method private setMimeTypeIndex(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 537
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    sget-object v4, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->clear()V

    .line 541
    const/4 v2, -0x1

    .line 542
    .local v2, "position":I
    const-string v0, ""

    .line 543
    .local v0, "mimeType":Ljava/lang/String;
    const/4 v1, -0x1

    .line 544
    .local v1, "mimeTypeNum":I
    const/4 v3, -0x1

    .line 545
    .local v3, "savedMimeType":I
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 547
    :cond_2
    invoke-static {p1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 548
    invoke-static {v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getMimeTypeNum(Ljava/lang/String;)I

    move-result v1

    .line 549
    if-gez v1, :cond_4

    .line 557
    :cond_3
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 552
    :cond_4
    if-eq v1, v3, :cond_3

    .line 553
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 554
    sget-object v4, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 555
    move v3, v1

    goto :goto_1
.end method

.method public static setSpinnerState(Landroid/app/Activity;)V
    .locals 3
    .param p0, "a"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x5

    .line 412
    invoke-static {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->isMediaScannerScanning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x3

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureInt(II)V

    .line 417
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureInt(II)V

    .line 424
    :goto_0
    return-void

    .line 421
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x2

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureInt(II)V

    goto :goto_0
.end method


# virtual methods
.method public getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    return-object v0
.end method

.method public init(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    if-nez v0, :cond_1

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 450
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public initService()V
    .locals 17

    .prologue
    .line 172
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "initService()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    new-instance v15, Landroid/content/IntentFilter;

    invoke-direct {v15}, Landroid/content/IntentFilter;-><init>()V

    .line 174
    .local v15, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v15, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 175
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v15, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 176
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v15, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 177
    const-string v1, "file"

    invoke-virtual {v15, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v15}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 180
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v16

    .line 182
    .local v16, "intent":Landroid/content/Intent;
    if-nez v16, :cond_0

    .line 183
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "intent is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :goto_0
    return-void

    .line 187
    :cond_0
    const v1, 0x7f040011

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setContentView(I)V

    .line 189
    sget-boolean v1, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v1, :cond_1

    .line 194
    const v1, 0x7f0f0035

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mMainView:Landroid/view/View;

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 196
    const/16 v4, 0x35

    .line 197
    .local v4, "gravity":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 199
    .local v5, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 207
    .local v6, "height":I
    :goto_1
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setFinishOnTouchOutside(Z)V

    .line 208
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mMainView:Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-static/range {v1 .. v6}, Lcom/sec/android/mmapp/util/MusicListUtils;->updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V

    .line 212
    .end local v4    # "gravity":I
    .end local v5    # "width":I
    .end local v6    # "height":I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0034

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    .line 213
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 214
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 215
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 217
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 220
    const-string v1, "QuickCommand"

    const-string v2, "QuickSearch"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 221
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 224
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mTrackList:Landroid/widget/ListView;

    .line 226
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mTrackList:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mTrackList:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    if-nez v1, :cond_4

    .line 231
    new-instance v7, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    const v11, 0x7f040009

    const/4 v12, 0x0

    const/4 v1, 0x0

    new-array v13, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-array v14, v1, [I

    move-object/from16 v8, p0

    move-object/from16 v10, p0

    invoke-direct/range {v7 .. v14}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;-><init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Landroid/content/Context;Lcom/sec/android/mmapp/PickerQueryBrowserActivity;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    .line 233
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 234
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getQueryHandler()Landroid/content/AsyncQueryHandler;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;

    goto/16 :goto_0

    .line 202
    :cond_3
    const/16 v4, 0x35

    .line 203
    .restart local v4    # "gravity":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 205
    .restart local v5    # "width":I
    const/4 v6, -0x1

    .restart local v6    # "height":I
    goto/16 :goto_1

    .line 236
    .end local v4    # "gravity":I
    .end local v5    # "width":I
    .end local v6    # "height":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->setActivity(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    .line 239
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_5

    .line 240
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->init(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 242
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getQueryHandler()Landroid/content/AsyncQueryHandler;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x2

    .line 249
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 250
    iget v0, p1, Landroid/content/res/Configuration;->keyboard:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v1, :cond_0

    .line 253
    iget-boolean v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mIsKeyboardVisible:Z

    if-nez v0, :cond_0

    .line 254
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    .line 255
    .local v6, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->findFocus()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 259
    .end local v6    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_1

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 265
    const/16 v3, 0x35

    .line 266
    .local v3, "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 268
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 278
    .local v5, "height":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mmapp/util/MusicListUtils;->updateWindowLayout(Landroid/content/Context;Landroid/view/View;Landroid/view/Window;III)V

    .line 281
    .end local v3    # "gravity":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 282
    .local v7, "lp":Landroid/view/WindowManager$LayoutParams;
    sget-boolean v0, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v0, :cond_2

    .line 283
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v0, v0, 0x100

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 284
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 286
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 287
    return-void

    .line 272
    .end local v7    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_3
    const/16 v3, 0x35

    .line 273
    .restart local v3    # "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 275
    .restart local v4    # "width":I
    const/4 v5, -0x1

    .restart local v5    # "height":I
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 119
    sget-object v3, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 123
    .local v0, "bar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v3

    or-int/lit8 v3, v3, 0x2

    or-int/lit8 v3, v3, 0x4

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 125
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 126
    sget-boolean v3, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v3, :cond_0

    .line 127
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 129
    :cond_0
    sget-boolean v3, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-nez v3, :cond_2

    .line 133
    :goto_0
    const v3, 0x7f040010

    invoke-static {p0, v3, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 134
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 136
    iget-object v3, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    if-nez v3, :cond_3

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->initService()V

    .line 143
    :goto_1
    invoke-static {}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/sec/android/mmapp/util/AlbumArtLoader;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 145
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    sget-boolean v3, Lcom/sec/android/mmapp/ProductFeature;->IS_TABLET:Z

    if-eqz v3, :cond_1

    .line 146
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x100

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 147
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 151
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 153
    return-void

    .line 130
    .end local v1    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "v":Landroid/view/View;
    :cond_2
    const/16 v3, 0x14

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 131
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 139
    .restart local v2    # "v":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    iput-object v3, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 336
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapterSent:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 353
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 354
    invoke-virtual {p0, v3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 356
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    .line 357
    iput-object v3, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    .line 358
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 359
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 460
    const/4 v2, 0x0

    .line 461
    .local v2, "Title_OK":I
    const/4 v0, 0x1

    .line 462
    .local v0, "Album_OK":I
    const/4 v1, 0x2

    .line 463
    .local v1, "Artist_OK":I
    iget-object v5, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    invoke-interface {v5, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 464
    iget-object v5, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    const-string v7, "mime_type"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 469
    .local v4, "selectedType":Ljava/lang/String;
    const-string v5, "artist"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 470
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 471
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "artist"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 472
    invoke-virtual {p0, v1, v3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 473
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->finish()V

    goto :goto_0

    .line 474
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_2
    const-string v5, "album"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 475
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 476
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v5, "album"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 477
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 478
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->finish()V

    goto :goto_0

    .line 479
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_3
    if-ltz p3, :cond_4

    const-wide/16 v6, 0x0

    cmp-long v5, p4, v6

    if-ltz v5, :cond_4

    .line 480
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 481
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v5, "title"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 483
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->finish()V

    goto :goto_0

    .line 485
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v5, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "invalid position/id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 157
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v2, "onNewIntent()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 159
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 160
    .local v0, "action":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_0

    .line 161
    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 162
    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mFilterString:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    .line 169
    :cond_0
    :goto_1
    return-void

    .line 159
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 165
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->finish()V

    .line 166
    invoke-virtual {p0, p1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 941
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 946
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 943
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->finish()V

    .line 944
    const/4 v0, 0x1

    goto :goto_0

    .line 941
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 302
    sget-object v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 305
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 306
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 3
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 927
    sget-object v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryTextChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    iput-object p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    .line 930
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->init(Landroid/database/Cursor;)V

    .line 932
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 5
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 918
    sget-object v1, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onQueryTextSubmit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 921
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 922
    return v4
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->setKeyboardBroadcastReceiver()V

    .line 311
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 312
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryText:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->init(Landroid/database/Cursor;)V

    .line 314
    :cond_0
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 315
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapterSent:Z

    .line 297
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAdapter:Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 957
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 961
    iget v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScrollState:I

    if-eq v0, p2, :cond_0

    .line 962
    iput p2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScrollState:I

    .line 964
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x1

    return v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 952
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 292
    return-void
.end method
