.class public Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;
.super Ljava/lang/Object;
.source "MusicListUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/util/MusicListUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QueryArgs"
.end annotation


# instance fields
.field public albumArtCol:Ljava/lang/String;

.field public albumIdCol:Ljava/lang/String;

.field public audioIdCol:Ljava/lang/String;

.field public durationCol:Ljava/lang/String;

.field public indexBy:Ljava/lang/String;

.field public keyWord:Ljava/lang/String;

.field public orderBy:Ljava/lang/String;

.field public projection:[Ljava/lang/String;

.field public selection:Ljava/lang/String;

.field public selectionArgs:[Ljava/lang/String;

.field public text1Col:Ljava/lang/String;

.field public text2Col:Ljava/lang/String;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->keyWord:Ljava/lang/String;

    .line 302
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    .line 307
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->durationCol:Ljava/lang/String;

    .line 312
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    .line 317
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    .line 323
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->albumIdCol:Ljava/lang/String;

    .line 329
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/mmapp/util/MusicListUtils$QueryArgs;->albumArtCol:Ljava/lang/String;

    return-void
.end method
