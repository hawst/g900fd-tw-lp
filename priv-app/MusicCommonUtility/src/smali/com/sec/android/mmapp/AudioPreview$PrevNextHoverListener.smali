.class Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrevNextHoverListener"
.end annotation


# static fields
.field public static final NEXT:I = 0x1

.field public static final PREV:I


# instance fields
.field private final mHoverContentTV:Landroid/widget/TextView;

.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method public constructor <init>(Lcom/sec/android/mmapp/AudioPreview;ILandroid/widget/HoverPopupWindow;)V
    .locals 3
    .param p2, "type"    # I
    .param p3, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 3361
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3362
    iput p2, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->mType:I

    .line 3363
    invoke-virtual {p1}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040005

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->mHoverContentTV:Landroid/widget/TextView;

    .line 3365
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->mHoverContentTV:Landroid/widget/TextView;

    invoke-virtual {p3, v0}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 3366
    return-void
.end method

.method private getSongTitle(I)Ljava/lang/String;
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v3, 0x0

    .line 3447
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v1

    .line 3448
    .local v1, "targetPostion":I
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v4

    if-nez v4, :cond_1

    .line 3473
    :cond_0
    :goto_0
    return-object v3

    .line 3452
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 3460
    const/4 v1, -0x1

    .line 3464
    :goto_1
    if-ltz v1, :cond_0

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v4

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 3465
    sget-object v4, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v5}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v5

    aget-wide v6, v5, v1

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 3467
    .local v2, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getSongInfo(Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    invoke-static {v4, v2}, Lcom/sec/android/mmapp/AudioPreview;->access$7600(Lcom/sec/android/mmapp/AudioPreview;Landroid/net/Uri;)Lcom/sec/android/mmapp/AudioPreview$SongInfo;

    move-result-object v0

    .line 3468
    .local v0, "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    if-eqz v0, :cond_0

    .line 3469
    # getter for: Lcom/sec/android/mmapp/AudioPreview$SongInfo;->title:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$SongInfo;->access$800(Lcom/sec/android/mmapp/AudioPreview$SongInfo;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 3454
    .end local v0    # "songInfo":Lcom/sec/android/mmapp/AudioPreview$SongInfo;
    .end local v2    # "uri":Landroid/net/Uri;
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v1, v4, -0x1

    .line 3455
    :goto_2
    goto :goto_1

    .line 3454
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    goto :goto_2

    .line 3457
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mListPosition:I
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$7500(Lcom/sec/android/mmapp/AudioPreview;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v5}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v5

    array-length v5, v5

    rem-int v1, v4, v5

    .line 3458
    goto :goto_1

    .line 3452
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setHapticEffect(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 3444
    return-void
.end method

.method private setHoverFocusImage(Landroid/view/View;I)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # I

    .prologue
    const/16 v4, 0xa

    const/16 v3, 0x9

    const/4 v2, 0x1

    .line 3391
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/AudioPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 3392
    .local v0, "mContext":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3432
    .end local p1    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 3394
    .restart local p1    # "v":Landroid/view/View;
    :pswitch_1
    if-ne p2, v3, :cond_2

    .line 3395
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    array-length v1, v1

    if-le v1, v2, :cond_1

    move-object v1, p1

    .line 3396
    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020028

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3402
    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->setHapticEffect(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_1
    move-object v1, p1

    .line 3399
    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f02002a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 3403
    :cond_2
    if-ne p2, v4, :cond_0

    .line 3404
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    array-length v1, v1

    if-le v1, v2, :cond_3

    .line 3405
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v1, 0x7f020047

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 3407
    .restart local p1    # "v":Landroid/view/View;
    :cond_3
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v1, 0x7f020048

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 3412
    .restart local p1    # "v":Landroid/view/View;
    :pswitch_2
    if-ne p2, v3, :cond_5

    .line 3413
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    array-length v1, v1

    if-le v1, v2, :cond_4

    move-object v1, p1

    .line 3414
    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f02001a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3420
    :goto_2
    invoke-direct {p0, v0, p1}, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->setHapticEffect(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_4
    move-object v1, p1

    .line 3417
    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f02001c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 3421
    :cond_5
    if-ne p2, v4, :cond_0

    .line 3422
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mList:[J
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7400(Lcom/sec/android/mmapp/AudioPreview;)[J

    move-result-object v1

    array-length v1, v1

    if-le v1, v2, :cond_6

    .line 3423
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v1, 0x7f020003

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 3425
    .restart local p1    # "v":Landroid/view/View;
    :cond_6
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v1, 0x7f020004

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 3392
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0006
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 3370
    iget v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->mType:I

    invoke-direct {p0, v1}, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->getSongTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 3372
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 3373
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->mHoverContentTV:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3384
    :cond_0
    :goto_0
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 3385
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->setHoverFocusImage(Landroid/view/View;I)V

    .line 3387
    :cond_1
    return v3

    .line 3375
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mmapp/AudioPreview;->access$7300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3376
    iget-object v1, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->mHoverContentTV:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$PrevNextHoverListener;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mTitleText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$7300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
