.class public Lcom/sec/android/mmapp/util/AlbumArtLoader;
.super Ljava/lang/Object;
.source "AlbumArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;,
        Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DECODE_COMPELETE:I = 0x0

.field public static final LIST:I = 0x1

.field private static final THREAD_NAME:Ljava/lang/String; = "AlbumArtLoader"

.field private static volatile sAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

.field private static final sListAlbumArtHandler:Landroid/os/Handler;


# instance fields
.field private mAlbumArtHandler:Landroid/os/Handler;

.field private mThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->CLASSNAME:Ljava/lang/String;

    .line 73
    new-instance v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/mmapp/util/AlbumArtLoader$1;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sListAlbumArtHandler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->CLASSNAME:Ljava/lang/String;

    const-string v1, "AlbumArtLoader start creating"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AlbumArtLoader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    .line 129
    iget-object v0, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 130
    new-instance v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$2;

    iget-object v1, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mmapp/util/AlbumArtLoader$2;-><init>(Lcom/sec/android/mmapp/util/AlbumArtLoader;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mAlbumArtHandler:Landroid/os/Handler;

    .line 156
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->CLASSNAME:Ljava/lang/String;

    const-string v1, "AlbumArtLoader end creating"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method static synthetic access$000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sListAlbumArtHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getAlbumArtLoader()Lcom/sec/android/mmapp/util/AlbumArtLoader;
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->CLASSNAME:Ljava/lang/String;

    const-string v1, "AlbumArtLoader start getAlbumArtLoader"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    if-nez v0, :cond_1

    .line 109
    const-class v1, Lcom/sec/android/mmapp/util/AlbumArtLoader;

    monitor-enter v1

    .line 110
    :try_start_0
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;

    invoke-direct {v0}, Lcom/sec/android/mmapp/util/AlbumArtLoader;-><init>()V

    sput-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    .line 113
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_1
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->CLASSNAME:Ljava/lang/String;

    const-string v1, "AlbumArtLoader end getAlbumArtLoader"

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;

    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getArtwork(ILcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;)V
    .locals 2
    .param p1, "message"    # I
    .param p2, "info"    # Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mAlbumArtHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mAlbumArtHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 173
    return-void
.end method

.method public quit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 161
    iget-object v0, p0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->mAlbumArtHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 162
    sget-object v0, Lcom/sec/android/mmapp/util/AlbumArtLoader;->sListAlbumArtHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 123
    invoke-static {}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->clearAlbumArtCache()V

    .line 124
    return-void
.end method
