.class Lcom/sec/android/mmapp/AudioPreview$18;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mmapp/AudioPreview;->prepareRemoteControlClient()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 2736
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$18;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetPlaybackPosition()J
    .locals 2

    .prologue
    .line 2739
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$18;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2740
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$18;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v0

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v0

    int-to-long v0, v0

    .line 2742
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
