.class Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "PickerQueryBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;
    }
.end annotation


# instance fields
.field private mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

.field private mConstraint:Ljava/lang/String;

.field private mConstraintIsValid:Z

.field private final mQueryHandler:Landroid/content/AsyncQueryHandler;

.field final synthetic this$0:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Landroid/content/Context;Lcom/sec/android/mmapp/PickerQueryBrowserActivity;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "currentactivity"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    .param p4, "layout"    # I
    .param p5, "cursor"    # Landroid/database/Cursor;
    .param p6, "from"    # [Ljava/lang/String;
    .param p7, "to"    # [I

    .prologue
    const/4 v6, 0x0

    .line 617
    iput-object p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->this$0:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    .line 618
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    .line 593
    iput-object v6, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .line 597
    iput-object v6, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;

    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraintIsValid:Z

    .line 619
    iput-object p3, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .line 620
    new-instance v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$QueryHandler;-><init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    .line 621
    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 592
    iput-object p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)Lcom/sec/android/mmapp/PickerQueryBrowserActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    return-object v0
.end method

.method private getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "paint"    # Landroid/text/TextPaint;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 876
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 885
    :cond_0
    :goto_0
    return-object v1

    .line 879
    :cond_1
    const/4 v0, 0x0

    .line 880
    .local v0, "prefixForIndian":[C
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-static {p1, p2, v2}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v0

    .line 882
    if-eqz v0, :cond_0

    .line 885
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method private makeAlbumsSongsLabel(Landroid/content/Context;IIZ)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "numalbums"    # I
    .param p3, "numsongs"    # I
    .param p4, "isUnknown"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 639
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 640
    .local v0, "builder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v2, v0, v5}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 642
    .local v2, "formatter":Ljava/util/Formatter;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 643
    .local v4, "songs_albums":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 644
    .local v3, "r":Landroid/content/res/Resources;
    if-nez p4, :cond_0

    .line 645
    const/high16 v5, 0x7f0b0000

    invoke-virtual {v3, v5, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 646
    .local v1, "f":Ljava/lang/String;
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 647
    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v1, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 648
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 649
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    .end local v1    # "f":Ljava/lang/String;
    :cond_0
    const v5, 0x7f0b0009

    invoke-virtual {v3, v5, p3}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 652
    .restart local v1    # "f":Ljava/lang/String;
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 653
    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v1, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 654
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 655
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    .line 656
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private setArtWork(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ImageView;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "iv"    # Landroid/widget/ImageView;

    .prologue
    .line 846
    const-wide/16 v2, -0x1

    .line 847
    .local v2, "albumId":J
    const-string v7, "album_id"

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 848
    .local v1, "albumIdx":I
    if-ltz v1, :cond_0

    .line 849
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 852
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 853
    .local v6, "position":I
    new-instance v4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;

    invoke-direct {v4}, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;-><init>()V

    .line 854
    .local v4, "args":Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;
    iput v6, v4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->position:I

    .line 855
    iput-wide v2, v4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    .line 856
    invoke-virtual {p3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 859
    iget-wide v8, v4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    invoke-static {v8, v9}, Lcom/sec/android/mmapp/util/AlbumArtUtils;->getCachedArtworkWithoutMaking(J)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 860
    .local v5, "d":Landroid/graphics/drawable/Drawable;
    if-nez v5, :cond_1

    .line 861
    const/4 v7, 0x0

    invoke-virtual {p3, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 862
    new-instance v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;

    invoke-direct {v0}, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;-><init>()V

    .line 863
    .local v0, "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    iput-object p3, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->iv:Landroid/widget/ImageView;

    .line 864
    iget-wide v8, v4, Lcom/sec/android/mmapp/util/AlbumArtLoader$TagArgs;->albumId:J

    iput-wide v8, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->albumId:J

    .line 865
    iput-object p1, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->context:Landroid/content/Context;

    .line 866
    iget-object v7, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->this$0:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mScrollState:I
    invoke-static {v7}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$1000(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)I

    move-result v7

    iput v7, v0, Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;->scrollState:I

    .line 868
    iget-object v7, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->this$0:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mAlbumArtLoader:Lcom/sec/android/mmapp/util/AlbumArtLoader;
    invoke-static {v7}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$1100(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Lcom/sec/android/mmapp/util/AlbumArtLoader;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v0}, Lcom/sec/android/mmapp/util/AlbumArtLoader;->getArtwork(ILcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;)V

    .line 872
    .end local v0    # "ai":Lcom/sec/android/mmapp/util/AlbumArtLoader$AlbumArtInfo;
    :goto_0
    return-void

    .line 870
    :cond_1
    invoke-virtual {p3, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setArtWork(Landroid/widget/ImageView;I)V
    .locals 1
    .param p1, "iv"    # Landroid/widget/ImageView;
    .param p2, "mimeTypeNum"    # I

    .prologue
    const v0, 0x7f02003f

    .line 821
    packed-switch p2, :pswitch_data_0

    .line 832
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 835
    :goto_0
    return-void

    .line 823
    :pswitch_0
    const v0, 0x7f02003e

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 826
    :pswitch_1
    const v0, 0x7f02003d

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 829
    :pswitch_2
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 821
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 25
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 662
    const v22, 0x7f0f0017

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Lcom/sec/android/mmapp/widget/MatchedTextView;

    .line 663
    .local v19, "tv1":Lcom/sec/android/mmapp/widget/MatchedTextView;
    const v22, 0x7f0f0023

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/sec/android/mmapp/widget/MatchedTextView;

    .line 665
    .local v20, "tv2":Lcom/sec/android/mmapp/widget/MatchedTextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryText()Ljava/lang/String;

    move-result-object v13

    .line 666
    .local v13, "queryText":Ljava/lang/String;
    # invokes: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$700(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 667
    .local v8, "mimetype":Ljava/lang/String;
    const/4 v12, 0x0

    .line 669
    .local v12, "prefixForIndian":Ljava/lang/String;
    if-nez v8, :cond_0

    .line 670
    const-string v8, "audio/"

    .line 672
    :cond_0
    # invokes: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getMimeTypeNum(Ljava/lang/String;)I
    invoke-static {v8}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$800(Ljava/lang/String;)I

    move-result v7

    .line 673
    .local v7, "mimeTypeNum":I
    const v22, 0x7f0f0020

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 674
    .local v6, "iv":Landroid/widget/ImageView;
    sget-boolean v22, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v22, :cond_7

    .line 675
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->setArtWork(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ImageView;)V

    .line 680
    :goto_0
    const-string v22, "artist"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 681
    const-string v22, "artist"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 683
    .local v9, "name":Ljava/lang/String;
    move-object v3, v9

    .line 684
    .local v3, "displayname":Ljava/lang/String;
    const/4 v5, 0x0

    .line 685
    .local v5, "isunknown":Z
    if-eqz v9, :cond_1

    const-string v22, "<unknown>"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 686
    :cond_1
    const v22, 0x7f0c0002

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 687
    const/4 v5, 0x1

    .line 690
    :cond_2
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mmapp/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3, v13}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 691
    if-nez v12, :cond_8

    .line 692
    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v13}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 697
    :goto_1
    const-string v22, "data1"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 698
    .local v10, "numalbums":I
    const-string v22, "data2"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 700
    .local v11, "numsongs":I
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v10, v11, v5}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->makeAlbumsSongsLabel(Landroid/content/Context;IIZ)Ljava/lang/String;

    move-result-object v15

    .line 702
    .local v15, "songs_albums":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 764
    .end local v3    # "displayname":Ljava/lang/String;
    .end local v5    # "isunknown":Z
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "numalbums":I
    .end local v11    # "numsongs":I
    .end local v15    # "songs_albums":Ljava/lang/String;
    :cond_3
    :goto_2
    sget-boolean v22, Lcom/sec/android/mmapp/ProductFeature;->SUPPORT_LATEST_PHONE_UI:Z

    if-eqz v22, :cond_6

    .line 765
    const/4 v14, -0x1

    .line 766
    .local v14, "savedMimeTypeNum":I
    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;
    invoke-static {}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$900()Landroid/util/SparseIntArray;

    move-result-object v22

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/util/SparseIntArray;->get(I)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 767
    .local v4, "index":Ljava/lang/Integer;
    if-eqz v4, :cond_4

    .line 769
    check-cast v4, Ljava/lang/Integer;

    .end local v4    # "index":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 772
    :cond_4
    const v22, 0x7f0f0018

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    .line 774
    .local v17, "titleLayout":Landroid/view/View;
    new-instance v22, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter$1;-><init>(Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 781
    const v22, 0x7f0f001a

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 782
    .local v18, "titleText":Landroid/widget/TextView;
    const v22, 0x7f0f0019

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 784
    .local v16, "titleDivider":Landroid/view/View;
    if-ne v14, v7, :cond_18

    .line 785
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 786
    packed-switch v7, :pswitch_data_0

    .line 806
    :goto_3
    :pswitch_0
    const/16 v21, 0x0

    .line 807
    .local v21, "visibility":I
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v22

    if-nez v22, :cond_5

    .line 808
    const/16 v21, 0x8

    .line 810
    :cond_5
    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 812
    .end local v14    # "savedMimeTypeNum":I
    .end local v16    # "titleDivider":Landroid/view/View;
    .end local v17    # "titleLayout":Landroid/view/View;
    .end local v18    # "titleText":Landroid/widget/TextView;
    .end local v21    # "visibility":I
    :cond_6
    return-void

    .line 677
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->setArtWork(Landroid/widget/ImageView;I)V

    goto/16 :goto_0

    .line 694
    .restart local v3    # "displayname":Ljava/lang/String;
    .restart local v5    # "isunknown":Z
    .restart local v9    # "name":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v12}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 704
    .end local v3    # "displayname":Ljava/lang/String;
    .end local v5    # "isunknown":Z
    .end local v9    # "name":Ljava/lang/String;
    :cond_9
    const-string v22, "album"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 705
    const-string v22, "album"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 707
    .restart local v9    # "name":Ljava/lang/String;
    move-object v3, v9

    .line 708
    .restart local v3    # "displayname":Ljava/lang/String;
    if-eqz v9, :cond_a

    const-string v22, "<unknown>"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 709
    :cond_a
    const v22, 0x7f0c0001

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 712
    :cond_b
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mmapp/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3, v13}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 713
    if-nez v12, :cond_e

    .line 714
    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v13}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 719
    :goto_4
    const-string v22, "artist"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 721
    move-object v3, v9

    .line 722
    if-eqz v9, :cond_c

    const-string v22, "<unknown>"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 723
    :cond_c
    const v22, 0x7f0c0002

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 726
    :cond_d
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mmapp/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3, v13}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 727
    if-nez v12, :cond_f

    .line 728
    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v13}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 716
    :cond_e
    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v12}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto :goto_4

    .line 730
    :cond_f
    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v12}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 733
    .end local v3    # "displayname":Ljava/lang/String;
    .end local v9    # "name":Ljava/lang/String;
    :cond_10
    const-string v22, "audio/"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_11

    const-string v22, "application/ogg"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_11

    const-string v22, "application/x-ogg"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 735
    :cond_11
    const-string v22, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 738
    .restart local v9    # "name":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mmapp/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v9, v13}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 739
    if-nez v12, :cond_16

    .line 740
    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v13}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 745
    :goto_5
    const-string v22, "artist"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 747
    .restart local v3    # "displayname":Ljava/lang/String;
    if-eqz v3, :cond_12

    const-string v22, "<unknown>"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_13

    .line 748
    :cond_12
    const v22, 0x7f0c0002

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 750
    :cond_13
    const-string v22, "album"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 752
    if-eqz v9, :cond_14

    const-string v22, "<unknown>"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_15

    .line 753
    :cond_14
    const v22, 0x7f0c0001

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 756
    :cond_15
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mmapp/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2, v13}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 758
    if-nez v12, :cond_17

    .line 759
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v13}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 742
    .end local v3    # "displayname":Ljava/lang/String;
    :cond_16
    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v12}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 761
    .restart local v3    # "displayname":Ljava/lang/String;
    :cond_17
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v12}, Lcom/sec/android/mmapp/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 788
    .end local v3    # "displayname":Ljava/lang/String;
    .end local v9    # "name":Ljava/lang/String;
    .restart local v14    # "savedMimeTypeNum":I
    .restart local v16    # "titleDivider":Landroid/view/View;
    .restart local v17    # "titleLayout":Landroid/view/View;
    .restart local v18    # "titleText":Landroid/widget/TextView;
    :pswitch_1
    const v22, 0x7f0c000c

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 791
    :pswitch_2
    const v22, 0x7f0c0009

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 794
    :pswitch_3
    const v22, 0x7f0c0048

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 797
    :pswitch_4
    const/16 v22, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 803
    :cond_18
    const/16 v22, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 786
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 890
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 891
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 892
    const/4 p1, 0x0

    .line 894
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    # getter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$1200(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)Landroid/database/Cursor;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 895
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    # setter for: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->mQueryCursor:Landroid/database/Cursor;
    invoke-static {v0, p1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$1202(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 896
    invoke-super {p0, p1}, Landroid/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 898
    :cond_1
    return-void
.end method

.method public getQueryHandler()Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 4
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 902
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 903
    .local v1, "s":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraintIsValid:Z

    if-eqz v2, :cond_2

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;

    if-eqz v2, :cond_1

    :cond_0
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 905
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 910
    :goto_0
    return-object v0

    .line 907
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->getQueryCursor(Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;
    invoke-static {v2, v3, v1}, Lcom/sec/android/mmapp/PickerQueryBrowserActivity;->access$300(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;Landroid/content/AsyncQueryHandler;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 908
    .local v0, "c":Landroid/database/Cursor;
    iput-object v1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraint:Ljava/lang/String;

    .line 909
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mConstraintIsValid:Z

    goto :goto_0
.end method

.method public setActivity(Lcom/sec/android/mmapp/PickerQueryBrowserActivity;)V
    .locals 0
    .param p1, "newactivity"    # Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .prologue
    .line 624
    iput-object p1, p0, Lcom/sec/android/mmapp/PickerQueryBrowserActivity$QueryListAdapter;->mActivity:Lcom/sec/android/mmapp/PickerQueryBrowserActivity;

    .line 625
    return-void
.end method
