.class Lcom/sec/android/mmapp/AudioPreview$3;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/AudioPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/AudioPreview;)V
    .locals 0

    .prologue
    .line 809
    iput-object p1, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 812
    packed-switch p2, :pswitch_data_0

    .line 872
    :goto_0
    return v0

    .line 814
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :cond_0
    :goto_1
    move v0, v1

    .line 872
    goto :goto_0

    .line 816
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 817
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/mmapp/AudioPreview;->access$2702(Lcom/sec/android/mmapp/AudioPreview;J)J

    .line 818
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rew down"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 820
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto :goto_1

    .line 824
    :pswitch_2
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rew up"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2700(Lcom/sec/android/mmapp/AudioPreview;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-gez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 827
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->canPlayState()Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$1900(Lcom/sec/android/mmapp/AudioPreview$Player;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->getCurrentPosition()I
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2000(Lcom/sec/android/mmapp/AudioPreview$Player;)I

    move-result v2

    const/16 v3, 0xbb8

    if-le v2, v3, :cond_2

    .line 829
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mPlayer:Lcom/sec/android/mmapp/AudioPreview$Player;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1600(Lcom/sec/android/mmapp/AudioPreview;)Lcom/sec/android/mmapp/AudioPreview$Player;

    move-result-object v2

    # invokes: Lcom/sec/android/mmapp/AudioPreview$Player;->seekTo(I)V
    invoke-static {v2, v0}, Lcom/sec/android/mmapp/AudioPreview$Player;->access$2100(Lcom/sec/android/mmapp/AudioPreview$Player;I)V

    .line 830
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getTimeString(I)Ljava/lang/String;
    invoke-static {v3, v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2200(Lcom/sec/android/mmapp/AudioPreview;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 831
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mProgressText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2300(Lcom/sec/android/mmapp/AudioPreview;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->getDurationTalkback(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2400(Lcom/sec/android/mmapp/AudioPreview;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 838
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v2, v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_1

    .line 835
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playPreviousSong()V
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2500(Lcom/sec/android/mmapp/AudioPreview;)V

    goto :goto_2

    .line 846
    :pswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_2

    goto/16 :goto_1

    .line 848
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/mmapp/AudioPreview;->access$2702(Lcom/sec/android/mmapp/AudioPreview;J)J

    .line 850
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ff down"

    invoke-static {v0, v2}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mSeekHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mmapp/AudioPreview;->access$2900(Lcom/sec/android/mmapp/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 852
    iget-object v0, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v0, v1}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_1

    .line 856
    :pswitch_5
    # getter for: Lcom/sec/android/mmapp/AudioPreview;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/AudioPreview;->access$2800()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ff up"

    invoke-static {v2, v3}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->pressDownTime:J
    invoke-static {v4}, Lcom/sec/android/mmapp/AudioPreview;->access$2700(Lcom/sec/android/mmapp/AudioPreview;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-gez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # getter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$1400(Lcom/sec/android/mmapp/AudioPreview;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 859
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # invokes: Lcom/sec/android/mmapp/AudioPreview;->playNextSong()V
    invoke-static {v2}, Lcom/sec/android/mmapp/AudioPreview;->access$2600(Lcom/sec/android/mmapp/AudioPreview;)V

    .line 861
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mmapp/AudioPreview$3;->this$0:Lcom/sec/android/mmapp/AudioPreview;

    # setter for: Lcom/sec/android/mmapp/AudioPreview;->mIsPressedBtn:Z
    invoke-static {v2, v0}, Lcom/sec/android/mmapp/AudioPreview;->access$1402(Lcom/sec/android/mmapp/AudioPreview;Z)Z

    goto/16 :goto_1

    .line 812
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 814
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 846
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
