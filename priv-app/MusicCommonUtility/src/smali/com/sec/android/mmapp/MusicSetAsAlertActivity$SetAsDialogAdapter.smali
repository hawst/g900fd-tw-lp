.class public Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "MusicSetAsAlertActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/MusicSetAsAlertActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SetAsDialogAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    # getter for: Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->access$000(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 167
    iget-object v1, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    iget-object v0, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    # getter for: Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->access$000(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Ljava/util/ArrayList;

    move-result-object v0

    mul-int/lit8 v2, p1, 0x2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 172
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 178
    if-nez p2, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    # getter for: Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->access$100(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040004

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 180
    new-instance v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;

    invoke-direct {v0}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;-><init>()V

    .line 181
    .local v0, "vh":Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;
    const v1, 0x7f0f0011

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 182
    const v1, 0x7f0f0012

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;->name:Landroid/widget/TextView;

    .line 183
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 187
    :goto_0
    iget-object v2, v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    # getter for: Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->access$000(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Ljava/util/ArrayList;

    move-result-object v1

    mul-int/lit8 v3, p1, 0x2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 188
    iget-object v2, v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    invoke-virtual {v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$SetAsDialogAdapter;->this$0:Lcom/sec/android/mmapp/MusicSetAsAlertActivity;

    # getter for: Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mmapp/MusicSetAsAlertActivity;->access$000(Lcom/sec/android/mmapp/MusicSetAsAlertActivity;)Ljava/util/ArrayList;

    move-result-object v1

    mul-int/lit8 v4, p1, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 189
    return-object p2

    .line 185
    .end local v0    # "vh":Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;

    .restart local v0    # "vh":Lcom/sec/android/mmapp/MusicSetAsAlertActivity$ViewHolder;
    goto :goto_0
.end method
