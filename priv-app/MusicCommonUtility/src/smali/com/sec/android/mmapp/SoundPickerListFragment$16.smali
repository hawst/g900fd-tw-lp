.class Lcom/sec/android/mmapp/SoundPickerListFragment$16;
.super Ljava/lang/Object;
.source "SoundPickerListFragment.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mmapp/SoundPickerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/mmapp/SoundPickerListFragment;)V
    .locals 0

    .prologue
    .line 1448
    iput-object p1, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 1453
    packed-switch p1, :pswitch_data_0

    .line 1466
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1468
    :cond_0
    :goto_0
    return-void

    .line 1457
    :pswitch_0
    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioFocus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mmapp/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$300(Lcom/sec/android/mmapp/SoundPickerListFragment;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->stopMediaPlayer()V

    .line 1460
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    # getter for: Lcom/sec/android/mmapp/SoundPickerListFragment;->mIsPause:Z
    invoke-static {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->access$2000(Lcom/sec/android/mmapp/SoundPickerListFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1461
    iget-object v0, p0, Lcom/sec/android/mmapp/SoundPickerListFragment$16;->this$0:Lcom/sec/android/mmapp/SoundPickerListFragment;

    invoke-virtual {v0}, Lcom/sec/android/mmapp/SoundPickerListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0

    .line 1453
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
