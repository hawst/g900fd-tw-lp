.class public LDigiCAP/LGT/DRM/DRMInterface;
.super Ljava/lang/Object;
.source "DRMInterface.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native LDRMCleanupROStorage()I
.end method

.method public native LDRMDeleteROByFile(Ljava/lang/String;S)I
.end method

.method public native LDRMDestroyDRMLib()V
.end method

.method public native LDRMGetContentID(Ljava/lang/String;Ljava/lang/StringBuffer;)I
.end method

.method public native LDRMGetDeviceInfo(Ljava/lang/StringBuffer;Ljava/lang/String;)I
.end method

.method public native LDRMGetExpireDate(Ljava/lang/String;Ljava/lang/StringBuffer;)I
.end method

.method public native LDRMGetTextualHeaderAll(Ljava/lang/String;Ljava/lang/StringBuffer;)I
.end method

.method public native LDRMHandleROAP(Ljava/lang/String;)I
.end method

.method public native LDRMInitDRMLib()I
.end method

.method public native LDRMRepairCertificateFile()I
.end method

.method public loadLibrary()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "drminterface"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 40
    return-void
.end method
