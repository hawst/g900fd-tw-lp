.class Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;
.super Ljava/lang/Object;
.source "SpassFingerprint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->notifyDialogDismiss()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

.field private final synthetic val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

.field private final synthetic val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;Lcom/samsung/android/fingerprint/FingerprintEvent;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    iput-object p2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iput-object p3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 464
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iget v0, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 465
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getFingerIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)V

    .line 466
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iget v2, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    # invokes: Lcom/samsung/android/sdk/pass/SpassFingerprint;->convertStatusValues(I)I
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;->onFinished(I)V

    .line 467
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)V

    .line 468
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$8(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$9(Lcom/samsung/android/sdk/pass/SpassFingerprint;Landroid/app/Dialog;)V

    .line 472
    :cond_0
    return-void
.end method
