.class public Lcom/samsung/android/sdk/pass/SpassInvalidStateException;
.super Ljava/lang/IllegalStateException;
.source "SpassInvalidStateException.java"


# static fields
.field public static final STATUS_OPERATION_DENIED:I = 0x1

.field private static final serialVersionUID:J = 0x36acb9bf8ee3c80dL


# instance fields
.field private mErrorType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "errorType"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pass/SpassInvalidStateException;->mErrorType:I

    .line 34
    iput p2, p0, Lcom/samsung/android/sdk/pass/SpassInvalidStateException;->mErrorType:I

    .line 35
    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/sdk/pass/SpassInvalidStateException;->mErrorType:I

    return v0
.end method
