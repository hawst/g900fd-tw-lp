.class public Lcom/samsung/android/sdk/pass/support/SdkSupporter;
.super Ljava/lang/Object;
.source "SdkSupporter.java"


# static fields
.field public static final CLASSNAME_FINGERPRINT_CLIENT:Ljava/lang/String; = "com.samsung.android.fingerprint.IFingerprintClient$Stub"

.field public static final CLASSNAME_FINGERPRINT_CLIENT_SPEC_BUILDER:Ljava/lang/String; = "com.samsung.android.fingerprint.FingerprintManager$FingerprintClientSpecBuilder"

.field public static final CLASSNAME_FINGERPRINT_MANAGER:Ljava/lang/String; = "com.samsung.android.fingerprint.FingerprintManager"

.field private static final TAG:Ljava/lang/String; = "SdkSupporter"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyStaticFields(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p0, "targetObject"    # Ljava/lang/Object;
    .param p2, "className"    # Ljava/lang/String;
    .param p3, "fieldPrefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "dest":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v8, 0x0

    .line 23
    :try_start_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 25
    .local v4, "srcCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    .line 27
    .local v7, "srcFields":[Ljava/lang/reflect/Field;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 29
    .local v6, "srcFieldMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    array-length v10, v7

    move v9, v8

    :goto_0
    if-lt v9, v10, :cond_0

    .line 33
    invoke-virtual {p1}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    .line 35
    .local v0, "destFields":[Ljava/lang/reflect/Field;
    array-length v9, v0

    :goto_1
    if-lt v8, v9, :cond_1

    .line 56
    .end local v0    # "destFields":[Ljava/lang/reflect/Field;
    .end local v4    # "srcCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v6    # "srcFieldMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    .end local v7    # "srcFields":[Ljava/lang/reflect/Field;
    :goto_2
    const/4 v8, 0x1

    return v8

    .line 29
    .restart local v4    # "srcCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v6    # "srcFieldMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    .restart local v7    # "srcFields":[Ljava/lang/reflect/Field;
    :cond_0
    aget-object v2, v7, v9

    .line 30
    .local v2, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 35
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .restart local v0    # "destFields":[Ljava/lang/reflect/Field;
    :cond_1
    aget-object v2, v0, v8

    .line 36
    .restart local v2    # "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "fieldName":Ljava/lang/String;
    if-eqz p3, :cond_3

    invoke-virtual {v3, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 35
    :cond_2
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 42
    :cond_3
    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/reflect/Field;

    .line 44
    .local v5, "srcField":Ljava/lang/reflect/Field;
    if-eqz v5, :cond_2

    .line 48
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 49
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v2, p0, v10}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 52
    .end local v0    # "destFields":[Ljava/lang/reflect/Field;
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v3    # "fieldName":Ljava/lang/String;
    .end local v4    # "srcCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "srcField":Ljava/lang/reflect/Field;
    .end local v6    # "srcFieldMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Field;>;"
    .end local v7    # "srcFields":[Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 53
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "SdkSupporter"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "copyFields: failed - "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
