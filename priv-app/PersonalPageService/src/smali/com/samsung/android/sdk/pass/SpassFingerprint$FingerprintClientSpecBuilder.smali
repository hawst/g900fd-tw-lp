.class Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;
.super Ljava/lang/Object;
.source "SpassFingerprint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pass/SpassFingerprint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FingerprintClientSpecBuilder"
.end annotation


# instance fields
.field private mBundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 978
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;->mBundle:Landroid/os/Bundle;

    .line 986
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 987
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "appName"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    :cond_0
    return-void
.end method


# virtual methods
.method public build()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public setAllowFingers([I)Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;
    .locals 2
    .param p1, "allowFingers"    # [I

    .prologue
    .line 992
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 993
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "request_template_index_list"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 995
    :cond_0
    return-object p0
.end method
