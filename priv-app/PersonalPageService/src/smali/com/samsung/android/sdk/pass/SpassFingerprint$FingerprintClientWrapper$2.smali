.class Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;
.super Ljava/lang/Object;
.source "SpassFingerprint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 400
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$3(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$4(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$4(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->hasPendingCommand()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$4(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$3(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->cancel(Landroid/os/IBinder;)Z

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$4(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$3(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->unregisterClient(Landroid/os/IBinder;)Z

    .line 405
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$5(Lcom/samsung/android/sdk/pass/SpassFingerprint;Landroid/os/IBinder;)V

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$6(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/fingerprint/IFingerprintClient;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 408
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$7(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/fingerprint/IFingerprintClient;)V

    .line 410
    :cond_2
    return-void
.end method
