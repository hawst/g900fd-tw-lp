.class Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;
.super Ljava/lang/Object;
.source "SpassFingerprint.java"

# interfaces
.implements Lcom/samsung/android/fingerprint/FingerprintIdentifyDialog$FingerprintListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pass/SpassFingerprint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FingerprintListenerWrapper"
.end annotation


# instance fields
.field private finishEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

.field private mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .prologue
    .line 424
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    iput-object p2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 426
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)V
    .locals 0

    .prologue
    .line 424
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    return-object v0
.end method


# virtual methods
.method public notifyDialogDismiss()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 458
    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->finishEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 459
    .local v1, "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 460
    .local v0, "callback":Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 461
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;

    invoke-direct {v3, p0, v1, v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$2;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;Lcom/samsung/android/fingerprint/FingerprintEvent;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 474
    iput-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 475
    iput-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->finishEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 477
    :cond_0
    return-void
.end method

.method public onEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 5
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 430
    move-object v1, p1

    .line 432
    .local v1, "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    :try_start_0
    iget v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    const/16 v3, 0xd

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 433
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$1;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper$1;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 454
    :goto_0
    return-void

    .line 449
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;->finishEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SpassFingerprintSDK"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onFingerprintEvent: Error : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
