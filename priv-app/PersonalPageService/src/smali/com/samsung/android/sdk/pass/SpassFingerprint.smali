.class public Lcom/samsung/android/sdk/pass/SpassFingerprint;
.super Ljava/lang/Object;
.source "SpassFingerprint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;,
        Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;,
        Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;,
        Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;,
        Lcom/samsung/android/sdk/pass/SpassFingerprint$RegisterListener;
    }
.end annotation


# static fields
.field static final SDK_VERSION:I = 0x1010001

.field static final SDK_VERSION_CODE:I = 0x3

.field public static final STATUS_AUTHENTIFICATION_FAILED:I = 0x10

.field public static final STATUS_AUTHENTIFICATION_PASSWORD_SUCCESS:I = 0x64

.field public static final STATUS_AUTHENTIFICATION_SUCCESS:I = 0x0

.field public static final STATUS_QUALITY_FAILED:I = 0xc

.field public static final STATUS_SENSOR_FAILED:I = 0x7

.field public static final STATUS_TIMEOUT_FAILED:I = 0x4

.field public static final STATUS_USER_CANCELLED:I = 0x8

.field public static final STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE:I = 0xd

.field private static final TAG:Ljava/lang/String; = "SpassFingerprintSDK"

.field private static isSystemFeatureEnabeld:Z

.field private static isSystemFeatureQueried:Z


# instance fields
.field private client:Lcom/samsung/android/fingerprint/IFingerprintClient;

.field private clientSpec:Landroid/os/Bundle;

.field private enrolledFingerArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mClientToken:Landroid/os/IBinder;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

.field private mDialogIconName:Ljava/lang/String;

.field private mDialogTitleColor:I

.field private mDialogTitleText:Ljava/lang/String;

.field private mDialogTouchOutside:Z

.field private mDialogTransparency:I

.field private mFingerprintIndex:I

.field private mFingerprintIndexList:[I

.field private mHandler:Landroid/os/Handler;

.field private mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

.field private mServiceVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 145
    sput-boolean v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureQueried:Z

    .line 150
    sput-boolean v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureEnabeld:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 192
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 132
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndex:I

    .line 133
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    .line 134
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    .line 135
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    .line 136
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    .line 137
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    .line 138
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    .line 139
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTouchOutside:Z

    .line 140
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mServiceVersion:I

    .line 155
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    .line 161
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    .line 167
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 169
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->clientSpec:Landroid/os/Bundle;

    .line 170
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->client:Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 193
    const-string v2, "com.sec.feature.fingerprint_manager_service"

    .line 195
    .local v2, "FINGERPRINT_MANAGER_SERVICE":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    .line 197
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    if-nez v13, :cond_0

    .line 198
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "context is null."

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 201
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    sget-boolean v13, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureQueried:Z

    if-nez v13, :cond_1

    .line 207
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    const-string v14, "com.sec.feature.fingerprint_manager_service"

    invoke-virtual {v13, v14}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v13

    sput-boolean v13, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureEnabeld:Z

    .line 208
    const/4 v13, 0x1

    sput-boolean v13, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureQueried:Z

    .line 210
    :cond_1
    sget-boolean v13, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureEnabeld:Z

    if-eqz v13, :cond_4

    .line 212
    :try_start_1
    const-string v13, "com.samsung.android.fingerprint.FingerprintManager"

    invoke-static {v13}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 213
    .local v3, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v13, "getInstance"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Class;

    const/4 v15, 0x0

    const-class v16, Landroid/content/Context;

    aput-object v16, v14, v15

    invoke-virtual {v3, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 214
    .local v5, "methodGetInstance":Ljava/lang/reflect/Method;
    const-string v13, "getVersion"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Class;

    invoke-virtual {v3, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 215
    .local v6, "methodGetVersion":Ljava/lang/reflect/Method;
    const/4 v13, 0x0

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-virtual {v5, v13, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 216
    .local v8, "proxyTarget":Ljava/lang/Object;
    if-eqz v8, :cond_2

    .line 217
    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-virtual {v6, v8, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mServiceVersion:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 222
    .end local v3    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "methodGetInstance":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetVersion":Ljava/lang/reflect/Method;
    .end local v8    # "proxyTarget":Ljava/lang/Object;
    :cond_2
    :goto_0
    const v10, 0x1010001

    .line 223
    .local v10, "sdkVersion":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mServiceVersion:I

    shr-int/lit8 v13, v13, 0x18

    and-int/lit16 v11, v13, 0xff

    .line 224
    .local v11, "serviceMajorVersion":I
    const/4 v13, 0x1

    and-int/lit16 v9, v13, 0xff

    .line 225
    .local v9, "sdkMajorVersion":I
    if-le v11, v9, :cond_5

    move v12, v9

    .line 226
    .local v12, "supportSdkVersion":I
    :goto_1
    const/4 v13, 0x1

    if-lt v12, v13, :cond_3

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory;->create(Landroid/content/Context;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    .line 229
    :cond_3
    new-instance v13, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v14

    invoke-direct {v13, v14}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;

    .line 231
    .end local v9    # "sdkMajorVersion":I
    .end local v10    # "sdkVersion":I
    .end local v11    # "serviceMajorVersion":I
    .end local v12    # "supportSdkVersion":I
    :cond_4
    const-class v13, Lcom/samsung/android/sdk/pass/SpassFingerprint;

    const-string v14, "com.samsung.android.fingerprint.FingerprintManager"

    const-string v15, "EVENT_IDENTIFY_"

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14, v15}, Lcom/samsung/android/sdk/pass/support/SdkSupporter;->copyStaticFields(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Z

    .line 232
    return-void

    .line 202
    :catch_0
    move-exception v7

    .line 203
    .local v7, "npe":Ljava/lang/NullPointerException;
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "context is not valid."

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 219
    .end local v7    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v4

    .line 220
    .local v4, "e":Ljava/lang/Exception;
    const-string v13, "SpassFingerprintSDK"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getVersion failed : "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v9    # "sdkMajorVersion":I
    .restart local v10    # "sdkVersion":I
    .restart local v11    # "serviceMajorVersion":I
    :cond_5
    move v12, v11

    .line 225
    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)V
    .locals 0

    .prologue
    .line 132
    iput p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndex:I

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)I
    .locals 1

    .prologue
    .line 1009
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->convertStatusValues(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pass/SpassFingerprint;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Lcom/samsung/android/fingerprint/IFingerprintClient;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/fingerprint/IFingerprintClient;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pass/SpassFingerprint;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    return-void
.end method

.method private checkIconResource(Ljava/lang/String;)Z
    .locals 7
    .param p1, "iconName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 854
    iget-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 855
    .local v3, "pkgName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 857
    .local v0, "Res":Landroid/content/res/Resources;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 862
    if-nez v0, :cond_1

    .line 878
    :cond_0
    :goto_0
    return v5

    .line 858
    :catch_0
    move-exception v1

    .line 859
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 866
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    :try_start_1
    const-string v6, "drawable"

    invoke-virtual {v0, p1, v6, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 867
    .local v4, "resourceId":I
    if-eqz v4, :cond_0

    const/4 v6, -0x1

    if-eq v4, v6, :cond_0

    .line 870
    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 871
    .local v2, "icon":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 878
    const/4 v5, 0x1

    goto :goto_0

    .line 875
    .end local v2    # "icon":Landroid/graphics/Bitmap;
    .end local v4    # "resourceId":I
    :catch_1
    move-exception v1

    .line 876
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    goto :goto_0
.end method

.method private convertStatusValues(I)I
    .locals 1
    .param p1, "platformValue"    # I

    .prologue
    .line 1010
    sparse-switch p1, :sswitch_data_0

    .line 1026
    const/16 v0, 0x10

    :goto_0
    return v0

    .line 1012
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1014
    :sswitch_1
    const/16 v0, 0x64

    goto :goto_0

    .line 1016
    :sswitch_2
    const/16 v0, 0xc

    goto :goto_0

    .line 1018
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1020
    :sswitch_4
    const/4 v0, 0x7

    goto :goto_0

    .line 1022
    :sswitch_5
    const/16 v0, 0x8

    goto :goto_0

    .line 1024
    :sswitch_6
    const/16 v0, 0xd

    goto :goto_0

    .line 1010
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_3
        0x7 -> :sswitch_4
        0x8 -> :sswitch_5
        0xc -> :sswitch_2
        0xd -> :sswitch_6
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method private declared-synchronized ensureServiceSupported()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureEnabeld:Z

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Fingerprint Service is not supported in the platform."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 255
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    if-nez v0, :cond_1

    .line 256
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Fingerprint Service is not running on the device."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public cancelIdentify()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No Identify request."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->cancel(Landroid/os/IBinder;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 278
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cancel() returned RESULT_FAILED due to FingerprintService Error."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    const/4 v1, 0x4

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->notifyAppActivityState(ILandroid/os/Bundle;)V

    .line 283
    :cond_3
    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    .line 284
    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 285
    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    .line 286
    return-void
.end method

.method public getIdentifiedFingerprintIndex()I
    .locals 2

    .prologue
    .line 650
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 651
    iget v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 652
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FingerprintIndex is Invalid. This API must be called inside IdentifyListener.onFinished() only."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndex:I

    return v0
.end method

.method public getRegisteredFingerprintName()Landroid/util/SparseArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 668
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 669
    .local v2, "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->getEnrolledFingers()I

    move-result v0

    .line 670
    .local v0, "fingers":I
    if-gtz v0, :cond_1

    .line 671
    const/4 v2, 0x0

    .line 679
    :cond_0
    return-object v2

    .line 673
    :cond_1
    const/4 v1, 0x1

    .local v1, "index":I
    :goto_0
    const/16 v3, 0xa

    if-gt v1, v3, :cond_0

    .line 674
    const/4 v3, 0x1

    shl-int/2addr v3, v1

    and-int/2addr v3, v0

    if-eqz v3, :cond_2

    .line 675
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v3, v1}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->getIndexName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 673
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRegisteredFingerprintUniqueID()Landroid/util/SparseArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 693
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 695
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportFingerprintIds()Z

    move-result v3

    if-nez v3, :cond_0

    .line 696
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "getRegisteredFingerprintUniqueID is not supported."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 698
    :cond_0
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 699
    .local v2, "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->getEnrolledFingers()I

    move-result v0

    .line 700
    .local v0, "fingers":I
    if-gtz v0, :cond_2

    .line 701
    const/4 v2, 0x0

    .line 709
    :cond_1
    return-object v2

    .line 703
    :cond_2
    const/4 v1, 0x1

    .local v1, "index":I
    :goto_0
    const/16 v3, 0xa

    if-gt v1, v3, :cond_1

    .line 704
    const/4 v3, 0x1

    shl-int/2addr v3, v1

    and-int/2addr v3, v0

    if-eqz v3, :cond_3

    .line 705
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v3, v1}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->getFingerprintId(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 703
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public hasRegisteredFinger()Z
    .locals 1

    .prologue
    .line 588
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 589
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->getEnrolledFingers()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSupportFingerprintIds()Z
    .locals 5

    .prologue
    .line 742
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 743
    const/4 v2, 0x0

    .line 745
    .local v2, "isSupportFingerprintFWApi":Z
    :try_start_0
    const-string v3, "com.samsung.android.fingerprint.FingerprintManager"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 746
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "isSupportFingerprintIds"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 747
    const/4 v2, 0x1

    .line 752
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    if-eqz v2, :cond_0

    .line 753
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->isSupportFingerprintIds()Z

    move-result v3

    .line 755
    :goto_1
    return v3

    .line 748
    :catch_0
    move-exception v1

    .line 749
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "SpassFingerprintSDK"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 755
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v3

    goto :goto_1
.end method

.method isSupported()Z
    .locals 1

    .prologue
    .line 243
    sget-boolean v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSystemFeatureEnabeld:Z

    return v0
.end method

.method isSupportedCustomizedIdentifyDialog()Z
    .locals 3

    .prologue
    .line 723
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 724
    const/4 v0, 0x0

    .line 725
    .local v0, "mReturn":Z
    iget v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mServiceVersion:I

    const v2, 0x1010100

    if-lt v1, v2, :cond_0

    .line 726
    const/4 v0, 0x1

    .line 728
    :cond_0
    return v0
.end method

.method public registerFinger(Landroid/content/Context;Lcom/samsung/android/sdk/pass/SpassFingerprint$RegisterListener;)V
    .locals 5
    .param p1, "activityContext"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pass/SpassFingerprint$RegisterListener;

    .prologue
    .line 609
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 611
    if-nez p1, :cond_0

    .line 612
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "activityContext passed is null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 613
    :cond_0
    if-nez p2, :cond_1

    .line 614
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "listener passed is null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 615
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->isEnrolling()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 616
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->notifyEnrollEnd()Z

    .line 619
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    new-instance v3, Lcom/samsung/android/sdk/pass/SpassFingerprint$2;

    invoke-direct {v3, p0, p2}, Lcom/samsung/android/sdk/pass/SpassFingerprint$2;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$RegisterListener;)V

    .line 628
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 624
    invoke-interface {v2, p1, v3, v4}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->startEnrollActivity(Landroid/content/Context;Lcom/samsung/android/fingerprint/FingerprintManager$EnrollFinishListener;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_1 .. :try_end_1} :catch_1

    .line 632
    return-void

    .line 620
    :catch_0
    move-exception v0

    .line 621
    .local v0, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "activityContext is invalid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 629
    .end local v0    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 630
    .local v1, "ute":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "activityContext is invalid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 2
    .param p1, "cancel"    # Z

    .prologue
    .line 918
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 919
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 920
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setCanceledOnTouchOutside is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 922
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTouchOutside:Z

    .line 923
    return-void
.end method

.method public setDialogBgTransparency(I)V
    .locals 2
    .param p1, "transparency"    # I

    .prologue
    .line 895
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 897
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 898
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setDialogBGTransparency is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 900
    :cond_0
    if-ltz p1, :cond_1

    const/16 v0, 0xff

    if-le p1, v0, :cond_2

    .line 901
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the transparency passed is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 903
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    .line 904
    return-void
.end method

.method public setDialogIcon(Ljava/lang/String;)V
    .locals 2
    .param p1, "iconName"    # Ljava/lang/String;

    .prologue
    .line 840
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 842
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 843
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setDialogIcon is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 845
    :cond_0
    if-nez p1, :cond_1

    .line 846
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the iconName passed is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 848
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->checkIconResource(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 849
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the iconName passed is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 851
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    .line 852
    return-void
.end method

.method public setDialogTitle(Ljava/lang/String;I)V
    .locals 2
    .param p1, "titleText"    # Ljava/lang/String;
    .param p2, "titleColor"    # I

    .prologue
    .line 808
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 810
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 811
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setDialogTitle is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813
    :cond_0
    if-nez p1, :cond_1

    .line 814
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the titletext passed is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 816
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x100

    if-le v0, v1, :cond_2

    .line 817
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the title text passed is longer than 256 characters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819
    :cond_2
    shr-int/lit8 v0, p2, 0x18

    and-int/lit16 v0, v0, 0xff

    if-eqz v0, :cond_3

    .line 820
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "alpha value is not supported in the titleColor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 822
    :cond_3
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    .line 823
    const/high16 v0, -0x1000000

    add-int/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    .line 824
    return-void
.end method

.method public setIntendedFingerprintIndex(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 774
    .local p1, "requestedIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 776
    if-nez p1, :cond_1

    .line 777
    const-string v1, "SpassFingerprintSDK"

    const-string v2, "requestedIndex is null. Identify is carried out for all indexes."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    :cond_0
    return-void

    .line 780
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v1

    if-nez v1, :cond_2

    .line 781
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "setIntendedFingerprintIndex is not supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 783
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    .line 784
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 785
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public startIdentify(Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .prologue
    const/4 v5, 0x0

    .line 308
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 310
    if-nez p1, :cond_0

    .line 311
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "listener passed is null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 313
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->client:Lcom/samsung/android/fingerprint/IFingerprintClient;

    if-nez v2, :cond_1

    .line 314
    new-instance v2, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    invoke-direct {v2, p0, p1, v5}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->client:Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 316
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 317
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    .line 318
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 322
    .end local v0    # "i":I
    :cond_2
    new-instance v2, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;->setAllowFingers([I)Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientSpecBuilder;->build()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->clientSpec:Landroid/os/Bundle;

    .line 323
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->client:Lcom/samsung/android/fingerprint/IFingerprintClient;

    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->clientSpec:Landroid/os/Bundle;

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->registerClient(Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;)Landroid/os/IBinder;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    .line 324
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    if-nez v2, :cond_4

    .line 325
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "failed because registerClient returned null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 319
    .restart local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v0

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    .end local v0    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    invoke-interface {v2, v3, v5}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->identify(Landroid/os/IBinder;Ljava/lang/String;)I

    move-result v1

    .line 329
    .local v1, "result":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_6

    .line 330
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->hasPendingCommand()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 331
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->cancel(Landroid/os/IBinder;)Z

    .line 333
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->unregisterClient(Landroid/os/IBinder;)Z

    .line 334
    iput-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mClientToken:Landroid/os/IBinder;

    .line 335
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "identify() returned RESULT_FAILED."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 337
    :cond_6
    const/4 v2, -0x2

    if-ne v1, v2, :cond_7

    .line 338
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "identify() returned RESULT_IN_PROGRESS."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 339
    :cond_7
    const/16 v2, 0x33

    if-ne v1, v2, :cond_8

    .line 340
    new-instance v2, Lcom/samsung/android/sdk/pass/SpassInvalidStateException;

    const-string v3, "Identify request is denied in 30 seconds because 5 identify attempts are failed."

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/pass/SpassInvalidStateException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 342
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->client:Lcom/samsung/android/fingerprint/IFingerprintClient;

    check-cast v2, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->updateListener(Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V

    .line 343
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    if-eqz v2, :cond_9

    .line 344
    iput-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    .line 346
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    if-eqz v2, :cond_a

    .line 347
    iput-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    .line 349
    :cond_a
    return-void
.end method

.method public startIdentifyWithDialog(Landroid/content/Context;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Z)V
    .locals 9
    .param p1, "activityContext"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;
    .param p3, "enablePassword"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 500
    invoke-direct {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->ensureServiceSupported()V

    .line 502
    if-nez p1, :cond_0

    .line 503
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "activityContext passed is null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 504
    :cond_0
    if-nez p2, :cond_1

    .line 505
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "listener passed is null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 508
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->isSupportedCustomizedIdentifyDialog()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 513
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 514
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    .line 515
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_f

    .line 519
    .end local v2    # "i":I
    :cond_2
    new-instance v4, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    invoke-direct {v4, p0, p2, v6}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 521
    :try_start_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 522
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "password"

    invoke-virtual {v0, v4, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 523
    const-string v4, "packageName"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const-string v4, "demandExtraEvent"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 525
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    if-eqz v4, :cond_3

    .line 526
    const-string v4, "request_template_index_list"

    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 528
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 529
    const-string v4, "titletext"

    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_4
    iget v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    if-eq v4, v7, :cond_5

    .line 532
    const-string v4, "titlecolor"

    iget v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 534
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 535
    const-string v4, "iconname"

    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_6
    iget v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    if-eq v4, v7, :cond_7

    .line 538
    const-string v4, "transparency"

    iget v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 540
    :cond_7
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTouchOutside:Z

    if-eqz v4, :cond_8

    .line 541
    const-string v4, "touchoutside"

    iget-boolean v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTouchOutside:Z

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 543
    :cond_8
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    invoke-interface {v4, p1, v5, v0}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->identifyWithDialog(Landroid/content/Context;Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    if-eqz v4, :cond_9

    .line 546
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    .line 548
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    if-eqz v4, :cond_a

    .line 549
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    .line 551
    :cond_a
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    if-eqz v4, :cond_b

    .line 552
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    .line 554
    :cond_b
    iget v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    if-eq v4, v7, :cond_c

    .line 555
    iput v7, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    .line 557
    :cond_c
    iget v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    if-eq v4, v7, :cond_d

    .line 558
    iput v7, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    .line 560
    :cond_d
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    if-eqz v4, :cond_e

    .line 561
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    .line 563
    :cond_e
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTouchOutside:Z

    .line 576
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 509
    :catch_0
    move-exception v3

    .line 510
    .local v3, "npe":Ljava/lang/NullPointerException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "activityContext is invalid"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 516
    .end local v3    # "npe":Ljava/lang/NullPointerException;
    .restart local v2    # "i":I
    :cond_f
    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v5, v2

    .line 515
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 544
    .end local v2    # "i":I
    :catchall_0
    move-exception v4

    .line 545
    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    if-eqz v5, :cond_10

    .line 546
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->enrolledFingerArrayList:Ljava/util/ArrayList;

    .line 548
    :cond_10
    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    if-eqz v5, :cond_11

    .line 549
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mFingerprintIndexList:[I

    .line 551
    :cond_11
    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    if-eqz v5, :cond_12

    .line 552
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleText:Ljava/lang/String;

    .line 554
    :cond_12
    iget v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    if-eq v5, v7, :cond_13

    .line 555
    iput v7, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTitleColor:I

    .line 557
    :cond_13
    iget v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    if-eq v5, v7, :cond_14

    .line 558
    iput v7, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTransparency:I

    .line 560
    :cond_14
    iget-object v5, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    if-eqz v5, :cond_15

    .line 561
    iput-object v6, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogIconName:Ljava/lang/String;

    .line 563
    :cond_15
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialogTouchOutside:Z

    .line 564
    throw v4

    .line 566
    :cond_16
    new-instance v1, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;

    invoke-direct {v1, p0, p2, v6}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)V

    .line 567
    .local v1, "fingerprintListener":Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mProxy:Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-interface {v4, p1, v1, v6, p3}, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;->showIdentifyDialog(Landroid/content/Context;Lcom/samsung/android/fingerprint/FingerprintIdentifyDialog$FingerprintListener;Ljava/lang/String;Z)Landroid/app/Dialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    .line 568
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    new-instance v5, Lcom/samsung/android/sdk/pass/SpassFingerprint$1;

    invoke-direct {v5, p0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$1;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintListenerWrapper;)V

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 574
    iget-object v4, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto :goto_1
.end method
