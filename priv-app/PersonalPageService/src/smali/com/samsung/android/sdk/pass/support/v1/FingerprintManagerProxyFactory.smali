.class public Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory;
.super Ljava/lang/Object;
.source "FingerprintManagerProxyFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "FingerprintManagerProxy"

.field private static classLoader:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 22
    const/4 v5, 0x0

    .line 25
    .local v5, "proxyTarget":Ljava/lang/Object;
    :try_start_0
    const-string v6, "com.samsung.android.fingerprint.FingerprintManager"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 26
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v6, "getInstance"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    invoke-virtual {v0, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 27
    .local v3, "methodGetInstance":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p0, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 32
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "methodGetInstance":Ljava/lang/reflect/Method;
    .end local v5    # "proxyTarget":Ljava/lang/Object;
    :goto_0
    if-nez v5, :cond_0

    .line 43
    :goto_1
    return-object v4

    .line 28
    .restart local v5    # "proxyTarget":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 29
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "FingerprintManagerProxy"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Cannot create FingerprintManagerProxy : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 36
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v5    # "proxyTarget":Ljava/lang/Object;
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;

    invoke-direct {v2, v5}, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;-><init>(Ljava/lang/Object;)V

    .line 38
    .local v2, "handler":Ljava/lang/reflect/InvocationHandler;
    const-class v6, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory;

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    sput-object v6, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory;->classLoader:Ljava/lang/ClassLoader;

    .line 40
    sget-object v6, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory;->classLoader:Ljava/lang/ClassLoader;

    .line 41
    new-array v7, v11, [Ljava/lang/Class;

    const-class v8, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    aput-object v8, v7, v10

    .line 40
    invoke-static {v6, v7, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    .line 43
    .local v4, "proxy":Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;
    goto :goto_1
.end method
