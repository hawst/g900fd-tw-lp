.class Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;
.super Ljava/lang/Object;
.source "FingerprintManagerProxyFactory.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProxyInvocationHandler"
.end annotation


# instance fields
.field private impl:Ljava/lang/Object;

.field private methodMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 7
    .param p1, "impl"    # Ljava/lang/Object;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;->methodMap:Ljava/util/Map;

    .line 52
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;->impl:Ljava/lang/Object;

    .line 54
    const-class v4, Lcom/samsung/android/sdk/pass/support/IFingerprintManagerProxy;

    invoke-virtual {v4}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    .line 55
    .local v0, "imethods":[Ljava/lang/reflect/Method;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    .line 61
    .local v3, "methods":[Ljava/lang/reflect/Method;
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 73
    return-void

    .line 61
    :cond_0
    aget-object v1, v3, v4

    .line 63
    .local v1, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "methodName":Ljava/lang/String;
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;->findMatchMethod([Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 70
    iget-object v6, p0, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;->methodMap:Ljava/util/Map;

    invoke-interface {v6, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private findMatchMethod([Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)Z
    .locals 13
    .param p1, "targetMethods"    # [Ljava/lang/reflect/Method;
    .param p2, "method"    # Ljava/lang/reflect/Method;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 94
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "methodName":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    .line 97
    .local v4, "params":[Ljava/lang/Class;
    array-length v10, p1

    move v9, v8

    :goto_0
    if-lt v9, v10, :cond_1

    move v7, v8

    .line 146
    :cond_0
    :goto_1
    return v7

    .line 97
    :cond_1
    aget-object v5, p1, v9

    .line 99
    .local v5, "targetMethod":Ljava/lang/reflect/Method;
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 101
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    .line 107
    .local v6, "targetParams":[Ljava/lang/Class;
    if-nez v4, :cond_2

    if-eqz v6, :cond_0

    .line 112
    :cond_2
    if-eqz v4, :cond_3

    if-eqz v6, :cond_3

    .line 114
    array-length v11, v4

    array-length v12, v6

    if-eq v11, v12, :cond_4

    .line 97
    .end local v6    # "targetParams":[Ljava/lang/Class;
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 122
    .restart local v6    # "targetParams":[Ljava/lang/Class;
    :cond_4
    array-length v0, v4

    .line 124
    .local v0, "N":I
    const/4 v3, 0x0

    .line 126
    .local v3, "mismatch":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-lt v1, v0, :cond_5

    .line 132
    if-nez v3, :cond_3

    goto :goto_1

    .line 127
    :cond_5
    aget-object v11, v6, v1

    aget-object v12, v4, v1

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 128
    const/4 v3, 0x1

    .line 126
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "methodName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;->methodMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    .line 85
    .local v1, "targetMethod":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 86
    iget-object v2, p0, Lcom/samsung/android/sdk/pass/support/v1/FingerprintManagerProxyFactory$ProxyInvocationHandler;->impl:Ljava/lang/Object;

    invoke-virtual {v1, v2, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 89
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
