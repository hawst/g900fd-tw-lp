.class Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;
.super Lcom/samsung/android/fingerprint/IFingerprintClient$Stub;
.source "SpassFingerprint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pass/SpassFingerprint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FingerprintClientWrapper"
.end annotation


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/IFingerprintClient$Stub;-><init>()V

    .line 358
    iput-object p2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 359
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)V
    .locals 0

    .prologue
    .line 357
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    return-object v0
.end method


# virtual methods
.method public onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 6
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 367
    if-nez p1, :cond_1

    .line 368
    const-string v3, "SpassFingerprintSDK"

    const-string v4, "onFingerprintEvent: null event will be ignored!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    move-object v2, p1

    .line 373
    .local v2, "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 375
    .local v0, "callback":Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 378
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;

    invoke-direct {v4, p0, v2, v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;Lcom/samsung/android/fingerprint/FingerprintEvent;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 396
    iget v3, p1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    const/16 v4, 0xd

    if-ne v3, v4, :cond_0

    .line 397
    iget-object v3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$0(Lcom/samsung/android/sdk/pass/SpassFingerprint;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$2;-><init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 413
    .end local v0    # "callback":Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;
    :catch_0
    move-exception v1

    .line 414
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "SpassFingerprintSDK"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onFingerprintEvent: Error : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateListener(Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->mListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 363
    return-void
.end method
