.class Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;
.super Ljava/lang/Object;
.source "SpassFingerprint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

.field private final synthetic val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

.field private final synthetic val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;Lcom/samsung/android/fingerprint/FingerprintEvent;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    iput-object p2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iput-object p3, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 381
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iget v0, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    packed-switch v0, :pswitch_data_0

    .line 394
    :goto_0
    return-void

    .line 383
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;->onReady()V

    goto :goto_0

    .line 386
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;->onStarted()V

    goto :goto_0

    .line 389
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getFingerIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)V

    .line 390
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$callback:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->val$event:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iget v2, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    # invokes: Lcom/samsung/android/sdk/pass/SpassFingerprint;->convertStatusValues(I)I
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$2(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;->onFinished(I)V

    .line 391
    iget-object v0, p0, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper$1;->this$1:Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;

    # getter for: Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->this$0:Lcom/samsung/android/sdk/pass/SpassFingerprint;
    invoke-static {v0}, Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint$FingerprintClientWrapper;)Lcom/samsung/android/sdk/pass/SpassFingerprint;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->access$1(Lcom/samsung/android/sdk/pass/SpassFingerprint;I)V

    goto :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
