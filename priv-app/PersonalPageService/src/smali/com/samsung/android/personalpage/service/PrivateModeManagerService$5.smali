.class Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;
.super Ljava/lang/Object;
.source "PrivateModeManagerService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->showPrivateModeNormalDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/personalpage/service/PrivateModeManagerService;

.field final synthetic val$status:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/personalpage/service/PrivateModeManagerService;Z)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;->this$0:Lcom/samsung/android/personalpage/service/PrivateModeManagerService;

    iput-boolean p2, p0, Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;->val$status:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 588
    const-string v0, "PrivateModeManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NormalModePopup: onCancel, mIsMountedPrivateStorage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->mIsMountedPrivateStorage:Z
    invoke-static {}, Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->access$900()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/personalpage/service/util/PersonalPageLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-boolean v0, p0, Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;->val$status:Z

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;->this$0:Lcom/samsung/android/personalpage/service/PrivateModeManagerService;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->notifyEventToClient(I)V

    .line 592
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;->this$0:Lcom/samsung/android/personalpage/service/PrivateModeManagerService;

    iget-object v0, p0, Lcom/samsung/android/personalpage/service/PrivateModeManagerService$5;->this$0:Lcom/samsung/android/personalpage/service/PrivateModeManagerService;

    # getter for: Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->mPpService:Lcom/samsung/android/personalpage/service/PersonalPageService;
    invoke-static {v0}, Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->access$400(Lcom/samsung/android/personalpage/service/PrivateModeManagerService;)Lcom/samsung/android/personalpage/service/PersonalPageService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/personalpage/service/PersonalPageService;->isPrivateStorageAtLeastMounted()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->updatePrivateModeDB(I)V
    invoke-static {v1, v0}, Lcom/samsung/android/personalpage/service/PrivateModeManagerService;->access$700(Lcom/samsung/android/personalpage/service/PrivateModeManagerService;I)V

    .line 593
    return-void

    .line 592
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
