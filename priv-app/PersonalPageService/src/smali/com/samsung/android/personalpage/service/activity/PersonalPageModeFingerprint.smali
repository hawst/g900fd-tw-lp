.class public Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;
.super Lcom/samsung/android/personalpage/service/activity/PersonalPageServiceActivity;
.source "PersonalPageModeFingerprint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PersonalPageModeFingerprint"


# instance fields
.field private mContext:Landroid/content/Context;

.field final mH:Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;

.field private mIdenfityListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

.field private mSpassFingerprint:Lcom/samsung/android/sdk/pass/SpassFingerprint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageServiceActivity;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mContext:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mSpassFingerprint:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    .line 37
    new-instance v0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;

    invoke-direct {v0, p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;-><init>(Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;)V

    iput-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mH:Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;

    .line 97
    new-instance v0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$1;-><init>(Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;)V

    iput-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mIdenfityListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->handleIdentify()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->verifySucess()V

    return-void
.end method

.method private handleIdentify()V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mSpassFingerprint:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mSpassFingerprint:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    iget-object v1, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mIdenfityListener:Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pass/SpassFingerprint;->startIdentifyWithDialog(Landroid/content/Context;Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;Z)V

    .line 190
    :cond_0
    return-void
.end method

.method private initFingerprintManager()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mSpassFingerprint:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Lcom/samsung/android/sdk/pass/SpassFingerprint;

    iget-object v1, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pass/SpassFingerprint;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mSpassFingerprint:Lcom/samsung/android/sdk/pass/SpassFingerprint;

    .line 137
    :cond_0
    return-void
.end method

.method private requestIdentify()V
    .locals 2

    .prologue
    const/16 v1, 0x401

    .line 164
    iget-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mH:Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;

    invoke-virtual {v0, v1}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;->removeMessages(I)V

    .line 165
    iget-object v0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mH:Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;

    invoke-virtual {v0, v1}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint$H;->sendEmptyMessage(I)Z

    .line 166
    return-void
.end method

.method private verifySucess()V
    .locals 2

    .prologue
    .line 140
    const-string v0, "PersonalPageModeFingerprint"

    const-string v1, "veritySucess"

    invoke-static {v0, v1}, Lcom/samsung/android/personalpage/service/util/PersonalPageLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->requestPrivateModeOn()V

    .line 142
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/samsung/android/personalpage/service/activity/PersonalPageServiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    iput-object p0, p0, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->mContext:Landroid/content/Context;

    .line 77
    invoke-virtual {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 79
    invoke-direct {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->initFingerprintManager()V

    .line 80
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageServiceActivity;->onResume()V

    .line 85
    invoke-direct {p0}, Lcom/samsung/android/personalpage/service/activity/PersonalPageModeFingerprint;->requestIdentify()V

    .line 86
    return-void
.end method
