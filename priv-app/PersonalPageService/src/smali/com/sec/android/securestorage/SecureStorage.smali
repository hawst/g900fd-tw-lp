.class public Lcom/sec/android/securestorage/SecureStorage;
.super Ljava/lang/Object;
.source "SecureStorage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    }
.end annotation


# static fields
.field private static final DOUBLE_SIZE:I = 0x8

.field private static final INTEGER_SIZE:I = 0x4

.field private static secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/sec/android/securestorage/SecureStorageJNI;->getInstance()Lcom/sec/android/securestorage/SecureStorageJNI;

    move-result-object v0

    sput-object v0, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1615
    return-void
.end method

.method public static initialize()I
    .locals 1

    .prologue
    .line 1612
    sget-object v0, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Lcom/sec/android/securestorage/SecureStorageJNI;->initialize()I

    move-result v0

    return v0
.end method

.method public static isSupported()Z
    .locals 1

    .prologue
    .line 1599
    sget-object v0, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Lcom/sec/android/securestorage/SecureStorageJNI;->isSupported()Z

    move-result v0

    return v0
.end method

.method private throwException(Ljava/lang/String;)V
    .locals 2
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 45
    if-nez p1, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;

    const-string v1, "Error input data name"

    invoke-direct {v0, v1}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    return-void
.end method

.method private throwException(ZLjava/lang/String;)V
    .locals 1
    .param p1, "result"    # Z
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    new-instance v0, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;

    invoke-direct {v0, p2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    return-void
.end method

.method public static version()I
    .locals 1

    .prologue
    .line 1608
    sget-object v0, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Lcom/sec/android/securestorage/SecureStorageJNI;->getVersion()I

    move-result v0

    return v0
.end method


# virtual methods
.method public decrypt([B)[B
    .locals 5
    .param p1, "dataBlock"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1537
    if-nez p1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1539
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->decrypt([B)[B

    move-result-object v0

    .line 1540
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1542
    return-object v0

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1537
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1540
    goto :goto_1
.end method

.method public decrypt([BLjava/lang/String;)[B
    .locals 5
    .param p1, "dataBlock"    # [B
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1563
    if-nez p1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1565
    if-nez p2, :cond_1

    move v1, v2

    :goto_1
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1567
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->decrypt([BLjava/lang/String;)[B

    move-result-object v0

    .line 1568
    .local v0, "result":[B
    if-nez v0, :cond_2

    :goto_2
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1570
    return-object v0

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1563
    goto :goto_0

    :cond_1
    move v1, v3

    .line 1565
    goto :goto_1

    .restart local v0    # "result":[B
    :cond_2
    move v2, v3

    .line 1568
    goto :goto_2
.end method

.method public delete(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1586
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1587
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->delete(Ljava/lang/String;)Z

    move-result v0

    .line 1588
    .local v0, "result":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error input data name"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1589
    return v0

    .line 1588
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public encrypt([B)[B
    .locals 5
    .param p1, "dataBlock"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1488
    if-nez p1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1490
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->encrypt([B)[B

    move-result-object v0

    .line 1491
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Encryption error"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1493
    return-object v0

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1488
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1491
    goto :goto_1
.end method

.method public encrypt([BLjava/lang/String;)[B
    .locals 5
    .param p1, "dataBlock"    # [B
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1513
    if-nez p1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1515
    if-nez p2, :cond_1

    move v1, v2

    :goto_1
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1517
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->encrypt([BLjava/lang/String;)[B

    move-result-object v0

    .line 1518
    .local v0, "result":[B
    if-nez v0, :cond_2

    :goto_2
    const-string v1, "Encryption error"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1520
    return-object v0

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1513
    goto :goto_0

    :cond_1
    move v1, v3

    .line 1515
    goto :goto_1

    .restart local v0    # "result":[B
    :cond_2
    move v2, v3

    .line 1518
    goto :goto_2
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1443
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1444
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 1445
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1447
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 1445
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1466
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1467
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1469
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1470
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1472
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1467
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1470
    goto :goto_1
.end method

.method public getBooleanArray(Ljava/lang/String;)[Z
    .locals 7
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1381
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1382
    sget-object v3, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v3, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v2

    .line 1383
    .local v2, "result":[B
    if-nez v2, :cond_0

    move v3, v4

    :goto_0
    const-string v6, "Error: input data are incorrect"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1385
    array-length v3, v2

    new-array v1, v3, [Z

    .line 1386
    .local v1, "publicText":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 1387
    aget-byte v3, v2, v0

    if-ne v3, v4, :cond_1

    .line 1388
    aput-boolean v4, v1, v0

    .line 1386
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "publicText":[Z
    :cond_0
    move v3, v5

    .line 1383
    goto :goto_0

    .line 1390
    .restart local v0    # "i":I
    .restart local v1    # "publicText":[Z
    :cond_1
    aput-boolean v5, v1, v0

    goto :goto_2

    .line 1393
    :cond_2
    return-object v1
.end method

.method public getBooleanArray(Ljava/lang/String;Ljava/lang/String;)[Z
    .locals 7
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1413
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1414
    if-nez p2, :cond_0

    move v3, v4

    :goto_0
    const-string v6, "Error: password is null!"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1416
    sget-object v3, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 1417
    .local v2, "result":[B
    if-nez v2, :cond_1

    move v3, v4

    :goto_1
    const-string v6, "Error: input data are incorrect"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1419
    array-length v3, v2

    new-array v1, v3, [Z

    .line 1420
    .local v1, "publicText":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v3, v1

    if-ge v0, v3, :cond_3

    .line 1421
    aget-byte v3, v2, v0

    if-ne v3, v4, :cond_2

    .line 1422
    aput-boolean v4, v1, v0

    .line 1420
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .end local v0    # "i":I
    .end local v1    # "publicText":[Z
    .end local v2    # "result":[B
    :cond_0
    move v3, v5

    .line 1414
    goto :goto_0

    .restart local v2    # "result":[B
    :cond_1
    move v3, v5

    .line 1417
    goto :goto_1

    .line 1424
    .restart local v0    # "i":I
    .restart local v1    # "publicText":[Z
    :cond_2
    aput-boolean v5, v1, v0

    goto :goto_3

    .line 1427
    :cond_3
    return-object v1
.end method

.method public getByte(Ljava/lang/String;)B
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 964
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 965
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 966
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "Error: input data are incorrect"

    invoke-direct {p0, v1, v3}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 968
    aget-byte v1, v0, v2

    return v1

    :cond_0
    move v1, v2

    .line 966
    goto :goto_0
.end method

.method public getByte(Ljava/lang/String;Ljava/lang/String;)B
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 987
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 988
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 990
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 991
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 993
    aget-byte v1, v0, v3

    return v1

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 988
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 991
    goto :goto_1
.end method

.method public getByteArray(Ljava/lang/String;)[B
    .locals 3
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 919
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 920
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 921
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 923
    return-object v0

    .line 921
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getByteArray(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 942
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 943
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 945
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 946
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 948
    return-object v0

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 943
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 946
    goto :goto_1
.end method

.method public getChar(Ljava/lang/String;)C
    .locals 3
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 874
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 875
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 876
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 878
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    int-to-char v1, v1

    return v1

    .line 876
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getChar(Ljava/lang/String;Ljava/lang/String;)C
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 897
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 898
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 900
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 901
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 903
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    int-to-char v1, v1

    return v1

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 898
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 901
    goto :goto_1
.end method

.method public getCharArray(Ljava/lang/String;)[C
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 812
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 813
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v3

    .line 814
    .local v3, "result":[B
    if-nez v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Error: input data are incorrect"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 816
    array-length v4, v3

    div-int/lit8 v4, v4, 0x2

    new-array v2, v4, [C

    .line 817
    .local v2, "publicText":[C
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 818
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 819
    const/4 v1, 0x0

    .line 820
    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 821
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getChar()C

    move-result v4

    aput-char v4, v2, v1

    .line 822
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 814
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[C
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 824
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[C
    :cond_1
    return-object v2
.end method

.method public getCharArray(Ljava/lang/String;Ljava/lang/String;)[C
    .locals 8
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 844
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 845
    if-nez p2, :cond_0

    move v4, v5

    :goto_0
    const-string v7, "Error: password is null!"

    invoke-direct {p0, v4, v7}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 847
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 848
    .local v3, "result":[B
    if-nez v3, :cond_1

    :goto_1
    const-string v4, "Error: input data are incorrect"

    invoke-direct {p0, v5, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 850
    array-length v4, v3

    div-int/lit8 v4, v4, 0x2

    new-array v2, v4, [C

    .line 851
    .local v2, "publicText":[C
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 852
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 853
    const/4 v1, 0x0

    .line 854
    .local v1, "i":I
    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 855
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getChar()C

    move-result v4

    aput-char v4, v2, v1

    .line 856
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[C
    .end local v3    # "result":[B
    :cond_0
    move v4, v6

    .line 845
    goto :goto_0

    .restart local v3    # "result":[B
    :cond_1
    move v5, v6

    .line 848
    goto :goto_1

    .line 858
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[C
    :cond_2
    return-object v2
.end method

.method public getDouble(Ljava/lang/String;)D
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1334
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1335
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 1336
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1338
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    return-wide v2

    .line 1336
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDouble(Ljava/lang/String;Ljava/lang/String;)D
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1357
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1358
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1360
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1361
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1363
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    return-wide v2

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1358
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1361
    goto :goto_1
.end method

.method public getDoubleArray(Ljava/lang/String;)[D
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1272
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1273
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v3

    .line 1274
    .local v3, "result":[B
    if-nez v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Error: input data are incorrect"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1276
    array-length v4, v3

    div-int/lit8 v4, v4, 0x8

    new-array v2, v4, [D

    .line 1277
    .local v2, "publicText":[D
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1278
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1279
    const/4 v1, 0x0

    .line 1280
    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1281
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 1282
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1274
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[D
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1284
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[D
    :cond_1
    return-object v2
.end method

.method public getDoubleArray(Ljava/lang/String;Ljava/lang/String;)[D
    .locals 8
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1304
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1305
    if-nez p2, :cond_0

    move v4, v5

    :goto_0
    const-string v7, "Error: password is null!"

    invoke-direct {p0, v4, v7}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1307
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 1308
    .local v3, "result":[B
    if-nez v3, :cond_1

    :goto_1
    const-string v4, "Error: input data are incorrect"

    invoke-direct {p0, v5, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1310
    array-length v4, v3

    div-int/lit8 v4, v4, 0x8

    new-array v2, v4, [D

    .line 1311
    .local v2, "publicText":[D
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1312
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1313
    const/4 v1, 0x0

    .line 1314
    .local v1, "i":I
    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1315
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 1316
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[D
    .end local v3    # "result":[B
    :cond_0
    move v4, v6

    .line 1305
    goto :goto_0

    .restart local v3    # "result":[B
    :cond_1
    move v5, v6

    .line 1308
    goto :goto_1

    .line 1318
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[D
    :cond_2
    return-object v2
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 3
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1072
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1073
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 1074
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1076
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    return v1

    .line 1074
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1095
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1096
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1098
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1099
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1101
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    return v1

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1096
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1099
    goto :goto_1
.end method

.method public getIntArray(Ljava/lang/String;)[I
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1010
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1011
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v3

    .line 1012
    .local v3, "result":[B
    if-nez v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Error: input data are incorrect"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1014
    array-length v4, v3

    div-int/lit8 v4, v4, 0x4

    new-array v2, v4, [I

    .line 1015
    .local v2, "publicText":[I
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1016
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1017
    const/4 v1, 0x0

    .line 1018
    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1019
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    aput v4, v2, v1

    .line 1020
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1012
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[I
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1022
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[I
    :cond_1
    return-object v2
.end method

.method public getIntArray(Ljava/lang/String;Ljava/lang/String;)[I
    .locals 8
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1042
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1043
    if-nez p2, :cond_0

    move v4, v5

    :goto_0
    const-string v7, "Error: password is null!"

    invoke-direct {p0, v4, v7}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1045
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 1046
    .local v3, "result":[B
    if-nez v3, :cond_1

    :goto_1
    const-string v4, "Error: input data are incorrect"

    invoke-direct {p0, v5, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1048
    array-length v4, v3

    div-int/lit8 v4, v4, 0x4

    new-array v2, v4, [I

    .line 1049
    .local v2, "publicText":[I
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1050
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1051
    const/4 v1, 0x0

    .line 1052
    .local v1, "i":I
    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1053
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    aput v4, v2, v1

    .line 1054
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[I
    .end local v3    # "result":[B
    :cond_0
    move v4, v6

    .line 1043
    goto :goto_0

    .restart local v3    # "result":[B
    :cond_1
    move v5, v6

    .line 1046
    goto :goto_1

    .line 1056
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[I
    :cond_2
    return-object v2
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1180
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1181
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 1182
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1184
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v2

    return-wide v2

    .line 1182
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;Ljava/lang/String;)J
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1203
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1204
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1206
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1207
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1209
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v2

    return-wide v2

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1204
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1207
    goto :goto_1
.end method

.method public getLongArray(Ljava/lang/String;)[J
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1118
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1119
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v3

    .line 1120
    .local v3, "result":[B
    if-nez v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Error: input data are incorrect"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1122
    array-length v4, v3

    div-int/lit8 v4, v4, 0x8

    new-array v2, v4, [J

    .line 1123
    .local v2, "publicText":[J
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1124
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1125
    const/4 v1, 0x0

    .line 1126
    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1127
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 1128
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1120
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[J
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1130
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[J
    :cond_1
    return-object v2
.end method

.method public getLongArray(Ljava/lang/String;Ljava/lang/String;)[J
    .locals 8
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1150
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1151
    if-nez p2, :cond_0

    move v4, v5

    :goto_0
    const-string v7, "Error: password is null!"

    invoke-direct {p0, v4, v7}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1153
    sget-object v4, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 1154
    .local v3, "result":[B
    if-nez v3, :cond_1

    :goto_1
    const-string v4, "Error: input data are incorrect"

    invoke-direct {p0, v5, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1156
    array-length v4, v3

    div-int/lit8 v4, v4, 0x8

    new-array v2, v4, [J

    .line 1157
    .local v2, "publicText":[J
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1158
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1159
    const/4 v1, 0x0

    .line 1160
    .local v1, "i":I
    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1161
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 1162
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "i":I
    .end local v2    # "publicText":[J
    .end local v3    # "result":[B
    :cond_0
    move v4, v6

    .line 1151
    goto :goto_0

    .restart local v3    # "result":[B
    :cond_1
    move v5, v6

    .line 1154
    goto :goto_1

    .line 1164
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v1    # "i":I
    .restart local v2    # "publicText":[J
    :cond_2
    return-object v2
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "dataName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 1225
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1226
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 1227
    .local v0, "result":[B
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1229
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1

    .line 1227
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1248
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 1249
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1251
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1252
    .local v0, "result":[B
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error: input data are incorrect"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 1254
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1

    .end local v0    # "result":[B
    :cond_0
    move v1, v3

    .line 1249
    goto :goto_0

    .restart local v0    # "result":[B
    :cond_1
    move v2, v3

    .line 1252
    goto :goto_1
.end method

.method public put(Ljava/lang/String;B)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 301
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 302
    sget-object v3, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    new-array v4, v1, [B

    aput-byte p2, v4, v2

    invoke-virtual {v3, p1, v4}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    .line 304
    .local v0, "result":Z
    if-nez v0, :cond_0

    :goto_0
    const-string v2, "Error saving data"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 305
    return v0

    :cond_0
    move v1, v2

    .line 304
    goto :goto_0
.end method

.method public put(Ljava/lang/String;BLjava/lang/String;)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # B
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 327
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 328
    if-nez p3, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 330
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    new-array v4, v2, [B

    aput-byte p2, v4, v3

    invoke-virtual {v1, p1, v4, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v0

    .line 332
    .local v0, "result":Z
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 333
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 328
    goto :goto_0

    .restart local v0    # "result":Z
    :cond_1
    move v2, v3

    .line 332
    goto :goto_1
.end method

.method public put(Ljava/lang/String;C)Z
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 137
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    .line 139
    .local v0, "result":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error saving data"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 140
    return v0

    .line 139
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;CLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # C
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 163
    if-nez p3, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 165
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    int-to-long v4, p2

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v1, p1, v4, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v0

    .line 167
    .local v0, "result":Z
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 168
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 163
    goto :goto_0

    .restart local v0    # "result":Z
    :cond_1
    move v2, v3

    .line 167
    goto :goto_1
.end method

.method public put(Ljava/lang/String;D)Z
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 637
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 638
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 639
    .local v0, "dString":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v1

    .line 640
    .local v1, "result":Z
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Error saving data"

    invoke-direct {p0, v2, v3}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 641
    return v1

    .line 640
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;DLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # D
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 663
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 664
    if-nez p4, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: password is null!"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 666
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 667
    .local v0, "dString":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5, p4}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v1

    .line 669
    .local v1, "result":Z
    if-nez v1, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 670
    return v1

    .end local v0    # "dString":Ljava/lang/String;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 664
    goto :goto_0

    .restart local v0    # "dString":Ljava/lang/String;
    .restart local v1    # "result":Z
    :cond_1
    move v3, v4

    .line 669
    goto :goto_1
.end method

.method public put(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 413
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 414
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    .line 416
    .local v0, "result":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error saving data"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 417
    return v0

    .line 416
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # I
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 439
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 440
    if-nez p3, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 442
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    int-to-long v4, p2

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v1, p1, v4, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v0

    .line 444
    .local v0, "result":Z
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 445
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 440
    goto :goto_0

    .restart local v0    # "result":Z
    :cond_1
    move v2, v3

    .line 444
    goto :goto_1
.end method

.method public put(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 525
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 526
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-static {p2, p3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    .line 528
    .local v0, "result":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Error saving data"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 529
    return v0

    .line 528
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;JLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # J
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 551
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 552
    if-nez p4, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 554
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-static {p2, p3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v1, p1, v4, p4}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v0

    .line 556
    .local v0, "result":Z
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 557
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 552
    goto :goto_0

    .restart local v0    # "result":Z
    :cond_1
    move v2, v3

    .line 556
    goto :goto_1
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 189
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 190
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 192
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, p1, v4}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    .line 193
    .local v0, "result":Z
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 194
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 190
    goto :goto_0

    .restart local v0    # "result":Z
    :cond_1
    move v2, v3

    .line 193
    goto :goto_1
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 217
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 218
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 220
    if-nez p3, :cond_1

    move v1, v2

    :goto_1
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 222
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, p1, v4, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v0

    .line 224
    .local v0, "result":Z
    if-nez v0, :cond_2

    :goto_2
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 225
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 218
    goto :goto_0

    :cond_1
    move v1, v3

    .line 220
    goto :goto_1

    .restart local v0    # "result":Z
    :cond_2
    move v2, v3

    .line 224
    goto :goto_2
.end method

.method public put(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    .line 762
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 763
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 764
    .local v0, "dString":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v1

    .line 765
    .local v1, "result":Z
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Error saving data"

    invoke-direct {p0, v2, v3}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 766
    return v1

    .line 765
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # Z
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 788
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 789
    if-nez p3, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: password is null!"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 791
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 792
    .local v0, "dString":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v1

    .line 794
    .local v1, "result":Z
    if-nez v1, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 795
    return v1

    .end local v0    # "dString":Ljava/lang/String;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 789
    goto :goto_0

    .restart local v0    # "dString":Ljava/lang/String;
    .restart local v1    # "result":Z
    :cond_1
    move v3, v4

    .line 794
    goto :goto_1
.end method

.method public put(Ljava/lang/String;[B)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 246
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 247
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 249
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    .line 250
    .local v0, "result":Z
    if-nez v0, :cond_1

    :goto_1
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 251
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 247
    goto :goto_0

    .restart local v0    # "result":Z
    :cond_1
    move v2, v3

    .line 250
    goto :goto_1
.end method

.method public put(Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 5
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [B
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 274
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 275
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Error: data block is null"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 277
    if-nez p3, :cond_1

    move v1, v2

    :goto_1
    const-string v4, "Error: password is null!"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 279
    sget-object v1, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v0

    .line 280
    .local v0, "result":Z
    if-nez v0, :cond_2

    :goto_2
    const-string v1, "Error saving data"

    invoke-direct {p0, v2, v1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 281
    return v0

    .end local v0    # "result":Z
    :cond_0
    move v1, v3

    .line 275
    goto :goto_0

    :cond_1
    move v1, v3

    .line 277
    goto :goto_1

    .restart local v0    # "result":Z
    :cond_2
    move v2, v3

    .line 280
    goto :goto_2
.end method

.method public put(Ljava/lang/String;[C)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 76
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 78
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 79
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asCharBuffer()Ljava/nio/CharBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/CharBuffer;->put([C)Ljava/nio/CharBuffer;

    .line 80
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v1

    .line 81
    .local v1, "result":Z
    if-nez v1, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 82
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 76
    goto :goto_0

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_1
    move v3, v4

    .line 81
    goto :goto_1
.end method

.method public put(Ljava/lang/String;[CLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [C
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 106
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 107
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 109
    if-nez p3, :cond_1

    move v2, v3

    :goto_1
    const-string v5, "Error: password is null!"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 111
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 112
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asCharBuffer()Ljava/nio/CharBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/CharBuffer;->put([C)Ljava/nio/CharBuffer;

    .line 113
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v1

    .line 114
    .local v1, "result":Z
    if-nez v1, :cond_2

    :goto_2
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 115
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 107
    goto :goto_0

    :cond_1
    move v2, v4

    .line 109
    goto :goto_1

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_2
    move v3, v4

    .line 114
    goto :goto_2
.end method

.method public put(Ljava/lang/String;[D)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 578
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 579
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 581
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 582
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asDoubleBuffer()Ljava/nio/DoubleBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/DoubleBuffer;->put([D)Ljava/nio/DoubleBuffer;

    .line 583
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v1

    .line 584
    .local v1, "result":Z
    if-nez v1, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 585
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 579
    goto :goto_0

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_1
    move v3, v4

    .line 584
    goto :goto_1
.end method

.method public put(Ljava/lang/String;[DLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [D
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 608
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 609
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 611
    if-nez p3, :cond_1

    move v2, v3

    :goto_1
    const-string v5, "Error: password is null!"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 613
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 614
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asDoubleBuffer()Ljava/nio/DoubleBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/DoubleBuffer;->put([D)Ljava/nio/DoubleBuffer;

    .line 615
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v1

    .line 616
    .local v1, "result":Z
    if-nez v1, :cond_2

    :goto_2
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 617
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 609
    goto :goto_0

    :cond_1
    move v2, v4

    .line 611
    goto :goto_1

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_2
    move v3, v4

    .line 616
    goto :goto_2
.end method

.method public put(Ljava/lang/String;[I)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 354
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 355
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 357
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 358
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 359
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v1

    .line 360
    .local v1, "result":Z
    if-nez v1, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 361
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 355
    goto :goto_0

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_1
    move v3, v4

    .line 360
    goto :goto_1
.end method

.method public put(Ljava/lang/String;[ILjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [I
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 384
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 385
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 387
    if-nez p3, :cond_1

    move v2, v3

    :goto_1
    const-string v5, "Error: password is null!"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 389
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 390
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 391
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v1

    .line 392
    .local v1, "result":Z
    if-nez v1, :cond_2

    :goto_2
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 393
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 385
    goto :goto_0

    :cond_1
    move v2, v4

    .line 387
    goto :goto_1

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_2
    move v3, v4

    .line 392
    goto :goto_2
.end method

.method public put(Ljava/lang/String;[J)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 466
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 467
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 469
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 470
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asLongBuffer()Ljava/nio/LongBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/LongBuffer;->put([J)Ljava/nio/LongBuffer;

    .line 471
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v1

    .line 472
    .local v1, "result":Z
    if-nez v1, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 473
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 467
    goto :goto_0

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_1
    move v3, v4

    .line 472
    goto :goto_1
.end method

.method public put(Ljava/lang/String;[JLjava/lang/String;)Z
    .locals 6
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [J
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 496
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 497
    if-nez p2, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Error: data block is null"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 499
    if-nez p3, :cond_1

    move v2, v3

    :goto_1
    const-string v5, "Error: password is null!"

    invoke-direct {p0, v2, v5}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 501
    array-length v2, p2

    mul-int/lit8 v2, v2, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 502
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asLongBuffer()Ljava/nio/LongBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/nio/LongBuffer;->put([J)Ljava/nio/LongBuffer;

    .line 503
    sget-object v2, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, p1, v5, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v1

    .line 504
    .local v1, "result":Z
    if-nez v1, :cond_2

    :goto_2
    const-string v2, "Error saving data"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 505
    return v1

    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "result":Z
    :cond_0
    move v2, v4

    .line 497
    goto :goto_0

    :cond_1
    move v2, v4

    .line 499
    goto :goto_1

    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local v1    # "result":Z
    :cond_2
    move v3, v4

    .line 504
    goto :goto_2
.end method

.method public put(Ljava/lang/String;[Z)Z
    .locals 7
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 691
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 692
    if-nez p2, :cond_0

    move v3, v4

    :goto_0
    const-string v6, "Error: data block is null"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 694
    array-length v3, p2

    new-array v0, v3, [B

    .line 695
    .local v0, "dataInBytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_2

    .line 696
    aget-boolean v3, p2, v1

    if-eqz v3, :cond_1

    .line 697
    aput-byte v4, v0, v1

    .line 695
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "dataInBytes":[B
    .end local v1    # "i":I
    :cond_0
    move v3, v5

    .line 692
    goto :goto_0

    .line 699
    .restart local v0    # "dataInBytes":[B
    .restart local v1    # "i":I
    :cond_1
    aput-byte v5, v0, v1

    goto :goto_2

    .line 702
    :cond_2
    sget-object v3, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v3, p1, v0}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v2

    .line 703
    .local v2, "result":Z
    if-nez v2, :cond_3

    :goto_3
    const-string v3, "Error saving data"

    invoke-direct {p0, v4, v3}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 704
    return v2

    :cond_3
    move v4, v5

    .line 703
    goto :goto_3
.end method

.method public put(Ljava/lang/String;[ZLjava/lang/String;)Z
    .locals 7
    .param p1, "dataName"    # Ljava/lang/String;
    .param p2, "dataBlock"    # [Z
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 727
    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/SecureStorage;->throwException(Ljava/lang/String;)V

    .line 728
    if-nez p2, :cond_0

    move v3, v4

    :goto_0
    const-string v6, "Error: data block is null"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 730
    if-nez p3, :cond_1

    move v3, v4

    :goto_1
    const-string v6, "Error: password is null!"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 732
    array-length v3, p2

    new-array v0, v3, [B

    .line 733
    .local v0, "dataInBytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    array-length v3, v0

    if-ge v1, v3, :cond_3

    .line 734
    aget-boolean v3, p2, v1

    if-eqz v3, :cond_2

    .line 735
    aput-byte v4, v0, v1

    .line 733
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "dataInBytes":[B
    .end local v1    # "i":I
    :cond_0
    move v3, v5

    .line 728
    goto :goto_0

    :cond_1
    move v3, v5

    .line 730
    goto :goto_1

    .line 737
    .restart local v0    # "dataInBytes":[B
    .restart local v1    # "i":I
    :cond_2
    aput-byte v5, v0, v1

    goto :goto_3

    .line 740
    :cond_3
    sget-object v3, Lcom/sec/android/securestorage/SecureStorage;->secureStorageJNI:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v3, p1, v0, p3}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v2

    .line 741
    .local v2, "result":Z
    if-nez v2, :cond_4

    :goto_4
    const-string v3, "Error saving data"

    invoke-direct {p0, v4, v3}, Lcom/sec/android/securestorage/SecureStorage;->throwException(ZLjava/lang/String;)V

    .line 742
    return v2

    :cond_4
    move v4, v5

    .line 741
    goto :goto_4
.end method
