.class public Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
.super Ljava/lang/Exception;
.source "SecureStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/securestorage/SecureStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecureStorageException"
.end annotation


# static fields
.field public static final ERROR_INPUT_DATA_NAME:Ljava/lang/String; = "Error input data name"

.field public static final ERROR_NULL_DATA_BLOCK:Ljava/lang/String; = "Error: data block is null"

.field public static final ERROR_NULL_PASSWORD:Ljava/lang/String; = "Error: password is null!"

.field public static final ERROR_PUT_DATA:Ljava/lang/String; = "Error saving data"

.field public static final SECURE_STORAGE_ERROR_ENCRYPTION:Ljava/lang/String; = "Encryption error"

.field public static final SECURE_STORAGE_ERROR_INPUT_DATA:Ljava/lang/String; = "Error: input data are incorrect"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1657
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 1658
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 1668
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1669
    return-void
.end method
