.class public Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;
.super Ljava/lang/Object;
.source "DsWallpaperSetting.java"


# static fields
.field static final isMultiSIM:Z

.field static final isSkipCTC:Z

.field static final isSkipMultiSim:Z

.field private static mSetAsSim1:Z

.field private static mSetAsSim2:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 23
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 26
    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ctc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSkipCTC:Z

    .line 27
    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "trlteduos"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSkipMultiSim:Z

    .line 28
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v1

    const-string v2, "SEC_FLOATING_FEATURE_COMMON_USE_MULTISIM"

    invoke-virtual {v1, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSkipCTC:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSkipMultiSim:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    return-void
.end method

.method private static clearSetting()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    if-eqz v0, :cond_0

    .line 44
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 45
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 47
    :cond_0
    return-void
.end method

.method private static getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    if-eqz v1, :cond_0

    .line 33
    const-string v0, "WallpaperChooserDsWallpaperSetting"

    .line 35
    .local v0, "TAG":Ljava/lang/String;
    const-string v1, "WallpaperChooserDsWallpaperSetting"

    .line 37
    :goto_0
    return-object v1

    .end local v0    # "TAG":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSetAsSIM1()Z
    .locals 1

    .prologue
    .line 303
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    return v0
.end method

.method public static isSetAsSIM2()Z
    .locals 1

    .prologue
    .line 307
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    return v0
.end method

.method public static selectSimByNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "isSelected":Z
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    if-eqz v1, :cond_0

    .line 53
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->clearSetting()V

    .line 54
    invoke-static {p0}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getSimState(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 55
    const/4 v0, 0x0

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getSimState(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 57
    invoke-static {p0}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getCurrentSimSlot(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2

    .line 58
    sput-boolean v3, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 59
    sput-boolean v4, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 64
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 61
    :cond_2
    sput-boolean v4, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 62
    sput-boolean v3, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_1

    .line 66
    :cond_3
    sput-boolean v3, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 67
    sput-boolean v4, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 68
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static selectSimByUser(I)V
    .locals 3
    .param p0, "command"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 76
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->clearSetting()V

    .line 78
    if-nez p0, :cond_1

    .line 79
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 80
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    if-ne p0, v1, :cond_2

    .line 82
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 83
    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_0

    .line 84
    :cond_2
    const/4 v0, 0x2

    if-ne p0, v0, :cond_3

    .line 85
    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 86
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_0

    .line 88
    :cond_3
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    .line 89
    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    goto :goto_0
.end method

.method public static setAsDsWallpaper(Landroid/content/Context;Landroid/content/res/Resources;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resID"    # I

    .prologue
    const/4 v4, 0x1

    .line 153
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    if-eqz v2, :cond_0

    .line 154
    const/4 v1, 0x0

    .line 155
    .local v1, "stream":Ljava/io/InputStream;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 196
    .end local v1    # "stream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-void

    .line 159
    .restart local v1    # "stream":Ljava/io/InputStream;
    :cond_1
    :try_start_0
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    if-ne v2, v4, :cond_2

    .line 160
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_2

    .line 162
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;I)V

    .line 163
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 167
    :cond_2
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    if-ne v2, v4, :cond_3

    .line 168
    const/4 v1, 0x0

    .line 169
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_3

    .line 171
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;I)V

    .line 172
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 176
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    if-nez v2, :cond_4

    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    if-nez v2, :cond_4

    .line 177
    const/4 v1, 0x0

    .line 178
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_4

    .line 180
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 181
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :cond_4
    if-eqz v1, :cond_0

    .line 189
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "stream close error setAsDsWallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 184
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 188
    if-eqz v1, :cond_0

    .line 189
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 191
    :catch_2
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "stream close error setAsDsWallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 187
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 188
    if-eqz v1, :cond_5

    .line 189
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 193
    :cond_5
    :goto_1
    throw v2

    .line 191
    :catch_3
    move-exception v0

    .line 192
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "stream close error setAsDsWallpaper"

    invoke-static {v3, v4, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static setAsDsWallpaper(Landroid/content/Context;Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mInStream"    # Ljava/io/InputStream;
    .param p2, "mInStream2"    # Ljava/io/InputStream;

    .prologue
    const/4 v5, 0x1

    .line 254
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isMultiSIM:Z

    if-eqz v2, :cond_0

    .line 255
    move-object v1, p1

    .line 256
    .local v1, "stream":Ljava/io/InputStream;
    if-nez p0, :cond_1

    .line 300
    .end local v1    # "stream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-void

    .line 261
    .restart local v1    # "stream":Ljava/io/InputStream;
    :cond_1
    :try_start_0
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    if-ne v2, v5, :cond_2

    .line 262
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSetAsSim1 stream:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    if-eqz v1, :cond_2

    .line 264
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;I)V

    .line 265
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 269
    :cond_2
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    if-ne v2, v5, :cond_4

    .line 271
    if-eqz p2, :cond_3

    .line 272
    move-object v1, p2

    .line 274
    :cond_3
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSetAsSim2 stream:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mInStream2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    if-eqz v1, :cond_4

    .line 277
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;I)V

    .line 278
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 282
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim1:Z

    if-nez v2, :cond_5

    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->mSetAsSim2:Z

    if-nez v2, :cond_5

    .line 283
    if-eqz v1, :cond_5

    .line 284
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 285
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :cond_5
    if-eqz v1, :cond_0

    .line 293
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "stream close error setAsDsWallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 288
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 289
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 292
    if-eqz v1, :cond_0

    .line 293
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 295
    :catch_2
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "stream close error setAsDsWallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 291
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 292
    if-eqz v1, :cond_6

    .line 293
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 297
    :cond_6
    :goto_1
    throw v2

    .line 295
    :catch_3
    move-exception v0

    .line 296
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "stream close error setAsDsWallpaper"

    invoke-static {v3, v4, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
