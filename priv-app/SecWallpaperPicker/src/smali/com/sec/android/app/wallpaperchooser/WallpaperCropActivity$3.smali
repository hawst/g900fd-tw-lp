.class Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;
.super Ljava/lang/Object;
.source "WallpaperCropActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;

.field final synthetic val$finishActivityWhenDone:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;Z)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;

    iput-boolean p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;->val$finishActivityWhenDone:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;->val$finishActivityWhenDone:Z

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setTransparency(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$000(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setResult(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->finish()V

    .line 360
    :cond_0
    return-void
.end method
