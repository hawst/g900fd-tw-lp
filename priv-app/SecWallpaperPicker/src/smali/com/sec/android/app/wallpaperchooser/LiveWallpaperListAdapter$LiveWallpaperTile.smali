.class public Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "LiveWallpaperListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LiveWallpaperTile"
.end annotation


# instance fields
.field private mInfo:Landroid/app/WallpaperInfo;

.field private mThumbnail:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/app/WallpaperInfo;Landroid/content/Intent;)V
    .locals 0
    .param p1, "thumbnail"    # Landroid/graphics/drawable/Drawable;
    .param p2, "info"    # Landroid/app/WallpaperInfo;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;->mThumbnail:Landroid/graphics/drawable/Drawable;

    .line 118
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;->mInfo:Landroid/app/WallpaperInfo;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;->mThumbnail:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;)Landroid/app/WallpaperInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;->mInfo:Landroid/app/WallpaperInfo;

    return-object v0
.end method


# virtual methods
.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 3
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 123
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.service.wallpaper.CHANGE_LIVE_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 124
    .local v0, "preview":Landroid/content/Intent;
    const-string v1, "android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT"

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;->mInfo:Landroid/app/WallpaperInfo;

    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 126
    sget v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-nez v1, :cond_1

    .line 127
    const-string v1, "SET_LOCKSCREEN_WALLPAPER"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 131
    :cond_0
    :goto_0
    const/4 v1, 0x7

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 132
    return-void

    .line 128
    :cond_1
    sget v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 129
    const-string v1, "SET_LOCKSCREEN_WALLPAPER"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method
