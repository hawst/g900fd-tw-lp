.class public Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;
.super Ljava/lang/Object;
.source "HelpAnimatedDialog.java"


# static fields
.field private static mGoalAnimationView:Landroid/view/View;


# instance fields
.field private flashCount:I

.field private mFadingAnimation:Landroid/view/animation/Animation;

.field private mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFlashingAnimation:Landroid/view/animation/Animation;

.field private mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mImageTouch:Landroid/view/View$OnTouchListener;

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I

    .line 61
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$1;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 77
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 95
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$3;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 111
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$4;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mImageTouch:Landroid/view/View$OnTouchListener;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I

    .line 61
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$1;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 77
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 95
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$3;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 111
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$4;-><init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mImageTouch:Landroid/view/View$OnTouchListener;

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->init(Landroid/content/Context;Landroid/view/View;)V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$100()Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I

    return v0
.end method

.method static synthetic access$208(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private init(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 45
    sput-object p2, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    .line 46
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mImageTouch:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 47
    const v0, 0x7f050002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 51
    :cond_0
    const/high16 v0, 0x7f050000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 55
    :cond_1
    const v0, 0x7f050001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 59
    :cond_2
    return-void
.end method


# virtual methods
.method public start()V
    .locals 2

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 133
    return-void
.end method

.method public startZoom(FFLandroid/view/View;)V
    .locals 9
    .param p1, "pivotX"    # F
    .param p2, "pivotY"    # F
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const v1, 0x3f4ccccd    # 0.8f

    .line 136
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, p1

    move v7, v5

    move v8, p2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 139
    .local v0, "animation":Landroid/view/animation/ScaleAnimation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 140
    invoke-virtual {p3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 141
    invoke-virtual {v0}, Landroid/view/animation/ScaleAnimation;->start()V

    .line 142
    return-void
.end method
