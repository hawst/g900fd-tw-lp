.class public Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "BuiltInCustomerWallpapersAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomerWallpaperTile"
.end annotation


# instance fields
.field private mFullPath:Ljava/lang/String;

.field private mImageName:Ljava/lang/String;

.field private mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

.field private mThumb:Landroid/graphics/drawable/Drawable;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 2
    .param p1, "fullPath"    # Ljava/lang/String;
    .param p2, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p3, "imageName"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    .line 57
    iput-object p3, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mImageName:Ljava/lang/String;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mFullPath:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 60
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mFullPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mUri:Landroid/net/Uri;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mImageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mThumb:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public isNamelessWallpaper()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 5
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 64
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 74
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;

    move-result-object v1

    .line 68
    .local v1, "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mUri:Landroid/net/Uri;

    invoke-static {p1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    .line 70
    .local v0, "rotation":I
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    if-nez v2, :cond_1

    .line 71
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mUri:Landroid/net/Uri;

    const/16 v4, 0x400

    invoke-direct {v2, p1, v3, v4, v0}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/Context;Landroid/net/Uri;II)V

    iput-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    .line 72
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 73
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    goto :goto_0
.end method

.method public onDelete(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 90
    return-void
.end method

.method public onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 4
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 78
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 86
    :goto_0
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x1

    .line 82
    .local v0, "finishActivityWhenDone":Z
    const/4 v1, 0x1

    .line 85
    .local v1, "isNoCrop":Z
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mUri:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;ZZ)V

    goto :goto_0
.end method
