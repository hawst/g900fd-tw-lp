.class public Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
.super Landroid/os/AsyncTask;
.source "WallpaperCropActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "BitmapCropTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mCropBounds:Landroid/graphics/RectF;

.field mCroppedBitmap:Landroid/graphics/Bitmap;

.field mInFilePath:Ljava/lang/String;

.field mInImageBytes:[B

.field mInResId:I

.field mInStream:Ljava/io/InputStream;

.field mInStream2:Ljava/io/InputStream;

.field mInUri:Landroid/net/Uri;

.field mNoCrop:Z

.field mOnBitmapCroppedHandler:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;

.field mOnEndRunnable:Ljava/lang/Runnable;

.field mOutHeight:I

.field mOutWidth:I

.field mOutputFormat:Ljava/lang/String;

.field mResources:Landroid/content/res/Resources;

.field mRotation:I

.field mSaveCroppedBitmap:Z

.field mSavedImagePath:Ljava/lang/String;

.field mSetWallpaper:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "inResId"    # I
    .param p4, "cropBounds"    # Landroid/graphics/RectF;
    .param p5, "rotation"    # I
    .param p6, "outWidth"    # I
    .param p7, "outHeight"    # I
    .param p8, "setWallpaper"    # Z
    .param p9, "saveCroppedBitmap"    # Z
    .param p10, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 451
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    .line 410
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 416
    const-string v0, "jpg"

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    .line 452
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    .line 453
    iput p3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    .line 454
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mResources:Landroid/content/res/Resources;

    move-object v0, p0

    move-object v1, p4

    move v2, p5

    move v3, p6

    move v4, p7

    move/from16 v5, p8

    move/from16 v6, p9

    move-object/from16 v7, p10

    .line 455
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 456
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "inUri"    # Landroid/net/Uri;
    .param p3, "cropBounds"    # Landroid/graphics/RectF;
    .param p4, "rotation"    # I
    .param p5, "outWidth"    # I
    .param p6, "outHeight"    # I
    .param p7, "setWallpaper"    # Z
    .param p8, "saveCroppedBitmap"    # Z
    .param p9, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 443
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    .line 410
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 416
    const-string v0, "jpg"

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    .line 444
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    .line 445
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    .line 446
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 447
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "cropBounds"    # Landroid/graphics/RectF;
    .param p4, "rotation"    # I
    .param p5, "outWidth"    # I
    .param p6, "outHeight"    # I
    .param p7, "setWallpaper"    # Z
    .param p8, "saveCroppedBitmap"    # Z
    .param p9, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 427
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    .line 410
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 416
    const-string v0, "jpg"

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    .line 428
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    .line 429
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSavedImagePath:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    .line 431
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 432
    return-void
.end method

.method public constructor <init>([BLandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8
    .param p1, "imageBytes"    # [B
    .param p2, "cropBounds"    # Landroid/graphics/RectF;
    .param p3, "rotation"    # I
    .param p4, "outWidth"    # I
    .param p5, "outHeight"    # I
    .param p6, "setWallpaper"    # Z
    .param p7, "saveCroppedBitmap"    # Z
    .param p8, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 436
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    .line 410
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 416
    const-string v0, "jpg"

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    .line 437
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    move-object/from16 v7, p8

    .line 438
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 439
    return-void
.end method

.method private init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 0
    .param p1, "cropBounds"    # Landroid/graphics/RectF;
    .param p2, "rotation"    # I
    .param p3, "outWidth"    # I
    .param p4, "outHeight"    # I
    .param p5, "setWallpaper"    # Z
    .param p6, "saveCroppedBitmap"    # Z
    .param p7, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 460
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 461
    iput p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    .line 462
    iput p3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    .line 463
    iput p4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    .line 464
    iput-boolean p5, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    .line 465
    iput-boolean p6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSaveCroppedBitmap:Z

    .line 466
    iput-object p7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    .line 467
    return-void
.end method

.method private regenerateInputStream()V
    .locals 4

    .prologue
    .line 483
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    if-nez v1, :cond_0

    .line 484
    const-string v1, "Wallpapers.CropActivity"

    const-string v2, "cannot read original file, no input URI, resource ID, or image byte array given"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 488
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream2:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 490
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 491
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    .line 493
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream2:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Ljava/io/FileNotFoundException;
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    if-eqz v1, :cond_4

    .line 511
    const-string v1, "Wallpapers.CropActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot read file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 495
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 496
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    .line 497
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream2:Ljava/io/InputStream;

    goto :goto_0

    .line 498
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    if-eqz v1, :cond_3

    .line 499
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    .line 501
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream2:Ljava/io/InputStream;

    goto/16 :goto_0

    .line 504
    :cond_3
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    .line 506
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream2:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 513
    .restart local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    const-string v1, "Wallpapers.CropActivity"

    const-string v2, "cannot read file: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method


# virtual methods
.method public cropBitmap()Z
    .locals 45

    .prologue
    .line 541
    const/16 v17, 0x0

    .line 542
    .local v17, "failure":Z
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, -0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_0

    .line 543
    const-string v41, "Wallpapers.CropActivity"

    const-string v42, "cannot write stream to wallpaper, plz check currentMode."

    invoke-static/range {v41 .. v42}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    const/16 v41, 0x0

    .line 815
    :goto_0
    return v41

    .line 546
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()V

    .line 548
    const/16 v40, 0x0

    .line 550
    .local v40, "wallpaperManager":Landroid/app/WallpaperManager;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v41, v0

    if-eqz v41, :cond_1

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v40

    .line 553
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v41, v0

    if-eqz v41, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mNoCrop:Z

    move/from16 v41, v0

    if-eqz v41, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    move-object/from16 v41, v0

    if-eqz v41, :cond_d

    if-eqz v40, :cond_d

    .line 554
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-nez v41, :cond_3

    .line 555
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setHomescreenWallpaperFromInputStream(Landroid/app/WallpaperManager;)Z

    move-result v17

    .line 601
    :cond_2
    :goto_1
    if-nez v17, :cond_c

    const/16 v41, 0x1

    goto :goto_0

    .line 556
    :cond_3
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_4

    .line 557
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setLockScreenWallpaperFromInputStream()Z

    move-result v17

    goto :goto_1

    .line 558
    :cond_4
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, 0x2

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_7

    .line 559
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setHomescreenWallpaperFromInputStream(Landroid/app/WallpaperManager;)Z

    move-result v17

    .line 560
    if-nez v17, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setLockScreenWallpaperFromInputStream()Z

    move-result v41

    if-eqz v41, :cond_6

    :cond_5
    const/16 v17, 0x1

    :goto_2
    goto :goto_1

    :cond_6
    const/16 v17, 0x0

    goto :goto_2

    .line 561
    :cond_7
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, 0x3

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_2

    .line 562
    const/16 v28, 0x0

    .line 563
    .local v28, "os":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 564
    .local v5, "b":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSavedImagePath:Ljava/lang/String;

    move-object/from16 v41, v0

    if-eqz v41, :cond_9

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSavedImagePath:Ljava/lang/String;

    move-object/from16 v41, v0

    invoke-static/range {v41 .. v41}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 570
    :cond_8
    :goto_3
    if-nez v5, :cond_a

    .line 571
    const-string v41, "Wallpapers.CropActivity"

    const-string v42, "cannot load Bitmap, please check save image"

    invoke-static/range {v41 .. v42}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 566
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v41, v0

    if-eqz v41, :cond_8

    .line 567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_3

    .line 575
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v41

    const-string v42, "sview_bg_wallpaper_path"

    invoke-static/range {v41 .. v42}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 576
    .local v26, "mPath":Ljava/lang/String;
    if-nez v26, :cond_b

    .line 577
    const-string v26, "/storage/emulated/0/Android/data/com.sec.android.sviewcover/files/Pictures/cover_wallpaper.jpg"

    .line 581
    :goto_4
    new-instance v37, Ljava/io/File;

    move-object/from16 v0, v37

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 582
    .local v37, "sview_wallpaper":Ljava/io/File;
    const/16 v29, 0x0

    .line 585
    .local v29, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual/range {v37 .. v37}, Ljava/io/File;->createNewFile()Z

    .line 586
    new-instance v30, Ljava/io/FileOutputStream;

    move-object/from16 v0, v30

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    .end local v29    # "out":Ljava/io/OutputStream;
    .local v30, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v41, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v42, 0x64

    move-object/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v30

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    move-object/from16 v29, v30

    .line 592
    .end local v30    # "out":Ljava/io/OutputStream;
    .restart local v29    # "out":Ljava/io/OutputStream;
    :goto_5
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "file://"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 593
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v41

    const-string v42, "sview_bg_wallpaper_path"

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v41 .. v43}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 596
    new-instance v19, Landroid/content/Intent;

    const-string v41, "com.sec.android.sviewcover.CHANGE_COVER_BACKGROUND"

    move-object/from16 v0, v19

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 597
    .local v19, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 599
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_1

    .line 579
    .end local v19    # "intent":Landroid/content/Intent;
    .end local v29    # "out":Ljava/io/OutputStream;
    .end local v37    # "sview_wallpaper":Ljava/io/File;
    :cond_b
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "/cover_wallpaper.jpg"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_4

    .line 588
    .restart local v29    # "out":Ljava/io/OutputStream;
    .restart local v37    # "sview_wallpaper":Ljava/io/File;
    :catch_0
    move-exception v15

    .line 589
    .local v15, "e":Ljava/lang/Exception;
    :goto_6
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 601
    .end local v5    # "b":Landroid/graphics/Bitmap;
    .end local v15    # "e":Ljava/lang/Exception;
    .end local v26    # "mPath":Ljava/lang/String;
    .end local v28    # "os":Ljava/io/FileOutputStream;
    .end local v29    # "out":Ljava/io/OutputStream;
    .end local v37    # "sview_wallpaper":Ljava/io/File;
    :cond_c
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 604
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    move-object/from16 v41, v0

    if-eqz v41, :cond_1f

    .line 606
    new-instance v35, Landroid/graphics/Rect;

    invoke-direct/range {v35 .. v35}, Landroid/graphics/Rect;-><init>()V

    .line 607
    .local v35, "roundedTrueCrop":Landroid/graphics/Rect;
    new-instance v33, Landroid/graphics/Matrix;

    invoke-direct/range {v33 .. v33}, Landroid/graphics/Matrix;-><init>()V

    .line 608
    .local v33, "rotateMatrix":Landroid/graphics/Matrix;
    new-instance v20, Landroid/graphics/Matrix;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Matrix;-><init>()V

    .line 609
    .local v20, "inverseRotateMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v41, v0

    if-lez v41, :cond_f

    .line 610
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    move-object/from16 v0, v33

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 611
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v41, v0

    move/from16 v0, v41

    neg-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    move-object/from16 v0, v20

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 614
    new-instance v41, Landroid/graphics/RectF;

    move-object/from16 v0, v41

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 616
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->getImageBounds()Landroid/graphics/Point;

    move-result-object v6

    .line 618
    .local v6, "bounds":Landroid/graphics/Point;
    if-nez v6, :cond_e

    .line 619
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 621
    :cond_e
    const/16 v41, 0x2

    move/from16 v0, v41

    new-array v0, v0, [F

    move-object/from16 v34, v0

    const/16 v41, 0x0

    iget v0, v6, Landroid/graphics/Point;->x:I

    move/from16 v42, v0

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v42, v0

    aput v42, v34, v41

    const/16 v41, 0x1

    iget v0, v6, Landroid/graphics/Point;->y:I

    move/from16 v42, v0

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v42, v0

    aput v42, v34, v41

    .line 624
    .local v34, "rotatedBounds":[F
    invoke-virtual/range {v33 .. v34}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 625
    const/16 v41, 0x0

    const/16 v42, 0x0

    aget v42, v34, v42

    invoke-static/range {v42 .. v42}, Ljava/lang/Math;->abs(F)F

    move-result v42

    aput v42, v34, v41

    .line 626
    const/16 v41, 0x1

    const/16 v42, 0x1

    aget v42, v34, v42

    invoke-static/range {v42 .. v42}, Ljava/lang/Math;->abs(F)F

    move-result v42

    aput v42, v34, v41

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget v42, v34, v42

    move/from16 v0, v42

    neg-float v0, v0

    move/from16 v42, v0

    const/high16 v43, 0x40000000    # 2.0f

    div-float v42, v42, v43

    const/16 v43, 0x1

    aget v43, v34, v43

    move/from16 v0, v43

    neg-float v0, v0

    move/from16 v43, v0

    const/high16 v44, 0x40000000    # 2.0f

    div-float v43, v43, v44

    invoke-virtual/range {v41 .. v43}, Landroid/graphics/RectF;->offset(FF)V

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    iget v0, v6, Landroid/graphics/Point;->x:I

    move/from16 v42, v0

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v42, v0

    const/high16 v43, 0x40000000    # 2.0f

    div-float v42, v42, v43

    iget v0, v6, Landroid/graphics/Point;->y:I

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    const/high16 v44, 0x40000000    # 2.0f

    div-float v43, v43, v44

    invoke-virtual/range {v41 .. v43}, Landroid/graphics/RectF;->offset(FF)V

    .line 632
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()V

    .line 635
    .end local v6    # "bounds":Landroid/graphics/Point;
    .end local v34    # "rotatedBounds":[F
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 637
    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v41

    if-lez v41, :cond_10

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->height()I

    move-result v41

    if-gtz v41, :cond_11

    .line 638
    :cond_10
    const-string v41, "Wallpapers.CropActivity"

    const-string v42, "crop has bad values for full size image"

    invoke-static/range {v41 .. v42}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    const/16 v17, 0x1

    .line 640
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 644
    :cond_11
    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v41

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v42, v0

    div-int v41, v41, v42

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->height()I

    move-result v42

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v43, v0

    div-int v42, v42, v43

    invoke-static/range {v41 .. v42}, Ljava/lang/Math;->min(II)I

    move-result v36

    .line 647
    .local v36, "scaleDownSampleSize":I
    const/4 v13, 0x0

    .line 649
    .local v13, "decoder":Landroid/graphics/BitmapRegionDecoder;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    move-object/from16 v41, v0

    const/16 v42, 0x1

    invoke-static/range {v41 .. v42}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v13

    .line 657
    :goto_7
    const/4 v11, 0x0

    .line 658
    .local v11, "crop":Landroid/graphics/Bitmap;
    const/16 v18, 0x0

    .line 659
    .local v18, "fullSize":Landroid/graphics/Bitmap;
    if-eqz v13, :cond_13

    .line 661
    new-instance v27, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 662
    .local v27, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v41, 0x1

    move/from16 v0, v36

    move/from16 v1, v41

    if-le v0, v1, :cond_12

    .line 663
    move/from16 v0, v36

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 664
    :cond_12
    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-virtual {v13, v0, v1}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 665
    invoke-virtual {v13}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 668
    .end local v27    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_13
    if-nez v11, :cond_16

    if-eqz v36, :cond_16

    .line 670
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()V

    .line 672
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    move-object/from16 v41, v0

    if-eqz v41, :cond_15

    .line 673
    new-instance v27, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 674
    .restart local v27    # "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v41, 0x1

    move/from16 v0, v36

    move/from16 v1, v41

    if-le v0, v1, :cond_14

    .line 675
    move/from16 v0, v36

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 677
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 679
    .end local v27    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_15
    if-eqz v18, :cond_16

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v42, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v43, v0

    div-float v42, v42, v43

    move/from16 v0, v42

    move-object/from16 v1, v41

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 681
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v42, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v43, v0

    div-float v42, v42, v43

    move/from16 v0, v42

    move-object/from16 v1, v41

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 682
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v42, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v43, v0

    div-float v42, v42, v43

    move/from16 v0, v42

    move-object/from16 v1, v41

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v42, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v43, v0

    div-float v42, v42, v43

    move/from16 v0, v42

    move-object/from16 v1, v41

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 686
    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v41, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v42, v0

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v43

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->height()I

    move-result v44

    move-object/from16 v0, v18

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-static {v0, v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 693
    :cond_16
    if-nez v11, :cond_18

    .line 694
    const-string v41, "Wallpapers.CropActivity"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "cannot decode file: "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v41 .. v42}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    const/16 v17, 0x1

    .line 696
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 650
    .end local v11    # "crop":Landroid/graphics/Bitmap;
    .end local v18    # "fullSize":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v15

    .line 651
    .local v15, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v41, v0

    if-eqz v41, :cond_17

    .line 652
    const-string v41, "Wallpapers.CropActivity"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "cannot open region decoder for file: "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    invoke-static {v0, v1, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_7

    .line 654
    :cond_17
    const-string v41, "Wallpapers.CropActivity"

    const-string v42, "cannot open region decoder for file: mInUri is null"

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    invoke-static {v0, v1, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_7

    .line 699
    .end local v15    # "e":Ljava/io/IOException;
    .restart local v11    # "crop":Landroid/graphics/Bitmap;
    .restart local v18    # "fullSize":Landroid/graphics/Bitmap;
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v41, v0

    if-lez v41, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v41, v0

    if-gtz v41, :cond_1a

    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v41, v0

    if-lez v41, :cond_1d

    .line 700
    :cond_1a
    const/16 v41, 0x2

    move/from16 v0, v41

    new-array v14, v0, [F

    const/16 v41, 0x0

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v42

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v42, v0

    aput v42, v14, v41

    const/16 v41, 0x1

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v42

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v42, v0

    aput v42, v14, v41

    .line 703
    .local v14, "dimsAfter":[F
    move-object/from16 v0, v33

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 704
    const/16 v41, 0x0

    const/16 v42, 0x0

    aget v42, v14, v42

    invoke-static/range {v42 .. v42}, Ljava/lang/Math;->abs(F)F

    move-result v42

    aput v42, v14, v41

    .line 705
    const/16 v41, 0x1

    const/16 v42, 0x1

    aget v42, v14, v42

    invoke-static/range {v42 .. v42}, Ljava/lang/Math;->abs(F)F

    move-result v42

    aput v42, v14, v41

    .line 707
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v41, v0

    if-lez v41, :cond_1b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v41, v0

    if-gtz v41, :cond_1c

    .line 708
    :cond_1b
    const/16 v41, 0x0

    aget v41, v14, v41

    invoke-static/range {v41 .. v41}, Ljava/lang/Math;->round(F)I

    move-result v41

    move/from16 v0, v41

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    .line 709
    const/16 v41, 0x1

    aget v41, v14, v41

    invoke-static/range {v41 .. v41}, Ljava/lang/Math;->round(F)I

    move-result v41

    move/from16 v0, v41

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    .line 712
    :cond_1c
    new-instance v12, Landroid/graphics/RectF;

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    aget v43, v14, v43

    const/16 v44, 0x1

    aget v44, v14, v44

    move/from16 v0, v41

    move/from16 v1, v42

    move/from16 v2, v43

    move/from16 v3, v44

    invoke-direct {v12, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 713
    .local v12, "cropRect":Landroid/graphics/RectF;
    new-instance v32, Landroid/graphics/RectF;

    const/16 v41, 0x0

    const/16 v42, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v44, v0

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v43

    move/from16 v4, v44

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 715
    .local v32, "returnRect":Landroid/graphics/RectF;
    new-instance v21, Landroid/graphics/Matrix;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Matrix;-><init>()V

    .line 716
    .local v21, "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v41, v0

    if-nez v41, :cond_20

    .line 717
    sget-object v41, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    move-object/from16 v2, v41

    invoke-virtual {v0, v12, v1, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 735
    :goto_8
    invoke-virtual/range {v32 .. v32}, Landroid/graphics/RectF;->width()F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/RectF;->height()F

    move-result v42

    move/from16 v0, v42

    float-to-int v0, v0

    move/from16 v42, v0

    sget-object v43, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v41 .. v43}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v38

    .line 736
    .local v38, "tmp":Landroid/graphics/Bitmap;
    if-eqz v38, :cond_1d

    .line 737
    new-instance v7, Landroid/graphics/Canvas;

    move-object/from16 v0, v38

    invoke-direct {v7, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 738
    .local v7, "c":Landroid/graphics/Canvas;
    new-instance v31, Landroid/graphics/Paint;

    invoke-direct/range {v31 .. v31}, Landroid/graphics/Paint;-><init>()V

    .line 739
    .local v31, "p":Landroid/graphics/Paint;
    const/16 v41, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 740
    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v7, v11, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 741
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 742
    const/4 v11, 0x0

    .line 743
    move-object/from16 v11, v38

    .line 744
    if-eqz v18, :cond_1d

    .line 745
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    .line 749
    .end local v7    # "c":Landroid/graphics/Canvas;
    .end local v12    # "cropRect":Landroid/graphics/RectF;
    .end local v14    # "dimsAfter":[F
    .end local v21    # "m":Landroid/graphics/Matrix;
    .end local v31    # "p":Landroid/graphics/Paint;
    .end local v32    # "returnRect":Landroid/graphics/RectF;
    .end local v38    # "tmp":Landroid/graphics/Bitmap;
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSaveCroppedBitmap:Z

    move/from16 v41, v0

    if-eqz v41, :cond_1e

    .line 750
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCroppedBitmap:Landroid/graphics/Bitmap;

    .line 753
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    move-object/from16 v41, v0

    invoke-static/range {v41 .. v41}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v10

    .line 756
    .local v10, "cf":Landroid/graphics/Bitmap$CompressFormat;
    new-instance v39, Ljava/io/ByteArrayOutputStream;

    const/16 v41, 0x800

    move-object/from16 v0, v39

    move/from16 v1, v41

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 757
    .local v39, "tmpOut":Ljava/io/ByteArrayOutputStream;
    const/16 v41, 0x5a

    move/from16 v0, v41

    move-object/from16 v1, v39

    invoke-virtual {v11, v10, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v41

    if-eqz v41, :cond_1f

    .line 758
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v41, v0

    if-eqz v41, :cond_1f

    .line 759
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-nez v41, :cond_21

    .line 761
    if-eqz v40, :cond_1f

    .line 762
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setHomescreenWallpaperFromResId(Landroid/app/WallpaperManager;Ljava/io/ByteArrayOutputStream;)Z

    move-result v17

    .line 815
    .end local v10    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .end local v11    # "crop":Landroid/graphics/Bitmap;
    .end local v13    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    .end local v18    # "fullSize":Landroid/graphics/Bitmap;
    .end local v20    # "inverseRotateMatrix":Landroid/graphics/Matrix;
    .end local v33    # "rotateMatrix":Landroid/graphics/Matrix;
    .end local v35    # "roundedTrueCrop":Landroid/graphics/Rect;
    .end local v36    # "scaleDownSampleSize":I
    .end local v39    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    :cond_1f
    :goto_9
    if-nez v17, :cond_29

    const/16 v41, 0x1

    goto/16 :goto_0

    .line 719
    .restart local v11    # "crop":Landroid/graphics/Bitmap;
    .restart local v12    # "cropRect":Landroid/graphics/RectF;
    .restart local v13    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    .restart local v14    # "dimsAfter":[F
    .restart local v18    # "fullSize":Landroid/graphics/Bitmap;
    .restart local v20    # "inverseRotateMatrix":Landroid/graphics/Matrix;
    .restart local v21    # "m":Landroid/graphics/Matrix;
    .restart local v32    # "returnRect":Landroid/graphics/RectF;
    .restart local v33    # "rotateMatrix":Landroid/graphics/Matrix;
    .restart local v35    # "roundedTrueCrop":Landroid/graphics/Rect;
    .restart local v36    # "scaleDownSampleSize":I
    :cond_20
    new-instance v22, Landroid/graphics/Matrix;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Matrix;-><init>()V

    .line 720
    .local v22, "m1":Landroid/graphics/Matrix;
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v41

    move/from16 v0, v41

    neg-int v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    const/high16 v42, 0x40000000    # 2.0f

    div-float v41, v41, v42

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v42

    move/from16 v0, v42

    neg-int v0, v0

    move/from16 v42, v0

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v42, v0

    const/high16 v43, 0x40000000    # 2.0f

    div-float v42, v42, v43

    move-object/from16 v0, v22

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 721
    new-instance v23, Landroid/graphics/Matrix;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Matrix;-><init>()V

    .line 722
    .local v23, "m2":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    move-object/from16 v0, v23

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 723
    new-instance v24, Landroid/graphics/Matrix;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Matrix;-><init>()V

    .line 724
    .local v24, "m3":Landroid/graphics/Matrix;
    const/16 v41, 0x0

    aget v41, v14, v41

    const/high16 v42, 0x40000000    # 2.0f

    div-float v41, v41, v42

    const/16 v42, 0x1

    aget v42, v14, v42

    const/high16 v43, 0x40000000    # 2.0f

    div-float v42, v42, v43

    move-object/from16 v0, v24

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 725
    new-instance v25, Landroid/graphics/Matrix;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Matrix;-><init>()V

    .line 726
    .local v25, "m4":Landroid/graphics/Matrix;
    sget-object v41, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    move-object/from16 v2, v41

    invoke-virtual {v0, v12, v1, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 728
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 729
    .local v8, "c1":Landroid/graphics/Matrix;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 730
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 731
    .local v9, "c2":Landroid/graphics/Matrix;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 732
    move-object/from16 v0, v21

    invoke-virtual {v0, v9, v8}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    goto/16 :goto_8

    .line 763
    .end local v8    # "c1":Landroid/graphics/Matrix;
    .end local v9    # "c2":Landroid/graphics/Matrix;
    .end local v12    # "cropRect":Landroid/graphics/RectF;
    .end local v14    # "dimsAfter":[F
    .end local v21    # "m":Landroid/graphics/Matrix;
    .end local v22    # "m1":Landroid/graphics/Matrix;
    .end local v23    # "m2":Landroid/graphics/Matrix;
    .end local v24    # "m3":Landroid/graphics/Matrix;
    .end local v25    # "m4":Landroid/graphics/Matrix;
    .end local v32    # "returnRect":Landroid/graphics/RectF;
    .restart local v10    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .restart local v39    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    :cond_21
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, 0x1

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_22

    .line 764
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setLockscreenWallpaperFromResId(Landroid/graphics/Bitmap;)Z

    move-result v17

    goto/16 :goto_9

    .line 765
    :cond_22
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, 0x2

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_26

    .line 766
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v41, v0

    if-eqz v41, :cond_1f

    .line 767
    if-eqz v40, :cond_23

    .line 768
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setHomescreenWallpaperFromResId(Landroid/app/WallpaperManager;Ljava/io/ByteArrayOutputStream;)Z

    move-result v17

    .line 770
    :cond_23
    if-nez v17, :cond_24

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setLockscreenWallpaperFromResId(Landroid/graphics/Bitmap;)Z

    move-result v41

    if-eqz v41, :cond_25

    :cond_24
    const/16 v17, 0x1

    :goto_a
    goto/16 :goto_9

    :cond_25
    const/16 v17, 0x0

    goto :goto_a

    .line 772
    :cond_26
    sget v41, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/16 v42, 0x3

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_1f

    .line 773
    const/16 v28, 0x0

    .line 775
    .restart local v28    # "os":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v41

    const-string v42, "sview_bg_wallpaper_path"

    invoke-static/range {v41 .. v42}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 776
    .restart local v26    # "mPath":Ljava/lang/String;
    if-nez v26, :cond_27

    .line 777
    const-string v26, "/storage/emulated/0/Android/data/com.sec.android.sviewcover/files/Pictures/cover_wallpaper.jpg"

    .line 781
    :goto_b
    new-instance v37, Ljava/io/File;

    move-object/from16 v0, v37

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 782
    .restart local v37    # "sview_wallpaper":Ljava/io/File;
    const/16 v29, 0x0

    .line 785
    .restart local v29    # "out":Ljava/io/OutputStream;
    :try_start_3
    invoke-virtual/range {v37 .. v37}, Ljava/io/File;->createNewFile()Z

    .line 786
    new-instance v30, Ljava/io/FileOutputStream;

    move-object/from16 v0, v30

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 787
    .end local v29    # "out":Ljava/io/OutputStream;
    .restart local v30    # "out":Ljava/io/OutputStream;
    :try_start_4
    sget-object v41, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v42, 0x64

    move-object/from16 v0, v41

    move/from16 v1, v42

    move-object/from16 v2, v30

    invoke-virtual {v11, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    .line 800
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v41

    const-string v42, "sview_color_wallpaper"

    const/16 v43, 0x1

    invoke-static/range {v41 .. v43}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 802
    new-instance v19, Landroid/content/Intent;

    const-string v41, "com.sec.android.sviewcover.CHANGE_COVER_BACKGROUND"

    move-object/from16 v0, v19

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 803
    .restart local v19    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 806
    if-eqz v30, :cond_1f

    .line 807
    :try_start_5
    invoke-virtual/range {v30 .. v30}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_9

    .line 808
    :catch_2
    move-exception v15

    .line 809
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_9

    .line 779
    .end local v15    # "e":Ljava/io/IOException;
    .end local v19    # "intent":Landroid/content/Intent;
    .end local v30    # "out":Ljava/io/OutputStream;
    .end local v37    # "sview_wallpaper":Ljava/io/File;
    :cond_27
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "/cover_wallpaper.jpg"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto :goto_b

    .line 788
    .restart local v29    # "out":Ljava/io/OutputStream;
    .restart local v37    # "sview_wallpaper":Ljava/io/File;
    :catch_3
    move-exception v15

    .line 789
    .local v15, "e":Ljava/lang/Exception;
    :goto_c
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    .line 792
    if-eqz v29, :cond_28

    .line 793
    :try_start_6
    invoke-virtual/range {v29 .. v29}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 798
    :cond_28
    :goto_d
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 794
    :catch_4
    move-exception v16

    .line 795
    .local v16, "e2":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 815
    .end local v10    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .end local v11    # "crop":Landroid/graphics/Bitmap;
    .end local v13    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    .end local v15    # "e":Ljava/lang/Exception;
    .end local v16    # "e2":Ljava/io/IOException;
    .end local v18    # "fullSize":Landroid/graphics/Bitmap;
    .end local v20    # "inverseRotateMatrix":Landroid/graphics/Matrix;
    .end local v26    # "mPath":Ljava/lang/String;
    .end local v28    # "os":Ljava/io/FileOutputStream;
    .end local v29    # "out":Ljava/io/OutputStream;
    .end local v33    # "rotateMatrix":Landroid/graphics/Matrix;
    .end local v35    # "roundedTrueCrop":Landroid/graphics/Rect;
    .end local v36    # "scaleDownSampleSize":I
    .end local v37    # "sview_wallpaper":Ljava/io/File;
    .end local v39    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    :cond_29
    const/16 v41, 0x0

    goto/16 :goto_0

    .line 788
    .restart local v10    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .restart local v11    # "crop":Landroid/graphics/Bitmap;
    .restart local v13    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    .restart local v18    # "fullSize":Landroid/graphics/Bitmap;
    .restart local v20    # "inverseRotateMatrix":Landroid/graphics/Matrix;
    .restart local v26    # "mPath":Ljava/lang/String;
    .restart local v28    # "os":Ljava/io/FileOutputStream;
    .restart local v30    # "out":Ljava/io/OutputStream;
    .restart local v33    # "rotateMatrix":Landroid/graphics/Matrix;
    .restart local v35    # "roundedTrueCrop":Landroid/graphics/Rect;
    .restart local v36    # "scaleDownSampleSize":I
    .restart local v37    # "sview_wallpaper":Ljava/io/File;
    .restart local v39    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    :catch_5
    move-exception v15

    move-object/from16 v29, v30

    .end local v30    # "out":Ljava/io/OutputStream;
    .restart local v29    # "out":Ljava/io/OutputStream;
    goto :goto_c

    .line 588
    .end local v10    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .end local v11    # "crop":Landroid/graphics/Bitmap;
    .end local v13    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    .end local v18    # "fullSize":Landroid/graphics/Bitmap;
    .end local v20    # "inverseRotateMatrix":Landroid/graphics/Matrix;
    .end local v29    # "out":Ljava/io/OutputStream;
    .end local v33    # "rotateMatrix":Landroid/graphics/Matrix;
    .end local v35    # "roundedTrueCrop":Landroid/graphics/Rect;
    .end local v36    # "scaleDownSampleSize":I
    .end local v39    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "b":Landroid/graphics/Bitmap;
    .restart local v30    # "out":Ljava/io/OutputStream;
    :catch_6
    move-exception v15

    move-object/from16 v29, v30

    .end local v30    # "out":Ljava/io/OutputStream;
    .restart local v29    # "out":Ljava/io/OutputStream;
    goto/16 :goto_6
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->cropBitmap()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 404
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getCroppedBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCroppedBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getImageBounds()Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 519
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()V

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    if-eqz v2, :cond_0

    .line 521
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 522
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    invoke-static {v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 524
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eqz v2, :cond_0

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eqz v2, :cond_0

    .line 525
    new-instance v1, Landroid/graphics/Point;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 528
    .end local v0    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    return-object v1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 825
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 828
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 404
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method public setCropBounds(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "cropBounds"    # Landroid/graphics/RectF;

    .prologue
    .line 532
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 533
    return-void
.end method

.method protected setHomescreenWallpaperFromInputStream(Landroid/app/WallpaperManager;)Z
    .locals 6
    .param p1, "wm"    # Landroid/app/WallpaperManager;

    .prologue
    const/4 v5, 0x2

    .line 831
    const/4 v1, 0x0

    .line 833
    .local v1, "failure":Z
    :try_start_0
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isMultiSIM:Z

    if-eqz v2, :cond_3

    .line 834
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream2:Ljava/io/InputStream;

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->setAsDsWallpaper(Landroid/content/Context;Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 835
    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkLiveViewLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$100()Z

    move-result v2

    if-nez v2, :cond_0

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkFestivalLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$200()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-ne v2, v5, :cond_2

    .line 836
    :cond_1
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 847
    :cond_2
    :goto_0
    return v1

    .line 838
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInStream:Ljava/io/InputStream;

    invoke-virtual {p1, v2}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 839
    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkLiveViewLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$100()Z

    move-result v2

    if-nez v2, :cond_4

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkFestivalLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$200()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-ne v2, v5, :cond_2

    .line 840
    :cond_5
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 842
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Wallpapers.CropActivity"

    const-string v3, "cannot write stream to wallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 844
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected setHomescreenWallpaperFromResId(Landroid/app/WallpaperManager;Ljava/io/ByteArrayOutputStream;)Z
    .locals 7
    .param p1, "wm"    # Landroid/app/WallpaperManager;
    .param p2, "tmpOut"    # Ljava/io/ByteArrayOutputStream;

    .prologue
    const/4 v6, 0x2

    .line 963
    const/4 v1, 0x0

    .line 966
    .local v1, "failure":Z
    :try_start_0
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 967
    .local v2, "outByteArray":[B
    sget-boolean v3, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isMultiSIM:Z

    if-eqz v3, :cond_3

    .line 968
    const-string v3, "Wallpapers.CropActivity"

    const-string v4, "cropBitmap ByteArrayOutputStream HOMESCREEN_POSITION multisim"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mResources:Landroid/content/res/Resources;

    iget v5, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->setAsDsWallpaper(Landroid/content/Context;Landroid/content/res/Resources;I)V

    .line 970
    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkLiveViewLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$100()Z

    move-result v3

    if-nez v3, :cond_0

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkFestivalLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$200()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    sget v3, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-ne v3, v6, :cond_2

    .line 971
    :cond_1
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "lockscreen_wallpaper"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 985
    .end local v2    # "outByteArray":[B
    :cond_2
    :goto_0
    return v1

    .line 973
    .restart local v2    # "outByteArray":[B
    :cond_3
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p1, v3}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 974
    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkLiveViewLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$100()Z

    move-result v3

    if-nez v3, :cond_4

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkFestivalLockScreen()Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->access$200()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    sget v3, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-ne v3, v6, :cond_6

    .line 975
    :cond_5
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "lockscreen_wallpaper"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 977
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnBitmapCroppedHandler:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;

    if-eqz v3, :cond_2

    .line 978
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnBitmapCroppedHandler:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;

    invoke-interface {v3, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;->onBitmapCropped([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 980
    .end local v2    # "outByteArray":[B
    :catch_0
    move-exception v0

    .line 981
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "Wallpapers.CropActivity"

    const-string v4, "cannot write stream to wallpaper"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 982
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected setLockScreenWallpaperFromInputStream()Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 852
    const/4 v6, 0x0

    .line 853
    .local v6, "os":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 854
    .local v0, "b":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 856
    .local v3, "failure":Z
    iget-object v7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSavedImagePath:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 857
    iget-object v7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mSavedImagePath:Ljava/lang/String;

    invoke-static {v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 862
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 863
    const-string v7, "Wallpapers.CropActivity"

    const-string v8, "cannot load Bitmap, please check save image"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    const/4 v3, 0x1

    move v4, v3

    .line 959
    .end local v3    # "failure":Z
    .local v4, "failure":I
    :goto_1
    return v4

    .line 858
    .end local v4    # "failure":I
    .restart local v3    # "failure":Z
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    if-eqz v7, :cond_0

    .line 859
    iget-object v7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 879
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper.jpg"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 898
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v0, v7, v8, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 900
    sget-boolean v7, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isMultiSIM:Z

    if-eqz v7, :cond_9

    .line 901
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSetAsSIM2()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSimSelectedID()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    .line 902
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_2"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 905
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_path_2"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper_2"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 909
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_path_ripple_2"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper_2"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 913
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper"

    invoke-static {v7, v8, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 917
    :cond_4
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSetAsSIM1()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSimSelectedID()I

    move-result v7

    if-ne v7, v11, :cond_6

    .line 918
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 921
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_path"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 925
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_path_ripple"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 929
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper"

    invoke-static {v7, v8, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 947
    :cond_6
    :goto_2
    new-instance v5, Landroid/content/Intent;

    const-string v7, "com.sec.android.gallery3d.LOCKSCREEN_IMAGE_CHANGED"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 948
    .local v5, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 950
    if-eqz v6, :cond_7

    .line 952
    :try_start_1
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 958
    :cond_7
    :goto_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move v4, v3

    .line 959
    .restart local v4    # "failure":I
    goto/16 :goto_1

    .line 881
    .end local v4    # "failure":I
    .end local v5    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 882
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v7, "Wallpapers.CropActivity"

    const-string v8, "File not found! - lockscreen_wallpaper.jpg"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 885
    if-eqz v6, :cond_8

    .line 887
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 893
    :cond_8
    :goto_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 895
    const/4 v3, 0x1

    move v4, v3

    .restart local v4    # "failure":I
    goto/16 :goto_1

    .line 888
    .end local v4    # "failure":I
    :catch_1
    move-exception v2

    .line 889
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 933
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 936
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_path"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 940
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper_path_ripple"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 944
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "lockscreen_wallpaper"

    invoke-static {v7, v8, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2

    .line 953
    .restart local v5    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v1

    .line 954
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3
.end method

.method protected setLockscreenWallpaperFromResId(Landroid/graphics/Bitmap;)Z
    .locals 11
    .param p1, "crop"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v10, 0x1

    .line 990
    const/4 v5, 0x0

    .line 991
    .local v5, "os":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 1002
    .local v2, "failure":Z
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper.jpg"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1016
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x64

    invoke-virtual {p1, v6, v7, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1018
    sget-boolean v6, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isMultiSIM:Z

    if-eqz v6, :cond_6

    .line 1019
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSetAsSIM2()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSimSelectedID()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 1020
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_2"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 1023
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path_2"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lockscreen_wallpaper_2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1027
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path_ripple_2"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lockscreen_wallpaper_2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1031
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1035
    :cond_1
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSetAsSIM1()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSimSelectedID()I

    move-result v6

    if-ne v6, v10, :cond_3

    .line 1036
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 1039
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lockscreen_wallpaper"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1043
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path_ripple"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lockscreen_wallpaper"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1047
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1066
    :cond_3
    :goto_0
    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.sec.android.gallery3d.LOCKSCREEN_IMAGE_CHANGED"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1067
    .local v4, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1070
    if-eqz v5, :cond_4

    .line 1071
    :try_start_1
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_4
    :goto_1
    move v3, v2

    .line 1076
    .end local v2    # "failure":Z
    .end local v4    # "intent":Landroid/content/Intent;
    .local v3, "failure":I
    :goto_2
    return v3

    .line 1003
    .end local v3    # "failure":I
    .restart local v2    # "failure":Z
    :catch_0
    move-exception v0

    .line 1004
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 1006
    if-eqz v5, :cond_5

    .line 1008
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1014
    :cond_5
    :goto_3
    const/4 v2, 0x1

    move v3, v2

    .restart local v3    # "failure":I
    goto :goto_2

    .line 1009
    .end local v3    # "failure":I
    :catch_1
    move-exception v1

    .line 1010
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1051
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 1055
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lockscreen_wallpaper"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1059
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path_ripple"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lockscreen_wallpaper"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1063
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 1072
    .restart local v4    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v0

    .line 1073
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public setNoCrop(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 474
    iput-boolean p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mNoCrop:Z

    .line 475
    return-void
.end method

.method public setOnBitmapCropped(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnBitmapCroppedHandler:Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;

    .line 471
    return-void
.end method

.method public setOnEndRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    .line 479
    return-void
.end method
