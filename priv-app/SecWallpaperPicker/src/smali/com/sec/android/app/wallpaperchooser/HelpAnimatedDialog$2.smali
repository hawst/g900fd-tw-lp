.class Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;
.super Ljava/lang/Object;
.source "HelpAnimatedDialog.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;->this$0:Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;->this$0:Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    # getter for: Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->access$200(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 91
    # getter for: Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mGoalAnimationView:Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->access$100()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;->this$0:Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    # getter for: Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->mFadingAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->access$300(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 92
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 86
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog$2;->this$0:Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    # operator++ for: Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->flashCount:I
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->access$208(Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;)I

    .line 82
    return-void
.end method
