.class Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;
.super Ljava/lang/Thread;
.source "TiledImageRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TileDecoder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)V
    .locals 0

    .prologue
    .line 790
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->this$0:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;
    .param p2, "x1"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;

    .prologue
    .line 790
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;-><init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)V

    return-void
.end method

.method private waitForTile()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 802
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->this$0:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    # getter for: Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->access$700(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 804
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->this$0:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    # getter for: Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->access$800(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->pop()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v0

    .line 805
    .local v0, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    if-eqz v0, :cond_0

    .line 806
    monitor-exit v2

    return-object v0

    .line 808
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->this$0:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    # getter for: Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->access$700(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 810
    .end local v0    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public finishAndWait()V
    .locals 3

    .prologue
    .line 793
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->interrupt()V

    .line 795
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 799
    :goto_0
    return-void

    .line 796
    :catch_0
    move-exception v0

    .line 797
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "TiledImageRenderer"

    const-string v2, "Interrupted while waiting for TileDecoder thread to finish!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 816
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 817
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->waitForTile()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v0

    .line 818
    .local v0, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->this$0:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    # invokes: Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->decodeTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->access$900(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 820
    .end local v0    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :catch_0
    move-exception v1

    .line 823
    :cond_0
    return-void
.end method
