.class public Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;
.super Landroid/animation/AnimatorListenerAdapter;
.source "FirstFrameAnimatorHelper.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# static fields
.field private static sGlobalFrameCounter:J

.field private static sVisible:Z


# instance fields
.field private mAdjustedSecondFrameTime:Z

.field private mHandlingOnAnimationUpdate:Z

.field private mStartFrame:J

.field private mStartTime:J

.field private mTarget:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V
    .locals 2
    .param p1, "vpa"    # Landroid/view/ViewPropertyAnimator;
    .param p2, "target"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartTime:J

    .line 53
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mTarget:Landroid/view/View;

    .line 54
    invoke-virtual {p1, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 55
    return-void
.end method


# virtual methods
.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 59
    move-object v0, p1

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 60
    .local v0, "va":Landroid/animation/ValueAnimator;
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 61
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->onAnimationUpdate(Landroid/animation/ValueAnimator;)V

    .line 62
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 89
    .local v0, "currentTime":J
    iget-wide v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartTime:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 90
    sget-wide v4, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->sGlobalFrameCounter:J

    iput-wide v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartFrame:J

    .line 91
    iput-wide v0, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartTime:J

    .line 94
    :cond_0
    iget-boolean v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mHandlingOnAnimationUpdate:Z

    if-nez v4, :cond_2

    sget-boolean v4, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->sVisible:Z

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 100
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mHandlingOnAnimationUpdate:Z

    .line 101
    sget-wide v4, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->sGlobalFrameCounter:J

    iget-wide v6, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartFrame:J

    sub-long v2, v4, v6

    .line 105
    .local v2, "frameNum":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_3

    iget-wide v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartTime:J

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-gez v4, :cond_3

    .line 108
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mTarget:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    .line 109
    const-wide/16 v4, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 129
    :cond_1
    :goto_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mHandlingOnAnimationUpdate:Z

    .line 133
    .end local v2    # "frameNum":J
    :cond_2
    return-void

    .line 114
    .restart local v2    # "frameNum":J
    :cond_3
    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_4

    iget-wide v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartTime:J

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-gez v4, :cond_4

    iget-boolean v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mAdjustedSecondFrameTime:Z

    if-nez v4, :cond_4

    iget-wide v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mStartTime:J

    const-wide/16 v6, 0x10

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-lez v4, :cond_4

    .line 117
    const-wide/16 v4, 0x10

    invoke-virtual {p1, v4, v5}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 118
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mAdjustedSecondFrameTime:Z

    goto :goto_0

    .line 120
    :cond_4
    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;->mTarget:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper$2;

    invoke-direct {v5, p0, p1}, Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper$2;-><init>(Lcom/sec/android/app/wallpaperchooser/FirstFrameAnimatorHelper;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
