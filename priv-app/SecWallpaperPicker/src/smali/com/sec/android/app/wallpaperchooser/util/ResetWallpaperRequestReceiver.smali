.class public Lcom/sec/android/app/wallpaperchooser/util/ResetWallpaperRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ResetWallpaperRequestReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public getDefaultWallpaper(Landroid/content/Context;)Ljava/io/InputStream;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const/4 v8, 0x0

    .line 68
    .local v8, "is":Ljava/io/InputStream;
    :try_start_0
    const-string v3, "/carrier/data/app/WallpaperChooser/Customization_DefaultBackground.jpg"

    .line 69
    .local v3, "customWpFilePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    .local v2, "custWpFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->length()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-lez v17, :cond_2

    .line 72
    :try_start_1
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 74
    .end local v8    # "is":Ljava/io/InputStream;
    .local v9, "is":Ljava/io/InputStream;
    :try_start_2
    const-string v17, "ResetWallpaperRequestReceiver"

    const-string v18, " Chameleon Wallpaper found"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 81
    :goto_0
    if-nez v9, :cond_7

    .line 82
    :try_start_3
    const-string v4, "/system/wallpaper/default_wallpaper/"

    .line 83
    .local v4, "defaultWpFilePath":Ljava/lang/String;
    const-string v5, "/system/csc_contents/"

    .line 84
    .local v5, "defaultWpFilePathMultiCSC":Ljava/lang/String;
    const/4 v14, 0x0

    .line 86
    .local v14, "wpFileMultiCSC":Ljava/io/File;
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    .local v16, "wpFilePathMultiCSC":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v13

    .line 88
    .local v13, "wpFileListMultiCSC":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 90
    .local v10, "isDefaultWallpaperInMultiCsc":Z
    if-eqz v13, :cond_1

    .line 93
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v13

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v7, v0, :cond_0

    .line 94
    aget-object v17, v13, v7

    const-string v18, "default_wallpaper"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 95
    const/4 v10, 0x1

    .line 100
    :cond_0
    if-eqz v10, :cond_1

    .line 101
    new-instance v14, Ljava/io/File;

    .end local v14    # "wpFileMultiCSC":Ljava/io/File;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    aget-object v18, v13, v7

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .end local v7    # "i":I
    .restart local v14    # "wpFileMultiCSC":Ljava/io/File;
    :cond_1
    const/4 v15, 0x0

    .line 106
    .local v15, "wpFilePath":Ljava/io/File;
    if-eqz v10, :cond_4

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 107
    const-string v17, "ResetWallpaperRequestReceiver"

    const-string v18, "getDefaultWallpaperPath() symbolic link is used."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v15, Ljava/io/File;

    .end local v15    # "wpFilePath":Ljava/io/File;
    invoke-direct {v15, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 113
    .restart local v15    # "wpFilePath":Ljava/io/File;
    :goto_2
    invoke-virtual {v15}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v12

    .line 114
    .local v12, "wpFileList":[Ljava/lang/String;
    if-eqz v12, :cond_7

    .line 115
    const/4 v11, 0x0

    .line 116
    .local v11, "wpFile":Ljava/io/File;
    if-eqz v10, :cond_5

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 117
    move-object v11, v14

    .line 118
    const-string v17, "ResetWallpaperRequestReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "getDefaultWallpaperPath(). symbolic link path is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_3
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-virtual {v11}, Ljava/io/File;->length()J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-lez v17, :cond_7

    .line 125
    :try_start_4
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 126
    .end local v9    # "is":Ljava/io/InputStream;
    .restart local v8    # "is":Ljava/io/InputStream;
    :try_start_5
    const-string v17, "ResetWallpaperRequestReceiver"

    const-string v18, "CSC Wallpaper found"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 134
    .end local v4    # "defaultWpFilePath":Ljava/lang/String;
    .end local v5    # "defaultWpFilePathMultiCSC":Ljava/lang/String;
    .end local v10    # "isDefaultWallpaperInMultiCsc":Z
    .end local v11    # "wpFile":Ljava/io/File;
    .end local v12    # "wpFileList":[Ljava/lang/String;
    .end local v13    # "wpFileListMultiCSC":[Ljava/lang/String;
    .end local v14    # "wpFileMultiCSC":Ljava/io/File;
    .end local v15    # "wpFilePath":Ljava/io/File;
    .end local v16    # "wpFilePathMultiCSC":Ljava/io/File;
    :goto_4
    if-eqz v8, :cond_6

    .line 140
    .end local v2    # "custWpFile":Ljava/io/File;
    .end local v3    # "customWpFilePath":Ljava/lang/String;
    .end local v8    # "is":Ljava/io/InputStream;
    :goto_5
    return-object v8

    .line 75
    .restart local v2    # "custWpFile":Ljava/io/File;
    .restart local v3    # "customWpFilePath":Ljava/lang/String;
    .restart local v8    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v6

    .line 76
    .local v6, "e":Ljava/io/IOException;
    :goto_6
    :try_start_6
    const-string v17, "ResetWallpaperRequestReceiver"

    const-string v18, "Chameleon Wallpaper FileInputStream error"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .end local v6    # "e":Ljava/io/IOException;
    :cond_2
    move-object v9, v8

    .end local v8    # "is":Ljava/io/InputStream;
    .restart local v9    # "is":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 93
    .restart local v4    # "defaultWpFilePath":Ljava/lang/String;
    .restart local v5    # "defaultWpFilePathMultiCSC":Ljava/lang/String;
    .restart local v7    # "i":I
    .restart local v10    # "isDefaultWallpaperInMultiCsc":Z
    .restart local v13    # "wpFileListMultiCSC":[Ljava/lang/String;
    .restart local v14    # "wpFileMultiCSC":Ljava/io/File;
    .restart local v16    # "wpFilePathMultiCSC":Ljava/io/File;
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 110
    .end local v7    # "i":I
    .restart local v15    # "wpFilePath":Ljava/io/File;
    :cond_4
    :try_start_7
    new-instance v15, Ljava/io/File;

    .end local v15    # "wpFilePath":Ljava/io/File;
    invoke-direct {v15, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v15    # "wpFilePath":Ljava/io/File;
    goto :goto_2

    .line 120
    .restart local v11    # "wpFile":Ljava/io/File;
    .restart local v12    # "wpFileList":[Ljava/lang/String;
    :cond_5
    new-instance v11, Ljava/io/File;

    .end local v11    # "wpFile":Ljava/io/File;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v18, v12, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .restart local v11    # "wpFile":Ljava/io/File;
    goto :goto_3

    .line 127
    :catch_1
    move-exception v6

    move-object v8, v9

    .line 128
    .end local v9    # "is":Ljava/io/InputStream;
    .restart local v6    # "e":Ljava/io/IOException;
    .restart local v8    # "is":Ljava/io/InputStream;
    :goto_7
    :try_start_8
    const-string v17, "ResetWallpaperRequestReceiver"

    const-string v18, "CSC Wallpaper FileInputStream error"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_4

    .line 137
    .end local v2    # "custWpFile":Ljava/io/File;
    .end local v3    # "customWpFilePath":Ljava/lang/String;
    .end local v4    # "defaultWpFilePath":Ljava/lang/String;
    .end local v5    # "defaultWpFilePathMultiCSC":Ljava/lang/String;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v10    # "isDefaultWallpaperInMultiCsc":Z
    .end local v11    # "wpFile":Ljava/io/File;
    .end local v12    # "wpFileList":[Ljava/lang/String;
    .end local v13    # "wpFileListMultiCSC":[Ljava/lang/String;
    .end local v14    # "wpFileMultiCSC":Ljava/io/File;
    .end local v15    # "wpFilePath":Ljava/io/File;
    .end local v16    # "wpFilePathMultiCSC":Ljava/io/File;
    :catch_2
    move-exception v6

    .line 138
    .local v6, "e":Ljava/lang/Exception;
    :goto_8
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 140
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    .line 137
    .end local v8    # "is":Ljava/io/InputStream;
    .restart local v2    # "custWpFile":Ljava/io/File;
    .restart local v3    # "customWpFilePath":Ljava/lang/String;
    .restart local v9    # "is":Ljava/io/InputStream;
    :catch_3
    move-exception v6

    move-object v8, v9

    .end local v9    # "is":Ljava/io/InputStream;
    .restart local v8    # "is":Ljava/io/InputStream;
    goto :goto_8

    .line 127
    .restart local v4    # "defaultWpFilePath":Ljava/lang/String;
    .restart local v5    # "defaultWpFilePathMultiCSC":Ljava/lang/String;
    .restart local v10    # "isDefaultWallpaperInMultiCsc":Z
    .restart local v11    # "wpFile":Ljava/io/File;
    .restart local v12    # "wpFileList":[Ljava/lang/String;
    .restart local v13    # "wpFileListMultiCSC":[Ljava/lang/String;
    .restart local v14    # "wpFileMultiCSC":Ljava/io/File;
    .restart local v15    # "wpFilePath":Ljava/io/File;
    .restart local v16    # "wpFilePathMultiCSC":Ljava/io/File;
    :catch_4
    move-exception v6

    goto :goto_7

    .line 75
    .end local v4    # "defaultWpFilePath":Ljava/lang/String;
    .end local v5    # "defaultWpFilePathMultiCSC":Ljava/lang/String;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v10    # "isDefaultWallpaperInMultiCsc":Z
    .end local v11    # "wpFile":Ljava/io/File;
    .end local v12    # "wpFileList":[Ljava/lang/String;
    .end local v13    # "wpFileListMultiCSC":[Ljava/lang/String;
    .end local v14    # "wpFileMultiCSC":Ljava/io/File;
    .end local v15    # "wpFilePath":Ljava/io/File;
    .end local v16    # "wpFilePathMultiCSC":Ljava/io/File;
    .restart local v9    # "is":Ljava/io/InputStream;
    :catch_5
    move-exception v6

    move-object v8, v9

    .end local v9    # "is":Ljava/io/InputStream;
    .restart local v8    # "is":Ljava/io/InputStream;
    goto :goto_6

    .end local v8    # "is":Ljava/io/InputStream;
    .restart local v9    # "is":Ljava/io/InputStream;
    :cond_7
    move-object v8, v9

    .end local v9    # "is":Ljava/io/InputStream;
    .restart local v8    # "is":Ljava/io/InputStream;
    goto :goto_4
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ServiceCast"
        }
    .end annotation

    .prologue
    .line 28
    const-string v4, "VZW"

    const-string v5, "ro.csc.sales_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 30
    const/4 v2, 0x0

    .line 31
    .local v2, "stream":Ljava/io/InputStream;
    :try_start_0
    const-string v4, "wallpaper"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/WallpaperManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 34
    .local v3, "wpm":Landroid/app/WallpaperManager;
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/util/ResetWallpaperRequestReceiver;->getDefaultWallpaper(Landroid/content/Context;)Ljava/io/InputStream;

    move-result-object v2

    .line 35
    if-eqz v2, :cond_1

    .line 36
    invoke-virtual {v3, v2}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 37
    const-string v4, "ResetWallpaperRequestReceiver"

    const-string v5, "Wallpaper successfully reset to default from CSC image"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :goto_0
    if-eqz v2, :cond_0

    .line 53
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 62
    .end local v2    # "stream":Ljava/io/InputStream;
    .end local v3    # "wpm":Landroid/app/WallpaperManager;
    :cond_0
    :goto_1
    return-void

    .line 40
    .restart local v2    # "stream":Ljava/io/InputStream;
    .restart local v3    # "wpm":Landroid/app/WallpaperManager;
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Landroid/app/WallpaperManager;->clear()V

    .line 41
    const-string v4, "ResetWallpaperRequestReceiver"

    const-string v5, "Wallpaper successfully reset to default by clear wallpaper"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 45
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v4, "android.wallpaper.settings_systemui_transparency"

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 48
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v4, "ResetWallpaperRequestReceiver"

    const-string v5, "can not do FileInputStream"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 52
    if-eqz v2, :cond_0

    .line 53
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 54
    :catch_1
    move-exception v1

    .line 55
    :try_start_6
    const-string v4, "ResetWallpaperRequestReceiver"

    const-string v5, "can not do stream close"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    .line 58
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "wpm":Landroid/app/WallpaperManager;
    :catch_2
    move-exception v1

    .line 59
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v4, "ResetWallpaperRequestReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to reset to default wallpaper :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 54
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "wpm":Landroid/app/WallpaperManager;
    :catch_3
    move-exception v1

    .line 55
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_7
    const-string v4, "ResetWallpaperRequestReceiver"

    const-string v5, "can not do stream close"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 51
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 52
    if-eqz v2, :cond_2

    .line 53
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 56
    :cond_2
    :goto_2
    :try_start_9
    throw v4

    .line 54
    :catch_4
    move-exception v1

    .line 55
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v5, "ResetWallpaperRequestReceiver"

    const-string v6, "can not do stream close"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_2
.end method
