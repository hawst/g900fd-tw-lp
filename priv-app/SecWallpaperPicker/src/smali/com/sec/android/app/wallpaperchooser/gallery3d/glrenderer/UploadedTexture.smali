.class public abstract Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;
.super Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;
.source "UploadedTexture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$1;,
        Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;
    }
.end annotation


# static fields
.field private static sBorderKey:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;

.field private static sBorderLines:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static sUploadedCount:I


# instance fields
.field protected mBitmap:Landroid/graphics/Bitmap;

.field private mBorder:I

.field private mContentValid:Z

.field private mIsUploading:Z

.field private mOpaque:Z

.field private mThrottled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sBorderLines:Ljava/util/HashMap;

    .line 48
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;-><init>(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$1;)V

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sBorderKey:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;-><init>(Z)V

    .line 66
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 3
    .param p1, "hasBorder"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;-><init>(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;II)V

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mIsUploading:Z

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mOpaque:Z

    .line 57
    iput-boolean v1, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mThrottled:Z

    .line 70
    if-eqz p1, :cond_0

    .line 71
    invoke-virtual {p0, v2}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->setBorder(Z)V

    .line 72
    iput v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    .line 74
    :cond_0
    return-void
.end method

.method private freeBitmap()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->onFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    .line 149
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 134
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->onGetBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    .line 137
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    mul-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    .line 138
    .local v0, "h":I
    iget v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 139
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->setSize(II)V

    .line 142
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    return-object v2
.end method

.method private static getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "vertical"    # Z
    .param p1, "config"    # Landroid/graphics/Bitmap$Config;
    .param p2, "length"    # I

    .prologue
    const/4 v3, 0x1

    .line 119
    sget-object v1, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sBorderKey:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;

    .line 120
    .local v1, "key":Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;
    iput-boolean p0, v1, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;->vertical:Z

    .line 121
    iput-object p1, v1, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;->config:Landroid/graphics/Bitmap$Config;

    .line 122
    iput p2, v1, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;->length:I

    .line 123
    sget-object v2, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sBorderLines:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 124
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 125
    if-eqz p0, :cond_1

    invoke-static {v3, p2, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 128
    :goto_0
    sget-object v2, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sBorderLines:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;->clone()Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture$BorderKey;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    :cond_0
    return-object v0

    .line 125
    :cond_1
    invoke-static {p2, v3, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private uploadToCanvas(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V
    .locals 21
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 211
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 212
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_5

    .line 214
    :try_start_0
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .line 215
    .local v17, "bWidth":I
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    .line 216
    .local v16, "bHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getTextureWidth()I

    move-result v20

    .line 217
    .local v20, "texWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getTextureHeight()I

    move-result v19

    .line 219
    .local v19, "texHeight":I
    move/from16 v0, v17

    move/from16 v1, v20

    if-gt v0, v1, :cond_1

    move/from16 v0, v16

    move/from16 v1, v19

    if-gt v0, v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 222
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->getGLId()Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLId;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLId;->generateTexture()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mId:I

    .line 223
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->setTextureParameters(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;)V

    .line 225
    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 226
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v6}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->initializeTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    :cond_0
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    .line 261
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->setAssociatedCanvas(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    .line 262
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mState:I

    .line 263
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    .line 268
    return-void

    .line 219
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 228
    :cond_2
    :try_start_1
    invoke-static {v6}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v7

    .line 229
    .local v7, "format":I
    invoke-static {v6}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v8

    .line 230
    .local v8, "type":I
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v18

    .line 232
    .local v18, "config":Landroid/graphics/Bitmap$Config;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v7, v8}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->initializeTextureSize(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;II)V

    .line 233
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    invoke-interface/range {v2 .. v8}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 235
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    if-lez v2, :cond_3

    .line 237
    const/4 v2, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 238
    .local v13, "line":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 241
    const/4 v2, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 242
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 246
    .end local v13    # "line":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v2, v2, v17

    move/from16 v0, v20

    if-ge v2, v0, :cond_4

    .line 247
    const/4 v2, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 248
    .restart local v13    # "line":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v11, v2, v17

    const/4 v12, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 252
    .end local v13    # "line":Landroid/graphics/Bitmap;
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v2, v2, v16

    move/from16 v0, v19

    if-ge v2, v0, :cond_0

    .line 253
    const/4 v2, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBorderLine(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 254
    .restart local v13    # "line":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    add-int v12, v2, v16

    move-object/from16 v9, p1

    move-object/from16 v10, p0

    move v14, v7

    move v15, v8

    invoke-interface/range {v9 .. v15}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 258
    .end local v7    # "format":I
    .end local v8    # "type":I
    .end local v13    # "line":Landroid/graphics/Bitmap;
    .end local v16    # "bHeight":I
    .end local v17    # "bWidth":I
    .end local v18    # "config":Landroid/graphics/Bitmap$Config;
    .end local v19    # "texHeight":I
    .end local v20    # "texWidth":I
    :catchall_0
    move-exception v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    throw v2

    .line 265
    :cond_5
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mState:I

    .line 266
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Texture load fail, no bitmap"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public getHeight()I
    .locals 2

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    .line 160
    :cond_0
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mHeight:I

    return v0
.end method

.method protected getTarget()I
    .locals 1

    .prologue
    .line 278
    const/16 v0, 0xde1

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 153
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    .line 154
    :cond_0
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    return v0
.end method

.method protected invalidateContent()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    .line 169
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    .line 170
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mWidth:I

    .line 171
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mHeight:I

    .line 172
    return-void
.end method

.method public isContentValid()Z
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mOpaque:Z

    return v0
.end method

.method protected onBind(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 272
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->updateContent(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->isContentValid()Z

    move-result v0

    return v0
.end method

.method protected abstract onFreeBitmap(Landroid/graphics/Bitmap;)V
.end method

.method protected abstract onGetBitmap()Landroid/graphics/Bitmap;
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 292
    invoke-super {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;->recycle()V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    .line 294
    :cond_0
    return-void
.end method

.method public updateContent(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 187
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mThrottled:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sUploadedCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->sUploadedCount:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->uploadToCanvas(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    goto :goto_0

    .line 191
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    if-nez v0, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 193
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 194
    .local v5, "format":I
    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 195
    .local v6, "type":I
    iget v2, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mBorder:I

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->freeBitmap()V

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/UploadedTexture;->mContentValid:Z

    goto :goto_0
.end method
