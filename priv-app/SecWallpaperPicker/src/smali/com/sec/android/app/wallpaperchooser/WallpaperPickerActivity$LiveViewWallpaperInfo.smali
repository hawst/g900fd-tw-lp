.class public Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LiveViewWallpaperInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 3
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 289
    :try_start_0
    const-string v1, "com.yahoo.mobile.client.android.liveweather/com.yahoo.mobile.client.android.liveweather.MainActivity"

    const/16 v2, 0xb

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->launchTargetApp(Ljava/lang/String;I)V
    invoke-static {p1, v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 295
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 292
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "Wallpapers.WallpaperPickerActivity"

    const-string v2, "Launcher does not have the permission to launch com.yahoo.mobile.client.android.liveweather/com.yahoo.mobile.client.android.liveweather.MainActivity"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
