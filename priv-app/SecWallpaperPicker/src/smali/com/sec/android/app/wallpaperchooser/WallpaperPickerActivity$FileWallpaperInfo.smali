.class public Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileWallpaperInfo"
.end annotation


# instance fields
.field private mFilePath:Ljava/lang/String;

.field private mMultipleImage:Z

.field private mOrientation:I

.field private mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "multipleImage"    # Z
    .param p3, "orientation"    # I

    .prologue
    .line 328
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    .line 329
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mFilePath:Ljava/lang/String;

    .line 330
    iput-boolean p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mMultipleImage:Z

    .line 331
    iput p3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mOrientation:I

    .line 332
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

    .prologue
    .line 322
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mOrientation:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mMultipleImage:Z

    return v0
.end method


# virtual methods
.method public isNamelessWallpaper()Z
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x1

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 5
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 336
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;

    move-result-object v1

    .line 337
    .local v1, "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    const/4 v0, 0x0

    .line 338
    .local v0, "rotation":I
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    if-nez v2, :cond_0

    .line 339
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mFilePath:Ljava/lang/String;

    const/16 v4, 0x400

    invoke-direct {v2, p1, v3, v4, v0}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/Context;Ljava/lang/String;II)V

    iput-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    .line 340
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 341
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    .line 342
    return-void
.end method

.method public onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 7
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 347
    new-instance v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 359
    .local v1, "h":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;
    const/4 v2, 0x0

    .line 360
    .local v2, "outByteArray":[B
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mFilePath:Ljava/lang/String;

    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 361
    .local v3, "thumb":Landroid/graphics/Bitmap;
    const-string v5, "jpg"

    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v0

    .line 362
    .local v0, "cf":Landroid/graphics/Bitmap$CompressFormat;
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x800

    invoke-direct {v4, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 363
    .local v4, "tmpOut":Ljava/io/ByteArrayOutputStream;
    if-eqz v3, :cond_0

    .line 364
    const/16 v5, 0x5a

    invoke-virtual {v3, v0, v5, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 365
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 366
    invoke-interface {v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;->onBitmapCropped([B)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    const-string v5, "Wallpapers.WallpaperPickerActivity"

    const-string v6, "Could not decode mFilePath to bitmap called thumb."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
