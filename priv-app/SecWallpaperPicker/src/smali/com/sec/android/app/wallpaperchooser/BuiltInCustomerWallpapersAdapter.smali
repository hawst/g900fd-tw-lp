.class public Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;
.super Landroid/widget/BaseAdapter;
.source "BuiltInCustomerWallpapersAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private MAX_ARRAY:I

.field mContext:Landroid/content/Context;

.field mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;",
            ">;"
        }
    .end annotation
.end field

.field mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "Wallpapaers.BuiltInCustomerWallpapersAdapter"

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 101
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 47
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->MAX_ARRAY:I

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mContext:Landroid/content/Context;

    .line 103
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 104
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getImageName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    # getter for: Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mImageName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->access$000(Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->getItem(I)Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 180
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    # getter for: Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->mThumb:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;->access$100(Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 185
    .local v4, "thumbDrawable":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_0

    .line 186
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error decoding thumbnail for wallpaper #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createImageTileView(Landroid/view/LayoutInflater;ILandroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public loadThumbnailsAndImageIdList()V
    .locals 14

    .prologue
    .line 107
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    .line 108
    const/4 v3, 0x0

    .line 109
    .local v3, "imagePath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 110
    .local v4, "imageThumbPath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 111
    .local v2, "imageName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 114
    .local v5, "jpgImagePath":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v9, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->MAX_ARRAY:I

    if-ge v1, v9, :cond_4

    .line 115
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/wallpaper/drawable/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v11, 0x0

    invoke-virtual {v10, v1, v11}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCscImageSmallFileName(IZ)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".png"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 117
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/wallpaper/drawable/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v11, 0x1

    invoke-virtual {v10, v1, v11}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCscImageSmallFileName(IZ)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 119
    sget-object v9, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v10, 0x0

    invoke-virtual {v9, v1, v10}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCscImageSmallFileName(IZ)Ljava/lang/String;

    move-result-object v2

    .line 121
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/wallpaper/drawable/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v11, 0x0

    invoke-virtual {v10, v1, v11}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCscImageSmallFileName(IZ)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 123
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 125
    .local v6, "jpgOriginFile":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 126
    .local v7, "originFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_2

    .line 114
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 130
    :cond_2
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 131
    move-object v7, v6

    .line 133
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 134
    .local v8, "thumb":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_1

    if-eqz v8, :cond_1

    .line 135
    iget-object v9, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v13, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v12, v13, v8}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-direct {v10, v11, v12, v2}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 141
    .end local v0    # "file":Ljava/io/File;
    .end local v6    # "jpgOriginFile":Ljava/io/File;
    .end local v7    # "originFile":Ljava/io/File;
    .end local v8    # "thumb":Landroid/graphics/Bitmap;
    :cond_4
    const/4 v1, 0x0

    :goto_2
    sget-object v9, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    invoke-virtual {v9}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCustomerCount()I

    move-result v9

    if-ge v1, v9, :cond_8

    .line 142
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/wallpaper/drawable/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v11, 0x1

    invoke-virtual {v10, v1, v11}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCustomerWallpaer(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 144
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/wallpaper/drawable/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v11, 0x0

    invoke-virtual {v10, v1, v11}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCustomerWallpaer(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 146
    sget-object v9, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    invoke-virtual {v9, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCustomerWallpaperName(I)Ljava/lang/String;

    move-result-object v2

    .line 151
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    .restart local v7    # "originFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 153
    .restart local v0    # "file":Ljava/io/File;
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_7

    .line 141
    :cond_6
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 159
    :cond_7
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 160
    .restart local v8    # "thumb":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_6

    if-eqz v8, :cond_6

    .line 161
    iget-object v9, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mImages:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v13, p0, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v12, v13, v8}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-direct {v10, v11, v12, v2}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter$CustomerWallpaperTile;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 165
    .end local v0    # "file":Ljava/io/File;
    .end local v7    # "originFile":Ljava/io/File;
    .end local v8    # "thumb":Landroid/graphics/Bitmap;
    :cond_8
    return-void
.end method
