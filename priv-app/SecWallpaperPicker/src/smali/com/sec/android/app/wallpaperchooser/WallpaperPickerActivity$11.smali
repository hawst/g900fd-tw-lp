.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;
.super Landroid/database/DataSetObserver;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

.field final synthetic val$a:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;)V
    .locals 0

    .prologue
    .line 921
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->val$a:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->val$a:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    const/4 v5, -0x1

    move v4, v3

    move v6, v3

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->initializeScrollForRtl()V
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateTileIndices()V
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 927
    return-void
.end method
