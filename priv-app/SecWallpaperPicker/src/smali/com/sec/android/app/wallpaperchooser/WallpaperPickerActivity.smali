.class public Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ZeroPaddingDrawable;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$TravelImageInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MyInterestsWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$SlidingWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FestivalWallpaperInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$PickImageInfo;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    }
.end annotation


# static fields
.field private static gInstance:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

.field private static final isMultiSIM:Z

.field private static final isSkipCTC:Z

.field private static final isSkipMultiSim:Z

.field public static mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

.field private static mDownloadWallpaper:Z

.field private static mEnableSurveyMode:Z

.field private static mFirstSelect:Z

.field private static mGuideFirstStep:Z

.field private static mHandler:Landroid/os/Handler;

.field private static mInstalledMontblanc:Z

.field private static mInstalledPhotoFrame:Z

.field private static mKnoxMode:Z

.field private static mSpanMenuList:Z

.field private static mSviewCover:Z

.field private static mTTSWallpaper:Z

.field private static mUpdateWallpaperListOnDelete:Z

.field private static mWallpaperSelected:Z

.field private static mpredefinedWallpaper:I

.field private static selectedSim:I


# instance fields
.field private isModeEasyLauncher1:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field private mBtnDropDown:Landroid/widget/ImageView;

.field private mBtnSetWallpaper:Landroid/widget/Button;

.field private mBuiltInCustomerWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

.field private mBuiltInWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;

.field private mIgnoreNextTap:Z

.field private mIsForTestApp:Z

.field private mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

.field private mLiveViewTv:Landroid/widget/TextView;

.field private mLiveWallpapersView:Landroid/widget/LinearLayout;

.field private mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mMasterWallpaperList:Landroid/widget/LinearLayout;

.field private mMontblancWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

.field private mMontblancWallpapersView:Landroid/widget/LinearLayout;

.field private mMyInterestTv:Landroid/widget/TextView;

.field private mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

.field private mPickImageTileLiveView:Landroid/widget/FrameLayout;

.field private mPickImageTileMyInterests:Landroid/widget/FrameLayout;

.field private mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

.field private mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

.field private mSavedImageView:Landroid/widget/LinearLayout;

.field private mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

.field private mSelectedThumb:Landroid/view/View;

.field private mShowDropdownImage:Z

.field private mTextView_1:Landroid/widget/TextView;

.field private mTextView_2:Landroid/widget/TextView;

.field private mTextView_3:Landroid/widget/TextView;

.field private mTextView_4:Landroid/widget/TextView;

.field private mTextView_5:Landroid/widget/TextView;

.field private mThirdPartyListAdapter:Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;

.field private mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

.field private mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

.field private mThumbnailOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mWallpaperStrip:Landroid/view/View;

.field private mWallpapersView:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 141
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ctc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isSkipCTC:Z

    .line 142
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trlteduos"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klteduos"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isSkipMultiSim:Z

    .line 143
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v3, "SEC_FLOATING_FEATURE_COMMON_USE_MULTISIM"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isSkipCTC:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isSkipMultiSim:Z

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z

    .line 145
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->selectedSim:I

    .line 146
    sput v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mpredefinedWallpaper:I

    .line 147
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTTSWallpaper:Z

    .line 148
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z

    .line 149
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z

    .line 150
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mUpdateWallpaperListOnDelete:Z

    .line 151
    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 152
    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mFirstSelect:Z

    .line 153
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    .line 154
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    .line 155
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledPhotoFrame:Z

    .line 156
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledMontblanc:Z

    .line 157
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    .line 158
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    .line 159
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->gInstance:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    return-void

    :cond_1
    move v0, v1

    .line 142
    goto :goto_0

    :cond_2
    move v0, v1

    .line 143
    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;-><init>()V

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIsForTestApp:Z

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mShowDropdownImage:Z

    .line 2799
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledPhotoFrame:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->launchTargetApp(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->changeMenuListVisibility()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Landroid/view/ActionMode;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z

    return p1
.end method

.method static synthetic access$1700()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    return v0
.end method

.method static synthetic access$1702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 106
    sput-boolean p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    return p0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->visibleMenuList()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->invisibleMenuList()V

    return-void
.end method

.method static synthetic access$2100()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z

    return v0
.end method

.method static synthetic access$2200()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z

    return v0
.end method

.method static synthetic access$2202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 106
    sput-boolean p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z

    return p0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View$OnLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method static synthetic access$2400()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mFirstSelect:Z

    return v0
.end method

.method static synthetic access$2402(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 106
    sput-boolean p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mFirstSelect:Z

    return p0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$2700()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Landroid/view/ViewGroup;
    .param p2, "x2"    # Landroid/widget/BaseAdapter;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 106
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->initializeScrollForRtl()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateTileIndices()V

    return-void
.end method

.method static synthetic access$3500()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->checkSim()V

    return-void
.end method

.method static synthetic access$3700()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTTSWallpaper:Z

    return v0
.end method

.method static synthetic access$4000()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    return v0
.end method

.method static synthetic access$4100()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$4302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 106
    sput-boolean p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mUpdateWallpaperListOnDelete:Z

    return p0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mShowDropdownImage:Z

    return v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/content/res/Resources;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Landroid/content/res/Resources;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addMontblancWallpapers(Landroid/content/res/Resources;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setMenuList(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    return-object v0
.end method

.method private addLongPressHandler(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2221
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2222
    return-void
.end method

.method private addMontblancWallpapers(Landroid/content/res/Resources;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 16
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "listResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2848
    new-instance v9, Ljava/util/ArrayList;

    const/16 v13, 0x18

    invoke-direct {v9, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 2849
    .local v9, "montblancWallpapers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;>;"
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 2850
    .local v4, "extras":[Ljava/lang/String;
    const/4 v6, 0x1

    .line 2852
    .local v6, "imageIndex":I
    move-object v2, v4

    .local v2, "arr$":[Ljava/lang/String;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v7, v6

    .end local v6    # "imageIndex":I
    .local v7, "imageIndex":I
    :goto_0
    if-ge v5, v8, :cond_2

    aget-object v3, v2, v5

    .line 2853
    .local v3, "extra":Ljava/lang/String;
    const-string v13, "drawable"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v13, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 2854
    .local v10, "resId":I
    if-eqz v10, :cond_0

    .line 2855
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "_small"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "drawable"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v13, v14, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 2857
    .local v11, "thumbRes":I
    if-eqz v11, :cond_1

    .line 2858
    new-instance v12, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "imageIndex":I
    .restart local v6    # "imageIndex":I
    move-object/from16 v0, p1

    invoke-direct {v12, v0, v10, v13, v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;-><init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;I)V

    .line 2859
    .local v12, "wallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2852
    .end local v11    # "thumbRes":I
    .end local v12    # "wallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;
    :goto_1
    add-int/lit8 v5, v5, 0x1

    move v7, v6

    .end local v6    # "imageIndex":I
    .restart local v7    # "imageIndex":I
    goto :goto_0

    .line 2862
    :cond_0
    const-string v13, "Wallpapers.WallpaperPickerActivity"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Couldn\'t find Montblanc wallpaper "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v6, v7

    .end local v7    # "imageIndex":I
    .restart local v6    # "imageIndex":I
    goto :goto_1

    .line 2866
    .end local v3    # "extra":Ljava/lang/String;
    .end local v6    # "imageIndex":I
    .end local v10    # "resId":I
    .restart local v7    # "imageIndex":I
    :cond_2
    return-object v9
.end method

.method private addTemporaryWallpaperTile(Ljava/lang/String;IZ)Z
    .locals 12
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "orientation"    # I
    .param p3, "multipleImage"    # Z

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 1931
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040006

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout;

    .line 1932
    .local v8, "pickedImageThumbnail":Landroid/widget/FrameLayout;
    invoke-static {v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setWallpaperItemPaddingToZero(Landroid/widget/FrameLayout;)V

    .line 1934
    const v1, 0x7f100023

    invoke-virtual {v8, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1935
    .local v6, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getDefaultThumbnailSize(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v0

    .line 1936
    .local v0, "defaultSize":Landroid/graphics/Point;
    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createThumbnail(Landroid/graphics/Point;Landroid/content/Context;Ljava/lang/String;[BIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1938
    .local v9, "thumb":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_0

    .line 1939
    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1940
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 1941
    .local v10, "thumbDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 1947
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1949
    new-instance v7, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

    invoke-direct {v7, p1, p3, p2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;-><init>(Ljava/lang/String;ZI)V

    .line 1950
    .local v7, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;
    invoke-virtual {v8, v7}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 1951
    invoke-virtual {v7, v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->setView(Landroid/view/View;)V

    .line 1952
    invoke-direct {p0, v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addLongPressHandler(Landroid/view/View;)V

    .line 1953
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateTileIndices()V

    .line 1954
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1955
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v1, v8}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    move v5, v11

    .line 1957
    .end local v7    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;
    .end local v10    # "thumbDrawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return v5

    .line 1943
    :cond_0
    const-string v1, "Wallpapers.WallpaperPickerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error loading thumbnail for path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addWallpapers(Landroid/content/res/Resources;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "listResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2382
    .local p4, "realCscWallpaperList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p5, "cscWallpaperList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    const/16 v10, 0x18

    invoke-direct {v2, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 2383
    .local v2, "bundledWallpapers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;>;"
    move/from16 v0, p3

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 2385
    .local v4, "extras":[Ljava/lang/String;
    move-object v1, v4

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_5

    aget-object v3, v1, v5

    .line 2386
    .local v3, "extra":Ljava/lang/String;
    const-string v10, "drawable"

    invoke-virtual {p1, v3, v10, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 2387
    .local v7, "resId":I
    if-eqz v7, :cond_4

    .line 2388
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_small"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "drawable"

    invoke-virtual {p1, v10, v11, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 2390
    .local v8, "thumbRes":I
    if-eqz v8, :cond_0

    .line 2391
    new-instance v9, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    invoke-virtual {p1, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v9, p1, v7, v10, v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;-><init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    .line 2401
    .local v9, "wallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    if-eqz p4, :cond_1

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_1

    .line 2402
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2404
    const-string v10, "Wallpapers.WallpaperPickerActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "skip add real csc image="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2385
    .end local v8    # "thumbRes":I
    .end local v9    # "wallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2411
    .restart local v8    # "thumbRes":I
    .restart local v9    # "wallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    :cond_1
    if-eqz p5, :cond_3

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_3

    .line 2412
    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isInCscWallpaperList(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 2414
    const-string v10, "Wallpapers.WallpaperPickerActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "skip add cscWallpaperList image="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2418
    :cond_2
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2424
    :cond_3
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2428
    .end local v8    # "thumbRes":I
    .end local v9    # "wallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    :cond_4
    const-string v10, "Wallpapers.WallpaperPickerActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Couldn\'t find wallpaper "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2431
    .end local v3    # "extra":Ljava/lang/String;
    .end local v7    # "resId":I
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    sput v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mpredefinedWallpaper:I

    .line 2432
    return-object v2
.end method

.method private animateHelpDialogs(Landroid/widget/ImageView;Landroid/view/View;)V
    .locals 3
    .param p1, "tapImage"    # Landroid/widget/ImageView;
    .param p2, "layout"    # Landroid/view/View;

    .prologue
    .line 1634
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 1635
    .local v0, "animdialog":Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->start()V

    .line 1636
    const v1, 0x3f19999a    # 0.6f

    const v2, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1, v2, p2}, Lcom/sec/android/app/wallpaperchooser/HelpAnimatedDialog;->startZoom(FFLandroid/view/View;)V

    .line 1637
    return-void
.end method

.method private changeMenuListVisibility()V
    .locals 1

    .prologue
    .line 1235
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    if-eqz v0, :cond_0

    .line 1236
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V

    .line 1239
    :goto_0
    return-void

    .line 1238
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->invisibleMenuList()V

    goto :goto_0
.end method

.method private checkDownloadWallpapers()Z
    .locals 1

    .prologue
    .line 2796
    const/4 v0, 0x0

    return v0
.end method

.method private checkSim()V
    .locals 3

    .prologue
    .line 2588
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z

    if-eqz v2, :cond_0

    .line 2589
    const/4 v1, 0x0

    .line 2590
    .local v1, "isSelected":Z
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->selectSimByNetwork(Landroid/content/Context;)Z

    move-result v1

    .line 2591
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2592
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 2593
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 2594
    .local v0, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    invoke-virtual {v0, p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 2600
    .end local v0    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .end local v1    # "isSelected":Z
    :cond_0
    :goto_0
    return-void

    .line 2597
    .restart local v1    # "isSelected":Z
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->displaySimSelectorDialog()V

    goto :goto_0
.end method

.method private checkSviewCover()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    .line 2657
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getTypeOfCover(Landroid/content/Context;)I

    move-result v0

    .line 2658
    .local v0, "covertype":I
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getModelOfCover()I

    move-result v2

    if-eq v2, v3, :cond_0

    if-eq v0, v1, :cond_1

    :cond_0
    if-eq v0, v3, :cond_1

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    .line 2661
    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private closeMenuList()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1298
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1299
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1300
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mShowDropdownImage:Z

    if-eqz v0, :cond_0

    .line 1301
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1304
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v0, :cond_2

    .line 1305
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1306
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v0, :cond_1

    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1313
    :cond_1
    :goto_0
    sput-boolean v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 1314
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1315
    return-void

    .line 1309
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v0, :cond_1

    .line 1310
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static createImageTileView(Landroid/view/LayoutInflater;ILandroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;Z)Landroid/view/View;
    .locals 6
    .param p0, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p5, "multipleImage"    # Z

    .prologue
    const/4 v5, 0x0

    .line 2505
    if-nez p2, :cond_2

    .line 2506
    const v4, 0x7f040006

    invoke-virtual {p0, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .local v2, "view":Landroid/view/View;
    :goto_0
    move-object v4, v2

    .line 2510
    check-cast v4, Landroid/widget/FrameLayout;

    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setWallpaperItemPaddingToZero(Landroid/widget/FrameLayout;)V

    .line 2512
    const v4, 0x7f100023

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2514
    .local v0, "image":Landroid/widget/ImageView;
    if-eqz p4, :cond_0

    .line 2515
    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2516
    const/4 v4, 0x1

    invoke-virtual {p4, v4}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 2519
    :cond_0
    const v4, 0x7f100026

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2520
    .local v1, "multipleIcon":Landroid/widget/ImageView;
    if-eqz p5, :cond_3

    .line 2521
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2525
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTTSWallpaper:Z

    if-eqz v4, :cond_1

    .line 2526
    invoke-static {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperDescription(I)Ljava/lang/String;

    move-result-object v3

    .line 2527
    .local v3, "wallpaperDescription":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 2528
    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2532
    .end local v3    # "wallpaperDescription":Ljava/lang/String;
    :cond_1
    return-object v2

    .line 2508
    .end local v0    # "image":Landroid/widget/ImageView;
    .end local v1    # "multipleIcon":Landroid/widget/ImageView;
    .end local v2    # "view":Landroid/view/View;
    :cond_2
    move-object v2, p2

    .restart local v2    # "view":Landroid/view/View;
    goto :goto_0

    .line 2523
    .restart local v0    # "image":Landroid/widget/ImageView;
    .restart local v1    # "multipleIcon":Landroid/widget/ImageView;
    :cond_3
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public static createThumbnail(Landroid/graphics/Point;Landroid/content/Context;Ljava/lang/String;[BIZ)Landroid/graphics/Bitmap;
    .locals 19
    .param p0, "size"    # Landroid/graphics/Point;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "imageBytes"    # [B
    .param p4, "rotation"    # I
    .param p5, "leftAligned"    # Z

    .prologue
    .line 1888
    move-object/from16 v0, p0

    iget v6, v0, Landroid/graphics/Point;->x:I

    .line 1889
    .local v6, "width":I
    move-object/from16 v0, p0

    iget v7, v0, Landroid/graphics/Point;->y:I

    .line 1890
    .local v7, "height":I
    const/4 v14, 0x0

    .line 1891
    .local v14, "outByteArray":[B
    if-eqz p2, :cond_1

    if-nez p3, :cond_1

    .line 1892
    invoke-static/range {p2 .. p2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 1893
    .local v17, "thumb":Landroid/graphics/Bitmap;
    if-nez v17, :cond_0

    .line 1894
    const/4 v3, 0x0

    .line 1927
    .end local v17    # "thumb":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    .line 1895
    .restart local v17    # "thumb":Landroid/graphics/Bitmap;
    :cond_0
    const-string v3, "jpg"

    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v12

    .line 1896
    .local v12, "cf":Landroid/graphics/Bitmap$CompressFormat;
    new-instance v18, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x800

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 1897
    .local v18, "tmpOut":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x5a

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v12, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1898
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v14

    .line 1902
    .end local v12    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .end local v17    # "thumb":Landroid/graphics/Bitmap;
    .end local v18    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    :cond_1
    if-eqz p3, :cond_3

    .line 1903
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v3, p3

    move/from16 v5, p4

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;-><init>([BLandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 1909
    .local v2, "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->getImageBounds()Landroid/graphics/Point;

    move-result-object v11

    .line 1910
    .local v11, "bounds":Landroid/graphics/Point;
    if-eqz v11, :cond_2

    iget v3, v11, Landroid/graphics/Point;->x:I

    if-eqz v3, :cond_2

    iget v3, v11, Landroid/graphics/Point;->y:I

    if-nez v3, :cond_5

    .line 1911
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 1904
    .end local v2    # "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    .end local v11    # "bounds":Landroid/graphics/Point;
    :cond_3
    if-eqz v14, :cond_4

    .line 1905
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v3, v14

    move/from16 v5, p4

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;-><init>([BLandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .restart local v2    # "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    goto :goto_1

    .line 1907
    .end local v2    # "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    .line 1913
    .restart local v2    # "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    .restart local v11    # "bounds":Landroid/graphics/Point;
    :cond_5
    new-instance v15, Landroid/graphics/Matrix;

    invoke-direct {v15}, Landroid/graphics/Matrix;-><init>()V

    .line 1914
    .local v15, "rotateMatrix":Landroid/graphics/Matrix;
    move/from16 v0, p4

    int-to-float v3, v0

    invoke-virtual {v15, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 1915
    const/4 v3, 0x2

    new-array v0, v3, [F

    move-object/from16 v16, v0

    const/4 v3, 0x0

    iget v4, v11, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    aput v4, v16, v3

    const/4 v3, 0x1

    iget v4, v11, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    aput v4, v16, v3

    .line 1916
    .local v16, "rotatedBounds":[F
    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1917
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, v16, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v16, v3

    .line 1918
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget v4, v16, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v16, v3

    .line 1920
    const/4 v3, 0x0

    aget v3, v16, v3

    float-to-int v3, v3

    const/4 v4, 0x1

    aget v4, v16, v4

    float-to-int v4, v4

    move/from16 v0, p5

    invoke-static {v3, v4, v6, v7, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getMaxCropRect(IIIIZ)Landroid/graphics/RectF;

    move-result-object v13

    .line 1922
    .local v13, "cropRect":Landroid/graphics/RectF;
    invoke-virtual {v2, v13}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setCropBounds(Landroid/graphics/RectF;)V

    .line 1924
    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->cropBitmap()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1925
    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->getCroppedBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    goto/16 :goto_0

    .line 1927
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method private displaySimSelectorDialog()V
    .locals 3

    .prologue
    .line 2574
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-nez v1, :cond_0

    .line 2575
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;-><init>(Landroid/content/Context;)V

    .line 2576
    .local v0, "simSelectorDataAdapter":Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c0007

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2585
    .end local v0    # "simSelectorDataAdapter":Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;
    :cond_0
    return-void
.end method

.method private findBundledWallpapers(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2225
    .local p1, "customerImageNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "cscWallpaperList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    const/16 v0, 0x18

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2227
    .local v6, "bundledWallpapers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperArrayResourceId()Landroid/util/Pair;

    move-result-object v9

    .line 2228
    .local v9, "r":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ApplicationInfo;Ljava/lang/Integer;>;"
    if-eqz v9, :cond_0

    .line 2230
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v1

    .line 2231
    .local v1, "wallpaperRes":Landroid/content/res/Resources;
    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addWallpapers(Landroid/content/res/Resources;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 2238
    .end local v1    # "wallpaperRes":Landroid/content/res/Resources;
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-nez v0, :cond_1

    .line 2239
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getDefaultWallpaperInfo()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    move-result-object v7

    .line 2240
    .local v7, "defaultWallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    if-eqz v7, :cond_1

    .line 2241
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2244
    .end local v7    # "defaultWallpaperInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    :cond_1
    return-object v6

    .line 2232
    :catch_0
    move-exception v8

    .line 2233
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private findWallpapers()V
    .locals 2

    .prologue
    .line 2554
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getInstance(Ljava/lang/String;I)Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    .line 2561
    return-void
.end method

.method public static getDefaultThumbnailSize(Landroid/content/res/Resources;)Landroid/graphics/Point;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1847
    new-instance v0, Landroid/graphics/Point;

    const/high16 v1, 0x7f0a0000

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0a0001

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method private getDefaultWallpaperInfo()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    .locals 23

    .prologue
    .line 2248
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v19

    .line 2249
    .local v19, "sysRes":Landroid/content/res/Resources;
    const-string v4, "default_wallpaper"

    const-string v6, "drawable"

    const-string v8, "android"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v6, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 2250
    .local v17, "resId":I
    const-string v4, "default_thumb"

    const-string v6, "drawable"

    const-string v8, "android"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v6, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v18

    .line 2252
    .local v18, "resIdThumb":I
    new-instance v10, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v6, "default_thumb.jpg"

    invoke-direct {v10, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2253
    .local v10, "defaultThumbFile":Ljava/io/File;
    const/16 v20, 0x0

    .line 2254
    .local v20, "thumb":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    .line 2255
    .local v5, "path":Ljava/lang/String;
    const/16 v22, 0x0

    .line 2256
    .local v22, "uri":Landroid/net/Uri;
    const/4 v11, 0x0

    .line 2259
    .local v11, "defaultWallpaperExists":Z
    const-string v14, "system/wallpaper/default_wallpaper/default_wallpaper.png"

    .line 2260
    .local v14, "path_1":Ljava/lang/String;
    const-string v15, "system/wallpaper/default_wallpaper/default_wallpaper.jpg"

    .line 2261
    .local v15, "path_2":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2262
    .local v12, "defaultWallpaperFileInCSC":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2263
    move-object v5, v14

    .line 2273
    :goto_0
    if-eqz v5, :cond_0

    .line 2274
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v22

    .line 2276
    :cond_0
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2277
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2278
    const/4 v11, 0x1

    .line 2306
    :goto_1
    if-eqz v11, :cond_6

    .line 2307
    new-instance v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-direct {v6, v8, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v22

    invoke-direct {v4, v0, v1, v6, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;-><init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    .line 2321
    :goto_2
    return-object v4

    .line 2265
    :cond_1
    new-instance v12, Ljava/io/File;

    .end local v12    # "defaultWallpaperFileInCSC":Ljava/io/File;
    invoke-direct {v12, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2266
    .restart local v12    # "defaultWallpaperFileInCSC":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2267
    move-object v5, v15

    goto :goto_0

    .line 2269
    :cond_2
    const-string v4, "Wallpapers.WallpaperPickerActivity"

    const-string v6, "No default_wallpaper in CSC(system/wallpaper/default_wallpaper/)"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2280
    :cond_3
    const-string v4, "Wallpapers.WallpaperPickerActivity"

    const-string v6, "getDefaultWallpaperInfo() - no \'default_thumb.jpg\' file, create thumbnail."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2281
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 2282
    .local v16, "res":Landroid/content/res/Resources;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getDefaultThumbnailSize(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v3

    .line 2284
    .local v3, "defaultThumbSize":Landroid/graphics/Point;
    if-eqz v5, :cond_4

    .line 2285
    const/4 v7, 0x0

    .line 2286
    .local v7, "rotation":I
    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p0

    invoke-static/range {v3 .. v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createThumbnail(Landroid/graphics/Point;Landroid/content/Context;Ljava/lang/String;[BIZ)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 2289
    .end local v7    # "rotation":I
    :cond_4
    if-eqz v20, :cond_5

    .line 2291
    :try_start_0
    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    .line 2292
    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v21

    .line 2294
    .local v21, "thumbFileStream":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x5f

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2295
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2296
    const/4 v11, 0x1

    goto :goto_1

    .line 2297
    .end local v21    # "thumbFileStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v13

    .line 2298
    .local v13, "e":Ljava/io/IOException;
    const-string v4, "Wallpapers.WallpaperPickerActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error while writing default wallpaper thumbnail to file "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2299
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 2302
    .end local v13    # "e":Ljava/io/IOException;
    :cond_5
    const-string v4, "Wallpapers.WallpaperPickerActivity"

    const-string v6, "Could not create thumbnail bitmap. Cannot create default_thumb tile."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2309
    .end local v3    # "defaultThumbSize":Landroid/graphics/Point;
    .end local v16    # "res":Landroid/content/res/Resources;
    :cond_6
    const/4 v9, 0x0

    .line 2311
    .local v9, "d":Landroid/graphics/drawable/Drawable;
    :try_start_1
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v9

    .line 2318
    :cond_7
    :goto_3
    if-eqz v9, :cond_8

    .line 2319
    new-instance v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v22

    invoke-direct {v4, v0, v1, v9, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;-><init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    goto/16 :goto_2

    .line 2312
    :catch_1
    move-exception v13

    .line 2313
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 2314
    if-nez v9, :cond_7

    .line 2315
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto :goto_3

    .line 2321
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public static getDisplayHeight(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2007
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2008
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 2009
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2010
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v2
.end method

.method public static getDisplayWidth(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2000
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2001
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 2002
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2003
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v2
.end method

.method private getLogExtra(I)Ljava/lang/String;
    .locals 1
    .param p1, "extraValue"    # I

    .prologue
    .line 2906
    const/4 v0, 0x0

    .line 2908
    .local v0, "extra":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 2930
    :goto_0
    return-object v0

    .line 2910
    :sswitch_0
    const-string v0, "Default image"

    .line 2911
    goto :goto_0

    .line 2914
    :sswitch_1
    const-string v0, "From Gallery"

    .line 2915
    goto :goto_0

    .line 2918
    :sswitch_2
    const-string v0, "Photo frame"

    .line 2919
    goto :goto_0

    .line 2922
    :sswitch_3
    const-string v0, "Live view"

    .line 2923
    goto :goto_0

    .line 2926
    :sswitch_4
    const-string v0, "My interests"

    goto :goto_0

    .line 2908
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0xb -> :sswitch_3
        0xc -> :sswitch_4
    .end sparse-switch
.end method

.method private static getModelOfCover()I
    .locals 5

    .prologue
    .line 2695
    const/4 v1, 0x0

    .line 2696
    .local v1, "model":I
    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2697
    .local v0, "mProductName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isJapanModel()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2698
    const-string v2, "SC-01G"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SCL24"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2699
    :cond_0
    const-string v0, "tblte"

    .line 2702
    :cond_1
    const-string v2, "tblte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "tbhplte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "tbelte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2704
    :cond_2
    const/4 v1, 0x3

    .line 2705
    :cond_3
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Scover getModelOfCover model : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2707
    return v1
.end method

.method public static getSimSelectedID()I
    .locals 1

    .prologue
    .line 2603
    sget v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->selectedSim:I

    return v0
.end method

.method private static getTypeOfCover(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2675
    const/4 v2, 0x2

    .line 2676
    .local v2, "type":I
    new-instance v0, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    .line 2677
    .local v0, "covermanager":Lcom/samsung/android/sdk/cover/ScoverManager;
    if-eqz v0, :cond_0

    .line 2678
    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v1

    .line 2679
    .local v1, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    if-eqz v1, :cond_1

    .line 2680
    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v2

    .line 2684
    .end local v1    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    :cond_0
    :goto_0
    const-string v3, "Wallpapers.WallpaperPickerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Scover getTypeOfCover type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2686
    return v2

    .line 2682
    .restart local v1    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    :cond_1
    const-string v3, "Wallpapers.WallpaperPickerActivity"

    const-string v4, "Scover getTypeOfCover ScoverState is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getWallpaperDescription(I)Ljava/lang/String;
    .locals 3
    .param p0, "position"    # I

    .prologue
    .line 2564
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 2566
    .local v0, "wallpaperDescription":[Ljava/lang/String;
    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 2567
    aget-object v1, v0, p0

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2569
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initializeScrollForRtl()V
    .locals 4

    .prologue
    .line 1721
    const v2, 0x7f100010

    invoke-virtual {p0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/HorizontalScrollView;

    .line 1723
    .local v1, "scroll":Landroid/widget/HorizontalScrollView;
    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getLayoutDirection()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1724
    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1725
    .local v0, "observer":Landroid/view/ViewTreeObserver;
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$15;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$15;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/widget/HorizontalScrollView;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1733
    .end local v0    # "observer":Landroid/view/ViewTreeObserver;
    :cond_0
    return-void
.end method

.method public static instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
    .locals 1

    .prologue
    .line 2536
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->gInstance:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    return-object v0
.end method

.method private invisibleMenuList()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1263
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1264
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->cancel()V

    .line 1268
    :cond_0
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->alpha(F)Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->start()V

    .line 1273
    return-void
.end method

.method private isInCscWallpaperList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 2436
    const/16 v0, 0x18

    .line 2437
    .local v0, "MAX_ARRAY":I
    const/4 v1, 0x0

    .line 2438
    .local v1, "cscImageName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 2439
    .local v3, "isIn":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 2440
    sget-object v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCscImageSmallFileName(IZ)Ljava/lang/String;

    move-result-object v1

    .line 2441
    if-eqz v1, :cond_0

    .line 2442
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2443
    const/4 v3, 0x1

    .line 2439
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2447
    :cond_1
    return v3
.end method

.method private static isJapanModel()Z
    .locals 2

    .prologue
    .line 2711
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2712
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "DCM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KDI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "XJP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SBM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSViewCover2014Supported()Z
    .locals 4

    .prologue
    .line 2643
    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2644
    .local v0, "mProductName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2645
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 2647
    .local v2, "sViewCoverAppLevel":I
    if-eqz v1, :cond_0

    .line 2648
    const-string v3, "com.sec.feature.cover.sviewcover"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v2

    .line 2650
    :cond_0
    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const-string v3, "slte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "a5lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "a53g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2651
    :cond_1
    const/4 v3, 0x1

    .line 2653
    :goto_0
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private launchTargetApp(Ljava/lang/String;I)V
    .locals 2
    .param p1, "activityName"    # Ljava/lang/String;
    .param p2, "requestCode"    # I

    .prologue
    .line 2607
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2608
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2609
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v1

    invoke-static {v1, v0, p2}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 2610
    return-void
.end method

.method private loadActivityLabel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 2613
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 2614
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 2615
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    const/4 v1, 0x0

    .line 2618
    .local v1, "activityLabel":Ljava/lang/String;
    :try_start_0
    new-instance v4, Landroid/content/ComponentName;

    invoke-direct {v4, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 2619
    invoke-virtual {v0, v3}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2624
    :goto_0
    return-object v1

    .line 2620
    :catch_0
    move-exception v2

    .line 2621
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private makeIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2014
    const/16 v8, 0x400

    .line 2016
    .local v8, "type":I
    sget v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    if-nez v10, :cond_1

    .line 2017
    const/16 v8, 0x400

    .line 2023
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperDesiredMinimumWidth()I

    move-result v9

    .line 2024
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperDesiredMinimumHeight()I

    move-result v2

    .line 2025
    .local v2, "height":I
    if-le v9, v2, :cond_3

    move v3, v9

    .line 2026
    .local v3, "longLength":I
    :goto_1
    const/4 v6, 0x0

    .line 2027
    .local v6, "spotlightX":F
    const/4 v7, 0x0

    .line 2028
    .local v7, "spotlightY":F
    const/16 v10, 0x402

    if-ne v8, v10, :cond_4

    .line 2029
    invoke-static {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getDisplayWidth(Landroid/content/Context;)I

    move-result v10

    int-to-float v6, v10

    .line 2030
    invoke-static {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getDisplayHeight(Landroid/content/Context;)I

    move-result v10

    int-to-float v7, v10

    .line 2039
    :goto_2
    new-instance v0, Landroid/content/ComponentName;

    const-string v10, "com.sec.android.gallery3d"

    const-string v11, "com.sec.android.gallery3d.app.CropImage"

    invoke-direct {v0, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v11, Landroid/content/Intent;

    const/16 v10, 0x400

    if-ne v8, v10, :cond_5

    const-string v10, "com.android.camera.action.CROP"

    :goto_3
    invoke-direct {v11, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "image/*"

    invoke-virtual {v10, p1, v11}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "outputX"

    invoke-virtual {v10, v11, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "outputY"

    invoke-virtual {v10, v11, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "aspectX"

    invoke-virtual {v10, v11, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "aspectY"

    invoke-virtual {v10, v11, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "spotlightX"

    invoke-virtual {v10, v11, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "spotlightY"

    invoke-virtual {v10, v11, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "scale"

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "noFaceDetection"

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v11

    const-string v12, "set-as-wallpaper"

    const/16 v10, 0x400

    if-ne v8, v10, :cond_6

    const/4 v10, 0x1

    :goto_4
    invoke-virtual {v11, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v11

    const-string v12, "set-as-lockscreen"

    const/16 v10, 0x401

    if-ne v8, v10, :cond_7

    const/4 v10, 0x1

    :goto_5
    invoke-virtual {v11, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v11

    const-string v12, "set-as-bothscreen"

    const/16 v10, 0x402

    if-ne v8, v10, :cond_8

    const/4 v10, 0x1

    :goto_6
    invoke-virtual {v11, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    .line 2055
    .local v5, "request":Landroid/content/Intent;
    return-object v5

    .line 2018
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v2    # "height":I
    .end local v3    # "longLength":I
    .end local v5    # "request":Landroid/content/Intent;
    .end local v6    # "spotlightX":F
    .end local v7    # "spotlightY":F
    .end local v9    # "width":I
    :cond_1
    sget v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    .line 2019
    const/16 v8, 0x401

    goto/16 :goto_0

    .line 2020
    :cond_2
    sget v10, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_0

    .line 2021
    const/16 v8, 0x402

    goto/16 :goto_0

    .restart local v2    # "height":I
    .restart local v9    # "width":I
    :cond_3
    move v3, v2

    .line 2025
    goto/16 :goto_1

    .line 2032
    .restart local v3    # "longLength":I
    .restart local v6    # "spotlightX":F
    .restart local v7    # "spotlightY":F
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 2033
    .local v1, "display":Landroid/view/Display;
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 2034
    .local v4, "outSize":Landroid/graphics/Point;
    invoke-virtual {v1, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2035
    iget v10, v4, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    int-to-float v11, v3

    div-float v6, v10, v11

    .line 2036
    iget v10, v4, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    int-to-float v11, v3

    div-float v7, v10, v11

    goto/16 :goto_2

    .line 2041
    .end local v1    # "display":Landroid/view/Display;
    .end local v4    # "outSize":Landroid/graphics/Point;
    .restart local v0    # "cn":Landroid/content/ComponentName;
    :cond_5
    const-string v10, "com.sed.android.gallery3d.CROP_SEC_ONLY"

    goto/16 :goto_3

    :cond_6
    const/4 v10, 0x0

    goto :goto_4

    :cond_7
    const/4 v10, 0x0

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    goto :goto_6
.end method

.method private populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "adapter"    # Landroid/widget/BaseAdapter;
    .param p3, "addLongPressHandler"    # Z
    .param p4, "selectFirstTile"    # Z
    .param p5, "lastIndex"    # I
    .param p6, "selectLastTile"    # Z

    .prologue
    .line 1786
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_6

    .line 1787
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3, p1}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 1788
    .local v2, "thumbnail":Landroid/widget/FrameLayout;
    if-nez v2, :cond_1

    .line 1786
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1790
    :cond_1
    invoke-virtual {p1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1791
    invoke-virtual {p2, v0}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1792
    .local v1, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 1793
    invoke-virtual {v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->setView(Landroid/view/View;)V

    .line 1794
    if-eqz p3, :cond_2

    .line 1795
    invoke-direct {p0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addLongPressHandler(Landroid/view/View;)V

    .line 1797
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1798
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1799
    if-gez p5, :cond_3

    if-nez v0, :cond_3

    if-nez p4, :cond_5

    :cond_3
    if-ltz p5, :cond_4

    if-ne v0, p5, :cond_4

    if-nez p4, :cond_5

    :cond_4
    if-ltz p5, :cond_0

    if-ne v0, p5, :cond_0

    if-eqz p6, :cond_0

    .line 1802
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v3, v2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 1805
    .end local v1    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .end local v2    # "thumbnail":Landroid/widget/FrameLayout;
    :cond_6
    return-void
.end method

.method private returnForGetWallpaperListAction()V
    .locals 3

    .prologue
    .line 1658
    iget-boolean v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIsForTestApp:Z

    if-eqz v2, :cond_0

    .line 1660
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->getCount()I

    move-result v1

    .line 1671
    .local v1, "mNumOfImage":I
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1672
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "mNumOfImage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1674
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1675
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    .line 1677
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mNumOfImage":I
    :cond_0
    return-void
.end method

.method private sendSurveyLog(I)V
    .locals 5
    .param p1, "extraValue"    # I

    .prologue
    .line 2891
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2892
    .local v0, "cv":Landroid/content/ContentValues;
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLogExtra(I)Ljava/lang/String;

    move-result-object v1

    .line 2894
    .local v1, "extra":Ljava/lang/String;
    const-string v3, "app_id"

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2895
    const-string v3, "feature"

    const-string v4, "WPSF"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2896
    const-string v3, "extra"

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2898
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2899
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "com.samsung.android.providers.context.log.action.REPORT_APP_STATUS_SURVEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2900
    const-string v3, "data"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2901
    const-string v3, "com.samsung.android.providers.context"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2902
    invoke-virtual {p0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2903
    return-void
.end method

.method private setMenuList(Landroid/widget/TextView;)V
    .locals 3
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    const/16 v1, 0x8

    .line 1318
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-nez v0, :cond_0

    .line 1319
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1320
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->spanMenuList()V

    .line 1359
    :goto_0
    return-void

    .line 1325
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1327
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->spanMenuList()V

    goto :goto_0

    .line 1332
    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1333
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 1334
    sget v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMenuList(I)V

    .line 1335
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateHistoryWallpapersView()V

    .line 1336
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledMontblanc:Z

    if-eqz v0, :cond_2

    .line 1337
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMontblancWallpapersView()V

    .line 1358
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V

    goto :goto_0

    .line 1338
    :cond_3
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1339
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 1340
    sget v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMenuList(I)V

    .line 1341
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateHistoryWallpapersView()V

    .line 1342
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledMontblanc:Z

    if-eqz v0, :cond_2

    .line 1343
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMontblancWallpapersView()V

    goto :goto_1

    .line 1344
    :cond_4
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1345
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 1346
    sget v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMenuList(I)V

    .line 1347
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateHistoryWallpapersView()V

    .line 1348
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledMontblanc:Z

    if-eqz v0, :cond_2

    .line 1349
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMontblancWallpapersView()V

    goto :goto_1

    .line 1350
    :cond_5
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1351
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->startSviewSetting()V

    goto/16 :goto_0

    .line 1353
    :cond_6
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1354
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->startDownloadWallpapers()V

    goto/16 :goto_0
.end method

.method static setWallpaperItemPaddingToZero(Landroid/widget/FrameLayout;)V
    .locals 2
    .param p0, "frameLayout"    # Landroid/widget/FrameLayout;

    .prologue
    const/4 v0, 0x0

    .line 2216
    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 2217
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ZeroPaddingDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ZeroPaddingDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2218
    return-void
.end method

.method private showHelpDialog_start()V
    .locals 21

    .prologue
    .line 1519
    const/16 v19, 0x0

    sput-boolean v19, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z

    .line 1521
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v19, -0x2

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1522
    .local v10, "lptext":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v19, -0x2

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1523
    .local v9, "lp_Bubble":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v19, -0x2

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1525
    .local v11, "lptext1":Landroid/widget/RelativeLayout$LayoutParams;
    const v19, 0x7f10001e

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 1526
    .local v15, "pointerImage":Landroid/widget/ImageView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0002

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v16

    .line 1527
    .local v16, "pointerLeftMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0003

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    .line 1528
    .local v14, "pointerBottomMargin":I
    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v10, v0, v14, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1529
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 1530
    invoke-virtual {v15, v10}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1531
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1533
    const v19, 0x7f10001c

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 1535
    .local v13, "mainLayout_firststep":Landroid/widget/LinearLayout;
    const v19, 0x7f10001d

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 1536
    .local v4, "bubble_pointer":Landroid/widget/ImageView;
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1537
    const v19, 0x7f100020

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1538
    .local v5, "bubble_text":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0004

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v17

    .line 1539
    .local v17, "selectScreenLeftMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0005

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    .line 1540
    .local v18, "selectScreenTopMargin":I
    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1541
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 1542
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1543
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1546
    const v19, 0x7f10001f

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1547
    .local v8, "image_pointer":Landroid/widget/ImageView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0006

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 1548
    .local v6, "downPointerLeftMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0007

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 1549
    .local v7, "downPointerTopMargin":I
    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v11, v6, v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1550
    invoke-virtual {v11, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 1551
    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1552
    invoke-virtual {v8}, Landroid/widget/ImageView;->bringToFront()V

    .line 1553
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1555
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->animateHelpDialogs(Landroid/widget/ImageView;Landroid/view/View;)V

    .line 1556
    const v19, 0x7f100007

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1557
    .local v12, "mTextView1":Landroid/widget/TextView;
    new-instance v19, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$14;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$14;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1566
    return-void
.end method

.method private spanMenuList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1276
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-nez v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1280
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    const v1, 0x7f02000c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1282
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v0, :cond_2

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1284
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v0, :cond_1

    .line 1285
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1291
    :cond_1
    :goto_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 1293
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1294
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1295
    return-void

    .line 1287
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v0, :cond_1

    .line 1288
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private startDownloadWallpapers()V
    .locals 4

    .prologue
    .line 2781
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "http://waprd.telstra.com/redirect?target=3glatestpics"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2784
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    const/16 v3, 0xe

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2789
    :goto_0
    return-void

    .line 2785
    :catch_0
    move-exception v0

    .line 2786
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Activity Not Found for download wallpapers option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2787
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startSviewSetting()V
    .locals 4

    .prologue
    .line 2628
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2629
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isSViewCover2014Supported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2630
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.SViewColor2014"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2635
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    const/16 v3, 0x9

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2640
    :goto_1
    return-void

    .line 2632
    :cond_0
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.SViewColor"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 2636
    :catch_0
    move-exception v0

    .line 2637
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Activity Not Found for S View cover option"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2638
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private updateHistoryWallpapersView()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    .line 2717
    const/4 v10, 0x0

    .local v10, "oldImageCount":I
    const/4 v9, 0x0

    .line 2718
    .local v9, "newImageCount":I
    const/4 v7, 0x0

    .local v7, "index":I
    const/4 v8, -0x1

    .local v8, "multipleIndex":I
    const/4 v5, -0x1

    .line 2720
    .local v5, "lastIndex":I
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    if-eqz v0, :cond_0

    .line 2721
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getCount()I

    move-result v10

    .line 2722
    const/4 v7, 0x0

    :goto_0
    if-ge v7, v10, :cond_0

    .line 2723
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getItem(I)Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->isMultipleImage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2724
    move v8, v7

    .line 2730
    :cond_0
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    .line 2731
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 2732
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->loadThumbnailsAndImageIdList()V

    .line 2733
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getCount()I

    move-result v9

    .line 2735
    const/4 v7, 0x0

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 2736
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2737
    if-ne v10, v9, :cond_4

    .line 2738
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getItem(I)Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->isMultipleImage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2739
    add-int/lit8 v5, v7, 0x1

    .line 2771
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    if-nez v0, :cond_a

    .line 2772
    const v0, 0x7f100012

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    .line 2776
    :goto_3
    const-string v0, "Wallpapers.WallpaperPickerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateHistoryWallpapersView() : lastIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2777
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    const/4 v4, 0x0

    move-object v0, p0

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 2778
    return-void

    .line 2722
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 2741
    :cond_3
    move v5, v7

    .line 2742
    goto :goto_2

    .line 2745
    :cond_4
    if-le v10, v9, :cond_6

    .line 2746
    const/4 v0, -0x1

    if-eq v8, v0, :cond_5

    const/4 v0, 0x2

    if-eq v8, v0, :cond_5

    .line 2747
    add-int/lit8 v5, v7, -0x1

    .line 2748
    if-gez v5, :cond_1

    .line 2749
    const/4 v5, 0x0

    goto :goto_2

    .line 2751
    :cond_5
    move v5, v7

    goto :goto_2

    .line 2753
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getItem(I)Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->isMultipleImage()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2754
    add-int/lit8 v5, v7, 0x1

    goto :goto_2

    .line 2756
    :cond_7
    move v5, v7

    .line 2757
    const/4 v7, 0x0

    :goto_4
    if-ge v7, v9, :cond_1

    .line 2758
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getItem(I)Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->isMultipleImage()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2759
    if-ge v7, v5, :cond_1

    .line 2760
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2757
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 2735
    :cond_9
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 2774
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_3
.end method

.method private updateMenuList(I)V
    .locals 8
    .param p1, "type"    # I

    .prologue
    const v7, 0x7f0c0009

    const v6, 0x7f0c0008

    const/4 v5, 0x0

    const v3, 0x7f0c000c

    const/16 v4, 0x8

    .line 1362
    packed-switch p1, :pswitch_data_0

    .line 1516
    :goto_0
    return-void

    .line 1364
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1365
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1366
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1367
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v1, :cond_6

    .line 1368
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1369
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_0

    .line 1370
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1373
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 1375
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    .line 1376
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1377
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_2

    .line 1378
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1379
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    .line 1380
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1381
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_4

    .line 1382
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1383
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_5

    .line 1384
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1386
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1387
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 1371
    :cond_6
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_0

    .line 1372
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 1391
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-nez v1, :cond_7

    .line 1392
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1393
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1394
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1396
    :cond_7
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v1, :cond_11

    .line 1397
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1398
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_8

    .line 1399
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1403
    :cond_8
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 1405
    const/4 v0, 0x1

    .line 1406
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_a

    .line 1407
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    if-eq v1, v2, :cond_9

    .line 1408
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1409
    :cond_9
    add-int/lit8 v0, v0, 0x1

    .line 1412
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_c

    .line 1413
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    if-eq v1, v2, :cond_b

    .line 1414
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1415
    :cond_b
    add-int/lit8 v0, v0, 0x1

    .line 1418
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_e

    .line 1419
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    if-eq v1, v2, :cond_d

    .line 1420
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1421
    :cond_d
    add-int/lit8 v0, v0, 0x1

    .line 1426
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_f

    .line 1427
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    if-eq v1, v2, :cond_f

    .line 1428
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1431
    :cond_f
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_10

    .line 1432
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 1433
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    if-eq v1, v2, :cond_10

    .line 1434
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1436
    :cond_10
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1437
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1400
    .end local v0    # "index":I
    :cond_11
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_8

    .line 1401
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 1441
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1442
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1443
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1444
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v1, :cond_18

    .line 1445
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1446
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_12

    .line 1447
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1450
    :cond_12
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 1452
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_13

    .line 1453
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1454
    :cond_13
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_14

    .line 1455
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1456
    :cond_14
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_15

    .line 1457
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1458
    :cond_15
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_16

    .line 1459
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1460
    :cond_16
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_17

    .line 1461
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1463
    :cond_17
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1464
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1448
    :cond_18
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_12

    .line 1449
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 1468
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1469
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1470
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1471
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1472
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v1, :cond_19

    .line 1473
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1474
    :cond_19
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1476
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1a

    .line 1477
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1478
    :cond_1a
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1b

    .line 1479
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1480
    :cond_1b
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1c

    .line 1481
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1482
    :cond_1c
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1d

    .line 1483
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1484
    :cond_1d
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1e

    .line 1485
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1487
    :cond_1e
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1488
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1492
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1493
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1494
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1495
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1496
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v1, :cond_1f

    .line 1497
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1498
    :cond_1f
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1500
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_20

    .line 1501
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1502
    :cond_20
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_21

    .line 1503
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1504
    :cond_21
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_22

    .line 1505
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1506
    :cond_22
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_23

    .line 1507
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1508
    :cond_23
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_24

    .line 1509
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1512
    :cond_24
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1513
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1362
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateMontblancWallpapersView()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2870
    const/4 v5, -0x1

    .line 2872
    .local v5, "lastIndex":I
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

    .line 2874
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 2875
    const v0, 0x7f100014

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    .line 2887
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

    const/4 v6, 0x1

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 2888
    return-void

    .line 2877
    :cond_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 2878
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2879
    move v5, v7

    .line 2884
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0

    .line 2877
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method private updateTileIndices()V
    .locals 18

    .prologue
    .line 1808
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v14}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 1809
    .local v2, "childCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 1812
    .local v9, "res":Landroid/content/res/Resources;
    const/4 v7, 0x0

    .line 1813
    .local v7, "numTiles":I
    const/4 v8, 0x0

    .local v8, "passNum":I
    :goto_0
    const/4 v14, 0x2

    if-ge v8, v14, :cond_5

    .line 1814
    const/4 v13, 0x0

    .line 1815
    .local v13, "tileIndex":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_4

    .line 1816
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1821
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    instance-of v14, v14, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    if-eqz v14, :cond_1

    .line 1822
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    .line 1823
    .local v10, "subList":Landroid/widget/LinearLayout;
    move v12, v3

    .line 1824
    .local v12, "subListStart":I
    add-int/lit8 v11, v3, 0x1

    .line 1831
    .local v11, "subListEnd":I
    :goto_2
    move v5, v12

    .local v5, "j":I
    :goto_3
    if-ge v5, v11, :cond_3

    .line 1832
    invoke-virtual {v10, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1833
    .local v4, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isNamelessWallpaper()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1834
    if-nez v8, :cond_2

    .line 1835
    add-int/lit8 v7, v7, 0x1

    .line 1831
    :cond_0
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .end local v4    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .end local v5    # "j":I
    .end local v10    # "subList":Landroid/widget/LinearLayout;
    .end local v11    # "subListEnd":I
    .end local v12    # "subListStart":I
    :cond_1
    move-object v10, v1

    .line 1826
    check-cast v10, Landroid/widget/LinearLayout;

    .line 1827
    .restart local v10    # "subList":Landroid/widget/LinearLayout;
    const/4 v12, 0x0

    .line 1828
    .restart local v12    # "subListStart":I
    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v11

    .restart local v11    # "subListEnd":I
    goto :goto_2

    .line 1837
    .restart local v4    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .restart local v5    # "j":I
    :cond_2
    const v14, 0x7f0c0004

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v9, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1838
    .local v6, "label":Ljava/lang/CharSequence;
    invoke-virtual {v4, v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onIndexUpdated(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 1815
    .end local v4    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .end local v6    # "label":Ljava/lang/CharSequence;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1813
    .end local v1    # "child":Landroid/view/View;
    .end local v5    # "j":I
    .end local v10    # "subList":Landroid/widget/LinearLayout;
    .end local v11    # "subListEnd":I
    .end local v12    # "subListStart":I
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1844
    .end local v3    # "i":I
    .end local v13    # "tileIndex":I
    :cond_5
    return-void
.end method

.method private visibleMenuList()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1242
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1244
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mShowDropdownImage:Z

    if-nez v0, :cond_2

    .line 1245
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1249
    :goto_0
    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    if-eqz v0, :cond_1

    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->cancel()V

    .line 1253
    :cond_1
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    .line 1254
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->alpha(F)Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1257
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLVPAnim:Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/LauncherViewPropertyAnimator;->start()V

    .line 1258
    return-void

    .line 1247
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;
    .locals 1

    .prologue
    .line 2451
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    return-object v0
.end method

.method public getSavedImages()Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;
    .locals 1

    .prologue
    .line 2455
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    return-object v0
.end method

.method protected getThumbnailOfLastPhoto()Landroid/graphics/Bitmap;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x1

    .line 1742
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v11

    const-string v0, "_data"

    aput-object v0, v2, v10

    const-string v0, "orientation"

    aput-object v0, v2, v12

    const/4 v0, 0x3

    const-string v1, "datetaken"

    aput-object v1, v2, v0

    .line 1744
    .local v2, "columns":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "datetaken DESC LIMIT 1"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Landroid/provider/MediaStore$Images$Media;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1748
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 1749
    .local v9, "thumb":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1

    .line 1750
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1751
    aget-object v0, v2, v11

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1752
    .local v7, "id":I
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    int-to-long v4, v7

    invoke-static {v0, v4, v5, v10, v3}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1754
    aget-object v0, v2, v12

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1756
    .local v8, "orientation":I
    if-eqz v8, :cond_0

    if-eqz v9, :cond_0

    .line 1757
    invoke-static {v9, v8, v10}, Lcom/sec/android/app/wallpaperchooser/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1760
    .end local v7    # "id":I
    .end local v8    # "orientation":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1763
    :cond_1
    return-object v9
.end method

.method public getWallpaperArrayResourceId()Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2330
    sget-boolean v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-eqz v4, :cond_0

    .line 2331
    const v3, 0x7f070001

    .line 2335
    .local v3, "resId":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v2

    .line 2337
    .local v2, "packageName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 2338
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    new-instance v4, Landroid/util/Pair;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2340
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    :goto_1
    return-object v4

    .line 2333
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "resId":I
    :cond_0
    const v3, 0x7f070002

    .restart local v3    # "resId":I
    goto :goto_0

    .line 2339
    .restart local v2    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2340
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 32
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 574
    const/4 v7, -0x1

    .line 575
    .local v7, "lastIndex":I
    if-eqz p1, :cond_0

    .line 576
    const-string v2, "extra_current_selected_thumbnail"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 578
    :cond_0
    sput-object p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->gInstance:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .line 579
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findWallpapers()V

    .line 581
    const v2, 0x7f040003

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setContentView(I)V

    .line 583
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_switch"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_8

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_EASY_MODE_30"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    .line 584
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "isKnoxMode"

    invoke-static {v2, v3}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v17

    .line 585
    .local v17, "bundle":Landroid/os/Bundle;
    const-string v2, "true"

    const-string v3, "isKnoxMode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 586
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    .line 590
    :goto_1
    const-string v2, "com.samsung.android.keyguardwallpaperupdator"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledPhotoFrame:Z

    .line 591
    const-string v2, "com.sec.android.app.montblanc"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledMontblanc:Z

    .line 593
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->checkSviewCover()Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    .line 594
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->checkDownloadWallpapers()Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    .line 595
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    .line 596
    sget-object v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->gInstance:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTTSWallpaper:Z

    .line 598
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 600
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 601
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mFirstSelect:Z

    .line 602
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-eqz v2, :cond_1

    .line 603
    const/4 v2, 0x1

    sput v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 605
    :cond_1
    const v2, 0x7f100015

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveWallpapersView:Landroid/widget/LinearLayout;

    .line 606
    const v2, 0x7f100016

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    .line 608
    const v2, 0x7f100008

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 611
    const v2, 0x7f100017

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    .line 613
    const v2, 0x7f100007

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    .line 614
    const v2, 0x7f10000a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    .line 615
    const v2, 0x7f10000b

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    .line 616
    const v2, 0x7f10000c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    .line 617
    const v2, 0x7f10000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    .line 618
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-nez v2, :cond_a

    .line 620
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    const v3, 0x7f0c0008

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 621
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    const v3, 0x7f0c0009

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 622
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const v3, 0x7f0c000a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 632
    :cond_2
    :goto_2
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v2, :cond_b

    .line 633
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const v3, 0x7f020011

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v3, 0x7f0c000b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 635
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v2, :cond_3

    .line 636
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v3, 0x7f020011

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    const v3, 0x7f0c000c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 645
    :cond_3
    :goto_3
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-eqz v2, :cond_c

    .line 646
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 647
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 649
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 661
    :cond_4
    :goto_4
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-eqz v2, :cond_e

    .line 662
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 666
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-eqz v2, :cond_5

    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-nez v2, :cond_5

    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-nez v2, :cond_5

    .line 668
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 670
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    .line 671
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mShowDropdownImage:Z

    .line 674
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$1;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 682
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$2;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$3;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 698
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$4;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 706
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$5;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$5;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 714
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$6;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    sput-object v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    .line 722
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 723
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x258

    if-ge v2, v3, :cond_6

    .line 724
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setRequestedOrientation(I)V

    .line 726
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->showHelpDialog_start()V

    .line 729
    :cond_7
    const v2, 0x7f100005

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/wallpaperchooser/CropView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    .line 730
    const v2, 0x7f10000e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    .line 731
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchCallback(Lcom/sec/android/app/wallpaperchooser/CropView$TouchCallback;)V

    .line 772
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    .line 836
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$9;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$9;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 851
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 880
    const v2, 0x7f100013

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    .line 883
    const/16 v18, 0x0

    .line 884
    .local v18, "customerImageNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    if-nez v2, :cond_10

    .line 886
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the built-in customer wallpapers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInCustomerWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

    .line 888
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInCustomerWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->loadThumbnailsAndImageIdList()V

    .line 889
    new-instance v18, Ljava/util/ArrayList;

    .end local v18    # "customerImageNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 890
    .restart local v18    # "customerImageNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInCustomerWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->getCount()I

    move-result v2

    move/from16 v0, v22

    if-ge v0, v2, :cond_f

    .line 891
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInCustomerWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;->getImageName(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 890
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 583
    .end local v17    # "bundle":Landroid/os/Bundle;
    .end local v18    # "customerImageNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v22    # "i":I
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 588
    .restart local v17    # "bundle":Landroid/os/Bundle;
    :cond_9
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z

    goto/16 :goto_1

    .line 624
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-eqz v2, :cond_2

    .line 626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    const v3, 0x7f0c0009

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    const/4 v2, 0x1

    sput v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    goto/16 :goto_2

    .line 639
    :cond_b
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v2, :cond_3

    .line 640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;

    const v3, 0x7f020011

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 641
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const v3, 0x7f0c000c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    .line 651
    :cond_c
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z

    if-eqz v2, :cond_d

    .line 652
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 653
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v2, :cond_4

    .line 654
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 656
    :cond_d
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z

    if-eqz v2, :cond_4

    .line 657
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 664
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 893
    .restart local v18    # "customerImageNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v22    # "i":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInCustomerWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/BuiltInCustomerWallpapersAdapter;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 896
    .end local v22    # "i":I
    :cond_10
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the built-in wallpapers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    sget-object v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCustomer:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getImagesFromCsc()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findBundledWallpapers(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v31

    .line 898
    .local v31, "wallpapers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;>;"
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;

    .line 899
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBuiltInWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 902
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the saved wallpapers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    .line 904
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->loadThumbnailsAndImageIdList()V

    .line 905
    const v2, 0x7f100012

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    .line 906
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImages:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 909
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledMontblanc:Z

    if-eqz v2, :cond_11

    .line 910
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the montblanc wallpaper"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

    .line 912
    const v2, 0x7f100014

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    .line 913
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 914
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersAdapter:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 918
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-nez v2, :cond_12

    .line 919
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the live wallpapers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    new-instance v15, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;-><init>(Landroid/content/Context;)V

    .line 921
    .local v15, "a":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v15}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$11;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;)V

    invoke-virtual {v15, v2}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 932
    .end local v15    # "a":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-nez v2, :cond_13

    .line 933
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the third-party wallpaper pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyListAdapter:Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;

    .line 935
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyWallpapersView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThirdPartyListAdapter:Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->populateWallpapersFromAdapter(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;ZZIZ)V

    .line 939
    :cond_13
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the from gallery wallpapers pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 940
    const v2, 0x7f100011

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    .line 941
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040005

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/FrameLayout;

    .line 942
    .local v27, "pickImageTile":Landroid/widget/FrameLayout;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setWallpaperItemPaddingToZero(Landroid/widget/FrameLayout;)V

    .line 943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v2, v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 944
    new-instance v24, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$PickImageInfo;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$PickImageInfo;-><init>()V

    .line 945
    .local v24, "pickImageInfo":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$PickImageInfo;
    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$PickImageInfo;->setView(Landroid/view/View;)V

    .line 946
    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 947
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 950
    const/16 v16, 0x0

    .line 951
    .local v16, "appName":Ljava/lang/String;
    const-string v2, "com.yahoo.mobile.client.android.liveweather"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 952
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the live view wallpapers pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040007

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    .line 954
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    const v3, 0x7f10002a

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveViewTv:Landroid/widget/TextView;

    .line 955
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveViewTv:Landroid/widget/TextView;

    if-eqz v2, :cond_14

    .line 956
    const-string v2, "com.yahoo.mobile.client.android.liveweather"

    const-string v3, "com.yahoo.mobile.client.android.liveweather.MainActivity"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->loadActivityLabel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 957
    if-eqz v16, :cond_1c

    .line 958
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveViewTv:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 962
    :cond_14
    :goto_7
    new-instance v26, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;

    invoke-direct/range {v26 .. v26}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;-><init>()V

    .line 963
    .local v26, "pickImageInfoLiveView":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;->setView(Landroid/view/View;)V

    .line 964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 965
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileLiveView:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 969
    .end local v26    # "pickImageInfoLiveView":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$LiveViewWallpaperInfo;
    :cond_15
    const-string v2, "com.samsung.android.keyguardwallpaperupdator"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 970
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the my interests wallpapers pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    const/16 v16, 0x0

    .line 972
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040009

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    .line 973
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    const v3, 0x7f10002d

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMyInterestTv:Landroid/widget/TextView;

    .line 974
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMyInterestTv:Landroid/widget/TextView;

    if-eqz v2, :cond_16

    .line 975
    const-string v2, "com.samsung.android.keyguardwallpaperupdator"

    const-string v3, "com.samsung.android.keyguardwallpaperupdator.ui.KeyguardCategoriesEntryActivity"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->loadActivityLabel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 976
    if-eqz v16, :cond_1d

    .line 977
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMyInterestTv:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 981
    :cond_16
    :goto_8
    new-instance v25, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MyInterestsWallpaperInfo;

    invoke-direct/range {v25 .. v25}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MyInterestsWallpaperInfo;-><init>()V

    .line 982
    .local v25, "pickImageInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MyInterestsWallpaperInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MyInterestsWallpaperInfo;->setView(Landroid/view/View;)V

    .line 983
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 984
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileMyInterests:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 988
    .end local v25    # "pickImageInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MyInterestsWallpaperInfo;
    :cond_17
    const-string v2, "com.samsung.festivalwallpaper"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 989
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the Festival wallpaper wallpapers pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040004

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    .line 992
    new-instance v20, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FestivalWallpaperInfo;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FestivalWallpaperInfo;-><init>()V

    .line 993
    .local v20, "festivalWallpaperInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FestivalWallpaperInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FestivalWallpaperInfo;->setView(Landroid/view/View;)V

    .line 994
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 995
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileFestivalWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 999
    .end local v20    # "festivalWallpaperInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FestivalWallpaperInfo;
    :cond_18
    const-string v2, "com.samsung.slidinggallery"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1000
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the Sliding wallpaper wallpapers pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04000a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    .line 1003
    new-instance v28, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$SlidingWallpaperInfo;

    invoke-direct/range {v28 .. v28}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$SlidingWallpaperInfo;-><init>()V

    .line 1004
    .local v28, "slidingWallpaperInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$SlidingWallpaperInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$SlidingWallpaperInfo;->setView(Landroid/view/View;)V

    .line 1005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 1006
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileSlidingWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1010
    .end local v28    # "slidingWallpaperInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$SlidingWallpaperInfo;
    :cond_19
    const-string v2, "com.samsung.android.service.travel"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1011
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Populate the Travel wallpaper wallpapers pickers"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04000c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMasterWallpaperList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    .line 1014
    new-instance v30, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$TravelImageInfo;

    invoke-direct/range {v30 .. v30}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$TravelImageInfo;-><init>()V

    .line 1015
    .local v30, "travelWallpaperInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$TravelImageInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$TravelImageInfo;->setView(Landroid/view/View;)V

    .line 1016
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 1017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mPickImageTileTravelWallpaper:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1021
    .end local v30    # "travelWallpaperInfoCategories":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$TravelImageInfo;
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getThumbnailOfLastPhoto()Landroid/graphics/Bitmap;

    move-result-object v23

    .line 1022
    .local v23, "lastPhoto":Landroid/graphics/Bitmap;
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "Make its background the last photo taken on external storage"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    if-eqz v23, :cond_1b

    .line 1024
    const v2, 0x7f100024

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageView;

    .line 1025
    .local v19, "emptyBg":Landroid/widget/ImageView;
    const/16 v2, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1026
    const v2, 0x7f100023

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 1027
    .local v21, "galleryThumbnailBg":Landroid/widget/ImageView;
    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1030
    .end local v19    # "emptyBg":Landroid/widget/ImageView;
    .end local v21    # "galleryThumbnailBg":Landroid/widget/ImageView;
    :cond_1b
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateTileIndices()V

    .line 1033
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->initializeScrollForRtl()V

    .line 1036
    new-instance v29, Landroid/animation/LayoutTransition;

    invoke-direct/range {v29 .. v29}, Landroid/animation/LayoutTransition;-><init>()V

    .line 1037
    .local v29, "transitioner":Landroid/animation/LayoutTransition;
    const-wide/16 v2, 0xc8

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 1038
    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 1039
    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 1040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1042
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1092
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 1232
    return-void

    .line 960
    .end local v23    # "lastPhoto":Landroid/graphics/Bitmap;
    .end local v29    # "transitioner":Landroid/animation/LayoutTransition;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLiveViewTv:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 979
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMyInterestTv:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8
.end method

.method isApplicationInstalled(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2365
    const/4 v1, 0x0

    .line 2368
    .local v1, "installed":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2369
    .local v2, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_0

    .line 2370
    const/4 v1, 0x1

    .line 2377
    .end local v2    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v1

    .line 2372
    :catch_0
    move-exception v0

    .line 2373
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "Wallpapers.WallpaperPickerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not installed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2374
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 21
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 2059
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z

    if-eqz v2, :cond_0

    .line 2060
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V

    .line 2062
    :cond_0
    const/4 v2, 0x5

    move/from16 v0, p1

    if-ne v0, v2, :cond_8

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_8

    .line 2063
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mInstalledPhotoFrame:Z

    if-eqz v2, :cond_6

    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 2064
    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2065
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "selectedCount"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_4

    .line 2066
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "selectedItems"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 2067
    .local v17, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v17, :cond_2

    const/4 v11, 0x0

    .line 2068
    .local v11, "imagePath":Ljava/lang/String;
    :goto_0
    if-eqz v11, :cond_3

    .line 2069
    const/4 v2, 0x1

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 2070
    .local v19, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->makeIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v16

    .line 2071
    .local v16, "request":Landroid/content/Intent;
    if-eqz v16, :cond_1

    .line 2072
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    const/16 v3, 0x403

    move-object/from16 v0, v16

    invoke-static {v2, v0, v3}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 2213
    .end local v11    # "imagePath":Ljava/lang/String;
    .end local v16    # "request":Landroid/content/Intent;
    .end local v17    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_1
    return-void

    .line 2067
    .restart local v17    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 2074
    .restart local v11    # "imagePath":Ljava/lang/String;
    :cond_3
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2075
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto :goto_1

    .line 2078
    .end local v11    # "imagePath":Ljava/lang/String;
    .end local v17    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    .line 2079
    .local v13, "intentMultiplelWallpaper":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.samsung.android.keyguardwallpaperupdator"

    const-string v5, "com.samsung.android.keyguardwallpaperupdator.ui.KeyguardPhotoSlideMainActivity"

    invoke-direct {v2, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2082
    const-string v2, "imageUriList"

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "selectedItems"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2083
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v13, v3}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V

    goto :goto_1

    .line 2086
    .end local v13    # "intentMultiplelWallpaper":Landroid/content/Intent;
    :cond_5
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2087
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto :goto_1

    .line 2090
    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 2091
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v19

    .line 2092
    .restart local v19    # "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->makeIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v16

    .line 2093
    .restart local v16    # "request":Landroid/content/Intent;
    if-eqz v16, :cond_1

    .line 2094
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    const/16 v3, 0x403

    move-object/from16 v0, v16

    invoke-static {v2, v0, v3}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V

    goto :goto_1

    .line 2096
    .end local v16    # "request":Landroid/content/Intent;
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_7
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2097
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto :goto_1

    .line 2100
    :cond_8
    const/4 v2, 0x6

    move/from16 v0, p1

    if-ne v0, v2, :cond_9

    .line 2101
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2102
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto :goto_1

    .line 2103
    :cond_9
    const/4 v2, 0x7

    move/from16 v0, p1

    if-ne v0, v2, :cond_a

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_a

    .line 2104
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2105
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2106
    :cond_a
    const/16 v2, 0x403

    move/from16 v0, p1

    if-ne v0, v2, :cond_14

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_14

    .line 2107
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v8

    .line 2108
    .local v8, "currentUserId":I
    if-nez v8, :cond_f

    .line 2109
    const/4 v15, 0x0

    .line 2110
    .local v15, "path":Ljava/lang/String;
    if-eqz p3, :cond_b

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 2111
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "sim_selection"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->selectedSim:I

    .line 2112
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult 4 sim_selection / selectedSim : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v5, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->selectedSim:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2115
    :cond_b
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z

    if-eqz v2, :cond_12

    .line 2116
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "multisim"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2117
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSetAsSIM2()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->isSetAsSIM1()Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_c
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->selectedSim:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_11

    .line 2118
    :cond_d
    const-string v15, "data/system/users/0/wallpaper2"

    .line 2126
    :goto_2
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_e

    .line 2127
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z

    if-eqz v2, :cond_13

    .line 2128
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, " LOCKSCREEN_POSITION multisim"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2130
    const-string v15, "data/data/com.sec.android.gallery3d/files/lockscreen_wallpaper_ripple.jpg"

    .line 2135
    :cond_e
    :goto_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addTemporaryWallpaperTile(Ljava/lang/String;IZ)Z

    move-result v18

    .line 2136
    .local v18, "success":Z
    if-eqz v18, :cond_f

    .line 2137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 2138
    .local v12, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 2142
    .end local v12    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .end local v15    # "path":Ljava/lang/String;
    .end local v18    # "success":Z
    :cond_f
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    if-eqz v2, :cond_10

    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    .line 2143
    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V

    .line 2145
    :cond_10
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2146
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2120
    .restart local v15    # "path":Ljava/lang/String;
    :cond_11
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "selectedSim!=2"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2121
    const-string v15, "data/system/users/0/wallpaper"

    goto :goto_2

    .line 2124
    :cond_12
    const-string v15, "data/system/users/0/wallpaper"

    goto :goto_2

    .line 2132
    :cond_13
    const-string v15, "data/data/com.sec.android.gallery3d/files/lockscreen_wallpaper_ripple.jpg"

    goto :goto_3

    .line 2147
    .end local v8    # "currentUserId":I
    .end local v15    # "path":Ljava/lang/String;
    :cond_14
    const/16 v2, 0x8

    move/from16 v0, p1

    if-ne v0, v2, :cond_15

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_15

    .line 2148
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2149
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2150
    :cond_15
    const/16 v2, 0x9

    move/from16 v0, p1

    if-ne v0, v2, :cond_16

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_16

    .line 2151
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2152
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2153
    :cond_16
    const/16 v2, 0xa

    move/from16 v0, p1

    if-ne v0, v2, :cond_1c

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_1c

    .line 2154
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v8

    .line 2155
    .restart local v8    # "currentUserId":I
    if-nez v8, :cond_19

    .line 2156
    if-eqz p3, :cond_19

    .line 2157
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    .line 2158
    .local v10, "extras":Landroid/os/Bundle;
    const/4 v11, 0x0

    .line 2159
    .restart local v11    # "imagePath":Ljava/lang/String;
    const/4 v14, 0x0

    .line 2160
    .local v14, "orientation":I
    if-eqz v10, :cond_18

    .line 2161
    const-string v2, "wallpaper_file_uri"

    invoke-virtual {v10, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    .line 2162
    .local v20, "wallpaperUriObject":Ljava/lang/Object;
    if-eqz v20, :cond_18

    move-object/from16 v0, v20

    instance-of v2, v0, Landroid/net/Uri;

    if-eqz v2, :cond_18

    .line 2163
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_data"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "orientation"

    aput-object v3, v4, v2

    .line 2164
    .local v4, "columns":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v3, v20

    check-cast v3, Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2165
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_18

    .line 2166
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2167
    const/4 v2, 0x0

    aget-object v2, v4, v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2168
    const/4 v2, 0x1

    aget-object v2, v4, v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 2170
    :cond_17
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2175
    .end local v4    # "columns":[Ljava/lang/String;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v20    # "wallpaperUriObject":Ljava/lang/Object;
    :cond_18
    if-eqz v11, :cond_19

    .line 2176
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v14, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addTemporaryWallpaperTile(Ljava/lang/String;IZ)Z

    move-result v18

    .line 2177
    .restart local v18    # "success":Z
    if-eqz v18, :cond_1b

    .line 2178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 2179
    .restart local v12    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 2186
    .end local v10    # "extras":Landroid/os/Bundle;
    .end local v11    # "imagePath":Ljava/lang/String;
    .end local v12    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .end local v14    # "orientation":I
    .end local v18    # "success":Z
    :cond_19
    :goto_4
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    if-eqz v2, :cond_1a

    .line 2187
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V

    .line 2189
    :cond_1a
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2190
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2181
    .restart local v10    # "extras":Landroid/os/Bundle;
    .restart local v11    # "imagePath":Ljava/lang/String;
    .restart local v14    # "orientation":I
    .restart local v18    # "success":Z
    :cond_1b
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    const-string v3, "PICK_MULTIPLE_WALLPAPER - RESULT_OK : success == false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 2191
    .end local v8    # "currentUserId":I
    .end local v10    # "extras":Landroid/os/Bundle;
    .end local v11    # "imagePath":Ljava/lang/String;
    .end local v14    # "orientation":I
    .end local v18    # "success":Z
    :cond_1c
    const/16 v2, 0xb

    move/from16 v0, p1

    if-ne v0, v2, :cond_1e

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_1e

    .line 2192
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    if-eqz v2, :cond_1d

    .line 2193
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V

    .line 2195
    :cond_1d
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2196
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2197
    :cond_1e
    const/16 v2, 0xc

    move/from16 v0, p1

    if-ne v0, v2, :cond_20

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_20

    .line 2198
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z

    if-eqz v2, :cond_1f

    .line 2199
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V

    .line 2201
    :cond_1f
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2202
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2203
    :cond_20
    const/16 v2, 0xd

    move/from16 v0, p1

    if-ne v0, v2, :cond_21

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_21

    .line 2204
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2205
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2206
    :cond_21
    const/16 v2, 0xe

    move/from16 v0, p1

    if-ne v0, v2, :cond_22

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_22

    .line 2207
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2208
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1

    .line 2209
    :cond_22
    const/16 v2, 0xf

    move/from16 v0, p1

    if-ne v0, v2, :cond_1

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_1

    .line 2210
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 2211
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto/16 :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 561
    sput-boolean v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z

    .line 562
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 563
    invoke-super {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->onBackPressed()V

    .line 564
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 508
    invoke-super {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->onCreate(Landroid/os/Bundle;)V

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionBar:Landroid/app/ActionBar;

    .line 511
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->hide()V

    .line 513
    const-string v2, "enterprise_policy"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 514
    .local v1, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/enterprise/RestrictionPolicy;->isWallpaperChangeAllowed(Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 515
    invoke-virtual {p0, v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    .line 538
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x258

    if-ge v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 521
    :cond_2
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setRequestedOrientation(I)V

    .line 525
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.android.app.wallpaperchooser.GET_WALLPAPER_LIST"

    if-ne v0, v2, :cond_3

    .line 527
    iput-boolean v7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIsForTestApp:Z

    .line 529
    :cond_3
    sput v6, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 531
    sget-object v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 532
    sget-object v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 533
    iget-boolean v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isModeEasyLauncher1:Z

    if-eqz v2, :cond_0

    .line 534
    sput v7, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    .line 535
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCurrentMode:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateMenuList(I)V

    .line 536
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateHistoryWallpapersView()V

    goto :goto_0

    .line 523
    .end local v0    # "action":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setRequestedOrientation(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/CropView;->destroy()V

    .line 569
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 570
    invoke-super {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->onDestroy()V

    .line 571
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 542
    invoke-super {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->onResume()V

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 545
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->visibleMenuList()V

    .line 547
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIsForTestApp:Z

    if-eqz v0, :cond_1

    .line 548
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->returnForGetWallpaperListAction()V

    .line 551
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mUpdateWallpaperListOnDelete:Z

    if-eqz v0, :cond_2

    .line 552
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mUpdateWallpaperListOnDelete:Z

    .line 553
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateHistoryWallpapersView()V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 557
    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1771
    const/4 v2, -0x1

    .line 1772
    .local v2, "lastIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1773
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;

    .line 1774
    .local v0, "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1775
    move v2, v1

    .line 1779
    .end local v0    # "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    sget v4, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mpredefinedWallpaper:I

    add-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 1780
    const-string v3, "extra_current_selected_thumbnail"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1781
    invoke-super {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1782
    return-void

    .line 1772
    .restart local v0    # "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 1767
    invoke-super {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->onStop()V

    .line 1768
    return-void
.end method

.method protected setCheckBackgroundView(Z)V
    .locals 7
    .param p1, "checked"    # Z

    .prologue
    const v6, 0x7f100027

    const/4 v5, 0x4

    .line 1640
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 1641
    .local v0, "childCount":I
    if-eqz p1, :cond_1

    .line 1642
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 1643
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1644
    .local v2, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isCheckable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1645
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1642
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1649
    .end local v1    # "i":I
    .end local v2    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 1650
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1651
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f100028

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1649
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1654
    :cond_2
    return-void
.end method

.method public setWallpaperStripYOffset(F)V
    .locals 3
    .param p1, "offset"    # F

    .prologue
    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;

    float-to-int v1, p1

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 504
    return-void
.end method

.method public showHelpDialog()V
    .locals 26

    .prologue
    .line 1569
    const/16 v22, 0x1

    sput-boolean v22, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z

    .line 1575
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x2

    const/16 v23, -0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1577
    .local v10, "lp_text":Landroid/widget/LinearLayout$LayoutParams;
    const v22, 0x7f10001f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1578
    .local v8, "image_pointer":Landroid/widget/ImageView;
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1579
    const v22, 0x7f100020

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1580
    .local v7, "bubble_text":Landroid/widget/TextView;
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1582
    sget-boolean v22, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z

    if-eqz v22, :cond_0

    .line 1583
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v22

    move-object/from16 v0, v22

    iget v15, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1584
    .local v15, "rightMargin":I
    div-int/lit8 v15, v15, 0xc

    .line 1585
    const v22, 0x7f10001e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 1586
    .local v17, "tapImage":Landroid/widget/ImageView;
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1587
    const/16 v22, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1588
    const v22, 0x7f10001c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 1589
    .local v11, "mainLayout":Landroid/widget/LinearLayout;
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1590
    const v22, 0x7f100019

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    .end local v17    # "tapImage":Landroid/widget/ImageView;
    check-cast v17, Landroid/widget/ImageView;

    .line 1591
    .restart local v17    # "tapImage":Landroid/widget/ImageView;
    const v22, 0x7f100018

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 1592
    .local v12, "mainLayout_top":Landroid/widget/LinearLayout;
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1593
    .local v16, "tap":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b000a

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 1594
    .local v5, "bottomMargin":I
    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v15, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1595
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->requestLayout()V

    .line 1596
    const v22, 0x7f10001b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 1597
    .local v21, "upDownPointer":Landroid/widget/ImageView;
    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->bringToFront()V

    .line 1598
    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .end local v16    # "tap":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1599
    .restart local v16    # "tap":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->requestLayout()V

    .line 1600
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1601
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->bringToFront()V

    .line 1602
    invoke-virtual {v12}, Landroid/widget/LinearLayout;->bringToFront()V

    .line 1603
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1604
    const/16 v22, 0x0

    sput-boolean v22, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z

    .line 1605
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->animateHelpDialogs(Landroid/widget/ImageView;Landroid/view/View;)V

    .line 1631
    .end local v5    # "bottomMargin":I
    .end local v12    # "mainLayout_top":Landroid/widget/LinearLayout;
    .end local v15    # "rightMargin":I
    .end local v16    # "tap":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v21    # "upDownPointer":Landroid/widget/ImageView;
    :goto_0
    return-void

    .line 1607
    .end local v11    # "mainLayout":Landroid/widget/LinearLayout;
    .end local v17    # "tapImage":Landroid/widget/ImageView;
    :cond_0
    const v22, 0x7f10001e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 1608
    .restart local v17    # "tapImage":Landroid/widget/ImageView;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x2

    const/16 v23, -0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1609
    .local v9, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const/high16 v23, 0x7f0b0000

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    .line 1610
    .local v14, "pointerleftMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0001

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    .line 1611
    .local v13, "pointerBottomMargin":I
    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v14, v0, v1, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 1612
    const/16 v22, 0xc

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1613
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1614
    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0a0026

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPaddingRelative(IIII)V

    .line 1615
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1616
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->bringToFront()V

    .line 1618
    const v22, 0x7f10001c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 1619
    .restart local v11    # "mainLayout":Landroid/widget/LinearLayout;
    const v22, 0x7f10001d

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1620
    .local v6, "bubble_pointer":Landroid/widget/ImageView;
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1622
    const v22, 0x7f10001a

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 1623
    .local v20, "tw":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0c0015

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0008

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v19

    .line 1625
    .local v19, "textBubbleTopMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0009

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    .line 1626
    .local v18, "textBubbleRightMargin":I
    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v19

    move/from16 v2, v18

    move/from16 v3, v23

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 1627
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1628
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1629
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->animateHelpDialogs(Landroid/widget/ImageView;Landroid/view/View;)V

    goto/16 :goto_0
.end method
