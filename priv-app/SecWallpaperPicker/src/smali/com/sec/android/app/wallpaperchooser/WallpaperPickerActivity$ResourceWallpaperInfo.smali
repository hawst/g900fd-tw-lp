.class public Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResourceWallpaperInfo"
.end annotation


# instance fields
.field private mResId:I

.field private mResources:Landroid/content/res/Resources;

.field private mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

.field private mThumb:Landroid/graphics/drawable/Drawable;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;Landroid/net/Uri;)V
    .locals 0
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    .line 391
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResources:Landroid/content/res/Resources;

    .line 392
    iput p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResId:I

    .line 393
    iput-object p3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 394
    iput-object p4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mUri:Landroid/net/Uri;

    .line 395
    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mThumb:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public isNamelessWallpaper()Z
    .locals 1

    .prologue
    .line 441
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTTSWallpaper:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$400()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    const/4 v0, 0x0

    .line 444
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 11
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    const/4 v10, 0x0

    const/16 v4, 0x400

    const/4 v9, 0x0

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 400
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;

    move-result-object v7

    .line 401
    .local v7, "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mUri:Landroid/net/Uri;

    invoke-static {p1, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v5

    .line 402
    .local v5, "rotation":I
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    if-nez v0, :cond_0

    .line 403
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mUri:Landroid/net/Uri;

    invoke-direct {v0, p1, v1, v4, v5}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/Context;Landroid/net/Uri;II)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v7, v0, v10}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 405
    invoke-virtual {v7, v9}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    .line 421
    :goto_0
    return-void

    .line 407
    .end local v5    # "rotation":I
    .end local v7    # "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResId:I

    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Landroid/content/res/Resources;I)I

    move-result v5

    .line 408
    .restart local v5    # "rotation":I
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    if-nez v0, :cond_2

    .line 409
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResId:I

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/res/Resources;Landroid/content/Context;III)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    .line 411
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;

    move-result-object v7

    .line 412
    .restart local v7    # "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v7, v0, v10}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 413
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getDefaultWallpaperSize(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v8

    .line 414
    .local v8, "wallpaperSize":Landroid/graphics/Point;
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;->getImageWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;->getImageHeight()I

    move-result v1

    iget v2, v8, Landroid/graphics/Point;->x:I

    iget v3, v8, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1, v2, v3, v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getMaxCropRect(IIIIZ)Landroid/graphics/RectF;

    move-result-object v6

    .line 415
    .local v6, "crop":Landroid/graphics/RectF;
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-ge v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 416
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v7, v0}, Lcom/sec/android/app/wallpaperchooser/CropView;->setScale(F)V

    .line 419
    :goto_1
    invoke-virtual {v7, v9}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    goto :goto_0

    .line 418
    :cond_4
    iget v0, v8, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {v7, v0}, Lcom/sec/android/app/wallpaperchooser/CropView;->setScale(F)V

    goto :goto_1
.end method

.method public onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 4
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 425
    const/4 v0, 0x1

    .line 426
    .local v0, "finishActivityWhenDone":Z
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 427
    const/4 v1, 0x1

    .line 428
    .local v1, "isNoCrop":Z
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mUri:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;ZZ)V

    .line 432
    .end local v1    # "isNoCrop":Z
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mResId:I

    invoke-virtual {p1, v2, v3, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->cropImageAndSetWallpaper(Landroid/content/res/Resources;IZ)V

    goto :goto_0
.end method
