.class public Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;
.super Landroid/app/IntentService;
.source "BackupLockScreenImageChooser.java"


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field cr:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    .line 20
    const-string v0, "content://com.sec.android.sCloudWallpaperBackupProvider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "BackupLockScreenImageChooser"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method private getLockScreen(Ljava/lang/String;)V
    .locals 21
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 94
    const/4 v12, 0x0

    .line 95
    .local v12, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 97
    .local v5, "file":Ljava/io/File;
    const-string v18, "default"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 98
    new-instance v6, Ljava/io/File;

    const-string v18, "//system/wallpaper/lockscreen_default_wallpaper.jpg"

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .local v6, "fileJpg":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const-string v18, "//system/csc_contents/lockscreen_default_wallpaper.jpg"

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .local v7, "fileMultiCSCJpg":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    const-string v18, "//system/wallpaper/lockscreen_default_wallpaper.png"

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 102
    .local v10, "filePng":Ljava/io/File;
    new-instance v8, Ljava/io/File;

    const-string v18, "//system/csc_contents/lockscreen_default_wallpaper.png"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v8, "fileMultiCSCPng":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 106
    move-object v5, v6

    .line 116
    .end local v6    # "fileJpg":Ljava/io/File;
    .end local v7    # "fileMultiCSCJpg":Ljava/io/File;
    .end local v8    # "fileMultiCSCPng":Ljava/io/File;
    .end local v10    # "filePng":Ljava/io/File;
    :cond_0
    :goto_0
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 117
    const/4 v14, 0x0

    .line 119
    .local v14, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    sget-object v18, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v18

    const-string v19, "filename"

    const-string v20, "lockscreen_wallpaper"

    invoke-virtual/range {v18 .. v20}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v11

    .line 120
    .local v11, "fileuri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->cr:Landroid/content/ContentResolver;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v11, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 122
    .local v9, "fileOp":Landroid/os/ParcelFileDescriptor;
    if-nez v9, :cond_7

    .line 143
    if-eqz v12, :cond_1

    .line 144
    :try_start_1
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_1
    if-eqz v14, :cond_2

    .line 146
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 152
    .end local v9    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v11    # "fileuri":Landroid/net/Uri;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    :goto_1
    return-void

    .line 107
    .restart local v6    # "fileJpg":Ljava/io/File;
    .restart local v7    # "fileMultiCSCJpg":Ljava/io/File;
    .restart local v8    # "fileMultiCSCPng":Ljava/io/File;
    .restart local v10    # "filePng":Ljava/io/File;
    :cond_3
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 108
    move-object v5, v7

    goto :goto_0

    .line 109
    :cond_4
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 110
    move-object v5, v10

    goto :goto_0

    .line 111
    :cond_5
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 112
    move-object v5, v8

    goto :goto_0

    .line 114
    .end local v6    # "fileJpg":Ljava/io/File;
    .end local v7    # "fileMultiCSCJpg":Ljava/io/File;
    .end local v8    # "fileMultiCSCPng":Ljava/io/File;
    .end local v10    # "filePng":Ljava/io/File;
    :cond_6
    new-instance v5, Ljava/io/File;

    .end local v5    # "file":Ljava/io/File;
    const-string v18, "/data/data/com.sec.android.app.wallpaperchooser/files/lockscreen_wallpaper.jpg"

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v5    # "file":Ljava/io/File;
    goto :goto_0

    .line 147
    .restart local v9    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v11    # "fileuri":Landroid/net/Uri;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 148
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 125
    .end local v3    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_2
    new-instance v15, Ljava/io/FileOutputStream;

    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .local v15, "fos":Ljava/io/FileOutputStream;
    :try_start_3
    new-instance v13, Ljava/io/FileInputStream;

    invoke-direct {v13, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 128
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .local v13, "fis":Ljava/io/FileInputStream;
    if-eqz v13, :cond_8

    .line 130
    :try_start_4
    invoke-virtual {v13}, Ljava/io/FileInputStream;->available()I

    move-result v17

    .line 131
    .local v17, "size":I
    move/from16 v0, v17

    new-array v2, v0, [B

    .line 133
    .local v2, "buffer":[B
    invoke-virtual {v13, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v16

    .line 134
    .local v16, "len":I
    const/16 v18, 0x0

    move/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v15, v2, v0, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 143
    .end local v2    # "buffer":[B
    .end local v16    # "len":I
    .end local v17    # "size":I
    :cond_8
    if-eqz v13, :cond_9

    .line 144
    :try_start_5
    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_9
    if-eqz v15, :cond_a

    .line 146
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_a
    move-object v12, v13

    .line 149
    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 147
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 148
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v12, v13

    .line 150
    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 136
    .end local v3    # "e":Ljava/io/IOException;
    .end local v9    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v11    # "fileuri":Landroid/net/Uri;
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v4

    .line 137
    .local v4, "e1":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 143
    if-eqz v12, :cond_b

    .line 144
    :try_start_7
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_b
    if-eqz v14, :cond_2

    .line 146
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 147
    :catch_3
    move-exception v3

    .line 148
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 138
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "e1":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v3

    .line 139
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_8
    sget-object v18, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v19, "IOException occurred "

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 143
    if-eqz v12, :cond_c

    .line 144
    :try_start_9
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_c
    if-eqz v14, :cond_2

    .line 146
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_1

    .line 147
    :catch_5
    move-exception v3

    .line 148
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 142
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    .line 143
    :goto_4
    if-eqz v12, :cond_d

    .line 144
    :try_start_a
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_d
    if-eqz v14, :cond_e

    .line 146
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 149
    :cond_e
    :goto_5
    throw v18

    .line 147
    :catch_6
    move-exception v3

    .line 148
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 142
    .end local v3    # "e":Ljava/io/IOException;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v11    # "fileuri":Landroid/net/Uri;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v18

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v18

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 138
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v3

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v3

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 136
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_9
    move-exception v4

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v4

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private setLockScreen(Ljava/lang/String;I)V
    .locals 17
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "rippleValue"    # I

    .prologue
    .line 29
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "trying to copy the file here"

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    const/4 v6, 0x0

    .line 31
    .local v6, "fis":Ljava/io/FileInputStream;
    const/4 v8, 0x0

    .line 32
    .local v8, "fos":Ljava/io/FileOutputStream;
    const/4 v12, 0x0

    .line 34
    .local v12, "mLockscreenPath":Ljava/lang/String;
    :try_start_0
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v13}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v13

    const-string v14, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v13, v14, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 35
    .local v5, "fileuri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->cr:Landroid/content/ContentResolver;

    const/4 v14, 0x0

    invoke-virtual {v13, v5, v14}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 37
    .local v4, "fileOp":Landroid/os/ParcelFileDescriptor;
    if-eqz v4, :cond_0

    .line 38
    new-instance v7, Ljava/io/FileInputStream;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v13

    invoke-direct {v7, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    move-object v6, v7

    .line 39
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :cond_0
    if-eqz v6, :cond_6

    .line 40
    invoke-virtual {v6}, Ljava/io/FileInputStream;->available()I

    move-result v11

    .line 41
    .local v11, "length":I
    if-gtz v11, :cond_3

    .line 42
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "Corrupt file.. Need to check !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    if-eqz v6, :cond_1

    .line 79
    :try_start_1
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 83
    :goto_0
    if-eqz v8, :cond_2

    .line 84
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 91
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    .end local v11    # "length":I
    :goto_1
    return-void

    .line 81
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    .restart local v11    # "length":I
    :cond_1
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fis is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 86
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fos is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 46
    :cond_3
    :try_start_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".jpg"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 47
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_4

    .line 49
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 50
    :cond_4
    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Ljava/io/File;->setReadable(ZZ)Z

    .line 51
    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Ljava/io/File;->setWritable(ZZ)Z

    .line 52
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 53
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .local v9, "fos":Ljava/io/FileOutputStream;
    :try_start_4
    new-array v1, v11, [B

    .line 54
    .local v1, "buffer":[B
    :goto_2
    invoke-virtual {v6, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v11

    if-lez v11, :cond_5

    .line 55
    const/4 v13, 0x0

    invoke-virtual {v9, v1, v13, v11}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 72
    .end local v1    # "buffer":[B
    :catch_1
    move-exception v2

    move-object v8, v9

    .line 73
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "length":I
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 78
    if-eqz v6, :cond_9

    .line 79
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 83
    :goto_4
    if-eqz v8, :cond_a

    .line 84
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    .line 87
    :catch_2
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .end local v2    # "e":Ljava/io/IOException;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "length":I
    :cond_5
    move-object v8, v9

    .line 59
    .end local v1    # "buffer":[B
    .end local v3    # "file":Ljava/io/File;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "length":I
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :cond_6
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "lockscreen_wallpaper_path"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/data/data/com.sec.android.app.wallpaperchooser/files/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ".jpg"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "lockscreen_wallpaper"

    const/4 v15, 0x1

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 62
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "lockscreen_wallpaper_path_ripple"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/data/data/com.sec.android.app.wallpaperchooser/files/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ".jpg"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "lockscreen_ripple_effect"

    move/from16 v0, p2

    invoke-static {v13, v14, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "lockscreen_wallpaper_transparent"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 70
    new-instance v10, Landroid/content/Intent;

    const-string v13, "com.sec.android.gallery3d.LOCKSCREEN_IMAGE_CHANGED"

    invoke-direct {v10, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    .local v10, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 78
    if-eqz v6, :cond_7

    .line 79
    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 83
    :goto_5
    if-eqz v8, :cond_8

    .line 84
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_1

    .line 87
    :catch_3
    move-exception v2

    .line 88
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 81
    .end local v2    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_9
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fis is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 86
    :cond_8
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fos is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_1

    .line 81
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    .end local v10    # "intent":Landroid/content/Intent;
    .local v2, "e":Ljava/io/FileNotFoundException;
    :cond_9
    :try_start_a
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fis is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 86
    :cond_a
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fos is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_1

    .line 74
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v2

    .line 75
    .local v2, "e":Ljava/lang/Exception;
    :goto_6
    :try_start_b
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 78
    if-eqz v6, :cond_b

    .line 79
    :try_start_c
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 83
    :goto_7
    if-eqz v8, :cond_c

    .line 84
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_1

    .line 87
    :catch_5
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 81
    .local v2, "e":Ljava/lang/Exception;
    :cond_b
    :try_start_d
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fis is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 86
    :cond_c
    sget-object v13, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v14, "fos is NULL !!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5

    goto/16 :goto_1

    .line 77
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v13

    .line 78
    :goto_8
    if-eqz v6, :cond_d

    .line 79
    :try_start_e
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 83
    :goto_9
    if-eqz v8, :cond_e

    .line 84
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6

    .line 89
    :goto_a
    throw v13

    .line 81
    :cond_d
    :try_start_f
    sget-object v14, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v15, "fis is NULL !!"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    goto :goto_9

    .line 87
    :catch_6
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 86
    .end local v2    # "e":Ljava/io/IOException;
    :cond_e
    :try_start_10
    sget-object v14, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v15, "fos is NULL !!"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6

    goto :goto_a

    .line 77
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "length":I
    :catchall_1
    move-exception v13

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 74
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v2

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 72
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    .end local v11    # "length":I
    :catch_8
    move-exception v2

    goto/16 :goto_3
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 156
    if-eqz p1, :cond_0

    .line 157
    const-string v3, "default"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 158
    .local v1, "defaultValue":I
    const-string v3, "rippleValue"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 160
    .local v2, "rippleValue":I
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->cr:Landroid/content/ContentResolver;

    .line 162
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 164
    const-string v3, "SET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 165
    sget-object v3, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v4, "SET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER received"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v3, "lockscreen_wallpaper"

    invoke-direct {p0, v3, v2}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->setLockScreen(Ljava/lang/String;I)V

    .line 176
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "defaultValue":I
    .end local v2    # "rippleValue":I
    :cond_0
    :goto_0
    return-void

    .line 167
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "defaultValue":I
    .restart local v2    # "rippleValue":I
    :cond_1
    const-string v3, "GET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 168
    sget-object v3, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->TAG:Ljava/lang/String;

    const-string v4, "GET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER received"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    if-ne v1, v5, :cond_2

    .line 170
    const-string v3, "default"

    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getLockScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_2
    const-string v3, "lockscreen_wallpaper"

    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/util/BackupLockScreenImageChooser;->getLockScreen(Ljava/lang/String;)V

    goto :goto_0
.end method
