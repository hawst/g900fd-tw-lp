.class public abstract Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "WallpaperTileInfo"
.end annotation


# instance fields
.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isCheckable()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public isMultipleImage()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public isNamelessWallpaper()Z
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 201
    return-void
.end method

.method public onDelete(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 203
    return-void
.end method

.method public onIndexUpdated(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "label"    # Ljava/lang/CharSequence;

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isNamelessWallpaper()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 212
    :cond_0
    return-void
.end method

.method public onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 202
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->mView:Landroid/view/View;

    .line 200
    return-void
.end method
