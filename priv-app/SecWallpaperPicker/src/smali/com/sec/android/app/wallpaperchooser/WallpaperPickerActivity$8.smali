.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v8, 0x7f0c0005

    const/4 v7, 0x4

    const/4 v4, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 774
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-eqz v2, :cond_1

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2100()Z

    move-result v2

    if-eqz v2, :cond_1

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2200()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 778
    invoke-virtual {p1}, Landroid/view/View;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 779
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mLongClickListener:Landroid/view/View$OnLongClickListener;
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View$OnLongClickListener;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    goto :goto_0

    .line 783
    :cond_2
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mFirstSelect:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2400()Z

    move-result v2

    if-nez v2, :cond_5

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1700()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-eqz v2, :cond_3

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2100()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-nez v2, :cond_5

    .line 784
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 788
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 790
    .local v0, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 791
    instance-of v2, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$PickImageInfo;

    if-eqz v2, :cond_6

    .line 792
    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z
    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2202(Z)Z

    goto :goto_0

    .line 786
    .end local v0    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    :cond_5
    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mFirstSelect:Z
    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2402(Z)Z

    goto :goto_1

    .line 796
    .restart local v0    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    :cond_6
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2100()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 797
    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperSelected:Z
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2202(Z)Z

    .line 798
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->showHelpDialog()V

    .line 802
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "pickImageInfoLiveView"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "pickImageInfoCategories"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "LiveWallpaperTile"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PickImageInfo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 804
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-eq v2, v4, :cond_8

    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-eq v2, v7, :cond_8

    .line 805
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 811
    :cond_8
    const-string v2, "Wallpapers.WallpaperPickerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mThumbnailOnClickListener : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isSelectable()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 813
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 814
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setSelected(Z)V

    .line 815
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/View;)Landroid/view/View;

    .line 817
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v2, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/View;)Landroid/view/View;

    .line 818
    invoke-virtual {p1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 819
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 820
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 825
    :cond_a
    :goto_2
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isMultipleImage()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 826
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 827
    .local v1, "intentMultiplelWallpaper":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.samsung.android.keyguardwallpaperupdator"

    const-string v4, "com.samsung.android.keyguardwallpaperupdator.ui.KeyguardPhotoSlideMainActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 830
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/wallpaperchooser/Utilities;->startActivityForResultSafely(Landroid/app/Activity;Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 807
    .end local v1    # "intentMultiplelWallpaper":Landroid/content/Intent;
    :cond_b
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-eq v2, v4, :cond_0

    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-ne v2, v7, :cond_8

    goto/16 :goto_0

    .line 822
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 832
    :cond_d
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$8;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    goto/16 :goto_0
.end method
