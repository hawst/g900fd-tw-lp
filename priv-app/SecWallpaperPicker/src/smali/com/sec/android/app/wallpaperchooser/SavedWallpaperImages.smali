.class public Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;
.super Landroid/widget/BaseAdapter;
.source "SavedWallpaperImages.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;,
        Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field mContext:Landroid/content/Context;

.field private mDb:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

.field mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;",
            ">;"
        }
    .end annotation
.end field

.field mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "Wallpapaers.SavedWallpaperImages"

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 114
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 115
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mDb:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

    .line 116
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    .line 117
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 118
    return-void
.end method

.method private getImageFilenames(I)Landroid/util/Pair;
    .locals 14
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v5, 0x0

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mDb:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 208
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "saved_wallpaper_images"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "image_thumbnail"

    aput-object v3, v2, v12

    const-string v3, "image"

    aput-object v3, v2, v13

    const-string v3, "id = ?"

    new-array v4, v13, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v12

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 217
    .local v10, "result":Landroid/database/Cursor;
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 218
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 219
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 220
    .local v11, "thumbFilename":Ljava/lang/String;
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 221
    .local v9, "imageFilename":Ljava/lang/String;
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 222
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 223
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v11, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 227
    .end local v9    # "imageFilename":Ljava/lang/String;
    .end local v11    # "thumbFilename":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 225
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 226
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0
.end method


# virtual methods
.method public deleteImage(I)V
    .locals 9
    .param p1, "id"    # I

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getImageFilenames(I)Landroid/util/Pair;

    move-result-object v1

    .line 233
    .local v1, "filenames":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v1, :cond_0

    .line 234
    sget-object v4, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->TAG:Ljava/lang/String;

    const-string v5, "Failed getImageFilenames"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :goto_0
    return-void

    .line 238
    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 239
    .local v2, "imageFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 240
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    iget-object v4, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-direct {v3, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 241
    .local v3, "thumbFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 242
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mDb:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 243
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "saved_wallpaper_images"

    const-string v5, "id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 248
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getImageFilename(I)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getImageFilenames(I)Landroid/util/Pair;

    move-result-object v0

    .line 200
    .local v0, "filenames":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 201
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 203
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getItem(I)Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 186
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    # getter for: Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mThumb:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->access$000(Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 191
    .local v4, "thumbDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->isMultipleImage()Z

    move-result v5

    .line 192
    .local v5, "multipleImage":Z
    if-nez v4, :cond_0

    .line 193
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error decoding thumbnail for wallpaper #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mLayoutInflater:Landroid/view/LayoutInflater;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createImageTileView(Landroid/view/LayoutInflater;ILandroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public loadThumbnailsAndImageIdList()V
    .locals 18

    .prologue
    .line 121
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mDb:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 123
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v16, 0x0

    .line 124
    .local v16, "result":Landroid/database/Cursor;
    if-eqz v1, :cond_0

    .line 125
    const-string v2, "saved_wallpaper_images"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "image_thumbnail"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "multiple_image"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "id DESC"

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 138
    :cond_0
    if-eqz v16, :cond_3

    :try_start_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 139
    const/4 v10, 0x0

    .line 140
    .local v10, "count":I
    const/4 v14, 0x0

    .line 142
    .local v14, "multipleCount":I
    :cond_1
    const/4 v2, 0x3

    if-lt v10, v2, :cond_6

    .line 143
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->deleteImage(I)V

    .line 165
    :cond_2
    :goto_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 170
    .end local v10    # "count":I
    .end local v14    # "multipleCount":I
    :cond_3
    if-eqz v16, :cond_4

    .line 171
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 172
    :cond_4
    if-eqz v1, :cond_5

    .line 173
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 175
    :cond_5
    :goto_1
    return-void

    .line 147
    .restart local v10    # "count":I
    .restart local v14    # "multipleCount":I
    :cond_6
    const/4 v2, 0x1

    :try_start_1
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 148
    .local v13, "filename":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v12, v2, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 149
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 150
    .local v17, "thumb":Landroid/graphics/Bitmap;
    if-eqz v17, :cond_2

    .line 151
    const/4 v2, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 152
    .local v15, "multipleImage":I
    const/4 v2, 0x1

    if-ne v15, v2, :cond_a

    .line 153
    const/4 v2, 0x1

    if-ge v14, v2, :cond_8

    .line 154
    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-direct {v5, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;-><init>(ILandroid/graphics/drawable/Drawable;Z)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_7
    add-int/lit8 v14, v14, 0x1

    .line 163
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 158
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->deleteImage(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 167
    .end local v10    # "count":I
    .end local v12    # "file":Ljava/io/File;
    .end local v13    # "filename":Ljava/lang/String;
    .end local v14    # "multipleCount":I
    .end local v15    # "multipleImage":I
    .end local v17    # "thumb":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v11

    .line 168
    .local v11, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170
    if-eqz v16, :cond_9

    .line 171
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 172
    :cond_9
    if-eqz v1, :cond_5

    .line 173
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_1

    .line 162
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v10    # "count":I
    .restart local v12    # "file":Ljava/io/File;
    .restart local v13    # "filename":Ljava/lang/String;
    .restart local v14    # "multipleCount":I
    .restart local v15    # "multipleImage":I
    .restart local v17    # "thumb":Landroid/graphics/Bitmap;
    :cond_a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mImages:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-direct {v5, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;-><init>(ILandroid/graphics/drawable/Drawable;Z)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 170
    .end local v10    # "count":I
    .end local v12    # "file":Ljava/io/File;
    .end local v13    # "filename":Ljava/lang/String;
    .end local v14    # "multipleCount":I
    .end local v15    # "multipleImage":I
    .end local v17    # "thumb":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v2

    if-eqz v16, :cond_b

    .line 171
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 172
    :cond_b
    if-eqz v1, :cond_c

    .line 173
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_c
    throw v2
.end method

.method public writeImage(Landroid/graphics/Bitmap;[BZ)V
    .locals 11
    .param p1, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p2, "imageBytes"    # [B
    .param p3, "multipleImage"    # Z

    .prologue
    .line 252
    const/4 v4, 0x0

    .line 254
    .local v4, "imageFileStream":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v8, "wallpaper"

    const-string v9, ""

    iget-object v10, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-static {v8, v9, v10}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    .line 255
    .local v3, "imageFile":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 256
    invoke-virtual {v4, p2}, Ljava/io/FileOutputStream;->write([B)V

    .line 257
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 259
    const-string v8, "wallpaperthumb"

    const-string v9, ""

    iget-object v10, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-static {v8, v9, v10}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    .line 260
    .local v5, "thumbFile":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v6

    .line 261
    .local v6, "thumbFileStream":Ljava/io/FileOutputStream;
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x5f

    invoke-virtual {p1, v8, v9, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 262
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 264
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->mDb:Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;

    invoke-virtual {v8}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$ImageDb;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 265
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 266
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "image_thumbnail"

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v8, "image"

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    if-eqz p3, :cond_0

    .line 269
    const-string v8, "multiple_image"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 272
    :goto_0
    const-string v8, "saved_wallpaper_images"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 273
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 284
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "imageFile":Ljava/io/File;
    .end local v5    # "thumbFile":Ljava/io/File;
    .end local v6    # "thumbFileStream":Ljava/io/FileOutputStream;
    .end local v7    # "values":Landroid/content/ContentValues;
    :goto_1
    return-void

    .line 271
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "imageFile":Ljava/io/File;
    .restart local v5    # "thumbFile":Ljava/io/File;
    .restart local v6    # "thumbFileStream":Ljava/io/FileOutputStream;
    .restart local v7    # "values":Landroid/content/ContentValues;
    :cond_0
    const-string v8, "multiple_image"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "imageFile":Ljava/io/File;
    .end local v5    # "thumbFile":Ljava/io/File;
    .end local v6    # "thumbFileStream":Ljava/io/FileOutputStream;
    .end local v7    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 275
    .local v1, "e":Ljava/io/IOException;
    if-eqz v4, :cond_1

    .line 277
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 282
    :cond_1
    :goto_2
    sget-object v8, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed writing images to storage "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 278
    :catch_1
    move-exception v2

    .line 279
    .local v2, "e1":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed close "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
