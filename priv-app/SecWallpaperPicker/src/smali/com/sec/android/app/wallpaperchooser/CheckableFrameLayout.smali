.class public Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
.super Landroid/widget/FrameLayout;
.source "CheckableFrameLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field mChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->mChecked:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 68
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 69
    .local v0, "drawableState":[I
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    sget-object v1, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->mergeDrawableStates([I[I)[I

    .line 72
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->mChecked:Z

    if-eq p1, v0, :cond_0

    .line 48
    iput-boolean p1, p0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->mChecked:Z

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->refreshDrawableState()V

    .line 51
    :cond_0
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->setChecked(Z)V

    .line 55
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggle(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "checkImage"    # Landroid/widget/ImageView;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->toggle()V

    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->mChecked:Z

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
