.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->displaySimSelectorDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

.field final synthetic val$simSelectorDataAdapter:Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;)V
    .locals 0

    .prologue
    .line 2576
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;->val$simSelectorDataAdapter:Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2578
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;->val$simSelectorDataAdapter:Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->getCommand(I)I

    move-result v0

    .line 2579
    .local v0, "command":I
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->selectSimByUser(I)V

    .line 2580
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 2581
    .local v1, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$16;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 2582
    return-void
.end method
