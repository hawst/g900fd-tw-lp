.class Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;
.super Ljava/lang/Object;
.source "TiledImageRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TileQueue"
.end annotation


# instance fields
.field private mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;

    .prologue
    .line 752
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;-><init>()V

    return-void
.end method

.method private contains(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z
    .locals 2
    .param p1, "tile"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .prologue
    .line 775
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 776
    .local v0, "other":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :goto_0
    if-eqz v0, :cond_1

    .line 777
    if-ne v0, p1, :cond_0

    .line 778
    const/4 v1, 0x1

    .line 782
    :goto_1
    return v1

    .line 780
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mNext:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    goto :goto_0

    .line 782
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 786
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 787
    return-void
.end method

.method public pop()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .locals 2

    .prologue
    .line 756
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 757
    .local v0, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    if-eqz v0, :cond_0

    .line 758
    iget-object v1, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mNext:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    iput-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 760
    :cond_0
    return-object v0
.end method

.method public push(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z
    .locals 3
    .param p1, "tile"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .prologue
    const/4 v0, 0x0

    .line 764
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->contains(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 765
    const-string v1, "TiledImageRenderer"

    const-string v2, "Attempting to add a tile already in the queue!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :goto_0
    return v0

    .line 768
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 769
    .local v0, "wasEmpty":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    iput-object v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mNext:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 770
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->mHead:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    goto :goto_0
.end method
