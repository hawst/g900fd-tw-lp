.class public Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;
.super Ljava/lang/Object;
.source "TiledImageRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;,
        Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;,
        Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;,
        Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;,
        Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;
    }
.end annotation


# static fields
.field private static sTilePool:Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActiveRange:[Landroid/graphics/Rect;

.field private final mActiveTiles:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundTileUploaded:Z

.field protected mCenterX:I

.field protected mCenterY:I

.field private final mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

.field protected mImageHeight:I

.field protected mImageWidth:I

.field private mLayoutTiles:Z

.field private mLevel:I

.field protected mLevelCount:I

.field private mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

.field private mOffsetX:I

.field private mOffsetY:I

.field private mParent:Landroid/view/View;

.field private mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

.field private final mQueueLock:Ljava/lang/Object;

.field private final mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

.field private mRenderComplete:Z

.field protected mRotation:I

.field protected mScale:F

.field private final mSourceRect:Landroid/graphics/RectF;

.field private final mTargetRect:Landroid/graphics/RectF;

.field private mTileDecoder:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;

.field private final mTileRange:Landroid/graphics/Rect;

.field private mTileSize:I

.field private final mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

.field private mUploadQuota:I

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/util/Pools$SynchronizedPool;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/util/Pools$SynchronizedPool;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->sTilePool:Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 4
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput v2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    .line 94
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mSourceRect:Landroid/graphics/RectF;

    .line 95
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTargetRect:Landroid/graphics/RectF;

    .line 97
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    .line 100
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    .line 101
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-direct {v0, v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;-><init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    .line 102
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-direct {v0, v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;-><init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    .line 103
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-direct {v0, v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;-><init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    .line 106
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    .line 107
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    .line 117
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileRange:Landroid/graphics/Rect;

    .line 118
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveRange:[Landroid/graphics/Rect;

    .line 175
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mParent:Landroid/view/View;

    .line 176
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;-><init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$1;)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileDecoder:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileDecoder:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->start()V

    .line 178
    return-void
.end method

.method static synthetic access$200()Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->sTilePool:Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;III)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->getTile(III)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;
    .param p1, "x1"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->decodeTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V

    return-void
.end method

.method private activateTile(III)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 548
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->makeTileKey(III)J

    move-result-wide v0

    .line 549
    .local v0, "key":J
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 550
    .local v2, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    if-eqz v2, :cond_1

    .line 551
    iget v3, v2, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 552
    const/4 v3, 0x1

    iput v3, v2, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->obtainTile(III)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v2

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0, v1, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0
.end method

.method private calculateLevelCount()V
    .locals 6

    .prologue
    .line 204
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    if-eqz v3, :cond_0

    .line 205
    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    invoke-virtual {v5}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/gallery3d/common/Utils;->ceilLog2(F)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    .line 217
    :goto_0
    return-void

    .line 208
    :cond_0
    const/4 v0, 0x1

    .line 209
    .local v0, "levels":I
    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    iget v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 210
    .local v1, "maxDim":I
    iget v2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    .line 211
    .local v2, "t":I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 212
    shl-int/lit8 v2, v2, 0x1

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 215
    :cond_1
    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    goto :goto_0
.end method

.method private decodeTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V
    .locals 4
    .param p1, "tile"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .prologue
    .line 494
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v2

    .line 495
    :try_start_0
    iget v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    .line 496
    monitor-exit v2

    .line 518
    :goto_0
    return-void

    .line 498
    :cond_0
    const/4 v1, 0x4

    iput v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 499
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 500
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->decode()Z

    move-result v0

    .line 501
    .local v0, "decodeComplete":Z
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v2

    .line 502
    :try_start_1
    iget v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/16 v3, 0x20

    if-ne v1, v3, :cond_2

    .line 503
    const/16 v1, 0x40

    iput v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 504
    iget-object v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 505
    sget-object v1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->sTilePool:Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;

    iget-object v3, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    invoke-interface {v1, v3}, Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;->release(Ljava/lang/Object;)Z

    .line 506
    const/4 v1, 0x0

    iput-object v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 508
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->push(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z

    .line 509
    monitor-exit v2

    goto :goto_0

    .line 516
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 499
    .end local v0    # "decodeComplete":Z
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 511
    .restart local v0    # "decodeComplete":Z
    :cond_2
    if-eqz v0, :cond_3

    const/16 v1, 0x8

    :goto_1
    :try_start_3
    iput v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 512
    if-nez v0, :cond_4

    .line 513
    monitor-exit v2

    goto :goto_0

    .line 511
    :cond_3
    const/16 v1, 0x10

    goto :goto_1

    .line 515
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->push(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z

    .line 516
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 517
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->invalidate()V

    goto :goto_0
.end method

.method private drawTile(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;IIIFFF)V
    .locals 12
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;
    .param p2, "tx"    # I
    .param p3, "ty"    # I
    .param p4, "level"    # I
    .param p5, "x"    # F
    .param p6, "y"    # F
    .param p7, "length"    # F

    .prologue
    .line 599
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mSourceRect:Landroid/graphics/RectF;

    .line 600
    .local v5, "source":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTargetRect:Landroid/graphics/RectF;

    .line 601
    .local v6, "target":Landroid/graphics/RectF;
    add-float v8, p5, p7

    add-float v9, p6, p7

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v6, v0, v1, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 602
    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v10, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    int-to-float v11, v11

    invoke-virtual {v5, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 604
    move/from16 v0, p4

    invoke-direct {p0, p2, p3, v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->getTile(III)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v7

    .line 605
    .local v7, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    if-eqz v7, :cond_4

    .line 606
    invoke-virtual {v7}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->isContentValid()Z

    move-result v8

    if-nez v8, :cond_0

    .line 607
    iget v8, v7, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_3

    .line 608
    iget v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQuota:I

    if-lez v8, :cond_2

    .line 609
    iget v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQuota:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQuota:I

    .line 610
    invoke-virtual {v7, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->updateContent(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    .line 619
    :cond_0
    :goto_0
    invoke-direct {p0, v7, p1, v5, v6}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->drawTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 631
    :cond_1
    :goto_1
    return-void

    .line 612
    :cond_2
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRenderComplete:Z

    goto :goto_0

    .line 614
    :cond_3
    iget v8, v7, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/16 v9, 0x10

    if-eq v8, v9, :cond_0

    .line 615
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRenderComplete:Z

    .line 616
    invoke-direct {p0, v7}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->queueForDecode(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V

    goto :goto_0

    .line 623
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    if-eqz v8, :cond_1

    .line 624
    iget v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    shl-int v4, v8, p4

    .line 625
    .local v4, "size":I
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    invoke-virtual {v8}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    int-to-float v9, v9

    div-float v2, v8, v9

    .line 626
    .local v2, "scaleX":F
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    invoke-virtual {v8}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    int-to-float v9, v9

    div-float v3, v8, v9

    .line 627
    .local v3, "scaleY":F
    int-to-float v8, p2

    mul-float/2addr v8, v2

    int-to-float v9, p3

    mul-float/2addr v9, v3

    add-int v10, p2, v4

    int-to-float v10, v10

    mul-float/2addr v10, v2

    add-int v11, p3, v4

    int-to-float v11, v11

    mul-float/2addr v11, v3

    invoke-virtual {v5, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 629
    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    invoke-interface {p1, v8, v5, v6}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_1
.end method

.method private drawTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 4
    .param p1, "tile"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .param p2, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;
    .param p3, "source"    # Landroid/graphics/RectF;
    .param p4, "target"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 636
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->isContentValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 637
    invoke-interface {p2, p1, p3, p4}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 638
    const/4 v1, 0x1

    .line 644
    :goto_1
    return v1

    .line 642
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->getParentTile()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v0

    .line 643
    .local v0, "parent":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    if-nez v0, :cond_1

    .line 644
    const/4 v1, 0x0

    goto :goto_1

    .line 646
    :cond_1
    iget v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mX:I

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mX:I

    if-ne v1, v2, :cond_2

    .line 647
    iget v1, p3, Landroid/graphics/RectF;->left:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->left:F

    .line 648
    iget v1, p3, Landroid/graphics/RectF;->right:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->right:F

    .line 653
    :goto_2
    iget v1, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mY:I

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mY:I

    if-ne v1, v2, :cond_3

    .line 654
    iget v1, p3, Landroid/graphics/RectF;->top:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->top:F

    .line 655
    iget v1, p3, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    .line 660
    :goto_3
    move-object p1, v0

    .line 661
    goto :goto_0

    .line 650
    :cond_2
    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->left:F

    .line 651
    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 657
    :cond_3
    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->top:F

    .line 658
    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method private getRange(Landroid/graphics/Rect;IIIFI)V
    .locals 26
    .param p1, "out"    # Landroid/graphics/Rect;
    .param p2, "cX"    # I
    .param p3, "cY"    # I
    .param p4, "level"    # I
    .param p5, "scale"    # F
    .param p6, "rotation"    # I

    .prologue
    .line 363
    move/from16 v0, p6

    neg-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    .line 364
    .local v10, "radians":D
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v16, v0

    .line 365
    .local v16, "w":D
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewHeight:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v6, v0

    .line 367
    .local v6, "h":D
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 368
    .local v4, "cos":D
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    .line 369
    .local v12, "sin":D
    mul-double v20, v4, v16

    mul-double v22, v12, v6

    sub-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    mul-double v22, v4, v16

    mul-double v24, v12, v6

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->max(DD)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v18, v0

    .line 371
    .local v18, "width":I
    mul-double v20, v12, v16

    mul-double v22, v4, v6

    add-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    mul-double v22, v12, v16

    mul-double v24, v4, v6

    sub-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->max(DD)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v3, v0

    .line 374
    .local v3, "height":I
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    mul-float v21, v21, p5

    div-float v20, v20, v21

    sub-float v19, v19, v20

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v8, v0

    .line 375
    .local v8, "left":I
    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v3

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    mul-float v21, v21, p5

    div-float v20, v20, v21

    sub-float v19, v19, v20

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v15, v0

    .line 376
    .local v15, "top":I
    int-to-float v0, v8

    move/from16 v19, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v20, v20, p5

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v9, v0

    .line 377
    .local v9, "right":I
    int-to-float v0, v15

    move/from16 v19, v0

    int-to-float v0, v3

    move/from16 v20, v0

    div-float v20, v20, p5

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v2, v0

    .line 380
    .local v2, "bottom":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    move/from16 v19, v0

    shl-int v14, v19, p4

    .line 381
    .local v14, "size":I
    const/16 v19, 0x0

    div-int v20, v8, v14

    mul-int v20, v20, v14

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 382
    const/16 v19, 0x0

    div-int v20, v15, v14

    mul-int v20, v20, v14

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 383
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 384
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 386
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15, v9, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 387
    return-void
.end method

.method private getRange(Landroid/graphics/Rect;IIII)V
    .locals 7
    .param p1, "out"    # Landroid/graphics/Rect;
    .param p2, "cX"    # I
    .param p3, "cY"    # I
    .param p4, "level"    # I
    .param p5, "rotation"    # I

    .prologue
    .line 351
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    add-int/lit8 v2, p4, 0x1

    shl-int/2addr v1, v2

    int-to-float v1, v1

    div-float v5, v0, v1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->getRange(Landroid/graphics/Rect;IIIFI)V

    .line 352
    return-void
.end method

.method private getTile(III)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 561
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-static {p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->makeTileKey(III)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    return-object v0
.end method

.method private invalidate()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 190
    return-void
.end method

.method private invalidateTiles()V
    .locals 5

    .prologue
    .line 336
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v4

    .line 337
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->clean()V

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->clean()V

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 342
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 343
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 344
    .local v2, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    invoke-direct {p0, v2}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->recycleTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    .end local v2    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->clear()V

    .line 347
    monitor-exit v4

    .line 348
    return-void

    .line 347
    .end local v0    # "i":I
    .end local v1    # "n":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private static isHighResolution(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x800

    .line 167
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 168
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 170
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 171
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v2, v3, :cond_0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v2, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private layoutTiles()V
    .locals 22

    .prologue
    .line 260
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewWidth:I

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewHeight:I

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLayoutTiles:Z

    if-nez v3, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLayoutTiles:Z

    .line 271
    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    div-float/2addr v3, v5

    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/gallery3d/common/Utils;->floorLog2(F)I

    move-result v3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    invoke-static {v3, v5, v6}, Lcom/sec/android/app/wallpaperchooser/gallery3d/common/Utils;->clamp(III)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    .line 276
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    if-eq v3, v5, :cond_3

    .line 277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileRange:Landroid/graphics/Rect;

    .line 278
    .local v4, "range":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterX:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterY:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRotation:I

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->getRange(Landroid/graphics/Rect;IIIFI)V

    .line 279
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewWidth:I

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iget v5, v4, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterX:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float/2addr v5, v6

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetX:I

    .line 280
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewHeight:I

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iget v5, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterY:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float/2addr v5, v6

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetY:I

    .line 281
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    shl-int/2addr v5, v6

    int-to-float v5, v5

    mul-float/2addr v3, v5

    const/high16 v5, 0x3f400000    # 0.75f

    cmpl-float v3, v3, v5

    if-lez v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    add-int/lit8 v13, v3, -0x1

    .line 289
    .end local v4    # "range":Landroid/graphics/Rect;
    .local v13, "fromLevel":I
    :goto_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    add-int/lit8 v5, v5, -0x2

    invoke-static {v13, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 290
    add-int/lit8 v3, v13, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 292
    .local v12, "endLevel":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveRange:[Landroid/graphics/Rect;

    .line 293
    .local v4, "range":[Landroid/graphics/Rect;
    move v9, v13

    .local v9, "i":I
    :goto_2
    if-ge v9, v12, :cond_4

    .line 294
    sub-int v3, v9, v13

    aget-object v6, v4, v3

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterX:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterY:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRotation:I

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->getRange(Landroid/graphics/Rect;IIII)V

    .line 293
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 281
    .end local v9    # "i":I
    .end local v12    # "endLevel":I
    .end local v13    # "fromLevel":I
    .local v4, "range":Landroid/graphics/Rect;
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    goto :goto_1

    .line 284
    .end local v4    # "range":Landroid/graphics/Rect;
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    add-int/lit8 v13, v3, -0x2

    .line 285
    .restart local v13    # "fromLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewWidth:I

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterX:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float/2addr v5, v6

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetX:I

    .line 286
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewHeight:I

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterY:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float/2addr v5, v6

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetY:I

    goto :goto_1

    .line 298
    .local v4, "range":[Landroid/graphics/Rect;
    .restart local v9    # "i":I
    .restart local v12    # "endLevel":I
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRotation:I

    rem-int/lit8 v3, v3, 0x5a

    if-nez v3, :cond_0

    .line 302
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v5

    .line 303
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->clean()V

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->clean()V

    .line 305
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mBackgroundTileUploaded:Z

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v15

    .line 310
    .local v15, "n":I
    const/4 v9, 0x0

    :goto_3
    if-ge v9, v15, :cond_7

    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v9}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 312
    .local v19, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    move-object/from16 v0, v19

    iget v14, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileLevel:I

    .line 313
    .local v14, "level":I
    if-lt v14, v13, :cond_5

    if-ge v14, v12, :cond_5

    sub-int v3, v14, v13

    aget-object v3, v4, v3

    move-object/from16 v0, v19

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mX:I

    move-object/from16 v0, v19

    iget v7, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mY:I

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_6

    .line 315
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v9}, Landroid/util/LongSparseArray;->removeAt(I)V

    .line 316
    add-int/lit8 v9, v9, -0x1

    .line 317
    add-int/lit8 v15, v15, -0x1

    .line 318
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->recycleTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V

    .line 310
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 321
    .end local v14    # "level":I
    .end local v19    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :cond_7
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    move v9, v13

    :goto_4
    if-ge v9, v12, :cond_a

    .line 324
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    shl-int v18, v3, v9

    .line 325
    .local v18, "size":I
    sub-int v3, v9, v13

    aget-object v16, v4, v3

    .line 326
    .local v16, "r":Landroid/graphics/Rect;
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    .local v21, "y":I
    move-object/from16 v0, v16

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .local v11, "bottom":I
    :goto_5
    move/from16 v0, v21

    if-ge v0, v11, :cond_9

    .line 327
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    .local v20, "x":I
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    .local v17, "right":I
    :goto_6
    move/from16 v0, v20

    move/from16 v1, v17

    if-ge v0, v1, :cond_8

    .line 328
    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->activateTile(III)V

    .line 327
    add-int v20, v20, v18

    goto :goto_6

    .line 321
    .end local v11    # "bottom":I
    .end local v15    # "n":I
    .end local v16    # "r":Landroid/graphics/Rect;
    .end local v17    # "right":I
    .end local v18    # "size":I
    .end local v20    # "x":I
    .end local v21    # "y":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 326
    .restart local v11    # "bottom":I
    .restart local v15    # "n":I
    .restart local v16    # "r":Landroid/graphics/Rect;
    .restart local v17    # "right":I
    .restart local v18    # "size":I
    .restart local v20    # "x":I
    .restart local v21    # "y":I
    :cond_8
    add-int v21, v21, v18

    goto :goto_5

    .line 323
    .end local v17    # "right":I
    .end local v20    # "x":I
    :cond_9
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 332
    .end local v11    # "bottom":I
    .end local v16    # "r":Landroid/graphics/Rect;
    .end local v18    # "size":I
    .end local v21    # "y":I
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->invalidate()V

    goto/16 :goto_0
.end method

.method private static makeTileKey(III)J
    .locals 7
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "level"    # I

    .prologue
    const/16 v6, 0x10

    .line 565
    int-to-long v0, p0

    .line 566
    .local v0, "result":J
    shl-long v2, v0, v6

    int-to-long v4, p1

    or-long v0, v2, v4

    .line 567
    shl-long v2, v0, v6

    int-to-long v4, p2

    or-long v0, v2, v4

    .line 568
    return-wide v0
.end method

.method private obtainTile(III)Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 521
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v2

    .line 522
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->pop()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v0

    .line 523
    .local v0, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    if-eqz v0, :cond_0

    .line 524
    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 525
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->update(III)V

    .line 526
    monitor-exit v2

    .line 528
    .end local v0    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :goto_0
    return-object v0

    .restart local v0    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :cond_0
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .end local v0    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;-><init>(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;III)V

    monitor-exit v2

    goto :goto_0

    .line 529
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private queueForDecode(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V
    .locals 3
    .param p1, "tile"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .prologue
    .line 483
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v1

    .line 484
    :try_start_0
    iget v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 485
    const/4 v0, 0x2

    iput v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->push(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 490
    :cond_0
    monitor-exit v1

    .line 491
    return-void

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private recycleTile(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V
    .locals 3
    .param p1, "tile"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .prologue
    .line 533
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v1

    .line 534
    :try_start_0
    iget v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 535
    const/16 v0, 0x20

    iput v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 536
    monitor-exit v1

    .line 545
    :goto_0
    return-void

    .line 538
    :cond_0
    const/16 v0, 0x40

    iput v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    .line 539
    iget-object v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 540
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->sTilePool:Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;

    iget-object v2, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    invoke-interface {v0, v2}, Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;->release(Ljava/lang/Object;)Z

    .line 541
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->push(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)Z

    .line 544
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static suggestedTileSize(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    invoke-static {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->isHighResolution(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x200

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x100

    goto :goto_0
.end method

.method private uploadBackgroundTiles(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V
    .locals 4
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 472
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mBackgroundTileUploaded:Z

    .line 473
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 474
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 475
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 476
    .local v2, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->isContentValid()Z

    move-result v3

    if-nez v3, :cond_0

    .line 477
    invoke-direct {p0, v2}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->queueForDecode(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;)V

    .line 474
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 480
    .end local v2    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :cond_1
    return-void
.end method

.method private uploadTiles(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V
    .locals 5
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 572
    const/4 v0, 0x1

    .line 573
    .local v0, "quota":I
    const/4 v1, 0x0

    .line 574
    .local v1, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 575
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v3

    .line 576
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->pop()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v1

    .line 577
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    if-nez v1, :cond_3

    .line 590
    :cond_1
    if-eqz v1, :cond_2

    .line 591
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->invalidate()V

    .line 593
    :cond_2
    return-void

    .line 577
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 581
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->isContentValid()Z

    move-result v2

    if-nez v2, :cond_0

    .line 582
    iget v2, v1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 583
    invoke-virtual {v1, p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->updateContent(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    .line 584
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 586
    :cond_4
    const-string v2, "TiledImageRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tile in upload queue has invalid state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->mTileState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public draw(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)Z
    .locals 24
    .param p1, "canvas"    # Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 415
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->layoutTiles()V

    .line 416
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->uploadTiles(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    .line 418
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQuota:I

    .line 419
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRenderComplete:Z

    .line 421
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevel:I

    .line 422
    .local v6, "level":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRotation:I

    move/from16 v22, v0

    .line 423
    .local v22, "rotation":I
    const/16 v18, 0x0

    .line 424
    .local v18, "flags":I
    if-eqz v22, :cond_0

    .line 425
    or-int/lit8 v18, v18, 0x2

    .line 428
    :cond_0
    if-eqz v18, :cond_1

    .line 429
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 430
    if-eqz v22, :cond_1

    .line 431
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewWidth:I

    div-int/lit8 v16, v2, 0x2

    .local v16, "centerX":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewHeight:I

    div-int/lit8 v17, v2, 0x2

    .line 432
    .local v17, "centerY":I
    move/from16 v0, v16

    int-to-float v2, v0

    move/from16 v0, v17

    int-to-float v3, v0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 433
    move/from16 v0, v22

    int-to-float v2, v0

    const/4 v3, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v10, v11}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 434
    move/from16 v0, v16

    neg-int v2, v0

    int-to-float v2, v2

    move/from16 v0, v17

    neg-int v3, v0

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 438
    .end local v16    # "centerX":I
    .end local v17    # "centerY":I
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    if-eq v6, v2, :cond_3

    .line 439
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    shl-int v23, v2, v6

    .line 440
    .local v23, "size":I
    move/from16 v0, v23

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float v9, v2, v3

    .line 441
    .local v9, "length":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileRange:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    .line 443
    .local v21, "r":Landroid/graphics/Rect;
    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/Rect;->top:I

    .local v5, "ty":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v5, v2, :cond_4

    .line 444
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetY:I

    int-to-float v2, v2

    move/from16 v0, v19

    int-to-float v3, v0

    mul-float/2addr v3, v9

    add-float v8, v2, v3

    .line 445
    .local v8, "y":F
    move-object/from16 v0, v21

    iget v4, v0, Landroid/graphics/Rect;->left:I

    .local v4, "tx":I
    const/16 v20, 0x0

    .local v20, "j":I
    :goto_1
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Rect;->right:I

    if-ge v4, v2, :cond_2

    .line 446
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetX:I

    int-to-float v2, v2

    move/from16 v0, v20

    int-to-float v3, v0

    mul-float/2addr v3, v9

    add-float v7, v2, v3

    .local v7, "x":F
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 447
    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->drawTile(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;IIIFFF)V

    .line 445
    add-int v4, v4, v23

    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 443
    .end local v7    # "x":F
    :cond_2
    add-int v5, v5, v23

    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 450
    .end local v4    # "tx":I
    .end local v5    # "ty":I
    .end local v8    # "y":F
    .end local v9    # "length":F
    .end local v19    # "i":I
    .end local v20    # "j":I
    .end local v21    # "r":Landroid/graphics/Rect;
    .end local v23    # "size":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    if-eqz v2, :cond_4

    .line 451
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetX:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mOffsetY:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v15

    move-object/from16 v11, p1

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;->draw(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    :cond_4
    if-eqz v18, :cond_5

    .line 457
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 461
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRenderComplete:Z

    if-eqz v2, :cond_9

    .line 462
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mBackgroundTileUploaded:Z

    if-nez v2, :cond_6

    .line 463
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->uploadBackgroundTiles(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;)V

    .line 468
    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRenderComplete:Z

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    if-eqz v2, :cond_a

    :cond_7
    const/4 v2, 0x1

    :goto_3
    return v2

    .line 456
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_8

    .line 457
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;->restore()V

    :cond_8
    throw v2

    .line 466
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->invalidate()V

    goto :goto_2

    .line 468
    :cond_a
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public freeTextures()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 390
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLayoutTiles:Z

    .line 392
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileDecoder:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileDecoder;->finishAndWait()V

    .line 393
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mQueueLock:Ljava/lang/Object;

    monitor-enter v5

    .line 394
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mUploadQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->clean()V

    .line 395
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mDecodeQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->clean()V

    .line 396
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->pop()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v3

    .line 397
    .local v3, "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :goto_0
    if-eqz v3, :cond_0

    .line 398
    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->recycle()V

    .line 399
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRecycledQueue:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileQueue;->pop()Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    move-result-object v3

    goto :goto_0

    .line 401
    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 404
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v4, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;

    .line 406
    .local v2, "texture":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;->recycle()V

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 401
    .end local v0    # "i":I
    .end local v1    # "n":I
    .end local v2    # "texture":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    .end local v3    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 408
    .restart local v0    # "i":I
    .restart local v1    # "n":I
    .restart local v3    # "tile":Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$Tile;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/util/LongSparseArray;->clear()V

    .line 409
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileRange:Landroid/graphics/Rect;

    invoke-virtual {v4, v6, v6, v6, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 411
    :cond_2
    sget-object v4, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->sTilePool:Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;

    invoke-interface {v4}, Lcom/sec/android/app/wallpaperchooser/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 412
    return-void
.end method

.method public notifyModelInvalidated()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->invalidateTiles()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    if-nez v0, :cond_0

    .line 222
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    .line 223
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    .line 224
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLevelCount:I

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    .line 233
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLayoutTiles:Z

    .line 234
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageWidth:I

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mImageHeight:I

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;->getPreview()Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mPreview:Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;->getTileSize()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mTileSize:I

    .line 231
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->calculateLevelCount()V

    goto :goto_0
.end method

.method public setModel(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;I)V
    .locals 1
    .param p1, "model"    # Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;
    .param p2, "rotation"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    if-eq v0, p1, :cond_0

    .line 194
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mModel:Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->notifyModelInvalidated()V

    .line 197
    :cond_0
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRotation:I

    if-eq v0, p2, :cond_1

    .line 198
    iput p2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mRotation:I

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLayoutTiles:Z

    .line 201
    :cond_1
    return-void
.end method

.method public setPosition(IIF)V
    .locals 1
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I
    .param p3, "scale"    # F

    .prologue
    .line 242
    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterX:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterY:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 246
    :cond_0
    iput p1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterX:I

    .line 247
    iput p2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mCenterY:I

    .line 248
    iput p3, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mScale:F

    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mLayoutTiles:Z

    goto :goto_0
.end method

.method public setViewSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 237
    iput p1, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewWidth:I

    .line 238
    iput p2, p0, Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer;->mViewHeight:I

    .line 239
    return-void
.end method
