.class public Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;
.super Ljava/lang/Object;
.source "LauncherCustomer.java"


# static fields
.field private static mCscImageCount:I

.field private static mCustomerCount:I

.field private static mCustomerList:Lorg/w3c/dom/NodeList;

.field private static mCustomerNode:Lorg/w3c/dom/Node;

.field private static mDoc:Lorg/w3c/dom/Document;

.field private static mRoot:Lorg/w3c/dom/Node;

.field private static sInstance:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;


# instance fields
.field isCSC:Z

.field private isMImagesFromCsc:[Ljava/lang/Boolean;

.field private mImagesFromCsc:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field totalImage:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    invoke-direct {v0}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;-><init>()V

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->sInstance:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530
    iput-boolean v1, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isCSC:Z

    .line 532
    iput v1, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->totalImage:I

    .line 114
    const-string v0, "customer.xml"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->loadXMLFile(Ljava/lang/String;I)V

    .line 115
    return-void
.end method

.method private getAttribute(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 290
    sget-object v1, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 291
    sget-object v1, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v1, p1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 292
    .local v0, "list":Lorg/w3c/dom/Element;
    const-string v1, "src"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 294
    .end local v0    # "list":Lorg/w3c/dom/Element;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInstance(Ljava/lang/String;I)Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;
    .locals 2
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "mode"    # I

    .prologue
    .line 118
    if-nez p0, :cond_0

    .line 119
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->sInstance:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    const-string v1, "customer.xml"

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->loadXMLFile(Ljava/lang/String;I)V

    .line 122
    :goto_0
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->sInstance:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    return-object v0

    .line 121
    :cond_0
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->sInstance:Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->loadXMLFile(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private getTagCount(Lorg/w3c/dom/NodeList;)I
    .locals 1
    .param p1, "list"    # Lorg/w3c/dom/NodeList;

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "count":I
    if-eqz p1, :cond_0

    .line 245
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    .line 246
    :cond_0
    return v0
.end method

.method private getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;
    .locals 7
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 217
    sget-object v5, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mDoc:Lorg/w3c/dom/Document;

    if-eqz v5, :cond_0

    if-nez p1, :cond_1

    .line 218
    :cond_0
    const/4 v5, 0x0

    .line 232
    :goto_0
    return-object v5

    .line 220
    :cond_1
    sget-object v5, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mDoc:Lorg/w3c/dom/Document;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 221
    .local v3, "list":Lorg/w3c/dom/Element;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 222
    .local v1, "children":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_3

    .line 223
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 224
    .local v4, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 225
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 226
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 227
    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 224
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 232
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v4    # "n":I
    :cond_3
    invoke-interface {v3}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    goto :goto_0
.end method

.method private getTagNode(Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "tagFullName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 187
    sget-object v4, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mRoot:Lorg/w3c/dom/Node;

    if-nez v4, :cond_1

    move-object v0, v3

    .line 200
    :cond_0
    :goto_0
    return-object v0

    .line 190
    :cond_1
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mRoot:Lorg/w3c/dom/Node;

    .line 191
    .local v0, "node":Lorg/w3c/dom/Node;
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v4, "."

    invoke-direct {v2, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .local v2, "tokenizer":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 194
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "tagName":Ljava/lang/String;
    if-nez v0, :cond_2

    move-object v0, v3

    .line 196
    goto :goto_0

    .line 197
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 198
    goto :goto_1
.end method

.method private getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 205
    .local v1, "children":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_1

    .line 206
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    .line 207
    .local v3, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 208
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 209
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 213
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v3    # "n":I
    :goto_1
    return-object v0

    .line 207
    .restart local v0    # "child":Lorg/w3c/dom/Node;
    .restart local v2    # "i":I
    .restart local v3    # "n":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v3    # "n":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private initLauncherCustomer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 250
    sput-object v1, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    .line 251
    sput-object v1, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    .line 252
    sput v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    .line 254
    sput v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCscImageCount:I

    .line 256
    return-void
.end method

.method private loadWallpaperNameFromCsc(Lorg/xmlpull/v1/XmlPullParser;Z)I
    .locals 9
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "isCSC"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x18

    const/4 v8, 0x1

    .line 538
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    .line 539
    new-array v6, v7, [Ljava/lang/Boolean;

    iput-object v6, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isMImagesFromCsc:[Ljava/lang/Boolean;

    .line 540
    const/4 v4, 0x0

    .line 541
    .local v4, "total":I
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v3

    .line 543
    .local v3, "startDepth":I
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    .local v5, "type":I
    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    if-le v6, v3, :cond_2

    .line 544
    :cond_1
    if-ne v5, v8, :cond_3

    .line 565
    :cond_2
    return v4

    .line 547
    :cond_3
    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 550
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 552
    .local v2, "name":Ljava/lang/String;
    const-string v6, "item"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 554
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 555
    .local v1, "imageName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/system/wallpaper/drawable/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 556
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 557
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isMImagesFromCsc:[Ljava/lang/Boolean;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v4

    .line 561
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 559
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isMImagesFromCsc:[Ljava/lang/Boolean;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v4

    goto :goto_1
.end method

.method private loadXMLFile(Ljava/lang/String;I)V
    .locals 16
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 128
    const/4 v11, 0x2

    move/from16 v0, p2

    if-ne v0, v11, :cond_1

    .line 129
    move-object/from16 v9, p1

    .line 135
    .local v9, "fullPath":Ljava/lang/String;
    :goto_0
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->initLauncherCustomer()V

    .line 136
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v6

    .line 137
    .local v6, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v6}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 138
    .local v2, "builder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v11

    sput-object v11, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mDoc:Lorg/w3c/dom/Document;

    .line 139
    sget-object v11, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mDoc:Lorg/w3c/dom/Document;

    invoke-interface {v11}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v11

    sput-object v11, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mRoot:Lorg/w3c/dom/Node;

    .line 140
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->setLauncherCustomer(I)V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 150
    .end local v2    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :goto_1
    const/4 v7, 0x0

    .line 151
    .local v7, "fileReader":Ljava/io/FileReader;
    const/4 v10, 0x0

    .line 154
    .local v10, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v3, Ljava/io/File;

    const-string v11, "/system/wallpaper/wallpapers.xml"

    invoke-direct {v3, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 155
    .local v3, "cscFileChk":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-lez v11, :cond_0

    .line 157
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v6

    .line 158
    .local v6, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    .end local v7    # "fileReader":Ljava/io/FileReader;
    .local v8, "fileReader":Ljava/io/FileReader;
    :try_start_2
    invoke-virtual {v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v10

    .line 160
    invoke-interface {v10, v8}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 161
    invoke-interface {v10}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    .line 163
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isCSC:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->loadWallpaperNameFromCsc(Lorg/xmlpull/v1/XmlPullParser;Z)I

    move-result v11

    sput v11, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCscImageCount:I

    .line 164
    sget v11, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCscImageCount:I

    if-lez v11, :cond_2

    .line 165
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isCSC:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 174
    :goto_2
    if-eqz v8, :cond_4

    .line 176
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    move-object v7, v8

    .line 184
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "fileReader":Ljava/io/FileReader;
    .restart local v7    # "fileReader":Ljava/io/FileReader;
    :cond_0
    :goto_3
    return-void

    .line 131
    .end local v3    # "cscFileChk":Ljava/io/File;
    .end local v7    # "fileReader":Ljava/io/FileReader;
    .end local v9    # "fullPath":Ljava/lang/String;
    .end local v10    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "/system/csc/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "fullPath":Ljava/lang/String;
    goto/16 :goto_0

    .line 141
    :catch_0
    move-exception v5

    .line 142
    .local v5, "ex":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v11, "Launcher.LauncherCustomer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ParserConfigurationException:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 143
    .end local v5    # "ex":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v5

    .line 144
    .local v5, "ex":Lorg/xml/sax/SAXException;
    const-string v11, "Launcher.LauncherCustomer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "SAXException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 145
    .end local v5    # "ex":Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v5

    .line 146
    .local v5, "ex":Ljava/io/IOException;
    const-string v11, "Launcher.LauncherCustomer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IOException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 167
    .end local v5    # "ex":Ljava/io/IOException;
    .restart local v3    # "cscFileChk":Ljava/io/File;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "fileReader":Ljava/io/FileReader;
    .restart local v10    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_2
    const/4 v11, 0x0

    :try_start_4
    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->isCSC:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 171
    :catch_3
    move-exception v4

    move-object v7, v8

    .line 172
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "fileReader":Ljava/io/FileReader;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v7    # "fileReader":Ljava/io/FileReader;
    :goto_4
    :try_start_5
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 174
    if-eqz v7, :cond_0

    .line 176
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_3

    .line 177
    :catch_4
    move-exception v4

    .line 178
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 177
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v7    # "fileReader":Ljava/io/FileReader;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "fileReader":Ljava/io/FileReader;
    :catch_5
    move-exception v4

    .line 178
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    move-object v7, v8

    .line 179
    .end local v8    # "fileReader":Ljava/io/FileReader;
    .restart local v7    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_3

    .line 174
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catchall_0
    move-exception v11

    :goto_5
    if-eqz v7, :cond_3

    .line 176
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    .line 179
    :cond_3
    :goto_6
    throw v11

    .line 177
    :catch_6
    move-exception v4

    .line 178
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 174
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v7    # "fileReader":Ljava/io/FileReader;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "fileReader":Ljava/io/FileReader;
    :catchall_1
    move-exception v11

    move-object v7, v8

    .end local v8    # "fileReader":Ljava/io/FileReader;
    .restart local v7    # "fileReader":Ljava/io/FileReader;
    goto :goto_5

    .line 171
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catch_7
    move-exception v4

    goto :goto_4

    .end local v7    # "fileReader":Ljava/io/FileReader;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "fileReader":Ljava/io/FileReader;
    :cond_4
    move-object v7, v8

    .end local v8    # "fileReader":Ljava/io/FileReader;
    .restart local v7    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_3
.end method

.method private readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 576
    const-string v0, ""

    .line 577
    .local v0, "result":Ljava/lang/String;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 578
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 579
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 581
    :cond_0
    return-object v0
.end method

.method private setLauncherCustomer(I)V
    .locals 7
    .param p1, "mode"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    .line 263
    if-eqz p1, :cond_0

    if-ne p1, v6, :cond_5

    .line 264
    :cond_0
    const-string v3, "Settings.Main.Display"

    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagNode(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    .line 265
    sget-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    const-string v4, "Wallpaper"

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    .line 266
    if-ne p1, v6, :cond_2

    .line 267
    const/4 v3, 0x0

    sput v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    .line 287
    :cond_1
    :goto_0
    return-void

    .line 269
    :cond_2
    sget-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagCount(Lorg/w3c/dom/NodeList;)I

    move-result v3

    sput v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    .line 270
    sget v2, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    .line 271
    .local v2, "mCustomerFileCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    if-ge v1, v3, :cond_4

    .line 272
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/system/wallpaper/drawable/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v1, v5}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getCustomerWallpaer(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 274
    const/4 v2, 0x0

    .line 271
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 276
    .end local v0    # "f":Ljava/io/File;
    :cond_4
    sput v2, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    goto :goto_0

    .line 282
    .end local v1    # "i":I
    .end local v2    # "mCustomerFileCount":I
    :cond_5
    if-eq p1, v5, :cond_6

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 283
    :cond_6
    const-string v3, "Launcher"

    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagNode(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    .line 284
    sget-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    const-string v4, "favorites"

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    .line 285
    sget-object v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-direct {p0, v3}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getTagCount(Lorg/w3c/dom/NodeList;)I

    move-result v3

    sput v3, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    goto :goto_0
.end method


# virtual methods
.method public getCscImageSmallFileName(IZ)Ljava/lang/String;
    .locals 3
    .param p1, "position"    # I
    .param p2, "small"    # Z

    .prologue
    const/4 v0, 0x0

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-object v0

    .line 602
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->totalImage:I

    if-ge v1, v2, :cond_2

    .line 603
    add-int/lit8 p1, p1, -0x1

    .line 605
    :cond_2
    sget v1, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCscImageCount:I

    if-le v1, p1, :cond_0

    .line 606
    if-eqz p2, :cond_3

    .line 607
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_small"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 609
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCustomerCount()I
    .locals 1

    .prologue
    .line 335
    sget v0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mCustomerCount:I

    return v0
.end method

.method public getCustomerWallpaer(II)Ljava/lang/String;
    .locals 9
    .param p1, "index"    # I
    .param p2, "mode"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 312
    const/4 v4, 0x0

    .line 313
    .local v4, "wallpaper":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getAttribute(I)Ljava/lang/String;

    move-result-object v1

    .line 315
    .local v1, "strAttr":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 316
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 317
    .local v3, "strSlash":[Ljava/lang/String;
    array-length v5, v3

    add-int/lit8 v0, v5, -0x1

    .line 319
    .local v0, "cntSlash":I
    aget-object v5, v3, v0

    if-eqz v5, :cond_0

    .line 320
    if-nez p2, :cond_1

    .line 321
    aget-object v5, v3, v0

    const-string v6, "[.]"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 323
    .local v2, "strDots":[Ljava/lang/String;
    aget-object v5, v2, v8

    if-eqz v5, :cond_0

    aget-object v5, v2, v7

    if-eqz v5, :cond_0

    .line 324
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v2, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_small"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 331
    .end local v0    # "cntSlash":I
    .end local v2    # "strDots":[Ljava/lang/String;
    .end local v3    # "strSlash":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 326
    .restart local v0    # "cntSlash":I
    .restart local v3    # "strSlash":[Ljava/lang/String;
    :cond_1
    if-ne p2, v7, :cond_0

    .line 327
    aget-object v4, v3, v0

    goto :goto_0
.end method

.method public getCustomerWallpaperName(I)Ljava/lang/String;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 298
    const/4 v4, 0x0

    .line 299
    .local v4, "wallpaper":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->getAttribute(I)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "strAttr":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 302
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 303
    .local v3, "strSlash":[Ljava/lang/String;
    array-length v5, v3

    add-int/lit8 v0, v5, -0x1

    .line 304
    .local v0, "cntSlash":I
    aget-object v5, v3, v0

    const-string v6, "[.]"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 305
    .local v2, "strDots":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v4, v2, v5

    .line 308
    .end local v0    # "cntSlash":I
    .end local v2    # "strDots":[Ljava/lang/String;
    .end local v3    # "strSlash":[Ljava/lang/String;
    :cond_0
    return-object v4
.end method

.method public getImagesFromCsc()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/LauncherCustomer;->mImagesFromCsc:Ljava/util/ArrayList;

    return-object v0
.end method
