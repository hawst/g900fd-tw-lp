.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

.field final synthetic val$a:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0

    .prologue
    .line 347
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;->val$a:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapCropped([B)V
    .locals 7
    .param p1, "imageBytes"    # [B

    .prologue
    const/4 v1, 0x0

    .line 349
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;->val$a:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getDefaultThumbnailSize(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v0

    .line 351
    .local v0, "thumbSize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mOrientation:I
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->access$200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;)I

    move-result v4

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createThumbnail(Landroid/graphics/Point;Landroid/content/Context;Ljava/lang/String;[BIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 352
    .local v6, "thumb":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_0

    .line 353
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;->val$a:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSavedImages()Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo$1;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->mMultipleImage:Z
    invoke-static {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;->access$300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$FileWallpaperInfo;)Z

    move-result v2

    invoke-virtual {v1, v6, p1, v2}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->writeImage(Landroid/graphics/Bitmap;[BZ)V

    .line 356
    :goto_0
    return-void

    .line 355
    :cond_0
    const-string v1, "Wallpapers.WallpaperPickerActivity"

    const-string v2, "Could not create thumbnail bitmap. Cannot write to DB."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
