.class public Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "WallpaperPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MontblancWallpaperInfo"
.end annotation


# instance fields
.field private mImageIndex:I

.field private mResId:I

.field private mResources:Landroid/content/res/Resources;

.field private mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

.field private mThumb:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;I)V
    .locals 0
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p4, "imageIndex"    # I

    .prologue
    .line 455
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    .line 456
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResources:Landroid/content/res/Resources;

    .line 457
    iput p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResId:I

    .line 458
    iput-object p3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 459
    iput p4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mImageIndex:I

    .line 460
    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mThumb:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public isNamelessWallpaper()Z
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x1

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 493
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 10
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    const/4 v9, 0x0

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResId:I

    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Landroid/content/res/Resources;I)I

    move-result v5

    .line 465
    .local v5, "rotation":I
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    if-nez v0, :cond_0

    .line 466
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResId:I

    const/16 v4, 0x400

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/res/Resources;Landroid/content/Context;III)V

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    .line 468
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;

    move-result-object v7

    .line 469
    .local v7, "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 470
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getDefaultWallpaperSize(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v8

    .line 471
    .local v8, "wallpaperSize":Landroid/graphics/Point;
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;->getImageWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;->getImageHeight()I

    move-result v1

    iget v2, v8, Landroid/graphics/Point;->x:I

    iget v3, v8, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1, v2, v3, v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getMaxCropRect(IIIIZ)Landroid/graphics/RectF;

    move-result-object v6

    .line 472
    .local v6, "crop":Landroid/graphics/RectF;
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-ge v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 473
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v7, v0}, Lcom/sec/android/app/wallpaperchooser/CropView;->setScale(F)V

    .line 476
    :goto_0
    invoke-virtual {v7, v9}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    .line 477
    return-void

    .line 475
    :cond_2
    iget v0, v8, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {v7, v0}, Lcom/sec/android/app/wallpaperchooser/CropView;->setScale(F)V

    goto :goto_0
.end method

.method public onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 5
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 481
    const/4 v0, 0x1

    .line 483
    .local v0, "finishActivityWhenDone":Z
    sget v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 484
    :cond_0
    const-string v1, "Wallpapers.WallpaperPickerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set lockscreen_montblanc_wallpaper = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mImageIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "lockscreen_montblanc_wallpaper"

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mImageIndex:I

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 488
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResources:Landroid/content/res/Resources;

    iget v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mResId:I

    invoke-virtual {p1, v1, v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->cropImageAndSetWallpaper(Landroid/content/res/Resources;IZ)V

    .line 489
    return-void
.end method
