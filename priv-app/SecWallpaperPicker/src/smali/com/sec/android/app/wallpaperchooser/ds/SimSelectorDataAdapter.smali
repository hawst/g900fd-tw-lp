.class public Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;
.super Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;
.source "SimSelectorDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;
    }
.end annotation


# static fields
.field static final isMultiSIM:Z

.field static final isSkipCTC:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->isSkipCTC:Z

    .line 21
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_USE_MULTISIM"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->isSkipCTC:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->isMultiSIM:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-static {p1}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->getData(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 24
    return-void
.end method

.method protected static addItem(Ljava/util/List;Ljava/lang/String;II)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "resource"    # I
    .param p3, "command"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;",
            ">;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;>;"
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->isMultiSIM:Z

    if-eqz v1, :cond_0

    .line 56
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;-><init>(Ljava/lang/String;II)V

    .line 57
    .local v0, "temp":Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    .end local v0    # "temp":Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;
    :cond_0
    return-void
.end method

.method protected static getData(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    sget-boolean v2, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->isMultiSIM:Z

    if-eqz v2, :cond_0

    .line 37
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "allSim":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 40
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;>;"
    const/4 v2, -0x1

    invoke-static {v1, v0, v2, v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 41
    invoke-static {p0, v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getSimName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getSimIcon(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 44
    invoke-static {p0, v5}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getSimName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v5}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getSimIcon(Landroid/content/Context;I)I

    move-result v3

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 50
    .end local v0    # "allSim":Ljava/lang/String;
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;>;"
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCommand(I)I
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 27
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->isMultiSIM:Z

    if-eqz v1, :cond_0

    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;

    .line 29
    .local v0, "item":Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;->getCommand()I

    move-result v1

    .line 31
    .end local v0    # "item":Lcom/sec/android/app/wallpaperchooser/ds/SimSelectorDataAdapter$SimSelectorListItem;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
