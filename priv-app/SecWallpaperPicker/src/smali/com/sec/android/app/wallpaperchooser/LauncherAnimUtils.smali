.class public Lcom/sec/android/app/wallpaperchooser/LauncherAnimUtils;
.super Ljava/lang/Object;
.source "LauncherAnimUtils.java"


# static fields
.field static sAnimators:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field static sEndAnimListener:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherAnimUtils;->sAnimators:Ljava/util/HashSet;

    .line 31
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/LauncherAnimUtils$1;

    invoke-direct {v0}, Lcom/sec/android/app/wallpaperchooser/LauncherAnimUtils$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherAnimUtils;->sEndAnimListener:Landroid/animation/Animator$AnimatorListener;

    return-void
.end method

.method public static cancelOnDestroyActivity(Landroid/animation/Animator;)V
    .locals 1
    .param p0, "a"    # Landroid/animation/Animator;

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/wallpaperchooser/LauncherAnimUtils;->sEndAnimListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p0, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 50
    return-void
.end method
