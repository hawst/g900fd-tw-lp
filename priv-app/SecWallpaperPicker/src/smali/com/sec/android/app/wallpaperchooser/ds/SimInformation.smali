.class public Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;
.super Ljava/lang/Object;
.source "SimInformation.java"


# static fields
.field static final isMultiSIM:Z

.field static final isSkipCTC:Z

.field static final isSkipMultiSim:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isSkipCTC:Z

    .line 27
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "trlteduos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isSkipMultiSim:Z

    .line 28
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_USE_MULTISIM"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isSkipCTC:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isSkipMultiSim:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isMultiSIM:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCurrentSimSlot(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 88
    sget-boolean v8, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isMultiSIM:Z

    if-eqz v8, :cond_6

    .line 100
    const-string v0, "ril.MSIMM"

    .line 101
    .local v0, "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    const-string v1, "1"

    .line 103
    .local v1, "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    const-string v8, "ril.MSIMM"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 105
    .local v3, "mSimSlot":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "phone1_on"

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-ne v8, v6, :cond_1

    move v4, v6

    .line 106
    .local v4, "sim1Enabled":Z
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "phone2_on"

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-ne v8, v6, :cond_2

    move v5, v6

    .line 108
    .local v5, "sim2Enabled":Z
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 109
    const-string v8, "1"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v6, :cond_3

    .line 134
    .end local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .end local v1    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .end local v3    # "mSimSlot":Ljava/lang/String;
    .end local v4    # "sim1Enabled":Z
    .end local v5    # "sim2Enabled":Z
    :cond_0
    :goto_2
    return v6

    .restart local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .restart local v1    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .restart local v3    # "mSimSlot":Ljava/lang/String;
    :cond_1
    move v4, v7

    .line 105
    goto :goto_0

    .restart local v4    # "sim1Enabled":Z
    :cond_2
    move v5, v7

    .line 106
    goto :goto_1

    .line 112
    .restart local v5    # "sim2Enabled":Z
    :cond_3
    if-nez v4, :cond_4

    if-nez v5, :cond_0

    :cond_4
    move v6, v7

    .line 115
    goto :goto_2

    .line 119
    :cond_5
    const/4 v2, 0x0

    .line 124
    .local v2, "currentSim":I
    const-string v8, "persist.radio.calldefault.simid"

    invoke-static {v8, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 126
    if-nez v2, :cond_0

    move v6, v7

    .line 127
    goto :goto_2

    .end local v0    # "SP_KEY_CURRENT_SIM_SLOT":Ljava/lang/String;
    .end local v1    # "SP_VALUE_SIM_SLOT_SLOT2":Ljava/lang/String;
    .end local v2    # "currentSim":I
    .end local v3    # "mSimSlot":Ljava/lang/String;
    .end local v4    # "sim1Enabled":Z
    .end local v5    # "sim2Enabled":Z
    :cond_6
    move v6, v7

    .line 134
    goto :goto_2
.end method

.method public static getSimIcon(Landroid/content/Context;I)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simId"    # I

    .prologue
    const/4 v9, 0x1

    .line 210
    sget-boolean v8, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isMultiSIM:Z

    if-eqz v8, :cond_5

    .line 211
    const-string v1, "SELECT_ICON_1"

    .line 212
    .local v1, "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    const-string v2, "SELECT_ICON_2"

    .line 214
    .local v2, "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    const/4 v7, 0x0

    .line 215
    .local v7, "simIconIdx":I
    const v6, 0x7f02002c

    .line 216
    .local v6, "simIcon":I
    const/4 v5, 0x0

    .line 218
    .local v5, "iconNameField":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 219
    const-string v8, "TAG"

    const-string v9, "context is null"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    .end local v5    # "iconNameField":Ljava/lang/String;
    .end local v6    # "simIcon":I
    .end local v7    # "simIconIdx":I
    :goto_0
    return v6

    .line 224
    .restart local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    .restart local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    .restart local v5    # "iconNameField":Ljava/lang/String;
    .restart local v6    # "simIcon":I
    .restart local v7    # "simIconIdx":I
    :cond_0
    :try_start_0
    const-class v3, Landroid/provider/Settings$System;

    .line 226
    .local v3, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-ne p1, v9, :cond_1

    .line 232
    const-string v8, "SELECT_ICON_2"

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    .line 244
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v7

    .line 276
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_2
    packed-switch v7, :pswitch_data_0

    .line 305
    const v6, 0x7f02002c

    goto :goto_0

    .line 240
    .restart local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const-string v8, "SELECT_ICON_1"

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 245
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v4

    .line 246
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    if-ne p1, v9, :cond_2

    .line 247
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal argument of SELECT_ICON_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :goto_3
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 250
    :cond_2
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal argument of SELECT_ICON_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 254
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v4

    .line 255
    .local v4, "e":Ljava/lang/IllegalAccessException;
    if-ne p1, v9, :cond_3

    .line 256
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal access of SELECT_ICON_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :goto_4
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 259
    :cond_3
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "illegal access of SELECT_ICON_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 263
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 264
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    if-ne p1, v9, :cond_4

    .line 265
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "there is no SELECT_ICON_2"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :goto_5
    invoke-virtual {v4}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_2

    .line 267
    :cond_4
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "there is no SELECT_ICON_1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 270
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v4

    .line 271
    .local v4, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v4}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 272
    .end local v4    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_4
    move-exception v4

    .line 273
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 278
    .end local v4    # "e":Ljava/lang/Exception;
    :pswitch_0
    const v6, 0x7f02002c

    .line 279
    goto/16 :goto_0

    .line 281
    :pswitch_1
    const v6, 0x7f02002d

    .line 282
    goto/16 :goto_0

    .line 284
    :pswitch_2
    const v6, 0x7f02002b

    .line 285
    goto/16 :goto_0

    .line 287
    :pswitch_3
    const v6, 0x7f020033

    .line 288
    goto/16 :goto_0

    .line 290
    :pswitch_4
    const v6, 0x7f020032

    .line 291
    goto/16 :goto_0

    .line 293
    :pswitch_5
    const v6, 0x7f020031

    .line 294
    goto/16 :goto_0

    .line 296
    :pswitch_6
    const v6, 0x7f02002f

    .line 297
    goto/16 :goto_0

    .line 299
    :pswitch_7
    const v6, 0x7f020030

    .line 300
    goto/16 :goto_0

    .line 302
    :pswitch_8
    const v6, 0x7f02002e

    .line 303
    goto/16 :goto_0

    .line 311
    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_ICON_2":Ljava/lang/String;
    .end local v5    # "iconNameField":Ljava/lang/String;
    .end local v6    # "simIcon":I
    .end local v7    # "simIconIdx":I
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getSimName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simId"    # I

    .prologue
    const/4 v8, 0x1

    .line 140
    const/4 v5, 0x0

    .line 142
    .local v5, "simName":Ljava/lang/String;
    sget-boolean v7, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isMultiSIM:Z

    if-eqz v7, :cond_1

    .line 143
    const/4 v6, 0x0

    .line 145
    .local v6, "simNameField":Ljava/lang/String;
    const-string v1, "SELECT_NAME_1"

    .line 146
    .local v1, "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    const-string v2, "SELECT_NAME_2"

    .line 148
    .local v2, "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 149
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "context is null"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v7, 0x0

    .line 205
    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .end local v6    # "simNameField":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 154
    .restart local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .restart local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .restart local v6    # "simNameField":Ljava/lang/String;
    :cond_0
    :try_start_0
    const-class v3, Landroid/provider/Settings$System;

    .line 156
    .local v3, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-ne p1, v8, :cond_2

    .line 162
    const-string v7, "SELECT_NAME_2"

    invoke-virtual {v3, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    .line 174
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .end local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .end local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v6    # "simNameField":Ljava/lang/String;
    :cond_1
    :goto_2
    move-object v7, v5

    .line 205
    goto :goto_0

    .line 170
    .restart local v1    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_1":Ljava/lang/String;
    .restart local v2    # "STATIC_FIELD_SETTING_SYSTEM_SELECT_NAME_2":Ljava/lang/String;
    .restart local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v6    # "simNameField":Ljava/lang/String;
    :cond_2
    const-string v7, "SELECT_NAME_1"

    invoke-virtual {v3, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/lang/String;

    move-object v6, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 175
    .end local v3    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v4

    .line 176
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    if-ne p1, v8, :cond_3

    .line 177
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "illegal argument of SELECT_NAME_2"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_3
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 180
    :cond_3
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "illegal argument of SELECT_NAME_1"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 184
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v4

    .line 185
    .local v4, "e":Ljava/lang/IllegalAccessException;
    if-ne p1, v8, :cond_4

    .line 186
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "illegal access of SELECT_NAME_2"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :goto_4
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 189
    :cond_4
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "illegal access of SELECT_NAME_1"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 193
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 194
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    if-ne p1, v8, :cond_5

    .line 195
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "there is no SELECT_NAME_2"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :goto_5
    invoke-virtual {v4}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_2

    .line 197
    :cond_5
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->getTag()Ljava/lang/String;

    move-result-object v7

    const-string v8, "there is no SELECT_NAME_1"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 200
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v4

    .line 201
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static getSimState(Landroid/content/Context;)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 44
    sget-boolean v9, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isMultiSIM:Z

    if-eqz v9, :cond_0

    .line 59
    const-string v0, "ril.ICC_TYPE"

    .line 60
    .local v0, "SP_KEY_SIM_STATE_SIM1":Ljava/lang/String;
    const-string v1, "ril.ICC_TYPE2"

    .line 61
    .local v1, "SP_KEY_SIM_STATE_SIM2":Ljava/lang/String;
    const-string v2, "0"

    .line 63
    .local v2, "SP_VALUE_SIM_STATE_ABSENT":Ljava/lang/String;
    const-string v9, "ril.ICC_TYPE"

    const-string v10, "0"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    .local v4, "sim1State":Ljava/lang/String;
    const-string v9, "ril.ICC_TYPE2"

    const-string v10, "0"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, "sim2State":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "phone1_on"

    invoke-static {v9, v10, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    if-ne v9, v7, :cond_1

    move v3, v7

    .line 67
    .local v3, "sim1Enabled":Z
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "phone2_on"

    invoke-static {v9, v10, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    if-ne v9, v7, :cond_2

    move v5, v7

    .line 69
    .local v5, "sim2Enabled":Z
    :goto_1
    const-string v9, "0"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v3, :cond_3

    const-string v9, "0"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v5, :cond_3

    .line 71
    const/4 v8, 0x2

    .line 82
    .end local v0    # "SP_KEY_SIM_STATE_SIM1":Ljava/lang/String;
    .end local v1    # "SP_KEY_SIM_STATE_SIM2":Ljava/lang/String;
    .end local v2    # "SP_VALUE_SIM_STATE_ABSENT":Ljava/lang/String;
    .end local v3    # "sim1Enabled":Z
    .end local v4    # "sim1State":Ljava/lang/String;
    .end local v5    # "sim2Enabled":Z
    .end local v6    # "sim2State":Ljava/lang/String;
    :cond_0
    :goto_2
    return v8

    .restart local v0    # "SP_KEY_SIM_STATE_SIM1":Ljava/lang/String;
    .restart local v1    # "SP_KEY_SIM_STATE_SIM2":Ljava/lang/String;
    .restart local v2    # "SP_VALUE_SIM_STATE_ABSENT":Ljava/lang/String;
    .restart local v4    # "sim1State":Ljava/lang/String;
    .restart local v6    # "sim2State":Ljava/lang/String;
    :cond_1
    move v3, v8

    .line 66
    goto :goto_0

    .restart local v3    # "sim1Enabled":Z
    :cond_2
    move v5, v8

    .line 67
    goto :goto_1

    .line 73
    .restart local v5    # "sim2Enabled":Z
    :cond_3
    const-string v9, "0"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-ne v9, v7, :cond_4

    const-string v9, "0"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eq v9, v7, :cond_0

    :cond_4
    move v8, v7

    .line 78
    goto :goto_2
.end method

.method private static getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    sget-boolean v1, Lcom/sec/android/app/wallpaperchooser/ds/SimInformation;->isMultiSIM:Z

    if-eqz v1, :cond_0

    .line 35
    const-string v0, "SimInformation"

    .line 36
    .local v0, "TAG":Ljava/lang/String;
    const-string v1, "SimInformation"

    .line 38
    :goto_0
    return-object v1

    .end local v0    # "TAG":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
