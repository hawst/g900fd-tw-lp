.class public interface abstract Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLCanvas;
.super Ljava/lang/Object;
.source "GLCanvas.java"


# virtual methods
.method public abstract drawTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IIII)V
.end method

.method public abstract drawTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
.end method

.method public abstract getGLId()Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/GLId;
.end method

.method public abstract initializeTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;Landroid/graphics/Bitmap;)V
.end method

.method public abstract initializeTextureSize(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;II)V
.end method

.method public abstract restore()V
.end method

.method public abstract rotate(FFFF)V
.end method

.method public abstract save(I)V
.end method

.method public abstract setTextureParameters(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;)V
.end method

.method public abstract texSubImage2D(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V
.end method

.method public abstract translate(FF)V
.end method

.method public abstract unloadTexture(Lcom/sec/android/app/wallpaperchooser/gallery3d/glrenderer/BasicTexture;)Z
.end method
