.class public Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ThirdPartyWallpaperPickerListAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;
    }
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private mThirdPartyWallpaperPickers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 41
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mThirdPartyWallpaperPickers:Ljava/util/List;

    .line 66
    const-string v15, "layout_inflater"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 67
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 68
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 70
    .local v14, "pm":Landroid/content/pm/PackageManager;
    new-instance v13, Landroid/content/Intent;

    const-string v15, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v13, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    .local v13, "pickWallpaperIntent":Landroid/content/Intent;
    const/4 v15, 0x0

    invoke-virtual {v14, v13, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 74
    .local v2, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v12, Landroid/content/Intent;

    const-string v15, "android.intent.action.GET_CONTENT"

    invoke-direct {v12, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    .local v12, "pickImageIntent":Landroid/content/Intent;
    const-string v15, "image/*"

    invoke-virtual {v12, v15}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const/4 v15, 0x0

    invoke-virtual {v14, v12, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    .line 77
    .local v7, "imagePickerActivities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v15

    new-array v6, v15, [Landroid/content/ComponentName;

    .line 78
    .local v6, "imageActivities":[Landroid/content/ComponentName;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v15

    if-ge v3, v15, :cond_0

    .line 79
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/pm/ResolveInfo;

    iget-object v1, v15, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 80
    .local v1, "activityInfo":Landroid/content/pm/ActivityInfo;
    new-instance v15, Landroid/content/ComponentName;

    iget-object v0, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v0, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v6, v3

    .line 78
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 84
    .end local v1    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    .line 85
    .local v9, "info":Landroid/content/pm/ResolveInfo;
    new-instance v10, Landroid/content/ComponentName;

    iget-object v15, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v15, v15, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .local v10, "itemComponentName":Landroid/content/ComponentName;
    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 89
    .local v11, "itemPackageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string v15, "com.android.launcher"

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string v15, "com.android.wallpaper.livepicker"

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string v15, "com.android.launcher3"

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string v15, "com.sec.android.app.wallpaperchooser"

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 97
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 98
    .local v8, "imagePickerActivityInfo":Landroid/content/pm/ResolveInfo;
    iget-object v15, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v15, v15, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    goto :goto_1

    .line 102
    .end local v8    # "imagePickerActivityInfo":Landroid/content/pm/ResolveInfo;
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mThirdPartyWallpaperPickers:Ljava/util/List;

    new-instance v16, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;

    move-object/from16 v0, v16

    invoke-direct {v0, v9}, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;-><init>(Landroid/content/pm/ResolveInfo;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 104
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v9    # "info":Landroid/content/pm/ResolveInfo;
    .end local v10    # "itemComponentName":Landroid/content/ComponentName;
    .end local v11    # "itemPackageName":Ljava/lang/String;
    :cond_4
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mThirdPartyWallpaperPickers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mThirdPartyWallpaperPickers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->getItem(I)Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 115
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 121
    if-nez p2, :cond_1

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f04000b

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .local v4, "view":Landroid/view/View;
    :goto_0
    move-object v5, v4

    .line 127
    check-cast v5, Landroid/widget/FrameLayout;

    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setWallpaperItemPaddingToZero(Landroid/widget/FrameLayout;)V

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mThirdPartyWallpaperPickers:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;

    # getter for: Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;->mResolveInfo:Landroid/content/pm/ResolveInfo;
    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;->access$000(Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter$ThirdPartyWallpaperTile;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 130
    .local v2, "info":Landroid/content/pm/ResolveInfo;
    const v5, 0x7f100025

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 131
    .local v3, "label":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/ThirdPartyWallpaperPickerListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v5}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 133
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    const v5, 0x7f10002b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 134
    .local v1, "iconView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 135
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    :cond_0
    return-object v4

    .line 124
    .end local v0    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "iconView":Landroid/widget/ImageView;
    .end local v2    # "info":Landroid/content/pm/ResolveInfo;
    .end local v3    # "label":Landroid/widget/TextView;
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    move-object v4, p2

    .restart local v4    # "view":Landroid/view/View;
    goto :goto_0
.end method
