.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;
.super Landroid/widget/BaseAdapter;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BuiltInWallpapersAdapter"
.end annotation


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mWallpapers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2476
    .local p2, "wallpapers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2477
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 2478
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    .line 2479
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 2482
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2486
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 2472
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->getItem(I)Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 2490
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2494
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->mThumb:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;->access$4500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$ResourceWallpaperInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2495
    .local v4, "thumb":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_0

    .line 2496
    const-string v0, "Wallpapers.WallpaperPickerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error decoding thumbnail for wallpaper #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2498
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$BuiltInWallpapersAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createImageTileView(Landroid/view/LayoutInflater;ILandroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
