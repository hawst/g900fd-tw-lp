.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0

    .prologue
    .line 851
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 853
    const-string v6, "Wallpapers.WallpaperPickerActivity"

    const-string v7, "mLongClickListener - onLongClick()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 876
    :goto_0
    return v4

    :cond_0
    move-object v0, p1

    .line 857
    check-cast v0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;

    .line 858
    .local v0, "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setCheckBackgroundView(Z)V

    .line 859
    const v6, 0x7f100028

    invoke-virtual {v0, v6}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 860
    .local v1, "checkImageFg":Landroid/widget/ImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->toggle(Landroid/widget/ImageView;)V

    .line 862
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 863
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ActionMode;->invalidate()V

    :cond_1
    move v4, v5

    .line 876
    goto :goto_0

    .line 865
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    iget-object v7, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    iget-object v8, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionModeCallback:Landroid/view/ActionMode$Callback;
    invoke-static {v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode$Callback;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v7

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v6, v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1402(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 866
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 867
    .local v2, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_3

    .line 868
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/View;->setSelected(Z)V

    .line 867
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 871
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 872
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_1

    .line 873
    iget-object v6, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$10;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/View;->setSelected(Z)V

    .line 872
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method
