.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0

    .prologue
    .line 1042
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1088
    :cond_0
    :goto_0
    return-void

    .line 1048
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1049
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mGuideFirstStep:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2100()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1051
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const v5, 0x7f100019

    invoke-virtual {v4, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1052
    .local v1, "helpTapImage":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const v5, 0x7f100018

    invoke-virtual {v4, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1053
    .local v2, "helpTextBox":Landroid/view/View;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 1054
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1055
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1059
    .end local v1    # "helpTapImage":Landroid/view/View;
    .end local v2    # "helpTextBox":Landroid/view/View;
    :cond_2
    sget v4, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    if-nez v4, :cond_3

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3500()Z

    move-result v4

    if-eqz v4, :cond_3

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$500()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1060
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->checkSim()V
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    goto :goto_0

    .line 1062
    :cond_3
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->isMultiSIM:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3500()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1063
    invoke-static {v6}, Lcom/sec/android/app/wallpaperchooser/ds/DsWallpaperSetting;->selectSimByUser(I)V

    .line 1065
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const-string v5, "enterprise_policy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1066
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/enterprise/RestrictionPolicy;->isWallpaperChangeAllowed()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1067
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setResult(I)V

    .line 1068
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->finish()V

    goto :goto_0

    .line 1072
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1073
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1700()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1074
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 1076
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1077
    .local v3, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 1078
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setClickable(Z)V

    .line 1080
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mEnableSurveyMode:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3700()Z

    move-result v4

    if-eqz v4, :cond_0

    sget v4, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1081
    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isCheckable()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1082
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v5, 0x5

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V
    invoke-static {v4, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;I)V

    goto/16 :goto_0

    .line 1084
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$12;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->sendSurveyLog(I)V
    invoke-static {v4, v6}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;I)V

    goto/16 :goto_0
.end method
