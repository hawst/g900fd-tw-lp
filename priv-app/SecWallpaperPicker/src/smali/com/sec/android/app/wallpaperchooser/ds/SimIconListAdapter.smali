.class public Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SimIconListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;",
        ">;"
    }
.end annotation


# static fields
.field static final isMultiSIM:Z

.field static final isSkipCTC:Z


# instance fields
.field protected mInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->isSkipCTC:Z

    .line 38
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_USE_MULTISIM"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->isSkipCTC:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->isMultiSIM:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;>;"
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->getLayout()I

    move-result v0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 42
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->isMultiSIM:Z

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 45
    :cond_0
    return-void
.end method

.method private static getLayout()I
    .locals 1

    .prologue
    .line 48
    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->isMultiSIM:Z

    if-eqz v0, :cond_0

    .line 49
    const v0, 0x7f040001

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 57
    const/4 v3, 0x0

    .line 58
    .local v3, "view":Landroid/view/View;
    sget-boolean v4, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->isMultiSIM:Z

    if-eqz v4, :cond_1

    .line 59
    const/4 v1, 0x0

    .line 60
    .local v1, "isNoIcon":Z
    const/4 v2, 0x0

    .line 61
    .local v2, "text":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 63
    .local v0, "image":Landroid/widget/ImageView;
    if-nez p2, :cond_2

    .line 64
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->getLayout()I

    move-result v5

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 69
    :goto_0
    if-eqz v3, :cond_1

    .line 71
    const v4, 0x7f100002

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "image":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 72
    .restart local v0    # "image":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;->getResource()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 74
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    const/4 v1, 0x1

    .line 82
    :cond_0
    :goto_1
    const v4, 0x7f100003

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 83
    .restart local v2    # "text":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 84
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    if-eqz v1, :cond_1

    .line 86
    const/high16 v4, 0x41100000    # 9.0f

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v2, v4, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 94
    .end local v0    # "image":Landroid/widget/ImageView;
    .end local v1    # "isNoIcon":Z
    .end local v2    # "text":Landroid/widget/TextView;
    :cond_1
    return-object v3

    .line 66
    .restart local v0    # "image":Landroid/widget/ImageView;
    .restart local v1    # "isNoIcon":Z
    .restart local v2    # "text":Landroid/widget/TextView;
    :cond_2
    move-object v3, p2

    goto :goto_0

    .line 77
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/ds/SimIconListAdapter$IconListItem;->getResource()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
