.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Lcom/sec/android/app/wallpaperchooser/CropView$TouchCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 0

    .prologue
    .line 731
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTap()V
    .locals 3

    .prologue
    .line 758
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 769
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Z

    move-result v0

    .line 762
    .local v0, "ignoreTap":Z
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Z)Z

    .line 763
    if-nez v0, :cond_0

    .line 764
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->visibleMenuList()V
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    goto :goto_0

    .line 767
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->changeMenuListVisibility()V
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1300(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    goto :goto_0
.end method

.method public onTouchDown()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/ActionMode;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getWallpaperGuideMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpaperStrip:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Z)Z

    .line 740
    :cond_2
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1700()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->closeMenuList()V
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    goto :goto_0

    .line 743
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->visibleMenuList()V
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Z)Z

    goto :goto_0

    .line 747
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->invisibleMenuList()V
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    goto :goto_0
.end method

.method public onTouchUp()V
    .locals 2

    .prologue
    .line 753
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$7;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mIgnoreNextTap:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1602(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Z)Z

    .line 754
    return-void
.end method
