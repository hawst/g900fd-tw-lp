.class Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;
.super Landroid/os/AsyncTask;
.source "LiveWallpaperListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LiveWallpaperEnumerator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;",
        "Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mWallpaperPosition:I

.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->this$0:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    .line 141
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 142
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mContext:Landroid/content/Context;

    .line 143
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mWallpaperPosition:I

    .line 144
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 135
    check-cast p1, [Ljava/util/List;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->doInBackground([Ljava/util/List;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/util/List;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "params":[Ljava/util/List;, "[Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v9, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 150
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v9, 0x0

    aget-object v4, p1, v9

    .line 152
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v9, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator$1;

    invoke-direct {v9, p0, v5}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator$1;-><init>(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;Landroid/content/pm/PackageManager;)V

    invoke-static {v4, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 161
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 162
    .local v6, "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v2, 0x0

    .line 164
    .local v2, "info":Landroid/app/WallpaperInfo;
    :try_start_0
    new-instance v2, Landroid/app/WallpaperInfo;

    .end local v2    # "info":Landroid/app/WallpaperInfo;
    iget-object v9, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mContext:Landroid/content/Context;

    invoke-direct {v2, v9, v6}, Landroid/app/WallpaperInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 174
    .restart local v2    # "info":Landroid/app/WallpaperInfo;
    invoke-virtual {v2, v5}, Landroid/app/WallpaperInfo;->loadThumbnail(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 175
    .local v7, "thumb":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.service.wallpaper.WallpaperService"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 176
    .local v3, "launchIntent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getServiceName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    new-instance v8, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    invoke-direct {v8, v7, v2, v3}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;-><init>(Landroid/graphics/drawable/Drawable;Landroid/app/WallpaperInfo;Landroid/content/Intent;)V

    .line 178
    .local v8, "wallpaper":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
    const/4 v9, 0x1

    new-array v9, v9, [Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    invoke-virtual {p0, v9}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    .end local v2    # "info":Landroid/app/WallpaperInfo;
    .end local v3    # "launchIntent":Landroid/content/Intent;
    .end local v7    # "thumb":Landroid/graphics/drawable/Drawable;
    .end local v8    # "wallpaper":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v9, "Wallpapaers.LiveWallpaperListAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Skipping wallpaper "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 168
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v0

    .line 169
    .local v0, "e":Ljava/io/IOException;
    const-string v9, "Wallpapaers.LiveWallpaperListAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Skipping wallpaper "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 181
    .end local v0    # "e":Ljava/io/IOException;
    .end local v6    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    const/4 v9, 0x1

    new-array v10, v9, [Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    const/4 v11, 0x0

    const/4 v9, 0x0

    check-cast v9, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    aput-object v9, v10, v11

    invoke-virtual {p0, v10}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->publishProgress([Ljava/lang/Object;)V

    .line 183
    const/4 v9, 0x0

    return-object v9
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;)V
    .locals 6
    .param p1, "infos"    # [Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    .prologue
    .line 188
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 189
    .local v2, "info":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
    if-nez v2, :cond_1

    .line 190
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->this$0:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->notifyDataSetChanged()V

    .line 202
    .end local v2    # "info":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
    :cond_0
    return-void

    .line 195
    .restart local v2    # "info":Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;
    :cond_1
    iget v4, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mWallpaperPosition:I

    iget-object v5, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->this$0:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    # getter for: Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->mWallpapers:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->access$200(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 196
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->this$0:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    # getter for: Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->mWallpapers:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->access$200(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;)Ljava/util/List;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mWallpaperPosition:I

    invoke-interface {v4, v5, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 200
    :goto_1
    iget v4, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mWallpaperPosition:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->mWallpaperPosition:I

    .line 188
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->this$0:Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;

    # getter for: Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->mWallpapers:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;->access$200(Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 135
    check-cast p1, [Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperEnumerator;->onProgressUpdate([Lcom/sec/android/app/wallpaperchooser/LiveWallpaperListAdapter$LiveWallpaperTile;)V

    return-void
.end method
