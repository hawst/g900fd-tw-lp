.class public Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;
.super Landroid/app/Activity;
.source "WallpaperCropActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;,
        Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;
    }
.end annotation


# static fields
.field static final isMultiSIM:Z

.field static final isSkipCTC:Z

.field static final isSkipMultiSim:Z

.field protected static mCurrentMode:I


# instance fields
.field protected mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

.field private mGuideMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 99
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ctc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isSkipCTC:Z

    .line 100
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "trlteduos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isSkipMultiSim:Z

    .line 101
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_USE_MULTISIM"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isSkipCTC:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isSkipMultiSim:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->isMultiSIM:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mGuideMode:Z

    .line 404
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setTransparency(I)V

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkLiveViewLockScreen()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->checkFestivalLockScreen()Z

    move-result v0

    return v0
.end method

.method private static checkFestivalLockScreen()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1141
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1143
    .local v0, "lockType":I
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 1146
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static checkLiveViewLockScreen()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1132
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1134
    .local v0, "lockType":I
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1137
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected static convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;
    .locals 1
    .param p0, "extension"    # Ljava/lang/String;

    .prologue
    .line 1118
    const-string v0, "png"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0
.end method

.method protected static getDefaultWallpaperSize(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;
    .locals 6
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "windowManager"    # Landroid/view/WindowManager;

    .prologue
    .line 190
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 191
    .local v3, "realSize":Landroid/graphics/Point;
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 192
    iget v4, v3, Landroid/graphics/Point;->x:I

    iget v5, v3, Landroid/graphics/Point;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 205
    .local v2, "maxDim":I
    move v1, v2

    .line 206
    .local v1, "defaultWidth":I
    move v0, v2

    .line 208
    .local v0, "defaultHeight":I
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v4
.end method

.method protected static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "requestFormat"    # Ljava/lang/String;

    .prologue
    .line 1122
    if-nez p0, :cond_1

    const-string v0, "jpg"

    .line 1125
    .local v0, "outputFormat":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1126
    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "png"

    :goto_1
    return-object v1

    .end local v0    # "outputFormat":Ljava/lang/String;
    :cond_1
    move-object v0, p0

    .line 1122
    goto :goto_0

    .line 1126
    .restart local v0    # "outputFormat":Ljava/lang/String;
    :cond_2
    const-string v1, "jpg"

    goto :goto_1
.end method

.method protected static getMaxCropRect(IIIIZ)Landroid/graphics/RectF;
    .locals 6
    .param p0, "inWidth"    # I
    .param p1, "inHeight"    # I
    .param p2, "outWidth"    # I
    .param p3, "outHeight"    # I
    .param p4, "leftAligned"    # Z

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1097
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1099
    .local v0, "cropRect":Landroid/graphics/RectF;
    int-to-float v1, p0

    int-to-float v2, p1

    div-float/2addr v1, v2

    int-to-float v2, p2

    int-to-float v3, p3

    div-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 1100
    iput v4, v0, Landroid/graphics/RectF;->top:F

    .line 1101
    int-to-float v1, p1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1102
    int-to-float v1, p0

    int-to-float v2, p2

    int-to-float v3, p3

    div-float/2addr v2, v3

    int-to-float v3, p1

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1103
    int-to-float v1, p0

    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1104
    if-eqz p4, :cond_0

    .line 1105
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1106
    iput v4, v0, Landroid/graphics/RectF;->left:F

    .line 1114
    :cond_0
    :goto_0
    return-object v0

    .line 1109
    :cond_1
    iput v4, v0, Landroid/graphics/RectF;->left:F

    .line 1110
    int-to-float v1, p0

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1111
    int-to-float v1, p1

    int-to-float v2, p3

    int-to-float v3, p2

    div-float/2addr v2, v3

    int-to-float v3, p0

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1112
    int-to-float v1, p1

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public static getRotationFromExif(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 216
    const/4 v0, 0x0

    invoke-static {v1, v1, v0, p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExifHelper(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public static getRotationFromExif(Landroid/content/res/Resources;I)I
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "resId"    # I

    .prologue
    const/4 v0, 0x0

    .line 220
    invoke-static {v0, p0, p1, v0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExifHelper(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public static getRotationFromExif(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 212
    const/4 v0, 0x0

    invoke-static {p0, v1, v0, v1, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExifHelper(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method private static getRotationFromExifHelper(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I
    .locals 7
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 225
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;

    invoke-direct {v2}, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;-><init>()V

    .line 227
    .local v2, "ei":Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;
    if-eqz p0, :cond_1

    .line 228
    :try_start_0
    invoke-virtual {v2, p0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V

    .line 240
    :cond_0
    :goto_0
    sget v5, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    invoke-virtual {v2, v5}, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;->getTagIntValue(I)Ljava/lang/Integer;

    move-result-object v4

    .line 241
    .local v4, "ori":Ljava/lang/Integer;
    if-eqz v4, :cond_2

    .line 242
    invoke-virtual {v4}, Ljava/lang/Integer;->shortValue()S

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;->getRotationForOrientationValue(S)I

    move-result v5

    .line 247
    .end local v4    # "ori":Ljava/lang/Integer;
    :goto_1
    return v5

    .line 229
    :cond_1
    if-eqz p4, :cond_3

    .line 230
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, p4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    .line 231
    .local v3, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 232
    .local v0, "bis":Ljava/io/BufferedInputStream;
    invoke-virtual {v2, v0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;->readExif(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 245
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "Wallpapers.CropActivity"

    const-string v6, "No exif data, rotation = 0"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 234
    :cond_3
    if-eqz p1, :cond_0

    .line 235
    :try_start_1
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 236
    .restart local v3    # "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 237
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    invoke-virtual {v2, v0}, Lcom/sec/android/app/wallpaperchooser/gallery3d/exif/ExifInterface;->readExif(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private setTransparency(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 380
    const-string v0, "Wallpapers.CropActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SYSTEMUI_TRANSPARENCY : Built in wallpaper set currentMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    sget v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    packed-switch v0, :pswitch_data_0

    .line 402
    :goto_0
    return-void

    .line 383
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android.wallpaper.settings_systemui_transparency"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 388
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "lockscreen_wallpaper_transparent"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 393
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android.wallpaper.settings_systemui_transparency"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 395
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "lockscreen_wallpaper_transparent"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setWallpaperGuideMode()V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Guide_Mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mGuideMode:Z

    .line 117
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected cropImageAndSetWallpaper(Landroid/content/res/Resources;IZ)V
    .locals 14
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "finishActivityWhenDone"    # Z

    .prologue
    .line 268
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Landroid/content/res/Resources;I)I

    move-result v6

    .line 269
    .local v6, "rotation":I
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/CropView;->getSourceDimensions()Landroid/graphics/Point;

    move-result-object v12

    .line 270
    .local v12, "inSize":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getDefaultWallpaperSize(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v13

    .line 271
    .local v13, "outSize":Landroid/graphics/Point;
    iget v2, v12, Landroid/graphics/Point;->x:I

    iget v3, v12, Landroid/graphics/Point;->y:I

    iget v4, v13, Landroid/graphics/Point;->x:I

    iget v7, v13, Landroid/graphics/Point;->y:I

    const/4 v8, 0x0

    invoke-static {v2, v3, v4, v7, v8}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getMaxCropRect(IIIIZ)Landroid/graphics/RectF;

    move-result-object v5

    .line 272
    .local v5, "crop":Landroid/graphics/RectF;
    new-instance v11, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$2;

    move/from16 v0, p3

    invoke-direct {v11, p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$2;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;Z)V

    .line 282
    .local v11, "onEndCrop":Ljava/lang/Runnable;
    new-instance v1, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;

    iget v7, v13, Landroid/graphics/Point;->x:I

    iget v8, v13, Landroid/graphics/Point;->y:I

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v2, p0

    move-object v3, p1

    move/from16 v4, p2

    invoke-direct/range {v1 .. v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 283
    .local v1, "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 284
    return-void
.end method

.method protected cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;ZZ)V
    .locals 25
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "onBitmapCroppedHandler"    # Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;
    .param p3, "finishActivityWhenDone"    # Z
    .param p4, "isNoCrop"    # Z

    .prologue
    .line 288
    new-instance v20, Landroid/graphics/Point;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Point;-><init>()V

    .line 289
    .local v20, "minDims":Landroid/graphics/Point;
    new-instance v19, Landroid/graphics/Point;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Point;-><init>()V

    .line 290
    .local v19, "maxDims":Landroid/graphics/Point;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v13

    .line 291
    .local v13, "d":Landroid/view/Display;
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 293
    new-instance v14, Landroid/graphics/Point;

    invoke-direct {v14}, Landroid/graphics/Point;-><init>()V

    .line 294
    .local v14, "displaySize":Landroid/graphics/Point;
    invoke-virtual {v13, v14}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 296
    iget v3, v14, Landroid/graphics/Point;->x:I

    iget v4, v14, Landroid/graphics/Point;->y:I

    if-ge v3, v4, :cond_2

    const/16 v18, 0x1

    .line 298
    .local v18, "isPortrait":Z
    :goto_0
    if-eqz v18, :cond_3

    .line 299
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->getHeight()I

    move-result v21

    .line 305
    .local v21, "portraitHeight":I
    :goto_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_0

    .line 307
    new-instance v22, Landroid/graphics/Point;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Point;-><init>()V

    .line 308
    .local v22, "realSize":Landroid/graphics/Point;
    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 309
    move-object/from16 v0, v22

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v22

    iget v4, v0, Landroid/graphics/Point;->y:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 312
    .end local v22    # "realSize":Landroid/graphics/Point;
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->getCrop()Landroid/graphics/RectF;

    move-result-object v5

    .line 313
    .local v5, "cropRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->getImageRotation()I

    move-result v6

    .line 314
    .local v6, "cropRotation":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float v12, v3, v4

    .line 316
    .local v12, "cropScale":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    invoke-virtual {v3}, Lcom/sec/android/app/wallpaperchooser/CropView;->getSourceDimensions()Landroid/graphics/Point;

    move-result-object v17

    .line 317
    .local v17, "inSize":Landroid/graphics/Point;
    new-instance v23, Landroid/graphics/Matrix;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Matrix;-><init>()V

    .line 318
    .local v23, "rotateMatrix":Landroid/graphics/Matrix;
    int-to-float v3, v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 319
    const/4 v3, 0x2

    new-array v0, v3, [F

    move-object/from16 v24, v0

    const/4 v3, 0x0

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    aput v4, v24, v3

    const/4 v3, 0x1

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    aput v4, v24, v3

    .line 320
    .local v24, "rotatedInSize":[F
    invoke-virtual/range {v23 .. v24}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 321
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, v24, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v24, v3

    .line 322
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget v4, v24, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v24, v3

    .line 339
    if-eqz v18, :cond_4

    .line 340
    iget v3, v5, Landroid/graphics/RectF;->top:F

    move/from16 v0, v21

    int-to-float v4, v0

    div-float/2addr v4, v12

    add-float/2addr v3, v4

    iput v3, v5, Landroid/graphics/RectF;->bottom:F

    .line 350
    :goto_2
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, v12

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 351
    .local v7, "outWidth":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v3, v12

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 353
    .local v8, "outHeight":I
    new-instance v11, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v11, v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$3;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;Z)V

    .line 363
    .local v11, "onEndCrop":Ljava/lang/Runnable;
    new-instance v2, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 364
    .local v2, "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    if-eqz p2, :cond_1

    .line 365
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setOnBitmapCropped(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$OnBitmapCroppedHandler;)V

    .line 367
    :cond_1
    move/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setNoCrop(Z)V

    .line 368
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 369
    return-void

    .line 296
    .end local v2    # "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    .end local v5    # "cropRect":Landroid/graphics/RectF;
    .end local v6    # "cropRotation":I
    .end local v7    # "outWidth":I
    .end local v8    # "outHeight":I
    .end local v11    # "onEndCrop":Ljava/lang/Runnable;
    .end local v12    # "cropScale":F
    .end local v17    # "inSize":Landroid/graphics/Point;
    .end local v18    # "isPortrait":Z
    .end local v21    # "portraitHeight":I
    .end local v23    # "rotateMatrix":Landroid/graphics/Matrix;
    .end local v24    # "rotatedInSize":[F
    :cond_2
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 303
    .restart local v18    # "isPortrait":Z
    :cond_3
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Point;->y:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v21

    .restart local v21    # "portraitHeight":I
    goto/16 :goto_1

    .line 342
    .restart local v5    # "cropRect":Landroid/graphics/RectF;
    .restart local v6    # "cropRotation":I
    .restart local v12    # "cropScale":F
    .restart local v17    # "inSize":Landroid/graphics/Point;
    .restart local v23    # "rotateMatrix":Landroid/graphics/Matrix;
    .restart local v24    # "rotatedInSize":[F
    :cond_4
    move/from16 v0, v21

    int-to-float v3, v0

    div-float/2addr v3, v12

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float v16, v3, v4

    .line 344
    .local v16, "extraPortraitHeight":F
    const/4 v3, 0x1

    aget v3, v24, v3

    iget v4, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    iget v4, v5, Landroid/graphics/RectF;->top:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v16, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v15

    .line 347
    .local v15, "expandHeight":F
    iget v3, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v15

    iput v3, v5, Landroid/graphics/RectF;->top:F

    .line 348
    iget v3, v5, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v15

    iput v3, v5, Landroid/graphics/RectF;->bottom:F

    goto :goto_2
.end method

.method public getWallpaperGuideMode()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mGuideMode:Z

    return v0
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 124
    const v3, 0x7f040002

    invoke-virtual {p0, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setContentView(I)V

    .line 126
    const v3, 0x7f100005

    invoke-virtual {p0, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/wallpaperchooser/CropView;

    iput-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 129
    .local v0, "cropIntent":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 131
    .local v1, "imageUri":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 132
    const-string v3, "Wallpapers.CropActivity"

    const-string v4, "No URI passed in intent, exiting WallpaperCropActivity"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->finish()V

    .line 140
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-static {p0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v2

    .line 138
    .local v2, "rotation":I
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    new-instance v4, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    const/16 v5, 0x400

    invoke-direct {v4, p0, v1, v5, v2}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/Context;Landroid/net/Uri;II)V

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCropView:Lcom/sec/android/app/wallpaperchooser/CropView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setWallpaperGuideMode()V

    .line 107
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->init(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setRequestedOrientation(I)V

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method protected setWallpaper(Ljava/lang/String;Z)V
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "finishActivityWhenDone"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 251
    invoke-static {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Ljava/lang/String;)I

    move-result v4

    .line 252
    .local v4, "rotation":I
    new-instance v0, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;

    move-object v1, p0

    move-object v2, p1

    move v6, v5

    move v8, v5

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 253
    .local v0, "cropTask":Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;
    new-instance v10, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$1;

    invoke-direct {v10, p0, p2}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$1;-><init>(Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;Z)V

    .line 262
    .local v10, "onEndCrop":Ljava/lang/Runnable;
    invoke-virtual {v0, v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setOnEndRunnable(Ljava/lang/Runnable;)V

    .line 263
    invoke-virtual {v0, v7}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->setNoCrop(Z)V

    .line 264
    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity$BitmapCropTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 265
    return-void
.end method
