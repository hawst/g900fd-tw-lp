.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;
.super Landroid/widget/BaseAdapter;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MontblancWallpapersAdapter"
.end annotation


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mWallpapers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 7
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 2803
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2804
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 2806
    :try_start_0
    const-string v4, "com.sec.android.app.montblanc"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/app/Activity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    .line 2807
    .local v1, "montblancContext":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2810
    .local v2, "res":Landroid/content/res/Resources;
    sget v4, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 2811
    const-string v4, "lock_wallpaper"

    const-string v5, "array"

    const-string v6, "com.sec.android.app.montblanc"

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 2815
    .local v3, "wallpaperArrayId":I
    :goto_0
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v4

    const-string v5, "com.sec.android.app.montblanc"

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->addMontblancWallpapers(Landroid/content/res/Resources;Ljava/lang/String;I)Ljava/util/ArrayList;
    invoke-static {v4, v2, v5, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/content/res/Resources;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    .line 2817
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 2818
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4700(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2824
    .end local v1    # "montblancContext":Landroid/content/Context;
    .end local v2    # "res":Landroid/content/res/Resources;
    .end local v3    # "wallpaperArrayId":I
    :goto_1
    return-void

    .line 2813
    .restart local v1    # "montblancContext":Landroid/content/Context;
    .restart local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    const-string v4, "home_wallpaper"

    const-string v5, "array"

    const-string v6, "com.sec.android.app.montblanc"

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .restart local v3    # "wallpaperArrayId":I
    goto :goto_0

    .line 2820
    :cond_1
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mMontblancWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4700(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2821
    .end local v1    # "montblancContext":Landroid/content/Context;
    .end local v2    # "res":Landroid/content/res/Resources;
    .end local v3    # "wallpaperArrayId":I
    :catch_0
    move-exception v0

    .line 2822
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 2827
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2831
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 2799
    invoke-virtual {p0, p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->getItem(I)Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 2835
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2839
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mWallpapers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->mThumb:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;->access$4800(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpaperInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2840
    .local v4, "thumb":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_0

    .line 2841
    const-string v0, "Wallpapers.WallpaperPickerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error decoding thumbnail for wallpaper #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2843
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$MontblancWallpapersAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->createImageTileView(Landroid/view/LayoutInflater;ILandroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
