.class public Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;
.super Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
.source "SavedWallpaperImages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedWallpaperTile"
.end annotation


# instance fields
.field private mDbId:I

.field private mMultipleImage:Z

.field private mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

.field private mThumb:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(ILandroid/graphics/drawable/Drawable;Z)V
    .locals 0
    .param p1, "dbId"    # I
    .param p2, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p3, "multipleImage"    # Z

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;-><init>()V

    .line 60
    iput p1, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mDbId:I

    .line 61
    iput-object p2, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 62
    iput-boolean p3, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mMultipleImage:Z

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mThumb:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public isCheckable()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public isMultipleImage()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mMultipleImage:Z

    return v0
.end method

.method public isNamelessWallpaper()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->isMultipleImage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onClick(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 7
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSavedImages()Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mDbId:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getImageFilename(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "imageFilename":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 69
    .local v0, "file":Ljava/io/File;
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getCropView()Lcom/sec/android/app/wallpaperchooser/CropView;

    move-result-object v3

    .line 70
    .local v3, "v":Lcom/sec/android/app/wallpaperchooser/CropView;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->getRotationFromExif(Ljava/lang/String;)I

    move-result v2

    .line 72
    .local v2, "rotation":I
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    if-nez v4, :cond_0

    .line 73
    new-instance v4, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x400

    invoke-direct {v4, p1, v5, v6, v2}, Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;-><init>(Landroid/content/Context;Ljava/lang/String;II)V

    iput-object v4, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    .line 74
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mSource:Lcom/sec/android/app/wallpaperchooser/photos/BitmapRegionTileSource;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTileSource(Lcom/sec/android/app/wallpaperchooser/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 75
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/wallpaperchooser/CropView;->setTouchEnabled(Z)V

    .line 76
    return-void
.end method

.method public onDelete(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 2
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSavedImages()Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mDbId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->deleteImage(I)V

    .line 88
    return-void
.end method

.method public onSave(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 4
    .param p1, "a"    # Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    .prologue
    .line 80
    const/4 v0, 0x1

    .line 81
    .local v0, "finishActivityWhenDone":Z
    invoke-virtual {p1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getSavedImages()Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages$SavedWallpaperTile;->mDbId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/wallpaperchooser/SavedWallpaperImages;->getImageFilename(I)Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "imageFilename":Ljava/lang/String;
    invoke-virtual {p1, v1, v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setWallpaper(Ljava/lang/String;Z)V

    .line 83
    return-void
.end method
