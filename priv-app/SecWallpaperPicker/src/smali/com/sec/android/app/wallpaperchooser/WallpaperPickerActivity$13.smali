.class Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;
.super Ljava/lang/Object;
.source "WallpaperPickerActivity.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field actionModeView:Landroid/view/View;

.field context:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V
    .locals 3

    .prologue
    .line 1092
    iput-object p1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1093
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->instance()Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->context:Landroid/content/Context;

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->actionModeView:Landroid/view/View;

    return-void
.end method

.method private numCheckedItems()I
    .locals 5

    .prologue
    .line 1121
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 1122
    .local v1, "childCount":I
    const/4 v3, 0x0

    .line 1123
    .local v3, "numCheckedItems":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1124
    iget-object v4, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;

    .line 1125
    .local v0, "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1126
    add-int/lit8 v3, v3, 0x1

    .line 1123
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1129
    .end local v0    # "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    :cond_1
    return v3
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1151
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    .line 1152
    .local v6, "itemId":I
    const v12, 0x7f100032

    if-ne v6, v12, :cond_8

    .line 1153
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 1154
    .local v1, "childCount":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1155
    .local v9, "viewsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v2, 0x0

    .line 1156
    .local v2, "currentPosition":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 1157
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;

    .line 1158
    .local v0, "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1159
    invoke-virtual {v0}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1160
    .local v5, "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v5, v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->onDelete(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 1161
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1162
    move v2, v3

    .line 1156
    .end local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1166
    .end local v0    # "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    :cond_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 1167
    .local v8, "v":Landroid/view/View;
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 1169
    .end local v8    # "v":Landroid/view/View;
    :cond_2
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # invokes: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->updateTileIndices()V
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)V

    .line 1171
    const/4 v7, 0x0

    .line 1172
    .local v7, "thumbnail":Landroid/widget/FrameLayout;
    add-int/lit8 v2, v2, 0x1

    .line 1173
    if-ge v2, v1, :cond_7

    .line 1174
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v12

    sub-int/2addr v2, v12

    .line 1175
    if-gez v2, :cond_3

    .line 1176
    const/4 v2, 0x0

    .line 1178
    :cond_3
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "thumbnail":Landroid/widget/FrameLayout;
    check-cast v7, Landroid/widget/FrameLayout;

    .line 1179
    .restart local v7    # "thumbnail":Landroid/widget/FrameLayout;
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1181
    .restart local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    invoke-virtual {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isMultipleImage()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1182
    const/4 v7, 0x0

    .line 1183
    :goto_2
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v2, v12, :cond_4

    .line 1184
    add-int/lit8 v2, v2, 0x1

    .line 1185
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "thumbnail":Landroid/widget/FrameLayout;
    check-cast v7, Landroid/widget/FrameLayout;

    .line 1186
    .restart local v7    # "thumbnail":Landroid/widget/FrameLayout;
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    check-cast v5, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;

    .line 1187
    .restart local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    invoke-virtual {v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;->isMultipleImage()Z

    move-result v12

    if-nez v12, :cond_6

    .line 1194
    :cond_4
    if-nez v7, :cond_5

    .line 1195
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "thumbnail":Landroid/widget/FrameLayout;
    check-cast v7, Landroid/widget/FrameLayout;

    .line 1199
    .end local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .restart local v7    # "thumbnail":Landroid/widget/FrameLayout;
    :cond_5
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v11, v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1402(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1200
    iget-object v11, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mThumbnailOnClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v11}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-interface {v11, v7}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1202
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 1203
    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mUpdateWallpaperListOnDelete:Z
    invoke-static {v10}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4302(Z)Z

    .line 1206
    .end local v1    # "childCount":I
    .end local v2    # "currentPosition":I
    .end local v3    # "i":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "thumbnail":Landroid/widget/FrameLayout;
    .end local v9    # "viewsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :goto_4
    return v10

    .line 1190
    .restart local v1    # "childCount":I
    .restart local v2    # "currentPosition":I
    .restart local v3    # "i":I
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    .restart local v7    # "thumbnail":Landroid/widget/FrameLayout;
    .restart local v9    # "viewsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_6
    const/4 v7, 0x0

    goto :goto_2

    .line 1197
    .end local v5    # "info":Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$WallpaperTileInfo;
    :cond_7
    iget-object v12, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mWallpapersView:Landroid/widget/LinearLayout;
    invoke-static {v12}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v12

    invoke-virtual {v12, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "thumbnail":Landroid/widget/FrameLayout;
    check-cast v7, Landroid/widget/FrameLayout;

    .restart local v7    # "thumbnail":Landroid/widget/FrameLayout;
    goto :goto_3

    .end local v1    # "childCount":I
    .end local v2    # "currentPosition":I
    .end local v3    # "i":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "thumbnail":Landroid/widget/FrameLayout;
    .end local v9    # "viewsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_8
    move v10, v11

    .line 1206
    goto :goto_4
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1098
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1099
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_2:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1100
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_3:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1101
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1102
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1103
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSviewCover:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4000()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1104
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1105
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1106
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_5:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1200(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1112
    :cond_0
    :goto_0
    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSpanMenuList:Z
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1702(Z)Z

    .line 1113
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2700()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1115
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1116
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0f0000

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1117
    const/4 v1, 0x1

    return v1

    .line 1108
    .end local v0    # "inflater":Landroid/view/MenuInflater;
    :cond_1
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mDownloadWallpaper:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1109
    iget-object v1, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_4:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1100(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 6
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v5, 0x0

    .line 1211
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mTextView_1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1212
    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mKnoxMode:Z
    invoke-static {}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$500()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mShowDropdownImage:Z
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$4400(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1213
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnDropDown:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3900(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1214
    :cond_0
    sget v3, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    sget v3, Lcom/sec/android/app/wallpaperchooser/WallpaperCropActivity;->mCurrentMode:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    .line 1215
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mBtnSetWallpaper:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2500(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 1217
    :cond_1
    const/4 v1, 0x0

    .line 1218
    .local v1, "childCount":I
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1219
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 1221
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 1222
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSavedImageView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$3000(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;

    .line 1223
    .local v0, "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;->setChecked(Z)V

    .line 1221
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1226
    .end local v0    # "c":Lcom/sec/android/app/wallpaperchooser/CheckableFrameLayout;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1227
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    # getter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mSelectedThumb:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$2600(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1228
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->setCheckBackgroundView(Z)V

    .line 1229
    iget-object v3, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v3, v4}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1402(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1230
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 8
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    .line 1136
    invoke-direct {p0}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->numCheckedItems()I

    move-result v0

    .line 1137
    .local v0, "numCheckedItems":I
    if-nez v0, :cond_0

    .line 1138
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->access$1402(Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1139
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 1145
    :goto_0
    return v7

    .line 1142
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->actionModeView:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 1143
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    const v3, 0x7f100001

    invoke-virtual {v2, v3}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1144
    .local v1, "title":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity$13;->this$0:Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/wallpaperchooser/WallpaperPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0d0000

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
