.class public interface abstract Lcom/samsung/android/aidl/ICheckAppInstallState;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract cancelInstall(Ljava/lang/String;Lcom/samsung/android/aidl/ICancelInstallCallback;)V
.end method

.method public abstract checkInstalledWGTVersion(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getInstalledResult(Ljava/lang/String;)I
.end method

.method public abstract getInstalledWGTPackageInfo()Ljava/util/List;
.end method

.method public abstract getInstallingState(Ljava/lang/String;)I
.end method

.method public abstract getWearableInfo(I)Ljava/lang/String;
.end method

.method public abstract installViaPackageName(Ljava/lang/String;Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;)V
.end method

.method public abstract installWGT(Ljava/lang/String;Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;)V
.end method

.method public abstract installWGTinAPK(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;)V
.end method

.method public abstract installWGTinAPKFromGearStore(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;)V
.end method

.method public abstract prepareInstall(Ljava/lang/String;)V
.end method
