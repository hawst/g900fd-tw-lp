.class public abstract Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;
.super Landroid/os/Binder;
.source "ProGuard"

# interfaces
.implements Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;
    .locals 2

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v0, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/knox/containeragent/service/containermanager/a;

    invoke-direct {v0, p0}, Lcom/sec/knox/containeragent/service/containermanager/a;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 94
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 42
    :sswitch_0
    const-string v0, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v2, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/containeragent/IContainerManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/IContainerManagerCallback;

    move-result-object v3

    .line 52
    invoke-virtual {p0, v2, v3}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;->installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z

    move-result v2

    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 54
    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 59
    :sswitch_2
    const-string v2, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/containeragent/IContainerManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/IContainerManagerCallback;

    move-result-object v3

    .line 64
    invoke-virtual {p0, v2, v3}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;->uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z

    move-result v2

    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    if-eqz v2, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 71
    :sswitch_3
    const-string v2, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;->isContainerAvailable()Z

    move-result v2

    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    :sswitch_4
    const-string v2, "com.sec.knox.containeragent.service.containermanager.IContainerManagerService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual {p0, v2}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    if-eqz v2, :cond_3

    .line 85
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    invoke-virtual {v2, p3, v1}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 89
    :cond_3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
