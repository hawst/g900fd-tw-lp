.class public interface abstract Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
.end method

.method public abstract isContainerAvailable()Z
.end method

.method public abstract uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
.end method
