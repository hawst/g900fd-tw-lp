.class final Lcom/sec/knox/containeragent/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/knox/containeragent/ContainerManager;


# direct methods
.method constructor <init>(Lcom/sec/knox/containeragent/ContainerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/knox/containeragent/a;->a:Lcom/sec/knox/containeragent/ContainerManager;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/knox/containeragent/ContainerManager;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceConnected() name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/containeragent/a;->a:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-static {v2}, Lcom/sec/knox/containeragent/ContainerManager;->a(Lcom/sec/knox/containeragent/ContainerManager;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    iget-object v0, p0, Lcom/sec/knox/containeragent/a;->a:Lcom/sec/knox/containeragent/ContainerManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/knox/containeragent/ContainerManager;->a(Lcom/sec/knox/containeragent/ContainerManager;Z)V

    .line 38
    iget-object v0, p0, Lcom/sec/knox/containeragent/a;->a:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-static {p2}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/containeragent/ContainerManager;->a(Lcom/sec/knox/containeragent/ContainerManager;Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;)V

    .line 39
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/knox/containeragent/ContainerManager;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceDisconnected() name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/sec/knox/containeragent/a;->a:Lcom/sec/knox/containeragent/ContainerManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/knox/containeragent/ContainerManager;->a(Lcom/sec/knox/containeragent/ContainerManager;Z)V

    .line 45
    return-void
.end method
