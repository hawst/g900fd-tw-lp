.class public Lcom/sec/knox/containeragent/ContainerManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

.field private c:Landroid/content/Context;

.field private d:Landroid/content/ServiceConnection;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "ContainerManager"

    sput-object v0, Lcom/sec/knox/containeragent/ContainerManager;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    .line 24
    iput-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->c:Landroid/content/Context;

    .line 25
    iput-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->e:Z

    .line 32
    iput-object p1, p0, Lcom/sec/knox/containeragent/ContainerManager;->c:Landroid/content/Context;

    .line 33
    new-instance v0, Lcom/sec/knox/containeragent/a;

    invoke-direct {v0, p0}, Lcom/sec/knox/containeragent/a;-><init>(Lcom/sec/knox/containeragent/ContainerManager;)V

    iput-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    .line 48
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.containeragent.service.containermanager.ContainerManagerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/sec/knox/containeragent/ContainerManager;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/knox/containeragent/ContainerManager;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/knox/containeragent/ContainerManager;Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    return-void
.end method

.method static synthetic a(Lcom/sec/knox/containeragent/ContainerManager;Z)V
    .locals 0

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/knox/containeragent/ContainerManager;->e:Z

    return-void
.end method


# virtual methods
.method public getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    if-eqz v1, :cond_0

    .line 138
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    invoke-interface {v1, p1}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 143
    :cond_0
    :goto_0
    return-object v0

    .line 140
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 79
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 81
    sget-object v1, Lcom/sec/knox/containeragent/ContainerManager;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " does not exist() and return false"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    if-eqz v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    invoke-interface {v0, p1, p2}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;->installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public isContainerAvailable()Z
    .locals 2

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    invoke-interface {v1}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;->isContainerAvailable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 125
    :cond_0
    :goto_0
    return v0

    .line 123
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public unbindContainerManager()V
    .locals 3

    .prologue
    .line 64
    sget-object v0, Lcom/sec/knox/containeragent/ContainerManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unbindContainerManager() mServiceBound="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/knox/containeragent/ContainerManager;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-boolean v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerManager;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 68
    :cond_0
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
    .locals 1

    .prologue
    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerManager;->b:Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;

    invoke-interface {v0, p1, p2}, Lcom/sec/knox/containeragent/service/containermanager/IContainerManagerService;->uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 107
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
