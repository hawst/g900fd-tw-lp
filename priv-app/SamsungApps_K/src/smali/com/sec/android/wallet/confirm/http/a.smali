.class final Lcom/sec/android/wallet/confirm/http/a;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

.field private final synthetic b:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;


# direct methods
.method constructor <init>(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;Landroid/os/Looper;Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    iput-object p3, p0, Lcom/sec/android/wallet/confirm/http/a;->b:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;

    .line 80
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/a;->b:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v1}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v2}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v3}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v4}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v5}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v6}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/a;->a:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    invoke-static {v7}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->saveCacheResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/a;->b:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;->onResponseCheckWallet(I)V

    .line 90
    :cond_0
    return-void
.end method
