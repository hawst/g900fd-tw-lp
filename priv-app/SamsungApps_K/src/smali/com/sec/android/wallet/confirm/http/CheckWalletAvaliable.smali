.class public Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private final h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

.field private i:Z

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private l:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    .line 22
    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->i:Z

    .line 23
    const-string v0, "https://api.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->j:Ljava/lang/String;

    .line 24
    const-string v0, "http://api.stg.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->k:Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    .line 30
    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    .line 31
    new-instance v0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    .line 32
    const-string v0, "PRD"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    .line 33
    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->i:Z

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    .line 22
    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->i:Z

    .line 23
    const-string v0, "https://api.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->j:Ljava/lang/String;

    .line 24
    const-string v0, "http://api.stg.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->k:Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    .line 38
    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    .line 41
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "STG"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "STG"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->i:Z

    .line 49
    :goto_0
    return-void

    .line 46
    :cond_0
    const-string v0, "PRD"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->i:Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Lcom/sec/android/wallet/confirm/data/WalletStateCache;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x3

    .line 110
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 112
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 113
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    .line 114
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    .line 115
    const-string v1, "06"

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    .line 117
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v3, :cond_0

    .line 118
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    .line 119
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    const-string v1, "450"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "kor"

    :goto_0
    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    .line 123
    return-void

    .line 122
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 9

    .prologue
    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "http://api.stg.wallet.samsung.com/cmn/isAvailable?"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "moduleId="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "&mcc="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&mnc="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&csc="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&deviceId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&openApi="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "https://api.wallet.samsung.com/cmn/isAvailable?"

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public canLinkWallet()I
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 53
    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->checkCachedResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 55
    if-ne v0, v8, :cond_0

    move v0, v8

    .line 75
    :goto_0
    return v0

    .line 57
    :cond_0
    if-nez v0, :cond_1

    move v0, v9

    .line 58
    goto :goto_0

    .line 60
    :cond_1
    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v10

    .line 61
    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setInitControlDatas()V

    .line 63
    new-instance v0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;

    invoke-direct {v0}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;-><init>()V

    .line 64
    new-array v1, v8, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 66
    :goto_1
    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getPolingState()Z

    move-result v0

    if-nez v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->saveCacheResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 75
    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v0

    goto :goto_0

    .line 68
    :cond_2
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public canLinkWallet(Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 80
    new-instance v8, Lcom/sec/android/wallet/confirm/http/a;

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v8, p0, v0, p1}, Lcom/sec/android/wallet/confirm/http/a;-><init>(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;Landroid/os/Looper;Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a()V

    .line 95
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->h:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->g:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->checkCachedResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 96
    if-ne v0, v9, :cond_0

    .line 97
    invoke-interface {p1, v9}, Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;->onResponseCheckWallet(I)V

    .line 106
    :goto_0
    return-void

    .line 98
    :cond_0
    if-nez v0, :cond_1

    .line 99
    invoke-interface {p1, v10}, Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;->onResponseCheckWallet(I)V

    goto :goto_0

    .line 101
    :cond_1
    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setInitControlDatas()V

    .line 103
    new-instance v0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;

    invoke-direct {v0, v8}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;-><init>(Landroid/os/Handler;)V

    .line 104
    new-array v1, v9, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
