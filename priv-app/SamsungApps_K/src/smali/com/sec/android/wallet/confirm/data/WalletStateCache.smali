.class public Lcom/sec/android/wallet/confirm/data/WalletStateCache;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final INVALID_CACHED_WALLET_STATE:I = -0x1


# instance fields
.field private final a:J

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->a:J

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    .line 18
    iput-object p1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    .line 19
    return-void
.end method


# virtual methods
.method public checkCachedResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 22
    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/data/Preference;->getUseWalletState(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getCachedTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 25
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 28
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 29
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 31
    sub-long v4, v6, v4

    const-wide/32 v6, 0x240c8400

    cmp-long v2, v4, v6

    if-gtz v2, :cond_0

    .line 35
    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getModuleId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getMcc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getMnc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getCsc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getOpenApi(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/sec/android/wallet/confirm/data/Preference;->getTargetUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    if-eqz v2, :cond_0

    .line 38
    if-eq v1, v3, :cond_2

    .line 39
    if-nez v1, :cond_0

    :cond_2
    move v0, v1

    .line 40
    goto :goto_0

    .line 35
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public saveCacheResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 61
    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-static {p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/wallet/confirm/data/Preference;->setUseWalletState(Landroid/content/Context;Ljava/lang/String;)Z

    .line 63
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/wallet/confirm/data/Preference;->setModuleId(Landroid/content/Context;Ljava/lang/String;)Z

    .line 64
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/wallet/confirm/data/Preference;->setMcc(Landroid/content/Context;Ljava/lang/String;)Z

    .line 65
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/wallet/confirm/data/Preference;->setMnc(Landroid/content/Context;Ljava/lang/String;)Z

    .line 66
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/wallet/confirm/data/Preference;->setCsc(Landroid/content/Context;Ljava/lang/String;)Z

    .line 67
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p5}, Lcom/sec/android/wallet/confirm/data/Preference;->setDeviceId(Landroid/content/Context;Ljava/lang/String;)Z

    .line 68
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p6}, Lcom/sec/android/wallet/confirm/data/Preference;->setOpenApi(Landroid/content/Context;Ljava/lang/String;)Z

    .line 69
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p7}, Lcom/sec/android/wallet/confirm/data/Preference;->setTargetUrl(Landroid/content/Context;Ljava/lang/String;)Z

    .line 70
    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->b:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/wallet/confirm/data/Preference;->setCachedTime(Landroid/content/Context;Ljava/lang/String;)Z

    .line 71
    return-void
.end method
