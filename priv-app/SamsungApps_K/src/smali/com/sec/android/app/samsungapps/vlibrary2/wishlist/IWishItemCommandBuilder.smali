.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract addToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getButtonState()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract goToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract hasOrderID()Z
.end method

.method public abstract isAddedItem()Z
.end method

.method public abstract isInstalledItem()Z
.end method

.method public abstract isLogedIn()Z
.end method

.method public abstract isPurchaseDetailItem()Z
.end method

.method public abstract removeWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract setAddedWishItem()V
.end method
