.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 9
    const-string v2, "download"

    const-string v3, "3040"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, "orderID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->getOrderID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 15
    :cond_0
    const-string v0, "orderItemSEQ"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    :goto_0
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v0, "trialClsf"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 25
    return-void

    .line 19
    :cond_1
    const-string v0, "orderItemSEQ"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadRequestParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 28
    const-string v2, "download"

    const-string v3, "3040"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 29
    const-string v0, "orderID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadRequestParam;->getOrderID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadRequestParam;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadRequestParam;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 32
    :cond_0
    const-string v0, "orderItemSEQ"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    :goto_0
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadRequestParam;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v0, "trialClsf"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 42
    return-void

    .line 36
    :cond_1
    const-string v0, "orderItemSEQ"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadRequestParam;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DownloadRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
