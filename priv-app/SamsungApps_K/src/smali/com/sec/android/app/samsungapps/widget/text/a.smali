.class final Lcom/sec/android/app/samsungapps/widget/text/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckedTextView;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

.field final synthetic c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Landroid/widget/CheckedTextView;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->a:Landroid/widget/CheckedTextView;

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f080193

    const/4 v5, 0x1

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->getSetting()I

    move-result v0

    if-nez v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801aa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v1

    if-nez v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 314
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;)V

    goto :goto_0

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->c(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->setUpdateEnable()Z

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0800ff

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v1

    if-nez v1, :cond_3

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->d(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto :goto_0

    .line 310
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/a;->c:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->d(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    goto :goto_0
.end method
