.class final Lcom/sec/android/app/samsungapps/widget/list/ak;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 108
    if-eqz p1, :cond_3

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->c(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setWidgetState(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->e(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801cf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 126
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->onWidgetViewState()V

    goto :goto_0

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setWidgetState(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Z

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;[Z)[Z

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->updateWidget()V

    goto :goto_1

    .line 124
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ak;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setWidgetState(I)V

    goto :goto_1
.end method
