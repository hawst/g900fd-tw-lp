.class public Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 15
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;

    .line 22
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 67
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 81
    :goto_0
    :pswitch_0
    return-void

    .line 70
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->SHOW_ASK_DELETE_CARD_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 73
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 74
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->REQUEST_DELETE_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->SEND_SIG_CARD_DELETED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 78
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)Z
    .locals 2

    .prologue
    .line 27
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 58
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 30
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 33
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->DELETE_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 44
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->ASK_INVALID_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->CARD_DELETED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 55
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->ASK_INVALID_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_6
    .end packed-switch

    .line 30
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 41
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 52
    :pswitch_data_3
    .packed-switch 0x5
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 85
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 91
    :goto_0
    :pswitch_0
    return-void

    .line 90
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
