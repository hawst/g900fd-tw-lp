.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IAutoLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->_IAutoLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;

    .line 15
    return-void
.end method

.method private checkAutoLogin(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isAutoLoginCondition(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->_IAutoLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;->getNormalLoginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method private isAccountDisabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 56
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isDisabledSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private isAccountInstalled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 51
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private isAutoLoginCondition(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isAccountInstalled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isNewAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isAccountDisabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isSAUserAccountRegistered(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isNewAccount(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 46
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private isSAUserAccountRegistered(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 61
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private isTokenBasedSamsungAccountInstalled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isAccountInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isNewAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->onFinalResult(Z)V

    .line 33
    :goto_0
    return-void

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->_IAutoLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;->setAutoLogin(Z)V

    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->isTokenBasedSamsungAccountInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->checkAutoLogin(Landroid/content/Context;)V

    goto :goto_0

    .line 31
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method protected onFinalResult(Z)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;->_IAutoLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;->setAutoLogin(Z)V

    .line 38
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onFinalResult(Z)V

    .line 39
    return-void
.end method
