.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _CardSortArray:Ljava/util/ArrayList;

.field private _CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CardSortArray:Ljava/util/ArrayList;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    if-eqz v0, :cond_0

    .line 30
    const-string v0, "cardCompany"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    const-string v0, "cardCompanyCode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompanyCode:Ljava/lang/String;

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CardSortArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 45
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CardSortArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    .line 24
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CardSortArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CurCardSort:Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    .line 15
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->_CardSortArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
