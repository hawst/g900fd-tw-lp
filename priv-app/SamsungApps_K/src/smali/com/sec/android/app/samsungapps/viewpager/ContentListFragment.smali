.class public Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

.field b:Ljava/lang/Boolean;

.field private c:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

.field protected mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field protected mContext:Landroid/content/Context;

.field protected mIsFromFeatured:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;-><init>()V

    .line 24
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->b:Ljava/lang/Boolean;

    .line 25
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mIsFromFeatured:Z

    .line 26
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_ALL:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->c:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    return-void
.end method


# virtual methods
.method protected getContentArrayAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreate(Landroid/os/Bundle;)V

    .line 57
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->release()V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 68
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    .line 69
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onDestroy()V

    .line 70
    return-void
.end method

.method protected setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->c:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    .line 30
    return-void
.end method

.method public setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const v1, 0x7f0c0186

    const v2, 0x7f0c0078

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v3, 0x7f0c0078

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    if-nez v3, :cond_2

    new-instance v3, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->c:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-direct {v3, v4, v0, v5}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const v1, 0x7f0c016a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setMoreView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->isMoreLoadingViewShown()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->showMoreLoadingView(Z)V

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    new-instance v2, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    new-instance v3, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    new-instance v4, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0c0083

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v6, 0x7f0c007e

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v6, 0x7f0c0080

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0c007f

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0c0084

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0c0086

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    new-instance v5, Lcom/sec/android/app/samsungapps/viewpager/l;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/viewpager/l;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setLoadingEmptyView()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setArrayAdapter(Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V

    .line 47
    :cond_1
    return-void

    .line 46
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setListView(Landroid/widget/AbsListView;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->b:Ljava/lang/Boolean;

    move-object v2, v3

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    :goto_1
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_3

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    if-nez v1, :cond_5

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->c:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    move-object v2, v3

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setListView(Landroid/widget/AbsListView;)V

    goto/16 :goto_0
.end method

.method public setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->b:Ljava/lang/Boolean;

    .line 39
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 40
    return-void
.end method

.method public setResizeInMultiWindow()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public setVisibleLoading(I)Z
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 160
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 163
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 164
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 165
    const/4 v0, 0x0

    return v0
.end method
