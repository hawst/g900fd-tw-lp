.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

.field _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

.field private _SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

.field handler:Landroid/os/Handler;

.field private mRequestTokenForServiceCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    .line 272
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->handler:Landroid/os/Handler;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->mRequestTokenForServiceCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method private requestGetAccessToken(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/t;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/t;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;->start(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;)V

    .line 81
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getMyPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 84
    if-nez v0, :cond_1

    .line 86
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->unregisterReceiver()V

    .line 89
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFinalResult(Z)V

    .line 122
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 96
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    const-string v2, "client_id"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v2, "client_secret"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v2, "mypackage"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string v0, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->isAutoLogin()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->isHideNotificationSamsungAccountInstalled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 102
    :cond_2
    const-string v0, "MODE"

    const-string v2, "SHOW_NOTIFICATION_WITH_POPUP"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    :goto_1
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "server_url"

    aput-object v2, v0, v4

    const/4 v2, 0x1

    const-string v3, "api_server_url"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "auth_server_url"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "cc"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "user_id"

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "birthday"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-string v3, "email_id"

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "mcc"

    aput-object v3, v0, v2

    .line 109
    const-string v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    if-eqz p1, :cond_3

    .line 112
    const-string v0, "expired_access_token"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    :cond_3
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 116
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    const-string v0, "SAR"

    const-string v3, "SB"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 121
    const-string v0, "SAR"

    const-string v1, "SC"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 105
    :cond_4
    const-string v0, "MODE"

    const-string v2, "HIDE_NOTIFICATION_WITH_RESULT"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private requestGetAccessTokenByNewInterface()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->getNewInterfaceViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 130
    :cond_0
    return-void
.end method


# virtual methods
.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->createCancellableLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    move-result-object v0

    return-object v0
.end method

.method getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 263
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 265
    array-length v1, v0

    if-lez v1, :cond_0

    .line 266
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 268
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getExpiredToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->isTokenExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->isNewInterfaceSamsungAccountInstalled()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->requestGetAccessTokenByNewInterface()V

    .line 65
    :goto_0
    return-void

    .line 45
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->mRequestTokenForServiceCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->mRequestTokenForServiceCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->getExpiredToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->requestGetAccessToken(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFailGetAccessToken()V
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFinalResult(Z)V

    .line 170
    return-void
.end method

.method public onFailGetAccessToken(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 160
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->isAutoLogin()Z

    .line 164
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFinalResult(Z)V

    .line 165
    return-void
.end method

.method public onGetAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 143
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->setTokenResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFinalResult(Z)V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method public onResultToGetToken(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)V
    .locals 2

    .prologue
    .line 331
    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/v;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/v;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 340
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    :goto_0
    return-void

    .line 341
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method unregisterReceiver()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    if-eqz v0, :cond_0

    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_SamsungAccountBroadcastReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/account/w;

    .line 326
    :cond_0
    return-void

    .line 320
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
