.class final Lcom/sec/android/app/samsungapps/downloadableapps/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hideButton()V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    const/4 v1, 0x0

    const v2, 0x7f0c0171

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->showDownloadAll(ZI)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Z)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    sget-object v1, Lcom/sec/android/app/samsungapps/downloadableapps/i;->c:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Lcom/sec/android/app/samsungapps/downloadableapps/i;)Lcom/sec/android/app/samsungapps/downloadableapps/i;

    .line 172
    return-void
.end method

.method public final showCancelAllButton()V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    const/4 v1, 0x1

    const v2, 0x7f0c0171

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->showDownloadAll(ZI)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    sget-object v1, Lcom/sec/android/app/samsungapps/downloadableapps/i;->b:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Lcom/sec/android/app/samsungapps/downloadableapps/i;)Lcom/sec/android/app/samsungapps/downloadableapps/i;

    .line 165
    return-void
.end method

.method public final showInstallAllButton()V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    const/4 v1, 0x1

    const v2, 0x7f0c0171

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->showDownloadAll(ZI)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/c;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    sget-object v1, Lcom/sec/android/app/samsungapps/downloadableapps/i;->a:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Lcom/sec/android/app/samsungapps/downloadableapps/i;)Lcom/sec/android/app/samsungapps/downloadableapps/i;

    .line 157
    return-void
.end method
