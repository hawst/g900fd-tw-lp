.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/ICommentListCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;


# instance fields
.field private _CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private _LastAppendedList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private _ProductID:Ljava/lang/String;

.field private _ProhibitWords:[Ljava/lang/String;

.field private _Type:I

.field private _bTablet:Z

.field private _iRatingValue:I

.field private mIsMyReviewDuplicated:Z

.field private mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_Type:I

    .line 29
    iput-boolean p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_bTablet:Z

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_ProductID:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_Type:I

    invoke-direct {v0, p2, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;-><init>(Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 33
    return-void
.end method

.method private findComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    .line 126
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public appendCommentList(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)V

    .line 171
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_LastAppendedList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 172
    return-void
.end method

.method public checkRatingAuthorityCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;)V

    return-object v0
.end method

.method public deleteComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;Ljava/lang/String;)V

    return-object v0
.end method

.method public abstract getAddCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getCommentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    if-nez v0, :cond_0

    .line 213
    const-string v0, ""

    .line 215
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->getCommentID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    return-object v0
.end method

.method public getCommentText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->findComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 117
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getComment()Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCommentType()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_Type:I

    return v0
.end method

.method public getLastAppendedList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_LastAppendedList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    return-object v0
.end method

.method public abstract getModifyCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getOldCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    if-nez v0, :cond_0

    .line 200
    const-string v0, ""

    .line 202
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->getMyComment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getOldRatingValue()I
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    if-nez v0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 189
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->getMyRatingValue()I

    move-result v0

    goto :goto_0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_ProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getProhibitWords()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_ProhibitWords:[Ljava/lang/String;

    return-object v0
.end method

.method public getRatingValue(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->findComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 86
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getRating()I

    move-result v0

    int-to-double v0, v0

    .line 88
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    goto :goto_0
.end method

.method public getRatingValue()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->getMyRatingValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_iRatingValue:I

    .line 109
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_iRatingValue:I

    return v0
.end method

.method public abstract getReviewDetailViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public hasMyComment()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    if-nez v0, :cond_0

    .line 226
    const/4 v0, 0x0

    .line 228
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->hasMyComment()Z

    move-result v0

    goto :goto_0
.end method

.method public isMyReviewDuplicated()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mIsMyReviewDuplicated:Z

    return v0
.end method

.method public isTablet()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_bTablet:Z

    return v0
.end method

.method public loadComment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;)V

    return-object v0
.end method

.method public loadExpertCommend()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;)V

    return-object v0
.end method

.method public modifyComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;Ljava/lang/String;)V

    return-object v0
.end method

.method public myReviewCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;)V

    return-object v0
.end method

.method public registerComment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;)V

    return-object v0
.end method

.method public setDuplicatedMyReview(Z)V
    .locals 0

    .prologue
    .line 165
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mIsMyReviewDuplicated:Z

    .line 166
    return-void
.end method

.method public setMyReviewData(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    .line 100
    return-void
.end method

.method public setProhibitWords([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_ProhibitWords:[Ljava/lang/String;

    .line 154
    return-void
.end method

.method public validateLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method
