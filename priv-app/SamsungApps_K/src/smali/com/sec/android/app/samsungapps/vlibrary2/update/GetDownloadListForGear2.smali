.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;
.super Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;
.source "ProGuard"


# instance fields
.field private mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;-><init>(Landroid/content/Context;)V

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->supersendRequestGetDownloadList()V

    return-void
.end method

.method private supersendRequestGetDownloadList()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendRequestGetDownloadList()V

    .line 58
    return-void
.end method


# virtual methods
.method protected getParams(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->createWithWGT(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    move-result-object v0

    return-object v0
.end method

.method protected sendRequestGetDownloadList()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->setConnectionObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager$IGearAPIConnectionStateObserver;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->connect()V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;->supersendRequestGetDownloadList()V

    goto :goto_0
.end method
