.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;
.super Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field private static final serialVersionUID:J = 0x32164f88c1e56ba6L


# instance fields
.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 17
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->clear()V

    .line 49
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 27
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->add(Ljava/lang/Object;)Z

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 29
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 22
    return-void
.end method

.method public bridge synthetic remove(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    return-object v0
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 60
    return-void
.end method
