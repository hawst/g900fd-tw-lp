.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadUpdateInfo;


# instance fields
.field public GUID:Ljava/lang/String;

.field public UpgradeClsf:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public linkProductYn:Ljava/lang/String;

.field public loadType:Ljava/lang/String;

.field public productName:Ljava/lang/String;

.field public registeredVersion:Ljava/lang/String;

.field public registeredVersionCode:Ljava/lang/String;

.field public updatableVersion:Ljava/lang/String;

.field public updatableVersionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->GUID:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->contentType:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->loadType:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->registeredVersion:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->registeredVersionCode:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->updatableVersion:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->updatableVersionCode:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->UpgradeClsf:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->linkProductYn:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->productName:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegisteredVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->registeredVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getRegisteredVersionCode()J
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->registeredVersionCode:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method
