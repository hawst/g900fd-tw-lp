.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)V

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 10
    const-string v2, "bundleList2Notc"

    const-string v3, "2090"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    const-string v0, "productID"

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    return-void
.end method
