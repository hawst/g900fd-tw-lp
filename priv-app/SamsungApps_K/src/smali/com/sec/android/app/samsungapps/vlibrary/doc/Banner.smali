.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public GUID:Ljava/lang/String;

.field public bannerDescription:Ljava/lang/String;

.field public bannerImgURL:Ljava/lang/String;

.field public bannerIndex:I

.field public bannerLinkURL:Ljava/lang/String;

.field public bannerPos:I

.field public bannerProductID:Ljava/lang/String;

.field public bannerTitle:Ljava/lang/String;

.field public bannerType:I

.field public linkProductYn:Ljava/lang/String;

.field mLinkedContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field public promotionTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/e;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/e;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>(Landroid/os/Parcel;)V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerType:I

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerPos:I

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->mLinkedContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 49
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerType:I

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerPos:I

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->mLinkedContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 43
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 44
    return-void
.end method


# virtual methods
.method public getBannerDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getBannerIndex()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerIndex:I

    return v0
.end method

.method public getBannerLinkURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerLinkURL:Ljava/lang/String;

    return-object v0
.end method

.method public getBannerPosByGUI()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerPos:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getBannerPromotionTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->promotionTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getBannerType()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerType:I

    return v0
.end method

.method public getContentDetailContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 0

    .prologue
    .line 103
    return-object p0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkedContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->mLinkedContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerImgURL:Ljava/lang/String;

    return-object v0
.end method

.method public isBigBanner()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public isFeaturedBanner()Z
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerPos:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInteractionBanner()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public isLinkApp()Z
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->isLinkProduct()Z

    move-result v0

    return v0
.end method

.method public isLinkProduct()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->linkProductYn:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->linkProductYn:Ljava/lang/String;

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNewBanner()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 169
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerPos:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isURLBanner()Z
    .locals 2

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needGetProductSetList()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 85
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needProductDetail()Z
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->writeToParcel(Landroid/os/Parcel;I)V

    .line 56
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 57
    return-void
.end method
