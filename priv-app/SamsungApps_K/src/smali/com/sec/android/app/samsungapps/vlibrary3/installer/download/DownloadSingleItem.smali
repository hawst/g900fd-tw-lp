.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

.field b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private i:Landroid/content/Context;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;

.field private k:Z

.field private l:Landroid/os/Handler;

.field private m:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private n:Z

.field private o:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field private p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

.field private q:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;

.field private r:Z

.field private s:Z

.field private t:Landroid/os/CountDownTimer;

.field private u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

.field private v:J

.field private w:J

.field private x:J

.field private y:Ljava/lang/String;

.field private z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;ZZLcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    .line 48
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->l:Landroid/os/Handler;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->n:Z

    .line 409
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    .line 499
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 513
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    .line 515
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->x:J

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 73
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->g:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;

    .line 74
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->j:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;

    .line 75
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->k:Z

    .line 76
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->m:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 77
    iput-boolean p7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->n:Z

    .line 78
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    .line 79
    iput-object p9, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->o:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 80
    iput-object p10, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->q:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;

    .line 81
    iput-boolean p11, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->s:Z

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V
    .locals 1

    .prologue
    .line 602
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)Z

    .line 603
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 103
    :goto_0
    return v0

    .line 100
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->getGearSignature()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 127
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->signature:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    return-object v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 402
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isKNOXApp()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 405
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;

    .line 571
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;->onPaymentSuccess()V

    goto :goto_0

    .line 573
    :cond_0
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 578
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 581
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    return-void
.end method

.method public execute()V
    .locals 1

    .prologue
    .line 615
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 616
    return-void
.end method

.method public getOldDownloadedSize()J
    .locals 2

    .prologue
    .line 521
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->w:J

    return-wide v0
.end method

.method public getOldExpectedSize()J
    .locals 2

    .prologue
    .line 526
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->x:J

    return-wide v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public getTotalSize()J
    .locals 2

    .prologue
    .line 681
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->v:J

    return-wide v0
.end method

.method public isCancellable()Z
    .locals 2

    .prologue
    .line 474
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/h;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 479
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 477
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 145
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/h;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 324
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 148
    :pswitch_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->t:Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->t:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 151
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->t:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->t:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->t:Landroid/os/CountDownTimer;

    goto :goto_0

    .line 154
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;->onDownloadFailed()V

    goto :goto_1

    .line 157
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->enqueue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V

    goto :goto_0

    .line 160
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->g:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;->createURLRequestor(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->execute()V

    goto :goto_0

    .line 163
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->cancel()Z

    goto :goto_0

    .line 168
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;->onDownloadCanceled()V

    goto :goto_2

    .line 171
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;->onDownloadSuccess()V

    goto :goto_3

    .line 174
    :pswitch_9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTOnly()Z

    move-result v0

    :goto_4
    if-eqz v0, :cond_2

    .line 176
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "WGTOnlyInstall"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->o:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getSaveFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-interface {v0, v1, v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createWGTOnlyInstaller(Landroid/content/Context;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto/16 :goto_0

    :cond_1
    move v0, v7

    .line 174
    goto :goto_4

    .line 180
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_5
    if-eqz v7, :cond_4

    .line 182
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "WGTInAPKInstall"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    const-string v1, "signature.zip"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->o:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getSaveFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b()Ljava/lang/String;

    move-result-object v4

    move-object v6, p0

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createWGTInAPKInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto/16 :goto_0

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTInAPK()Z

    move-result v7

    goto :goto_5

    .line 186
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->r:Z

    if-eqz v0, :cond_5

    .line 188
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "DeltaInstall"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;->fromContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getSaveFileName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->o:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isKNOXApp()Z

    move-result v6

    iget-boolean v8, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->k:Z

    move-object v7, p0

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createSigProtectedDeltaSupportedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto/16 :goto_0

    .line 193
    :cond_5
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "NormalInstall"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->o:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getSaveFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isKNOXApp()Z

    move-result v5

    iget-boolean v7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->k:Z

    move-object v6, p0

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createSigProtectedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto/16 :goto_0

    .line 199
    :pswitch_a
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/util/MultiUserUtil;->isMultiUserSupport(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v6, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/util/MultiUserUtil;->isInstalledForOtherUser(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v6, :cond_6

    .line 201
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->onRequestFILEResult(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 205
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ON_DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    goto/16 :goto_0

    .line 203
    :cond_6
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->s:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ON_DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->forDelta(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->r:Z

    :goto_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getExpectedSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->v:J

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->q:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getDownloadURI()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getSaveFileName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getExpectedSize()J

    move-result-wide v4

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;

    invoke-direct {v6, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;->createForOneTimeURL(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->request()V

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->r:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->forApk(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->u:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6

    .line 220
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->j:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->j:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;->execute()V

    goto/16 :goto_0

    :cond_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->NOT_SUPPORT_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    goto/16 :goto_0

    .line 229
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->dequeue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V

    goto/16 :goto_0

    .line 232
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 233
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v4

    iget-boolean v8, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->n:Z

    if-ne v8, v6, :cond_b

    :goto_7
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setTotalSize(I)V

    .line 236
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 237
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->WAITING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    goto/16 :goto_0

    :cond_b
    move v6, v7

    .line 233
    goto :goto_7

    .line 242
    :pswitch_e
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    goto/16 :goto_0

    .line 245
    :pswitch_f
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;)V

    .line 257
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->validate()V

    goto/16 :goto_0

    .line 260
    :pswitch_10
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFinished(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 263
    :pswitch_11
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->remove(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    goto/16 :goto_0

    .line 266
    :pswitch_12
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->add(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    goto/16 :goto_0

    .line 269
    :pswitch_13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->l:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 272
    :pswitch_14
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    goto/16 :goto_0

    .line 275
    :pswitch_15
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    .line 276
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    goto/16 :goto_0

    .line 279
    :pswitch_16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->z:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;->getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->p:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;->getResultURI()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->copyFrom(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Z

    goto/16 :goto_0

    .line 286
    :pswitch_17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/b;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;)V

    .line 298
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->forceLoad()V

    goto/16 :goto_0

    .line 301
    :pswitch_18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->notifyBroadcastIntentForStartDownload(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 304
    :pswitch_19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->purchaseComplete(Landroid/content/Context;Ljava/lang/String;)V

    .line 305
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e()V

    goto/16 :goto_0

    .line 308
    :pswitch_1a
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->w:J

    .line 309
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->x:J

    goto/16 :goto_0

    .line 312
    :pswitch_1b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->userCancel()V

    goto/16 :goto_0

    .line 318
    :pswitch_1c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->y:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;->onInstallFailedWithErrCode(Ljava/lang/String;)V

    goto :goto_8

    .line 321
    :pswitch_1d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setDetailMain(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V

    goto/16 :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;)V

    return-void
.end method

.method public onCanceled()V
    .locals 1

    .prologue
    .line 637
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->CANCEL_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 638
    return-void
.end method

.method public onForegroundInstalling()V
    .locals 1

    .prologue
    .line 663
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->INSTALL_FOREGROUND_MODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 664
    return-void
.end method

.method public onInstallFailed()V
    .locals 1

    .prologue
    .line 668
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 669
    return-void
.end method

.method public onInstallFailed(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 657
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->y:Ljava/lang/String;

    .line 658
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->INSTALL_FAILED_WITH_ERRORCODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 659
    return-void
.end method

.method public onInstallSuccess()V
    .locals 1

    .prologue
    .line 673
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->INSTALL_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 674
    return-void
.end method

.method public onNeedDownloadThrough3GConnection()V
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->m:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->m:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->i:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 713
    :goto_0
    return-void

    .line 711
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->handoverAgreeFromWiFiTo3G(Z)V

    goto :goto_0
.end method

.method public onNeedPayment()V
    .locals 1

    .prologue
    .line 642
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->URL_NEED_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 643
    return-void
.end method

.method public onPaymentFailed()V
    .locals 1

    .prologue
    .line 691
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 692
    return-void
.end method

.method public onPaymentSuccess()V
    .locals 1

    .prologue
    .line 686
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 687
    return-void
.end method

.method public onPaymentSuccessForDownloadURL()V
    .locals 0

    .prologue
    .line 736
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e()V

    .line 737
    return-void
.end method

.method public onProgress(I)V
    .locals 6

    .prologue
    .line 717
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    if-ne v0, v1, :cond_1

    .line 718
    int-to-long v1, p1

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->v:J

    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->w:J

    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->x:J

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;->onProgress(JJ)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    long-to-int v1, v1

    long-to-int v2, v3

    invoke-virtual {v0, v5, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;II)V

    .line 720
    :cond_1
    return-void
.end method

.method public onRequestFILEResult(Z)V
    .locals 1

    .prologue
    .line 724
    if-eqz p1, :cond_0

    .line 726
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ON_DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 732
    :goto_0
    return-void

    .line 730
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ON_DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    goto :goto_0
.end method

.method public onRun(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;)V
    .locals 1

    .prologue
    .line 620
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->WAIT_LOCK_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 621
    return-void
.end method

.method public onURLFailed()V
    .locals 1

    .prologue
    .line 652
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->URL_REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 653
    return-void
.end method

.method public onURLSucceed()V
    .locals 1

    .prologue
    .line 647
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->URL_REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 648
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 511
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    .line 87
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;)V

    return-void
.end method

.method public userCancel()V
    .locals 2

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    if-ne v0, v1, :cond_0

    .line 590
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 592
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)V

    .line 599
    :cond_1
    return-void
.end method
