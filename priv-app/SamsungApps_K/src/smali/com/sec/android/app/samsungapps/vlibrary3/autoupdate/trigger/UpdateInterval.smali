.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromHours(I)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    .line 16
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->setHours(I)V

    .line 17
    return-object v0
.end method

.method public static fromMinutes(J)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    .line 23
    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->setMinutes(J)V

    .line 24
    return-object v0
.end method

.method public static fromSec(J)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    .line 9
    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->setSeconds(J)V

    .line 10
    return-object v0
.end method


# virtual methods
.method public getHours()J
    .locals 4

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    const-wide/16 v2, 0xe10

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getMills()J
    .locals 4

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getMinutes()J
    .locals 4

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getSeconds()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    return-wide v0
.end method

.method public setHours(I)V
    .locals 2

    .prologue
    .line 33
    mul-int/lit16 v0, p1, 0xe10

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    .line 34
    return-void
.end method

.method public setMinutes(J)V
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0x3c

    mul-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    .line 39
    return-void
.end method

.method public setSeconds(J)V
    .locals 0

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->a:J

    .line 44
    return-void
.end method
