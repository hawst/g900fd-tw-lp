.class public Lcom/sec/android/app/samsungapps/DisclaimerActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# instance fields
.field a:Landroid/content/Context;

.field b:Z

.field c:Z

.field d:Lcom/sec/android/app/samsungapps/WebTermConditionManager;

.field e:Ljava/lang/String;

.field f:Landroid/os/Handler;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->a:Landroid/content/Context;

    .line 26
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->b:Z

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->c:Z

    .line 31
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->e:Ljava/lang/String;

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->h:Z

    .line 115
    new-instance v0, Lcom/sec/android/app/samsungapps/dc;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/dc;-><init>(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->f:Landroid/os/Handler;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->b:Z

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->userAgree(Z)V

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->c:Z

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->cancel()V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->finish()V

    .line 90
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/DisclaimerActivity;Z)V
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->h:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 23
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->b:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->userAgree(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->c:Z

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->mCurActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->sendTermsAgreeMsgToSSuggest()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->finish()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->a()V

    return-void
.end method


# virtual methods
.method protected isHiddenSamsungAccount()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 211
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    if-nez v1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v0

    .line 216
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v1

    .line 217
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v3

    .line 220
    const-string v4, "DTM"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "262"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_2
    const-string v4, "TMZ"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "230"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_3
    const-string v4, "TNL"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "204"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "16"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_4
    const-string v4, "MAX"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "232"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "03"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_5
    const-string v4, "TRG"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "232"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "07"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_6
    const-string v4, "CRO"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "219"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_7
    const-string v4, "TMT"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "297"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_8
    const-string v4, "MBM"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "294"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_9
    const-string v4, "ERA"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "260"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_a
    const-string v4, "COS"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "202"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_b
    const-string v4, "DTR"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "262"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_c
    const-string v4, "TPL"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "260"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_d
    const-string v4, "TMS"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "231"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_e
    const-string v4, "TMU"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "234"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "30"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_f
    const-string v4, "TMU"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    const-string v4, "234"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    const-string v4, "31"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    :cond_10
    const-string v4, "TMU"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "234"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "32"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    :cond_11
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    if-eqz v0, :cond_0

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->a()V

    .line 42
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 43
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 57
    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->a:Landroid/content/Context;

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->d:Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    if-nez v0, :cond_0

    .line 64
    const-string v0, "DisclaimerActivity::onCreate::Command is empty"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->finish()V

    .line 77
    :goto_0
    return-void

    .line 69
    :cond_0
    const v0, 0x7f0802a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f040046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->setMainViewAndEmptyView(I)V

    :goto_1
    const v0, 0x7f0802eb

    const v1, 0x7f0802ec

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/dd;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/dd;-><init>(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/de;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/de;-><init>(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0052

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0802b0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/df;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/df;-><init>(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0053

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0802a0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/dg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/dg;-><init>(Lcom/sec/android/app/samsungapps/DisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c014a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c0149

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->getDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->onDisplayDisclaimer(Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;)V

    goto/16 :goto_0

    .line 76
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 69
    :cond_2
    const v0, 0x7f040046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->setMainView(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 77
    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method

.method public onDisplayDisclaimer(Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 265
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->getText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 267
    :cond_0
    const-string v0, "DisclaimerActivity::onDisplayDisclaimer::Param is empty"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 289
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v1

    .line 272
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 273
    const v0, 0x7f0c0149

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 274
    const v0, 0x7f0c014a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 275
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    if-nez v0, :cond_3

    .line 277
    :cond_2
    const-string v0, "DisclaimerActivity::onDisplayDisclaimer::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 281
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->getText()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/samsungapps/DisclaimerActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 285
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 286
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 287
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
