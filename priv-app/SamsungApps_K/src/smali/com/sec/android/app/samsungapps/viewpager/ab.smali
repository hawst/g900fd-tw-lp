.class final Lcom/sec/android/app/samsungapps/viewpager/ab;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ab;->a:Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public final onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method public final onPageSelected(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ab;->a:Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    .line 177
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;

    .line 178
    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->setBigBannerHandler(Z)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ab;->a:Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;I)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ab;->a:Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ab;->a:Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->b(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getCurrentTabDescription(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->setTitle(Ljava/lang/CharSequence;)V

    .line 183
    return-void
.end method
