.class public Lcom/sec/android/app/samsungapps/MenuItem;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/sec/android/app/samsungapps/MenuItem;->b:I

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/MenuItem;->a:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/MenuItem;->c:Landroid/view/View$OnClickListener;

    .line 19
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->c:Landroid/view/View$OnClickListener;

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->a:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->b:I

    .line 25
    return-void
.end method

.method public execute(Lcom/sec/android/app/samsungapps/MenuItemList;Lcom/sec/android/app/samsungapps/view/MenuView;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 43
    :cond_0
    invoke-virtual {p1, p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->setSel(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 44
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->b:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItem;->a:Ljava/lang/String;

    return-object v0
.end method
