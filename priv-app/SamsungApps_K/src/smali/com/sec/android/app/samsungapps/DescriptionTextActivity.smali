.class public Lcom/sec/android/app/samsungapps/DescriptionTextActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# instance fields
.field a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;->a:[I

    return-void

    :array_0
    .array-data 4
        0xa0005
        0xa0007
    .end array-data
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "descriptionText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "titleString"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f040028

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;->setMainView(I)V

    const v0, 0x7f0c00ab

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;->a:[I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 22
    return-void
.end method
