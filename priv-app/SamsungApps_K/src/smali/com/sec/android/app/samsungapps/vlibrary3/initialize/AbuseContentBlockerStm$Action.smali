.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ASK_POPUP_FOR_VALIDATION_USER_AGE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum NOTIFY_CLOSE_WINDOW:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum NOTIFY_CLOSE_WINDOW_CAUSE_LOGEDOUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum NOTIFY_CLOSE_WINDOW_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum NOTIFY_CLOSE_WITHOUT_NOTICE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum REQ_CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum REQ_LOGIN_AND_AGE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum SHOW_NEED_TO_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field public static final enum VERIFY_REAL_AGE_NAME:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "REQ_CHECK_CONDITION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQ_CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "NOTIFY_SHOW_DETAIL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "REQ_LOGIN_AND_AGE_CHECK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQ_LOGIN_AND_AGE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "NOTIFY_CLOSE_WINDOW"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "SHOW_NEED_TO_LOGIN"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->SHOW_NEED_TO_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "REQUEST_LOGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "ASK_POPUP_FOR_VALIDATION_USER_AGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->ASK_POPUP_FOR_VALIDATION_USER_AGE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "VERIFY_REAL_AGE_NAME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->VERIFY_REAL_AGE_NAME:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "NOTIFY_CLOSE_WINDOW_UNDER19"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "NOTIFY_CLOSE_WINDOW_CAUSE_LOGEDOUT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW_CAUSE_LOGEDOUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    const-string v1, "NOTIFY_CLOSE_WITHOUT_NOTICE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WITHOUT_NOTICE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    .line 16
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQ_CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQ_LOGIN_AND_AGE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->SHOW_NEED_TO_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->ASK_POPUP_FOR_VALIDATION_USER_AGE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->VERIFY_REAL_AGE_NAME:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW_CAUSE_LOGEDOUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WITHOUT_NOTICE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    return-object v0
.end method
