.class public Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 19
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getMultipleDeviceDownloadCount()Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->isDownloadCountReachedMax()Z

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isFreeContent()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasOrderID()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isAlreadyPurchased()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    .line 49
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected matchCondition()Z
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->a()Z

    move-result v0

    return v0
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->_Context:Landroid/content/Context;

    const v1, 0xff37

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->_Context:Landroid/content/Context;

    const v3, 0x7f0802f4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->_Context:Landroid/content/Context;

    const v4, 0x7f0802ee

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/i;-><init>(Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;->invokeCompleted()V

    .line 33
    return-void
.end method
