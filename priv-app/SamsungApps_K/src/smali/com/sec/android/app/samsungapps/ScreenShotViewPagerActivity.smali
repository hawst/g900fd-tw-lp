.class public Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

.field private c:Landroid/support/v4/view/ViewPager;

.field private final d:I

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->d:I

    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/hv;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hv;-><init>(Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->e:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->beginFakeDrag()Z

    .line 81
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->e:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->e:Landroid/os/Handler;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_4

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    .line 90
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 93
    if-eqz v0, :cond_2

    .line 94
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 90
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 96
    :pswitch_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->getHeight()I

    move-result v4

    if-gt v3, v4, :cond_3

    .line 97
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 103
    :pswitch_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->getHeight()I

    move-result v4

    if-le v3, v4, :cond_3

    .line 104
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 106
    :cond_3
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 113
    :cond_4
    return-void

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/high16 v4, 0x1000000

    const/16 v3, 0x400

    const/4 v1, 0x1

    .line 48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 55
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->findSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->finish()V

    .line 74
    :cond_1
    :goto_1
    return-void

    .line 57
    :cond_2
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    move v0, v1

    goto :goto_0

    .line 61
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 63
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->requestWindowFeature(I)Z

    .line 65
    const v0, 0x7f0400c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->setContentView(I)V

    .line 67
    const v0, 0x7f0c02a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getSel()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    .line 159
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 161
    if-eqz v2, :cond_0

    .line 162
    invoke-virtual {v2}, Landroid/view/View;->destroyDrawingCache()V

    .line 163
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 168
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->c:Landroid/support/v4/view/ViewPager;

    .line 170
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 171
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 177
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->findSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_0

    .line 180
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z

    .line 182
    :cond_0
    return-void
.end method

.method public setImageScaleType(Landroid/content/res/Configuration;Lcom/sec/android/app/samsungapps/view/CacheWebImageView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getScreenShot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getOrientation(I)I

    move-result v0

    .line 135
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v1, :pswitch_data_0

    .line 153
    :goto_0
    return-void

    .line 137
    :pswitch_0
    if-eq v0, v2, :cond_0

    .line 138
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 145
    :pswitch_1
    if-ne v0, v2, :cond_0

    .line 146
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 148
    :cond_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
