.class final Lcom/sec/android/app/samsungapps/notipopup/d;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static e:Ljava/util/HashMap;

.field private static f:Ljava/util/HashMap;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

.field public d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/notipopup/d;->e:Ljava/util/HashMap;

    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/notipopup/d;->f:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/sec/android/app/samsungapps/notipopup/d;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 156
    new-instance v1, Lcom/sec/android/app/samsungapps/notipopup/d;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/notipopup/d;-><init>()V

    .line 157
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/notipopup/d;->b:Ljava/lang/String;

    .line 158
    const-string v0, "TITLE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/notipopup/d;->a:Ljava/lang/String;

    .line 159
    const-string v0, "OBSERVERID"

    invoke-virtual {p0, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 160
    sget-object v2, Lcom/sec/android/app/samsungapps/notipopup/d;->e:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    .line 161
    const-string v0, "OKCANCEL_OBSERVERID"

    invoke-virtual {p0, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 162
    sget-object v2, Lcom/sec/android/app/samsungapps/notipopup/d;->f:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    .line 163
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;)Lcom/sec/android/app/samsungapps/notipopup/d;
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/sec/android/app/samsungapps/notipopup/d;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/notipopup/d;-><init>()V

    .line 168
    iput-object p0, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->a:Ljava/lang/String;

    .line 169
    iput-object p1, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->b:Ljava/lang/String;

    .line 170
    iput-object p2, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    .line 171
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;)Lcom/sec/android/app/samsungapps/notipopup/d;
    .locals 1

    .prologue
    .line 175
    new-instance v0, Lcom/sec/android/app/samsungapps/notipopup/d;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/notipopup/d;-><init>()V

    .line 176
    iput-object p0, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->a:Ljava/lang/String;

    .line 177
    iput-object p1, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->b:Ljava/lang/String;

    .line 178
    iput-object p2, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    .line 179
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 183
    const-string v0, "TITLE"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    const-string v0, "MESSAGE"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "OBSERVERID"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 187
    sget-object v0, Lcom/sec/android/app/samsungapps/notipopup/d;->e:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    if-eqz v0, :cond_1

    .line 190
    const-string v0, "OKCANCEL_OBSERVERID"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 191
    sget-object v0, Lcom/sec/android/app/samsungapps/notipopup/d;->f:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_1
    return-void
.end method
