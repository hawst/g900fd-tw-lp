.class final Lcom/sec/android/app/samsungapps/commands/al;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/al;->a:Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 99
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;

    .line 100
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 102
    const v1, 0x7f0802f4

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/am;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/commands/am;-><init>(Lcom/sec/android/app/samsungapps/commands/al;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 110
    const v1, 0x7f0802ee

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/an;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/commands/an;-><init>(Lcom/sec/android/app/samsungapps/commands/al;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 119
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 120
    return-void
.end method
