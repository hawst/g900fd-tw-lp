.class final Lcom/sec/android/app/samsungapps/widget/list/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/e;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/e;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailProductListWidget::onItemClick  content ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 166
    if-nez v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/e;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getVproductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/e;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->c(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/e;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/e;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V

    goto :goto_0
.end method
