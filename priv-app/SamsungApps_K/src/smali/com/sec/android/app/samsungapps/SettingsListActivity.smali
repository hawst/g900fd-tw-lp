.class public Lcom/sec/android/app/samsungapps/SettingsListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;
.source "ProGuard"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;


# static fields
.field public static final KEY_ABOUT:Ljava/lang/String; = "About"

.field public static final KEY_AD_PREFERENCE:Ljava/lang/String; = "AdPreference"

.field public static final KEY_AUTO_UPDATE:Ljava/lang/String; = "AutoUpdate"

.field public static final KEY_NOTIFY_APP_UPDATES:Ljava/lang/String; = "NotifyAppUpdates"

.field public static final KEY_NOTIFY_STORE_ACTIVITIES:Ljava/lang/String; = "NotifyStoreActivities"

.field public static final KEY_PURCHASE_PROTECTION:Ljava/lang/String; = "PurchaseProtection"

.field public static final KEY_PUSH_NOTI:Ljava/lang/String; = "PushNoti"

.field public static final KEY_RESET_LOCALE:Ljava/lang/String; = "ResetLocale"

.field public static final KEY_SAMSUNG_ACC:Ljava/lang/String; = "SamsungAcc"

.field public static final KEY_SEARCH_SETTING:Ljava/lang/String; = "SearchSetting"

.field public static final KEY_SUGGEST_ALLSHARE:Ljava/lang/String; = "SuggestAllSharePreference"

.field public static final KEY_UPDATE_NOTI:Ljava/lang/String; = "UpdateNoti"

.field public static final REQUEST_CODE_SAC_EDIT_ACCOUNT:I = 0x2015


# instance fields
.field a:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field b:Landroid/preference/PreferenceScreen;

.field c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

.field d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    .line 61
    const-string v0, "SettingsListActivity"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->e:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->finish()V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 191
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 195
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :goto_0
    return-void

    .line 197
    :catch_0
    move-exception v0

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SettingsListActivity::_restartActivity::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/LoadingDialog;->setCancelable(Z)V

    .line 215
    return-void
.end method


# virtual methods
.method public getSubTitleAutoUpdate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 255
    const-string v0, ""

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    if-nez v1, :cond_0

    .line 259
    const-string v1, "SettingsListActivity::getSubTitleAutoUpdate::Helper is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 266
    :goto_0
    return-object v0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->getSubTitleAutoUpdate()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSubTitleUpdateNotification()Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    const-string v0, ""

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    if-nez v1, :cond_0

    .line 238
    const-string v1, "SettingsListActivity::getSubTitleUpdateNotification::Helper is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 245
    :goto_0
    return-object v0

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->getSubTitleUpdateNotification()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 477
    .line 479
    sget-object v1, Lcom/sec/android/app/samsungapps/io;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 498
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    .line 501
    :cond_0
    :goto_0
    return v0

    .line 485
    :pswitch_0
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 486
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-eq v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogOutFailed:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v1, v2, :cond_0

    .line 492
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a()V

    goto :goto_0

    .line 479
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 119
    const/16 v0, 0x2015

    if-ne p1, v0, :cond_0

    .line 121
    new-instance v0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/app/Activity;)V

    .line 122
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b()V

    .line 130
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a()V

    .line 136
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 66
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f04005d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->setMainView(I)V

    .line 70
    const v0, 0x7f0802b4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v4, v3, [I

    const/4 v0, 0x0

    const v5, 0xa0008

    aput v5, v4, v0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 73
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 74
    const v0, 0x7f0c0188

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->setWidgetClickListener(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;-><init>(Landroid/content/Context;Landroid/preference/PreferenceScreen;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->loadWidget()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->setWidget(Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;)V

    .line 75
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 89
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->clear()V

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->release()V

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    .line 109
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b:Landroid/preference/PreferenceScreen;

    .line 111
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onDestroy()V

    .line 112
    return-void
.end method

.method public onItemClick(Landroid/preference/Preference;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 275
    .line 276
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    if-nez v0, :cond_2

    .line 278
    :cond_0
    const-string v0, "SettingsListActivity::onItemClick::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 472
    :cond_1
    :goto_0
    return-void

    .line 282
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 283
    if-nez v0, :cond_3

    .line 285
    const-string v0, "SettingsListActivity::onItemClick::Key is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 292
    :cond_3
    const-string v1, "UpdateNoti"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_4

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onUpdateNotificationClick(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 301
    :cond_4
    const-string v1, "ResetLocale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_5

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onResetLocaleClick(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 310
    :cond_5
    const-string v1, "SearchSetting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_6

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onSearchSettingClick(Landroid/preference/Preference;)Z

    .line 318
    :cond_6
    const-string v1, "PushNoti"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_a

    .line 320
    const v0, 0x7f0c02b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 321
    const v1, 0x7f0c02b3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 322
    if-eqz v0, :cond_7

    if-nez v1, :cond_8

    .line 324
    :cond_7
    const-string v0, "SettingsListActivity::onItemClick::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-nez v5, :cond_9

    :goto_1
    invoke-virtual {v4, p1, v2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onPushNotificationClick(Landroid/preference/Preference;Z)Z

    .line 333
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 334
    const v0, 0x7f080281

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_9
    move v2, v3

    .line 328
    goto :goto_1

    .line 341
    :cond_a
    const-string v1, "AdPreference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_b

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onAdPreferenceClick(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 361
    :cond_b
    const-string v1, "SuggestAllSharePreference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_f

    .line 363
    const v0, 0x7f0c02b8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 364
    if-nez v0, :cond_c

    .line 366
    const-string v0, "SettingsListActivity::onItemClick::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 370
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_d

    move v1, v2

    :goto_2
    invoke-virtual {v4, p1, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onAllShareClick(Landroid/preference/Preference;Z)Z

    .line 371
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_e

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    :cond_d
    move v1, v3

    .line 370
    goto :goto_2

    :cond_e
    move v2, v3

    .line 371
    goto :goto_3

    .line 378
    :cond_f
    const-string v1, "SamsungAcc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_11

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onAccountClick(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v2, :cond_10

    .line 403
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 404
    const-string v1, "com.msc.action.samsungaccount.accountsetting"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    const/16 v1, 0x2015

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 410
    :catch_0
    move-exception v0

    .line 412
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SettingsListActivity::onItemClick::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 417
    :cond_10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->b()V

    goto/16 :goto_0

    .line 426
    :cond_11
    const-string v1, "About"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_12

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onAboutClick(Landroid/preference/Preference;)Z

    .line 430
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/AboutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 431
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 438
    :cond_12
    const-string v1, "AutoUpdate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_13

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onAutoUpdateClick(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 447
    :cond_13
    const-string v1, "NotifyAppUpdates"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_14

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onNotifyAppUpdatesClick(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 456
    :cond_14
    const-string v1, "NotifyStoreActivities"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_15

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onNotifyStoreActivitiesClick(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 466
    :cond_15
    const-string v1, "PurchaseProtection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->onPurchaseProtectionClick(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onResume()V

    .line 81
    return-void
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    if-nez v0, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 178
    :goto_0
    return v0

    .line 175
    :cond_0
    const v0, 0x7f0c0006

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 176
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 178
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 510
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    .line 515
    const-string v0, "PushNoti"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 517
    const v0, 0x7f0c02b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 519
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v4

    if-ne v4, v1, :cond_0

    .line 521
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isPushSvcTurnedOn()Z

    move-result v3

    .line 523
    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 542
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 523
    goto :goto_0

    .line 532
    :cond_2
    const-string v0, "SuggestAllSharePreference"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 534
    const v0, 0x7f0c02b8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 535
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v4

    if-ne v4, v1, :cond_0

    .line 537
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isSuggestAllShareContentChecked()Z

    move-result v3

    .line 539
    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method
