.class public Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

.field c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field final d:I

.field e:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 27
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->d:I

    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/n;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/n;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->e:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 27
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->d:I

    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/n;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/n;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->e:Landroid/os/Handler;

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->a:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public IsLogin()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    return v0
.end method

.method public getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    return-object v0
.end method

.method public getContentDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public onRefreshContainer(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 72
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public setContentDetailContainer(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 63
    if-nez p1, :cond_0

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    goto :goto_0
.end method
