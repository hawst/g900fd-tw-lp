.class public Lcom/sec/android/app/samsungapps/base/BaseActivity;
.super Landroid/app/Activity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;


# instance fields
.field protected mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    return-void
.end method


# virtual methods
.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGearPlatformVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 20
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    const-string v1, "deepLinkAppName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_1

    .line 33
    const-string v1, "deepLinkAppName"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 39
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_1

    .line 50
    const-string v1, "deepLinkAppName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    .line 53
    const-string v1, "deepLinkAppName"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 59
    return-void
.end method
