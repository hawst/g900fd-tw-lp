.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 20
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;

    .line 34
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 179
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 180
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 253
    :goto_0
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_STATE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 254
    return-void

    .line 183
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 184
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 185
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ADD_DLSTATE_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ADD_QUICKPANEL_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 190
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 191
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ENQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 194
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 197
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CLEAR_OLD_DOWNLOAD_PROGRESS_VAR:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_DOWNLOADING_TO_GAMEHUB:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DL_STATE_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 204
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DL_STATE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 206
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 207
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 208
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DEQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 211
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 212
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_QUICKPANEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 213
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 214
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_DL_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 215
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 216
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DEQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 219
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 223
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQ_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DL_STATE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 228
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 233
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CHECK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 236
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_DL_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 237
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 238
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DEQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 239
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 242
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 243
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DOWNLOADURI_FROMPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 244
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->REFRESH_DETAL_AFTERPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 247
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->FORCE_UPDATE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 250
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->FORCE_UPDATE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_a
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_d
        :pswitch_e
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 40
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 41
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 174
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 43
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DETAIL_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 54
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 65
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->WAIT_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 85
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    :pswitch_e
    goto :goto_0

    .line 99
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 104
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    :pswitch_13
    goto/16 :goto_0

    .line 117
    :pswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->INSTALL_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 107
    :pswitch_15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 110
    :pswitch_16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 113
    :pswitch_17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 120
    :pswitch_18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_INSTALL_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 121
    :pswitch_19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 129
    :pswitch_1a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    .line 132
    :pswitch_1b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 137
    :pswitch_1c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_8

    goto/16 :goto_0

    .line 140
    :pswitch_1d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 143
    :pswitch_1e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->REFRESH_DETAIL_AFTER_PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 146
    :pswitch_1f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 151
    :pswitch_20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_9

    goto/16 :goto_0

    .line 157
    :pswitch_21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->WAIT_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    :pswitch_22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 162
    :pswitch_23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_a

    goto/16 :goto_0

    .line 166
    :goto_1
    :pswitch_24
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 165
    :pswitch_25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CLEAR_CONTENT_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_1

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_a
        :pswitch_d
        :pswitch_12
        :pswitch_1a
        :pswitch_1c
        :pswitch_20
        :pswitch_23
    .end packed-switch

    .line 43
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 51
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 62
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 79
    :pswitch_data_4
    .packed-switch 0x7
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 90
    :pswitch_data_5
    .packed-switch 0x7
        :pswitch_f
        :pswitch_e
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 104
    :pswitch_data_6
    .packed-switch 0x7
        :pswitch_14
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 129
    :pswitch_data_7
    .packed-switch 0x10
        :pswitch_1b
    .end packed-switch

    .line 137
    :pswitch_data_8
    .packed-switch 0x11
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch

    .line 151
    :pswitch_data_9
    .packed-switch 0x2
        :pswitch_21
        :pswitch_22
    .end packed-switch

    .line 162
    :pswitch_data_a
    .packed-switch 0x2
        :pswitch_24
        :pswitch_25
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 258
    const-string v0, "DownloadSingleItemStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 259
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/i;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->ordinal()I

    .line 263
    return-void
.end method
