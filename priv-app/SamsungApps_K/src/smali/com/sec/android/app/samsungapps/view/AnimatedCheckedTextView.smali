.class public Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;
.super Landroid/widget/CheckedTextView;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/res/TypedArray;

.field private b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 31
    sget-object v0, Lcom/android/internal/R$styleable;->TextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/sec/android/app/samsungapps/R$styleable;->CheckBoxImage:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    .line 33
    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 35
    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 37
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 22
    sget-object v0, Lcom/android/internal/R$styleable;->TextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 23
    sget-object v1, Lcom/sec/android/app/samsungapps/R$styleable;->CheckBoxImage:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    .line 24
    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 25
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 26
    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 28
    :cond_0
    return-void
.end method


# virtual methods
.method public setChecked(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 70
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 71
    return-void

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->a:Landroid/content/res/TypedArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
