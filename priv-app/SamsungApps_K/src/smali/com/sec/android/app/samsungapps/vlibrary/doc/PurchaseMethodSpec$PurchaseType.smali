.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field public static final enum AliPayPurchase:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field public static final enum ChinaCyberCash:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field public static final enum ChinaPPC:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field public static final enum GlocalCreditCard:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field public static final enum NULL:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field public static final enum PSMS:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->NULL:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    const-string v1, "PSMS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->PSMS:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    const-string v1, "GlocalCreditCard"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->GlocalCreditCard:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    const-string v1, "ChinaCyberCash"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->ChinaCyberCash:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    const-string v1, "ChinaPPC"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->ChinaPPC:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    const-string v1, "AliPayPurchase"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->AliPayPurchase:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 4
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->NULL:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->PSMS:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->GlocalCreditCard:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->ChinaCyberCash:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->ChinaPPC:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->AliPayPurchase:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    return-object v0
.end method
