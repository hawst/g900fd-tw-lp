.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected checkCardBalance()V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->checkCardBalance()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 34
    return-void
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->checkCardBalance()V

    .line 23
    return-void
.end method

.method protected onAfterCheckCardBalance(Z)V
    .locals 1

    .prologue
    .line 38
    if-eqz p1, :cond_1

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->isCardBalanceEnough()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->requestPayment()V

    .line 53
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->requestRegisterCard()V

    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->onFinalResult(Z)V

    goto :goto_0
.end method

.method protected requestPayment()V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 85
    return-void
.end method

.method protected requestRegisterCard()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->requestRegisterCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/b;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 74
    return-void
.end method
