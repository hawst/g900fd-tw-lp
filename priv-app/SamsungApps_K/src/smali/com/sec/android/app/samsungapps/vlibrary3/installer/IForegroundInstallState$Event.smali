.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

.field public static final enum INSTALLCOMPLETED_AND_GUIDMATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    const-string v1, "INSTALLCOMPLETED_AND_GUIDMATCH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->INSTALLCOMPLETED_AND_GUIDMATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->INSTALLCOMPLETED_AND_GUIDMATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    return-object v0
.end method
