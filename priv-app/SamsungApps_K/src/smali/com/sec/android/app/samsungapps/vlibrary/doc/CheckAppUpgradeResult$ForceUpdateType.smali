.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

.field public static final enum AllowNotUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

.field public static final enum ForceUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

.field public static final enum ForceUpdateAndDeepLinkUpdateVisible:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

.field public static final enum PossibleToUseWithoutUpdateAndDoesNotShowInDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 287
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    const-string v1, "AllowNotUpdate"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->AllowNotUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    .line 288
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    const-string v1, "ForceUpdate"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ForceUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    .line 289
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    const-string v1, "ForceUpdateAndDeepLinkUpdateVisible"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ForceUpdateAndDeepLinkUpdateVisible:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    const-string v1, "PossibleToUseWithoutUpdateAndDoesNotShowInDeepLink"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->PossibleToUseWithoutUpdateAndDoesNotShowInDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    .line 285
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->AllowNotUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ForceUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ForceUpdateAndDeepLinkUpdateVisible:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->PossibleToUseWithoutUpdateAndDoesNotShowInDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;
    .locals 1

    .prologue
    .line 285
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    return-object v0
.end method
