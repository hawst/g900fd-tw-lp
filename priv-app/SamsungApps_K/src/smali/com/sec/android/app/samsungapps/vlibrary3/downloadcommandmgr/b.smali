.class final Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadPrecheckFailed()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->onPrecheckResult(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;->onFailed()V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->c(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

    .line 108
    :cond_0
    return-void
.end method

.method public final onDownloadPrecheckSucceed()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->onPrecheckResult(Z)V

    .line 114
    return-void
.end method
