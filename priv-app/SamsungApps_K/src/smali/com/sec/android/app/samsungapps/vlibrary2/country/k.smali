.class final Lcom/sec/android/app/samsungapps/vlibrary2/country/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 29
    if-eqz p2, :cond_0

    .line 31
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->save()V

    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyCountryDecided()V

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;Z)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;

    # setter for: Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;
    invoke-static {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->access$102(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;Z)V

    goto :goto_0
.end method
