.class final Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 43
    if-eqz p2, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->_IGetCategoryProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->setCategoryProductList(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;Z)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;Z)V

    goto :goto_0
.end method
