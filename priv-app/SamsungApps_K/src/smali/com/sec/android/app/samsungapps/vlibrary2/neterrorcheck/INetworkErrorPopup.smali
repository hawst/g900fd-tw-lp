.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract showAirplaneMode(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IConfirm;)V
.end method

.method public abstract showChinaNetworkWarningPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IChinaNetworkWarninigSetter;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;Z)V
.end method

.method public abstract showJellyBeanNetworkPopup(Landroid/content/Context;ILcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;)V
.end method

.method public abstract showNetworkDisconnectedPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IRetryObserver;)V
.end method
