.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

.field private _IInicisRequest:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;

.field private _IInicisWebClientResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IURLProvider:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;

.field _InicisActivity:Ljava/lang/Class;

.field private _InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field private _UPoint:Z

.field private _View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;

.field private _bPhoneBill:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;ZLcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Z)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_UPoint:Z

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    .line 38
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InicisActivity:Ljava/lang/Class;

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisRequest:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;

    .line 41
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_UPoint:Z

    .line 42
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IURLProvider:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;

    .line 43
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 44
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 45
    iput-boolean p9, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_bPhoneBill:Z

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_UPoint:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisWebClientResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;

    return-object v0
.end method


# virtual methods
.method public getCouponSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getGiftCardCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    .line 177
    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 180
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProductToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProductToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->invokeUI(Landroid/content/Context;)V

    .line 157
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;

    .line 167
    return-void
.end method

.method protected invokeUI(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InicisActivity:Ljava/lang/Class;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public isPhoneBill()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_bPhoneBill:Z

    return v0
.end method

.method public isUpoint()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_UPoint:Z

    return v0
.end method

.method protected onRequestCompleteResult(ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 1

    .prologue
    .line 125
    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;->onRequestCompleteResult(Z)V

    .line 133
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->onFinalResult(Z)V

    .line 134
    return-void
.end method

.method protected onRequestInitResult(ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;->onRequestInitResult(Z)V

    .line 113
    if-eqz p1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IURLProvider:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;->openWebURL(Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method

.method public requestConfirmInicis(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;Z)V
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisWebClientResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;

    .line 67
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisRequest:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;Z)V

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/d;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_Context:Landroid/content/Context;

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;->requestComplete(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Landroid/content/Context;)V

    .line 108
    return-void
.end method

.method public requestInitInicis(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;)V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisRequest:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_Context:Landroid/content/Context;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;->requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Landroid/content/Context;)V

    .line 60
    return-void
.end method
