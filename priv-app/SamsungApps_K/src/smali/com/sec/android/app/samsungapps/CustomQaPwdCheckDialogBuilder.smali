.class public Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;
.super Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
.source "ProGuard"


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/widget/EditText;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->b:Landroid/view/View;

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->c:Landroid/widget/EditText;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->createQaPwdCheckDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;

    return-object v0
.end method


# virtual methods
.method public createQaPwdCheckDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 30
    const v2, 0x7f0400ec

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->b:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 49
    :goto_0
    return-object v0

    .line 37
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f08026f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->b:Landroid/view/View;

    const v1, 0x7f0c0291

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->c:Landroid/widget/EditText;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->onPositiveListener()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->onNegativeListener()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    goto :goto_0
.end method

.method public onNegativeListener()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/sec/android/app/samsungapps/cq;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/cq;-><init>(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)V

    return-object v0
.end method

.method public onPositiveListener()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/cp;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/cp;-><init>(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)V

    return-object v0
.end method

.method public startQaStorePwdCheck(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 54
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 58
    :cond_0
    const/4 v0, 0x0

    .line 62
    :goto_0
    return v0

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->checkQAPassword(Ljava/lang/String;)V

    goto :goto_0
.end method
