.class public Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;


# instance fields
.field protected _CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 12
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 17
    return-void
.end method


# virtual methods
.method public getContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    return-object v0
.end method

.method public loadAllCategory()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V

    return-object v0
.end method

.method public loadRootCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V

    return-object v0
.end method

.method public loadSubCatagory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    return-object v0
.end method

.method public setCategoryContainer(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 22
    return-void
.end method
