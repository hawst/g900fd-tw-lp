.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _PermissionProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;

.field private _ProductID:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->mContext:Landroid/content/Context;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->_PermissionProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->_ProductID:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->_PermissionProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;

    return-object v0
.end method


# virtual methods
.method protected abstract onResult(ZLjava/util/HashMap;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
.end method

.method public request()V
    .locals 4

    .prologue
    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->_ProductID:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->_PermissionProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/b;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->androidManifest(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 46
    return-void
.end method
