.class final Lcom/sec/android/app/samsungapps/vlibrary/doc/j;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Z

.field private b:I

.field private c:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 516
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->a:Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->a:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 527
    const v0, 0x36ee80

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->b:I

    .line 528
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 532
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->c:J

    .line 533
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->a:Z

    .line 534
    return-void
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 544
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 554
    :cond_0
    :goto_0
    return v0

    .line 548
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 549
    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->c:J

    sub-long/2addr v1, v3

    .line 550
    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->b:I

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 554
    const/4 v0, 0x0

    goto :goto_0
.end method
