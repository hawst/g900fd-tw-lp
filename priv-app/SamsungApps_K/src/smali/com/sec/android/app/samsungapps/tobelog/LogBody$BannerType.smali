.class public final enum Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CONTENTID:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

.field public static final enum CONTENTSET:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

.field public static final enum URL:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    const-string v1, "CONTENTSET"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->CONTENTSET:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    const-string v1, "CONTENTID"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->CONTENTID:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    const-string v1, "URL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->URL:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    .line 23
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->CONTENTSET:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->CONTENTID:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->URL:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    return-object v0
.end method
