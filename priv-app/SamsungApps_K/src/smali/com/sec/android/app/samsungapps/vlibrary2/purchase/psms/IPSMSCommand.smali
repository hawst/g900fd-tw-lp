.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    .line 14
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onInitResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onTNCCheckResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onRandomkeyResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onConfirmMsgResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onSendSMSResult(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onMoResult(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 30
    return-void
.end method

.method private onConfirmMsgResult(Z)V
    .locals 3

    .prologue
    .line 82
    if-nez p1, :cond_0

    .line 84
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    .line 94
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->sendSMS()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method private onInitResult(Z)V
    .locals 3

    .prologue
    .line 34
    if-eqz p1, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->checkTNC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 46
    :goto_0
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method private onMoResult(Z)V
    .locals 3

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 116
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    .line 126
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method private onRandomkeyResult(Z)V
    .locals 3

    .prologue
    .line 66
    if-nez p1, :cond_0

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    .line 78
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->checkConfirmMSG()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method private onSendSMSResult(Z)V
    .locals 3

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    .line 110
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->moDelivery()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method private onTNCCheckResult(Z)V
    .locals 3

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 52
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->onFinalResult(Z)V

    .line 62
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_IPSMSCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;->checkRandomKey()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;->init()V

    .line 19
    return-void
.end method
