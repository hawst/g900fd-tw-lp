.class public Lcom/sec/android/app/samsungapps/AlipayModule;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field a:Landroid/os/Handler;

.field private b:Landroid/app/Activity;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/h;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/h;-><init>(Lcom/sec/android/app/samsungapps/AlipayModule;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->a:Landroid/os/Handler;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->b:Landroid/app/Activity;

    .line 20
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/AlipayModule;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/AlipayModule;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 13
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/AlipayModule;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 49
    const-string v0, ""

    .line 55
    if-eqz p0, :cond_0

    .line 57
    :try_start_0
    const-string v0, "resultStatus={"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 58
    add-int/lit8 v0, v0, 0xe

    .line 59
    const-string v3, "};memo="

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 61
    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 70
    :cond_0
    const-string v3, "9000"

    .line 71
    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v2

    .line 75
    :goto_0
    return v0

    .line 66
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 75
    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->orderinfo:Ljava/lang/String;

    new-instance v1, Lcom/sec/android/app/samsungapps/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/i;-><init>(Lcom/sec/android/app/samsungapps/AlipayModule;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/AlipayModule;->pay(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;)V

    .line 108
    return-void
.end method

.method public onPayResult(Z)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->d:Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->d:Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;->onResult(Z)V

    .line 95
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->d:Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;

    .line 96
    return-void
.end method

.method public pay(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;)V
    .locals 4

    .prologue
    .line 82
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->d:Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;

    .line 83
    new-instance v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/MobileSecurePayer;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/AlipayModule;->b:Landroid/app/Activity;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->pay(Ljava/lang/String;Landroid/os/Handler;ILandroid/app/Activity;)Z

    .line 86
    return-void
.end method
