.class final Lcom/sec/android/app/samsungapps/v;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/CommonActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/CommonActivity;)V
    .locals 0

    .prologue
    .line 1122
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 1125
    if-eqz p1, :cond_1

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->b(Lcom/sec/android/app/samsungapps/CommonActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1128
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    const v2, 0x7f08012d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 1134
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->requestAndShowType6NoticeList(ZLandroid/content/Context;)V

    .line 1136
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->c(Lcom/sec/android/app/samsungapps/CommonActivity;)Z

    .line 1137
    return-void

    .line 1131
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/v;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    const v2, 0x7f0801d5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0
.end method
