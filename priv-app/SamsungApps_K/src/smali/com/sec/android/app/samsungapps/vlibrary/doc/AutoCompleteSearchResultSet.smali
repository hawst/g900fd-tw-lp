.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static mCacheManager:Ljava/util/HashMap; = null

.field private static final serialVersionUID:J = 0x7e10625a648b335aL


# instance fields
.field bCanceled:Z

.field private bCompleted:Z

.field private mContentType:Ljava/lang/String;

.field private mSearchKeyword:Ljava/lang/String;

.field request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mCacheManager:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    const-string v0, "all"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mSearchKeyword:Ljava/lang/String;

    .line 35
    const-string v0, "all"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mContentType:Ljava/lang/String;

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCompleted:Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 90
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCanceled:Z

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mSearchKeyword:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mContentType:Ljava/lang/String;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mSearchKeyword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mCacheManager:Ljava/util/HashMap;

    return-object v0
.end method

.method public static clearCache()V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mCacheManager:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 69
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->cancel()Z

    .line 138
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCanceled:Z

    .line 139
    return-void
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mSearchKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCanceled:Z

    return v0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCompleted:Z

    return v0
.end method

.method public requestAutoSearch(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/OnResultReceiver;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->cancel()Z

    .line 97
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCanceled:Z

    .line 100
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mCacheManager:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mSearchKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mCacheManager:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->mSearchKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 103
    if-eqz v0, :cond_1

    .line 105
    iput-boolean v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCanceled:Z

    .line 106
    const/4 v1, 0x1

    invoke-interface {p2, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/OnResultReceiver;->onResult(ZLjava/lang/Object;)V

    .line 130
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/d;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/OnResultReceiver;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->autoCompleteSearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 128
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->request:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method public setCompleted(Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->bCompleted:Z

    .line 82
    return-void
.end method
