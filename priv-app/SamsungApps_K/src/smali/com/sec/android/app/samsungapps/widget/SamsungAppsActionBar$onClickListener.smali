.class public interface abstract Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract onActionItemActionBar(ILandroid/view/View;)V
.end method

.method public abstract onLongClickActionItemActionBar(ILandroid/view/View;)Z
.end method

.method public abstract onNavigationActionBar(ILandroid/view/View;)V
.end method

.method public abstract onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
.end method

.method public abstract onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
.end method

.method public abstract onSamsungAppsCreateSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z
.end method

.method public abstract onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
.end method

.method public abstract onSamsungAppsOpenSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z
.end method
