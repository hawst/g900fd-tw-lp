.class public Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactoryForDemo;
.super Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactory;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactory;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    .line 20
    return-void
.end method


# virtual methods
.method public createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;
    .locals 6

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    new-instance v4, Lcom/sec/android/app/samsungapps/helper/n;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/helper/n;-><init>(Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactoryForDemo;)V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactoryForDemo;->mDownloadItemCreator:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V

    return-object v0
.end method

.method protected createPermissionFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/l;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/helper/l;-><init>(Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactoryForDemo;)V

    return-object v0
.end method
