.class public Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "ProGuard"


# instance fields
.field private final a:I

.field private b:Landroid/content/Context;

.field private c:Landroid/os/Handler;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->a:I

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->b:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/z;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/z;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->c:Landroid/os/Handler;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->d:Z

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->b:Landroid/content/Context;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->a:I

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->b:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/z;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/z;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->c:Landroid/os/Handler;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->d:Z

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->b:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->isFakeDragging()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->beginFakeDrag()Z

    .line 37
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->c:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->c:Landroid/os/Handler;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 45
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getCurrentItem()I

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 89
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 71
    :pswitch_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->d:Z

    .line 72
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->skipInGallery(F)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->d:Z

    goto :goto_1

    .line 78
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->d:Z

    if-ne v1, v2, :cond_0

    goto :goto_1

    .line 84
    :pswitch_2
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->d:Z

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public skipInGallery(F)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    .line 94
    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->getMainViewFragment()Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_1

    .line 97
    const v1, 0x7f0c01b5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 98
    if-eqz v0, :cond_1

    .line 99
    const v1, 0x7f0c018d

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    .line 100
    if-eqz v1, :cond_1

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->b:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    add-float/2addr v2, v3

    .line 106
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->getBottom()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v2

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    .line 107
    cmpg-float v1, v2, p1

    if-gez v1, :cond_1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 108
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
