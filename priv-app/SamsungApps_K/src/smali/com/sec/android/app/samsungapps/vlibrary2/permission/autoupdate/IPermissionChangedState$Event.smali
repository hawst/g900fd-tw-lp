.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

.field public static final enum CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

.field public static final enum FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

.field public static final enum SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

.field public static final enum SUCCESS_RECIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    const-string v1, "SUCCESS_RECIVE_PERMISSION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->SUCCESS_RECIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    const-string v1, "FAIL_RECEIVE_PERMISSION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    const-string v1, "SAME_PERMISSION"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    const-string v1, "CHANGED_PERMISSION"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->SUCCESS_RECIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    return-object v0
.end method
