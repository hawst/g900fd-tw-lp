.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private mJob:Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 8
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;->mJob:Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;

    .line 13
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public onEndParse()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onEndParse()V

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;->mJob:Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;->mJob:Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;->onSuccess()V

    .line 43
    :cond_0
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method
