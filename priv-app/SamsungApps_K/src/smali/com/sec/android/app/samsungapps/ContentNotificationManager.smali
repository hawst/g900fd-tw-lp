.class public Lcom/sec/android/app/samsungapps/ContentNotificationManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    .line 21
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 22
    return-void
.end method


# virtual methods
.method public onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 124
    invoke-virtual {p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)V

    .line 128
    return-void
.end method

.method public onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 137
    sget-object v0, Lcom/sec/android/app/samsungapps/bz;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 161
    :goto_0
    :pswitch_0
    return-void

    .line 141
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->onDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    goto :goto_0

    .line 146
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getLoadType()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v6

    int-to-long v8, v6

    invoke-direct {v5, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v8

    int-to-long v8, v8

    invoke-direct {v6, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDontOpenDetailPage()Z

    move-result v9

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getFakeModel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->isCancellable()Z

    move-result v11

    move v8, v7

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->setItemInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    goto :goto_0

    .line 149
    :pswitch_3
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getLoadType()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v6

    int-to-long v8, v6

    invoke-direct {v5, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v8

    int-to-long v8, v8

    invoke-direct {v6, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDontOpenDetailPage()Z

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->isCancellable()Z

    move-result v11

    move v8, v7

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->addInstallItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    goto :goto_0

    .line 152
    :pswitch_4
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    const-wide/16 v8, 0x0

    invoke-direct {v5, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v0

    int-to-long v8, v0

    invoke-direct {v6, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getLoadType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDontOpenDetailPage()Z

    move-result v9

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getFakeModel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->isCancellable()Z

    move-result v11

    move v8, v7

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->setDownloadProgress(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    goto/16 :goto_0

    .line 158
    :pswitch_5
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getLoadType()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v6

    int-to-long v8, v6

    invoke-direct {v5, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v8

    int-to-long v8, v8

    invoke-direct {v6, v8, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDontOpenDetailPage()Z

    move-result v9

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getFakeModel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->isCancellable()Z

    move-result v11

    move v8, v7

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->removeItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    goto/16 :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 132
    invoke-virtual {p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)V

    .line 133
    return-void
.end method

.method public onDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 18

    .prologue
    .line 72
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v2

    int-to-long v14, v2

    .line 73
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v16, v0

    .line 74
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onDownloadProgress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 75
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v3

    .line 76
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v4

    .line 77
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getName()Ljava/lang/String;

    move-result-object v5

    .line 80
    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v7, v14, v15}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    .line 83
    new-instance v8, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-wide/from16 v0, v16

    invoke-direct {v8, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    .line 85
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getLoadType()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDontOpenDetailPage()Z

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getFakeModel()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->isCancellable()Z

    move-result v13

    invoke-direct/range {v2 .. v13}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V

    .line 88
    const-wide/16 v3, 0x64

    mul-long/2addr v3, v14

    :try_start_0
    div-long v3, v3, v16

    long-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->setDownloadProgress(I)V
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    invoke-interface {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 99
    return-void

    .line 91
    :catch_0
    move-exception v3

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->setDownloadProgress(I)V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 27
    return-void
.end method
