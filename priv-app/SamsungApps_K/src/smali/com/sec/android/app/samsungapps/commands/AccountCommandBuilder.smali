.class public Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    :goto_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    :goto_1
    new-instance v3, Lcom/sec/android/app/samsungapps/commands/a;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/commands/a;-><init>()V

    new-instance v4, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    const/4 v5, 0x1

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Z)V

    new-instance v3, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    new-instance v4, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;-><init>()V

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    :goto_2
    invoke-direct {v6, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    invoke-direct {v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;)V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->a:Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;

    .line 55
    return-void

    .line 44
    :cond_0
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->context:Landroid/content/Context;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->context:Landroid/content/Context;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->context:Landroid/content/Context;

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 59
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    new-instance v0, Lcom/sec/android/app/samsungapps/commands/h;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/h;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    const/4 v3, 0x1

    invoke-direct {v2, p1, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Z)V

    new-instance v3, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    new-instance v4, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;-><init>()V

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    invoke-direct {v5, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;)V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->a:Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;

    .line 70
    return-void
.end method


# virtual methods
.method public createCancellableLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;
    .locals 2

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->bAutoLogin:Z

    if-eqz v0, :cond_0

    .line 304
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/g;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    .line 317
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->bAutoLogin:Z

    if-eqz v0, :cond_0

    .line 251
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/d;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    .line 268
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/e;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    goto :goto_0
.end method

.method public createRegisterVoucherCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/j;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/commands/i;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/commands/j;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V

    return-object v0
.end method

.method public doAfterLogin()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 322
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->doAfterLogin()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isPushSvcTurnedOn()Z

    move-result v1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->isUsablePushService(Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v3, :cond_0

    if-ne v1, v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "extra_command"

    const-string v3, "cmd_registration"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 324
    :cond_0
    return-void
.end method

.method public getConfirmPasswordViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/f;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/f;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method protected getIranDebitCardRegisterActivity()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    return-object v0
.end method

.method protected getLoginViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/n;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/n;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method public getNewInterfaceViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 231
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/c;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method protected getOldSamsungAccountLoginInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/m;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/m;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method public getPasswordConfirmViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 202
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/o;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/o;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method protected getPushServiceRegCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/sec/android/app/samsungapps/uisideCommand/PushServiceRegCommand;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/uisideCommand/PushServiceRegCommand;-><init>()V

    .line 142
    return-object v0
.end method

.method protected getRegisterCreditCardInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->viewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    return-object v0
.end method

.method protected getRequestTokenViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/l;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/l;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method protected getSamsungAccountHandlerClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 110
    const-class v0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;

    return-object v0
.end method

.method protected getSamsungAccountNewHandlerClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;

    return-object v0
.end method

.method protected getSignInActivity()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/k;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method

.method public getValidationViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/b;-><init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V

    return-object v0
.end method
